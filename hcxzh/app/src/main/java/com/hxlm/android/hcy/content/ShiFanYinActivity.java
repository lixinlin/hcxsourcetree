package com.hxlm.android.hcy.content;

import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.view.ConfirmDialog;

import java.util.List;

public class ShiFanYinActivity extends BaseActivity implements OnClickListener {
    private TextView mInfoTv;
    private ListView yueyaoItemsListView;
    private ShiFanYinListAdapter shiFanYinListAdapter;
    private final Handler serviceHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case ShiFanYinManager.REFRESH_LIST_VIEW:
                    shiFanYinListAdapter.notifyDataSetChanged();
                    break;
                default:
                    break;
            }
            return false;
        }
    });
    private ConfirmDialog downloadDialog;

    private int toDownloadPosition = Constant.UNDEFINED;
    private ShiFanYinManager shiFanYinManager;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_shifanyin);
        initViews();
        initYueyaoListView();

        shiFanYinManager = new ShiFanYinManager(serviceHandler);

        shiFanYinManager.getOrderedItems(new AbstractDefaultHttpHandlerCallback(this) {
            @Override
            protected void onResponseSuccess(Object obj) {
                List<ResourceItem> yueyaoItems = (List<ResourceItem>) obj;
                if (yueyaoItems.size() > 0) {
                    mInfoTv.setVisibility(View.GONE);
                    yueyaoItemsListView.setVisibility(View.VISIBLE);

                    shiFanYinListAdapter.setDatas(yueyaoItems);
                    shiFanYinListAdapter.notifyDataSetChanged();
                } else {
                    // 查询成功，但没有数据
                    mInfoTv.setVisibility(View.VISIBLE);
                    yueyaoItemsListView.setVisibility(View.GONE);
                    mInfoTv.setText(getString(R.string.shifanyin_zanshiweitigong));
                }
            }

            @Override
            protected void onHttpError(int httpErrorCode) {
                // 查询失败
                mInfoTv.setVisibility(View.VISIBLE);
                yueyaoItemsListView.setVisibility(View.GONE);
                mInfoTv.setText(getString(R.string.shifanyin_chaxunshibai));
            }

            @Override
            protected void onResponseError(int errorCode, String errorDesc) {
                mInfoTv.setVisibility(View.VISIBLE);
                yueyaoItemsListView.setVisibility(View.GONE);
                mInfoTv.setText(getString(R.string.shifanyin_chaxunshibai));
            }
        });
    }

    @Override
    public void initViews() {

    }

    @Override
    public void initDatas() {
        TitleBarView titleView = new TitleBarView();
        titleView.init(ShiFanYinActivity.this, getString(R.string.shifanyin_yundongshifanyin), titleView, 1);
        ImageView mDownload = (ImageView) findViewById(R.id.download_button);
        mDownload.setOnClickListener(this);
    }


    private void initYueyaoListView() {
        mInfoTv = (TextView) findViewById(R.id.info_tv);
        yueyaoItemsListView = (ListView) findViewById(R.id.my_list);

        shiFanYinListAdapter = new ShiFanYinListAdapter(this);
        yueyaoItemsListView.setAdapter(shiFanYinListAdapter);

        yueyaoItemsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final ResourceItem item = shiFanYinListAdapter.getItem(position);

                switch (item.getItemStatus()) {
                    case ResourceItem.NOT_ORDERED:
                        if (downloadDialog == null) {
                            initDownloadDialog();
                        }
                        toDownloadPosition = position;
                        downloadDialog.show();
                        break;

                    case ResourceItem.NOT_DOWNLOAD:
                        if (downloadDialog == null) {
                            initDownloadDialog();
                        }
                        toDownloadPosition = position;
                        downloadDialog.show();
                        break;
                    default:
                        break;
                }
                shiFanYinListAdapter.notifyDataSetChanged();
            }
        });
    }

    private void initDownloadDialog() {
        //初始化确认下载的Dialog
        downloadDialog = new ConfirmDialog(this);

        downloadDialog.setTextResourceId(R.string.downlistitem);

        downloadDialog.setCancelListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                toDownloadPosition = Constant.UNDEFINED;
                downloadDialog.dismiss();
            }
        });

        // 确认
        downloadDialog.setOkListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (toDownloadPosition != Constant.UNDEFINED) {
                    final ResourceItem item = shiFanYinListAdapter.getItem(toDownloadPosition);

                    item.setItemStatus(ResourceItem.DOWNLOADING);
                    shiFanYinListAdapter.notifyDataSetChanged();

                    downloadDialog.dismiss();
                    shiFanYinManager.downloadItemFile(item, new AbstractDefaultHttpHandlerCallback(ShiFanYinActivity.this) {
                        @Override
                        protected void onResponseSuccess(Object obj) {
                            item.setItemStatus(ResourceItem.STOP);
                            shiFanYinListAdapter.notifyDataSetChanged();
                        }

                        @Override
                        protected void onResponseError(int errorCode, String errorDesc) {
                            Toast.makeText(ShiFanYinActivity.this, getString(R.string.shifanyin_xiazai) + item.getName() +
                                    getString(R.string.shifanyin_shibai), Toast.LENGTH_SHORT).show();
                            shiFanYinListAdapter.notifyDataSetChanged();
                        }

                        @Override
                        protected void onProgress(int bytesWritten, int totalSize) {
                            item.setTotalSize(totalSize);
                            item.setDownloadedSize(bytesWritten);
                            shiFanYinListAdapter.notifyDataSetChanged();
                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.download_button:
                for (int i = 0; i < 35; i++) {
                    final ResourceItem item = shiFanYinListAdapter.getItem(i);

                    if (item.getItemStatus() != ResourceItem.STOP) {

                        item.setItemStatus(ResourceItem.DOWNLOADING);
                        shiFanYinListAdapter.notifyDataSetChanged();

                        shiFanYinManager.downloadItemFile(item, new AbstractDefaultHttpHandlerCallback(ShiFanYinActivity.this) {
                            @Override
                            protected void onResponseSuccess(Object obj) {
                                item.setItemStatus(ResourceItem.STOP);
                                shiFanYinListAdapter.notifyDataSetChanged();
                            }

                            @Override
                            protected void onResponseError(int errorCode, String errorDesc) {
                                Toast.makeText(ShiFanYinActivity.this, getString(R.string.shifanyin_xiazai )+ item.getName() +
                                        getString(R.string.shifanyin_shibai), Toast.LENGTH_SHORT).show();
                                shiFanYinListAdapter.notifyDataSetChanged();
                            }

                            @Override
                            protected void onProgress(int bytesWritten, int totalSize) {
                                item.setTotalSize(totalSize);
                                item.setDownloadedSize(bytesWritten);
                                shiFanYinListAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                }
                break;
            default:
                break;
        }
    }
}
