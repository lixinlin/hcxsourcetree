package com.hxlm.android.hcy.report;

import com.hxlm.hcyandroid.bean.Subject;

import java.util.Date;

public class Report {
	private int id;
	private long createDate;
	private long modiftyDate;
	private Subject subject;
	private String reportJson;
	private Date createTime;


	public String getReportJson() {
		return reportJson;
	}

	public void setReportJson(String reportJson) {
		this.reportJson = reportJson;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getCreateDate() {
		return createDate;
	}

	public void setCreateDate(long createDate) {
		this.createDate = createDate;
	}

	public long getModiftyDate() {
		return modiftyDate;
	}

	public void setModiftyDate(long modiftyDate) {
		this.modiftyDate = modiftyDate;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return "Report{" +
				"id=" + id +
				", createDate=" + createDate +
				", modiftyDate=" + modiftyDate +
				", subject=" + subject +
				", reportJson='" + reportJson + '\'' +
				'}';
	}
}
