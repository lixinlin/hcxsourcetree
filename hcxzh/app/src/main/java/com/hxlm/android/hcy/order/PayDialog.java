package com.hxlm.android.hcy.order;

import com.hcy.ky3h.R;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 用于确认用户的购买乐药
 */
public class PayDialog extends Dialog {
    private View.OnClickListener cancelListener;
    private View.OnClickListener okListener;
    private int textResourceId;
    private String textPrice;
    private boolean isCancleDisabled =false;

    public PayDialog(Context context) {
        super(context, R.style.dialog);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.pay_dialog);

        final TextView text = (TextView) findViewById(R.id.text);
        text.setText(textResourceId);

        final View closeView = findViewById(R.id.close);
        closeView.setVisibility(View.GONE);
        
        final TextView price = (TextView) findViewById(R.id.price);
        price.setText(textPrice);

        final ImageView cancel = (ImageView) findViewById(R.id.duihuan);
        if(!isCancleDisabled) {
            cancel.setVisibility(View.VISIBLE);
            cancel.setImageResource(R.drawable.an_fanhui1);
            cancel.setOnClickListener(cancelListener);
        }else {
            cancel.setVisibility(View.GONE);
        }

        final ImageView ok = (ImageView) findViewById(R.id.goumai);
        ok.setImageResource(R.drawable.an_queding1);
        ok.setOnClickListener(okListener);
    }

    public void setCancelListener(View.OnClickListener cancelListener) {
        this.cancelListener = cancelListener;
    }

    public void setOkListener(View.OnClickListener okListener) {
        this.okListener = okListener;
    }

    public void setTextResourceId(int textResourceId) {
        this.textResourceId = textResourceId;
    }
    public void setTextPrice(String textPrice) {
        this.textPrice = textPrice;
    }

    public boolean isCancleDisabled() {
        return isCancleDisabled;
    }

    public void setCancleDisabled(boolean isDisableCancle) {
        this.isCancleDisabled = isDisableCancle;
    }
}
