package com.hxlm.android.hcy.user;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hhmedic.bean.MemberInfoBean;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.asynchttp.HcyHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * @author Lirong
 * @date 2018/12/14.
 * @description
 */

public class HhUserManager {
    /**
     * 用户信息查询
     * @param callback
     */
    public void getMemberInfo(AbstractDefaultHttpHandlerCallback callback){
        String url = "/md/getMemberInfo.jhtml";

        RequestParams params = new RequestParams();
        params.put("memberId",LoginControllor.getLoginMember().getId());

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                try {
                    JSONObject data = JSON.parseObject(content);
                    String userToken = data.getString("userToken");
                    String uuid = data.getString("uuid");
                    MemberInfoBean memberInfoBean = new MemberInfoBean();
                    memberInfoBean.setToken(userToken);
                    memberInfoBean.setUuid(uuid);
                    return memberInfoBean;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        }, LoginControllor.MEMBER, callback.getContext());
    }
}
