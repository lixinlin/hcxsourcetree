package com.hxlm.android.hcy.content;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;

/**
 * Created by l on 2016/12/15.
 * 乐药支付成功之后，刷新当前页面的乐药状态
 */
public class YueYaoStaUpdateBroadcast extends BroadcastReceiver{

    public static final int IS_YUEYAO_STAUPDATE=11000;//乐药页面的乐药状态刷新

    private Handler handler;
    public YueYaoStaUpdateBroadcast(Handler handler)
    {
        this.handler=handler;
    }
    //设置action
    public static final String ACTION="com.hxlm.android.hcy.content.intent.action.YueYaoStaUpdateBroadcast";

    @Override
    public void onReceive(Context context, Intent intent) {

        Message message=Message.obtain();
        message.what=IS_YUEYAO_STAUPDATE;
        handler.sendMessage(message);
    }
}
