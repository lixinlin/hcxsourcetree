package com.hxlm.android.hcy.order;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.hcy.ky3h.R;
import com.hxlm.hcyandroid.BaseApplication;
import com.hxlm.hcyandroid.Constant;

import java.util.Calendar;
import java.util.List;

/**
 * 显示卡列表
 * Created by Zhenyu on 2016/4/20.
 */
public class CardsListAdapter extends BaseAdapter {

    private final LayoutInflater inflater;

    private List<Card> myCards;

    private final boolean isCheckButtonGone;

    public CardsListAdapter(Context context, List<Card> myCards,boolean isCheckButtonGone) {
        inflater = LayoutInflater.from(context);
        this.myCards = myCards;
        this.isCheckButtonGone = isCheckButtonGone;
    }

    @Override
    public int getCount() {
        return myCards.size();
    }

    @Override
    public Object getItem(int position) {
        return myCards.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            holder = new Holder();

            convertView = inflater.inflate(R.layout.reservation_card_item, null);
            holder.iv_card_icon = (ImageView) convertView.findViewById(R.id.iv_card_icon);
            holder.tv_card_name = (TextView) convertView.findViewById(R.id.tv_card_name);
            holder.tv_balance = (TextView) convertView.findViewById(R.id.tv_balance);
            holder.tv_balance2 = (TextView) convertView.findViewById(R.id.tv_balance2);
            holder.tv_valid_date = (TextView) convertView.findViewById(R.id.tv_valid_date);
            holder.iv_card_checked_status = (ImageView) convertView.findViewById(R.id.iv_card_checked_status);
            holder.tv_valid_is_date = (TextView) convertView.findViewById(R.id.tv_valid_is_date);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        // 得到每一个mycard
        Card card = myCards.get(position);
        holder.tv_card_name.setText(card.getCashcard().getName());
        holder.tv_balance.setText(Constant.MONEY_PREFIX + card.getBalance());// 余额
        holder.tv_balance2.setText(BaseApplication.getContext().getString(R.string.cash_card_pay_adapter_zonge)+card.getCashcard().getAmount() + BaseApplication.getContext().getString(R.string.cash_card_pay_adapter_yuan));// 总额

        // 有效期：
        Calendar calendar = Calendar.getInstance();

        int expiredDays = card.getExpiredTime();

        if (expiredDays == 0) {
            calendar.setTimeInMillis(card.getCashcard().getEndDate());
        } else {
            calendar.setTimeInMillis(card.getBindDate());
            calendar.add(Calendar.DAY_OF_MONTH, expiredDays);
        }
        holder.tv_valid_date.setText(new StringBuilder().append(calendar.get(Calendar.YEAR)).append("-").
                append(calendar.get(Calendar.MONTH)+1).append("-").append(calendar.get(Calendar.DAY_OF_MONTH)));

        // 判断是否快过期
        if (card.isDated()) {
            holder.tv_valid_is_date.setText(BaseApplication.getContext().getString(R.string.cash_card_pay_adapter_kuaiguoqi));
            holder.tv_valid_is_date.setTextColor(parent.getResources().getColor(R.color.fc5959));
        }

        if(isCheckButtonGone){
            holder.iv_card_checked_status.setVisibility(View.GONE);
        }else {
            if (card.isChoosed()) {
                holder.iv_card_checked_status.setImageResource(R.drawable.card_checked);
            } else {
                holder.iv_card_checked_status.setImageResource(R.drawable.card_not_checked);
            }
        }

        return convertView;
    }

    private static class Holder {
        ImageView iv_card_icon;// 卡的图片
        TextView tv_card_name;// 卡名
        TextView tv_balance;// 余额
        TextView tv_balance2;// 总金额
        TextView tv_valid_date;// 有效日期
        ImageView iv_card_checked_status;// 选中与未选中时显示的图片
        TextView tv_valid_is_date;// 判断是否过期
    }
}
