package com.hxlm.android.hcy.asynchttp;

import android.os.Handler;
import android.os.Message;

/**
 * 和畅依平台http请求Handler基类
 * <p/>
 * Created by Zhenyu on 2016/4/14.
 */
public abstract class AbstractHttpHandlerCallback implements Handler.Callback {
    final static int ON_RESPONSE_SUCCESS = 1001;//请求成功
    final static int ON_RESPONSE_ERROR = 1002;//请求错误
    final static int ON_HTTP_ERROR = 1003;//http请求错误
    final static int ON_NETWORK_ERROR = 1004;//网络错误
    final static int ON_PROGRESS = 1005;

    protected final static int ON_CREATE_PATH_FAIL = 10001;//创建文件路径

    HttpRequest httpRequest;

    @Override
    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case ON_RESPONSE_SUCCESS:
                onResponseSuccess(msg.obj);
                break;
            case ON_RESPONSE_ERROR:
                onResponseError(msg.arg1, (String) msg.obj);
                break;
            case ON_HTTP_ERROR:
                onHttpError(msg.arg1);
                break;
            case ON_NETWORK_ERROR:
                onNetworkError();
                break;
            case ON_PROGRESS:
                onProgress(msg.arg1,msg.arg2);
                break;
        }
        return false;
    }

    void setHttpRequest(HttpRequest httpRequest) {
        this.httpRequest = httpRequest;
    }

    public abstract void preSubmit();

    protected abstract void onResponseSuccess(Object obj);

    protected abstract void onResponseError(int errorCode, String errorDesc);

    protected abstract void onHttpError(int httpErrorCode);

    protected abstract void onNetworkError();

    protected abstract void onProgress(int bytesWritten, int totalSize);
}
