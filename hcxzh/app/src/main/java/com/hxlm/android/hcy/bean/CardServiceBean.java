package com.hxlm.android.hcy.bean;

/**
 * @author Lirong
 * @date 2018/12/20.
 * @description  剩余服务bean
 */

public class CardServiceBean {
        /**
         * id : 1
         * member : 38
         * cardId : 1
         * cardNum : NGKHY99QMOKMOTMA7ZV
         * serviceName : 面对面咨询
         * unit : 次
         * value : 100
         * cardName : 测试服务卡
         * type : null
         * serviceCode : 100001
         * status : 1
         */

        private String id;
        private String member;
        private String cardId;
        private String cardNum;
        private String serviceName;
        private String unit;
        private String value;
        private String cardName;
        private Object type;
        private String serviceCode;
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getMember() {
            return member;
        }

        public void setMember(String member) {
            this.member = member;
        }

        public String getCardId() {
            return cardId;
        }

        public void setCardId(String cardId) {
            this.cardId = cardId;
        }

        public String getCardNum() {
            return cardNum;
        }

        public void setCardNum(String cardNum) {
            this.cardNum = cardNum;
        }

        public String getServiceName() {
            return serviceName;
        }

        public void setServiceName(String serviceName) {
            this.serviceName = serviceName;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getCardName() {
            return cardName;
        }

        public void setCardName(String cardName) {
            this.cardName = cardName;
        }

        public Object getType() {
            return type;
        }

        public void setType(Object type) {
            this.type = type;
        }

        public String getServiceCode() {
            return serviceCode;
        }

        public void setServiceCode(String serviceCode) {
            this.serviceCode = serviceCode;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

    @Override
    public String toString() {
        return "CardServiceBean{" +
                "id='" + id + '\'' +
                ", member='" + member + '\'' +
                ", cardId='" + cardId + '\'' +
                ", cardNum='" + cardNum + '\'' +
                ", serviceName='" + serviceName + '\'' +
                ", unit='" + unit + '\'' +
                ", value='" + value + '\'' +
                ", cardName='" + cardName + '\'' +
                ", type=" + type +
                ", serviceCode='" + serviceCode + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
