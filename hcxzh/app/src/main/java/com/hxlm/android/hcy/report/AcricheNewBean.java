package com.hxlm.android.hcy.report;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by lixinlin on 2018/12/14.
 */

public class AcricheNewBean {

    /**
     * XLBS : {"id":3937,"createDate":1478845072000,"modifyDate":1478845072000,"reportJson":"{\"titles\":[]}","subject":{"id":48,"createDate":1443165120000,"modifyDate":1462950021000,"order":null,"name":"轻度焦虑","picture":"http://app.ky3h.com:8001/healthlm/upload/image/2015/1030/400707f5-aed0-4d76-86df-91fdd09366d6.png","subject_sn":"SAS-02","introduction":"情绪上有<轻度的焦虑倾向>，如烦躁、坐立不安、神经过敏等不良情绪困扰。建议您调整心态，放松心情，解除心理紧张和心理压力","memo":null,"subjectAttributes":[]},"subjectCategorySn":"SAS20","isRead":false,"createTime":"2016-11-11","phoneReport":{},"parentName":null}
     * SAS20 : {"id":3937,"createDate":1478845072000,"modifyDate":1478845072000,"reportJson":"{\"titles\":[]}","subject":{"id":48,"createDate":1443165120000,"modifyDate":1462950021000,"order":null,"name":"轻度焦虑","picture":"http://app.ky3h.com:8001/healthlm/upload/image/2015/1030/400707f5-aed0-4d76-86df-91fdd09366d6.png","subject_sn":"SAS-02","introduction":"情绪上有<轻度的焦虑倾向>，如烦躁、坐立不安、神经过敏等不良情绪困扰。建议您调整心态，放松心情，解除心理紧张和心理压力","memo":null,"subjectAttributes":[]},"subjectCategorySn":"SAS20","isRead":false,"createTime":"2016-11-11","phoneReport":{},"parentName":null}
     * SDS20 : {"id":3936,"createDate":1478844929000,"modifyDate":1478844929000,"reportJson":"{\"titles\":[]}","subject":{"id":51,"createDate":1443165275000,"modifyDate":1446190494000,"order":null,"name":"无抑郁倾向","picture":"http://app.ky3h.com:8001/healthlm/upload/image/2015/1030/65e7b62e-abe2-4c89-891e-71d3d3f38e1d.png","subject_sn":"SDS-01","introduction":"无明显抑郁倾向，继续保持良好心态。","memo":null,"subjectAttributes":[]},"subjectCategorySn":"SDS20","isRead":false,"createTime":"2016-11-11","phoneReport":{},"parentName":null}
     * oxygen : {"id":561,"createDate":1528858196000,"modifyDate":1528858196000,"density":0.97,"isOk":true}
     * bloodPressure : {"id":656,"createDate":1510543882000,"modifyDate":1510543882000,"highPressure":118,"lowPressure":79,"pulse":88,"isOk":true}
     * ecg : {"id":512,"createDate":1513670933000,"modifyDate":1513670933000,"path":"http://119.254.24.4:7006/upload/file/201712/1219/bbc9521e-84fc-42f5-b088-dc0fb4125b18.ecg","heartRate":58,"subject":{"id":57,"createDate":1452152143000,"modifyDate":1467881766000,"order":null,"name":"心率过缓","picture":null,"subject_sn":"XLGH","introduction":"心率过缓。","memo":null,"subjectAttributes":[]},"content":null,"createTime":"2017-12-19"}
     * report : {"id":4,"createDate":1545285991000,"modifyDate":1545285991000,"year":"2018","quarter":3,"jlbsjson":"2018-07-06 09:40:52_少羽,2018-07-06 09:41:22_少羽,2018-07-06 09:43:26_桎羽,2018-07-06 09:44:21_桎羽,2018-07-06 10:01:36_桎羽,2018-07-11 13:56:58_桎羽,2018-07-11 13:58:14_桎羽,2018-07-11 14:00:09_桎羽,2018-07-11 14:00:49_桎羽,2018-07-11 14:03:48_判角,2018-07-11 14:10:26_少羽,2018-07-13 14:09:45_桎羽,2018-07-13 14:11:23_判角,2018-07-13 14:12:49_大角,2018-07-13 14:14:14_大宫,2018-07-13 14:16:18_大宫,2018-07-13 14:18:00_桎羽,2018-07-13 14:20:10_桎羽,2018-07-13 14:22:05_桎羽,2018-07-13 14:23:48_大角,2018-07-13 14:25:05_桎羽,2018-07-13 14:26:32_上宫,2018-07-13 14:28:03_大羽,2018-07-13 14:32:06_上宫,2018-07-13 15:10:25_桎羽,2018-07-13 15:11:49_桎羽,2018-07-13 15:13:08_桎羽,2018-07-13 15:14:36_大宫,2018-07-13 15:16:02_少羽,2018-07-13 15:17:23_桎羽,2018-07-13 15:18:52_桎羽,2018-07-13 15:20:11_大宫,2018-07-13 15:21:45_右商,2018-07-13 15:23:44_桎羽,2018-07-13 15:25:18_判角,2018-07-13 15:26:46_桎羽,2018-07-16 10:31:28_桎羽,2018-07-16 15:21:26_上角,2018-07-16 15:23:54_桎羽,2018-07-19 15:01:42_上角,2018-07-19 15:05:45_桎羽,2018-07-19 15:15:05_大角,2018-07-19 16:54:18_大宫,2018-07-20 11:09:58_左角宫,2018-07-20 14:19:07_桎羽,2018-07-20 14:19:57_桎羽,2018-07-26 15:55:26_上角,2018-07-26 15:55:57_上角,2018-07-26 16:01:49_桎羽,2018-07-26 16:02:26_桎羽,2018-07-26 16:06:43_加宫,2018-07-26 16:09:39_上角,2018-07-26 16:10:20_上角,2018-07-26 16:13:40_桎羽,2018-07-26 17:06:19_桎羽,2018-07-26 17:08:24_桎羽,2018-07-26 17:09:10_桎羽,2018-07-30 17:24:11_众羽,2018-07-30 17:33:41_判徵,2018-08-01 16:17:32_右商,2018-08-03 11:18:25_钛角,2018-08-03 11:19:57_桎羽,2018-08-03 11:57:52_桎羽,2018-08-03 11:58:50_判角,2018-08-03 14:10:03_加宫,2018-08-03 14:12:57_桎羽,2018-08-03 14:27:51_桎羽,2018-08-03 14:33:12_桎羽,2018-08-06 16:16:42_众羽,2018-08-06 16:17:13_判徵,2018-08-06 16:17:38_加宫,2018-08-06 16:18:17_判角,2018-08-07 10:13:04_桎羽,2018-08-08 16:39:50_桎羽,2018-08-08 17:27:17_桎羽,2018-08-09 09:50:36_桎羽,2018-08-09 14:16:08_判角,2018-08-10 15:35:12_少羽,2018-08-10 16:56:05_判角,2018-08-20 11:35:07_少羽,2018-08-20 11:35:46_桎羽,2018-08-30 17:29:19_少羽,2018-09-05 16:29:11_桎羽,2018-09-05 16:39:09_众羽,2018-09-05 16:42:08_少羽,2018-09-05 16:43:11_桎羽,2018-09-26 15:19:46_上宫,2018-09-26 15:22:11_上宫,2018-09-26 15:25:02_大羽,2018-09-26 15:27:54_大角,2018-09-26 15:30:17_桎羽,2018-09-26 15:30:52_桎羽,2018-09-26 15:33:47_桎羽,2018-09-26 15:36:58_大角,2018-09-26 15:37:37_大角,2018-09-26 15:40:37_钛角,2018-09-26 15:41:14_大羽,2018-09-26 15:44:19_桎羽,2018-09-26 15:44:51_桎羽,2018-09-26 15:47:25_质徵,2018-09-26 15:48:09_质徵,2018-09-26 15:51:33_桎羽,2018-09-26 15:52:16_桎羽,2018-09-26 16:09:07_桎羽,2018-09-26 16:09:54_钛角,2018-09-27 10:29:16_加宫,2018-09-27 10:36:28_众羽,2018-09-27 10:40:37_大羽,2018-09-27 12:00:08_少羽,2018-09-27 15:24:57_上宫,2018-09-27 15:38:25_加宫,2018-09-28 12:11:34_右商,2018-09-30 09:23:59_右商","jlbsremark":"","tzbsjson":"","tzbsremark":null,"zfbsjson":"2018-08-27 10:07:27_平和质,2018-08-28 13:41:07_平和质","zfbsremark":"","ecgjson":"2018-08-14 09:31:26_0,2018-08-14 09:31:32_0,2018-08-14 09:31:40_0,2018-08-19 11:05:12_0,2018-09-08 11:45:42_78","ecgremark":"2018-07-01至2018-09-30，您共进行了3次心率测量，其中心率正常0次，异常_3_次，您的心率不稳定，若排除外界因素，若有不适症状发生，请及时到医院就诊。","oxygenjson":"2018-08-25 17:10:34_0.900000,2018-08-25 18:46:59_0.950000,2018-09-25 18:47:49_0.900000","oxygenremark":null,"bloodjson":"2018-08-25 15:40:19_123_89,2018-09-13 14:43:49_25_34,2018-09-13 14:51:09_58_39","bloodremark":"2018-07-01至2018-09-30，您共进行了3次血压测量，其中血压正常0次，正常高值_1_次，血压偏低 __2_次，血压偏高_0_次。","bodytemjson":"2018-08-25 15:41:34_36.000000,2018-09-13 12:30:30_36.000000","bodytemremark":"","breathejson":"2018-08-25 15:42:51_35,2018-08-25 15:46:32_35,2018-09-13 14:37:54_78","breatheremark":"","status":0,"modify_user":null}
     * JLBS : {"id":6998,"createDate":1527149983000,"modifyDate":1527149983000,"reportJson":"{\"穴位调理\":\"http://eky3h.com/healthlm/upload/horizontalreport/content/201508/60.html\",\"运动调理\":\"http://eky3h.com/healthlm/upload/horizontalreport/content/201508/59.html\",\"状态结论\":\"http://eky3h.com/healthlm/upload/horizontalreport/content/201508/57.html\",\"titles\":[\"状态结论\",\"膳食调理\",\"运动调理\",\"乐药调理\",\"穴位调理\"],\"乐药调理\":\"http://eky3h.com/healthlm/upload/horizontalreport/content/201508/21.html\",\"膳食调理\":\"http://eky3h.com/healthlm/upload/horizontalreport/content/201508/58.html\"}","subject":{"id":21,"createDate":1436754225000,"modifyDate":1513045024000,"order":null,"name":"上商","picture":null,"subject_sn":"JLBS-S1","introduction":null,"memo":"胸闷、气短、乏力，活动时喜欢出汗","subjectAttributes":[]},"subjectCategorySn":"JLBS","isRead":false,"createTime":"2018-05-24","phoneReport":{"1":"http://eky3h.com/healthlm/upload/report/content/201508/57.html","2":"http://eky3h.com/healthlm/subject_report/viewreports.jhtml?subjectsn=JLBS-S1"},"parentName":null}
     * TZBS : {"id":7161,"createDate":1545123212000,"modifyDate":1545123212000,"reportJson":"{\"和畅方\":\"http://eky3h.com/healthlm/upload/horizontalreport/content/201508/145.html\",\"和畅体检报告\":\"http://eky3h.com/healthlm/upload/horizontalreport/content/201508/144.html\",\"titles\":[\"和畅体检报告\",\"和畅方\"]}","subject":{"id":11,"createDate":1436174835000,"modifyDate":1512979517000,"order":null,"name":"气郁质","picture":null,"subject_sn":"TZBS-05","introduction":null,"memo":"瘦者居多，多烦闷不乐。常出现胸胁、乳房胀痛，喜叹息，有的咽喉部有异物感。食欲不佳，睡眠较差","subjectAttributes":[]},"subjectCategorySn":"TZBS","isRead":false,"createTime":"2018-12-18","phoneReport":{"1":"http://eky3h.com/healthlm/upload/report/content/201508/144.html","2":"http://eky3h.com/healthlm/subject_report/viewreports.jhtml?subjectsn=TZBS-05"},"parentName":null}
     * ZFBS : {"id":null,"createDate":1527749727000,"modifyDate":null,"zhengzhuang_id":null,"zz_name":null,"zz_level":0,"icd_id":null,"icd_code":null,"cust_id":32,"physique_id":"180531145527042900","icd_name":null,"icd_name_str":"","zz_name_str":"出血浅淡,皮肤瘙痒,","createTime":"2018-05-31"}
     * bodyTemperature : {"id":296,"createDate":1511850474000,"modifyDate":1511850474000,"temperature":36.9,"isOk":true}
     */

    private OxygenBean oxygen;
    private BloodPressureBean bloodPressure;
    private EcgBean ecg;
    private ReportBean report;
    private JLBSBean JLBS;
    private TZBSBean TZBS;
    private ZFBSBean ZFBS;
    private BodyTemperatureBean bodyTemperature;

    public OxygenBean getOxygen() {
        return oxygen;
    }

    public void setOxygen(OxygenBean oxygen) {
        this.oxygen = oxygen;
    }

    public BloodPressureBean getBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(BloodPressureBean bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public EcgBean getEcg() {
        return ecg;
    }

    public void setEcg(EcgBean ecg) {
        this.ecg = ecg;
    }

    public ReportBean getReport() {
        return report;
    }

    public void setReport(ReportBean report) {
        this.report = report;
    }

    public JLBSBean getJLBS() {
        return JLBS;
    }

    public void setJLBS(JLBSBean JLBS) {
        this.JLBS = JLBS;
    }

    public TZBSBean getTZBS() {
        return TZBS;
    }

    public void setTZBS(TZBSBean TZBS) {
        this.TZBS = TZBS;
    }

    public ZFBSBean getZFBS() {
        return ZFBS;
    }

    public void setZFBS(ZFBSBean ZFBS) {
        this.ZFBS = ZFBS;
    }

    public BodyTemperatureBean getBodyTemperature() {
        return bodyTemperature;
    }

    public void setBodyTemperature(BodyTemperatureBean bodyTemperature) {
        this.bodyTemperature = bodyTemperature;
    }


    public static class OxygenBean {
        /**
         * id : 561
         * createDate : 1528858196000
         * modifyDate : 1528858196000
         * density : 0.97
         * isOk : true
         */

        private int id;
        private long createDate;
        private long modifyDate;
        private double density;
        private boolean isOk;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public long getCreateDate() {
            return createDate;
        }

        public void setCreateDate(long createDate) {
            this.createDate = createDate;
        }

        public long getModifyDate() {
            return modifyDate;
        }

        public void setModifyDate(long modifyDate) {
            this.modifyDate = modifyDate;
        }

        public double getDensity() {
            return density;
        }

        public void setDensity(double density) {
            this.density = density;
        }

        public boolean isIsOk() {
            return isOk;
        }

        public void setIsOk(boolean isOk) {
            this.isOk = isOk;
        }
    }

    public static class BloodPressureBean {
        /**
         * id : 656
         * createDate : 1510543882000
         * modifyDate : 1510543882000
         * highPressure : 118
         * lowPressure : 79
         * pulse : 88
         * isOk : true
         */

        private int id;
        private long createDate;
        private long modifyDate;
        private int highPressure;
        private int lowPressure;
        private int pulse;
        private boolean isOk;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public long getCreateDate() {
            return createDate;
        }

        public void setCreateDate(long createDate) {
            this.createDate = createDate;
        }

        public long getModifyDate() {
            return modifyDate;
        }

        public void setModifyDate(long modifyDate) {
            this.modifyDate = modifyDate;
        }

        public int getHighPressure() {
            return highPressure;
        }

        public void setHighPressure(int highPressure) {
            this.highPressure = highPressure;
        }

        public int getLowPressure() {
            return lowPressure;
        }

        public void setLowPressure(int lowPressure) {
            this.lowPressure = lowPressure;
        }

        public int getPulse() {
            return pulse;
        }

        public void setPulse(int pulse) {
            this.pulse = pulse;
        }

        public boolean isIsOk() {
            return isOk;
        }

        public void setIsOk(boolean isOk) {
            this.isOk = isOk;
        }
    }

    public static class EcgBean {
        /**
         * id : 512
         * createDate : 1513670933000
         * modifyDate : 1513670933000
         * path : http://119.254.24.4:7006/upload/file/201712/1219/bbc9521e-84fc-42f5-b088-dc0fb4125b18.ecg
         * heartRate : 58
         * subject : {"id":57,"createDate":1452152143000,"modifyDate":1467881766000,"order":null,"name":"心率过缓","picture":null,"subject_sn":"XLGH","introduction":"心率过缓。","memo":null,"subjectAttributes":[]}
         * content : null
         * createTime : 2017-12-19
         */

        private int id;
        private long createDate;
        private long modifyDate;
        private String path;
        private int heartRate;
        private SubjectBeanXXX subject;
        private Object content;
        private String createTime;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public long getCreateDate() {
            return createDate;
        }

        public void setCreateDate(long createDate) {
            this.createDate = createDate;
        }

        public long getModifyDate() {
            return modifyDate;
        }

        public void setModifyDate(long modifyDate) {
            this.modifyDate = modifyDate;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public int getHeartRate() {
            return heartRate;
        }

        public void setHeartRate(int heartRate) {
            this.heartRate = heartRate;
        }

        public SubjectBeanXXX getSubject() {
            return subject;
        }

        public void setSubject(SubjectBeanXXX subject) {
            this.subject = subject;
        }

        public Object getContent() {
            return content;
        }

        public void setContent(Object content) {
            this.content = content;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public static class SubjectBeanXXX {
            /**
             * id : 57
             * createDate : 1452152143000
             * modifyDate : 1467881766000
             * order : null
             * name : 心率过缓
             * picture : null
             * subject_sn : XLGH
             * introduction : 心率过缓。
             * memo : null
             * subjectAttributes : []
             */

            private int id;
            private long createDate;
            private long modifyDate;
            private Object order;
            private String name;
            private Object picture;
            private String subject_sn;
            private String introduction;
            private Object memo;
            private List<?> subjectAttributes;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public long getCreateDate() {
                return createDate;
            }

            public void setCreateDate(long createDate) {
                this.createDate = createDate;
            }

            public long getModifyDate() {
                return modifyDate;
            }

            public void setModifyDate(long modifyDate) {
                this.modifyDate = modifyDate;
            }

            public Object getOrder() {
                return order;
            }

            public void setOrder(Object order) {
                this.order = order;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public Object getPicture() {
                return picture;
            }

            public void setPicture(Object picture) {
                this.picture = picture;
            }

            public String getSubject_sn() {
                return subject_sn;
            }

            public void setSubject_sn(String subject_sn) {
                this.subject_sn = subject_sn;
            }

            public String getIntroduction() {
                return introduction;
            }

            public void setIntroduction(String introduction) {
                this.introduction = introduction;
            }

            public Object getMemo() {
                return memo;
            }

            public void setMemo(Object memo) {
                this.memo = memo;
            }

            public List<?> getSubjectAttributes() {
                return subjectAttributes;
            }

            public void setSubjectAttributes(List<?> subjectAttributes) {
                this.subjectAttributes = subjectAttributes;
            }
        }
    }

    public static class ReportBean {
        /**
         * id : 4
         * createDate : 1545285991000
         * modifyDate : 1545285991000
         * year : 2018
         * quarter : 3
         * jlbsjson : 2018-07-06 09:40:52_少羽,2018-07-06 09:41:22_少羽,2018-07-06 09:43:26_桎羽,2018-07-06 09:44:21_桎羽,2018-07-06 10:01:36_桎羽,2018-07-11 13:56:58_桎羽,2018-07-11 13:58:14_桎羽,2018-07-11 14:00:09_桎羽,2018-07-11 14:00:49_桎羽,2018-07-11 14:03:48_判角,2018-07-11 14:10:26_少羽,2018-07-13 14:09:45_桎羽,2018-07-13 14:11:23_判角,2018-07-13 14:12:49_大角,2018-07-13 14:14:14_大宫,2018-07-13 14:16:18_大宫,2018-07-13 14:18:00_桎羽,2018-07-13 14:20:10_桎羽,2018-07-13 14:22:05_桎羽,2018-07-13 14:23:48_大角,2018-07-13 14:25:05_桎羽,2018-07-13 14:26:32_上宫,2018-07-13 14:28:03_大羽,2018-07-13 14:32:06_上宫,2018-07-13 15:10:25_桎羽,2018-07-13 15:11:49_桎羽,2018-07-13 15:13:08_桎羽,2018-07-13 15:14:36_大宫,2018-07-13 15:16:02_少羽,2018-07-13 15:17:23_桎羽,2018-07-13 15:18:52_桎羽,2018-07-13 15:20:11_大宫,2018-07-13 15:21:45_右商,2018-07-13 15:23:44_桎羽,2018-07-13 15:25:18_判角,2018-07-13 15:26:46_桎羽,2018-07-16 10:31:28_桎羽,2018-07-16 15:21:26_上角,2018-07-16 15:23:54_桎羽,2018-07-19 15:01:42_上角,2018-07-19 15:05:45_桎羽,2018-07-19 15:15:05_大角,2018-07-19 16:54:18_大宫,2018-07-20 11:09:58_左角宫,2018-07-20 14:19:07_桎羽,2018-07-20 14:19:57_桎羽,2018-07-26 15:55:26_上角,2018-07-26 15:55:57_上角,2018-07-26 16:01:49_桎羽,2018-07-26 16:02:26_桎羽,2018-07-26 16:06:43_加宫,2018-07-26 16:09:39_上角,2018-07-26 16:10:20_上角,2018-07-26 16:13:40_桎羽,2018-07-26 17:06:19_桎羽,2018-07-26 17:08:24_桎羽,2018-07-26 17:09:10_桎羽,2018-07-30 17:24:11_众羽,2018-07-30 17:33:41_判徵,2018-08-01 16:17:32_右商,2018-08-03 11:18:25_钛角,2018-08-03 11:19:57_桎羽,2018-08-03 11:57:52_桎羽,2018-08-03 11:58:50_判角,2018-08-03 14:10:03_加宫,2018-08-03 14:12:57_桎羽,2018-08-03 14:27:51_桎羽,2018-08-03 14:33:12_桎羽,2018-08-06 16:16:42_众羽,2018-08-06 16:17:13_判徵,2018-08-06 16:17:38_加宫,2018-08-06 16:18:17_判角,2018-08-07 10:13:04_桎羽,2018-08-08 16:39:50_桎羽,2018-08-08 17:27:17_桎羽,2018-08-09 09:50:36_桎羽,2018-08-09 14:16:08_判角,2018-08-10 15:35:12_少羽,2018-08-10 16:56:05_判角,2018-08-20 11:35:07_少羽,2018-08-20 11:35:46_桎羽,2018-08-30 17:29:19_少羽,2018-09-05 16:29:11_桎羽,2018-09-05 16:39:09_众羽,2018-09-05 16:42:08_少羽,2018-09-05 16:43:11_桎羽,2018-09-26 15:19:46_上宫,2018-09-26 15:22:11_上宫,2018-09-26 15:25:02_大羽,2018-09-26 15:27:54_大角,2018-09-26 15:30:17_桎羽,2018-09-26 15:30:52_桎羽,2018-09-26 15:33:47_桎羽,2018-09-26 15:36:58_大角,2018-09-26 15:37:37_大角,2018-09-26 15:40:37_钛角,2018-09-26 15:41:14_大羽,2018-09-26 15:44:19_桎羽,2018-09-26 15:44:51_桎羽,2018-09-26 15:47:25_质徵,2018-09-26 15:48:09_质徵,2018-09-26 15:51:33_桎羽,2018-09-26 15:52:16_桎羽,2018-09-26 16:09:07_桎羽,2018-09-26 16:09:54_钛角,2018-09-27 10:29:16_加宫,2018-09-27 10:36:28_众羽,2018-09-27 10:40:37_大羽,2018-09-27 12:00:08_少羽,2018-09-27 15:24:57_上宫,2018-09-27 15:38:25_加宫,2018-09-28 12:11:34_右商,2018-09-30 09:23:59_右商
         * jlbsremark :
         * tzbsjson :
         * tzbsremark : null
         * zfbsjson : 2018-08-27 10:07:27_平和质,2018-08-28 13:41:07_平和质
         * zfbsremark :
         * ecgjson : 2018-08-14 09:31:26_0,2018-08-14 09:31:32_0,2018-08-14 09:31:40_0,2018-08-19 11:05:12_0,2018-09-08 11:45:42_78
         * ecgremark : 2018-07-01至2018-09-30，您共进行了3次心率测量，其中心率正常0次，异常_3_次，您的心率不稳定，若排除外界因素，若有不适症状发生，请及时到医院就诊。
         * oxygenjson : 2018-08-25 17:10:34_0.900000,2018-08-25 18:46:59_0.950000,2018-09-25 18:47:49_0.900000
         * oxygenremark : null
         * bloodjson : 2018-08-25 15:40:19_123_89,2018-09-13 14:43:49_25_34,2018-09-13 14:51:09_58_39
         * bloodremark : 2018-07-01至2018-09-30，您共进行了3次血压测量，其中血压正常0次，正常高值_1_次，血压偏低 __2_次，血压偏高_0_次。
         * bodytemjson : 2018-08-25 15:41:34_36.000000,2018-09-13 12:30:30_36.000000
         * bodytemremark :
         * breathejson : 2018-08-25 15:42:51_35,2018-08-25 15:46:32_35,2018-09-13 14:37:54_78
         * breatheremark :
         * status : 0
         * modify_user : null
         */

        private int id;
        private long createDate;
        private long modifyDate;
        private String year;
        private int quarter;
        private String jlbsjson;
        private String jlbsremark;
        private String tzbsjson;
        private Object tzbsremark;
        private String zfbsjson;
        private String zfbsremark;
        private String ecgjson;
        private String ecgremark;
        private String oxygenjson;
        private Object oxygenremark;
        private String bloodjson;
        private String bloodremark;
        private String bodytemjson;
        private String bodytemremark;
        private String breathejson;
        private String breatheremark;
        private int status;
        private Object modify_user;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public long getCreateDate() {
            return createDate;
        }

        public void setCreateDate(long createDate) {
            this.createDate = createDate;
        }

        public long getModifyDate() {
            return modifyDate;
        }

        public void setModifyDate(long modifyDate) {
            this.modifyDate = modifyDate;
        }

        public String getYear() {
            return year;
        }

        public void setYear(String year) {
            this.year = year;
        }

        public int getQuarter() {
            return quarter;
        }

        public void setQuarter(int quarter) {
            this.quarter = quarter;
        }

        public String getJlbsjson() {
            return jlbsjson;
        }

        public void setJlbsjson(String jlbsjson) {
            this.jlbsjson = jlbsjson;
        }

        public String getJlbsremark() {
            return jlbsremark;
        }

        public void setJlbsremark(String jlbsremark) {
            this.jlbsremark = jlbsremark;
        }

        public String getTzbsjson() {
            return tzbsjson;
        }

        public void setTzbsjson(String tzbsjson) {
            this.tzbsjson = tzbsjson;
        }

        public Object getTzbsremark() {
            return tzbsremark;
        }

        public void setTzbsremark(Object tzbsremark) {
            this.tzbsremark = tzbsremark;
        }

        public String getZfbsjson() {
            return zfbsjson;
        }

        public void setZfbsjson(String zfbsjson) {
            this.zfbsjson = zfbsjson;
        }

        public String getZfbsremark() {
            return zfbsremark;
        }

        public void setZfbsremark(String zfbsremark) {
            this.zfbsremark = zfbsremark;
        }

        public String getEcgjson() {
            return ecgjson;
        }

        public void setEcgjson(String ecgjson) {
            this.ecgjson = ecgjson;
        }

        public String getEcgremark() {
            return ecgremark;
        }

        public void setEcgremark(String ecgremark) {
            this.ecgremark = ecgremark;
        }

        public String getOxygenjson() {
            return oxygenjson;
        }

        public void setOxygenjson(String oxygenjson) {
            this.oxygenjson = oxygenjson;
        }

        public Object getOxygenremark() {
            return oxygenremark;
        }

        public void setOxygenremark(Object oxygenremark) {
            this.oxygenremark = oxygenremark;
        }

        public String getBloodjson() {
            return bloodjson;
        }

        public void setBloodjson(String bloodjson) {
            this.bloodjson = bloodjson;
        }

        public String getBloodremark() {
            return bloodremark;
        }

        public void setBloodremark(String bloodremark) {
            this.bloodremark = bloodremark;
        }

        public String getBodytemjson() {
            return bodytemjson;
        }

        public void setBodytemjson(String bodytemjson) {
            this.bodytemjson = bodytemjson;
        }

        public String getBodytemremark() {
            return bodytemremark;
        }

        public void setBodytemremark(String bodytemremark) {
            this.bodytemremark = bodytemremark;
        }

        public String getBreathejson() {
            return breathejson;
        }

        public void setBreathejson(String breathejson) {
            this.breathejson = breathejson;
        }

        public String getBreatheremark() {
            return breatheremark;
        }

        public void setBreatheremark(String breatheremark) {
            this.breatheremark = breatheremark;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public Object getModify_user() {
            return modify_user;
        }

        public void setModify_user(Object modify_user) {
            this.modify_user = modify_user;
        }
    }

    public static class JLBSBean {
        /**
         * id : 6998
         * createDate : 1527149983000
         * modifyDate : 1527149983000
         * reportJson : {"穴位调理":"http://eky3h.com/healthlm/upload/horizontalreport/content/201508/60.html","运动调理":"http://eky3h.com/healthlm/upload/horizontalreport/content/201508/59.html","状态结论":"http://eky3h.com/healthlm/upload/horizontalreport/content/201508/57.html","titles":["状态结论","膳食调理","运动调理","乐药调理","穴位调理"],"乐药调理":"http://eky3h.com/healthlm/upload/horizontalreport/content/201508/21.html","膳食调理":"http://eky3h.com/healthlm/upload/horizontalreport/content/201508/58.html"}
         * subject : {"id":21,"createDate":1436754225000,"modifyDate":1513045024000,"order":null,"name":"上商","picture":null,"subject_sn":"JLBS-S1","introduction":null,"memo":"胸闷、气短、乏力，活动时喜欢出汗","subjectAttributes":[]}
         * subjectCategorySn : JLBS
         * isRead : false
         * createTime : 2018-05-24
         * phoneReport : {"1":"http://eky3h.com/healthlm/upload/report/content/201508/57.html","2":"http://eky3h.com/healthlm/subject_report/viewreports.jhtml?subjectsn=JLBS-S1"}
         * parentName : null
         */

        private int id;
        private long createDate;
        private long modifyDate;
        private String reportJson;
        private SubjectBeanXXXX subject;
        private String subjectCategorySn;
        private boolean isRead;
        private String createTime;
        private PhoneReportBeanXXX phoneReport;
        private Object parentName;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public long getCreateDate() {
            return createDate;
        }

        public void setCreateDate(long createDate) {
            this.createDate = createDate;
        }

        public long getModifyDate() {
            return modifyDate;
        }

        public void setModifyDate(long modifyDate) {
            this.modifyDate = modifyDate;
        }

        public String getReportJson() {
            return reportJson;
        }

        public void setReportJson(String reportJson) {
            this.reportJson = reportJson;
        }

        public SubjectBeanXXXX getSubject() {
            return subject;
        }

        public void setSubject(SubjectBeanXXXX subject) {
            this.subject = subject;
        }

        public String getSubjectCategorySn() {
            return subjectCategorySn;
        }

        public void setSubjectCategorySn(String subjectCategorySn) {
            this.subjectCategorySn = subjectCategorySn;
        }

        public boolean isIsRead() {
            return isRead;
        }

        public void setIsRead(boolean isRead) {
            this.isRead = isRead;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public PhoneReportBeanXXX getPhoneReport() {
            return phoneReport;
        }

        public void setPhoneReport(PhoneReportBeanXXX phoneReport) {
            this.phoneReport = phoneReport;
        }

        public Object getParentName() {
            return parentName;
        }

        public void setParentName(Object parentName) {
            this.parentName = parentName;
        }

        public static class SubjectBeanXXXX {
            /**
             * id : 21
             * createDate : 1436754225000
             * modifyDate : 1513045024000
             * order : null
             * name : 上商
             * picture : null
             * subject_sn : JLBS-S1
             * introduction : null
             * memo : 胸闷、气短、乏力，活动时喜欢出汗
             * subjectAttributes : []
             */

            private int id;
            private long createDate;
            private long modifyDate;
            private Object order;
            private String name;
            private Object picture;
            private String subject_sn;
            private Object introduction;
            private String memo;
            private List<?> subjectAttributes;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public long getCreateDate() {
                return createDate;
            }

            public void setCreateDate(long createDate) {
                this.createDate = createDate;
            }

            public long getModifyDate() {
                return modifyDate;
            }

            public void setModifyDate(long modifyDate) {
                this.modifyDate = modifyDate;
            }

            public Object getOrder() {
                return order;
            }

            public void setOrder(Object order) {
                this.order = order;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public Object getPicture() {
                return picture;
            }

            public void setPicture(Object picture) {
                this.picture = picture;
            }

            public String getSubject_sn() {
                return subject_sn;
            }

            public void setSubject_sn(String subject_sn) {
                this.subject_sn = subject_sn;
            }

            public Object getIntroduction() {
                return introduction;
            }

            public void setIntroduction(Object introduction) {
                this.introduction = introduction;
            }

            public String getMemo() {
                return memo;
            }

            public void setMemo(String memo) {
                this.memo = memo;
            }

            public List<?> getSubjectAttributes() {
                return subjectAttributes;
            }

            public void setSubjectAttributes(List<?> subjectAttributes) {
                this.subjectAttributes = subjectAttributes;
            }
        }

        public static class PhoneReportBeanXXX {
            /**
             * 1 : http://eky3h.com/healthlm/upload/report/content/201508/57.html
             * 2 : http://eky3h.com/healthlm/subject_report/viewreports.jhtml?subjectsn=JLBS-S1
             */

            @SerializedName("1")
            private String _$1;
            @SerializedName("2")
            private String _$2;

            public String get_$1() {
                return _$1;
            }

            public void set_$1(String _$1) {
                this._$1 = _$1;
            }

            public String get_$2() {
                return _$2;
            }

            public void set_$2(String _$2) {
                this._$2 = _$2;
            }
        }
    }

    public static class TZBSBean {
        /**
         * id : 7161
         * createDate : 1545123212000
         * modifyDate : 1545123212000
         * reportJson : {"和畅方":"http://eky3h.com/healthlm/upload/horizontalreport/content/201508/145.html","和畅体检报告":"http://eky3h.com/healthlm/upload/horizontalreport/content/201508/144.html","titles":["和畅体检报告","和畅方"]}
         * subject : {"id":11,"createDate":1436174835000,"modifyDate":1512979517000,"order":null,"name":"气郁质","picture":null,"subject_sn":"TZBS-05","introduction":null,"memo":"瘦者居多，多烦闷不乐。常出现胸胁、乳房胀痛，喜叹息，有的咽喉部有异物感。食欲不佳，睡眠较差","subjectAttributes":[]}
         * subjectCategorySn : TZBS
         * isRead : false
         * createTime : 2018-12-18
         * phoneReport : {"1":"http://eky3h.com/healthlm/upload/report/content/201508/144.html","2":"http://eky3h.com/healthlm/subject_report/viewreports.jhtml?subjectsn=TZBS-05"}
         * parentName : null
         */

        private int id;
        private long createDate;
        private long modifyDate;
        private String reportJson;
        private SubjectBeanXXXXX subject;
        private String subjectCategorySn;
        private String cert_name;
        private boolean isRead;
        private String createTime;
        private PhoneReportBeanXXXX phoneReport;
        private Object parentName;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public long getCreateDate() {
            return createDate;
        }

        public void setCreateDate(long createDate) {
            this.createDate = createDate;
        }

        public long getModifyDate() {
            return modifyDate;
        }

        public void setModifyDate(long modifyDate) {
            this.modifyDate = modifyDate;
        }

        public String getCert_name() {
            return cert_name;
        }

        public void setCert_name(String cert_name) {
            this.cert_name = cert_name;
        }

        public String getReportJson() {
            return reportJson;
        }

        public void setReportJson(String reportJson) {
            this.reportJson = reportJson;
        }

        public SubjectBeanXXXXX getSubject() {
            return subject;
        }

        public void setSubject(SubjectBeanXXXXX subject) {
            this.subject = subject;
        }

        public String getSubjectCategorySn() {
            return subjectCategorySn;
        }

        public void setSubjectCategorySn(String subjectCategorySn) {
            this.subjectCategorySn = subjectCategorySn;
        }

        public boolean isIsRead() {
            return isRead;
        }

        public void setIsRead(boolean isRead) {
            this.isRead = isRead;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public PhoneReportBeanXXXX getPhoneReport() {
            return phoneReport;
        }

        public void setPhoneReport(PhoneReportBeanXXXX phoneReport) {
            this.phoneReport = phoneReport;
        }

        public Object getParentName() {
            return parentName;
        }

        public void setParentName(Object parentName) {
            this.parentName = parentName;
        }

        public static class SubjectBeanXXXXX {
            /**
             * id : 11
             * createDate : 1436174835000
             * modifyDate : 1512979517000
             * order : null
             * name : 气郁质
             * picture : null
             * subject_sn : TZBS-05
             * introduction : null
             * memo : 瘦者居多，多烦闷不乐。常出现胸胁、乳房胀痛，喜叹息，有的咽喉部有异物感。食欲不佳，睡眠较差
             * subjectAttributes : []
             */

            private int id;
            private long createDate;
            private long modifyDate;
            private Object order;
            private String name;
            private Object picture;
            private String subject_sn;
            private Object introduction;
            private String memo;
            private List<?> subjectAttributes;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public long getCreateDate() {
                return createDate;
            }

            public void setCreateDate(long createDate) {
                this.createDate = createDate;
            }

            public long getModifyDate() {
                return modifyDate;
            }

            public void setModifyDate(long modifyDate) {
                this.modifyDate = modifyDate;
            }

            public Object getOrder() {
                return order;
            }

            public void setOrder(Object order) {
                this.order = order;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public Object getPicture() {
                return picture;
            }

            public void setPicture(Object picture) {
                this.picture = picture;
            }

            public String getSubject_sn() {
                return subject_sn;
            }

            public void setSubject_sn(String subject_sn) {
                this.subject_sn = subject_sn;
            }

            public Object getIntroduction() {
                return introduction;
            }

            public void setIntroduction(Object introduction) {
                this.introduction = introduction;
            }

            public String getMemo() {
                return memo;
            }

            public void setMemo(String memo) {
                this.memo = memo;
            }

            public List<?> getSubjectAttributes() {
                return subjectAttributes;
            }

            public void setSubjectAttributes(List<?> subjectAttributes) {
                this.subjectAttributes = subjectAttributes;
            }
        }

        public static class PhoneReportBeanXXXX {
            /**
             * 1 : http://eky3h.com/healthlm/upload/report/content/201508/144.html
             * 2 : http://eky3h.com/healthlm/subject_report/viewreports.jhtml?subjectsn=TZBS-05
             */

            @SerializedName("1")
            private String _$1;
            @SerializedName("2")
            private String _$2;

            public String get_$1() {
                return _$1;
            }

            public void set_$1(String _$1) {
                this._$1 = _$1;
            }

            public String get_$2() {
                return _$2;
            }

            public void set_$2(String _$2) {
                this._$2 = _$2;
            }
        }
    }

    public static class ZFBSBean {
        /**
         * id : null
         * createDate : 1527749727000
         * modifyDate : null
         * zhengzhuang_id : null
         * zz_name : null
         * zz_level : 0
         * icd_id : null
         * icd_code : null
         * cust_id : 32
         * physique_id : 180531145527042900
         * icd_name : null
         * icd_name_str :
         * zz_name_str : 出血浅淡,皮肤瘙痒,
         * createTime : 2018-05-31
         */

        private Object id;
        private long createDate;
        private Object modifyDate;
        private Object zhengzhuang_id;
        private Object zz_name;
        private String name;
        private int zz_level;
        private Object icd_id;
        private Object icd_code;
        private int cust_id;
        private String physique_id;
        private Object icd_name;
        private String icd_name_str;
        private String zz_name_str;
        private String createTime;

        private String cert_name;

        public String getCert_name() {
            return cert_name;
        }

        public void setCert_name(String cert_name) {
            this.cert_name = cert_name;
        }

        public Object getId() {
            return id;
        }

        public void setId(Object id) {
            this.id = id;
        }

        public long getCreateDate() {
            return createDate;
        }

        public void setCreateDate(long createDate) {
            this.createDate = createDate;
        }

        public Object getModifyDate() {
            return modifyDate;
        }

        public void setModifyDate(Object modifyDate) {
            this.modifyDate = modifyDate;
        }

        public Object getZhengzhuang_id() {
            return zhengzhuang_id;
        }

        public void setZhengzhuang_id(Object zhengzhuang_id) {
            this.zhengzhuang_id = zhengzhuang_id;
        }

        public Object getZz_name() {
            return zz_name;
        }

        public void setZz_name(Object zz_name) {
            this.zz_name = zz_name;
        }

        public int getZz_level() {
            return zz_level;
        }

        public void setZz_level(int zz_level) {
            this.zz_level = zz_level;
        }

        public Object getIcd_id() {
            return icd_id;
        }

        public void setIcd_id(Object icd_id) {
            this.icd_id = icd_id;
        }

        public Object getIcd_code() {
            return icd_code;
        }

        public void setIcd_code(Object icd_code) {
            this.icd_code = icd_code;
        }

        public int getCust_id() {
            return cust_id;
        }

        public void setCust_id(int cust_id) {
            this.cust_id = cust_id;
        }

        public String getPhysique_id() {
            return physique_id;
        }

        public void setPhysique_id(String physique_id) {
            this.physique_id = physique_id;
        }

        public Object getIcd_name() {
            return icd_name;
        }

        public void setIcd_name(Object icd_name) {
            this.icd_name = icd_name;
        }

        public String getIcd_name_str() {
            return icd_name_str;
        }

        public void setIcd_name_str(String icd_name_str) {
            this.icd_name_str = icd_name_str;
        }

        public String getZz_name_str() {
            return zz_name_str;
        }

        public void setZz_name_str(String zz_name_str) {
            this.zz_name_str = zz_name_str;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public static class BodyTemperatureBean {
        /**
         * id : 296
         * createDate : 1511850474000
         * modifyDate : 1511850474000
         * temperature : 36.9
         * isOk : true
         */

        private int id;
        private long createDate;
        private long modifyDate;
        private double temperature;
        private boolean isOk;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public long getCreateDate() {
            return createDate;
        }

        public void setCreateDate(long createDate) {
            this.createDate = createDate;
        }

        public long getModifyDate() {
            return modifyDate;
        }

        public void setModifyDate(long modifyDate) {
            this.modifyDate = modifyDate;
        }

        public double getTemperature() {
            return temperature;
        }

        public void setTemperature(double temperature) {
            this.temperature = temperature;
        }

        public boolean isIsOk() {
            return isOk;
        }

        public void setIsOk(boolean isOk) {
            this.isOk = isOk;
        }
    }
}
