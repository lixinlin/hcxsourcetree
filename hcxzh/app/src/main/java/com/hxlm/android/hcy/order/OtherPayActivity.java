package com.hxlm.android.hcy.order;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.alipay.sdk.app.PayTask;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.*;
import com.hxlm.hcyandroid.alipay.PayResult;
import com.hxlm.hcyandroid.alipay.SignUtils;
import com.hxlm.hcyandroid.bean.Payment;
import com.hxlm.hcyandroid.bean.PaymentAndOrder;
import com.hxlm.hcyandroid.util.BroadcastUtil;
import com.hxlm.hcyandroid.util.PaymentPluginId;
import com.hxlm.hcyandroid.util.SharedPreferenceUtil;
import com.hxlm.hcyandroid.util.ToastUtil;
import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import net.sourceforge.simcpux.Constants;
import net.sourceforge.simcpux.MD5;
import net.sourceforge.simcpux.Util;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.*;

public class OtherPayActivity extends BaseActivity implements OnClickListener {

    // 支付宝支付↓↓↓
    // 商户PID
    private static final String PARTNER = "2088701095950295";
    // 商户收款账号
    private static final String SELLER = "mall@ky3h.com";
    // 商户私钥，pkcs8格式
    private static final String RSA_PRIVATE = "MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAKxSA4CfAAGrROcZ2h2BX/o17pARjDshInt4IsBSFdgkst9TYU4pxkFf8xU6bbawuD4YDNSoyDPTYKQYjk7xDRsd+vltrSukL6MKWa0qL1NVDLxM/nxtmSQb9hnWPa+J5hkgIBhgbBklJA9yxF06IyqX/AwrVaKXwxYb2cAe4qgnAgMBAAECgYA4wIoGwlbuNcnrksgTD5jcfwaizCSzFKaWo2pOjLpFBVEj3AIgDAIPZdpZE3UuxeToDmOrZisJZoRdhvjXZuw5h7xpgB75zgkAefeKvFv/xqGEJRLUYXlzL2cRCZCbuCoxFE3uxSke5y5OkjxUugXsCrh5OhV3redfjve1HO/mgQJBANOJkpcQ9SNW+2XVlIncuwPX3B9OFk9izevo0hYVhvDb3By4l3wVvW+X0EpYGI7QC0u50dPImwPs2H7d0PXZUYsCQQDQij83rAkvBaXmRWqPiwCo3XQe/njargbCPjxbXo9ZZZkR1qwfc7lzuw097WtOly2F5TGVDUsYf1skZNQoaF9VAkEAqc1x1T+wdzi4PotOW8fKexBISvzEnd7jCy5tjXqkQi1KJ+fo+Zr94FkNws+qjuDjYFr92rHZ9TGmq1flB6P1dwJBAMaDc55ZY/yhEcXcOo4eMiiNdqaycvUoSELL38TShP8Cme3DPuJJ1TX3z6ktwsJzYuBcxxAuMVhGk8pXOSamBGkCQQCrbpSJ1cpTiNCq8RZuZ69lcQQUFF18Uz5KyXaGRikJXvLr6U26rMoTaAWsr7ONXGnRr0LAHVL/2bPnnB5tea41";
    // 支付宝公钥
    public static final String RSA_PUBLIC = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCsUgOAnwABq0TnGdodgV/6Ne6QEYw7ISJ7eCLAUhXYJLLfU2FOKcZBX/MVOm22sLg+GAzUqMgz02CkGI5O8Q0bHfr5ba0rpC+jClmtKi9TVQy8TP58bZkkG/YZ1j2vieYZICAYYGwZJSQPcsRdOiMql/wMK1Wil8MWG9nAHuKoJwIDAQAB";
    private static final int ZFB_TO_PAY = 1;
    private static final int WY_TO_PAY = 2;
    private static final int WX_TO_PAY = 3;
    private static final int SDK_PAY_FLAG = 1;
    private static final int SDK_CHECK_FLAG = 2;
    public static Activity activity;
    private static int NO_WAY_CHOOSED = 0;
    private ImageView iv_ZFB_check;
    private ImageView iv_WY_check;
    private ImageView iv_WX_check;
    private TextView tv_pay;

    private PayManager payManager;
    private String orderSn;// 健康商城支付使用的订单sn
    private int wayToPay;
    private int reserveType;
    private int orderId;
    private String type;
    private String cardFees;
    private double balance;
    private String sn;
    private String title;// 商品标题
    private String desc;// 商品描述
    private Handler mHandler = new Handler(new Handler.Callback() {
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case SDK_PAY_FLAG: {
                    PayResult payResult = new PayResult((String) msg.obj);

                    // 支付宝返回此次支付结果及加签，建议对支付宝签名信息拿签约时支付宝提供的公钥做验签
                    String resultInfo = payResult.getResult();
                    String resultStatus = payResult.getResultStatus();

                    if (Constant.ISSHOW)
                        Log.i("OtherPayActivity", "resultStatus = " + resultStatus);

                    // 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
                    if (TextUtils.equals(resultStatus, "9000")) {
                        Toast.makeText(OtherPayActivity.this, getString(R.string.other_pay_tips_success), Toast.LENGTH_SHORT).show();
                        Constant.LIST.clear();
                        Intent i = new Intent(OtherPayActivity.this, PaySuccessActivity.class);

                        startActivity(i);
                    } else {
                        // 判断resultStatus 为非“9000”则代表可能支付失败
                        // “8000”代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
                        if (TextUtils.equals(resultStatus, "8000")) {
                            Toast.makeText(OtherPayActivity.this, getString(R.string.other_pay_tips_confirm), Toast.LENGTH_SHORT).show();

                        } else {
                            // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
                            Toast.makeText(OtherPayActivity.this, getString(R.string.other_pay_tips_failure), Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(OtherPayActivity.this, PayFailureActivity.class);

                            startActivity(i);
                        }
                    }
                    break;
                }
                default:
                    break;
            }
            return false;
        }
    });

    // 微信支付↓↓↓
    private StringBuffer sb;
    private IWXAPI msgApi;
    private PayReq req;
    private Map<String, String> resultunifiedorder;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_other_pay);
        activity = OtherPayActivity.this;

        reserveType = getIntent().getIntExtra("reserveType", -1);
        switch (reserveType) {
            case ReserveType.TYPE_LECTURE:
                desc = getIntent().getStringExtra("desc");
                title = getString(R.string.myfrag_jiankangjiangzuo);

                //健康讲座第三方支付
                SharedPreferenceUtil.saveString("healthlecturenum", "3");

                break;
            case ReserveType.TYPE_REGISTRATION:
                desc = getIntent().getStringExtra("desc");
                title = getString(R.string.other_pay_yuyueguahao);

                break;

            case ReserveType.TYPE_BODY_DIAGNOSE:
                desc = getIntent().getStringExtra("desc");
                title = getString(R.string.other_pay_yuyueguahao);
                break;
            case ReserveType.TYPE_VIDEO:
                desc =getIntent().getStringExtra("desc");
                title = getString(R.string.other_pay_yuyueshipin);
                break;
            case ReserveType.TYPE_CARD:
                break;
            case ReserveType.TYPE_STATIC_RESOURCE:
                desc = getIntent().getStringExtra("desc");
                title = getString(R.string.other_pay_leyao);
                break;
            case ReserveType.TYPE_MALL:
                title = getIntent().getStringExtra("title");
                desc = getIntent().getStringExtra("desc");
                break;
            default:
                break;
        }
        if (reserveType != ReserveType.TYPE_MALL && reserveType != -1) {
            orderId = getIntent().getIntExtra("orderId",0);
            type = getIntent().getStringExtra("type");
            cardFees = getIntent().getStringExtra("cardFees");
            balance = getIntent().getDoubleExtra("balance", -2);
        } else if (reserveType == ReserveType.TYPE_MALL) {
            orderSn = getIntent().getStringExtra("paymentSn");
            balance = getIntent().getDoubleExtra("money", -2);
        }
    }

    //生成收款单之后的操作
    private void pay() {
        switch (wayToPay) {
            case ZFB_TO_PAY:
                ZFB_pay();
                break;
            case WX_TO_PAY:
                msgApi = WXAPIFactory.createWXAPI(
                        OtherPayActivity.this, null);
                // 将应用通过APP_ID注册到微信
                msgApi.registerApp(Constants.APP_ID);
                req = new PayReq();
                getPrepay_id();
                break;
            case WY_TO_PAY:
                // 银联在线支付
                break;
            default:
                break;
        }
    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.other_pay_title), titleBar, 1);
        RelativeLayout rl_ZFB_pay = (RelativeLayout) findViewById(R.id.rl_ZFB_pay);
        RelativeLayout rl_WY_pay = (RelativeLayout) findViewById(R.id.rl_WY_pay);
        RelativeLayout rl_WX_pay = (RelativeLayout) findViewById(R.id.rl_WX_pay);

        iv_ZFB_check = (ImageView) findViewById(R.id.iv_ZFB_check);
        iv_WY_check = (ImageView) findViewById(R.id.iv_WY_check);
        iv_WX_check = (ImageView) findViewById(R.id.iv_WX_check);

        DecimalFormat decimalFormat = new DecimalFormat("#.##");

        TextView tv_moey_to_pay_down = (TextView) findViewById(R.id.tv_total_to_pay);
        tv_moey_to_pay_down.setText(String.format("消费金额：   ¥%s", decimalFormat.format(balance)));
        tv_pay = (TextView) findViewById(R.id.tv_pay);

        rl_ZFB_pay.setOnClickListener(this);
        rl_WY_pay.setOnClickListener(this);
        rl_WX_pay.setOnClickListener(this);
        tv_pay.setOnClickListener(this);

    }

    @Override
    public void initDatas() {
        payManager = new PayManager();
        wayToPay = NO_WAY_CHOOSED;
    }

    private void setImageViewsToDefult() {
        iv_ZFB_check.setImageResource(R.drawable.card_not_checked);
        iv_WY_check.setImageResource(R.drawable.card_not_checked);
        iv_WX_check.setImageResource(R.drawable.card_not_checked);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_ZFB_pay:
                setImageViewsToDefult();
                wayToPay = ZFB_TO_PAY;
                iv_ZFB_check.setImageResource(R.drawable.card_checked);
                tv_pay.setBackgroundResource(R.drawable.reservation_pay_bg);
                break;
            case R.id.rl_WY_pay:
                setImageViewsToDefult();
                wayToPay = WY_TO_PAY;
                iv_WY_check.setImageResource(R.drawable.card_checked);
                tv_pay.setBackgroundResource(R.drawable.reservation_pay_bg);
                break;
            case R.id.rl_WX_pay:
                setImageViewsToDefult();
                wayToPay = WX_TO_PAY;
                iv_WX_check.setImageResource(R.drawable.card_checked);
                tv_pay.setBackgroundResource(R.drawable.reservation_pay_bg);
                break;
            case R.id.tv_pay:

                if (wayToPay != NO_WAY_CHOOSED) {
                    if (reserveType != ReserveType.TYPE_MALL && reserveType != -1) {
                        // 体质门诊的判断（如果是体质门诊，那么参数应该传ReserveType.TYPE_REGISTRATION）
                        int theReserveType = reserveType;
                        if (reserveType == ReserveType.TYPE_BODY_DIAGNOSE) {
                            theReserveType = ReserveType.TYPE_REGISTRATION;
                        }

                        String payPortal = PaymentPluginId.PaymentPluginId_ZFB;
                        if (wayToPay == WX_TO_PAY) {
                            payPortal = PaymentPluginId.PaymentPluginId_WX;
                            msgApi = WXAPIFactory.createWXAPI(OtherPayActivity.this, null);
                        }

                        if (wayToPay == WX_TO_PAY && !isWXAppInstalledAndSupported(msgApi)) {
                            Toast.makeText(this, getString(R.string.other_pay_tips_not_installed_wechat), Toast.LENGTH_SHORT).show();
                        } else {
                            payManager.createOrder(theReserveType, orderId, type, payPortal, cardFees, balance, false,
                                    null, null, new AbstractDefaultHttpHandlerCallback(this) {
                                        @Override
                                        protected void onResponseSuccess(Object obj) {
                                            PaymentAndOrder payment = (PaymentAndOrder) obj;
                                            sn = payment.getPayment().getSn();
                                            pay();
                                        }
                                    });
                        }
                    } else if (reserveType == ReserveType.TYPE_MALL) {
                        String pluginName = null;
                        switch (wayToPay) {
                            case ZFB_TO_PAY:
                                pluginName = "alipayNativeAppPlugin";
                                break;
                            case WX_TO_PAY:
                                msgApi = WXAPIFactory.createWXAPI(OtherPayActivity.this, null);

                                if (isWXAppInstalledAndSupported(msgApi)) {
                                    pluginName = PaymentPluginId.PaymentPluginId_WX;
                                } else {
                                    ToastUtil.invokeShortTimeToast(
                                            OtherPayActivity.this, getString(R.string.other_pay_tips_not_installed_wechat));
                                }
                                break;
                            case WY_TO_PAY:
                                // 银联在线支付
                                break;
                        }

                        if (!TextUtils.isEmpty(pluginName)) {
                            payManager.createPayment(orderSn, pluginName, new AbstractDefaultHttpHandlerCallback(this) {
                                @Override
                                protected void onResponseSuccess(Object obj) {
                                    Payment payment = (Payment) obj;
                                    sn = payment.getSn();
                                    pay();
                                }
                            });
                        }
                    }

                    //如果支付类型不是预约挂号
                    if (reserveType != ReserveType.TYPE_REGISTRATION) {
                        BroadcastUtil.sendTypeToPayResult(this, reserveType);
                    }
                }
                break;
            default:
                break;
        }
    }

    // 微信支付↓↓↓
    private void getPrepay_id() {
        new getPrePayIdTask().execute(Constants.PREPAY_URL);

    }

    // 随机一个五位整数并加密
    private String genNonceStr() {
        Random random = new Random();
        return MD5.getMessageDigest(String.valueOf(random.nextInt(10000))
                .getBytes());
    }

    // 用于随机生成订单
    private String genOutTradNo() {
        return sn;
    }

    // 获取当前时间
    private long genTimeStamp() {
        return System.currentTimeMillis() / 1000;
    }

    // 将参数转化成字符串添加Apikey并进行加密
    private String genAppSign(List<NameValuePair> params) {
        StringBuilder sb = new StringBuilder();

        for (NameValuePair param : params) {
            sb.append(param.getName());
            sb.append('=');
            sb.append(param.getValue());
            sb.append('&');
        }
        sb.append("key=");
        sb.append(Constants.API_KEY);

        this.sb.append("sign str\n").append(sb.toString()).append("\n\n");
        String appSign = MD5.getMessageDigest(sb.toString().getBytes())                .toUpperCase();
        if (Constant.ISSHOW)
            Log.e("orion", appSign);
        return appSign;
    }

    // 提交微信支付请求
    private void sendPayReq() {
        // 将提交所需的参数赋值
        msgApi.registerApp(Constants.APP_ID);
        req.appId = Constants.APP_ID;
        req.partnerId = Constants.MCH_ID;
        req.prepayId = resultunifiedorder.get("prepay_id");
        req.packageValue = "Sign=WXPay";
        req.nonceStr = genNonceStr();
        req.timeStamp = String.valueOf(genTimeStamp());

        List<NameValuePair> signParams = new LinkedList<>();
        signParams.add(new BasicNameValuePair("appid", req.appId));
        signParams.add(new BasicNameValuePair("noncestr", req.nonceStr));
        signParams.add(new BasicNameValuePair("package", req.packageValue));
        signParams.add(new BasicNameValuePair("partnerid", req.partnerId));
        signParams.add(new BasicNameValuePair("prepayid", req.prepayId));
        signParams.add(new BasicNameValuePair("timestamp", req.timeStamp));
        // 生成签名
        req.sign = genAppSign(signParams);
        msgApi.sendReq(req);
    }

    // 支付宝支付↓↓↓
    private void ZFB_pay() {
        // 订单
        String orderInfo = getOrderInfo(title, desc, balance);
        // 对订单做RSA 签名
        String sign = sign(orderInfo);
        try {
            // 仅需对sign 做URL编码
            sign = URLEncoder.encode(sign, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        // 完整的符合支付宝参数规范的订单信息
        final String payInfo = orderInfo + "&sign=\"" + sign + "\"&"
                + getSignType();
        Runnable payRunnable = new Runnable() {
            @Override
            public void run() {
                // 构造PayTask 对象
                PayTask alipay = new PayTask(OtherPayActivity.this);
                // 调用支付接口，获取支付结果
                String result = alipay.pay(payInfo, true);

                mHandler.obtainMessage(SDK_PAY_FLAG, result).sendToTarget();
            }
        };

        // 必须异步调用
        Thread payThread = new Thread(payRunnable);
        payThread.start();
    }

    private String getOrderInfo(String subject, String body, double price) {
        // 签约合作者身份ID
        String orderInfo = "partner=" + "\"" + PARTNER + "\"";

        // 签约卖家支付宝账号
        orderInfo += "&seller_id=" + "\"" + SELLER + "\"";

        // 商户网站唯一订单号
        orderInfo += "&out_trade_no=" + "\"" + getOutTradeNo() + "\"";

        // 商品名称
        orderInfo += "&subject=" + "\"" + subject + "\"";

        // 商品详情
        orderInfo += "&body=" + "\"" + body + "\"";

        // 商品金额
        orderInfo += "&total_fee=" + "\"" + Constant.decimalFormat.format(price) + "\"";

        if(Constant.DEBUG)
        {
            Log.i("Log.i", "支付宝支付total_fee = "+Constant.decimalFormat.format(price));
        }

        // 服务器异步通知页面路径
        orderInfo += "&notify_url=" + "\"" + Constant.BASE_URL
                + "/payment/notify/async/" + sn + ".jhtml" + "\"";

        // 服务接口名称， 固定值
        orderInfo += "&service=\"mobile.securitypay.pay\"";

        // 支付类型， 固定值
        orderInfo += "&payment_type=\"1\"";

        // 参数编码， 固定值
        orderInfo += "&_input_charset=\"utf-8\"";

        // 设置未付款交易的超时时间
        // 默认30分钟，一旦超时，该笔交易就会自动被关闭。
        // 取值范围：1m～15d。
        // m-分钟，h-小时，d-天，1c-当天（无论交易何时创建，都在0点关闭）。
        // 该参数数值不接受小数点，如1.5h，可转换为90m。
        orderInfo += "&it_b_pay=\"30m\"";

        // extern_token为经过快登授权获取到的alipay_open_id,带上此参数用户将使用授权的账户进行支付
        // orderInfo += "&extern_token=" + "\"" + extern_token + "\"";

        // 支付宝处理完请求后，当前页面跳转到商户指定页面的路径，可空
        orderInfo += "&return_url=\"m.alipay.com\"";

        // 调用银行卡支付，需配置此参数，参与签名， 固定值 （需要签约《无线银行卡快捷支付》才能使用）
        // orderInfo += "&paymethod=\"expressGateway\"";
        if (Constant.DEBUG)
            Log.i("Log.i", "orderInfo = " + orderInfo);
        return orderInfo;
    }

    private String getOutTradeNo() {
        return sn;
    }

    /**
     * sign the order info. 对订单信息进行签名
     *
     * @param content 待签名订单信息
     */
    private String sign(String content) {
        return SignUtils.sign(content, RSA_PRIVATE);
    }

    /**
     * get the sign type we use. 获取签名方式
     */
    private String getSignType() {
        return "sign_type=\"RSA\"";
    }

    public Handler getHandler() {
        return mHandler;
    }

    public int getReserveType() {
        return reserveType;
    }

    // 判断微信是否已经安装
    private boolean isWXAppInstalledAndSupported(IWXAPI api) {
        return api.isWXAppInstalled() && api.isWXAppSupportAPI();
    }

    private class getPrePayIdTask extends AsyncTask<String, Void, Map<String, String>> {
        private ProgressDialog progressDialog;

        @Override
        protected Map<String, String> doInBackground(String... params) {
            // 获取预支付订单的URL
            String url = params[0];
            String entity = genProductArgs();
            if (Constant.DEBUG)
                Log.e("orion", entity);
            // 提交网络请求返回byte数组
            byte[] buf = Util.httpPost(url, entity);
            // 得到一个XML字符串 字符串里面是服务端返回的相关参数
            String content = new String(buf != null ? buf : new byte[0]);
            if (Constant.DEBUG)
                Log.e("orion", content);
            // 解析XML 得到返回结果并放入集合

            return decodeXml(content);
        }

        @Override
        protected void onPostExecute(Map<String, String> result) {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            sb = new StringBuffer();
            resultunifiedorder = result;
            sb.append("prepay_id\n").append(result.get("prepay_id")).append("\n\n");

            sendPayReq();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(OtherPayActivity.this, getString(R.string.other_pay_dialog_title),
                    getString(R.string.other_pay_tips_get_order));
        }

        // 将Post所需参数转化成Xml字符串
        private String genProductArgs() {
            StringBuilder xml = new StringBuilder();
            String nonceStr = genNonceStr();
            xml.append("</xml>");
            List<NameValuePair> packageParams = new LinkedList<>();

            packageParams.add(new BasicNameValuePair("appid", Constants.APP_ID));
            // 商品描述信息
            packageParams.add(new BasicNameValuePair("body", title + desc));
            // 商品详细信息
            packageParams.add(new BasicNameValuePair("detail", desc));
            // 商户id
            packageParams.add(new BasicNameValuePair("mch_id", Constants.MCH_ID));
            // 生成的随机字符串（少于32位即可）
            packageParams.add(new BasicNameValuePair("nonce_str", nonceStr));
            // 接收微信支付异步通知回调地址
            packageParams.add(new BasicNameValuePair("notify_url", Constant.BASE_URL + "/payment/notify/async/" + sn
                    + ".jhtml"));
            // 商户系统内部的订单号,32个字符内、可包含字母
            packageParams.add(new BasicNameValuePair("out_trade_no", genOutTradNo()));
            // APP和网页支付提交用户端ip，Native支付填调用微信支付API的机器IP。
            packageParams.add(new BasicNameValuePair("spbill_create_ip", "127.0.0.1"));
            // TODO 待支付的金额
            // 订单总金额，只能为整数(这里的单位是分)这时支付的是1分
            packageParams.add(new BasicNameValuePair("total_fee", Math.round(balance * 100) + ""));

            // packageParams.add(new BasicNameValuePair("total_fee", "1"));
            // 交易类型（APP）
            packageParams.add(new BasicNameValuePair("trade_type", "APP"));
            // 通过参数生成签名

            String sign = genPackageSign(packageParams);
            packageParams.add(new BasicNameValuePair("sign", sign));
            String xmlString = toXml(packageParams);
            try {
                // 一定要将字符串转化成ISO8859-1格式，不然商品描述不能写中文（如果不转，写成中文会无法获取PrePayID，返回签名错误）
                return new String(xmlString.getBytes(), "ISO8859-1");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return null;
            }
        }

        // 将参数集合转化成xml的字符串
        private String toXml(List<NameValuePair> params) {
            StringBuilder sb = new StringBuilder();
            sb.append("<xml>");
            for (NameValuePair param : params) {
                sb.append("<").append(param.getName()).append(">");
                sb.append(param.getValue());
                sb.append("</").append(param.getName()).append(">");

            }
            sb.append("</xml>");
            return sb.toString();
        }

        // 生成签名
        private String genPackageSign(List<NameValuePair> params) {
            StringBuffer sb = new StringBuffer();
            for (NameValuePair param : params) {
                sb.append(param.getName());
                sb.append('=');
                sb.append(param.getValue());
                sb.append('&');
            }
            sb.append("key=");
            sb.append(Constants.API_KEY);
            return MD5.getMessageDigest(sb.toString().getBytes()).toUpperCase();
        }

        // 解析XML
        private Map<String, String> decodeXml(String content) {

            try {
                Map<String, String> xml = new HashMap<>();
                XmlPullParser parser = Xml.newPullParser();
                parser.setInput(new StringReader(content));
                int event = parser.getEventType();
                while (event != XmlPullParser.END_DOCUMENT) {
                    String nodeName = parser.getName();
                    switch (event) {
                        case XmlPullParser.START_DOCUMENT:
                            break;
                        case XmlPullParser.START_TAG:
                            if (!"xml".equals(nodeName)) {
                                xml.put(nodeName, parser.nextText());
                            }
                            break;
                        case XmlPullParser.END_TAG:
                            break;

                    }
                    event = parser.next();

                }
                return xml;

            } catch (XmlPullParserException | IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
