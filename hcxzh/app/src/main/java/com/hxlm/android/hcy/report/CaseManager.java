package com.hxlm.android.hcy.report;

import com.alibaba.fastjson.JSON;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.asynchttp.HcyHttpResponseHandler;
import com.hxlm.android.hcy.user.LoginControllor;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lixinlin on 2018/12/14.
 */

public class CaseManager {
    public void getCaseList(int childMemberid,AbstractDefaultHttpHandlerCallback callback) {
        String url = "/md/diseaList.jhtml";
        RequestParams params = new RequestParams();
        params.put("memberId",childMemberid);
        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                List<Case> cases = new ArrayList<>();
                try {
                    cases = JSON.parseArray(content, Case.class);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return cases;
            }
        }, LoginControllor.CHILD_MEMBER, callback.getContext());
    }

}
