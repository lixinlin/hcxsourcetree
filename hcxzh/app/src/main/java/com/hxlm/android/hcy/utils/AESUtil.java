package com.hxlm.android.hcy.utils;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * @Author Will.Su
 * @Description
 * @Date 2018/10/6 0006 22:18
 */
public class AESUtil {
    public static final String ENC_FILE_SUFFIX = ".enc";
    private static final String DEC_FILE_SUFFIX = ".dec";
    private static String ALGORITHM = "AES";
    private static String ALGORITHM_PADDING = "AES/CBC/PKCS5Padding";
    private static final String AES_IV = "e@45*t8bzsUFdg9s";

    /**
     * 加密
     */
    public static String encrypt(String sourceFilePath, String key) {
        String destFilePath = sourceFilePath + ENC_FILE_SUFFIX;
        try {
            byte[] inBytes = getBytes(sourceFilePath);
            byte[] outBytes = encrypt(inBytes, key.getBytes("UTF-8"));
            getFile(outBytes, destFilePath);
            return destFilePath;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 解密
     */
    public static String decrypt(String encodedFilePath, String key) {
        String decodedFilePath = encodedFilePath.substring(0, encodedFilePath.indexOf(ENC_FILE_SUFFIX)) + DEC_FILE_SUFFIX;
        try {
            byte[] inBytes = getBytes(encodedFilePath);
            byte[] outBytes = decrypt(inBytes, key.getBytes("UTF-8"));
            getFile(outBytes, decodedFilePath);
            return decodedFilePath;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 原始加密
     */
    public static byte[] encrypt(byte[] input, byte[] key) {
        try {
            SecretKeySpec skeySpec = new SecretKeySpec(key, ALGORITHM);
            Cipher cipher = Cipher.getInstance(ALGORITHM_PADDING);
            IvParameterSpec iv = new IvParameterSpec(AES_IV.getBytes());
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
            return cipher.doFinal(input);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 原始解密
     */
    public static byte[] decrypt(byte[] input, byte[] key) {
        try {
            SecretKeySpec skeySpec = new SecretKeySpec(key, ALGORITHM);
            Cipher cipher = Cipher.getInstance(ALGORITHM_PADDING);
            IvParameterSpec iv = new IvParameterSpec(AES_IV.getBytes());
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
            return cipher.doFinal(input);
        } catch (Exception e) {
            return null;
        }
    }


    /**
     * 获得指定文件的byte数组
     */
    private static byte[] getBytes(String filePath) {
        byte[] buffer = null;
        try {
            File file = new File(filePath);
            FileInputStream fis = new FileInputStream(file);
            ByteArrayOutputStream bos = new ByteArrayOutputStream(1000);
            byte[] b = new byte[1000];
            int n;
            while ((n = fis.read(b)) != -1) {
                bos.write(b, 0, n);
            }
            fis.close();
            bos.close();
            buffer = bos.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buffer;
    }

    /**
     * 根据byte数组，生成文件
     */
    private static void getFile(byte[] bfile, String filePath) {
        BufferedOutputStream bos = null;
        FileOutputStream fos = null;
        try {
            File file = new File(filePath);
            fos = new FileOutputStream(file);
            bos = new BufferedOutputStream(fos);
            bos.write(bfile);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        AESUtil.encrypt("C:\\Users\\yuchen\\Desktop\\1", "18434009058b9c20e585f7c3b79dab6c");
        AESUtil.decrypt("C:\\Users\\yuchen\\Desktop\\1.enc", "18434009058b9c20e585f7c3b79dab6c");
    }
}