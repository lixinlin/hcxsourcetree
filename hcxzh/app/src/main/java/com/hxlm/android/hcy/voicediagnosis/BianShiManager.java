package com.hxlm.android.hcy.voicediagnosis;

import android.media.MediaRecorder;
import android.os.Build;
import android.os.Handler;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;

import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.report.UploadManager;
import com.hxlm.android.hcy.user.ChildMember;
import com.hxlm.android.hcy.user.ChooseMemberDialog;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.utils.Logger;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.datamanager.BaseManager;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * 提供闻音辨识基本功能 Created by Zhenyu on 2015/8/17.
 */
class BianShiManager extends BaseManager {
    static final int START_RECORD = 0;
    static final int INIT_RECORDER_FAIL = 1;
    static final int NO_VOICE = 2;

    private static final String TAG = "BianShiManager";
    public static boolean mIsRecordUsable;
    private MediaRecorder recorder;
    private boolean isRecording = false;
    private String voiceFilePath;
    private int hasPowerCount = 0; // 采集到声音的次数
    private boolean isUseVoiceAnalysis = false;
    BianShiManager(Handler handler) {
        super(handler);
    }

    synchronized void startVoiceRecording() {
        if (recorder == null) {
            Logger.i(TAG, "为空，去创建");
            setInitRecorderData(); //录音的基本设置
        } else {
            Logger.i(TAG, "不为空，先停止在去创建");
            // 停止录音
            stopVoiceRecord();
            setInitRecorderData(); //录音的基本设置
        }
    }

    //录音的基本设置
    synchronized void setInitRecorderData() {

        Logger.i(TAG, "创建录音");

        hasPowerCount = 0;
//todo  文件名中标识出测试账号的ID
        final String path = Constant.UPLOAD_PATH + "/"
                + "YH" + DateFormat.format("yyyyMMdd_hhmmss", System.currentTimeMillis());

        if (!createPath(path)) {
            mHandler.obtainMessage(INIT_RECORDER_FAIL).sendToTarget();
            return;
        }
        try {
            recorder = new MediaRecorder();// new出MediaRecorder对象

            // 设置MediaRecorder的音频源为麦克风
            recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            recorder.setAudioChannels(1);

            if (Build.VERSION.SDK_INT >= 10) {
                // 设置MediaRecorder录制的音频格式
                recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
                // 设置MediaRecorder录制音频的编码为AAC。
                recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
                // 设置MediaRecorder的音频参数
                recorder.setAudioSamplingRate(44100);
                recorder.setAudioEncodingBitRate(320000);

                voiceFilePath = path + ".m4a";
            } else {
                recorder.setAudioSamplingRate(8000);
                recorder.setAudioEncodingBitRate(12200);
                // older version of Android, use crappy sounding voice codec
                recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

                voiceFilePath = path + ".amr";
                Log.d(TAG, "setInitRecorderData: "+voiceFilePath);

            }

            recorder.setOutputFile(voiceFilePath);

            try {
                recorder.prepare();// 准备录制
            } catch (IOException e) {
                mHandler.obtainMessage(INIT_RECORDER_FAIL).sendToTarget();

                stopVoiceRecord();
                return;
            }
            recorder.start();// 开始录制
            isRecording = true;
            mIsRecordUsable = true;
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("1111",e.toString());
            mIsRecordUsable = false;
        }


        mHandler.obtainMessage(START_RECORD).sendToTarget();

        Runnable rChangeZiImgWorker = new changeZiImgWorker();
        Thread changeZiImgWorker = new Thread(rChangeZiImgWorker);
        changeZiImgWorker.start();
    }

    // 停止录音
    synchronized void stopVoiceRecord() {
        Logger.i(TAG, "删除录音1");
        isRecording = false;
        if (recorder != null) {

            Logger.i(TAG, "删除录音2");

            // recorder.stop();
            // recorder.release();
            //recorder = null;
            try {
                recorder.stop();
            } catch (IllegalStateException e) {
                // TODO 如果当前java状态和jni里面的状态不一致，
                //e.printStackTrace();
                recorder = null;
                recorder = new MediaRecorder();
            }catch (RuntimeException e){

            }
            recorder.release();
            recorder = null;
        }
    }

    void startUploadVoiceFile(final AbstractDefaultHttpHandlerCallback handler) {
        this.startUploadVoiceFile(voiceFilePath, handler);
    }

    //上传录音结果
    void startUploadVoiceFile(final String filePath, final AbstractDefaultHttpHandlerCallback handler) {


        LoginControllor.requestLogin(handler.getContext(), new OnCompleteListener() {
            @Override
            public void onComplete() {
                List<ChildMember> childMembers = LoginControllor.getLoginMember().getMengberchild();
                int size = childMembers.size();
                if (true) {
//                    Toast.makeText(handler.getContext(), handler.getContext().getString(R.string.file_uploading), Toast.LENGTH_LONG).show();
                    //  不及时返回结果的接口      String url = "media&convert=convert&memberChildId=" + LoginControllor.getChoosedChildMember().getId();
                    //及时返回结果的接口
                    String url = "/member/fileUpload/diagnosis.jhtml?convert=convert&memberChildId="
                            + LoginControllor.getChoosedChildMember().getId();
                    UploadManager uploadManager = new UploadManager();
                    uploadManager.uploadWenYinFile(new File(filePath), url, handler);
                } else {
                    new ChooseMemberDialog(handler.getContext(), new OnCompleteListener() {
                        @Override
                        public void onComplete() {
//                            Toast.makeText(handler.getContext(), handler.getContext().getString(R.string.file_uploading), Toast.LENGTH_LONG).show();
                            //  不及时返回结果的接口      String url = "media&convert=convert&memberChildId=" + LoginControllor.getChoosedChildMember().getId();
                            //及时返回结果的接口
                            String url = "/member/fileUpload/diagnosis.jhtml?convert=convert&memberChildId="
                                    + LoginControllor.getChoosedChildMember().getId();
                            UploadManager uploadManager = new UploadManager();
                            uploadManager.uploadWenYinFile(new File(filePath), url, handler);
                        }
                    }).show();
                }
            }
        });

        //每次都弹出选择家庭成员


//        LoginControllor.chooseChildMember(handler.getContext(), new OnCompleteListener() {
//            @Override
//            public void onComplete() {
//                String url = "media&convert=convert&memberChildId=" + LoginControllor.getChoosedChildMember().getId();
//
//                UploadManager uploadManager = new UploadManager();
//                uploadManager.uploadFile(new File(filePath), url, handler);
//            }
//        });
    }

    //删除录音文件
    void clearVoiceFile() {
        if (!TextUtils.isEmpty(voiceFilePath)) {
            final File voiceFile = new File(voiceFilePath);

            if (voiceFile.exists() && !voiceFile.delete()) {
                if (!voiceFile.delete()) {
                    Logger.i(TAG, "Voice File can not be delete. The Path is " + voiceFilePath);
                }
            }
        }
    }

    public boolean isUseVoiceAnalysis() {
        return isUseVoiceAnalysis;
    }

    public void setUseVoiceAnalysis(boolean isUseVoiceAnalysis) {
        this.isUseVoiceAnalysis = isUseVoiceAnalysis;
    }

    boolean isRecordSuccess() {
        return hasPowerCount >= 4;
    }

    private class changeZiImgWorker implements Runnable {
        private static final int RISING_DETECT_TIMES = 8;
        private static final int FALLING_DETECT_TIMES = 2;

        private static final int MAX_RISING_DETECT_TIMES = 20;

        @Override
        public void run() {

            if (recorder != null) {
                double powerCount = 0;
                double powerMap;
                double powerMapPre = 0;

                int risingCount = 0;
                int fallingCount = 0;
                int buildBase = 0;

                while (isRecording) {
                    buildBase++;
                    int maxAmp = recorder.getMaxAmplitude();

                    if (maxAmp > 0) {
                        powerCount += maxAmp;
//                        Logger.d(TAG, "Max Apmlitude = " + maxAmp);

                        powerMap = Math.log10(powerCount / buildBase);

                        if (powerMap > powerMapPre) {
                            risingCount++;

                            if (fallingCount > 0 && fallingCount < FALLING_DETECT_TIMES) {
                                fallingCount = 0;
                            }
                        } else if (powerMap < powerMapPre) {
                            if (risingCount > 0 && risingCount < RISING_DETECT_TIMES) {
                                risingCount = 0;
                            } else if (risingCount >= MAX_RISING_DETECT_TIMES) {
                                risingCount = 0;
                            } else if (risingCount >= RISING_DETECT_TIMES && risingCount < MAX_RISING_DETECT_TIMES) {
                                fallingCount++;
                            }

                            if (risingCount >= RISING_DETECT_TIMES && fallingCount >= FALLING_DETECT_TIMES) {

                                hasPowerCount++;

                                risingCount = 0;
                                fallingCount = 0;
                                powerCount = 0;
                                buildBase = 0;
                            }
                        }
                        Log.d(TAG, "Power Map = " + powerMap + "-----Rising Count = " + risingCount +
                                "------Falling Count = " + fallingCount);
                        powerMapPre = powerMap;
                    }

                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        Logger.e(TAG, e);
                    }
                }
                Log.i(TAG, "Has Power Count = " + hasPowerCount);
            }
        }
    }
}
