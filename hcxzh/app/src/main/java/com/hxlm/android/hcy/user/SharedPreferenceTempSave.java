package com.hxlm.android.hcy.user;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.hxlm.hcyandroid.BaseApplication;

public class SharedPreferenceTempSave {

    private static SharedPreferences sp = BaseApplication.getContext()
            .getSharedPreferences("tempSave", Context.MODE_PRIVATE);

    public static String getString(String string) {
        return sp.getString(string, "");
    }
    public static int getInt(String string) {
        return sp.getInt(string, -1);
    }
    public static Long getLong(String string) {
        return sp.getLong(string,0);
    }
    public static boolean getBoolean(String string) {
        return sp.getBoolean(string,false);
    }

    public static void saveString(String key, String value) {
        Editor editor = sp.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void saveInt(String key, int value) {
        Editor editor = sp.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static void saveLong(String key, Long value) {
        Editor editor = sp.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public static void saveBoolean(String key, boolean value) {
        Editor editor = sp.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }


}
