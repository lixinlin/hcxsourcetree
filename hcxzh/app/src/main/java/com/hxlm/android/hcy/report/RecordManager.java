package com.hxlm.android.hcy.report;

import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.hcy.ky3h.R;
import com.hcy_futejia.bean.FtjRecordIndexData;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.AbstractHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.HcyBinaryResponseHandler;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.asynchttp.HcyHttpResponseHandler;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.health.device.view.ECGDrawWaveManager;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.bean.BloodSugar;
import com.hxlm.hcyandroid.bean.ECGReport;
import com.hxlm.hcyandroid.bean.RecordIndexData;
import com.hxlm.hcyandroid.util.SharedPreferenceUtil;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RecordManager {
    /**
     * 根据sn编码获取web内容报告
     *
     * @param reportJson 根据答案获取的sn编码
     */
    ReportData getReportList(String reportJson) {
        JSONObject jo_reportJson = JSON.parseObject(reportJson);
        List<String> titles = JSON.parseArray(jo_reportJson.getString("titles"), String.class);
        Map<String, String> map = new HashMap<>();

        if (titles != null) {
            for (String title : titles) {
                String key = jo_reportJson.getString(title);
                map.put(title, key);
            }
            return new ReportData(titles, map);
        }
        return null;
    }

    /**
     * @param memberChildId
     * @param tag           获取报告的类型
     * @param callback
     */
    public void getReportListByTag(int memberChildId, String tag, String subjectCategorySn, String pageNo, AbstractDefaultHttpHandlerCallback callback) {
        switch (tag) {
//            所有
            case "all":
                getAllRecord(memberChildId,pageNo,callback);
                break;
//            最新
            case "new":
               getRecordIndex(memberChildId,callback);
                break;
//                经络
            case "meridian":
                getReportBySnByPage(memberChildId,subjectCategorySn,"yes",pageNo,callback);
                break;
//                体质
            case "body":
                getReportBySnByPage(memberChildId,subjectCategorySn,"yes",pageNo,callback);
                break;
//                脏腑
            case "viscera":

                break;
//                心率
            case "rate":
                getECGReportByPage(memberChildId,pageNo,callback);
                break;
//                血压
            case "bloodPressure":
                break;
//                血氧
            case "bloodOxygen":
                break;
//                血糖
            case "bloodSugar":
                break;
//                体温
            case "bodyTemperature":
                break;
//                呼吸
            case "breath":
                break;
//                阶段
            case "quaterly":
                getQuaterlyList(memberChildId,callback);
                break;

        }
    }

    /**
     * 获取季度报告列表
     * @param memberChildId
     * @param callback
     */
    private void getQuaterlyList(int memberChildId, AbstractDefaultHttpHandlerCallback callback) {
        String url = "/member/service/reportslist.jhtml";

        RequestParams params = new RequestParams();
        params.put("memberChildId", memberChildId);

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                return JSON.parseArray(content, QuaterReport.class);
            }
        }, LoginControllor.CHILD_MEMBER, callback.getContext());
    }

    /**
     * 获取数据展示页（H5页面）的地址
     *
     * @param mcId     子账户id
     * @param datatype 数据类型
     * @return 数据展示页的url
     */
    private String version = "2";
    public String getHsitoryReportUrl(String mcId, int datatype) {
        return Constant.BASE_URL + "/subject_report/getreport.jhtml?mcId=" + mcId + "&datatype=" + datatype+"&version=2";
    }

    public String getHsitoryReportUrlNOBaseUrl(String mcId, int datatype) {
        return "/subject_report/getreport.jhtml?mcId=" + mcId + "&datatype=" + datatype+"&version=2";
    }

    /**
     * 知己档案首页接口
     *
     * @param memberChildId 子账户id
     */
    public void getRecordIndex(int memberChildId, AbstractDefaultHttpHandlerCallback callback) {
        String url = "/member/myreport/view/" + memberChildId + ".jhtml?";

        RequestParams params = new RequestParams();
        params.put("memberChildId", memberChildId);
        params.put("teshu", true);

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {

            @Override
            public Object contentParse(String content) {
                if (content != null&&!content.equals("")) {
                    return JSON.parseObject(content, AcricheNewBean.class);
                }
                return null;
            }
        }, LoginControllor.MEMBER, callback.getContext());
    }

    public void oldgetRecordIndex(int memberChildId, AbstractDefaultHttpHandlerCallback callback) {
        String url = "/member/myreport/view/" + memberChildId + ".jhtml?";

        RequestParams params = new RequestParams();
        params.put("memberChildId", memberChildId);
        params.put("teshu",true);

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {

            @Override
            public Object contentParse(String content) {
                List<RecordIndexData> datas = new ArrayList<>();

                JSONObject data = JSON.parseObject(content);
                // 血氧数据
                JSONObject jo_oxygen = data.getJSONObject("oxygen");
                if (jo_oxygen != null) {
                    datas.add(new RecordIndexData(R.drawable.icon_spo2h, "血氧",
                            jo_oxygen.getDouble("density") + "", jo_oxygen.getLong("createDate")));
                    datas.add(new RecordIndexData(R.drawable.icon_spo2h, "Blood Oxygen",
                            jo_oxygen.getDouble("density") + "", jo_oxygen.getLong("createDate")));
                }
                // 血压
                JSONObject jo_bloodPressure = data.getJSONObject("bloodPressure");
                if (jo_bloodPressure != null) {
                    datas.add(new RecordIndexData(
                            R.drawable.icon_blood_pressure, "血压", jo_bloodPressure.getString("lowPressure")
                            + "-" + jo_bloodPressure.getString("highPressure"), jo_bloodPressure.getLong("createDate")));
                    datas.add(new RecordIndexData(
                            R.drawable.icon_blood_pressure, "Blood Pressure", jo_bloodPressure.getString("lowPressure")
                            + "-" + jo_bloodPressure.getString("highPressure"), jo_bloodPressure.getLong("createDate")));
                }
                // 心电数据
                JSONObject jo_ecg = data.getJSONObject("ecg");
                if (jo_ecg != null && jo_ecg.getJSONObject("subject") != null) {
                    datas.add(new RecordIndexData(R.drawable.icon_ecg, "心电",
                            jo_ecg.getJSONObject("subject").getString("name"), jo_ecg.getLong("createDate")));
                    datas.add(new RecordIndexData(R.drawable.icon_ecg, "Heart rate",
                            jo_ecg.getJSONObject("subject").getString("name"), jo_ecg.getLong("createDate")));
                }
                // 体温数据
                JSONObject jo_bodyTemperature = data.getJSONObject("bodyTemperature");
                if (jo_bodyTemperature != null) {
                    datas.add(new RecordIndexData(R.drawable.icon_temperature, "体温",
                            jo_bodyTemperature.getDouble("temperature") + "", jo_bodyTemperature.getLong("createDate")));
                    datas.add(new RecordIndexData(R.drawable.icon_temperature, "Body Temperature",
                            jo_bodyTemperature.getDouble("temperature") + "", jo_bodyTemperature.getLong("createDate")));
                }
                //血糖数据
                JSONObject jo_bloodSugar = data.getJSONObject("bloodSugar");
                if (jo_bloodSugar != null){
                    datas.add(new RecordIndexData(R.drawable.blood_glucose,"血糖",jo_bloodSugar.getDouble("levels")+"",jo_bloodSugar.getLong("createDate")));
                    datas.add(new RecordIndexData(R.drawable.blood_glucose,"Blood Sugar",jo_bloodSugar.getDouble("levels")+"",jo_bloodSugar.getLong("createDate")));
                }
                // 呼吸数据
                JSONObject jo_breathe = data.getJSONObject("breathe");
                if (jo_breathe!= null){
                    datas.add(new RecordIndexData(R.drawable.breathe, "呼吸",
                            jo_breathe.getDouble("nums") + "", jo_breathe.getLong("createDate")));
                    datas.add(new RecordIndexData(R.drawable.breathe, "Respiration",
                            jo_breathe.getDouble("nums") + "", jo_breathe.getLong("createDate")));
                }
                return datas;
            }
        }, LoginControllor.MEMBER, callback.getContext());
    }

    public void oldgetRecordIndexNew(int memberChildId, AbstractDefaultHttpHandlerCallback callback) {
        String url = "/member/myreport/view/" + memberChildId + ".jhtml?";

        RequestParams params = new RequestParams();
        params.put("memberChildId", memberChildId);
        params.put("teshu",true);

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {

            @Override
            public Object contentParse(String content) {

                List<FtjRecordIndexData> datas = new ArrayList<>();

                JSONObject data = JSON.parseObject(content);
                // 血压
                JSONObject jo_bloodPressure = data.getJSONObject("bloodPressure");
                if (jo_bloodPressure != null) {
                    Date inputDate = jo_bloodPressure.getDate("inputDate");
                    datas.add(new FtjRecordIndexData(jo_bloodPressure.getDate("inputDate"),
                            R.drawable.icon_blood_pressure, "血压", jo_bloodPressure.getString("lowPressure")
                            + "-" + jo_bloodPressure.getString("highPressure"), jo_bloodPressure.getLong("createDate"),jo_bloodPressure.getInteger("highPressure"),jo_bloodPressure.getInteger("lowPressure"),jo_bloodPressure.getInteger("pulse"),""));
                    datas.add(new FtjRecordIndexData(jo_bloodPressure.getDate("inputDate"),
                            R.drawable.icon_blood_pressure, "Blood Pressure", jo_bloodPressure.getString("lowPressure")
                            + "-" + jo_bloodPressure.getString("highPressure"), jo_bloodPressure.getLong("createDate"),jo_bloodPressure.getInteger("highPressure"),jo_bloodPressure.getInteger("lowPressure"),jo_bloodPressure.getInteger("pulse"),""));
                }
                //血糖数据
                JSONObject jo_bloodSugar = data.getJSONObject("bloodSugar");
                if (jo_bloodSugar != null) {
                    datas.add(new FtjRecordIndexData(jo_bloodSugar.getDate("inputDate"),R.drawable.blood_glucose, "血糖", jo_bloodSugar.getDouble("levels") + "", jo_bloodSugar.getLong("createDate"),0,0,0,jo_bloodSugar.getString("type")));
                    datas.add(new FtjRecordIndexData(jo_bloodSugar.getDate("inputDate"),R.drawable.blood_glucose, "Blood Sugar", jo_bloodSugar.getDouble("levels") + "", jo_bloodSugar.getLong("createDate"),0,0,0,jo_bloodSugar.getString("type")));
                }
                return datas;
            }
        }, LoginControllor.MEMBER, callback.getContext());
    }

    /**
     * 通过结论编码获取报告
     *
     * @param subjectCategorySn 结论编码
     */
    public void getReportBySn(int memberChildId, String subjectCategorySn, AbstractDefaultHttpHandlerCallback callback) {
        String url = "/member/myreport/list/" + subjectCategorySn + "/" + memberChildId + ".jhtml";
        RequestParams params = new RequestParams();
        params.put("subjectCategorySn", subjectCategorySn);

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                return JSON.parseArray(content, Report.class);
            }
        }, LoginControllor.CHILD_MEMBER, callback.getContext());
    }

    /**
     * 通过结论编码获取报告
     *
     * @param subjectCategorySn 结论编码
     */
    private void getReportBySnByPage(int memberChildId, String subjectCategorySn, String isphone, String pageNo, AbstractDefaultHttpHandlerCallback callback) {
        String url = "/member/myreport/list/" + subjectCategorySn + "/" + memberChildId
                + ".jhtml?isphone=" + isphone + "&pageNumber=" + pageNo;

        RequestParams params = new RequestParams();
        params.put("subjectCategorySn", subjectCategorySn);

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                return JSON.parseArray(content, Report.class);
            }
        }, LoginControllor.CHILD_MEMBER, callback.getContext());
    }


    /**
     * 获取心电报告的接口
     *
     * @param memberChildId 子账号id
     */
    public void getECGReport(int memberChildId, AbstractDefaultHttpHandlerCallback callback) {
        String url = "/member/myreport/getEcgList/" + memberChildId + ".jhtml";

        RequestParams params = new RequestParams();

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                JSONObject jsonObject = JSON.parseObject(content);
                if (jsonObject != null) {
                    return JSON.parseArray(jsonObject.getString("content"), ECGReport.class);
                }
                return null;
            }
        }, LoginControllor.CHILD_MEMBER, callback.getContext());
    }

    /**
     * 获取心电报告的接口
     *
     * @param memberChildId 子账号id
     */
    public void getECGReportByPage(int memberChildId,String pageNumber, AbstractDefaultHttpHandlerCallback callback) {
        String url = "/member/myreport/getEcgList/" + memberChildId + ".jhtml";

        RequestParams params = new RequestParams();
        params.put("pageNumber",pageNumber);

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                JSONObject jsonObject = JSON.parseObject(content);
                if (jsonObject != null) {
                    return JSON.parseArray(jsonObject.getString("content"), ECGReport.class);
                }
                return null;
            }
        }, LoginControllor.CHILD_MEMBER, callback.getContext());
    }

    /**
     * 获取血糖数据报告列表
     */
    public void getBloodSugarReport(AbstractDefaultHttpHandlerCallback callback) {
        String url = "/subject_report/findDate.jhtml";

        String memberChildId = SharedPreferenceUtil.getCurrnetMemberId();
        RequestParams params = new RequestParams();
        params.put("mcId", memberChildId);

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                Map<String, List<BloodSugar>> map = new HashMap<>();
                List<BloodSugar> emptyData = new ArrayList<>();
                List<BloodSugar> fullData = new ArrayList<>();

                JSONObject data = JSON.parseObject(content);

                JSONArray empty = data.getJSONArray("type=empty");
                for (int i = 0; i < empty.size(); i++) {
                    JSONObject item = empty.getJSONObject(i);
                    emptyData.add(new BloodSugar(item.getInteger("id"),
                            item.getLong("createDate"), item
                            .getLong("modifyDate"), item
                            .getString("type"), item
                            .getDouble("levels"), item
                            .getBoolean("isAbnormity")));
                }

                JSONArray full = data.getJSONArray("type=full");
                for (int i = 0; i < full.size(); i++) {
                    JSONObject item = full.getJSONObject(i);
                    fullData.add(new BloodSugar(item.getInteger("id"), item
                            .getLong("createDate"), item
                            .getLong("modifyDate"), item.getString("type"),
                            item.getDouble("levels"), item
                            .getBoolean("isAbnormity")));
                }
                map.put("empty", emptyData);
                map.put("full", fullData);
                return map;
            }
        }, LoginControllor.MEMBER, callback.getContext());

    }


    /**
     * 下载心电文件到本地
     */
    public void downloadECGItemFile(final ECGReport item, AbstractHttpHandlerCallback handler) {
        Log.i("ECGReviewActivity", "下载Path-->" + item.getPath());
        HcyHttpClient.download(item.getPath(), new HcyBinaryResponseHandler(getItemECGLocalPath(item), handler));
    }


    //下载的心电保存路径
    private String getItemECGLocalPath(ECGReport item) {
        int cutPosition = item.getPath().lastIndexOf("/") + 1;
        return Constant.BASE_PATH + ECGDrawWaveManager.ECG_FILE_PATH + "/" + item.getPath().substring(cutPosition);
    }

    /**
     * 知己档案首页接口
     *
     * @param memberChildId 子账户id
     */
    public void getAllRecord(int memberChildId,String pageNumber, AbstractDefaultHttpHandlerCallback callback) {
        String url = "/member/new_ins/all.jhtml";
        RequestParams params = new RequestParams();
        params.put("memberChildId", memberChildId);
        params.put("pageNumber", pageNumber);
        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {

            @Override
            public Object contentParse(String content) {
                List<ArchivesBean> beanList = new ArrayList<>();
                try {
                    if (content != null&&!content.equals("")) {
                        beanList = JSON.parseArray(content, ArchivesBean.class);
                        return beanList;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return beanList;
            }
        }, LoginControllor.MEMBER, callback.getContext());
    }
}
