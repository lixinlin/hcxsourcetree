package com.hxlm.android.hcy.message;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.text.TextUtils;
import com.hxlm.android.hcy.asynchttp.AbstractHttpHandlerCallback;
import com.hxlm.android.hcy.message.ActionManager;
import com.hxlm.android.hcy.message.HcyMessage;
import com.hxlm.android.hcy.message.MessageManager;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.user.Member;
import com.hxlm.android.hcy.user.UserManager;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.Constant;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * 消息服务
 */
public class MessageService extends Service {
    private final Handler handler = new Handler();

    private final AbstractHttpHandlerCallback messageHandler = new AbstractHttpHandlerCallback() {
        @Override
        public void preSubmit() {
        }

        @Override
        protected void onResponseSuccess(Object obj) {
            //得到返回消息
            MessageManager messageManager = new MessageManager();

            List<HcyMessage> hcyMessages = (List<HcyMessage>) obj;

            if (hcyMessages != null && hcyMessages.size() > 0) {
                //指定系统发送的特殊指令
                new ActionManager().handleMessage(hcyMessages);
            }

//            if (hcyMessages != null && hcyMessages.size() > 0) {
//                Log.i("HcyHttpResponseHandler","hcyMessages1111-->"+hcyMessages);
//                //保存消息到Share
//                messageManager.saveMessagesToSp(hcyMessages);
//
//                for (WeakReference<TitleBarView> weakTitleBarview : Constant.titleBarViews) {
//                    if (weakTitleBarview.get() != null) {
//                        //更新当前titlebar消息图片
//                        weakTitleBarview.get().refreshUnread(true);
//                    }
//                }
//            }

            handler.postDelayed(queryRunnable, 3 * 1000 * 60);
        }

        @Override
        protected void onResponseError(int errorCode, String errorDesc) {
            handler.postDelayed(queryRunnable, 60 * 1000 * 60);
        }

        @Override
        protected void onHttpError(int httpErrorCode) {
            handler.postDelayed(queryRunnable, 60 * 1000 * 60);
        }

        @Override
        protected void onNetworkError() {
            handler.postDelayed(queryRunnable, 60 * 1000 * 60);
        }

        @Override
        protected void onProgress(int bytesWritten, int totalSize) {

        }

    };
    private final Runnable queryRunnable = new Runnable() {
        @Override
        public void run() {
            new MessageManager().fetchMessage(messageHandler);
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        final MessageManager messageManager = new MessageManager();

        Member savedMember = LoginControllor.getSavedMember();

        //如果用户保存了密码，此处触发自动登录
        if (savedMember != null &&
                !TextUtils.isEmpty(savedMember.getUserName()) && !TextUtils.isEmpty(savedMember.getPassword())) {
            new UserManager().login(savedMember.getUserName(), savedMember.getPassword(),
                    new AbstractHttpHandlerCallback() {
                        @Override
                        public void preSubmit() {
                        }

                        @Override
                        protected void onResponseSuccess(Object obj) {
                            messageManager.fetchMessage(messageHandler);
                        }

                        @Override
                        protected void onResponseError(int errorCode, String errorDesc) {
                            LoginControllor.logout();
                        }

                        @Override
                        protected void onHttpError(int httpErrorCode) {
                            messageManager.fetchMessage(messageHandler);
                        }

                        @Override
                        protected void onNetworkError() {
                            handler.postDelayed(queryRunnable, 60 * 1000 * 60);
                        }

                        @Override
                        protected void onProgress(int bytesWritten, int totalSize) {

                        }
                    });
        } else {
            //请求消息接口
            messageManager.fetchMessage(messageHandler);
        }

        //从Share得到消息
        List<HcyMessage> hcyMessages = messageManager.getHcyMessagesFromSp();

        //在指定的消息集合中是否存在未读消息  如果有未读消息
        if (!messageManager.isAllRead(hcyMessages)) {
            for (WeakReference<TitleBarView> weakTitleBarview : Constant.titleBarViews) {
                if (weakTitleBarview.get() != null) {
                    weakTitleBarview.get().refreshUnread(true);
                }
            }
        }

    }

    @Override
    public void onDestroy() {
        handler.removeCallbacks(queryRunnable);
        super.onDestroy();
    }
}
