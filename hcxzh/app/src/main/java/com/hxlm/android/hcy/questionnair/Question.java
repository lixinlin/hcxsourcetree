package com.hxlm.android.hcy.questionnair;

import java.util.List;

public class Question implements Comparable<Question>{
	private int id;// 问题的id
	private long createDate;// 生成时间
	private long modifyDate;// 修改时间
	private int order;// 题号
	private String name;// 问题名称
	private String description;// 问题描述
	private boolean reverse;// 是否反向计分

	private List<Answer> answers;
	private Answer answer;
	private int classifyId; //题目所属分类

	private int answer_int;//所选答案编号，可能由Answer表示。
	private int bufen;//问题所属分类
	private boolean chang_bufen;//是否是下个分类中的第一个
	private int number_ui;//界面展示的题号

	public int getNumber_ui() {
		return number_ui;
	}

	public void setNumber_ui(int number_ui) {
		this.number_ui = number_ui;
	}

	public int getBufen() {
		return bufen;
	}

	public void setBufen(int bufen) {
		this.bufen = bufen;
	}

	public boolean isChang_bufen() {
		return chang_bufen;
	}

	public void setChang_bufen(boolean chang_bufen) {
		this.chang_bufen = chang_bufen;
	}

	public int getAnswer_int() {
		return answer_int;
	}

	public void setAnswer_int(int answer_int) {
		this.answer_int = answer_int;
	}

	public boolean isReverse() {
		return reverse;
	}

	public void setReverse(boolean reverse) {
		this.reverse = reverse;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getCreateDate() {
		return createDate;
	}

	public void setCreateDate(long createDate) {
		this.createDate = createDate;
	}

	public long getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(long modifyDate) {
		this.modifyDate = modifyDate;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

	public Answer getAnswer() {
		return answer;
	}

	public void setAnswer(Answer answer) {
		this.answer = answer;
	}

	public int getClassifyId() {
		return classifyId;
	}

	public void setClassifyId(int classifyId) {
		this.classifyId = classifyId;
	}

	@Override
	public String toString() {
		return "Question [id=" + id + ", createDate=" + createDate
				+ ", modifyDate=" + modifyDate + ", order=" + order + ", name="
				+ name + ", description=" + description + ", reverse="
				+ reverse + "]";
	}

	@Override
	public int compareTo(Question another) {
		return this.order - another.order;
	}
}
