package com.hxlm.android.hcy.user;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import com.hcy.ky3h.R;
import com.hcy_futejia.activity.CreatePasswordActivity;
import com.hcy_futejia.manager.UserEnglishManager;
import com.hxlm.android.hcy.AbstractBaseActivity;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.utils.MyCountTimer;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.util.IsMobile;
import com.hxlm.hcyandroid.util.StatusBarUtils;
import com.hxlm.hcyandroid.util.ToastUtil;
import com.hxlm.hcyandroid.view.ContainsEmojiEditText;

/**
 * Created by l on 2016/10/14.
 */
public class ForgetPasswordActivity extends AbstractBaseActivity implements View.OnClickListener {

    public static OnCompleteListener onCompleteListener;
    private UserManager userManager;
    private ContainsEmojiEditText et_account;// 账号
    private ContainsEmojiEditText et_password;// 密码
    private ContainsEmojiEditText et_confirm_password;// 确认密码
    private ContainsEmojiEditText et_test_number;// 验证码
    private String account;
    private String password;

    private Context context;
    private Button bt_get_test_number;
    private UserEnglishManager userEnglishManager;

    @Override
    protected void setContentView() {
        StatusBarUtils.setWindowStatusBarColor(this, R.color.color_1e82d2);
        setContentView(R.layout.activity_forget_password);
        this.context = ForgetPasswordActivity.this;
        userManager = new UserManager();
    }

    @Override
    protected void initViews() {
        et_account = findViewById(R.id.et_account);
        et_password = findViewById(R.id.et_password);
        et_confirm_password = findViewById(R.id.et_confirm_password);
        et_test_number = findViewById(R.id.et_test_number);

        if (Constant.isEnglish){
            CharSequence strHint = et_test_number.getHint();
            SpannableString span = new SpannableString(strHint);
            span.setSpan(new RelativeSizeSpan(0.9f), 0, strHint.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            et_test_number.setHint(span);
        }

        LinearLayout linear_back = findViewById(R.id.linear_back);
        bt_get_test_number = findViewById(R.id.bt_get_test_number);
        Button bt_commit_and_login = findViewById(R.id.bt_commit_and_login);

        linear_back.setOnClickListener(this);
        bt_get_test_number.setOnClickListener(this);
        bt_commit_and_login.setOnClickListener(this);
    }

    @Override
    protected void initDatas() {
        userEnglishManager = new UserEnglishManager();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linear_back:
                finish();
                break;

            // 获取验证码
            case R.id.bt_get_test_number:
                account = et_account.getText().toString();
                if (!TextUtils.isEmpty(account)) {
                    //验证手机号
                    boolean idMobile = IsMobile.isMobileNO(account);
                    boolean email = IsMobile.isEmail(account);
                    boolean englishMobileNO = IsMobile.isEnglishMobileNO(account);
                    if (Constant.isEnglish){
                        if (email ) {
                            userEnglishManager.getEmailCodeChangePwd(account, new AbstractDefaultHttpHandlerCallback(ForgetPasswordActivity.this) {
                                @Override
                                protected void onResponseSuccess(Object obj) {
                                    Log.e("retrofit", "====忘记密码邮箱获取验证码接口===" + obj.toString());
                                }
                            });
                        }else if (englishMobileNO){
                            userEnglishManager.getPhoneCodeChangePwd(account, new AbstractDefaultHttpHandlerCallback(ForgetPasswordActivity.this) {
                                @Override
                                protected void onResponseSuccess(Object obj) {
                                    Log.e("retrofit", "====忘记密码外国手机号获取验证码接口===" + obj.toString());
                                }
                            });
                        } else {
                            ToastUtil.invokeShortTimeToast(context, "incorrect format of phone number/Email address");
                        }
                    }else {
                        if (idMobile) {
                            getYAM();
                        } else {
                            ToastUtil.invokeShortTimeToast(context, getString(R.string.login_phone_format_error));
                        }
                    }
                } else {
                    ToastUtil.invokeShortTimeToast(context, context.getString(R.string.username_is_empty));
                }
                break;

            // 提交并登陆
            case R.id.bt_commit_and_login:
                account = et_account.getText().toString();
                password = et_password.getText().toString();
                String confirmPassword = et_confirm_password.getText().toString();
                String testNumber = et_test_number.getText().toString();

                if (TextUtils.isEmpty(account)) {
                    ToastUtil.invokeShortTimeToast(context, getString(R.string.username_is_empty));
                } else if (TextUtils.isEmpty(password)) {
                    ToastUtil.invokeShortTimeToast(context, getString(R.string.forget_pwd_mima_empty));
                } else if (TextUtils.isEmpty(confirmPassword)) {
                    ToastUtil.invokeShortTimeToast(context, getString(R.string.forget_pwd_sure_mima_empty));
                } else if (TextUtils.isEmpty(testNumber)) {
                    ToastUtil.invokeShortTimeToast(context, getString(R.string.forget_pwd_sms_empty));
                } else if (!password.equals(confirmPassword)) {
                    ToastUtil.invokeShortTimeToast(context, getString(R.string.forget_pwd_dont_agree));
                } else {
                    userManager.createNewPassword(account, testNumber, password,
                            new AbstractDefaultHttpHandlerCallback(context) {
                                @Override
                                protected void onResponseSuccess(Object obj) {
                                    //ForgetPasswordDialog.this.dismiss();

                                  //  ToastUtil.invokeShortTimeToast(getContext(), "修改成功，请重新登录");
                                    userManager.login(account, password, new AbstractDefaultHttpHandlerCallback(getContext()) {
                                        @Override
                                        protected void onResponseSuccess(Object obj) {
                                            ToastUtil.invokeShortTimeToast(getContext(), getString(R.string.forget_pwd_save_and_login));

                                            if (null != onCompleteListener) {
                                                onCompleteListener.onComplete();
                                            }

                                            ForgetPasswordActivity.this.finish();
                                        }
                                    });
                                }
                            });
                }
                break;
            default:
                break;
        }
    }

    private void getYAM() {
        userManager.getCaptchaAgain(account, new AbstractDefaultHttpHandlerCallback(context) {
            @Override
            protected void onResponseSuccess(Object obj) {
                ToastUtil.invokeShortTimeToast(getContext(), getString(R.string.login_has_been_send));
                MyCountTimer myCountTimer = new MyCountTimer(ForgetPasswordActivity.this, 60000, 1000, bt_get_test_number, 2);
                myCountTimer.start();
                bt_get_test_number.setClickable(false);
                myCountTimer.setCountTimerFinishListener(new MyCountTimer.OnCountTimerFinishListener() {
                    @Override
                    public void onTimerFinish() {
                        bt_get_test_number.setClickable(true);
                        bt_get_test_number.setText(getString(R.string.bt_get_test_number));
                    }
                });
            }
        });
    }
}