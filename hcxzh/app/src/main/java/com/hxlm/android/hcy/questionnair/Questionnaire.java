package com.hxlm.android.hcy.questionnair;

import com.hxlm.hcyandroid.bean.Subject;

import java.util.List;

/**
 * 问卷
 * 
 * @author Administrator
 *
 */
public class Questionnaire {
	private int id;// 问卷id
	private long createDate;// 生成时间
	private long modifyDate;// 修改时间
	private int order;// 问卷排序
	private String name;// 问卷名
	private String description;// 问卷描述
	private String tips;// 问卷提示语
	private String scoringWay;// 计分方式
	private String subjectCategory;// 所属结论分类的编码

	private List<Answer> answer;// 答案的集合
	private List<Question> question;// 问题的集合

	private int totalScore;
	private Subject subject;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getCreateDate() {
		return createDate;
	}

	public void setCreateDate(long createDate) {
		this.createDate = createDate;
	}

	public long getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(long modifyDate) {
		this.modifyDate = modifyDate;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTips() {
		return tips;
	}

	public void setTips(String tips) {
		this.tips = tips;
	}

	public String getScoringWay() {
		return scoringWay;
	}

	public void setScoringWay(String scoringWay) {
		this.scoringWay = scoringWay;
	}

	public String getSubjectCategory() {
		return subjectCategory;
	}

	public void setSubjectCategory(String subjectCategory) {
		this.subjectCategory = subjectCategory;
	}

	public List<Answer> getAnswer() {
		return answer;
	}

	public void setAnswer(List<Answer> answer) {
		this.answer = answer;
	}

	public List<Question> getQuestion() {
		return question;
	}

	public void setQuestion(List<Question> question) {
		this.question = question;
	}

	public int getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(int totalScore) {
		this.totalScore = totalScore;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public String getSubjectSn() {
		if(this.subject != null) {
			return subject.getSubject_sn();
		}else {
			return null;
		}
	}

	@Override
	public String toString() {
		return "Questionnaire [id=" + id + ", createDate=" + createDate
				+ ", modifyDate=" + modifyDate + ", order=" + order + ", name="
				+ name + ", description=" + description + ", tips=" + tips
				+ ", scoringWay=" + scoringWay + ", subjectCategory="
				+ subjectCategory + ", answer=" + answer + ", question="
				+ question + "]";
	}
}
