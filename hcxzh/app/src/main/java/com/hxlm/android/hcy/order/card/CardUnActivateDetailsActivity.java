package com.hxlm.android.hcy.order.card;

import android.app.Dialog;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hhmedic.bean.MemberInfoBean;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.order.CardManager;
import com.hxlm.android.hcy.user.HhUserManager;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.user.Member;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.util.ToastUtil;

/**
 * @author Lirong
 * @date 2018/12/5 0005.
 * @description  未激活的卡详情
 */

public class CardUnActivateDetailsActivity extends BaseActivity implements View.OnClickListener {

    private Button btn_card_toActivate;
    private Dialog dialog;
    private String card_no;
    private String card_name;
    private String card_desc;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_card_unactivate_details);
    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.card_unActive_details_title), titleBar, 1);

        //获取卡号
        Intent intent = getIntent();
        card_name = intent.getStringExtra("CARD_NAME");
        card_no = intent.getStringExtra("CARD_NO");
        card_desc = intent.getStringExtra("CARD_DESC");

        TextView tv_card_unActivate_title = findViewById(R.id.tv_card_unActivate_title);
        TextView tv_card_unActivate_introduce = findViewById(R.id.tv_card_unActivate_introduce);
        TextView tv_card_unActive_cardNo = findViewById(R.id.tv_card_unActive_cardNo);
        tv_card_unActivate_title.setText(card_name);
        tv_card_unActive_cardNo.setText(getString(R.string.card_details_card_no)+card_no);
        tv_card_unActivate_introduce.setText(card_desc);
        //激活
        btn_card_toActivate = findViewById(R.id.btn_card_toActivate);
        btn_card_toActivate.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            //激活
            case R.id.btn_card_toActivate:
                createDialog();
                break;
        }
    }

    @Override
    public void initDatas() {

    }

    /**
     * 创建dialog
     */
    private void createDialog(){
        dialog = new Dialog(this,R.style.dialog);
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_activate, null);
        dialog.setContentView(view);
        //初始化控件
        Button btn_dialog_activate_no = view.findViewById(R.id.btn_dialog_activate_no);
        Button btn_dialog_activate_yes = view.findViewById(R.id.btn_dialog_activate_yes);
        TextView tv_dialog_text = view.findViewById(R.id.tv_dialog_text);

        tv_dialog_text.setText(getString(R.string.card_unActive_details_dialog_text1)+card_name+getString(R.string.card_unActive_details_dialog_text2));

        btn_dialog_activate_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btn_dialog_activate_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //获取卡号
                Intent intent = getIntent();
                card_no = intent.getStringExtra("CARD_NO");
                new CardManager().activeCard(card_no, new AbstractDefaultHttpHandlerCallback(CardUnActivateDetailsActivity.this) {
                    @Override
                    protected void onResponseSuccess(Object obj) {
                        ToastUtil.invokeShortTimeToast(CardUnActivateDetailsActivity.this,getString(R.string.card_unActive_details_tips_success));
                        new HhUserManager().getMemberInfo(new AbstractDefaultHttpHandlerCallback(CardUnActivateDetailsActivity.this) {
                            @Override
                            protected void onResponseSuccess(Object obj) {
                                if(obj != null){
                                    MemberInfoBean memberInfoBean = (MemberInfoBean) obj;
                                    Member loginMember = LoginControllor.getLoginMember();
                                    if(loginMember!=null){
                                        loginMember.setUuid(memberInfoBean.getUid());
                                        loginMember.setUserToken(memberInfoBean.getToken());
                                        LoginControllor.setLoginMember(loginMember);
                                    }
                                }
                            }

                            @Override
                            protected void onResponseError(int errorCode, String errorDesc) {
                                super.onResponseError(errorCode, errorDesc);
                                //卡激活成功但获取和缓用户信息失败，退出登录重新进应用即可
                            }
                        });
                        CardUnActivateDetailsActivity.this.finish();
                    }
                });
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
    }

}
