package com.hxlm.android.hcy.user;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.hcy_futejia.activity.EnglishLoginActivity;
import com.hcy_futejia.activity.FtjLoginActivity;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.utils.AESUtil;
import com.hxlm.hcyandroid.BaseApplication;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.util.IsMobile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoginControllor {
    public static final String MEMBER = "memberId";
    public static final String CHILD_MEMBER = "memberChildId";

    private static final String SP_MEMBER_STORE_NAME = "member";
    private static final String SP_MEMBER_SETING = "seting";
    private static final String SP_AUTO_LOGIN_SMS = "autoLoginSms";
    private static final String SP_AUTO_LOGIN_WEICHAT = "autoLoginweichat";
    private static final String SP_AUTO_LOGIN_MM = "autoLoginmima";
    private static final String SP_USER_NAME = "username";
    private static final String SP_PASSWORD = "password";
    private static final String SP_MEMBER_OBJECT = "object";
    private static final String AES_KEY = "5c33tE2ah*4sM5cs";
    private static Member loginMember;
    private static ChildMember choosedChildMember;
    private static String token = "";
    private static String JSESSIONID = "";
    private static String weixinNickName = "";//登录时赋值，退出时清空
    private static boolean isAutologinBySms; //是否自动免密登录
    private static String phone; //手机号
    public static void clearLastLoginInfo() {
        weixinNickName = "";
        loginMember = null;
        choosedChildMember = null;
        //删除cookie信息
        HcyHttpClient.removeCookie();
    }

    public static void clearAutoLogin() {
        setIsAutologinBySms(false, "");
        setAutoLoginByMiMa(false, "", "");
        setAutoLoginByWeiChart(false, "", "", "", "");
    }

    public static boolean isLogin() {
        return loginMember != null;
    }

    private static boolean isChoosedChileMember() {
        return choosedChildMember != null;
    }

    public static void saveMember(String userName, String password) {
        SharedPreferences sp = BaseApplication.getContext().getSharedPreferences(SP_MEMBER_STORE_NAME, Context.MODE_PRIVATE);
        if (sp != null) {
            SharedPreferences.Editor editor = sp.edit();
            editor.putString(SP_USER_NAME, encrypt(userName));
            editor.putString(SP_PASSWORD, encrypt(password));
            editor.apply();
        }
    }

    public static Member getSavedMember() {
        SharedPreferences sp = BaseApplication.getContext().getSharedPreferences(SP_MEMBER_STORE_NAME, Context.MODE_PRIVATE);
        if (sp != null) {
            String userName = sp.getString(SP_USER_NAME, null);
            String password = sp.getString(SP_PASSWORD, null);
            userName = decrype(userName);
            password = decrype(password);
            Member saveMember = new Member();
            saveMember.setUsername(userName);
            saveMember.setPassword(password);
            return saveMember;
        }
        return null;
    }

    public static void logout() {
        clearLastLoginInfo();
        clearAutoLogin();
        SharedPreferences sp = BaseApplication.getContext().getSharedPreferences(SP_MEMBER_STORE_NAME, Context.MODE_PRIVATE);
        if (sp != null) {
            SharedPreferences.Editor editor = sp.edit();
            editor.putString(SP_PASSWORD, null);
            editor.apply();
        }
    }

    /**
     * 选择子账户，自动登录
     */
    public static void chooseChildMember(final Context context, final OnCompleteListener onCompleteListener) {
        if (!isLogin()) {
            autoLoginAndChooseChildMember(context, onCompleteListener);
        } else if (!isChoosedChileMember()) {//如果用户没有选择用户

//            new ChooseMemberDialog(context, onCompleteListener).show();

            try {
                //手动设置主账号
                List<ChildMember> childMembers = LoginControllor.getLoginMember().getMengberchild();
                Member loginMember = LoginControllor.getLoginMember();
                for (ChildMember cm :childMembers){
                    if(cm.getMobile().equals(loginMember.getUserName())){
                        LoginControllor.setChoosedChildMember(cm);
                    }else if(cm.getName().equals(loginMember.getUserName())){
                        LoginControllor.setChoosedChildMember(cm);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (onCompleteListener != null) {
                onCompleteListener.onComplete();
            }
        } else {
            if (onCompleteListener != null) {
                onCompleteListener.onComplete();
            }
        }
    }

    /**
     * 请求登录  用于判断用户是否登录，以及登录之后的操作
     */
    public static void requestLogin(final Context context, final OnCompleteListener onCompleteListener) {
        if (!isLogin()) {
            autoLogin(context, onCompleteListener);
        } else {
            if (onCompleteListener != null) {
                onCompleteListener.onComplete();
            }
        }
    }

    public static void saveObjectMember(Member member) {
//        SharedPreferences sp = BaseApplication.getContext().getSharedPreferences(SP_MEMBER_STORE_NAME,
//                Context.MODE_PRIVATE);
//
//        if (sp != null) {
//            SharedPreferences.Editor editor = sp.edit();
//            editor.putString(SP_MEMBER_OBJECT, new Gson().toJson(member));
//            editor.apply();
//        }
        BaseApplication.getInstance().setCurrentMember(member);
    }

    public static Member getObjectMember() {
//        SharedPreferences sp = BaseApplication.getContext().getSharedPreferences(SP_MEMBER_STORE_NAME,
//                Context.MODE_PRIVATE);
//
//        if (sp != null) {
//            String m = sp.getString(SP_MEMBER_OBJECT, null);
//            Member saveMember = new Gson().fromJson(m, Member.class);
//            return saveMember;
//        }
        try {
            Member currentMember = BaseApplication.getInstance().getCurrentMember();
            return currentMember;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static Member getLoginMember() {
        if (loginMember == null) {
            loginMember = getObjectMember();
        }
        return loginMember;
    }

    public static void setLoginMember(Member aMember) {
        if (aMember != null) {
            saveObjectMember(aMember);
            loginMember = aMember;
            if (loginMember.getMengberchild() != null && choosedChildMember != null) {
                int id = choosedChildMember.getId();
                choosedChildMember = null;
                for (ChildMember childMember : loginMember.getMengberchild()) {
                    if (childMember.getId() == id) {
                        choosedChildMember = childMember;
                        setChoosedChildMember(childMember);
                    }
                }
            }
        }
    }

    public static ChildMember getChoosedChildMember() {
        if (choosedChildMember == null) {
            try {
//                choosedChildMember = getSavedMember().getMengberchild().get(0);
                choosedChildMember = BaseApplication.getInstance().getChooseChildMember();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return choosedChildMember;
    }

    public static void setChoosedChildMember(ChildMember choosedChildMember) {
        LoginControllor.choosedChildMember = choosedChildMember;
        BaseApplication.getInstance().setChooseChildMember(choosedChildMember);
    }

    public static String getLoginMemberJSESSIONID() {
        return JSESSIONID;
    }

    public static void setLoginMemberJSESSIONID(String JSESSIONID) {
        LoginControllor.JSESSIONID = JSESSIONID;
    }

    public static String getLoginMemberToken() {
        return token;
    }

    public static void setLoginMemberToken(String token) {
        LoginControllor.token = token;
    }

    public static String getWeixinNickName() {
        return weixinNickName;
    }

    public static void setWeixinNickName(String weixinNickName) {
        LoginControllor.weixinNickName = weixinNickName;
    }

    public static boolean isIsAutologinBySms() {
        SharedPreferences sp = BaseApplication.getContext().getSharedPreferences(SP_MEMBER_SETING,
                Context.MODE_PRIVATE);
        if (sp != null) {
            boolean isAutoLogin = sp.getBoolean(SP_AUTO_LOGIN_SMS, false);
            return isAutoLogin;
        }
        return false;
    }


    public static void setIsAutologinBySms(boolean isAutologinBySms, String phone) {
        LoginControllor.isAutologinBySms = isAutologinBySms;
        SharedPreferences sp = BaseApplication.getContext().getSharedPreferences(SP_MEMBER_SETING,
                Context.MODE_PRIVATE);

        if (sp != null) {
            SharedPreferences.Editor editor = sp.edit();
            editor.putBoolean(SP_AUTO_LOGIN_SMS, isAutologinBySms);
            editor.putString("phone", encrypt(phone));
            editor.apply();
        }
    }

    public static String getPhone() {
        SharedPreferences sp = BaseApplication.getContext().getSharedPreferences(SP_MEMBER_SETING,
                Context.MODE_PRIVATE);
        if (sp != null) {
            String phone = sp.getString("phone", "");
            phone = decrype(phone);
            return phone;
        }
        return "";
    }

    public static void setAutoLoginByWeiChart(Boolean autoLogin, String unionid, String screen_name, String gender, String profile_image_url) {
        SharedPreferences sp = BaseApplication.getContext().getSharedPreferences(SP_MEMBER_SETING,
                Context.MODE_PRIVATE);
        if (sp != null) {
            SharedPreferences.Editor editor = sp.edit();
            editor.putBoolean(SP_AUTO_LOGIN_WEICHAT, autoLogin);
            editor.putString("unionid", encrypt(unionid));
            editor.putString("screen_name", encrypt(screen_name));
            editor.putString("gender", encrypt(gender));
            editor.putString("profile_image_url", encrypt(profile_image_url));
            editor.apply();
        }
    }

    public static boolean isIsAutologinByWeiChat() {
        SharedPreferences sp = BaseApplication.getContext().getSharedPreferences(SP_MEMBER_SETING,
                Context.MODE_PRIVATE);
        if (sp != null) {
            boolean isAutoLogin = sp.getBoolean(SP_AUTO_LOGIN_WEICHAT, false);
            return isAutoLogin;
        }
        return false;
    }

    /**
     * 微信自动登录信息
     *
     * @return
     */
    public static Map<String, String> getWeiChatInfo() {
        Map<String, String> infoMap = new HashMap<>();

        SharedPreferences sp = BaseApplication.getContext().getSharedPreferences(SP_MEMBER_SETING,
                Context.MODE_PRIVATE);
        if (sp != null) {
            String unionid = sp.getString("unionid", "");
            String screen_name = sp.getString("screen_name", "");
            String gender = sp.getString("gender", "");
            String profile_image_url = sp.getString("profile_image_url", "");
            unionid = decrype(unionid);
            screen_name = decrype(screen_name);
            gender = decrype(gender);
            profile_image_url = decrype(profile_image_url);
            infoMap.put("unionid", unionid);
            infoMap.put("screen_name", screen_name);
            infoMap.put("gender", gender);
            infoMap.put("profile_image_url", profile_image_url);
        }
        return infoMap;
    }

    public static boolean isIsAutologinByMiMa() {
        SharedPreferences sp = BaseApplication.getContext().getSharedPreferences(SP_MEMBER_SETING,
                Context.MODE_PRIVATE);
        if (sp != null) {
            boolean isAutoLogin = sp.getBoolean(SP_AUTO_LOGIN_MM, false);
            return isAutoLogin;
        }
        return false;
    }

    public static void setAutoLoginByMiMa(Boolean autoLogin, String mobile, String password) {
        SharedPreferences sp = BaseApplication.getContext().getSharedPreferences(SP_MEMBER_SETING,
                Context.MODE_PRIVATE);
        if (sp != null) {
            SharedPreferences.Editor editor = sp.edit();
            editor.putBoolean(SP_AUTO_LOGIN_MM, autoLogin);
            editor.putString("mobile", encrypt(mobile));
            editor.putString("password", encrypt(password));
            editor.apply();
        }
    }

    public static Map<String, String> getMiMaUserInfo() {
        Map<String, String> infoMap = new HashMap<>();

        SharedPreferences sp = BaseApplication.getContext().getSharedPreferences(SP_MEMBER_SETING,
                Context.MODE_PRIVATE);
        if (sp != null) {
            String mobile = sp.getString("mobile", "");
            String password = sp.getString("password", "");
            mobile = decrype(mobile);
            password = decrype(password);
            infoMap.put("mobile", mobile);
            infoMap.put("password", password);
        }
        return infoMap;
    }

    /**
     * 未注销用户自动登录
     */
    public static void autoLogin(Context context, OnCompleteListener onCompleteListener) {
        Log.d("autoLogin", "autoLogin: ");
        boolean isAutologinBySms = LoginControllor.isIsAutologinBySms();
        boolean isAutologinByWeiChat = LoginControllor.isIsAutologinByWeiChat();
        boolean isAutologinByMiMa = LoginControllor.isIsAutologinByMiMa();
        String phone = LoginControllor.getPhone();
        if (isAutologinBySms) {
            if (!TextUtils.isEmpty(phone) && IsMobile.isMobileNO(phone)) {
                long l = System.currentTimeMillis();
                new UserManager().loginByDuanXinAuto(phone, l, new AbstractDefaultHttpHandlerCallback(BaseApplication.getContext()) {
                    @Override
                    protected void onResponseSuccess(Object obj) {
                        if (onCompleteListener != null) {
                            onCompleteListener.onComplete();
                        }
                    }

                    @Override
                    protected void onResponseError(int errorCode, String errorDesc) {
                        super.onResponseError(errorCode, errorDesc);
                        //如果服务端返回失败信息，清除本地保存的密码
                        logout();
                    }
                });
            }
        } else if (isAutologinByWeiChat) {
            Map<String, String> weiChatInfo = LoginControllor.getWeiChatInfo();
            new UserManager().loginByWeiXin(
                    weiChatInfo.get("unionid")
                    , weiChatInfo.get("screen_name")
                    , weiChatInfo.get("gender")
                    , weiChatInfo.get("profile_image_url"), new AbstractDefaultHttpHandlerCallback(BaseApplication.getContext()) {
                        @Override
                        protected void onResponseSuccess(Object obj) {
                            if (onCompleteListener != null) {
                                onCompleteListener.onComplete();
                            }
                        }

                        @Override
                        protected void onResponseError(int errorCode, String errorDesc) {
                            super.onResponseError(errorCode, errorDesc);
                            //如果服务端返回失败信息，清除本地保存的密码
                            logout();
                        }
                    });

        } else if (isAutologinByMiMa) {
            Map<String, String> miMaUserInfo = LoginControllor.getMiMaUserInfo();
            new UserManager().login(miMaUserInfo.get("mobile"), miMaUserInfo.get("password"), new AbstractDefaultHttpHandlerCallback(BaseApplication.getContext()) {
                @Override
                protected void onResponseSuccess(Object obj) {
                    if (onCompleteListener != null) {
                        onCompleteListener.onComplete();
                    }
                }

                @Override
                protected void onResponseError(int errorCode, String errorDesc) {
                    super.onResponseError(errorCode, errorDesc);
                    //如果服务端返回失败信息，清除本地保存的密码
                    logout();
                }
            });
        } else {
            if (Constant.isEnglish){
                Intent intent = new Intent(context, EnglishLoginActivity.class);
                EnglishLoginActivity.onCompleteListener = onCompleteListener;
                context.startActivity(intent);
            }else {
                Intent intent = new Intent(context, FtjLoginActivity.class);
                FtjLoginActivity.onCompleteListener = onCompleteListener;
                context.startActivity(intent);
            }
        }
    }

    /**
     * 未注销用户自动登录并选择子账户
     */
    public static void autoLoginAndChooseChildMember(Context context, OnCompleteListener onCompleteListener) {
        Log.d("autoLogin", "autoLoginAndChooseChildMember: ");
        boolean isAutologinBySms = LoginControllor.isIsAutologinBySms();
        boolean isAutologinByWeiChat = LoginControllor.isIsAutologinByWeiChat();
        boolean isAutologinByMiMa = LoginControllor.isIsAutologinByMiMa();
        String phone = LoginControllor.getPhone();
        if (isAutologinBySms) {
            if (!TextUtils.isEmpty(phone) && IsMobile.isMobileNO(phone)) {
                long l = System.currentTimeMillis();
                new UserManager().loginByDuanXinAuto(phone, l, new AbstractDefaultHttpHandlerCallback(BaseApplication.getContext()) {
                    @Override
                    protected void onResponseSuccess(Object obj) {
                        //登录后重新构造界面
                        //            LoginControllor.setChoosedChildMember(childMembers.get(position));
                        if (onCompleteListener != null) {
                            onCompleteListener.onComplete();
                        }
                    }

                    @Override
                    protected void onResponseError(int errorCode, String errorDesc) {
                        super.onResponseError(errorCode, errorDesc);
                        //如果服务端返回失败信息，清除本地保存的密码
                        logout();
                    }
                });
            }
        } else if (isAutologinByWeiChat) {
            Map<String, String> weiChatInfo = LoginControllor.getWeiChatInfo();
            new UserManager().loginByWeiXin(
                    weiChatInfo.get("unionid")
                    , weiChatInfo.get("screen_name")
                    , weiChatInfo.get("gender")
                    , weiChatInfo.get("profile_image_url"), new AbstractDefaultHttpHandlerCallback(BaseApplication.getContext()) {
                        @Override
                        protected void onResponseSuccess(Object obj) {
//            LoginControllor.setChoosedChildMember(childMembers.get(position));
                            if (onCompleteListener != null) {
                                onCompleteListener.onComplete();
                            }
                        }

                        @Override
                        protected void onResponseError(int errorCode, String errorDesc) {
                            super.onResponseError(errorCode, errorDesc);
                            //如果服务端返回失败信息，清除本地保存的密码
                            logout();
                        }
                    });

        } else if (isAutologinByMiMa) {
            Map<String, String> miMaUserInfo = LoginControllor.getMiMaUserInfo();
            new UserManager().login(miMaUserInfo.get("mobile"), miMaUserInfo.get("password"), new AbstractDefaultHttpHandlerCallback(BaseApplication.getContext()) {
                @Override
                protected void onResponseSuccess(Object obj) {
                    //            LoginControllor.setChoosedChildMember(childMembers.get(position));
                    if (onCompleteListener != null) {
                        onCompleteListener.onComplete();
                    }
                }

                @Override
                protected void onResponseError(int errorCode, String errorDesc) {
                    super.onResponseError(errorCode, errorDesc);//如果服务端返回失败信息，清除本地保存的密码
                    logout();
                }
            });
        } else {
            //密码一旦清空跳转Activity
            if (Constant.isEnglish){
                Intent intent = new Intent(context, EnglishLoginActivity.class);
                EnglishLoginActivity.onCompleteListener = new OnCompleteListener() {
                    @Override
                    public void onComplete() {
                        //            LoginControllor.setChoosedChildMember(childMembers.get(position));
                        if (onCompleteListener != null) {
                            onCompleteListener.onComplete();
                        }
                    }
                };
                context.startActivity(intent);
            }else {
                Intent intent = new Intent(context, FtjLoginActivity.class);
                FtjLoginActivity.onCompleteListener = new OnCompleteListener() {
                    @Override
                    public void onComplete() {
                        //            LoginControllor.setChoosedChildMember(childMembers.get(position));
                        if (onCompleteListener != null) {
                            onCompleteListener.onComplete();
                        }
                    }
                };
                context.startActivity(intent);
            }
        }


    }

    public static String encrypt(String input) {
        try {
            byte[] encrypted = AESUtil.encrypt(input.getBytes(), AES_KEY.getBytes());
            return new String(Base64.encode(encrypted, Base64.DEFAULT)).replace("\n", "");// 此处返回使用BASE64做转码功能，同时能起到2次加密的作用。
        } catch (Exception e) {
            return "";
        }
    }

    public static String decrype(String input) {
        try {
            byte[] encrypted = Base64.decode(input, Base64.DEFAULT);
            byte[] original = AESUtil.decrypt(encrypted, AES_KEY.getBytes());
            return new String(original);
        } catch (Exception e) {
            return "";
        }
    }
}
