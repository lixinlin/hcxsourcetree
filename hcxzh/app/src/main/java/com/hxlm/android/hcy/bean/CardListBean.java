package com.hxlm.android.hcy.bean;

import java.util.List;

/**
 * @author Lirong
 * @date 2018/12/11 0011.
 * @description
 */

public class CardListBean {

    /**
     * data : [{"id":1,"createDate":1484729465000,"modifyDate":1484729482000,"code":"ZIUI2TXLNOCE6YSGI8L","bindDate":1484730255000,"balance":245.4,"isMove":false,"isExpired":false,"isActive":true,"isSale":true,"isSend":true,"isBind":true,"cashcard":{"id":1,"createDate":1484729445000,"modifyDate":1491536031000,"order":null,"type":"virtual","name":"测试卡","introduction":null,"beginDate":1484729445000,"endDate":2115881445000,"expiredTime":null,"amount":500,"price":0,"isEnabled":true},"isDated":false,"expire":true,"expireDate":2115881445000}]
     * status : 100
     * attr_data : [{"memberId":"1009","card_no":"JSYGRQPXAWL3G9HR9CH","card_name":"金玉满堂臻爱守护卡","description":"金玉满堂臻爱守护卡，是在昆仑2019年开门红到来之际，为了回馈广大用户，特制的健康服务卡。\r\n祝大家在新的一年里，健康祥和，身体健康万事如意。","status":"2","service_name":null,"exprise_time":null}]
     */

    private int status;
    private List<DataBean> data;
    private List<AttrDataBean> attr_data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public List<AttrDataBean> getAttr_data() {
        return attr_data;
    }

    public void setAttr_data(List<AttrDataBean> attr_data) {
        this.attr_data = attr_data;
    }

    public static class DataBean {
        /**
         * id : 1
         * createDate : 1484729465000
         * modifyDate : 1484729482000
         * code : ZIUI2TXLNOCE6YSGI8L
         * bindDate : 1484730255000
         * balance : 245.4
         * isMove : false
         * isExpired : false
         * isActive : true
         * isSale : true
         * isSend : true
         * isBind : true
         * cashcard : {"id":1,"createDate":1484729445000,"modifyDate":1491536031000,"order":null,"type":"virtual","name":"测试卡","introduction":null,"beginDate":1484729445000,"endDate":2115881445000,"expiredTime":null,"amount":500,"price":0,"isEnabled":true}
         * isDated : false
         * expire : true
         * expireDate : 2115881445000
         */

        private int id;
        private long createDate;
        private long modifyDate;
        private String code;
        private long bindDate;
        private double balance;
        private boolean isMove;
        private boolean isExpired;
        private boolean isActive;
        private boolean isSale;
        private boolean isSend;
        private boolean isBind;
        private CashcardBean cashcard;
        private boolean isDated;
        private boolean expire;
        private long expireDate;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public long getCreateDate() {
            return createDate;
        }

        public void setCreateDate(long createDate) {
            this.createDate = createDate;
        }

        public long getModifyDate() {
            return modifyDate;
        }

        public void setModifyDate(long modifyDate) {
            this.modifyDate = modifyDate;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public long getBindDate() {
            return bindDate;
        }

        public void setBindDate(long bindDate) {
            this.bindDate = bindDate;
        }

        public double getBalance() {
            return balance;
        }

        public void setBalance(double balance) {
            this.balance = balance;
        }

        public boolean isIsMove() {
            return isMove;
        }

        public void setIsMove(boolean isMove) {
            this.isMove = isMove;
        }

        public boolean isIsExpired() {
            return isExpired;
        }

        public void setIsExpired(boolean isExpired) {
            this.isExpired = isExpired;
        }

        public boolean isIsActive() {
            return isActive;
        }

        public void setIsActive(boolean isActive) {
            this.isActive = isActive;
        }

        public boolean isIsSale() {
            return isSale;
        }

        public void setIsSale(boolean isSale) {
            this.isSale = isSale;
        }

        public boolean isIsSend() {
            return isSend;
        }

        public void setIsSend(boolean isSend) {
            this.isSend = isSend;
        }

        public boolean isIsBind() {
            return isBind;
        }

        public void setIsBind(boolean isBind) {
            this.isBind = isBind;
        }

        public CashcardBean getCashcard() {
            return cashcard;
        }

        public void setCashcard(CashcardBean cashcard) {
            this.cashcard = cashcard;
        }

        public boolean isIsDated() {
            return isDated;
        }

        public void setIsDated(boolean isDated) {
            this.isDated = isDated;
        }

        public boolean isExpire() {
            return expire;
        }

        public void setExpire(boolean expire) {
            this.expire = expire;
        }

        public long getExpireDate() {
            return expireDate;
        }

        public void setExpireDate(long expireDate) {
            this.expireDate = expireDate;
        }

        public static class CashcardBean {
            /**
             * id : 1
             * createDate : 1484729445000
             * modifyDate : 1491536031000
             * order : null
             * type : virtual
             * name : 测试卡
             * introduction : null
             * beginDate : 1484729445000
             * endDate : 2115881445000
             * expiredTime : null
             * amount : 500.0
             * price : 0.0
             * isEnabled : true
             */

            private int id;
            private long createDate;
            private long modifyDate;
            private Object order;
            private String type;
            private String name;
            private String introduction;
            private long beginDate;
            private long endDate;
            private int expiredTime;
            private double amount;
            private double price;
            private boolean isEnabled;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public long getCreateDate() {
                return createDate;
            }

            public void setCreateDate(long createDate) {
                this.createDate = createDate;
            }

            public long getModifyDate() {
                return modifyDate;
            }

            public void setModifyDate(long modifyDate) {
                this.modifyDate = modifyDate;
            }

            public Object getOrder() {
                return order;
            }

            public void setOrder(Object order) {
                this.order = order;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getIntroduction() {
                return introduction;
            }

            public void setIntroduction(String introduction) {
                this.introduction = introduction;
            }

            public long getBeginDate() {
                return beginDate;
            }

            public void setBeginDate(long beginDate) {
                this.beginDate = beginDate;
            }

            public long getEndDate() {
                return endDate;
            }

            public void setEndDate(long endDate) {
                this.endDate = endDate;
            }

            public int getExpiredTime() {
                return expiredTime;
            }

            public void setExpiredTime(int expiredTime) {
                this.expiredTime = expiredTime;
            }

            public double getAmount() {
                return amount;
            }

            public void setAmount(double amount) {
                this.amount = amount;
            }

            public double getPrice() {
                return price;
            }

            public void setPrice(double price) {
                this.price = price;
            }

            public boolean isIsEnabled() {
                return isEnabled;
            }

            public void setIsEnabled(boolean isEnabled) {
                this.isEnabled = isEnabled;
            }
        }
    }

    public static class AttrDataBean {
        /**
         * memberId : 1009
         * card_no : JSYGRQPXAWL3G9HR9CH
         * card_name : 金玉满堂臻爱守护卡
         * description : 金玉满堂臻爱守护卡，是在昆仑2019年开门红到来之际，为了回馈广大用户，特制的健康服务卡。
         祝大家在新的一年里，健康祥和，身体健康万事如意。
         * status : 2
         * service_name : null
         * exprise_time : null
         */

        private String memberId;
        private String card_no;
        private String card_name;
        private String description;
        private String status;
        private String service_name;
        private String exprise_time;

        public String getMemberId() {
            return memberId;
        }

        public void setMemberId(String memberId) {
            this.memberId = memberId;
        }

        public String getCard_no() {
            return card_no;
        }

        public void setCard_no(String card_no) {
            this.card_no = card_no;
        }

        public String getCard_name() {
            return card_name;
        }

        public void setCard_name(String card_name) {
            this.card_name = card_name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getService_name() {
            return service_name;
        }

        public void setService_name(String service_name) {
            this.service_name = service_name;
        }

        public String getExprise_time() {
            return exprise_time;
        }

        public void setExprise_time(String exprise_time) {
            this.exprise_time = exprise_time;
        }

        @Override
        public String toString() {
            return "AttrDataBean{" +
                    "memberId='" + memberId + '\'' +
                    ", card_no='" + card_no + '\'' +
                    ", card_name='" + card_name + '\'' +
                    ", description='" + description + '\'' +
                    ", status='" + status + '\'' +
                    ", service_name='" + service_name + '\'' +
                    ", exprise_time='" + exprise_time + '\'' +
                    '}';
        }
    }
}
