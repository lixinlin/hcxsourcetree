package com.hxlm.android.hcy.content;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v4.view.ViewPager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.hcy.ky3h.R;
import com.hxlm.android.utils.Logger;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.bean.GongFaDatas;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lixinlin on 2019/3/6.
 */

public class GongFaUtils {

    /*
     * 4个按钮
     * 招式选择：
     * 轮播暂停
     * 播放乐药
     * 动作示范音乐
     * 默认
     *
     * 需要的状态：
     * 动画的暂停、播放
     * 乐药的暂停、播放
     * 文字的暂停、播放
     *
     * 需要记录的位置信息
     * 动画播放范围
     * 动画当前位置
     * 音乐播放范围
     * 音乐当前位置
     * 文字播放范围
     * 文字当前位置
     *
     * 基础资源
     * 图片
     * 文字
     * 示范音
     * 乐药
     */
    private Context context;
    List<String> vecFile = null;//本地乐药集合
    //图片集合
    //示范音集合
    //文字集合
    private final static String MUSIC_PATH = Constant.SHIFANYIN_PATH;     //存放示范音下载的地址
    public final int[][] GONGFA_GROUP = {{0, 35}, {0, 2}, {2, 2}, {4, 5}, {9, 5}, {14, 5}, {19, 5}, {24, 5}, {29, 4}, {33, 2}};
    private List<GongFaType> gongFaTypeList = new ArrayList<GongFaType>();//经络类型对应的招式，比如上宫对应第七式
    private MediaPlayer mediaPlayer;
    //乐药文件
    private List<String> audioFiles;

    public GongFaUtils(Context context) {
        this.context = context;
    }

    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    public void setMediaPlayer(MediaPlayer mediaPlayer) {
        this.mediaPlayer = mediaPlayer;
    }

    // 获取当前目录下所有的mp3文件
    private List<String> getYueYaoFiles() {

        File file = new File(Constant.YUEYAO_PATH);

        if (file.exists()) {
            File[] subFile = file.listFiles();

            for (File aSubFile : subFile) {
                // 判断是否为文件夹
                if (!aSubFile.isDirectory()) {
                    String filename = aSubFile.getName();
                    // 判断是否为MP3结尾
                    if (filename.endsWith(".mp3")) {
                        if (vecFile == null) {
                            vecFile = new ArrayList<String>();
                        }
                        vecFile.add(Constant.YUEYAO_PATH + "/" + filename);
                    }
                }
            }
        }
        return vecFile;
    }

    //根据不同的经络类型跳转不同的运动样式
    private void getGongFaTypeList() {

        gongFaTypeList.add(new GongFaType(context.getString(R.string.gongfa_gong_da), 7));
        gongFaTypeList.add(new GongFaType(context.getString(R.string.gongfa_gong_jia), 7));
        gongFaTypeList.add(new GongFaType(context.getString(R.string.gongfa_gong_shang), 7));
        gongFaTypeList.add(new GongFaType(context.getString(R.string.gongfa_gong_shao), 7));
        gongFaTypeList.add(new GongFaType(context.getString(R.string.gongfa_gong_zuo), 7));

        gongFaTypeList.add(new GongFaType(context.getString(R.string.gongfa_shang_shang), 3));
        gongFaTypeList.add(new GongFaType(context.getString(R.string.gongfa_shang_shao), 3));
        gongFaTypeList.add(new GongFaType(context.getString(R.string.gongfa_shang_tai), 3));
        gongFaTypeList.add(new GongFaType(context.getString(R.string.gongfa_shang_you), 3));
        gongFaTypeList.add(new GongFaType(context.getString(R.string.gongfa_shang_zuo), 3));

        gongFaTypeList.add(new GongFaType(context.getString(R.string.gongfa_jue_da), 6));
        gongFaTypeList.add(new GongFaType(context.getString(R.string.gongfa_jue_pan), 6));
        gongFaTypeList.add(new GongFaType(context.getString(R.string.gongfa_jue_shang), 6));
        gongFaTypeList.add(new GongFaType(context.getString(R.string.gongfa_jue_shao), 6));
        gongFaTypeList.add(new GongFaType(context.getString(R.string.gongfa_jue_tai), 6));

        gongFaTypeList.add(new GongFaType(context.getString(R.string.gongfa_zhi_pan), 5));
        gongFaTypeList.add(new GongFaType(context.getString(R.string.gongfa_zhi_shang), 5));
        gongFaTypeList.add(new GongFaType(context.getString(R.string.gongfa_zhi_shao), 5));
        gongFaTypeList.add(new GongFaType(context.getString(R.string.gongfa_zhi_you), 5));
        gongFaTypeList.add(new GongFaType(context.getString(R.string.gongfa_zhi_zhi), 5));

        gongFaTypeList.add(new GongFaType(context.getString(R.string.gongfa_yu_da), 4));
        gongFaTypeList.add(new GongFaType(context.getString(R.string.gongfa_yu_shang), 4));
        gongFaTypeList.add(new GongFaType(context.getString(R.string.gongfa_yu_shao), 4));
        gongFaTypeList.add(new GongFaType(context.getString(R.string.gongfa_yu_zhi), 4));
        gongFaTypeList.add(new GongFaType(context.getString(R.string.gongfa_yu_zhong), 4));

    }

    public void zhaoshixuanze(int index) {

    }


/**
 * 实现基本播放功能，和操作控制，暴露给界面使用，将界面和功能解耦以应对界面操作的各种变化
 * 有播放图片功能，播放乐药功能，播放示范音功能，播放文字功能
 * 播放位置对其：图片，示范音，文字的播放都按照一个位置索引index进行。例如index为13 对应的图片，示范音，文字
 * 示范音和乐药根据操作确定是否播放，所以要添加标识（play_demo_voice,play_yueyao_music,play_image,play_text)。通过界面操作控制是否播放
 *
 *播放范围控制，确定index的范围，例如选择第三式，其范围就是图片的[9,14]，文字的[9,14]，对文本和图片、示范音进行补齐，便于操作。
 *是否循环播放，loop
 *
 * 再切换招式时乐药是否切换需设置标识
 * 示范音也乐药不同时播放，声音播放互斥的，开启新的播放音乐任务时移除旧有的任务
 *
 * 轮播时长：根据示范音的时长变化，目前先固定6S。
 */

/**4个基本播放方法实现*/

    /**
     * 播放图片
     * @param start   播放范围的开始位置
     * @param end     播放范围的结束位置
     * @param cruuent 从哪个位置开始播放，用于暂停后在继续的情况，
     */
    private void playImage(int start, int end, int cruuent,ViewPager vp_banner) {
        vp_banner.setCurrentItem(cruuent);
    }

    /**
     * 播放文本描述
     * @param start   播放范围的开始位置
     * @param end     播放范围的结束位置
     * @param cruuent 从哪个位置开始播放，用于暂停后在继续的情况，
     */
    private void playText(int start, int end, int cruuent, TextView textView) {
        textView.setText(GongFaDatas.GONG_FA_EXPLAIN[cruuent]);
    }

    /**
     * 播放示范音
     * @param start   播放范围的开始位置
     * @param end     播放范围的结束位置
     * @param cruuent 从哪个位置开始播放，用于暂停后在继续的情况，
     */
    private void playDemoVoice(int start, int end, int cruuent,DemoVoiceInterfeace demoVoiceInterfeace) {
        File file = new File(MUSIC_PATH + "/" + GongFaDatas.GONG_FA_BIG_NAME[cruuent] + ".mp3");
        Log.d("GongFaActivity2", "filePath: "+ GongFaDatas.GONG_FA_BIG_NAME[cruuent]);
        if (file.exists()) {
            Uri uri = Uri.fromFile(file);
            songplay(uri,demoVoiceInterfeace);
//            handler.post(new Runnable() {
//                @Override
//                public void run() {
//
//                }
//            });
        }
    }


    /**
     *播放乐药，通过jingluotype 指定播放那首乐药，或者根据current确定，
     * 乐药播放可以单独控制，可能要考虑经络类型或者招式。
     * @param start
     * @param end
     * @param cruuent
     * @param jingluoType
     * @param resetYueYao 切换招式时是否从新播放乐药
     */
    private void playYueYao(int start, int end, int cruuent,String jingluoType,boolean resetYueYao) {
        audioFiles = getYueYaoFiles();
        if (audioFiles != null && audioFiles.size() > 0) {
            //播放乐药
//            songplay();
        } else {
            Toast.makeText(context,context.getString(R.string.gongfa_to_download), Toast.LENGTH_SHORT).show();

        }

}


    /**
     * 协调4个基本播放,设置参数适应更灵活的控制，
     * @param playImage  手势滑动的图片不在需要再切换图片，只需要记录位置
     * @param playDemoVoice
     * @param start
     * @param end
     * @param current
     */

    public void play(boolean playImage, boolean playText, boolean playDemoVoice, boolean yueyaoVoice, int start, int end, int current
            , ViewPager vp_banner, TextView textView, DemoVoiceInterfeace demoVoiceInterfeace){
        if(playImage){
            playImage(start,end,current,vp_banner);
        }
        playImage(start,end,current,vp_banner);
        if(playText){
            playText(start,end,current,textView);
        }
        if(playDemoVoice){
                playDemoVoice(start,end,current,demoVoiceInterfeace);
        }

        if (yueyaoVoice){
            playYueYao(start,end,current,"",false);
        }

    }

    /**
     * 停止音乐播放
     */
    public void stopPlayer() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    private int duration;
    /**
     * 开始音乐播放
     * @param audioFiles
     */
    private void songplay(Uri  audioFiles,DemoVoiceInterfeace demoVoiceInterfeace) {
        if (mediaPlayer == null) {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    mediaPlayer.reset();
                    return false;
                }
            });
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mediaPlayer.start();
                    duration = mediaPlayer.getDuration();
                    Log.d("GongFaActivity2", "songplay: "+duration);
                    //播放动画的时间推迟duration;
                    demoVoiceInterfeace.setDuration(duration);
                }
            });

        } else {
            mediaPlayer.reset();
        }
        try {
                mediaPlayer.setDataSource(context,audioFiles);
                mediaPlayer.prepareAsync();

        } catch (IllegalArgumentException | SecurityException | IllegalStateException | IOException e) {
            Logger.e("GongFaActivity2", e);
        }
    }

    public interface  DemoVoiceInterfeace{
        public void setDuration(int duration);
    }









    protected void onDestroy() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }
    /**
     * 按钮-监听电话
     */
    public void createPhoneListener() {
        TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        telephony.listen(new OnePhoneStateListener(),
                PhoneStateListener.LISTEN_CALL_STATE);
    }
    /**
     * 电话状态监听.
     */
    class OnePhoneStateListener extends PhoneStateListener {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            switch (state) {
                case TelephonyManager.CALL_STATE_RINGING://来电状态，电话铃声响起的那段时间或正在通话又来新电，新来电话不得不等待的那段时间
                    if (mediaPlayer != null) {
                        mediaPlayer.pause();
                    }

                    break;
                case TelephonyManager.CALL_STATE_IDLE://空闲状态，没有任何活动
                    if (mediaPlayer != null) {
                        mediaPlayer.start();
                    }
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK://摘机状态，至少有个电话活动。该活动或是拨打（dialing）或是通话
                    if (mediaPlayer != null) {
                        mediaPlayer.pause();
                    }
                    break;
                default:
                    break;
            }
            super.onCallStateChanged(state, incomingNumber);
        }
    }


}
