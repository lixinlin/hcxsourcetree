package com.hxlm.android.hcy.asynchttp;

/**
 * Created by lixinlin on 2018/12/31.
 */

public class RequestInfoBean {
    private String urlAndParmar;
    private long crateTime;

    public RequestInfoBean(String urlAndParmar, long crateTime) {
        this.urlAndParmar = urlAndParmar;
        this.crateTime = crateTime;
    }

    public String getUrlAndParmar() {
        return urlAndParmar;
    }

    public void setUrlAndParmar(String urlAndParmar) {
        this.urlAndParmar = urlAndParmar;
    }

    public long getCrateTime() {
        return crateTime;
    }

    public void setCrateTime(long crateTime) {
        this.crateTime = crateTime;
    }
}
