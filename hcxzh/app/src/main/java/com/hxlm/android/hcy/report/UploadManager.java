package com.hxlm.android.hcy.report;

import android.util.Log;

import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.asynchttp.HcyHttpResponseHandler;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.utils.Logger;
import com.hxlm.hcyandroid.CheckedDataType;
import com.hxlm.hcyandroid.bean.BloodPressureData;
import com.hxlm.hcyandroid.bean.BloodSugarData;
import com.hxlm.hcyandroid.bean.ECGData;
import com.loopj.android.http.RequestParams;

import java.io.File;
import java.io.FileNotFoundException;

public class UploadManager {
    private final String tag = getClass().getSimpleName();

    /**
     * 上传文件的接口
     *
     * @param file     待上传的文件
     * @param fileType 文件类型
     */
    public void uploadFile(File file, String fileType, AbstractDefaultHttpHandlerCallback callback) {
        String url = "/member/fileUpload/upload.jhtml?fileType=" + fileType;
        RequestParams params = new RequestParams();

        try {
            params.put("file", file);
            params.put("token", LoginControllor.getLoginMemberToken());
            LoginControllor.requestLogin(callback.getContext(), new OnCompleteListener() {
                @Override
                public void onComplete() {
                    HcyHttpClient.submit(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
                        @Override
                        public Object contentParse(String content) {
                            return content;
                        }
                    });
                }
            });
        } catch (FileNotFoundException e) {
            Logger.e(tag, e);
        }
    }

    /**
     * 上传闻音文件的接口
     *
     * @param file     待上传的文件
     * @param url
     */
    public void uploadWenYinFile(File file, String url, AbstractDefaultHttpHandlerCallback callback) {

        RequestParams params = new RequestParams();

        try {
            params.put("file", file);
            params.put("token", LoginControllor.getLoginMemberToken());
            HcyHttpClient.submitWenYin(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
                @Override
                public Object contentParse(String content) {
                    Log.d("contentParse", "contentParse: "+content);

                    return content;
                }
            });
        } catch (FileNotFoundException e) {
            Logger.e(tag, e.getMessage());
        }
    }


    /**
     * 上传检测数据的接口
     *
     * @param dataType 数据类别
     * @param data     待上传数据
     */
    public void uploadCheckedData(int dataType, Object data,long createDate, AbstractDefaultHttpHandlerCallback hacallbackdler) {
        String url = "/member/uploadData.jhtml";

        int memberId=LoginControllor.getLoginMember().getId();
        int memberChildId=LoginControllor.getChoosedChildMember().getId();
        RequestParams params = new RequestParams();
        params.put("memberId",memberId);
        params.put("memberChildId",memberChildId);
        params.put("datatype", dataType);
        params.put("createDate", createDate);
        //用户测试时间 只有心电才会上传 并且时间大于0
        if(CheckedDataType.ECG==dataType&&createDate>0)
        {
            params.put("createDate", createDate);//保存测试心电时间
        }



        switch (dataType) {
            case CheckedDataType.ECG:
                ECGData myData = (ECGData) data;
                params.put("path", myData.getPath());
                params.put("heartRate", myData.getHeartRate());
                params.put("subjectSn", myData.getSubjectSn());
                break;
            case CheckedDataType.SPO2H:
                params.put("density", (String) data);
                break;
            case CheckedDataType.BLOOD_PRESSURE:
                BloodPressureData bloodPressureData = (BloodPressureData) data;
                params.put("highPressure", Integer.parseInt(bloodPressureData.getHighPressure()));
                params.put("lowPressure", Integer.parseInt(bloodPressureData.getLowPressure()));
                params.put("pulse", Integer.parseInt(bloodPressureData.getPulse()));
                break;
            case CheckedDataType.TEMPERATURE:
                params.put("temperature", Double.parseDouble(data.toString()));
                break;
            case CheckedDataType.BREATH:
                params.put("nums", Integer.parseInt(data.toString()));
                break;
            case CheckedDataType.BLOOD_SUGAR:
                BloodSugarData bloodSugarData = (BloodSugarData) data;
                params.put("type", bloodSugarData.getType());
                params.put("levels", bloodSugarData.getLevels());
                params.put("isAbnormity", bloodSugarData.isAbnormity());
                break;
            default:
                break;
        }

//        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(hacallbackdler) {
//            @Override
//            public Object contentParse(String content) {
//                return content;
//            }
//        }, LoginControllor.CHILD_MEMBER, hacallbackdler.getContext());

        HcyHttpClient.submit(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(hacallbackdler) {
            @Override
            protected Object contentParse(String content) {
                return content;
            }
        });
    }

}

