package com.hxlm.android.hcy.order;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.utils.ViewUtil;
import com.hxlm.hcyandroid.Constant;

import java.util.List;

/**
 * 现金卡视图
 * Created by Zhenyu on 2016/4/21.
 */
public class CashCardPayView extends LinearLayout {
    private PayView payView;
    private ListView lvCards;
    private TextView tvNoCard;


    private List<Card> cards;// 自己定义的卡显示

    public CashCardPayView(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater.from(context).inflate(R.layout.cash_card, this);

        lvCards = (ListView) findViewById(R.id.lv_cards);
        tvNoCard = (TextView) findViewById(R.id.tv_no_card);
    }

    public void init(String goodsIdsStr, final PayView aPayView) {
        this.payView = aPayView;
        new CardManager().getCardsByProduct(goodsIdsStr,
                new AbstractDefaultHttpHandlerCallback(getContext()) {
                    @Override
                    protected void onResponseSuccess(Object obj) {
                        cards = (List<Card>) obj;

                        if(Constant.DEBUG)
                        {
                            Log.i("HcyHttpResponseHandler","cards-->"+cards.toString());
                        }


                        if (cards != null && cards.size() > 0) {
                            showCards();
                        }
                    }
                });
    }

    // 根据得到的卡进行判断
    private void showCards() {
        if (cards != null && cards.size() > 0) {
            lvCards.setVisibility(View.VISIBLE);
            tvNoCard.setVisibility(View.GONE);

            // 卡的支付
            final CardsListAdapter cardAdapter = new CardsListAdapter(getContext(), cards, false);
            lvCards.setAdapter(cardAdapter);

            ViewUtil.setListHeight(lvCards);

            lvCards.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    Card card = cards.get(position);

                    // 判断所选中的现金卡是否足够支付，如果选中了
                    if (!card.isChoosed()) {
                        if (payView.getMoneyRemain() == 0) {
                            Toast.makeText(getContext(), getContext().getString(R.string.cash_card_pay_tips_qingquxiao)
                                    , Toast.LENGTH_LONG).show();
                        } else {
                            card.setChoosed(true);

                            payView.addCardToPay(card);

                            cardAdapter.notifyDataSetChanged();
                        }
                    } else {
                        card.setChoosed(false);

                        payView.removeCardToPay(card);

                        cardAdapter.notifyDataSetChanged();
                    }
                }
            });
        }
    }
}
