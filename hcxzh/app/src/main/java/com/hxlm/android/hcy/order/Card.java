package com.hxlm.android.hcy.order;

/**
 * 卡，在获取现金卡的页面中使用
 *
 * @author Administrator
 */
public class Card {
    private int id;
    private double balance;
    private long bindDate;
    private String code;
    private long createDate;
    private int expiredTime;
    private boolean isExpired;
    private boolean isDated;//是否快过期
    private boolean isActive ;

    private CashCard cashcard;

    private transient boolean isChoosed;
    private transient double deductMoney;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public long getBindDate() {
        return bindDate;
    }

    public void setBindDate(long bindDate) {
        this.bindDate = bindDate;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public int getExpiredTime() {
        return expiredTime;
    }

    public void setExpiredTime(int expiredTime) {
        this.expiredTime = expiredTime;
    }

    public boolean isExpired() {
        return isExpired;
    }

    public void setExpired(boolean isExpired) {
        this.isExpired = isExpired;
    }

    public boolean isDated() {
        return isDated;
    }

    public void setDated(boolean isDated) {
        this.isDated = isDated;
    }

    public boolean isChoosed() {
        return isChoosed;
    }

    public void setChoosed(boolean isChoosed) {
        this.isChoosed = isChoosed;
    }

    public double getDeductMoney() {
        return deductMoney;
    }

    public void setDeductMoney(double deductMoney) {
        this.deductMoney = deductMoney;
    }

    public CashCard getCashcard() {
        return cashcard;
    }

    public void setCashcard(CashCard cashcard) {
        this.cashcard = cashcard;
    }

    @Override
    public String toString() {
        return "Card{" +
                "id=" + id +
                ", balance=" + balance +
                ", bindDate=" + bindDate +
                ", code='" + code + '\'' +
                ", createDate=" + createDate +
                ", expiredTime=" + expiredTime +
                ", isExpired=" + isExpired +
                ", isDated=" + isDated +
                ", cashcard=" + cashcard +
                ", isChoosed=" + isChoosed +
                ", deductMoney=" + deductMoney +
                '}';
    }

    public class CashCard {
        private int id;
        private String introduction;//介绍
        private String name;//卡的名称
        private int amount;  //面额
        private long beginDate;//开始日期
        private long endDate;//结束日期
        //卡的有效期
        private String expiredTime;

        public String getExpiredTime() {
            return expiredTime;
        }

        public void setExpiredTime(String expiredTime) {
            this.expiredTime = expiredTime;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getIntroduction() {
            return introduction;
        }

        public void setIntroduction(String introduction) {
            this.introduction = introduction;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAmount() {
            return amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public long getBeginDate() {
            return beginDate;
        }

        public void setBeginDate(long beginDate) {
            this.beginDate = beginDate;
        }

        public long getEndDate() {
            return endDate;
        }

        public void setEndDate(long endDate) {
            this.endDate = endDate;
        }

        @Override
        public String toString() {
            return "CashCard [amount=" + amount + ", id=" + id + ", introduction=" + introduction
                    + ", name=" + name + ", beginDate=" + beginDate + ", endDate=" + endDate + "]";
        }
    }
}