package com.hxlm.android.hcy.content;

import java.util.List;

public class ResourceItem {
    public final static int NOT_ORDERED = 0;
    public final static int NOT_DOWNLOAD = 1;
    public final static int DOWNLOADING = 2;
    public final static int STOP = 3;
    public final static int PLAYING = 4;
    public final static int NOT_PAY = 5;

    private int id;
    private String name;
    private String source;
    private long orderDate;
    private String localStoragePath;

    private long createDate;
    private long modifyDate;
    private Object order;
    private String type;
    private String content;
    private Object memo;
    private int productId;
    private double price;
    private String status;

    private List<ResourcesWarehousesEntity> resourcesWarehouses;

    private transient int downloadedSize;
    private transient int totalSize;
    private transient int itemStatus;

    public ResourceItem() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public long getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(long orderDate) {
        this.orderDate = orderDate;
    }

    public String getLocalStoragePath() {
        return localStoragePath;
    }

    public void setLocalStoragePath(String localStoragePath) {
        this.localStoragePath = localStoragePath;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(long modifyDate) {
        this.modifyDate = modifyDate;
    }

    public Object getOrder() {
        return order;
    }

    public void setOrder(Object order) {
        this.order = order;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Object getMemo() {
        return memo;
    }

    public void setMemo(Object memo) {
        this.memo = memo;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ResourcesWarehousesEntity> getResourcesWarehouses() {
        return resourcesWarehouses;
    }

    public void setResourcesWarehouses(List<ResourcesWarehousesEntity> resourcesWarehouses) {
        this.resourcesWarehouses = resourcesWarehouses;
    }

    public int getDownloadedSize() {
        return downloadedSize;
    }

    public void setDownloadedSize(int downloadedSize) {
        this.downloadedSize = downloadedSize;
    }

    public int getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(int totalSize) {
        this.totalSize = totalSize;
    }

    public int getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(int itemStatus) {
        this.itemStatus = itemStatus;
    }

    /**
     * title : 春江花月夜
     * source : 下载地址
     * order : null
     * file : null
     * empty : false
     */
    public class ResourcesWarehousesEntity {
        private String title;
        private String source;
        private Object order;
        private Object file;
        private boolean empty;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public Object getOrder() {
            return order;
        }

        public void setOrder(Object order) {
            this.order = order;
        }

        public Object getFile() {
            return file;
        }

        public void setFile(Object file) {
            this.file = file;
        }

        public boolean isEmpty() {
            return empty;
        }

        public void setEmpty(boolean empty) {
            this.empty = empty;
        }
    }

    @Override
    public String toString() {
        return "ResourceItem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", source='" + source + '\'' +
                ", orderDate=" + orderDate +
                ", localStoragePath='" + localStoragePath + '\'' +
                ", createDate=" + createDate +
                ", modifyDate=" + modifyDate +
                ", order=" + order +
                ", type='" + type + '\'' +
                ", content='" + content + '\'' +
                ", memo=" + memo +
                ", productId=" + productId +
                ", price=" + price +
                ", status='" + status + '\'' +
                ", resourcesWarehouses=" + resourcesWarehouses +
                ", downloadedSize=" + downloadedSize +
                ", totalSize=" + totalSize +
                ", itemStatus=" + itemStatus +
                '}';
    }
}
