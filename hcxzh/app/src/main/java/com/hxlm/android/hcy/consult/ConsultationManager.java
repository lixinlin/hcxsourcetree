package com.hxlm.android.hcy.consult;

import com.alibaba.fastjson.JSON;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.asynchttp.HcyHttpResponseHandler;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.hcyandroid.bean.MyConsultation;
import com.loopj.android.http.RequestParams;

import java.util.List;

public class ConsultationManager {
    /**
     * 提交咨询的接口
     *
     * @param consultationContent    咨询内容
     * @param userConsultationImages 咨询图片
     */
    public void commitConsultation(String consultationContent, List<String> userConsultationImages,
                                   AbstractDefaultHttpHandlerCallback callback) {
        String url = "/member/user_consultation/commit.jhtml";

        RequestParams params = new RequestParams();

        params.put("content", consultationContent);
        params.put("userConsultationImages", userConsultationImages);

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            public Object contentParse(String content) {
                return content;
            }
        }, LoginControllor.CHILD_MEMBER, callback.getContext());

    }

    /**
     * 获取我的咨询列表
     */
    public void getConsultation(AbstractDefaultHttpHandlerCallback callback) {
        String url = "/member/user_consultation/list.jhtml";

        RequestParams params = new RequestParams();

        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            public Object contentParse(String content) {
                return JSON.parseArray(content, MyConsultation.class);
            }
        }, LoginControllor.CHILD_MEMBER, callback.getContext());
    }

}
