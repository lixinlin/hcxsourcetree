package com.hxlm.android.hcy.utils;

import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.hcy.ky3h.R;

/**
 * @author donkor
 */
public class MyCountTimer extends CountDownTimer {
    public static final int TIME_COUNT = 31000;
    private TextView btn;
    private Context context;
    private OnCountTimerFinishListener mTimerFinishListener;

    /**
     * tag=1 一说辨识倒计时 tag=2 登录倒计时
     */
    private int tag;

    public MyCountTimer(Context context, long millisInFuture, long countDownInterval, TextView btn, int tag) {
        super(millisInFuture, countDownInterval);
        this.tag = tag;
        this.btn = btn;
        this.context = context;
    }

    /**
     * 参数上面有注释
     */
    public MyCountTimer(TextView btn, ImageView endStrRid) {
        super(TIME_COUNT, 1000);
        this.btn = btn;

        mTimerFinishListener = new OnCountTimerFinishListener() {
            @Override
            public void onTimerFinish() {
            }
        };
    }

    /**
     * 计时完毕时触发
     */
    @Override
    public void onFinish() {
        btn.setEnabled(true);
        mTimerFinishListener.onTimerFinish();
    }

    public interface OnCountTimerFinishListener {
        void onTimerFinish();
    }
    /**
     * 计时过程显示
     */
    @Override
    public void onTick(long millisUntilFinished) {
        Log.e("timer","===倒计时=="+(millisUntilFinished / 1000+1));
        btn.setEnabled(false);
        //每隔一秒修改一次UI
        if (tag == 1) {
            btn.setText((millisUntilFinished / 1000+1) + "");

            Animation animation = AnimationUtils.loadAnimation(context, R.anim.ani);
            btn.startAnimation(animation);
        }else if (tag == 2){
            btn.setText(millisUntilFinished / 1000 + context.getString(R.string.login_to_resend_sms));
        }
    }

    public void setCountTimerFinishListener(OnCountTimerFinishListener listener) {
        mTimerFinishListener = listener;
    }

}