package com.hxlm.android.hcy.order.card;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hhmedic.activity.CallSelectorAct;
import com.hxlm.android.hcy.bean.CardServiceBean;
import com.hxlm.android.hcy.order.erweima.activity.CaptureActivity;
import com.hxlm.hcyphone.harmonypackage.yihu.OnlineConsultingActivity;

import java.util.List;

/**
 * @author Lirong
 * @date 2018/12/10 0010.
 * @description 激活卡详细信息   剩余服务
 */

public class CardDetailsAdapter extends RecyclerView.Adapter<CardDetailsAdapter.CardDetailsViewHolder> {

    private Context context;
    private List<CardServiceBean> list;
    //serviceCode
    private String service_code = "10006";

    public CardDetailsAdapter(Context context,List<CardServiceBean> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public CardDetailsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_card_details, parent, false);
        return new CardDetailsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CardDetailsViewHolder holder, int position) {
        //获取数据
        CardServiceBean cardServiceBean = list.get(position);
        //设置下划线
        holder.tvDetails3.getPaint().setFlags(Paint. UNDERLINE_TEXT_FLAG );
        holder.tvDetails1.setText(cardServiceBean.getServiceName());
        holder.tvDetails2.setText(cardServiceBean.getValue()+cardServiceBean.getUnit());
        //根据serviceCode判断服务类型  当serviceCode为10006 时显示去咨询 其他都是去预约
        if (service_code.equals(cardServiceBean.getServiceCode()) ){
            holder.tvDetails3.setText(context.getString(R.string.card_details_adapter_to_consult));
            holder.tvDetails3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, CallSelectorAct.class);
                    context.startActivity(intent);
                }
            });
        }else {
            holder.tvDetails3.setText(context.getString(R.string.card_details_adapter_to_appointment));
            holder.tvDetails3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent1 = new Intent();
                    //表示打开拨打电话窗口，但还未拨出电话
                    intent1.setAction(Intent.ACTION_DIAL);
                    intent1.setData(Uri.parse("tel:" + "4006776668"));
                    context.startActivity(intent1);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class CardDetailsViewHolder extends RecyclerView.ViewHolder{

        private final TextView tvDetails1;
        private final TextView tvDetails2;
        private final TextView tvDetails3;

        public CardDetailsViewHolder(View itemView) {
            super(itemView);
            tvDetails1 = itemView.findViewById(R.id.item_card_details1);
            tvDetails2 = itemView.findViewById(R.id.item_card_details2);
            tvDetails3 = itemView.findViewById(R.id.item_card_details3);
        }
    }

}
