package com.hxlm.android.hcy.utils;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.hxlm.hcyandroid.BaseApplication;

/**
 * 设备工具类
 * Created by JQ on 15/8/1.
 */
public class DeviceUtils {

    /**
     * 获取手机型号
     * @return 手机型号
     */
    public static String getDeviceModel() {
        return android.os.Build.MODEL;
    }

    /**
     * 获取手机唯一标识
     * @return 手机唯一标识
     */
    public static String getDeviceId() {
        Context context = BaseApplication.getInstance();
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        String deviceId = manager.getDeviceId();
        // 处理极少设备取不到的情况
        if (TextUtils.isEmpty(deviceId)) {
            deviceId = Build.SERIAL;

//            if (TextUtils.isEmpty(deviceId)) {
//                deviceId = PreferenceHelper.getInstance().getString(Constant.DEVICE_ID, null);
//
//                if (TextUtils.isEmpty(deviceId)) {
//                    deviceId = UUID.randomUUID().toString();
//                    PreferenceHelper.getInstance().putString(Constant.DEVICE_ID, deviceId);
//                }
//            }
        }
        return deviceId;
    }

    /**
     * 获取手机制造商
     * @return 手机制造商
     */
    public static String getDeviceManufacturer() {
        return android.os.Build.MANUFACTURER;
    }

}
