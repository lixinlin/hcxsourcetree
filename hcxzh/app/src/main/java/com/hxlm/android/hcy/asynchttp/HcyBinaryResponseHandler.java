package com.hxlm.android.hcy.asynchttp;

import android.os.Handler;
import android.os.Message;
import com.hxlm.android.utils.Logger;
import com.loopj.android.http.BinaryHttpResponseHandler;
import org.apache.http.Header;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 下载型响应处理
 * <p/>
 * Created by Zhenyu on 2016/4/19.
 */
public class HcyBinaryResponseHandler extends BinaryHttpResponseHandler {
    private final static String[] ALLOWED_CONTENT_TYPES = new String[]{"audio/mpeg"};

    private static final int PROGRESS_REFRESH_RATE = 10;

    private final String tag = getClass().getSimpleName();

    private final AbstractHttpHandlerCallback abstractHttpHandlerCallback;
    private final Handler handler;
    private final String saveFileName;

    private int bytesWrittenLast = 0;

    public HcyBinaryResponseHandler(final String fileName, AbstractHttpHandlerCallback aHandler) {
        super(ALLOWED_CONTENT_TYPES);
        this.abstractHttpHandlerCallback = aHandler;
        this.handler = new Handler(aHandler);

        this.saveFileName = fileName;
    }

    @Override
    public void onSuccess(int i, Header[] headers, byte[] bytes) {
        if (createPath(saveFileName)) {

            BufferedOutputStream bos = null;

            try {
                bos = new BufferedOutputStream(new FileOutputStream(saveFileName));
                bos.write(bytes);
                bos.flush();
            } catch (IOException e) {
                Logger.e(tag, e);

            } finally {
                if (bos != null) {
                    try {
                        bos.close();
                    } catch (IOException e) {
                        Logger.e(tag, e);
                    }
                }
            }

            handler.sendEmptyMessage(AbstractHttpHandlerCallback.ON_RESPONSE_SUCCESS);

        } else {
            handler.obtainMessage(AbstractHttpHandlerCallback.ON_RESPONSE_ERROR, AbstractHttpHandlerCallback.ON_CREATE_PATH_FAIL, 0,
                    "Can not create local file path.").sendToTarget();
        }
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, byte[] bytes, Throwable e) {
        Logger.i(tag, "Http ERROR Response Code =" + statusCode);

        if (statusCode != 0) {
            Message.obtain(handler, AbstractHttpHandlerCallback.ON_HTTP_ERROR, statusCode, 0).sendToTarget();
        } else {
            handler.sendEmptyMessage(AbstractHttpHandlerCallback.ON_NETWORK_ERROR);
        }
    }

    @Override
    public void onProgress(int bytesWritten, int totalSize) {
        abstractHttpHandlerCallback.onProgress(bytesWritten, totalSize);
        if (((bytesWritten - bytesWrittenLast) * 100 / totalSize) > PROGRESS_REFRESH_RATE
                || bytesWritten == totalSize) {

            bytesWrittenLast = bytesWritten;

            handler.obtainMessage(AbstractHttpHandlerCallback.ON_PROGRESS, bytesWritten, totalSize).sendToTarget();
        }
    }

    /**
     * 创建本地文件存储需要的路径。
     *
     * @param filePath 本地文件的存储路径
     * @return boolean 路径创建成功返回true，失败返回false；
     */

    private boolean createPath(String filePath) {
        int cutPosition = filePath.lastIndexOf("/");
        String path = filePath.substring(0, cutPosition);
        File localPath = new File(path);

        return localPath.exists() || localPath.mkdirs();
    }
}
