package com.hxlm.android.hcy.asynchttp;

import com.loopj.android.http.RequestParams;

/**
 * 所有业务逻辑管理器的基类，用于提供一些公共的变量和方法
 * <p/>
 * Created by Zhenyu on 2015/8/22.
 */
public class HttpRequest {
    private final int method;
    private final String url;
    private final HcyHttpResponseHandler responseHandler;
    private RequestParams params;
    private String authType;

    public HttpRequest(int method, String url, RequestParams params,
                       HcyHttpResponseHandler responseHandler) {
        this.method = method;
        this.url = url;
        this.params = params;
        this.responseHandler = responseHandler;
    }

    public String getAuthType() {
        return authType;
    }

    public void setAuthType(String authType) {
        this.authType = authType;
    }

    public int getMethod() {
        return method;
    }

    public String getUrl() {
        return url;
    }

    public HcyHttpResponseHandler getResponseHandler() {
        return responseHandler;
    }

    public RequestParams getParams() {
        if(params == null){
            params = new RequestParams();
        }
        return params;
    }

    @Override
    public String toString() {
        String httpMethod = method == HcyHttpClient.METHOD_GET ? "GET" : "POST";
        return httpMethod + " : " + url + "\nParams : " + params.toString();
    }
}
