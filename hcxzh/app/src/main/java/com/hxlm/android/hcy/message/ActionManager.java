package com.hxlm.android.hcy.message;

import android.util.Log;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.android.utils.Logger;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.util.ACache;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.List;

/**
 * 指定系统发送的特殊指令
 * Created by Zhenyu on 2016/4/17.
 */
public class ActionManager {
    public static final String ACTION_CLEAR_CACHE = "QW12$12";
   // public static final String ACTION_CLEAR_CACHE = "QWHcyAndroid/cache";
    private static final String TAG = "Action";



    public void handleMessage(List<HcyMessage> hcyMessages) {
        for (int i = 0; i < hcyMessages.size(); i++) {

            HcyMessage hcyMessage = hcyMessages.get(i);

            if (HcyMessage.SEND_TYPE_SYSTEM.equals(hcyMessage.getSendType())) {

                String strContent=hcyMessage.getContent();
                //trim()是去掉首尾空格
                String strContentType=strContent.trim();

                Log.i("HcyHttpResponseHandler","strContent-->"+strContentType);

                if(ACTION_CLEAR_CACHE.equals(strContentType)){
                    hcyMessages.remove(i);
                    this.clearCache();

                }
            }
        }

        MessageManager messageManager = new MessageManager();
        if (hcyMessages != null && hcyMessages.size() > 0) {
            Log.i("HcyHttpResponseHandler","hcyMessages1111-->"+hcyMessages);
                //保存消息到Share
                messageManager.saveMessagesToSp(hcyMessages);

                for (WeakReference<TitleBarView> weakTitleBarview : Constant.titleBarViews) {
                    if (weakTitleBarview.get() != null) {
                        //更新当前titlebar消息图片
                        weakTitleBarview.get().refreshUnread(true);
                    }
                }
            }


    }

    public void clearCache(){
        File cache = new File(ACache.CACHE_PATH);

        if (cache.exists() && cache.isDirectory()) {
            File[] childFile = cache.listFiles();
            if (childFile != null && childFile.length > 0) {
                for (File f : childFile) {
                    if(!f.delete()){
                        Logger.i(TAG, "Delete file failed.. File Name : "+ f.getName());
                    }
                }
            }
        }
    }
}
