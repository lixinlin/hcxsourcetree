package com.hxlm.android.hcy.order;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.content.ResourceItem;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.view.ConfirmDialog;

/**
 * 购买乐药结算的列表的基本功能
 * <p/>
 * 
 */
public class GouwucheListAdapter extends BaseAdapter {
    private final LayoutInflater mInflater;
    private List<ResourceItem> mDatas;
    private int typeIndex;
    private ConfirmDialog deleteDialog;
    private final Context mContext;

    public GouwucheListAdapter(Context context) {
    	mContext = context;
        this.mInflater = LayoutInflater.from(context);
    }

    public void setDatas(List<ResourceItem> datas) {
        this.mDatas = datas;
    }

    @Override
    public int getCount() {
        if (mDatas == null) {
            return 0;
        } else {
            return mDatas.size();
        }
    }

    @Override
    public ResourceItem getItem(int position) {
        if (mDatas == null || mDatas.size() < position) {
            return null;
        }
        return mDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mDatas.get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.activity_yueyao_gouwuche_list_item, parent, false);

            holder = new ViewHolder();

            holder.qumingIcon = (ImageView) convertView.findViewById(R.id.quming_icon);
            holder.quMingText = (TextView) convertView.findViewById(R.id.quming_text);
            holder.goumaiText = (TextView) convertView.findViewById(R.id.goumai_text);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ResourceItem item = getItem(position);
        if (item == null) {
            return convertView;
        }

        holder.quMingText.setText(item.getName());
        holder.goumaiText.setText("￥" + Constant.decimalFormat.format(item.getPrice()));

        switch (typeIndex) {
            case 0:
                holder.qumingIcon.setImageResource(R.drawable.yueyao_gong);
                break;
            case 1:
                holder.qumingIcon.setImageResource(R.drawable.yueyao_shang);
                break;
            case 2:
                holder.qumingIcon.setImageResource(R.drawable.yueyao_jue);
                break;
            case 3:
                holder.qumingIcon.setImageResource(R.drawable.yueyao_zhi);
                break;
            case 4:
                holder.qumingIcon.setImageResource(R.drawable.yueyao_yu);
                break;
            default:
                break;
        }

        return convertView;
    }
    private void showDeleteDialog() {
        if (deleteDialog == null) {
            deleteDialog = new ConfirmDialog(mContext);

            deleteDialog.setTextResourceId(R.string.upload_del);

            deleteDialog.setCancelListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteDialog.dismiss();
                }
            });

            // 确认
            deleteDialog.setOkListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    deleteFile(typeIndex);
                    deleteDialog.dismiss();
                }
            });
        }
        deleteDialog.show();
    }

    protected void deleteFile(int typeIndex) {
    	mDatas.remove(typeIndex);
        notifyDataSetChanged();
	}

	private class ViewHolder {
        public ImageView qumingIcon;
        public TextView quMingText;
        public TextView goumaiText;
        public ImageView shanchuIcon;
    }
    
}