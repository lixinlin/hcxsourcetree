package com.hxlm.android.hcy.questionnair;

import android.util.Log;
import com.alibaba.fastjson.JSON;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.AbstractHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.asynchttp.HcyHttpResponseHandler;
import com.hxlm.android.hcy.report.Report;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.utils.Logger;
import com.loopj.android.http.RequestParams;

import java.util.*;

/**
 * 问卷
 * Created by Zhenyu on 2016/4/8.
 */
public class QuestionnaireManager {
    public static final String SN_TZBS = "TZBS";
    public static final String SN_SAS20 = "SAS20";// 焦虑
    public static final String SN_SDS20 = "SDS20";// 抑郁

    private final String tag = getClass().getSimpleName();

    private List<Questionnaire> questionnaires;// 问卷列表的集合
    private HashMap<String, Question> mergedQuestions = new HashMap<>();//滤重后的问题
    private List<Question> duplicateQuestions = new ArrayList<>(); // 重复的问题
    private List<Answer> defaultAnswers;  //默认答案列表

    /**
     * 根据sn编码获取相应的问题及答案
     *
     * @param sn 类型编码
     */
    public void getQuestionnaires(final String sn, AbstractHttpHandlerCallback callback) {
        String url = "/questionnaire/lists.jhtml";
        RequestParams params = new RequestParams();
        params.put("sn", sn);

        HcyHttpClient.submit(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            public Object contentParse(String content) {
                questionnaires = JSON.parseArray(content, Questionnaire.class);

                Log.i("Log.i","content-->"+content);


                List<Question> questionsToBeShow = new ArrayList<>();

                for (Questionnaire questionnaire : questionnaires) {
                    defaultAnswers = questionnaire.getAnswer();
                    List<Question> questions = questionnaire.getQuestion();

                    for (Question question : questions) {
                        if (!mergedQuestions.containsKey(question.getName())) {
                            mergedQuestions.put(question.getName(), question);
                            questionsToBeShow.add(question);
                        } else {
                            duplicateQuestions.add(question);
                        }
                    }
                }
                Collections.sort(questionsToBeShow);//进行排序，60道题按order排序
                return questionsToBeShow;
            }
        });
    }

    /**
     * 保存问卷答案接口
     *
     * @param subjectSn 辨识后结论（主题）编码
     */
    public void saveAnswersAndGetReport(String subjectSn, AbstractDefaultHttpHandlerCallback callback) {
        String resultJsonString = "";// 上传问题和答案接口所需的参数
        String questionnaireIds = "";// 上传问题和答案接口所需的参数
        String tzscore = "";//每种体质得分；
        for (Questionnaire questionnaire : questionnaires) {
            Map<Integer, Integer> result = new HashMap<>();
            int questionnaireId = questionnaire.getId();

            for (Question question : questionnaire.getQuestion()) {
                result.put(question.getId(), question.getAnswer().getId());
            }

            questionnaireIds = questionnaireIds + "," + questionnaireId;
            //resultJsonString  = 问题id+答案id
            resultJsonString = resultJsonString + ";" + JSON.toJSONString(result);
            tzscore = tzscore +questionnaire.getSubjectSn() + ":" + questionnaire.getTotalScore()+";";
//            Log.d(tag, "saveAnswersAndGetReport: "+tzscore);
        }
        questionnaireIds = questionnaireIds.substring(1, questionnaireIds.length());
        resultJsonString = resultJsonString.substring(1, resultJsonString.length());

        String url = "/member/myreport/save_report.jhtml";
        //---------------------------新加
        int memberChildId=LoginControllor.getChoosedChildMember().getId();
        //---------------------------新加

        RequestParams params = new RequestParams();
        params.put("memberChildId",memberChildId);//子账户

        params.put("jsonResults", resultJsonString);
        params.put("questionnaireIds", questionnaireIds);
        params.put("subjectSn", subjectSn);
        params.put("tzscore",tzscore);
        Log.e("retrofit","==一点提交参数memberChildId=="+memberChildId);
        Log.e("retrofit","==一点提交参数jsonResults=="+resultJsonString);
        Log.e("retrofit","==一点提交参数questionnaireIds=="+questionnaireIds);
        Log.e("retrofit","==一点提交参数subjectSn=="+subjectSn);
        Log.e("retrofit","==一点提交参数tzscore=="+tzscore);

        HcyHttpClient.submit(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                return JSON.parseObject(content, Report.class);
            }
        });

    }

    /**
     * 获得问卷的结果
     *
     * @param categorySn 评估结论分类编码
     */
    public Result getResult(String categorySn) {
        computeRawScore();
        Result result = null;
        switch (categorySn) {
            case SN_TZBS:
                //体质辨识
                result = getTZBSResult();
                break;
            default:
                break;
        }
        return result;
    }

    private Result getTZBSResult() {
        int pinghezhi = 0;
        int maxPianbo = 0;
        // 体质辨识
        for (Questionnaire questionnaire : questionnaires) {
            int size = questionnaire.getQuestion().size();
            int rawScore = questionnaire.getTotalScore();  //标准分
            questionnaire.setTotalScore(Math.round((float) (rawScore - size) / (size * 4) * 100));
            //保留了平和质和最大的偏颇体质得分
            if ("TZBS-01".equals(questionnaire.getSubjectSn())) {
                pinghezhi = questionnaire.getTotalScore();
            } else if (questionnaire.getTotalScore() > maxPianbo) {
                maxPianbo = questionnaire.getTotalScore();
            }
            Logger.i(tag, questionnaire.getName() + "," + questionnaire.getSubjectSn() + "=" + questionnaire.getTotalScore());
        }

        List<Questionnaire> maxQuestionnairs = new ArrayList<>();

        for (Questionnaire questionnare : questionnaires) {
            if (!"TZBS-01".equals(questionnare.getSubjectSn()) && questionnare.getTotalScore() == maxPianbo) {
                maxQuestionnairs.add(questionnare);
            }
        }

        Result result = new Result(SN_TZBS, maxPianbo);
        if (pinghezhi >= 60 && maxPianbo < 40) {
            result.setSubjectSn("TZBS-01");
        } else if (maxQuestionnairs.size() == 1) {
            result.setSubjectSn(maxQuestionnairs.get(0).getSubjectSn());
        } else {
            result.setSubjectSn(getSnByRule(maxQuestionnairs));
        }
        Logger.i(tag, "结论是：" + result.getSubjectSn());
        return result;
    }

    // 如果得分相同，根据规则，取出相应的体质报告
    private String getSnByRule(List<Questionnaire> maxQuestionnairs) {
        final String[] tizhi = {"TZBS-02", "TZBS-03", "TZBS-04", "TZBS-05", "TZBS-06", "TZBS-07", "TZBS-08", "TZBS-09"};
        int minTizhi = 100;

        for (Questionnaire questionnaire : maxQuestionnairs) {
            for (int i = 0; i < tizhi.length; i++) {
                if (tizhi[i].equals(questionnaire.getSubjectSn())) {
                    if (i < minTizhi) {
                        minTizhi = i;
                    }
                    break;
                }
            }
        }

        return tizhi[minTizhi];
    }

    private void computeRawScore() {
        for (Question question : duplicateQuestions) {
            question.setAnswer(mergedQuestions.get(question.getName()).getAnswer());
        }

        for (Questionnaire questionnaire : questionnaires) {
            int score = 0;
            for (Question question : questionnaire.getQuestion()) {
                if (!question.isReverse()) {
                    // 正常计分
                    score += question.getAnswer().getFraction();
                } else {
                    // 反向计分
                    List<Answer> theAnswers;
                    if (question.getAnswers() != null) {
                        theAnswers = question.getAnswers();
                        Collections.sort(theAnswers);
                    } else {
                        theAnswers = questionnaire.getAnswer();
                        Collections.sort(theAnswers);
                    }
                    int reverseAnswerIndex = theAnswers.size() - question.getAnswer().getOrder();
                    score += theAnswers.get(reverseAnswerIndex).getFraction();
                }
            }
            questionnaire.setTotalScore(score);
        }
    }

    public List<Answer> getDefaultAnswers() {
        return defaultAnswers;
    }
}
