package com.hxlm.android.hcy.questionnair;

/**
 * 问卷中的答案
 * 
 * @author Administrator
 *
 */
public class Answer implements Comparable<Answer>{
	private int id;// 答案id
	private long createDate;// 生成时间
	private long modifyDate;// 修改时间
	private int order;// 答案排序
	private String content;// 答案内容
	private int fraction;// 答案分值

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getCreateDate() {
		return createDate;
	}

	public void setCreateDate(long createDate) {
		this.createDate = createDate;
	}

	public long getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(long modifyDate) {
		this.modifyDate = modifyDate;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getFraction() {
		return fraction;
	}

	public void setFraction(int fraction) {
		this.fraction = fraction;
	}

	@Override
	public String toString() {
		return "Answer [id=" + id + ", createDate=" + createDate
				+ ", modifyDate=" + modifyDate + ", order=" + order
				+ ", content=" + content + ", fraction=" + fraction + "]";
	}

	public String toString2() {
		return "Answer [id=" + id +  ", order=" + order
				+ ", content=" + content + ", fraction=" + fraction + "]";
	}

	@Override
	public int compareTo(Answer another) {
		return this.order - another.order;
	}
}
