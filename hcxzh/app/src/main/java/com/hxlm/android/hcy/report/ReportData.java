package com.hxlm.android.hcy.report;

import java.util.List;
import java.util.Map;

public class ReportData {
	private List<String> titles;
	private Map<String, String> map;

	public ReportData(List<String> titles, Map<String, String> map) {
		super();
		this.titles = titles;
		this.map = map;
	}

	public List<String> getTitles() {
		return titles;
	}

	public void setTitles(List<String> titles) {
		this.titles = titles;
	}

	public Map<String, String> getMap() {
		return map;
	}

	public void setMap(Map<String, String> map) {
		this.map = map;
	}

}
