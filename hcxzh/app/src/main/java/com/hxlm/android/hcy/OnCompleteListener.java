package com.hxlm.android.hcy;

/**
 * 动作完成的回调接口
 * Created by Zhenyu on 2016/4/18.
 */
public interface OnCompleteListener {
    void onComplete();
}
