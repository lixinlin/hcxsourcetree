package com.hxlm.android.hcy.order.card;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.google.gson.Gson;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.bean.CardRecordBean;
import com.hxlm.android.hcy.bean.CardServiceBean;
import com.hxlm.android.hcy.order.CardManager;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.util.ToastUtil;

import org.w3c.dom.Text;

import java.util.List;

/**
 * @author Lirong
 * @date 2018/12/5 0005.
 * @description 激活的卡详情
 */

public class CardDetailsActivity extends BaseActivity implements View.OnClickListener {

    private TextView tvTitle;
    private TextView tvIntroduce;

    private RecyclerView rvRecord;
    private RecyclerView rvCardDetails;
    private String card_no;
    private String card_name;
    private TextView tv_service;
    private TextView tv_record;
    private Intent intent;
    private ImageView iv_record;
    private Dialog dialog;
    private List<CardRecordBean> cardRecordBeans;
    private Button btn_add_card_package;

    private CardManager cardManager;
    private int tag;
    private TextView tv_card_details_num;
    private String card_desc;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_card_details);
    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.kapianxinxi), titleBar, 1);
        //初始化控件
        tv_service = findViewById(R.id.tv_service);
        tv_record = findViewById(R.id.tv_record);
        tvTitle = findViewById(R.id.tv_card_details_title);
        tv_card_details_num = findViewById(R.id.tv_card_details_num);
        tvIntroduce = findViewById(R.id.tv_card_details_introduce);
        rvCardDetails = findViewById(R.id.rv_card_details);
        rvRecord = findViewById(R.id.rv_card_details_record);
        iv_record = findViewById(R.id.iv_record);

        btn_add_card_package = findViewById(R.id.btn_add_card_package);
        btn_add_card_package.setOnClickListener(this);

        iv_record.setOnClickListener(this);

        //获取卡号
        intent = getIntent();
        card_name = intent.getStringExtra("CARD_NAME1");
        card_no = intent.getStringExtra("CARD_NO1");
        card_desc = intent.getStringExtra("CARD_DESC1");

        //设置标题
        tvTitle.setText(card_name);
        if (!TextUtils.isEmpty(card_no)) {
            tv_card_details_num.setText(getString(R.string.card_details_card_no)+card_no);
        }else {
            tv_card_details_num.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(card_desc)){
            tvIntroduce.setText(card_desc);
        }else{
            tvIntroduce.setVisibility(View.GONE);
        }

        //剩余服务
        rvCardDetails.setLayoutManager(new LinearLayoutManager(this));

        //消费记录
        rvRecord.setLayoutManager(new LinearLayoutManager(this));
        rvRecord.setHasFixedSize(true);
        rvRecord.setNestedScrollingEnabled(false);
    }

    @Override
    public void initDatas() {
        cardManager = new CardManager();
        intent = getIntent();
        card_no = intent.getStringExtra("CARD_NO1");
        new CardManager().cardRecord(card_no,new AbstractDefaultHttpHandlerCallback(this) {
            @Override
            protected void onResponseSuccess(Object obj) {
                JSONArray jsonObject = JSON.parseArray((String) obj);
                //data   集合1：剩余服务    集合2：消费记录
                //剩余服务
                List<CardServiceBean> cardServiceBeans = JSON.parseArray(new Gson().toJson(jsonObject.get(0)), CardServiceBean.class);
                if (cardServiceBeans.size() != 0) {
                    tv_service.setVisibility(View.VISIBLE);
                    rvCardDetails.setVisibility(View.VISIBLE);
                    CardDetailsAdapter cardDetailsAdapter = new CardDetailsAdapter(CardDetailsActivity.this, cardServiceBeans);
                    rvCardDetails.setAdapter(cardDetailsAdapter);
                }else {
                    tv_service.setVisibility(View.GONE);
                    rvCardDetails.setVisibility(View.GONE);
                }
                Log.e("retrofit","剩余服务：："+cardServiceBeans.toString());
                //消费记录
                List<CardRecordBean> cardRecordBeans = JSON.parseArray(new Gson().toJson(jsonObject.get(1)), CardRecordBean.class);
                if (cardRecordBeans.size() != 0) {
                    tv_record.setVisibility(View.VISIBLE);
                    rvRecord.setVisibility(View.VISIBLE);
                    ConsumptionRecordsAdapter consumptionRecordsAdapter = new ConsumptionRecordsAdapter(CardDetailsActivity.this, cardRecordBeans);
                    rvRecord.setAdapter(consumptionRecordsAdapter);
                }else {
                    tv_record.setVisibility(View.GONE);
                    rvRecord.setVisibility(View.GONE);
                }
                Log.e("retrofit","消费记录：："+cardRecordBeans.toString());
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_record:

                intent = getIntent();
                card_no = intent.getStringExtra("CARD_NO1");
                new CardManager().cardRecord(card_no,new AbstractDefaultHttpHandlerCallback(this) {
                    @Override
                    protected void onResponseSuccess(Object obj) {
                        JSONArray jsonObject = JSON.parseArray((String) obj);
                       //data   集合1：剩余服务    集合2：消费记录
                        //消费记录
                        cardRecordBeans = JSON.parseArray(new Gson().toJson(jsonObject.get(1)), CardRecordBean.class);
                        Log.e("retrofit","====卡片信息消费记录==="+cardRecordBeans.size()+"===="+cardRecordBeans.toString());
                        if (cardRecordBeans.size() != 0) {
                            View view = LayoutInflater.from(CardDetailsActivity.this).inflate(R.layout.dialog_record, null);
                            dialog = new Dialog(CardDetailsActivity.this, R.style.dialog);
                            dialog.setContentView(view);
                            dialog.setCanceledOnTouchOutside(false);
                            ImageView iv_delete = view.findViewById(R.id.iv_delete);
                            RecyclerView rv_card_record = view.findViewById(R.id.rv_card_record);
                            iv_delete.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });
                            //消费记录
                            rv_card_record.setLayoutManager(new LinearLayoutManager(CardDetailsActivity.this));
                            rv_card_record.setHasFixedSize(true);
                            rv_card_record.setNestedScrollingEnabled(false);
                            ConsumptionRecordsAdapter consumptionRecordsAdapter = new ConsumptionRecordsAdapter(CardDetailsActivity.this, cardRecordBeans);
                            rv_card_record.setAdapter(consumptionRecordsAdapter);
                            dialog.show();

                        }else {
                            ToastUtil.invokeShortTimeToast(CardDetailsActivity.this,getString(R.string.ftj_card_zanwurecord));
                        }
                        Log.e("retrofit","消费记录：："+ cardRecordBeans.toString());
                    }
                });


                break;
            //添加至卡包
            case R.id.btn_add_card_package:
                intent = getIntent();
                card_no = intent.getStringExtra("CARD_NO1");
                cardManager.bindCard(card_no, new AbstractDefaultHttpHandlerCallback(this) {
                    @Override
                    protected void onResponseSuccess(Object obj) {
                        ToastUtil.invokeShortTimeToast(CardDetailsActivity.this,getString(R.string.input_add_card_tips_success));
                        CardDetailsActivity.this.finish();
                    }

                });
                break;
            default:
                break;
        }
    }
}
