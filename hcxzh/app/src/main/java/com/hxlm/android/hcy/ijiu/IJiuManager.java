package com.hxlm.android.hcy.ijiu;

import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.asynchttp.HcyHttpResponseHandler;
import com.jiudaifu.moxademo.jiuliaoData.ActiviteInfo;
import com.jiudaifu.moxademo.jiuliaoData.ErrorLog;
import com.jiudaifu.moxademo.jiuliaoData.I9DataHttp;
import com.jiudaifu.moxademo.jiuliaoData.I9DataJDF;
import com.loopj.android.http.RequestParams;

import java.util.List;

/**
 * Created by lixinlin on 2018/10/25.
 */

public class IJiuManager {

    private static final String TAG ="IJiuManager" ;

    public void sumbitActivitedData(ActiviteInfo activiteInfo, AbstractDefaultHttpHandlerCallback callback){
            String url = "/member/mox/activite.jhtml";
            RequestParams params = new RequestParams();
            params.put("member",activiteInfo.getMember());
            params.put("deviceSn",activiteInfo.getDeviceSn());
            params.put("activiteStatus",activiteInfo.getActiviteStatus());
            params.put("activiteTime",activiteInfo.getActiviteTime());
            HcyHttpClient.submitNoPre(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
                @Override
                protected Object contentParse(String content) {
                    Log.d(TAG, "contentParse: "+content);
                    return content;
                }
            });

    }

    public void sumbitDatatest(List<I9DataHttp> i9Data, AbstractDefaultHttpHandlerCallback callback){
        String dataJsonString = JSON.toJSONString(i9Data);
        String url = "/member/mox/moxdataupload.jhtml";
        RequestParams params = new RequestParams();
        String member = i9Data.get(0).getMember();
        params.put("member",member);
        params.put("data",dataJsonString);
        Log.d(TAG, "dataJsonString: "+dataJsonString);
        HcyHttpClient.submitNoPre(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                return content;
            }
        });
    }

    public void sumbitDataforjiudaifutest(List<I9DataJDF> i9Data, AbstractDefaultHttpHandlerCallback callback){
        String dataJsonString = JSON.toJSONString(i9Data);
        String url = "/member/mox/moxinfoupload.jhtml";
        RequestParams params = new RequestParams();
        String member = i9Data.get(0).getMember();
        params.put("member",member);
        params.put("data",dataJsonString);
        HcyHttpClient.submitNoPre(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                return content;
            }
        });
    }

    public void sumbitIJiuError(List<ErrorLog> errorLog, AbstractDefaultHttpHandlerCallback callback){
        String dataJsonString = JSON.toJSONString(errorLog);
        String url = "/member/mox/moxexceptiondataupload.jhtml";
        RequestParams params = new RequestParams();
        String member = errorLog.get(0).getMember();
        params.put("member",member);
        params.put("data",dataJsonString);
        HcyHttpClient.submitNoPre(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                return content;
            }
        });
    }

}
