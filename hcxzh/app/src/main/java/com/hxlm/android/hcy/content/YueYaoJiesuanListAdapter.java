package com.hxlm.android.hcy.content;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.callback.MyCallBack.OnChangeListener;
import com.hxlm.hcyandroid.view.ConfirmDialog;

/**
 * 购买乐药结算的列表的基本功能
 * <p/>
 * 
 */
public class YueYaoJiesuanListAdapter extends BaseAdapter {
    private final LayoutInflater mInflater;
    private List<ResourceItem> mDatas;
    private int typeIndex;
    private ConfirmDialog deleteDialog;
    private final Context mContext;
    private OnChangeListener onChangelistener;

    public YueYaoJiesuanListAdapter(Context context, OnChangeListener onChangeListener) {
    	mContext = context;
    	onChangelistener = onChangeListener;
        this.mInflater = LayoutInflater.from(context);
    }

    public void setDatas(List<ResourceItem> datas) {
        this.mDatas = datas;
    }

    @Override
    public int getCount() {
        if (mDatas == null) {
            return 0;
        } else {
            return mDatas.size();
        }
    }

    @Override
    public ResourceItem getItem(int position) {
        if (mDatas == null || mDatas.size() < position) {
            return null;
        }
        return mDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mDatas.get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.activity_yueyao_jiesuan_list_item, parent, false);

            holder = new ViewHolder();

            holder.qumingIcon = (ImageView) convertView.findViewById(R.id.quming_icon);
            holder.quMingText = (TextView) convertView.findViewById(R.id.quming_text);
            holder.goumaiText = (TextView) convertView.findViewById(R.id.goumai_text);
            holder.shanchuIcon = (ImageView) convertView.findViewById(R.id.jiesuan_shanchu);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ResourceItem item = getItem(position);
        if (item == null) {
            return convertView;
        }

        holder.quMingText.setText(item.getName());
        holder.goumaiText.setText("￥" + Constant.decimalFormat.format(item.getPrice()));
        holder.shanchuIcon.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				typeIndex = position;
				showDeleteDialog();
			}
		});
        
        switch (typeIndex) {
            case 0:
                holder.qumingIcon.setImageResource(R.drawable.yueyao_gong);
                break;
            case 1:
                holder.qumingIcon.setImageResource(R.drawable.yueyao_shang);
                break;
            case 2:
                holder.qumingIcon.setImageResource(R.drawable.yueyao_jue);
                break;
            case 3:
                holder.qumingIcon.setImageResource(R.drawable.yueyao_zhi);
                break;
            case 4:
                holder.qumingIcon.setImageResource(R.drawable.yueyao_yu);
                break;
        }

        return convertView;
    }
    private void showDeleteDialog() {
        if (deleteDialog == null) {
            deleteDialog = new ConfirmDialog(mContext);

            deleteDialog.setTextResourceId(R.string.upload_del);

            deleteDialog.setCancelListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteDialog.dismiss();
                }
            });

            // 确认
            deleteDialog.setOkListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    deleteFile(typeIndex);
                    deleteDialog.dismiss();
                }
            });
        }
        deleteDialog.show();
    }

    //删除乐药
    protected void deleteFile(int typeIndex) {

        //得到要删除的乐药,并保存到另一个List集合里
        ResourceItem itemDetele=mDatas.get(typeIndex);
        Constant.LIST_DELETE_YUEYAO.add(itemDetele);

    	mDatas.remove(typeIndex);
    	onChangelistener.onChange();
        notifyDataSetChanged();
	}

	private class ViewHolder {
        public ImageView qumingIcon;
        public TextView quMingText;
        public TextView goumaiText;
        public ImageView shanchuIcon;
    }
    
}