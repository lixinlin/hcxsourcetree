package com.hxlm.android.hcy.message;

public class HcyMessage implements Comparable<HcyMessage> {
    public static final String TYPE_ONE = "one";
    public static final String TYPE_ALL = "all";
    public static final String SEND_TYPE_BESPOKE = "bespoke"; //预约挂号
    public static final String SEND_TYPE_VIDEO = "video"; //视频预约
    public static final String SEND_TYPE_LECTURE = "lecture"; //讲座
    public static final String SEND_TYPE_SYSTEM = "system"; //系统消息

    private int id;
    private long modifyDate;
    private long createDate;
    private String title;
    private String sendType;
    private String type;
    private String content;

    private int order;
    private boolean isRead = false;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(long modifyDate) {
        this.modifyDate = modifyDate;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSendType() {
        return sendType;
    }

    public void setSendType(String sendType) {
        this.sendType = sendType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean isRead) {
        this.isRead = isRead;
    }

    @Override
    public String toString() {
        return "MyMessage [id=" + id + ", modifyDate=" + modifyDate
                + ", createDate=" + createDate + ", title=" + title
                + ", sendType=" + sendType + ", type=" + type + ", content="
                + content + "]";
    }

    @Override
    public int compareTo(HcyMessage another) {
        return another.id - this.id;
    }
}
