package com.hxlm.android.hcy.utils;

import android.content.Context;
import android.util.Log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by lixinlin on 2018/8/2.
 */

public class CrashHandler implements Thread.UncaughtExceptionHandler {
    public static final String TAG = "CrashHandler";
    private static CrashHandler INSTANCE = new CrashHandler();
    private Context mContext;

    private CrashHandler() {
    }

    public static CrashHandler getInstance() {
        return INSTANCE;
    }

    public void init(Context context) {
        mContext = context;
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        Log.e(TAG, "uncaughtException: ",ex);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日HHmmss");
        Date date = new Date(System.currentTimeMillis());
        LogToFile logToFile = new LogToFile();
        String exStr = logToFile.saveCrashInfo2File(ex);
//        ACache.putError("E"+simpleDateFormat.format(date),exStr);
//        new Thread() {
//            @Override
//            public void run() {
//                Intent intent = new Intent(mContext, MainActivity.class);
//                PendingIntent restartIntent = PendingIntent.getActivity(mContext, 0, intent, Intent.FLAG_ACTIVITY_NEW_TASK);
//                AlarmManager mgr = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
//                mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 1000, restartIntent);
//                android.os.Process.killProcess(android.os.Process.myPid());
//            }
//        }.start();
    }

    public String getCrashInfo(Throwable ex) {
        StringBuffer sb = new StringBuffer();
        Writer writer = new StringWriter();
        PrintWriter printWriter = new PrintWriter(writer);
        ex.printStackTrace(printWriter);
        Throwable cause = ex.getCause();
        while (cause != null) {
            cause.printStackTrace(printWriter);
            cause = cause.getCause();
        }
        printWriter.close();
        String result = writer.toString();
        sb.append(result);
        return sb.toString();
    }
}