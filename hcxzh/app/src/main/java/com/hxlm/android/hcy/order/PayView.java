package com.hxlm.android.hcy.order;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.hcyandroid.BaseApplication;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.PaySuccessActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * 支付界面
 * Created by Zhenyu on 2016/4/21.
 */
public class PayView extends LinearLayout {
    private final static String TOTAL_PAY_PRIFIX = BaseApplication.getContext().getString(R.string.pay_view_haixuzhifu);

    private final TextView tvTotalToPay;
    private final TextView tvPay;
    private TextView tvMoneySum;
    private TextView tvCardToPay;
    private PayManager payManager;
    private int reserveType;

    private double moneySum;
    private double moneyCardPay;
    private String productDesc;

    private String itemIdsStr;

    private int orderIdPay;//订单号

    private List<Card> cardsToPay=new ArrayList<>();

    private OnCompleteListener onCompleteListener;

    public PayView(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater.from(context).inflate(R.layout.pay_click, this);

        tvTotalToPay = (TextView) findViewById(R.id.tv_total_to_pay);

        tvPay = (TextView) findViewById(R.id.tv_pay);

        tvMoneySum = (TextView) findViewById(R.id.tv_money_to_pay);
        tvCardToPay = (TextView) findViewById(R.id.tv_card_to_pay);
    }

    //直接传递orderIdPay过来
    public void init(final int reserveType,double moneySum,String aItemIdsStr,String aProductDesc,int orderIdPay)
    {
        init(reserveType,moneySum,aItemIdsStr,aProductDesc);
        this.orderIdPay=orderIdPay;
    }


    //基本的根据itemIdsStr得到orderId
    public void init(final int reserveType,double moneySum,String aItemIdsStr,String aProductDesc){
        payManager = new PayManager();
        this.reserveType = reserveType;
        this.moneySum = moneySum;
        itemIdsStr = aItemIdsStr;
        this.productDesc = aProductDesc;

        tvMoneySum.setText(String.format("%s%s", Constant.MONEY_PREFIX, Constant.decimalFormat.format(moneySum)));
        tvTotalToPay.setText(String.format("%s%s", TOTAL_PAY_PRIFIX, Constant.decimalFormat.format(moneySum)));

        tvPay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {



                Log.i("Log.i","orderIdPay-->"+orderIdPay);
                //判读如果直接传递过来的orderId不为空
                if(orderIdPay>0&&reserveType > 0)
                {
                    Log.i("Log.i","orderIdPayOK-->");
                    //直接生成订单
                    pay(orderIdPay);
                }else
                {
                    if (!TextUtils.isEmpty(itemIdsStr) && reserveType > 0) {
                        payManager.getOrderId(itemIdsStr, new AbstractDefaultHttpHandlerCallback(getContext()) {
                            @Override
                            protected void onResponseSuccess(Object obj) {

                                pay((Integer) obj);
                            }
                        });
                    }
                }



            }
        });
    }

    private void pay(int orderId) {
        //卡支付
        if ((moneySum - moneyCardPay) == 0) {

            payManager.createOrder(reserveType, orderId, "payment", null,
                    payManager.getCardsPayInfoStr(cardsToPay), 0, false, null, null,
                    new AbstractDefaultHttpHandlerCallback(getContext()) {
                        @Override
                        protected void onResponseSuccess(Object obj) {
                            onCompleteListener.onComplete();
                            Constant.LIST.clear();
                            Intent intent = new Intent(getContext(), PaySuccessActivity.class);
                            getContext().startActivity(intent);

                        }
                    });
        } else {
            otherPay(orderId);
        }
    }

    //其他支付
    private void otherPay(int orderId) {
        Intent intent = new Intent(getContext(), OtherPayActivity.class);
        intent.putExtra("orderId", orderId);// 订单号
        intent.putExtra("reserveType", reserveType);// 预订的服务类型
        intent.putExtra("desc", productDesc);// 商品描述
        intent.putExtra("type", "payment");// 交易类型，payment 订单支付
        intent.putExtra("balance", (moneySum - moneyCardPay));// 余额

        intent.putExtra("cardFees", payManager.getCardsPayInfoStr(cardsToPay));

        getContext().startActivity(intent);

        onCompleteListener.onComplete();
    }

    double getMoneyRemain(){
        return moneySum - moneyCardPay;
    }

    void addCardToPay(Card card) {
        if(cardsToPay == null){
            cardsToPay = new ArrayList<>();
        }

        this.cardsToPay.add(card);
        if (moneyCardPay >= moneySum) {
            Toast.makeText(getContext(), BaseApplication.getContext().getString(R.string.payview_tips_no_pay_please_cancel)
                    , Toast.LENGTH_LONG).show();
        } else {
            card.setChoosed(true);

            double moneyRemain = moneySum - moneyCardPay;
            if (moneyRemain <= card.getBalance()) {
                card.setDeductMoney(moneyRemain);
                moneyCardPay += moneyRemain;

            } else {
                card.setDeductMoney(card.getBalance());
                moneyCardPay += card.getBalance();

            }

            tvCardToPay.setText(String.format("%s%s", Constant.MONEY_PREFIX, Constant.decimalFormat.format(moneyCardPay)));
            tvTotalToPay.setText(String.format("%s%s", TOTAL_PAY_PRIFIX, Constant.decimalFormat.format(moneySum - moneyCardPay)));
        }
    }

    void removeCardToPay(Card aCard) {
        moneyCardPay -= aCard.getDeductMoney();

        aCard.setChoosed(false);
        aCard.setDeductMoney(0);

        for (int i = 0; i < cardsToPay.size(); i++) {
            if(cardsToPay.get(i).getId() == aCard.getId()){
                cardsToPay.remove(i);
            }
        }

        tvCardToPay.setText(String.format("%s%s", Constant.MONEY_PREFIX, Constant.decimalFormat.format(moneyCardPay)));
        tvTotalToPay.setText(String.format("%s%s", TOTAL_PAY_PRIFIX, Constant.decimalFormat.format(moneySum - moneyCardPay)));
    }

    public void setOnCompleteListener(OnCompleteListener onCompleteListener) {
        this.onCompleteListener = onCompleteListener;
    }
}
