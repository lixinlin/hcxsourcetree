package com.hxlm.android.hcy.content;

import android.os.Handler;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.HcyBinaryResponseHandler;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.asynchttp.HcyHttpResponseHandler;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.datamanager.BaseManager;
import com.loopj.android.http.RequestParams;

import java.io.File;
import java.util.List;

/**
 * 示范音功能
 */
class ShiFanYinManager extends BaseManager {
    static final int REFRESH_LIST_VIEW = 9;

    ShiFanYinManager(Handler handler) {
        super(handler);
    }

    void getOrderedItems(final AbstractDefaultHttpHandlerCallback callback) {
        final String url = "/resources/list.jhtml?sn=ZY-YDSFY";

        RequestParams params = new RequestParams();

        HcyHttpClient.submit(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {

            @Override
            protected Object contentParse(String content) {
                JSONObject jsonObject = JSON.parseObject(content);
                List<ResourceItem> items = null;
                if (jsonObject != null) {
                    items = JSON.parseArray(jsonObject.getString("content"), ResourceItem.class);

                    if (items != null) {
                        for (ResourceItem resourceItem : items) {
                            resourceItem.setItemStatus(ResourceItem.NOT_DOWNLOAD);
                        }
                        processYueYaoItems(items);
                    }
                }
                return items;
            }
        });
    }

    private void processYueYaoItems(List<ResourceItem> items) {
        for (ResourceItem resourceItem : items) {
            if (resourceItem.getResourcesWarehouses().size() > 0) {
                resourceItem.setSource(resourceItem.getResourcesWarehouses().get(0).getSource());
            }

            String localStoragePath = getItemLocalPath(resourceItem);
            File yueyaoFile = new File(localStoragePath);

            if (yueyaoFile.exists()) {
                resourceItem.setItemStatus(ResourceItem.STOP);
                resourceItem.setLocalStoragePath(localStoragePath);
            }
        }
    }

    void downloadItemFile(final ResourceItem item, final AbstractDefaultHttpHandlerCallback callback) {
        item.setLocalStoragePath(getItemLocalPath(item));
        HcyHttpClient.download(item.getSource(), new HcyBinaryResponseHandler(getItemLocalPath(item), callback));
    }

    private String getItemLocalPath(ResourceItem item) {
        return Constant.SHIFANYIN_PATH + "/" + item.getResourcesWarehouses().get(0).getTitle() + ".mp3";
    }
}
