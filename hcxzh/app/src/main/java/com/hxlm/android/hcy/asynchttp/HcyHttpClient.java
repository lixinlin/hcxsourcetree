package com.hxlm.android.hcy.asynchttp;

import android.content.Context;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.util.Log;

import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.utils.Logger;
import com.hxlm.hcyandroid.BaseApplication;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.util.ACache;
import com.hxlm.hcyandroid.util.Md5Util;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import org.apache.http.cookie.Cookie;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;

/**
 * 基本的用于请求网络的工具类
 */
public class HcyHttpClient {
    public static final int METHOD_GET = 0;
    public static final int METHOD_POST = 1;

    private static AsyncHttpClient client;
    private static PersistentCookieStore cookieStore = null;
    private static String LANGUAGE;
    private static AsyncHttpClient client_en;

    static {
        client = new AsyncHttpClient();
        client.setThreadPool(Executors.newFixedThreadPool(3));
        client.setMaxConnections(3);
        client.setTimeout(200000);

        cookieStore = new PersistentCookieStore(BaseApplication.getContext());
        client.setCookieStore(cookieStore);

        if (Constant.isEnglish) {
            LANGUAGE = "us-en";
        }else {
            LANGUAGE = "";
        }

        if (Constant.BASE_URL.equals(Constant.TEST_SYSTEM_URL)) {
            client.addHeader("version", Constant.TEST_SYSTEM_HEADER + getVersionCode());
            client.addHeader("language", LANGUAGE);
        } else if (Constant.BASE_URL.equals(Constant.PRODUCTION_SYSTEM_URL)) {
            client.addHeader("version", Constant.PRODUCTION_SYSTEM_HEADER + getVersionCode());
            client.addHeader("language", LANGUAGE);
        }

//        client_en = new AsyncHttpClient();
//        client_en.setThreadPool(Executors.newFixedThreadPool(3));
//        client_en.setMaxConnections(3);
//        client_en.setTimeout(20000);
////
////        cookieStore = new PersistentCookieStore(BaseApplication.getContext());
//        client_en.setCookieStore(cookieStore);
//        if (Constant.isEnglish) {
//            LANGUAGE = "us-en";
//        }else {
//            LANGUAGE = "";
//        }
//
//        if (Constant.BASE_URL.equals(Constant.TEST_SYSTEM_URL)) {
//            client_en.addHeader("version", Constant.TEST_SYSTEM_HEADER + getVersionCode());
//            client_en.addHeader("language", LANGUAGE);
//        } else if (Constant.BASE_URL.equals(Constant.PRODUCTION_SYSTEM_URL)) {
//            client_en.addHeader("version", Constant.PRODUCTION_SYSTEM_HEADER + getVersionCode());
//            client_en.addHeader("language", LANGUAGE);
//        }
    }

    private String url;

    /**
     * 下载文件
     */
    public static void download(String url, HcyBinaryResponseHandler responseHandler) {
//        if (Constant.isEnglish){
//            client_en.get(url, responseHandler);
//        }else {
            client.get(url, responseHandler);
//        }
    }

    /**
     * 提交Http请求,不显示进度
     */
    public static void submitNoPre(int method, String url, RequestParams params, HcyHttpResponseHandler responseHandler) {
        if (method == METHOD_GET) {
//            if (Constant.isEnglish) {
//                client_en.get(Constant.BASE_URL + url, params, responseHandler);
//            }else{
                client.get(Constant.BASE_URL + url, params, responseHandler);
//            }
        } else if (method == METHOD_POST) {
//            if (Constant.isEnglish){
//                params.add("token", getCookie("token"));
//                client_en.post(Constant.BASE_URL + url, params, responseHandler);
//            }else {
                params.add("token", getCookie("token"));
                client.post(Constant.BASE_URL + url, params, responseHandler);
//            }
        }
    }

    /**
     * 提交Http请求
     */
    public static void  submit(int method, String url, RequestParams params, HcyHttpResponseHandler responseHandler) {

        String requestName = url + (params != null ? params.toString() : "");
        if(!TextUtils.isEmpty(requestName)){
            if(requestName.length()>100){
                requestName = requestName.substring(0,100);
            }
        }
        if (requestInfoBeanList.size() == 20) {
            requestInfoBeanList.remove(0);
        }
        requestInfoBeanList.add(new RequestInfoBean(requestName, System.currentTimeMillis()));
        if (isAllowSubmit(requestInfoBeanList)) {

            if (Constant.isEnglish){
                LANGUAGE = "us-en";
                Log.d("isAllowSubmit", "true: "+requestName);
            }else {
                LANGUAGE = "";
                Log.d("isAllowSubmit", "true: "+requestName);
            }
            Log.d("isAllowSubmit", "true: "+requestName);
            responseHandler.preSubmit();
            if (method == METHOD_GET) {
//                    client.addHeader("Cookie", "token=" + HcyHttpClient.getCookie("token")
//                            + ";JSESSIONID=" + HcyHttpClient.getCookie("JSESSIONID"));
                    client.addHeader("language", LANGUAGE);
                    client.get(Constant.BASE_URL + url, params, responseHandler);
            } else if (method == METHOD_POST) {
                    params.add("token", getCookie("token"));
//                    client.addHeader("Cookie", "token=" + HcyHttpClient.getCookie("token")
//                            + ";JSESSIONID=" + HcyHttpClient.getCookie("JSESSIONID"));
                    client.addHeader("language", LANGUAGE);
                    client.post(Constant.BASE_URL + url, params, responseHandler);
            }
        }else {
           //2秒后恢复请求，
           TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    requestInfoBeanList.clear();
                }
            };
            Timer timer = new Timer();
            timer.schedule(task,2000);
        }

    }

    public static void getSecret(HcyHttpResponseHandler responseHandler) {
        String url = "/weiq/weiq/getWeiqSecret.jhtml";
        RequestParams params = new RequestParams();
        params.put("pluginname", "weixinPayHcyPhonePlugin");
        String s = Md5Util.stringMD5("weixinPayHcyPhonePluginky3h.com");
        params.put("token", s);
//        if (Constant.isEnglish){
//            client_en.post(Constant.BASE_URL + url, params, responseHandler);
//        }else {
            client.post(Constant.BASE_URL + url, params, responseHandler);
//        }
    }

    private static List<RequestInfoBean> requestInfoBeanList = new ArrayList<>(20);

    /**
     * 提交闻音文件Http请求
     */
    public static void submitWenYin(int method, String url, RequestParams params,
                                    HcyHttpResponseHandler responseHandler) {
        responseHandler.preSubmit();

//        if (Constant.isEnglish){
//            if (method == METHOD_GET) {
//                client_en.get(Constant.BASE_URL + url, params, responseHandler);
//            } else if (method == METHOD_POST) {
//                client_en.addHeader("Cookie", "token=" + getCookie("token") + ";JSESSIONID=" + LoginControllor.getLoginMemberJSESSIONID());
//                client_en.post(Constant.BASE_URL + url, params, responseHandler);
//            }
//        }else {
            if (method == METHOD_GET) {
                client.get(Constant.BASE_URL + url, params, responseHandler);
            } else if (method == METHOD_POST) {
                client.addHeader("Cookie", "token=" + getCookie("token") + ";JSESSIONID=" + LoginControllor.getLoginMemberJSESSIONID());
                client.post(Constant.BASE_URL + url, params, responseHandler);
            }
//        }

    }

    private static boolean isAllowSubmit(List<RequestInfoBean> requestInfoBeans) {
        //同一个请求连续5次；20次发生在3秒内且有次数超过5次的请求，认为是程序循环导致的异常请求
        try {
            if (requestInfoBeans != null && requestInfoBeans.size() > 0) {
                RequestInfoBean start = requestInfoBeans.get(0);
                RequestInfoBean end = requestInfoBeans.get(requestInfoBeans.size() - 1);
                if (end.getCrateTime() - start.getCrateTime() < 3000) {
                    Map<String, Integer> map = new HashMap();
                    for (RequestInfoBean bean : requestInfoBeans) {
                        Integer count = map.get(bean.getUrlAndParmar());
                        map.put(bean.getUrlAndParmar(), (count == null) ? 1 : count + 1);
                    }
                    for (String key : map.keySet()) {
                        Integer integer = map.get(key);
                        if (integer > 4) {
                            return false;
                        }
                    }
                }
                RequestInfoBean start2 = requestInfoBeans.get(0);
                int sequentialTimes = 0;
                for (RequestInfoBean bean : requestInfoBeans) {
                    if (start2.getUrlAndParmar().equals(bean.getUrlAndParmar())) {
                        sequentialTimes++;
                        if (sequentialTimes == 5) {
                            return false;
                        }
                    } else {
                        start2 = bean;
                        sequentialTimes = 0;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    /**
     * 提交采用缓存机制的Http请求,存在缓存的情况下自动使用缓存，同时在请求成功后自动保存缓存
     */
    public static void submitCached(int method, String url, RequestParams params,
                                    HcyHttpResponseHandler responseHandler) {
        //缓存名称
        final String cachedFileName = url + (params != null ? params.toString() : "");

        String cacheJson = ACache.getAsString(cachedFileName);//读取缓存的string

        if (!TextUtils.isEmpty(cacheJson)) {
            //读取缓存成功数据
            responseHandler.cacheParse(cacheJson);
        } else {
            //设置缓存名称
            responseHandler.setCache(cachedFileName);

            //提交请求
            submit(method, url, params, responseHandler);
        }
    }

    /**
     * 提交采用缓存机制的Http请求,存在缓存的情况下自动使用缓存，同时在请求成功后自动保存缓存
     */
    public static void submitOneDayOneCached(int method, String url, RequestParams params,
                                             HcyHttpResponseHandler responseHandler) {
        //缓存名称
        final String cachedFileName = url + (params != null ? params.toString() : "");

        String cacheJson = ACache.getAsString(cachedFileName);//读取缓存的string

        if (!TextUtils.isEmpty(cacheJson)) {
            //读取缓存成功数据
            responseHandler.cacheParse(cacheJson);
        } else {
            //设置缓存名称
            responseHandler.setCache(cachedFileName, 24 * 36000);

            //提交请求
            submit(method, url, params, responseHandler);
        }
    }


    /**
     * 提交采用缓存机制的Http请求,存在缓存的情况下自动使用缓存，同时在请求成功后自动保存缓存
     */
    public static void submitCachedWithDay(int method,int day, String url, RequestParams params,
                                             HcyHttpResponseHandler responseHandler) {
        //缓存名称
        final String cachedFileName = url + (params != null ? params.toString() : "")+Constant.isEnglish;
        Log.d("cache", "submitCachedWithDay: "+cachedFileName);
        String cacheJson = ACache.getAsString(cachedFileName);//读取缓存的string

        if (!TextUtils.isEmpty(cacheJson)) {
            //读取缓存成功数据
            responseHandler.cacheParse(cacheJson);
        } else {
            //设置缓存名称
            responseHandler.setCache(cachedFileName, day*24 * 36000);

            //提交请求
            submit(method, url, params, responseHandler);
        }
    }

    /**
     * 具备自动登录能力的方法，提交Http请求时如果用户未登录,自动弹出登录窗口
     */
    public static void submitAutoLogin(final int method, final String url, final RequestParams params,
                                       final HcyHttpResponseHandler responseHandler, String authType, Context context) {
        HttpRequest httpRequest = new HttpRequest(method, url, params, responseHandler);

        if (!TextUtils.isEmpty(authType)) {
            httpRequest.setAuthType(authType);
            responseHandler.getAbstractHttpHandlerCallback().setHttpRequest(httpRequest);
        }

        submitAutoLogin(httpRequest, context);
    }

    /**
     * 提交封装的Http请求,自动弹出登录窗口，如果用户还未登录但是选择了“记住我”，则自动登录，登录后自动重发上次请求；
     * 如果用户未登录，自动弹出登录窗口
     *
     * @param httpRequest 需要用户信息的http请求
     * @param context     触发本次请求的Context
     */
    public static void submitAutoLogin(final HttpRequest httpRequest, Context context) {
        switch (httpRequest.getAuthType()) {
            case LoginControllor.MEMBER:
                LoginControllor.requestLogin(context, new OnCompleteListener() {
                    @Override
                    public void onComplete() {
                        httpRequest.getParams().put(LoginControllor.MEMBER, LoginControllor.getLoginMember().getId());
                        submit(httpRequest.getMethod(), httpRequest.getUrl(), httpRequest.getParams(), httpRequest.getResponseHandler());
                    }
                });
                break;

            case LoginControllor.CHILD_MEMBER:
                LoginControllor.chooseChildMember(context, new OnCompleteListener() {
                    @Override
                    public void onComplete() {
                        httpRequest.getParams().put(LoginControllor.CHILD_MEMBER, LoginControllor.getChoosedChildMember().getId());
                        httpRequest.getParams().put("cust_id", LoginControllor.getChoosedChildMember().getId());
                        submit(httpRequest.getMethod(), httpRequest.getUrl(), httpRequest.getParams(), httpRequest.getResponseHandler());
                    }
                });
                break;

            default:
                submit(httpRequest.getMethod(), httpRequest.getUrl(), httpRequest.getParams(), httpRequest.getResponseHandler());
        }

    }

    public static String getCookie(String key) {
        if (!TextUtils.isEmpty(key)) {
            for (Cookie cookie : cookieStore.getCookies()) {
                if (key.equals(cookie.getName())) {
                    return cookie.getValue();
                }
            }
        }
        return null;
    }

    //删除Cookie
    public static void removeCookie() {
        if (cookieStore != null) {
            cookieStore.clear();
        }
    }

    //得到当前版本号
    private static int getVersionCode() {
        Context context = BaseApplication.getContext();
        int versionCode = 0;
        String packageName = context.getPackageName();
        try {
            versionCode = context.getPackageManager().getPackageInfo(packageName, 0).versionCode;
            if (Constant.DEBUG) {
                Log.i("Log.i", "versionCode--->" + versionCode);
            }

        } catch (PackageManager.NameNotFoundException e) {
            Logger.i("hcy", e.getMessage());
        }
        return versionCode;
    }
}
