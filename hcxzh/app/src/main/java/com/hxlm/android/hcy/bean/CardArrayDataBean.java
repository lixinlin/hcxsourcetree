package com.hxlm.android.hcy.bean;

/**
 * @author Lirong
 * @date 2018/12/19.
 * @description  卡列表bean   array_data
 */

public class CardArrayDataBean {

    private String memberId;
    private String card_no; //卡号
    private String card_name;
    private String description;
    private String status;
    private String service_name;
    private String exprise_time;
    private int tag;

    public int getTag() {
        return tag;
    }

    public void setTag(int tag) {
        this.tag = tag;
    }

    public String getExprise_time() {
        return exprise_time;
    }

    public void setExprise_time(String exprise_time) {
        this.exprise_time = exprise_time;
    }

    /**
     * data
     */
    private double balance;
    private double amount;
    private int expiredTime;
    private long beginDate;
    private long endDate;
    private boolean isDated;
    private long bindDate;
    private transient boolean isChoosed;

    public long getBindDate() {
        return bindDate;
    }

    public void setBindDate(long bindDate) {
        this.bindDate = bindDate;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getExpiredTime() {
        return expiredTime;
    }

    public void setExpiredTime(int expiredTime) {
        this.expiredTime = expiredTime;
    }

    public long getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(long beginDate) {
        this.beginDate = beginDate;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    public boolean isDated() {
        return isDated;
    }

    public void setDated(boolean dated) {
        isDated = dated;
    }

    public boolean isChoosed() {
        return isChoosed;
    }

    public void setChoosed(boolean choosed) {
        isChoosed = choosed;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getCard_no() {
        return card_no;
    }

    public void setCard_no(String card_no) {
        this.card_no = card_no;
    }

    public String getCard_name() {
        return card_name;
    }

    public void setCard_name(String card_name) {
        this.card_name = card_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    @Override
    public String toString() {
        return "CardArrayDataBean{" +
                "card_name='" + card_name + '\'' +
                ", description='" + description + '\'' +
                ", status='" + status + '\'' +
                ", balance=" + balance +
                '}';
    }
}
