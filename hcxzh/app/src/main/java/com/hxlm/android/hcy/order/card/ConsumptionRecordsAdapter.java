package com.hxlm.android.hcy.order.card;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.bean.CardRecordBean;

import java.util.List;

/**
 * @author Lirong
 * @date 2018/12/6 0006.
 * @description  激活卡详情 消费记录适配器
 */

public class ConsumptionRecordsAdapter extends RecyclerView.Adapter<ConsumptionRecordsAdapter.ConsumptionRecordsViewHolder> {

    private Context context;
    private List<CardRecordBean> list;

    public ConsumptionRecordsAdapter(Context context,List<CardRecordBean> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ConsumptionRecordsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_card_details_record, parent, false);
        ConsumptionRecordsViewHolder consumptionRecordsViewHolder = new ConsumptionRecordsViewHolder(view);
        return consumptionRecordsViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ConsumptionRecordsViewHolder holder, int position) {
        CardRecordBean cardRecordBean = list.get(position);
        holder.tvTitle.setText(cardRecordBean.getServiceName());
        String createTime = cardRecordBean.getCreateTime();
        if (!TextUtils.isEmpty(createTime)) {
            String[] split = createTime.split(" ");
            holder.tvDate.setText(split[0]);
            holder.tvTime.setText(split[1]);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ConsumptionRecordsViewHolder extends RecyclerView.ViewHolder{

        private final TextView tvTitle;
        private final TextView tvDate;
        private final TextView tvTime;

        public ConsumptionRecordsViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_card_details_record_title);
            tvDate = itemView.findViewById(R.id.tv_card_details_record_date);
            tvTime = itemView.findViewById(R.id.tv_card_details_record_time);
        }
    }

}
