package com.hxlm.android.hcy.voicediagnosis;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.hcy.ky3h.R;
import com.hjq.permissions.OnPermission;
import com.hjq.permissions.Permission;
import com.hjq.permissions.XXPermissions;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.utils.MyCountTimer;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.android.utils.Logger;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.tabbar.archives.ReportInfoActivity;
import com.hxlm.hcyandroid.util.StatusBarUtils;

import net.sourceforge.simcpux.Constants;

import java.util.List;

public class BianShiActivity extends BaseActivity implements OnClickListener {
    private static final String TAG = "BianShiManager";
    private static final int ZI_CHANGE_INTERVAL = 2000;//每隔2秒更新一次
    private Runnable runnable;

    private ImageView bianshiMainImg;
    private ImageView mStartButton;// 开始辨识
    private boolean isStarting=false;//是否已经开始

    private AnimationDrawable mainAnim;

    private ImageView[] ziImg;
    private int[] mainAnimRidArray;
    private int[] pinyinImgRidArray;
    private int[] ziImgDefaultRidArray;
    private int[] ziImgFocusRidArray;
    private MyCountTimer myCountTimer;
    private TextView tvBtnCountTimer;
    private RelativeLayout rl;
    private boolean isCountDown = false; //是否正在倒计时

    private int ziAnimState = 0;
    public final int SUBJECTID[] = {
            21, 24, 22, 25, 23,
            18, 17, 16, 19, 20,
            28, 27, 26, 29, 30,
            32, 31, 34, 33, 35,
            37, 40, 36, 38, 39
    };
    private BianShiManager bianShiManager;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                //开始录音
                case BianShiManager.START_RECORD: {
                    msg.arg1 = 0;

//                    bianshiDemo.setClickable(false);
//                    mStartButton.setImageResource(R.drawable.wenyin_stopbianshi);//显示停止
                    changeZiAnim();
                    break;
                }

                //初始化录音文件失败
                case BianShiManager.INIT_RECORDER_FAIL: {
//                    Toast.makeText(BianShiActivity.this, getString(R.string.init_recorder_fail),
//                            Toast.LENGTH_LONG).show();
                    resetUI();
                    break;
                }

                case BianShiManager.NO_VOICE:
                    Toast.makeText(BianShiActivity.this, getString(R.string.no_voice),
                            Toast.LENGTH_LONG).show();
                    bianShiManager.stopVoiceRecord();
                    resetUI();
                    bianShiManager.clearVoiceFile();
                    break;
                default:
                    break;

            }
        }
    };
    private ImageView iv_back;

    @Override
    public void setContentView() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        StatusBarUtils.setWindowStatusBarColor(this,R.color.title_bar_bg);
        setContentView(R.layout.activity_bianshi);

        bianShiManager = new BianShiManager(mHandler);
    }

    @Override
    public void initViews() {
//        TitleBarView titleBar = new TitleBarView();
//        titleBar.init(this, getString(R.string.bianshi_title), titleBar, 1);

        ziImg = new ImageView[5];
        ziImg[0] = (ImageView) findViewById(R.id.zi_img1);
        ziImg[1] = (ImageView) findViewById(R.id.zi_img2);
        ziImg[2] = (ImageView) findViewById(R.id.zi_img3);
        ziImg[3] = (ImageView) findViewById(R.id.zi_img4);
        ziImg[4] = (ImageView) findViewById(R.id.zi_img5);

        bianshiMainImg = (ImageView) findViewById(R.id.bianshi_main_img);
        mStartButton = (ImageView) findViewById(R.id.record_start);
        tvBtnCountTimer = (TextView) findViewById(R.id.btnCountTimer);
        iv_back = findViewById(R.id.iv_back);
        iv_back.setOnClickListener(this);
        rl = (RelativeLayout) findViewById(R.id.rl);
        rl.setOnClickListener(this);
        myCountTimer = new MyCountTimer(BianShiActivity.this, 3000, 500, tvBtnCountTimer,1);

        //倒计时结束回调监听
        myCountTimer.setCountTimerFinishListener(new MyCountTimer.OnCountTimerFinishListener() {
            @Override
            public void onTimerFinish() {

                rl.setEnabled(true);
                rl.setClickable(true);
                tvBtnCountTimer.setVisibility(View.GONE);

                isCountDown = true; //倒计时结束
                mStartButton.setImageDrawable(
                        BianShiActivity.this.getResources().getDrawable(R.drawable.wenyin_stop));
                mStartButton.setVisibility(View.VISIBLE);
//                Toast.makeText(BianShiActivity.this, "开始录音", Toast.LENGTH_SHORT).show();
                startOrStopRecording();
            }
        });
    }

    private void startOrStopRecording() {
        //档录音开始运行
        if (isStarting == true) {
            //按钮可以点击
            if (rl.isEnabled()) {
                bianShiManager.setUseVoiceAnalysis(false);
                bianShiManager.stopVoiceRecord();
                bianShiManager.clearVoiceFile();
                //bianshiDemo.setClickable(true);
                resetUI();//跟新UI
                isStarting = false;
            } else {
                Logger.i(TAG, "不可点击");
            }

        } else {
            Logger.i(TAG, "开始录音");
            // 录音
            bianShiManager.setUseVoiceAnalysis(true);

//            mStartButton.setEnabled(false);//不可点击
            // 录音
            XXPermissions.with(this) .permission(Permission.RECORD_AUDIO)
                    .request(new OnPermission() {

                        @Override
                        public void hasPermission(List<String> granted, boolean isAll) {
                            bianShiManager.startVoiceRecording();
                        }

                        @Override
                        public void noPermission(List<String> denied, boolean quick) {
                            Toast.makeText(BianShiActivity.this, R.string.luyinquanxianbjz, Toast.LENGTH_SHORT).show();
                        }
                    });


//            if (!BianShiManager.mIsRecordUsable) {
//
//                finish();
//            }
        }
    }

    @Override
    public void initDatas() {
        mainAnimRidArray = new int[5];
        mainAnimRidArray[0] = R.drawable.bianshi_main_duo_anim;
        mainAnimRidArray[1] = R.drawable.bianshi_main_ruai_anim;
        mainAnimRidArray[2] = R.drawable.bianshi_main_mi_anim;
        mainAnimRidArray[3] = R.drawable.bianshi_main_sao_anim;
        mainAnimRidArray[4] = R.drawable.bianshi_main_la_anim;

        //拼音
        pinyinImgRidArray = new int[5];
        pinyinImgRidArray[0] = R.drawable.duo_03;
        pinyinImgRidArray[1] = R.drawable.ruai_03;
        pinyinImgRidArray[2] = R.drawable.mi_03;
        pinyinImgRidArray[3] = R.drawable.sao_03;
        pinyinImgRidArray[4] = R.drawable.la_03;

        //默认
        ziImgDefaultRidArray = new int[5];
        ziImgDefaultRidArray[0] = R.drawable.sa_03;
        ziImgDefaultRidArray[1] = R.drawable.sa_05;
        ziImgDefaultRidArray[2] = R.drawable.sa_07;
        ziImgDefaultRidArray[3] = R.drawable.sa_09;
        ziImgDefaultRidArray[4] = R.drawable.sa_11;

        //最后显示
        ziImgFocusRidArray = new int[5];
        ziImgFocusRidArray[0] = R.drawable.duow;
        ziImgFocusRidArray[1] = R.drawable.laiw;
        ziImgFocusRidArray[2] = R.drawable.miw;
        ziImgFocusRidArray[3] = R.drawable.souw;
        ziImgFocusRidArray[4] = R.drawable.law;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                BianShiActivity.this.finish();
                break;
            // 开始辨识
            case R.id.rl:
                XXPermissions.with(this) .permission(Permission.RECORD_AUDIO)
                        .request(new OnPermission() {

                            @Override
                            public void hasPermission(List<String> granted, boolean isAll) {
                                if (!isCountDown) {
                                    //倒计时期间禁止点击事件操作
                                    rl.setEnabled(false);
                                    rl.setClickable(false);
                                    //开始倒计时，隐藏按钮，倒计时结束，显示
                                    mStartButton.setVisibility(View.GONE);
                                    myCountTimer.start();
                                } else {

                                    mStartButton.setImageDrawable(BianShiActivity.this.getResources().getDrawable(R.drawable.wenyin_begin));
//                    Toast.makeText(BianShiActivity.this, "暂停录音", Toast.LENGTH_SHORT).show();
                                    mStartButton.setVisibility(View.VISIBLE);
                                    startOrStopRecording();
                                    isCountDown = false;
                                }

                            }

                            @Override
                            public void noPermission(List<String> denied, boolean quick) {
                                Toast.makeText(BianShiActivity.this, R.string.luyinquanxianbjz, Toast.LENGTH_SHORT).show();
                            }
                        });

                break;
            default:
                break;
        }
    }

    //切换动画
    private void changeZiAnim() {
        if (mainAnim != null) {
            mainAnim.stop();
        }
        Log.e(TAG,"==ziAnimState=="+ziAnimState+"==ziImg.length=="+ziImg.length);
        //全部录完
        if (ziAnimState >= ziImg.length) {
            bianShiManager.stopVoiceRecord();//删除录音
            resetUI();//更新UI
            isStarting=false;//未开始录音


            if (bianShiManager.isRecordSuccess()) {

                bianShiManager.startUploadVoiceFile(new AbstractDefaultHttpHandlerCallback(this) {
                    @Override
                    protected void onResponseSuccess(Object obj) {
                        Constants.needRefesh = true;
                        String str = (String) obj;
//                        iv_demonstration.setClickable(true);
//                        Toast.makeText(BianShiActivity.this, getString(R.string.file_upload_success),
//                                Toast.LENGTH_LONG).show();
                        bianShiManager.clearVoiceFile();
                        com.alibaba.fastjson.JSONObject jo = JSON.parseObject(str);

                        //21, 24, 22, 25, 23,
//                        18, 17, 16, 19, 20,
//                        28, 27, 26, 29, 30,
//                        32, 31, 34, 33, 35,
//                        37, 40, 36, 38, 39
                        int code = jo.getInteger("code");

                        int id = SUBJECTID[code - 1];
                        String subject_sn = null;

                        if (id == 16) {
                            subject_sn = "JLBS-G1";
                        } else if (id == 17) {
                            subject_sn = "JLBS-G2";
                        } else if (id == 18) {
                            subject_sn = "JLBS-G3";
                        } else if (id == 19) {
                            subject_sn = "JLBS-G4";
                        } else if (id == 20) {
                            subject_sn = "JLBS-G5";
                        } else if (id == 21) {
                            subject_sn = "JLBS-S1";
                        } else if (id == 22) {
                            subject_sn = "JLBS-S2";
                        } else if (id == 23) {
                            subject_sn = "JLBS-S3";
                        } else if (id == 24) {
                            subject_sn = "JLBS-S4";
                        } else if (id == 25) {
                            subject_sn = "JLBS-S5";
                        } else if (id == 26) {
                            subject_sn = "JLBS-J1";
                        } else if (id == 27) {
                            subject_sn = "JLBS-J2";
                        } else if (id == 28) {
                            subject_sn = "JLBS-J3";
                        } else if (id == 29) {
                            subject_sn = "JLBS-J4";
                        } else if (id == 30) {
                            subject_sn = "JLBS-J5";
                        } else if (id == 31) {
                            subject_sn = "JLBS-Z1";
                        } else if (id == 32) {
                            subject_sn = "JLBS-Z2";
                        } else if (id == 33) {
                            subject_sn = "JLBS-Z3";
                        } else if (id == 34) {
                            subject_sn = "JLBS-Z4";
                        } else if (id == 35) {
                            subject_sn = "JLBS-Z5";
                        } else if (id == 36) {
                            subject_sn = "JLBS-Y1";
                        } else if (id == 37) {
                            subject_sn = "JLBS-Y2";
                        } else if (id == 38) {
                            subject_sn = "JLBS-Y3";
                        } else if (id == 39) {
                            subject_sn = "JLBS-Y4";
                        } else if (id == 40) {
                            subject_sn = "JLBS-Y5";
                        }

                        Intent intent = new Intent(BianShiActivity.this, ReportInfoActivity.class);
                        intent.putExtra("subjectId", subject_sn);
                        intent.putExtra("title",getString(R.string.report_title_jl));
                        startActivity(intent);
                        BianShiActivity.this.finish();



//                        bianshiDemo.setClickable(true);
//                        Toast.makeText(BianShiActivity.this, getString(R.string.file_upload_success),
//                                Toast.LENGTH_LONG).show();
//                        bianShiManager.clearVoiceFile();
                    }

                    @Override
                    protected void onNetworkError() {
//                        Toast.makeText(BianShiActivity.this, getString(R.string.file_upload_network_error),
//                                Toast.LENGTH_LONG).show();
                    }

                    @Override
                    protected void onHttpError(int httpErrorCode) {
                        this.onNetworkError();
                    }
                });

            } else {
                bianShiManager.clearVoiceFile();
                Toast.makeText(BianShiActivity.this, getString(R.string.bianshi_no_voice), Toast.LENGTH_LONG).show();
            }
        } else {
            ziImg[ziAnimState].setImageResource(ziImgFocusRidArray[ziAnimState]);

            if (ziAnimState > 0) {
                ziImg[ziAnimState - 1].setImageResource(ziImgDefaultRidArray[ziAnimState - 1]);
            }

            bianshiMainImg.setImageResource(mainAnimRidArray[ziAnimState]);
            mainAnim = (AnimationDrawable) bianshiMainImg.getDrawable();
            mainAnim.start();



            ziAnimState++;
//            if(ziAnimState==2)
//            {
                isStarting=true;//开始运行
//                mStartButton.setEnabled(true);//可以点击
//
//                mStartButton.setImageResource(R.drawable.wenyin_stopbianshi);//显示停止
//            }

            Logger.i(TAG,"ziAnimState走到-->"+ziAnimState);

//            new Handler().postDelayed(new Runnable() {
//
//                @Override
//                public void run() {
//                    num[0]++;
//                    Logger.i(TAG,"num-->"+ num[0]);
//                    BianShiActivity.this.changeZiAnim();
//                }
//
//            }, ZI_CHANGE_INTERVAL);


           runnable=new Runnable() {
               @Override
               public void run() {
               //此处添加每个一段时间要执行的语句
                   BianShiActivity.this.changeZiAnim();
               }
           };

            //启动计时器
            mHandler.postDelayed(runnable,ZI_CHANGE_INTERVAL);
        }
    }

    //更新UI
    private void resetUI() {

        Logger.i(TAG,"更新的UI页面");

        if(runnable!=null)
        {
            Logger.i(TAG,"停止动画的播放runnable");
            mHandler.removeCallbacks(runnable);
            runnable=null;
        }

        if (mainAnim != null) {
            Logger.i(TAG,"停止动画的播放mainAnim");
            mainAnim.stop();
        }

        for (int i = 0; i < ziImg.length; i++) {
            ziImg[i].setImageResource(ziImgDefaultRidArray[i]);
        }
        bianshiMainImg.setImageResource(R.drawable.wenyin_dou1);

        ziAnimState = 0;

        mStartButton.setImageResource(R.drawable.wenyin_begin);
        isCountDown = false;
//        mainAnim = null;
//        mainAnim = (AnimationDrawable) bianshiMainImg.getDrawable();
    }

    @Override
    protected void onDestroy() {
        bianShiManager.stopVoiceRecord();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        bianShiManager.stopVoiceRecord();
        resetUI();
        super.onPause();
    }
}
