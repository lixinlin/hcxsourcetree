package com.hxlm.android.hcy.content;

/**
 * Created by l on 2016/6/16.
 * 宫法的类型以及所对应的运动样式
 */
public class GongFaType  {
    private String type;
    private int index;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public GongFaType(String type, int index) {
        this.type = type;
        this.index = index;
    }
}
