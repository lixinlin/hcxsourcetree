package com.hxlm.android.hcy.order;

import android.util.Log;
import android.view.Window;
import android.widget.ListView;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.content.ResourceItem;
import com.hxlm.android.hcy.message.ActionManager;
import com.hxlm.android.hcy.utils.ViewUtil;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.ReserveType;

import java.util.List;

public class GouWuCheActivity extends BaseActivity {
    private ListView yueyaoLv;
    private PayView payView;
    private CashCardPayView cashCardPayView;

    private List<ResourceItem> resourceItems;

    @Override
    public void setContentView() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_gouwuche);
    }

    @Override
    public void initViews() {
        TitleBarView titleView = new TitleBarView();
        titleView.init(GouWuCheActivity.this, getString(R.string.gouwuche_title), titleView, 1);

        payView = (PayView) findViewById(R.id.pv_yueyao);

        cashCardPayView = (CashCardPayView) findViewById(R.id.ccpv_yueyao);

        yueyaoLv = (ListView) findViewById(R.id.gouwuche_list);

        GouwucheListAdapter adapter = new GouwucheListAdapter(this);

        resourceItems = Constant.LIST;

        yueyaoLv.setAdapter(adapter);

        adapter.setDatas(resourceItems);
    }

    @Override
    public void initDatas() {
    }

    @Override
    protected void onResume() {
        super.onResume();
        ViewUtil.setListHeight(yueyaoLv);

        String productsStr = null;
        String itemIdsStr = null;
        String descStr = null;
        double moneySum = 0;
        if (null != resourceItems && resourceItems.size() > 0) {
            for (int i = 0; i < resourceItems.size(); i++) {

                Log.i("Log.i","resourceItems-->"+resourceItems);

                if (i == 0) {
                    descStr = resourceItems.get(i).getName();
                    itemIdsStr = String.valueOf(resourceItems.get(i).getId());
                    productsStr = String.valueOf(resourceItems.get(i).getProductId());

                   // ToastUtil.invokeShortTimeToast(GouWuCheActivity.this,"itemIdsStr-->"+itemIdsStr);

                } else {
                   // descStr += "," + resourceItems.get(i).getName();
                    descStr=resourceItems.get(0).getName()+"等";
                    itemIdsStr += "," + String.valueOf(resourceItems.get(i).getId());
                    productsStr += "," + resourceItems.get(i).getProductId();
                }
                moneySum += resourceItems.get(i).getPrice();
            }

            payView.init(ReserveType.TYPE_STATIC_RESOURCE, moneySum, itemIdsStr, descStr);

            payView.setOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete() {
                    new ActionManager().clearCache();
                    GouWuCheActivity.this.finish();
                }
            });

            cashCardPayView.init(productsStr, payView);
        }
    }
}
