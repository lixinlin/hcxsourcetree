package com.hxlm.android.hcy.asynchttp;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hxlm.android.utils.Logger;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.ResponseStatus;
import com.hxlm.hcyandroid.util.ACache;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;

/**
 * 和畅依平台的http response的处理
 * Created by Zhenyu on 2016/4/14.
 */
public abstract class HcyHttpResponseHandler extends TextHttpResponseHandler {
    private final String tag = "HcyHttpResponseHandler";

    private final AbstractHttpHandlerCallback abstractHttpHandlerCallback;
    private final Handler handler;
    private String cacheFileName;//缓存的名称
    private int saveTime;//缓存的名称

    public HcyHttpResponseHandler(AbstractHttpHandlerCallback aHandler) {
        this.abstractHttpHandlerCallback = aHandler;
        this.handler = new Handler(aHandler);
    }

    /**
     * 请求成功返回结果
     */
    @Override
    public void onSuccess(int responseCode, Header[] headers, String content) {
        Log.d(tag, "Http Response Success =" + responseCode + "; Content = ----------------------------------");
        Log.d(tag, content);
        Log.d(tag, "-----------------------------------------------------------------------------------------");

        if(Constant.DEBUG)
        {
            Log.i("Log.i","Http Response Success =" + responseCode + "; Content = "+content);
        }

        JSONObject jo = JSON.parseObject(content);
        int status = jo.getIntValue("status");

        //判断返回的状态
        switch (status) {
            //如果返回值为100
            case ResponseStatus.SUCCESS:
                String attr_data = jo.getString("attr_data");
                String dataStr = jo.getString("data");
                if (!TextUtils.isEmpty(cacheFileName)) {
                    /**
                     * 缓存数据
                     */
                    ACache.put(cacheFileName, dataStr, saveTime);
                }
                if (!TextUtils.isEmpty(attr_data)){
                    handler.obtainMessage(AbstractHttpHandlerCallback.ON_RESPONSE_SUCCESS, contentParse(content)).sendToTarget();
                }else {
                    handler.obtainMessage(AbstractHttpHandlerCallback.ON_RESPONSE_SUCCESS, contentParse(dataStr)).sendToTarget();
                }
                break;

            //错误数据
            default:
                String data = jo.getString("data");//旧接口数据提示使用同一个字段
                String message = jo.getString("message");//新接口的提示
                if(!TextUtils.isEmpty(message)){
                    Message.obtain(handler, AbstractHttpHandlerCallback.ON_RESPONSE_ERROR, status, 0, message ).sendToTarget();
                }else {
                    Message.obtain(handler, AbstractHttpHandlerCallback.ON_RESPONSE_ERROR, status, 0, data).sendToTarget();
                }
                break;
        }
    }

    //请求网络错误
    @Override
    public void onFailure(int responseCode, Header[] headers, String content, Throwable t) {
        Logger.i(tag, "Http ERROR Response Code =" + responseCode + "; Content = ----------------------------------");
        Logger.i(tag, content != null ? content : "");
        Logger.i(tag, "---------------------------------- ----------------------------------------------------------");

        if(Constant.DEBUG)
        {
            Log.i("Log.i","Http ERROR Response Code =" + responseCode + "; Content = "+content);
        }

        //判断返回码，如果不为0则是服务器错误，否则是网络错误
        if (responseCode != 0) {
            Message.obtain(handler, AbstractHttpHandlerCallback.ON_HTTP_ERROR, responseCode, 0).sendToTarget();
        } else {
            handler.sendEmptyMessage(AbstractHttpHandlerCallback.ON_NETWORK_ERROR);
        }
    }

    //设置缓存名称
    public void setCache(String cacheFileName) {
        this.cacheFileName = cacheFileName;
    }
    //设置缓存名称 缓存有效期
    public void setCache(String cacheFileName ,int saveTime) {
        this.cacheFileName = cacheFileName;
        this.saveTime = saveTime;
    }

    AbstractHttpHandlerCallback getAbstractHttpHandlerCallback() {
        return abstractHttpHandlerCallback;
    }

    void preSubmit() {
        if (abstractHttpHandlerCallback != null) {
            abstractHttpHandlerCallback.preSubmit();
        }
    }

    //读取缓存数据
    void cacheParse(String content) {
        handler.obtainMessage(AbstractHttpHandlerCallback.ON_RESPONSE_SUCCESS, contentParse(content)).sendToTarget();
    }

    //得到成功数据需要回调的方法
    protected abstract Object contentParse(String content);
}
