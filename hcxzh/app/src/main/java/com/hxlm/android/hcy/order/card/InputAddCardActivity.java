package com.hxlm.android.hcy.order.card;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.order.CardManager;
import com.hxlm.android.hcy.order.erweima.Constant;
import com.hxlm.android.hcy.order.erweima.activity.CaptureActivity;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.tabbar.usercenter.MyCardPackageActivity;
import com.hxlm.hcyandroid.util.ToastUtil;
import com.hxlm.hcyandroid.view.ContainsEmojiEditText;

/**
 * @author Lirong
 * @date 2018/12/6 0006.
 * @description 手动输入服务卡号添加卡
 */

public class InputAddCardActivity extends BaseActivity implements View.OnClickListener {

    private ContainsEmojiEditText et_card_number;
    private Button btn_add_card_package;
    private CardManager cardManager;
    private LinearLayout ll_erweima;

    private Bundle bundle;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_input_add_card);
    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.input_add_card_title), titleBar, 1);
        et_card_number = findViewById(R.id.et_card_number); //输入卡号
        btn_add_card_package = findViewById(R.id.btn_add_card_package);  //添加至卡包
        btn_add_card_package.setOnClickListener(this);

        ll_erweima = findViewById(R.id.ll_erweima);
        ll_erweima.setOnClickListener(this);
    }

    @Override
    public void initDatas() {
        cardManager = new CardManager();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_add_card_package:
                String card_number = et_card_number.getText().toString();
                if (TextUtils.isEmpty(card_number)) {
                    ToastUtil.invokeShortTimeToast(InputAddCardActivity.this, getString(R.string.input_add_card_tips_empty));
                } else {
                    cardManager.bindCard(card_number, new AbstractDefaultHttpHandlerCallback(this) {
                        @Override
                        protected void onResponseSuccess(Object obj) {
                            ToastUtil.invokeShortTimeToast(InputAddCardActivity.this,getString(R.string.input_add_card_tips_success));
                            InputAddCardActivity.this.finish();
                        }

                    });
                }
                break;
            case R.id.ll_erweima:
                startQrCode();
                break;
            default:
                break;
        }
    }
    private void startQrCode() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            // 申请权限
            ActivityCompat.requestPermissions(InputAddCardActivity.this, new String[]{Manifest.permission.CAMERA}, Constant.REQ_PERM_CAMERA);
            return;
        }
        // 二维码扫码
//                startActivity(new Intent(this, BindNewCardActivity.class));
        startActivity(new Intent(this, CaptureActivity.class));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //扫描结果回调
        if (requestCode == Constant.REQ_QR_CODE && resultCode == RESULT_OK) {
            bundle = data.getExtras();
        }
        String scanResult = bundle.getString(Constant.INTENT_EXTRA_KEY_QR_SCAN);

        Log.e("retrofit","====二维码扫描结果==="+scanResult);

//        tvResult.setText(scanResult);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constant.REQ_PERM_CAMERA:
                // 摄像头权限申请
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // 获得授权
                    startQrCode();
                } else {
                    // 被禁止授权
                    Toast.makeText(InputAddCardActivity.this, getString(R.string.my_card_package_qingzhiquanxianzxdk), Toast.LENGTH_LONG).show();
                } break;
            default:
                break;
        }
    }

}
