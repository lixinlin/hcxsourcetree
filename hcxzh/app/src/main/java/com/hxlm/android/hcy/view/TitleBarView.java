package com.hxlm.android.hcy.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.hcy.ky3h.BuildConfig;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.message.HcyMessage;
import com.hxlm.android.hcy.message.MessageManager;
import com.hxlm.android.hcy.message.MessagesActivity;
import com.hxlm.hcyandroid.BaseApplication;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.callback.MyCallBack;
import com.hxlm.hcyandroid.callback.MyCallBack.OnBackClickListener;
import com.ta.utdid2.android.utils.StringUtils;

import java.lang.ref.WeakReference;
import java.util.List;

public class TitleBarView implements OnClickListener {
    public ImageView iv_news;// 消息
    // 得到传递过来的图文edittext
    Context context;
    private WeakReference<Activity> weakActivity;
    private WeakReference<TitleBarView> weakTitleBarView;
    private View decorView;
    public TextView tv_title;

    public LinearLayout linear_back;
    private int index = -1;
    private String tvTitle;
    private OnCompleteListener onCompleteListener;
    private OnBackClickListener onBackClickListener;
    private MyCallBack.OnLoginTrueLinstener onLoginTrueLinstener;
    private ImageView iv_families;


    private MessageManager messageManager = new MessageManager();//消息message
//    private ImageView iv_families_circle;

    // 自己写的返回方法
    public void init(Activity activity, String titleText, TitleBarView titleBarView, int index,
                     OnBackClickListener onBackClickListener) {
        this.onBackClickListener = onBackClickListener;
        init(activity, titleText, titleBarView, index);
    }

    //用于用户在登录成功在我的界面更新显示
    public void init(Activity activity, String titleText, TitleBarView titleBarView, int index,
                     MyCallBack.OnLoginTrueLinstener onLoginTrueLinstener) {
        this.onLoginTrueLinstener = onLoginTrueLinstener;
        init(activity, titleText, titleBarView, index);
    }

    // 选择家庭成员的方法
    public void init(Activity activity, String titleText, TitleBarView titleBarView, int index,
                     OnCompleteListener onCompleteListener) {
        this.onCompleteListener = onCompleteListener;
        init(activity, titleText, titleBarView, index);
    }

    // 选择家庭成员的方法以及返回方法
    public void init(Activity activity, String titleText, TitleBarView titleBarView, int index,
                     OnCompleteListener onCompleteListener, OnBackClickListener onBackClickListener) {
        this.onCompleteListener = onCompleteListener;
        this.onBackClickListener = onBackClickListener;
        init(activity, titleText, titleBarView, index);
    }

    // 初始化导航栏
    public void init(Activity activity, String titleText, TitleBarView titleBarView, int index) {
        // ------------------------------------------------------
        context = activity;
        if (titleBarView != null) {
            if (weakTitleBarView == null) {
                weakTitleBarView = new WeakReference<>(titleBarView);
                Constant.titleBarViews.add(weakTitleBarView);
            }
        }
        if (weakActivity == null) {
            weakActivity = new WeakReference<>(activity);
        }

        decorView = weakActivity.get().getWindow().getDecorView();
        this.index = index;
        this.tvTitle = titleText;
        // 初始化数据
        initView();

        if (!TextUtils.isEmpty(titleText)) {
            tv_title.setText(titleText);
        }

        //更新每个titlebar消息 如果消息集合中含有未读消息，导航栏消息的小红点一直显示，如果消息全部读完，消息的小红点就会消失
        //从Share得到消息
        List<HcyMessage> hcyMessages = messageManager.getHcyMessagesFromSp();

        //在指定的消息集合中是否存在未读消息  如果有未读消息
        if (!messageManager.isAllRead(hcyMessages)) {
            for (WeakReference<TitleBarView> weakTitleBarview : Constant.titleBarViews) {
                if (weakTitleBarview.get() != null) {
                    weakTitleBarview.get().refreshUnread(true);
                }
            }
        }

    }

    // 更新当前titlebar消息图片,判断当前是否有新消息
    /**
     * int isHaveNews=0，代表没有消息，isHaveNews=1代表有新消息
      */
    public void refreshUnread(boolean haveUnreadMessage) {
        if (haveUnreadMessage) {
            iv_news.setImageResource(R.drawable.title_bar_right_have_news);
        } else {
            iv_news.setImageResource(R.drawable.title_bar_right_no_news);
        }
    }

    private void initView() {
        tv_title =  decorView.findViewById(R.id.tv_title);
        tv_title.setOnClickListener(new OnClickListener() {
            //需要监听几次点击事件数组的长度就为几
            // 如果要监听双击事件则数组长度为2，如果要监听3次连续点击事件则数组长度为3...
            //初始全部为0
            long[] mHints = new long[4];
            @Override
            public void onClick(View v) {
                //将mHints数组内的所有元素左移一个位置
                System.arraycopy(mHints, 1, mHints, 0, mHints.length - 1);
                //获得当前系统已经启动的时间
                mHints[mHints.length - 1] = SystemClock.uptimeMillis();
                if (SystemClock.uptimeMillis() - mHints[0] <= 700){
                    String str = Constant.BASE_URL
                            + " 宽" + BaseApplication.screenWidth
                            +"  高"+BaseApplication.screenHeight
                            +"  是否签名？"+!BaseApplication.isApkDebugable()
                            +"  版本号"+ BuildConfig.VERSION_NAME
                            +"  打包日期"+ BuildConfig.releaseTime;
                    Toast.makeText(weakActivity.get(), str, Toast.LENGTH_LONG).show();
                }
            }
        });
        linear_back =  decorView.findViewById(R.id.linear_back_title);
//        iv_families_circle = decorView.findViewById(R.id.iv_families_circle);
        iv_families =  decorView.findViewById(R.id.iv_families);
        linear_back.setOnClickListener(this);
        ImageView ivMainLeft = decorView.findViewById(R.id.iv_main_left);
        RadioGroup rgSegment =  decorView.findViewById(R.id.rg_segment);

        iv_news =  decorView.findViewById(R.id.iv_newmessage);
        // 默认是没有消息图片
        iv_news.setImageResource(R.drawable.title_bar_right_no_news);
        iv_news.setOnClickListener(this);

        // 0代表不显示，1代表显示
        if (index == 0 && StringUtils.isEmpty(tvTitle)) {
            linear_back.setVisibility(View.INVISIBLE);
            ivMainLeft.setVisibility(View.VISIBLE);
            rgSegment.setVisibility(View.GONE);
            tv_title.setVisibility(View.INVISIBLE);
            iv_families.setVisibility(View.INVISIBLE);
        } else if (index == 0 && !StringUtils.isEmpty(tvTitle)) {
            linear_back.setVisibility(View.INVISIBLE);
            ivMainLeft.setVisibility(View.INVISIBLE);
            rgSegment.setVisibility(View.GONE);
//            iv_families_circle.setVisibility(View.VISIBLE);
            iv_families.setVisibility(View.INVISIBLE);
            tv_title.setVisibility(View.VISIBLE);
        } else if (index == 1) {
            linear_back.setVisibility(View.VISIBLE);
            ivMainLeft.setVisibility(View.INVISIBLE);
            rgSegment.setVisibility(View.GONE);
            tv_title.setVisibility(View.VISIBLE);
            iv_families.setVisibility(View.INVISIBLE);
        }
        iv_families.setOnClickListener(this);
//        iv_families_circle.setOnClickListener(this);

        //            更新头像
//        Member loginMember = LoginControllor.getLoginMember();
//        if(loginMember!=null){
//            String memberImage = loginMember.getMemberImage();
//            if(memberImage!=null&&!memberImage.equals("")){
//                ImageLoaderUtil.displayImage(context,iv_families_circle,memberImage,R.drawable.main_family,R.drawable.main_family,R.drawable.main_family);
//            }
//        }

    }

    public void setIv_familiesUnable() {
        if (iv_families != null) {
            iv_families.setOnClickListener(null);
        }
//        if (iv_families_circle != null){
//            iv_families_circle.setOnClickListener(null);
//        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.linear_back_title:
                if (onBackClickListener != null) {
                    onBackClickListener.onBackClicked();
                } else if (weakActivity != null) {
                    weakActivity.get().finish();
                }
                break;

            // 选择家庭成员
            case R.id.iv_families:
//                LoginControllor.requestLogin(weakActivity.get(), new OnCompleteListener() {
//                    @Override
//                    public void onComplete() {
//                        if (onLoginTrueLinstener != null) {
//                            onLoginTrueLinstener.onLoginTrue();
//                        }
//
//                        new ChooseMemberDialog(weakActivity.get(), onCompleteListener).show();
//                    }
//                });

                break;
            case R.id.iv_families_circle:
                //                注释掉家庭成员切换
//                LoginControllor.requestLogin(weakActivity.get(), new OnCompleteListener() {
//                    @Override
//                    public void onComplete() {
//                        if (onLoginTrueLinstener != null) {
//                            onLoginTrueLinstener.onLoginTrue();
//                        }
//
//                        new ChooseMemberDialog(weakActivity.get(), onCompleteListener).show();
//                    }
//                });
                break;

            // 消息
            case R.id.iv_newmessage:
                // 有消息点击进入
                intent = new Intent(weakActivity.get(), MessagesActivity.class);
                break;
            default:
                break;
        }
        if (intent != null) {
            weakActivity.get().startActivity(intent);
        }
    }

}
