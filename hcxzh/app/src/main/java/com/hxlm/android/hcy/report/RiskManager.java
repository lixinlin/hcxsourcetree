package com.hxlm.android.hcy.report;

import com.alibaba.fastjson.JSON;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.asynchttp.HcyHttpResponseHandler;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.hcyandroid.bean.VisceraIdentityResult;
import com.loopj.android.http.RequestParams;

public class RiskManager {

    public void getIdentityReportList(AbstractDefaultHttpHandlerCallback callback) {
        String url = "/result/IdentificationList.jhtml";

        RequestParams params = new RequestParams();



        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                return JSON.parseArray(content, VisceraIdentityResult.class);
            }
        }, LoginControllor.CHILD_MEMBER, callback.getContext());
    }

    public void getIdentityReportList(String memberid,String pageNumber,AbstractDefaultHttpHandlerCallback callback) {
        String url = "/result/IdentificationList.jhtml";

        RequestParams params = new RequestParams();
        params.put("cust_id", memberid);
        params.put("pageNumber", pageNumber);
        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                return JSON.parseArray(content, VisceraIdentityResult.class);
            }
        }, LoginControllor.CHILD_MEMBER, callback.getContext());
    }
}
