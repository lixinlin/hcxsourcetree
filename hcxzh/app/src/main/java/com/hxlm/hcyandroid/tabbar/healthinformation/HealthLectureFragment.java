package com.hxlm.hcyandroid.tabbar.healthinformation;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.hcyandroid.MessageType;
import com.hxlm.hcyandroid.ResponseStatus;
import com.hxlm.hcyandroid.bean.HealthLecture;
import com.hxlm.hcyandroid.datamanager.LectureManager;
import com.hxlm.hcyandroid.util.DialogUtils;
import com.hxlm.hcyandroid.util.SharedPreferenceUtil;
import com.hxlm.hcyandroid.util.ToastUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 健康讲座
 *
 * @author l
 */
public class HealthLectureFragment extends Fragment implements
        OnItemClickListener {

    private ListView mListView;
    private LinearLayout ll_health_no_data;//没有讲座
    private List<HealthLecture> lectures;
    private Context context;
    private Dialog waittingDialog;
    private LectureManager lectureManager;

    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (waittingDialog != null && waittingDialog.isShowing()) {
                waittingDialog.dismiss();
            }
            switch (msg.what) {
                case MessageType.MESSAGE_GET_LECTURE:
                    if (msg.arg1 == ResponseStatus.SUCCESS) {
                        lectures = (List<HealthLecture>) msg.obj;
                        if (lectures != null && lectures.size() > 0) {

                            ll_health_no_data.setVisibility(View.GONE);
                            mListView.setVisibility(View.VISIBLE);
                            mListView.setAdapter(new LectureAdapter(getActivity(), lectures));
                        } else {
                            ll_health_no_data.setVisibility(View.VISIBLE);
                            mListView.setVisibility(View.GONE);
                        }


                    } else if (msg.arg1 == ResponseStatus.UNLOGIN) {
                        ToastUtil.invokeShortTimeToast(context,
                                context.getString(R.string.unlogin));
                        SharedPreferenceUtil.logoff();//删除用户信息

                        LoginControllor.requestLogin(getActivity(), new OnCompleteListener() {
                            @Override
                            public void onComplete() {
                                if (waittingDialog == null) {
                                    waittingDialog = DialogUtils
                                            .displayWaitDialog(context);
                                }
                                if (!waittingDialog.isShowing()) {
                                    waittingDialog.show();
                                }
                                lectureManager.getLectures();
                            }
                        });


                    } else {
                        ToastUtil.invokeShortTimeToast(context, "获取健康讲座失败");
                    }
                    break;
                case MessageType.MESSAGE_QUERY_REMOTE_FAILED:
                    ToastUtil.invokeShortTimeToast(context,
                            context.getString(R.string.check_net));
                    break;
            }
        }
    };

    public HealthLectureFragment() {
        super();
        context = this.getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_health_lecture,
                container, false);

        mListView = (ListView) view.findViewById(R.id.lv_healthlecture);
        ll_health_no_data = (LinearLayout) view.findViewById(R.id.ll_health_no_data);

        initData();

        mListView.setOnItemClickListener(this);

        return view;
    }

    private void initData() {

    }

    /**
     * adapter
     */

    class LectureAdapter extends BaseAdapter {
        private Context context;
        private List<HealthLecture> listData;
        private LayoutInflater mInflater;

        public LectureAdapter(Context context, List<HealthLecture> listData) {
            this.context = context;
            this.listData = listData;
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return listData.size();
        }

        @Override
        public Object getItem(int arg0) {
            return listData.get(arg0);
        }

        @Override
        public long getItemId(int arg0) {
            return arg0;
        }

        @Override
        public View getView(int position, View view, ViewGroup arg2) {
            ViewHolder honler = null;
            if (view == null) {
                honler = new ViewHolder();
                view = mInflater.inflate(R.layout.list_health_lecture_item,
                        null);
                honler.userName = (TextView) view.findViewById(R.id.tvuserName);
                honler.lectureAddress = (TextView) view
                        .findViewById(R.id.tvlectureAddress);
                honler.lectureKeTi = (TextView) view
                        .findViewById(R.id.tvlectureKeTi);
                honler.lectureDate = (TextView) view
                        .findViewById(R.id.tvlectureDate);
                honler.lectureTime = (TextView) view
                        .findViewById(R.id.tvlectureTime);
                honler.lecturePrice = (TextView) view
                        .findViewById(R.id.tvlecturePrice);
                view.setTag(honler);

            } else {
                honler = (ViewHolder) view.getTag();
            }

            HealthLecture lecture = lectures.get(position);

            honler.userName.setText(lecture.getTalker());
            honler.lectureAddress.setText(lecture.getArea());
            honler.lectureKeTi.setText(lecture.getTitle());

            long beginDate = lecture.getBeginDate();
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
            String str_beginDate = format1.format(new Date(beginDate));
            honler.lectureDate.setText(str_beginDate);

            long lectureDate = lecture.getLectureDate();
            SimpleDateFormat format2 = new SimpleDateFormat("HH:mm");
            String str_lecString = format2.format(new Date(lectureDate));
            honler.lectureTime.setText(str_lecString);

            if (0.0 == lecture.getPrice()) {
                honler.lecturePrice.setText("免费");
                honler.lecturePrice.setTextColor(getResources().getColor(
                        R.color.lvse2));
            } else {
                honler.lecturePrice.setText("¥ " + lecture.getPrice() + "");
            }

            return view;
        }

    }

    class ViewHolder {
        private TextView userName;
        private TextView lectureAddress;
        private TextView lectureKeTi;
        private TextView lectureDate;
        private TextView lectureTime;
        private TextView lecturePrice;
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        // 点击跳转到详情页
        Intent i = new Intent(getActivity(), HealthLectureDetailsActivity.class);
        HealthLecture healthLecture = lectures.get(arg2);
        Bundle bundle = new Bundle();
        bundle.putSerializable("healthweb", healthLecture);

        i.putExtra("bundleweb", bundle);
        startActivity(i);

    }

    @Override
    public void onResume() {
        lectureManager = new LectureManager(handler);
        if (waittingDialog == null) {
            waittingDialog = DialogUtils.displayWaitDialog(getActivity());
        }
        if (!waittingDialog.isShowing()) {
            waittingDialog.show();
        }
        lectureManager.getLectures();
        super.onResume();
    }

}