package com.hxlm.hcyandroid.bean;

import java.io.Serializable;

public class Data3 implements Serializable {
    private int imageid;
    private String data3;
    private boolean isChoosed;
    private String strDregree;// 疾病程度


    public int getImageid() {
        return imageid;
    }

    public void setImageid(int imageid) {
        this.imageid = imageid;
    }

    public String getStrDregree() {
        return strDregree;
    }

    public void setStrDregree(String strDregree) {
        this.strDregree = strDregree;
    }

    public String getData3() {
        return data3;
    }

    public void setData3(String data3) {
        this.data3 = data3;
    }

    public boolean isChoosed() {
        return isChoosed;
    }

    public void setChoosed(boolean isChoosed) {
        this.isChoosed = isChoosed;
    }

    public Data3(String data3, boolean isChoosed) {
        super();
        this.data3 = data3;
        this.isChoosed = isChoosed;
    }

    @Override
    public String toString() {
        return "Data3 [imageid=" + imageid + ", data3=" + data3
                + ", isChoosed=" + isChoosed + ", strDregree=" + strDregree
                + "]";
    }


}
