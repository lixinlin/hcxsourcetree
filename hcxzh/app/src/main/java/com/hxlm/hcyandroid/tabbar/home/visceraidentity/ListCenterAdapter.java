package com.hxlm.hcyandroid.tabbar.home.visceraidentity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.hcy.ky3h.R;
import com.hxlm.hcyandroid.bean.ICDItem;

import java.util.List;


// 中间adapter
public class ListCenterAdapter extends BaseAdapter {

    private Context context;
    private int position = 0;

    // 用来导入布局
    private LayoutInflater inflater = null;

    public List<ICDItem> listMoreData;

    public ListCenterAdapter(Context context, List<ICDItem> listMoreData) {
        this.context = context;
        this.listMoreData = listMoreData;
        inflater = LayoutInflater.from(context);

    }

    public int getCount() {
        if (listMoreData.size() == 0) {
            return 0;
        }
        return listMoreData.size();
    }

    public Object getItem(int position) {
        return listMoreData.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder hold = null;
        if (view == null) {
            hold = new ViewHolder();
            view = inflater.inflate(R.layout.list_center_disease_data_item,
                    null);
            hold.txt = (TextView) view.findViewById(R.id.moreitem_txt);
            hold.cb = (ImageView) view.findViewById(R.id.item_cb);
            view.setTag(hold);
        } else {
            hold = (ViewHolder) view.getTag();
        }

        ICDItem data3 = listMoreData.get(position);

        hold.txt.setText(data3.getName());

        if (data3.isChoosed()) {
            hold.cb.setVisibility(View.VISIBLE);
            hold.cb.setImageResource(R.drawable.icd10_checkbox);
        } else {
            hold.cb.setVisibility(View.GONE);
        }

        return view;
    }

    public void setSelectItem(int position) {
        this.position = position;
    }

    public class ViewHolder {
        TextView txt;
        ImageView cb;
    }

}
