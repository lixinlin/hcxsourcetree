package com.hxlm.hcyandroid.tabbar.healthinformation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.user.UserManager;
import com.hxlm.android.hcy.utils.SpUtils;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.bean.HealthInformation;
import com.hxlm.hcyandroid.bean.HealthInformationRes;
import com.hxlm.hcyandroid.tabbar.InformationFragment;
import com.hxlm.hcyandroid.util.ToastUtil;
import com.hxlm.hcyphone.adapter.HealthCultureAdapter;
import com.hxlm.hcyphone.okhttp.HttpCallbackHandle;
import com.hxlm.hcyphone.okhttp.HttpClient;
import com.hxlm.hcyphone.okhttp.HttpConstant;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

import java.text.SimpleDateFormat;
import java.util.List;

public class HealthInformationFragment extends Fragment implements
        OnItemClickListener {

    private ListView lv_healthculture;
    private List<HealthInformation> healthInformations;
    private int id; //通过该id去请求数据
    private Context context;
    private InformationAdapter adapter;
    private  RelativeLayout rl_no_data;
    private RecyclerView rv_health_culture;
    private int pageNumber = 1;
    private HealthCultureAdapter healthCultureAdapter;

    public HealthInformationFragment() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.context = this.getActivity();
        View view = inflater.inflate(R.layout.activity_health_information_fragment,
                container, false);
        lv_healthculture =  view.findViewById(R.id.lv_healthculture);
        rv_health_culture = view.findViewById(R.id.rv_health_culture);
        rv_health_culture.setLayoutManager(new LinearLayoutManager(getActivity()));

        rv_health_culture.addOnScrollListener(new RecyclerView.OnScrollListener() {
            private int count;
            private int last;

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (last == count - 1 && newState == RecyclerView.SCROLL_STATE_IDLE) {
                    pageNumber++;
                    getHealthCultureData();
                    Log.e("retrofit","====健康资讯集合onScroll==="+healthInformations.toString());
                    int totalPage = (int) SpUtils.get(getActivity(), "totalPage", 1);
                    if (pageNumber > totalPage ){
                        ToastUtil.invokeShortTimeToast(getActivity(),getString(R.string.body_diagnose_not_more_data));
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                RecyclerView.LayoutManager manager = recyclerView.getLayoutManager();
                if (manager.getChildCount() > 0) {
                    count = manager.getItemCount();
                    last = ((RecyclerView.LayoutParams) manager
                            .getChildAt(manager.getChildCount() - 1).getLayoutParams()).getViewAdapterPosition();

                }
            }

        });

        rl_no_data = view.findViewById(R.id.rl_no_data);
//        initData();
        getHealthCultureData();

        return view;
    }


    public void getHealthCultureData(){
         Log.e("retrofit","==健康资讯id=="+id);
         if (id == InformationFragment.NEW_INFORMATION_TAG) {
             new UserManager().getNewInformation2(getActivity(), pageNumber, new AbstractDefaultHttpHandlerCallback(getActivity()) {
                 @Override
                 protected void onResponseSuccess(Object obj) {
                     if (pageNumber == 1) {
                         healthInformations = (List<HealthInformation>) obj;
                     } else {
                         healthInformations.addAll((List<HealthInformation>) obj);
                     }
                     if (healthInformations.size()==0){
                         rv_health_culture.setVisibility(View.GONE);
                         rl_no_data.setVisibility(View.VISIBLE);
                     }else {
                         if (healthCultureAdapter == null) {
                             healthCultureAdapter = new HealthCultureAdapter(getActivity(), healthInformations);
                             rv_health_culture.setAdapter(healthCultureAdapter);
                             healthCultureAdapter.setOnItemClick(new HealthCultureAdapter.OnItemClick() {
                                 @Override
                                 public void onItem(int position, HealthInformation healthInformation) {
                                     Intent intent = new Intent(getActivity(),
                                             ShowHealthInformationActivity.class);
                                     intent.putExtra("healthInformation",
                                             healthInformations.get(position));
                                     context.startActivity(intent);
                                 }
                             });
                         } else {
                             healthCultureAdapter.notifyDataSetChanged();
                         }
                     }
                 }
             });
         }else {
             String url = Constant.BASE_URL + "/article/healthListByCategory/" + id + ".jhtml";
             HttpClient.getInstance().httpGet(url, null, new HttpCallbackHandle<HealthInformationRes>() {
                 @Override
                 public void onSuccess(HealthInformationRes obj) {
                     healthInformations = obj.getData();
                     if (healthInformations.size()==0){
                         rv_health_culture.setVisibility(View.GONE);
                         rl_no_data.setVisibility(View.VISIBLE);
                     }else {
                         if (healthCultureAdapter == null) {
                             healthCultureAdapter = new HealthCultureAdapter(getActivity(), healthInformations);
                             rv_health_culture.setAdapter(healthCultureAdapter);
                             healthCultureAdapter.setOnItemClick(new HealthCultureAdapter.OnItemClick() {
                                 @Override
                                 public void onItem(int position, HealthInformation healthInformation) {
                                     Intent intent = new Intent(getActivity(),
                                             ShowHealthInformationActivity.class);
                                     intent.putExtra("healthInformation",
                                             healthInformations.get(position));
                                     context.startActivity(intent);
                                 }
                             });
                         } else {
                             healthCultureAdapter.notifyDataSetChanged();
                         }
                     }

                 }

                 @Override
                 public void onFailure(String errorCode, String errorMessage) {

                 }
             });
         }
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

    }

    // 健康文化的adapter
    class InformationAdapter extends BaseAdapter {

        private LayoutInflater inflater;
        private Holder holder;
        private List<HealthInformation> informations;
        private Context context;

        public InformationAdapter(Context context,
                                  List<HealthInformation> informations) {
            super();
            this.context = context;
            this.informations = informations;
        }

        @Override
        public int getCount() {
            return informations.size();
        }

        @Override
        public Object getItem(int position) {
            return informations.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                holder = new Holder();
                inflater = LayoutInflater.from(context);
                convertView = inflater.inflate(R.layout.health_culture_item,
                        null);
                holder.iv_information_Icon = (ImageView) convertView
                        .findViewById(R.id.iv_information_icon);
                holder.tv_information_title = (TextView) convertView
                        .findViewById(R.id.tv_information_title);
                holder.tv_information_date = (TextView) convertView
                        .findViewById(R.id.tv_information_date);
                holder.tv_information_desc = (TextView) convertView
                        .findViewById(R.id.tv_information_desc);
                convertView.setTag(holder);
            } else {
                holder = (Holder) convertView.getTag();
            }

            HealthInformation information = informations.get(position);

            holder.tv_information_title.setText(information.getTitle());
            String desc = information.getSeoDescription();
            if (!TextUtils.isEmpty(desc)) {
                holder.tv_information_desc.setText(desc);
            }
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            holder.tv_information_date.setText(format.format(information
                    .getCreateDate()));

            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .showImageOnLoading(R.drawable.health_information_defult)
                    .showImageForEmptyUri(R.drawable.health_information_defult)
                    .showImageOnFail(R.drawable.health_information_defult)
                    .cacheOnDisk(true)
                    .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                    .cacheInMemory(false).build();

            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                    getActivity()).denyCacheImageMultipleSizesInMemory()
                    .threadPriority(Thread.NORM_PRIORITY - 2)
                    .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                    .tasksProcessingOrder(QueueProcessingType.LIFO)
                    .memoryCache(new LruMemoryCache(8 * 1024 * 1024))
                    .diskCacheSize(50 * 1024 * 1024).threadPoolSize(3).build();

            ImageLoader imageLoader = ImageLoader.getInstance();
            imageLoader.init(config);
            imageLoader.displayImage(information.getPicture(),
                    holder.iv_information_Icon, options);

            return convertView;
        }
    }

    static class Holder {
        ImageView iv_information_Icon;
        TextView tv_information_title;
        TextView tv_information_date;
        TextView tv_information_desc;
    }

}
