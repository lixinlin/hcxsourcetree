package com.hxlm.hcyandroid.bean;

/**
 * 心电数据
 *
 * @author Administrator
 */
public class ECGData {
    private String path;
    private int heartRate;
    private String subjectSn;

    public ECGData(){}

    public ECGData(String path, int heartRate, String subjectSn) {
        this.path = path;
        this.heartRate = heartRate;
        this.subjectSn = subjectSn;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int heartRate) {
        this.heartRate = heartRate;
    }

    public String getSubjectSn() {
        return subjectSn;
    }

    public void setSubjectSn(String subjectSn) {
        this.subjectSn = subjectSn;
    }

    @Override
    public String toString() {
        return "ECGData{" +
                "path='" + path + '\'' +
                ", heartRate=" + heartRate +
                ", subjectSn='" + subjectSn + '\'' +
                '}';
    }
}
