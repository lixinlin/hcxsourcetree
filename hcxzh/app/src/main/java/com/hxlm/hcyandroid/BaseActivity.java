package com.hxlm.hcyandroid;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.hcy.ky3h.R;
import com.hxlm.hcyandroid.bean.DeviceInfo;
import com.hxlm.hcyandroid.util.AppManager;
import com.hxlm.hcyandroid.util.StatusBarUtils;
import com.hxlm.hcyandroid.util.ToastUtil;

import java.util.Locale;

public abstract class BaseActivity extends Activity {
    public static final String ACTION_EXIT = "exit_system";
    public static DeviceInfo deviceInfo;
    private ExitReceiver mBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StatusBarUtils.setWindowStatusBarColor(this,R.color.white);
        //初始化
        setContentView();
        IntentFilter exitIntentFilter = new IntentFilter();
        exitIntentFilter.addAction(ACTION_EXIT);
        //注册广播
        mBroadcastReceiver = new ExitReceiver();
        registerReceiver(mBroadcastReceiver, exitIntentFilter);

        AppManager.getInstance().addActivity(this);

        initViews();
        initDatas();
        if (deviceInfo == null) {
            deviceInfo = new DeviceInfo();

            DisplayMetrics dm = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(dm);

            Locale locale = getResources().getConfiguration().locale;
            deviceInfo.setUserAgentName(getString(R.string.user_agent_name));
            deviceInfo.setSoftver(getString(R.string.version));
            deviceInfo.setWidthPixels(dm.widthPixels);
            deviceInfo.setHeightPixels(dm.heightPixels);
            deviceInfo.setLanguage(locale.getLanguage());
            deviceInfo.setCountry(locale.getCountry());
        }
      //  BaseApplication.getInstance().addActivity(this);
    }

    // 加载布局
    public abstract void setContentView();

    // 初始化views
    public abstract void initViews();

    // 初始化数据
    public abstract void initDatas();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mBroadcastReceiver);
    }

    /**
     * Android点击空白区域，隐藏输入法软键盘
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (isShouldHideKeyboard(v, ev)) {
                hideKeyboard(v.getWindowToken());
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时则不能隐藏
     */
    private boolean isShouldHideKeyboard(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0],
                    top = l[1],
                    bottom = top + v.getHeight(),
                    right = left + v.getWidth();
            return !(event.getX() > left && event.getX() < right
                    && event.getY() > top && event.getY() < bottom);
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditText上，和用户用轨迹球选择其他的焦点
        return false;
    }

    /**
     * 获取InputMethodManager，隐藏软键盘
     */
    private void hideKeyboard(IBinder token) {
        if (token != null) {
            InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(token, InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


    public void showMessage(String message) {
        ToastUtil.invokeShortTimeToast(BaseApplication.getContext(), message);
    }

    public class ExitReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    }
}
