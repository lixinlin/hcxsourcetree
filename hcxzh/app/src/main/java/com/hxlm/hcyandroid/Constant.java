package com.hxlm.hcyandroid;

import android.os.Environment;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.content.ResourceItem;
import com.hxlm.android.hcy.view.TitleBarView;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


public class Constant {
    public static final boolean ISSHOW = false;//是否显示自己写的log
    public static final boolean DEBUG = false;//添加标识，是否打印Log
    public static final boolean isoemPhone = true;//添加标识，是否是定制手机
    public static int loginType = 0; //1 短信登录/账号密码登录 2微信登录

    public static final String PRODUCTION_SYSTEM_URL = "http://eky3h.com/healthlm";//正式版
//    public static final String TEST_SYSTEM_URL = "http://119.254.24.4:7006";//测试版




//    public static final String TEST_SYSTEM_URL = "http://47.92.73.99:8080/healthlm";//测试版
//    public static final String TEST_SYSTEM_URL = "http://10.1.71.104:8080/hcy";//kangkang测试版
//    public static final String TEST_SYSTEM_URL = "http://10.1.71.121:8080/hcy-system";//limingwei
//    public static final String TEST_SYSTEM_URL = "http://10.1.71.52:8888/hcy-system";//gaolaoban测试版
//    public static final String TEST_SYSTEM_URL = "http://10.1.71.113:8888/hcy-system"; // chenshichao 测试版
//    public static final String TEST_SYSTEM_URL = "http://10.1.71.38:8888/hcy-system"; // liuyuanjie 测试版
//        public static final String TEST_SYSTEM_URL = "http://192.168.2.11:8888/hcy-system"; // liuyuanjie 测试版
        public static final String TEST_SYSTEM_URL = "http://10.1.71.181:8080/healthlm";

    public static final String BASE_URL = PRODUCTION_SYSTEM_URL;

    public static final String PRODUCTION_SYSTEM_COOKIE = "http://eky3h.com";//正式版的Cookie
//    public static final String TEST_SYSTEM_COOKIE = "http://119.254.24.4:7006";//测试版的Cookie

    public static final String TEST_SYSTEM_COOKIE = "http://47.92.73.99:8080";//测试版的Cookie
//    public static final String TEST_SYSTEM_COOKIE = "http://10.1.71.38:8888/hcy-system"; //liuyuanjie测试版Cookie
//    public static final String TEST_SYSTEM_COOKIE = "http://10.1.71.113:8888/hcy-system"; //chenshichao  测试版Cookie
//    public static final String TEST_SYSTEM_COOKIE = "http://10.1.71.121:8080/hcy-system"; //limingwei  测试版Cookie
//public static final String TEST_SYSTEM_COOKIE = "http://192.168.2.11:8888/hcy-system/"; // liuyuanjie 测试版



    public static final String PRODUCTION_SYSTEM_HEADER = "hcy_android_oem-oem-";//正式版
//    public static final String TEST_SYSTEM_HEADER = "android_phone_hcy-yh-";//普通测试版
    public static final String TEST_SYSTEM_HEADER = "hcy_android_oem-oem-";//http://47.92.73.99:8080


    public static final String BASE_PATH = Environment.getExternalStorageDirectory().getPath() + "/HCYAndroid";
    public static final String SAVED_IMAGE_DIR_PATH = BASE_PATH + "/Image";
    public static final String UPLOAD_PATH = BASE_PATH + "/" + "upload";
    public static final String YUEYAO_PATH = BASE_PATH + "/" + "yueyao";
    public static final String SHIFANYIN_PATH = BASE_PATH + "/" + "shifanyin";
    public static final String APK_PATH = BASE_PATH + "/" + "download";

    // 健康顾问总星级(取消，现在不设定上限)
    public static final int TOTAL_STAR = 5;
    public static final List<ResourceItem> LIST = new ArrayList<>();//保存加入购物车里面的乐药
    public static final List<ResourceItem> LIST_DELETE_YUEYAO=new ArrayList<>();//保存在乐药结算那一步删除的乐药


    public static final String OTHER_PAY_SEND_TYPE_ACTION = "com.hcy.ky3h.intent.OtherPay";

    public final static String BODY_INTRODUCE = BASE_URL + "/upload/article/content/201512/54/1.html";//体质介绍
    public final static String VISCERA_INTRODUCE = BASE_URL + "/upload/article/content/201512/56/1.html";//脏腑介绍
    public final static String MERIDIAN_INTRODUCE = BASE_URL + "/upload/article/content/201512/55/1.html";//经络介绍
    public final static String RISK_INTRODUCE = BASE_URL + "/upload/article/content/201512/58/1.html";//风险介绍

    //	public final static String BODY_INTRODUCE = "12";//体质介绍
//	public final static String VISCERA_INTRODUCE = "13";//脏腑介绍
//	public final static String MERIDIAN_INTRODUCE = "14";//经络介绍
//	public final static String RISK_INTRODUCE = "15";//风险介绍
    public final static String ABOUT_ME = BASE_URL + "/upload/article/content/201602/60/1.html";//关于我们
    public final static String PRIVICY_POLICY = BASE_URL + "/upload/article/content/201602/61/1.html";//隐私条款
    public final static String MEMNER_ZHANGCHENG = BASE_URL + "/upload/article/content/201602/62/1.html";//会员章程
    public final static String STATEMENT = BASE_URL + "/upload/article/content/201602/63/1.html";//声明
    public final static String HELP_US = BASE_URL + "/upload/article/content/201602/64/1.html";//帮助
    public final static String ABOUT_ME_EN = BASE_URL + "/upload/article/content/201903/60/1.html";//关于我们
    public final static String PRIVICY_POLICY_EN = BASE_URL + "/upload/article/content/201903/61/1.html";//隐私条款
    public final static String MEMNER_ZHANGCHENG_EN = BASE_URL + "/upload/article/content/201903/62/1.html";//会员章程
    public final static String STATEMENT_EN = BASE_URL + "/upload/article/content/201903/63/1.html";//声明
    public final static String HELP_US_EN = BASE_URL + "/upload/article/content/201903/64/1.html";//帮助

    public static final int UNDEFINED = -1;

    //以下为功法图片文案相关
    //预备动作
    public static final String yubeidongzuo = BaseApplication.getContext().getString(R.string.constant_yubeidongzuo);
    // 起式
    public static final String qishi = BaseApplication.getContext().getString(R.string.constant_qishi);
    // 第一式 箭指长天
    public static final String diyishi_jianzhichangtian_1  = BaseApplication.getContext().getString(R.string.constant_diyishi_jianzhichangtian_1);
    public static final String diyishi_jianzhichangtian_2 = BaseApplication.getContext().getString(R.string.constant_diyishi_jianzhichangtian_2);
    public static final String diyishi_jianzhichangtian_3 = BaseApplication.getContext().getString(R.string.constant_diyishi_jianzhichangtian_3);
    // 第二式 海底捞月
    public static final String diershi_haidianlaoyue_1 = BaseApplication.getContext().getString(R.string.constant_diershi_haidianlaoyue_1);
    public static final String diershi_haidianlaoyue_2 = BaseApplication.getContext().getString(R.string.constant_diershi_haidianlaoyue_2);
    public static final String diershi_haidianlaoyue_3 = BaseApplication.getContext().getString(R.string.constant_diershi_haidianlaoyue_3);
    // 第三式 太极云手
    public static final String disanshi_taijiyunshou_1 = BaseApplication.getContext().getString(R.string.constant_disanshi_taijiyunshou_1);
    public static final String disanshi_taijiyunshou_2 = BaseApplication.getContext().getString(R.string.constant_disanshi_taijiyunshou_2);
    public static final String disanshi_taijiyunshou_3 = BaseApplication.getContext().getString(R.string.constant_disanshi_taijiyunshou_3);
    // 第四式 高山流水
    public static final String disishi_gaoshanliushui_1 = BaseApplication.getContext().getString(R.string.constant_disishi_gaoshanliushui_1);
    public static final String disishi_gaoshanliushui_2 = BaseApplication.getContext().getString(R.string.constant_disishi_gaoshanliushui_2);
    // public static final String diershi_haidianlaoyue_4 =
    // "于极限处保持3分钟，此时腘窝会出现紧绷感，可拉抻足少阴肾经，有助于疏通此处的 “筋结”，能明显缓解腰背痛。";
    // public static final String diershi_haidianlaoyue_5 =
    // "身体缓缓归于正位，双臂放松，回归体侧。";
    public static final String disishi_gaoshanliushui_3 = BaseApplication.getContext().getString(R.string.constant_disishi_gaoshanliushui_3);
    // 第五式 俯身探海
    public static final String diwushi_wushentanhai_1 = BaseApplication.getContext().getString(R.string.constant_diwushi_wushentanhai_1);
    public static final String diwushi_wushentanhai_2 = BaseApplication.getContext().getString(R.string.constant_diwushi_wushentanhai_2);
    public static final String diwushi_wushentanhai_3 = BaseApplication.getContext().getString(R.string.constant_diwushi_wushentanhai_3);
    // 第六式 俯身抱月
    public static final String diliushi_fushenbaoyue_1 = BaseApplication.getContext().getString(R.string.constant_diliushi_fushenbaoyue_1);
    public static final String diliushi_fushenbaoyue_2 = BaseApplication.getContext().getString(R.string.constant_diliushi_fushenbaoyue_2);
    // public static final String disishi_gaoshanliushui_4 = "身体缓缓归于正位，调整呼吸。";
    // 收式
    public static final String shoushi = BaseApplication.getContext().getString(R.string.constant_shoushi);

    public final static String MONEY_PREFIX = "￥";
    public final static DecimalFormat decimalFormat = new DecimalFormat("#.##");

    public static List<WeakReference<TitleBarView>> titleBarViews = new ArrayList<>();
    public static final int API_SUCCESS_CODE = 100;
    public static final int API_UNLOGIN = 44;
    public static boolean isEnglish = false;




}
