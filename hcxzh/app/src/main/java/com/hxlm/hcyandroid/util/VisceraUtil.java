package com.hxlm.hcyandroid.util;

import com.hxlm.hcyandroid.bean.Symptom;

import java.util.ArrayList;
import java.util.List;

/**
 * 症状
 *
 * @author l
 */
public class VisceraUtil {
    public static List<Symptom> initSymptom() {
        List<Symptom> list = new ArrayList<>();
        //全身
        list.add(new Symptom(63, "全身", "出汗", "自汗", 2, "ZH", "清醒时出汗，活动时汗出明显"));
        list.add(new Symptom(64, "全身", "出汗", "盗汗", 2, "DH", "睡后出汗，醒则汗止"));
        list.add(new Symptom(65, "全身", "出汗", "新病无汗", 2, "XBWH", "新发疾病，没有出过汗"));
        list.add(new Symptom(66, "全身", "出汗", "新病有汗", 2, "XBYH", "新发疾病，已经出过汗"));
        list.add(new Symptom(67, "全身", "出汗", "壮热无汗", 2, "ZRWH", "高烧不退而没有出汗"));
        list.add(new Symptom(69, "全身", "出汗", "热甚汗多", 2, "RSHD", "发热明显，出汗多"));
        list.add(new Symptom(70, "全身", "出汗", "暑天汗多", 2, "STHD", "暑天气候炎热而出汗多"));
        list.add(new Symptom(73, "全身", "出汗", "冷汗淋漓", 2, "LHLL",
                "汗出清稀如水而身凉。又名冷汗"));
        list.add(new Symptom(75, "全身", "出汗", "病重大汗", 2, "BZDH", "病重危重时大汗出"));
        list.add(new Symptom(77, "全身", "出汗", "半身汗出", 2, "BSHC",
                "半边或半截肢体不出汗。又名半身无汗"));
        list.add(new Symptom(78, "全身", "出汗", "汗出不彻", 2, "HCBC", "虽有一点汗出，但汗出欠通畅"));
        list.add(new Symptom(79, "全身", "出汗", "出虚汗或易出汗", 2, "CXHHYCH",
                "体质虚弱而容易出汗"));
        list.add(new Symptom(361, "全身", "出血", "出血浅淡", 2, "CXQD",
                "所出之血质较稀薄、颜色较淡"));
        list.add(new Symptom(364, "全身", "出血", "出血色黯成块", 2, "CXSACK",
                "所出之血呈紫暗色、成块状"));
        list.add(new Symptom(366, "全身", "出血", "慢性出血", 2, "MXCX",
                "泛指慢性、小量、持续的出血"));
        list.add(new Symptom(494, "全身", "精神状态", "形体蜷卧", 2, "XTJW",
                "睡或坐时，身体喜欢卷曲"));
        list.add(new Symptom(179, "全身", "精神状态", "倦怠乏力", 2, "JDFL",
                "自觉疲倦而全身没有气力"));
        list.add(new Symptom(194, "全身", "精神状态", "精神疲倦", 2, "JSPJ", "精神疲倦"));
        list.add(new Symptom(200, "全身", "精神状态", "善悲易哭", 2, "SBYK",
                "容易出现悲哀情绪而好哭流泪"));
        list.add(new Symptom(201, "全身", "精神状态", "心烦", 2, "XF", "自觉情绪烦乱"));
        list.add(new Symptom(202, "全身", "精神状态", "烦躁", 2, "FZ2", "因情绪烦乱而躁动不安"));
        list.add(new Symptom(203, "全身", "精神状态", "急躁易怒", 2, "JZYN",
                "性情急、情绪躁动，容易发怒"));
        list.add(new Symptom(204, "全身", "精神状态", "胆怯易惊", 2, "DQYJ",
                "胆量小而容易产生惊恐、害怕情绪"));
        list.add(new Symptom(205, "全身", "精神状态", "情志抑郁或忧虑、孤僻", 2, "QZYYHYLGP",
                "情绪低落、不高兴，忧愁、考虑某事物而难以分散精力"));
        list.add(new Symptom(206, "全身", "精神状态", "喜叹气", 2, "XTQ",
                "常不自觉地在平常呼吸之后有延长和间歇的深长呼吸。又名太息"));
        list.add(new Symptom(207, "全身", "精神状态", "情绪易激动", 2, "QXYJD", "情绪容易激动"));
        list.add(new Symptom(371, "全身", "精神状态", "喜呵欠", 2, "XHQ", "时常打呵欠"));
        list.add(new Symptom(407, "全身", "精神状态", "渐入昏迷", 2, "JRHM", "逐渐出现神识不清"));
        list.add(new Symptom(408, "全身", "精神状态", "晕倒", 2, "YD",
                "先有头晕感，然后仆倒而不省人事"));
        list.add(new Symptom(409, "全身", "精神状态", "突然昏仆", 2, "TRHP",
                "无预感而突然仆倒，不省人事"));
        list.add(new Symptom(410, "全身", "精神状态", "躁扰不宁", 2, "ZRBN", "神情烦躁，躁动不安"));
        list.add(new Symptom(411, "全身", "精神状态", "神志错乱", 2, "SZCL",
                "哭笑无常、默默独语等，精神错乱失常"));
        list.add(new Symptom(412, "全身", "精神状态", "神志狂乱", 2, "SZKL",
                "打人毁物、裸体叫骂等，精神狂乱失常"));
        list.add(new Symptom(413, "全身", "精神状态", "神志痴呆", 2, "SZCD",
                "神志痴呆，反应不灵，意识欠清（给你发的中有解释）"));
        list.add(new Symptom(414, "全身", "精神状态", "昏迷吐涎沫", 2, "HMTXM",
                "神识不清而有痰涎从口中溢出"));
        list.add(new Symptom(1012, "全身", "精神状态", "神疲动后稍舒", 2, "SPDHSS", ""));
        list.add(new Symptom(41, "全身", "皮肤", "经常恶风", 2, "JCEF", "经常有怕风怕冷的感觉"));
        list.add(new Symptom(42, "全身", "皮肤", "容易感冒", 2, "RYGM", "经常、容易患感冒"));
        list.add(new Symptom(188, "全身", "皮肤", "皮肤瘙痒", 2, "PFSY", "皮肤瘙痒的感觉"));
        list.add(new Symptom(360, "全身", "皮肤", "紫斑", 2, "ZB",
                "皮肤出现不高出肤面的青紫色或紫红色斑块，不伴发热症状"));
        list.add(new Symptom(475, "全身", "皮肤", "甲状腺肿大", 2, "JZXZD",
                "颈部结喉处（甲状腺）肿大"));
        list.add(new Symptom(499, "全身", "皮肤", "身黄", 2, "SH1", "身体（包括面部）皮肤发黄"));
        list.add(new Symptom(500, "全身", "皮肤", "皮肤色素沉着", 2, "PFSSCZ",
                "皮肤颜色变褐、暗黑"));
        list.add(new Symptom(501, "全身", "皮肤", "肌肤甲错", 2, "JFJC",
                "皮肤干燥粗糙呈褐色，状若鱼鳞"));
        list.add(new Symptom(506, "全身", "皮肤", "水痘", 2, "SD1",
                "小儿皮肤出现粉红色斑丘疹，很快变成椭圆形小水疱"));
        list.add(new Symptom(507, "全身", "皮肤", "出疹", 2, "CZ", "皮肤出现红色或紫红色、粟粒状疹点"));
        list.add(new Symptom(508, "全身", "皮肤", "风疹", 2, "FZ1", "皮肤骤然出现细小淡红色的疹点"));
        list.add(new Symptom(509, "全身", "皮肤", "风团", 2, "FT1", "皮肤骤然出现隆起的斑丘团块"));
        list.add(new Symptom(511, "全身", "皮肤", "痈疖痱子", 2, "YJFZ", "肌肤生痈、疖、痱子等"));
        list.add(new Symptom(512, "全身", "皮肤", "疱疹", 2, "PZ", "皮肤出现水疱状疹点"));
        list.add(new Symptom(535, "全身", "皮肤", "毛发脱落", 2, "MFTL", "头发甚至毫毛脱落"));
        list.add(new Symptom(538, "全身", "皮肤", "皮肤红斑", 2, "PFHB",
                "皮肤出现不高出肤面的红色斑块"));
        list.add(new Symptom(616, "全身", "皮肤", "皮肤弹性差", 2, "PFTXC",
                "以手牵起皮肤，放开后不即刻恢复"));
        list.add(new Symptom(617, "全身", "皮肤", "皮肤粗厚硬肿", 2, "PFCHYZ",
                "皮肤变得粗糙、肿厚，甚至变硬"));
        list.add(new Symptom(620, "全身", "皮肤", "皮肤干燥", 2, "PFGZ", "皮肤干燥而枯涩不润"));
        list.add(new Symptom(110, "全身", "身体", "身痛", 2, "ST", "泛指没有明确部位的身体疼痛"));
        list.add(new Symptom(180, "全身", "身体", "气下坠感", 2, "QXZG", "自觉气往下坠"));
        list.add(new Symptom(181, "全身", "身体", "气上冲感", 2, "QSCG", "自觉似有气流向上冲顶"));
        list.add(new Symptom(183, "全身", "身体", "身体酸(困)重", 2, "STSKZ",
                "身体酸软沉重的感觉"));
        list.add(new Symptom(189, "全身", "身体", "筋惕肉目闰", 2, "JTRMR",
                "筋肉偶尔不自主的抽掣跳动"));
        list.add(new Symptom(295, "全身", "身体", "矢气无", 2, "SQW",
                "没有肠道内气体从肛门排出时发出的声响"));
        list.add(new Symptom(433, "全身", "身体", "囟门突起", 2, "XMTQ", "小儿囟门凸起"));
        list.add(new Symptom(434, "全身", "身体", "囟门凹陷", 2, "XMAX", "小儿囟门凹陷"));
        list.add(new Symptom(483, "全身", "身体", "内脏下垂", 2, "NZXC",
                "胃、肝、肾等内脏的位置下垂"));
        list.add(new Symptom(495, "全身", "身体", "反复水肿", 2, "FFSZ",
                "长期或反复发作的水肿，按之凹陷不能即起"));
        list.add(new Symptom(496, "全身", "身体", "新起水肿", 2, "XQSZ",
                "突然起、时间短的水肿，按之凹陷不能即起"));
        list.add(new Symptom(497, "全身", "身体", "水肿", 2, "SZ", "泛指未标明新久等的水肿"));
        list.add(new Symptom(498, "全身", "身体", "局限性水肿", 2, "JXXSZ",
                "某局部出现不对称的水肿"));
        list.add(new Symptom(503, "全身", "身体", "丝状红缕", 2, "SZHL",
                "某些局部皮肤的细小血管充盈，如红丝缠绕"));
        list.add(new Symptom(510, "全身", "身体", "白疒咅", 2, "BNP",
                "胸颈部皮肤出现晶莹如粟，高出皮肤，内含浆液，擦破流水的疱疹"));
        list.add(new Symptom(513, "全身", "身体", "患部溃烂", 2, "HBKL", "患病处肌肤出现溃烂"));
        list.add(new Symptom(514, "全身", "身体", "患部红肿", 2, "HBHZ", "患病处肌肤色红而肿起"));
        list.add(new Symptom(515, "全身", "身体", "脓肿或流脓", 2, "NZHLN",
                "肢体肌肤出现脓肿或流脓液"));
        list.add(new Symptom(516, "全身", "身体", "角弓反张", 2, "JGFZ", "项部拘急强硬向后仰。"));
        list.add(new Symptom(518, "全身", "身体", "惊跳", 2, "JT", "突然受惊而肢体跳动"));
        list.add(new Symptom(526, "全身", "身体", "半身不遂", 2, "BSBS",
                "一侧肢体不能随意活动。又名偏瘫"));
        list.add(new Symptom(536, "全身", "身体", "脱屑或皲裂", 2, "TXHJL",
                "脚或手、口唇等处脱屑或出现皲裂"));
        list.add(new Symptom(539, "全身", "身体", "渗液流脂水", 2, "SYLZS",
                "创面或患处有脂样液体渗出"));
        list.add(new Symptom(247, "全身", "食欲", "纳呆恶食", 2, "NDES",
                "无饥饿、无要求进食之感，可食可不食，甚至厌恶进食"));
        list.add(new Symptom(249, "全身", "食欲", "久不欲食", 2, "JBYS1",
                "长期不想进食。又名食欲不振、纳谷不香"));
        list.add(new Symptom(250, "全身", "食欲", "长期食少", 2, "CQSS", "长期实际进食量减少"));
        list.add(new Symptom(251, "全身", "食欲", "进食无味", 2, "JSWW", "进食无欣快感，口中无味"));
        list.add(new Symptom(252, "全身", "食欲", "食后痞胀", 2, "SHPZ", "进食后脘腹部有痞胀感"));
        list.add(new Symptom(253, "全身", "食欲", "饥不欲食", 2, "JBYS",
                "虽有饥饿感，但不欲进食或进食不多"));
        list.add(new Symptom(254, "全身", "食欲", "多食易饥", 2, "DSYJ",
                "食欲亢进，每次进食量多，且容易饥饿。又名消谷善饥"));
        list.add(new Symptom(255, "全身", "食欲", "厌油腻", 2, "YYN", "不愿吃油腻食品、闻油烟气味"));
        list.add(new Symptom(256, "全身", "食欲", "嗜食异物", 2, "SSYW",
                "喜欢吃盐、木炭、谷、米等特殊物质"));
        list.add(new Symptom(195, "全身", "睡眠", "失眠", 2, "SM", "不易入睡或睡后易醒，睡眠时间减少"));
        list.add(new Symptom(196, "全身", "睡眠", "多梦", 2, "DM", "睡后容易作梦、做梦多"));
        list.add(new Symptom(197, "全身", "睡眠", "睡眠不实", 2, "SMBS", "睡眠不深，容易醒、时时醒"));
        list.add(new Symptom(198, "全身", "睡眠", "健忘", 2, "JW", "记忆力减退"));
        list.add(new Symptom(199, "全身", "睡眠", "嗜睡", 2, "SS",
                "想睡，容易入睡，睡眠时间增加。又名多寐"));
        list.add(new Symptom(26, "全身", "体温", "新起微发热", 2, "XQWFR", "新起有轻微发烧感觉"));
        list.add(new Symptom(27, "全身", "体温", "微恶风寒", 2, "WEFH",
                "新起有轻微怕冷的感觉，避风可缓。又名恶风"));
        list.add(new Symptom(28, "全身", "体温", "新起恶寒重", 2, "XQEHZ",
                "新起自觉怕冷严重，得温不解"));
        list.add(new Symptom(29, "全身", "体温", "新起发热重", 2, "XQFRZ", "新起自觉发烧重"));
        list.add(new Symptom(30, "全身", "体温", "发热重恶寒轻", 2, "FRZEHQ",
                "新起自觉发烧且有轻微怕冷"));
        list.add(new Symptom(31, "全身", "体温", "恶寒发热", 2, "EHFR", "新起既感到怕冷，又有发烧感"));
        list.add(new Symptom(32, "全身", "体温", "寒战", 2, "HZ", "自觉寒冷甚，且躯体颤抖"));
        list.add(new Symptom(33, "全身", "体温", "壮热", 2, "ZR", "高热持续不退。又名高热"));
        list.add(new Symptom(35, "全身", "体温", "身热不扬", 2, "SRBY",
                "自觉发热，初扪热并不明显，久之则明显烫手"));
        list.add(new Symptom(36, "全身", "体温", "潮热", 2, "CR", "按时发热或按时热甚"));
        list.add(new Symptom(37, "全身", "体温", "往来寒热", 2, "WLHR",
                "恶寒与发热交替出现。又名寒热往来"));
        list.add(new Symptom(38, "全身", "体温", "身热夜甚", 2, "SRYS", "夜间发热明显，较白天为甚"));
        list.add(new Symptom(39, "全身", "体温", "发热", 2, "FR",
                "泛指体温升高，除微发热、壮热、潮热、身热夜甚等之外的发热"));
        list.add(new Symptom(40, "全身", "体温", "自觉发热", 2, "ZJFR", "自觉发烧"));
        list.add(new Symptom(43, "全身", "体温", "经常畏冷", 2, "JCWL", "长期怕冷。简称畏冷"));
        list.add(new Symptom(47, "全身", "体温", "半侧寒冷", 2, "BCHL", "半边身体较凉"));
        list.add(new Symptom(49, "全身", "体温", "久有低热", 2, "JYDR", "长期有轻微发热"));
        list.add(new Symptom(50, "全身", "体温", "手足心烧", 2, "SZXS", "手足心发热"));
        list.add(new Symptom(51, "全身", "体温", "骨蒸发热", 2, "GZFR",
                "发热似从骨中蒸发而出。简称骨蒸"));
        list.add(new Symptom(52, "全身", "体温", "劳累后发热", 2, "LLHFR", "活动劳累则有发热感"));
        list.add(new Symptom(53, "全身", "体温", "烦躁发热", 2, "FZFR",
                "情绪烦躁时则觉身体躁热。简称烦热"));
        list.add(new Symptom(62, "全身", "体温", "口鼻气冷", 2, "KBQL", "口鼻呼出的气体有凉觉"));
        list.add(new Symptom(359, "全身", "体温", "身热斑疹", 2, "SRBZ",
                "皮肤出现红色斑块或斑点，并有发热或胸腹部烫手"));
        list.add(new Symptom(612, "全身", "体温", "肢厥而身灼", 2, "ZJESZ",
                "四肢尤其是下肢冷，但胸腹部烫手"));
        list.add(new Symptom(613, "全身", "体温", "肢厥且身凉", 2, "ZJQSL",
                "四肢尤其是下肢冷，胸腹部亦偏凉"));
        list.add(new Symptom(1006, "全身", "体温", "阵发烘热", 2, "zfhr", ""));
        list.add(new Symptom(491, "全身", "形体", "身体素弱", 2, "STSR", "身体历来比较虚弱"));
        list.add(new Symptom(492, "全身", "形体", "形体消瘦", 2, "XTXS", "身体消瘦，体重减轻"));
        list.add(new Symptom(493, "全身", "形体", "形体肥胖", 2, "XTFP", "身体肥胖，体重增加"));
        list.add(new Symptom(541, "全身", "形体", "步态不稳", 2, "BTBW", "走路不稳"));
        list.add(new Symptom(1001, "全身", "形体", "肥胖", 2, "fp",
                "1、体形肥胖 2、超重或BMI>24 3、腰围男性大于85，女性大于80"));
        list.add(new Symptom(540, "全身", "肿块", "肿块圆/软或如囊状", 2, "ZKYRHRNZ",
                "肿块形态圆，按之软"));
        list.add(new Symptom(619, "全身", "肿块", "肿块质硬不平", 2, "ZKZYBP",
                "肿块质地坚硬，边界凹凸不平"));

        //头面部
        list.add(new Symptom(161, "头面部", "鼻", "鼻塞流清涕", 2, "BSLQT",
                "新起鼻腔堵塞不通气，或流清涕"));
        list.add(new Symptom(193, "头面部", "鼻", "鼻痒", 2, "BY1", "鼻腔瘙痒感"));
        list.add(new Symptom(352, "头面部", "鼻", "鼻衄", 2, "BN", "鼻中出血"));
        list.add(new Symptom(370, "头面部", "鼻", "喷嚏", 2, "PT", "新起打喷嚏"));
        list.add(new Symptom(393, "头面部", "鼻", "鼾声不止/酣睡", 2, "hSBZHS",
                "神识不清，鼾声不停"));
        list.add(new Symptom(452, "头面部", "鼻", "鼻翼煽动", 2, "BYSD", "鼻翼随呼吸而张开与缩陷"));
        list.add(new Symptom(453, "头面部", "鼻", "鼻唇干燥", 2, "BCGZ", "鼻孔和口唇干燥"));
        list.add(new Symptom(87, "头面部", "耳", "耳痛", 2, "ET", "自觉耳廓或耳道内疼痛"));
        list.add(new Symptom(157, "头面部", "耳", "耳暴鸣", 2, "EBM", "新起、突然耳内鸣响"));
        list.add(new Symptom(158, "头面部", "耳", "耳久鸣", 2, "EJM", "经常、长期耳内鸣响"));
        list.add(new Symptom(159, "头面部", "耳", "新病失聪(聋)", 2, "XBSCL",
                "新起、突然听力下降甚至丧失"));
        list.add(new Symptom(451, "头面部", "耳", "耳肿流脓", 2, "EZLN", "外耳道肿胀，流出脓液"));
        list.add(new Symptom(471, "头面部", "耳", "耳轮干枯", 2, "ELGK", "耳轮干枯"));
        list.add(new Symptom(233, "头面部", "呼吸", "新病气喘", 2, "XBQC",
                "新起疾病出现呼吸困难、迫促"));
        list.add(new Symptom(234, "头面部", "呼吸", "久病气喘", 2, "JBQC", "病久而长期出现呼吸困难"));
        list.add(new Symptom(235, "头面部", "呼吸", "气喘", 2, "QC", "泛指未分新久的呼吸困难"));
        list.add(new Symptom(236, "头面部", "呼吸", "气短", 2, "QD",
                "自觉呼吸短促而不相接续，气少不够用。又名短气"));
        list.add(new Symptom(237, "头面部", "呼吸", "喘不能卧", 2, "CBNW",
                "因呼吸困难而不能平卧，卧则气喘加重"));
        list.add(new Symptom(387, "头面部", "呼吸", "嗳气", 2, "AQ",
                "胃中气体上出咽喉所发出的一种长而缓的声音"));
        list.add(new Symptom(388, "头面部", "呼吸", "嗳气酸馊", 2, "AQSS", "嗳出的气体有酸馊味"));
        list.add(new Symptom(389, "头面部", "呼吸", "呃逆", 2, "EN",
                "不自主的从咽喉发出一种“呃、呃”作响的短促冲击声"));
        list.add(new Symptom(397, "头面部", "呼吸", "气息微弱", 2, "QXWR", "呼吸时口鼻气息很微弱"));
        list.add(new Symptom(474, "头面部", "颈", "颈脉怒张", 2, "JMNZ", "颈部两侧静脉充盈显露"));
        list.add(new Symptom(210, "头面部", "咳嗽", "咳嗽", 2, "KS1",
                "气流通过喉部而发出“咳、咳”的声音"));
        list.add(new Symptom(211, "头面部", "咳嗽", "新病咳嗽", 2, "XBKS", "新起疾病出现咳嗽"));
        list.add(new Symptom(212, "头面部", "咳嗽", "阵发呛咳", 2, "ZFQK", "突然出现一阵阵地咳嗽"));
        list.add(new Symptom(213, "头面部", "咳嗽", "干咳", 2, "GK", "咳而无痰"));
        list.add(new Symptom(349, "头面部", "咳嗽", "咳血", 2, "KX", "血液随咳嗽而咯出。又名咯血"));
        list.add(new Symptom(92, "头面部", "口腔", "口腔痛", 2, "KQT", "自觉口腔内疼痛。简称口痛"));
        list.add(new Symptom(187, "头面部", "口腔", "口舌发麻", 2, "KSFM",
                "口腔或舌体发麻，感觉减退"));
        list.add(new Symptom(238, "头面部", "口腔", "口不渴", 2, "KBK", "口不干而不想饮水"));
        list.add(new Symptom(239, "头面部", "口腔", "口渴", 2, "KK", "口中干燥而欲饮水"));
        list.add(new Symptom(240, "头面部", "口腔", "渴欲饮冷", 2, "KYYL", "口干而欲饮冷水"));
        list.add(new Symptom(241, "头面部", "口腔", "渴欲饮热", 2, "KYYR", "口干而欲饮热水"));
        list.add(new Symptom(242, "头面部", "口腔", "渴不欲饮", 2, "KBYY", "虽口干而不欲饮水"));
        list.add(new Symptom(258, "头面部", "口腔", "口苦", 2, "KK1", "自觉口中有苦味"));
        list.add(new Symptom(259, "头面部", "口腔", "口淡", 2, "KD", "味觉减退，自觉口中发淡而无味"));
        list.add(new Symptom(260, "头面部", "口腔", "口甜", 2, "KT1", "自觉口中有甜味"));
        list.add(new Symptom(261, "头面部", "口腔", "口咸", 2, "KX1", "自觉口中有咸味"));
        list.add(new Symptom(262, "头面部", "口腔", "口黏腻", 2, "KNN", "自觉口中黏腻不爽"));
        list.add(new Symptom(263, "头面部", "口腔", "口酸", 2, "KS",
                "自觉口中有酸味，甚至闻到酸腐气味"));
        list.add(new Symptom(404, "头面部", "口腔", "口臭", 2, "KC", "口中有臭秽气"));
        list.add(new Symptom(415, "头面部", "口腔", "口角流涎", 2, "KJLX", "时常有涎液从口角流出"));
        list.add(new Symptom(454, "头面部", "口腔", "唇淡", 2, "CD", "咀唇颜色浅淡而少红"));
        list.add(new Symptom(456, "头面部", "口腔", "唇紫", 2, "CZ1", "咀唇颜色紫暗"));
        list.add(new Symptom(459, "头面部", "口腔", "口腔赤烂", 2, "KQCL", "口腔黏膜有红色溃烂面"));
        list.add(new Symptom(460, "头面部", "口腔", "口腔糜烂", 2, "KQML", "口腔黏膜溃烂而颜色不红"));
        list.add(new Symptom(85, "头面部", "面部", "面痛", 2, "MT1", "自觉颜面部疼痛"));
        list.add(new Symptom(88, "头面部", "面部", "腮肿痛", 2, "SZT", "自觉耳前下方面颊处肿起疼痛"));
        list.add(new Symptom(435, "头面部", "面部", "面睑浮肿", 2, "MJFZ", "面部和眼睑浮肿"));
        list.add(new Symptom(418, "头面部", "面色", "面色少华", 2, "MSSH", "面部颜色缺少光泽"));
        list.add(new Symptom(419, "头面部", "面色", "面色萎黄", 2, "MSWH", "面部颜色偏黄而不润泽"));
        list.add(new Symptom(420, "头面部", "面色", "面黄如橘", 2, "MHRJ", "面色黄而鲜明如橘色"));
        list.add(new Symptom(421, "头面部", "面色", "面黄色黯", 2, "MHSA", "面色黄而晦暗如烟熏"));
        list.add(new Symptom(422, "头面部", "面色", "面色晦黯", 2, "MSHA",
                "面部颜色较黑而暗，缺乏光泽"));
        list.add(new Symptom(423, "头面部", "面色", "面色淡白", 2, "MSDB", "面部颜色较白而缺乏光泽"));
        list.add(new Symptom(424, "头面部", "面色", "面色苍白", 2, "MSCB", "面部颜色很白而带青紫"));
        list.add(new Symptom(425, "头面部", "面色", "面色晄白", 2, "MSHB",
                "面部皮肤较润，颜色白而略有反光"));
        list.add(new Symptom(426, "头面部", "面色", "面色黧黑", 2, "MSLH", "面部颜色较黑而略有光泽"));
        list.add(new Symptom(428, "头面部", "面色", "颧红", 2, "QH", "两颧部位显红色"));
        list.add(new Symptom(350, "头面部", "呕吐", "呕血", 2, "OX", "血液经口随呕吐而出。又名吐血"));
        list.add(new Symptom(365, "头面部", "呕吐", "血中夹不消化食物", 2, "XZJBXHSW",
                "吐出的血中含有不消化的食物"));
        list.add(new Symptom(377, "头面部", "呕吐", "恶心", 2, "EX", "自觉胃部有气上逆而欲呕"));
        list.add(new Symptom(378, "头面部", "呕吐", "呕吐", 2, "OT", "胃内容物从口中吐出"));
        list.add(new Symptom(379, "头面部", "呕吐", "干呕", 2, "GO", "有呕吐动作、声音而无物吐出"));
        list.add(new Symptom(380, "头面部", "呕吐", "呕吐酸水", 2, "OTSS",
                "呕吐物为酸水，或酸水自胃中上至咽喉，随即吞咽而下。又名吞酸"));
        list.add(new Symptom(381, "头面部", "呕吐", "呕吐苦水", 2, "OTKS", "呕吐物为苦水"));
        list.add(new Symptom(382, "头面部", "呕吐", "呕吐清水", 2, "OTQS", "呕吐物为清水"));
        list.add(new Symptom(383, "头面部", "呕吐", "呕吐痰涎", 2, "OTTX", "呕吐物为痰涎"));
        list.add(new Symptom(384, "头面部", "呕吐", "呕吐馊食或宿食", 2, "OTSSHSS",
                "呕吐物为馊食或未消化的食物"));
        list.add(new Symptom(385, "头面部", "呕吐", "呕吐蛔虫", 2, "OTHC", "呕吐出蛔虫"));
        list.add(new Symptom(386, "头面部", "呕吐", "吐粪样物", 2, "TFYW", "呕吐物如粪样有臭秽气"));
        list.add(new Symptom(91, "头面部", "舌", "舌痛", 2, "ST1", "自觉舌体疼痛"));
        list.add(new Symptom(354, "头面部", "舌", "舌衄", 2, "SN", "舌体上有出血斑点"));
        list.add(new Symptom(544, "头面部", "舌", "舌淡白", 2, "SDB",
                "比正常舌色浅，白色偏多红色偏少（给你发的中为舌淡）"));
        list.add(new Symptom(545, "头面部", "舌", "舌淡胖", 2, "SDP", "舌色浅淡，舌体胖大"));
        list.add(new Symptom(546, "头面部", "舌", "舌淡紫", 2, "SDZ", "舌色浅淡而带紫色"));
        list.add(new Symptom(547, "头面部", "舌", "舌红", 2, "SH",
                "比正常舌色红，甚至呈鲜红色（给你发的中为舌赤）"));
        list.add(new Symptom(548, "头面部", "舌", "舌绛", 2, "SJ", "舌色深红呈绛色"));
        list.add(new Symptom(549, "头面部", "舌", "舌红嫩小", 2, "SHNX", "舌色红，舌体较小，舌质嫩"));
        list.add(new Symptom(550, "头面部", "舌", "舌红胖", 2, "SHP", "舌色红，舌体胖大"));
        list.add(new Symptom(551, "头面部", "舌", "舌黯红", 2, "SAH", "舌红而暗"));
        list.add(new Symptom(553, "头面部", "舌", "舌尖红", 2, "SJH", "舌尖部色红"));
        list.add(new Symptom(556, "头面部", "舌", "舌起芒刺", 2, "SQMC",
                "舌尖等处有红色点状突起如刺，摸之棘手"));
        list.add(new Symptom(557, "头面部", "舌", "舌体胖大", 2, "STPD",
                "舌体比正常舌体大而厚，盈口满嘴"));
        list.add(new Symptom(558, "头面部", "舌", "舌体瘦小", 2, "STSX", "舌体较正常舌体小而薄"));
        list.add(new Symptom(559, "头面部", "舌", "舌有裂纹", 2, "SYLW", "舌面出现裂纹、裂沟"));
        list.add(new Symptom(560, "头面部", "舌", "舌紫黯", 2, "SZA", "舌体色紫而无光泽"));
        list.add(new Symptom(561, "头面部", "舌", "舌有斑点", 2, "SYBD", "舌体有紫色斑点条纹"));
        list.add(new Symptom(562, "头面部", "舌", "舌边齿印", 2, "SBCY", "舌体边缘有牙齿压迫的痕迹"));
        list.add(new Symptom(563, "头面部", "舌", "舌体萎软", 2, "STWR",
                "舌体软弱无力，不能随意伸缩回旋"));
        list.add(new Symptom(564, "头面部", "舌", "舌体歪斜", 2, "STWX",
                "伸舌时舌体偏向一侧，或左或右"));
        list.add(new Symptom(566, "头面部", "舌", "舌体颤动", 2, "STCD",
                "伸舌时舌体颤动，或舌体时时伸出口外，或舌舐口唇四周"));
        list.add(new Symptom(567, "头面部", "舌", "舌体强硬", 2, "STQY",
                "舌体失其柔和，卷伸不利，或板硬强直，不能转动"));
        list.add(new Symptom(568, "头面部", "舌", "舌白如镜", 2, "SBRJ",
                "舌色白光，白如镜，毫无血色"));
        list.add(new Symptom(569, "头面部", "舌", "舌红如镜", 2, "SHRJ", "舌色红光，红如镜"));
        list.add(new Symptom(570, "头面部", "舌", "舌绛深紫", 2, "SJSZ", "舌色深红带紫，呈绛紫色"));
        list.add(new Symptom(571, "头面部", "舌", "舌体溃烂", 2, "STKL", "舌体出现溃烂"));
        list.add(new Symptom(572, "头面部", "舌", "舌体干燥", 2, "STGZ", "舌体干燥乏津"));
        list.add(new Symptom(573, "头面部", "舌", "舌下络脉曲张", 2, "SXLMQZ",
                "舌下络脉充盈粗胀、纽曲"));
        list.add(new Symptom(575, "头面部", "舌", "舌苔白", 2, "STB", "舌苔色白"));
        list.add(new Symptom(576, "头面部", "舌", "苔白如积粉", 2, "TBRJF",
                "舌苔色白，干燥，如白粉堆积舌面"));
        list.add(new Symptom(577, "头面部", "舌", "舌苔腐垢", 2, "STFG",
                "舌苔疏松、垢秽不洁，如豆腐渣堆积舌面，揩之可去"));
        list.add(new Symptom(579, "头面部", "舌", "舌苔黄", 2, "STH", "舌苔呈黄色"));
        list.add(new Symptom(580, "头面部", "舌", "舌苔灰黑", 2, "STHH", "舌苔呈灰黑色"));
        list.add(new Symptom(581, "头面部", "舌", "舌苔黄白相兼", 2, "STHBXJ",
                "舌苔呈黄白相兼颜色"));
        list.add(new Symptom(582, "头面部", "舌", "舌苔腻", 2, "STN",
                "舌腻细腻、致密，如涂油腻之状，紧贴舌面，不易揩去"));
        list.add(new Symptom(583, "头面部", "舌", "苔剥、少、无", 2, "TBSW",
                "舌苔剥落，或舌苔少，或无舌苔"));
        list.add(new Symptom(584, "头面部", "舌", "舌苔厚", 2, "TH", "不能透过舌苔见到舌体"));
        list.add(new Symptom(585, "头面部", "舌", "舌苔润滑", 2, "STRH",
                "舌苔干湿适中而润，或津多而滑"));
        list.add(new Symptom(586, "头面部", "舌", "舌苔干燥", 2, "STGZ1", "舌苔干燥乏津"));
        list.add(new Symptom(215, "头面部", "痰", "吐痰", 2, "TT", "咯出痰液"));
        list.add(new Symptom(216, "头面部", "痰", "痰多质稠", 2, "TDZC", "咯多量黏稠痰"));
        list.add(new Symptom(217, "头面部", "痰", "痰少质稠", 2, "TSZC", "咯少量黏稠痰"));
        list.add(new Symptom(218, "头面部", "痰", "痰黏难咳", 2, "TNNK", "痰黏稠而难以咯出"));
        list.add(new Symptom(219, "头面部", "痰", "痰多质稀", 2, "TDZX", "咯多量稀薄痰"));
        list.add(new Symptom(220, "头面部", "痰", "痰少质稀", 2, "TSZX", "咯少量稀薄痰"));
        list.add(new Symptom(222, "头面部", "痰", "泡沫痰多", 2, "PMTD", "咯多量稀薄的泡沫状痰液"));
        list.add(new Symptom(223, "头面部", "痰", "铁锈色痰", 2, "TXST", "咯出的痰呈铁锈色"));
        list.add(new Symptom(224, "头面部", "痰", "痰色白", 2, "TSB", "咯出的痰呈白色"));
        list.add(new Symptom(226, "头面部", "痰", "痰色绿", 2, "TSL", "咯出的痰呈呈绿色"));
        list.add(new Symptom(227, "头面部", "痰", "痰色黄", 2, "TSH", "咯出的痰呈黄色"));
        list.add(new Symptom(228, "头面部", "痰", "腥臭痰", 2, "XCT", "咯出的痰有腥臭气"));
        list.add(new Symptom(229, "头面部", "痰", "脓性痰", 2, "NXT", "咯出的痰稠厚呈脓状"));
        list.add(new Symptom(230, "头面部", "痰", "痰中带血", 2, "TZDX", "咯出的痰中夹有血液或血丝"));
        list.add(new Symptom(231, "头面部", "痰", "痰滑易咳", 2, "THYK", "痰滑而容易咯出"));
        list.add(new Symptom(81, "头面部", "头", "颠顶痛", 2, "DDT", "自觉头顶部疼痛"));
        list.add(new Symptom(82, "头面部", "头", "偏头痛", 2, "PTT", "自觉头的左或右半边疼痛"));
        list.add(new Symptom(83, "头面部", "头", "后头痛", 2, "HTT", "自觉后头、枕部疼痛"));
        list.add(new Symptom(84, "头面部", "头", "头项强痛", 2, "TXQT",
                "自觉头痛而项部有拘急牵强不适感"));
        list.add(new Symptom(124, "头面部", "头", "固定痛", 2, "GDT", "疼痛部位固定不移"));
        list.add(new Symptom(125, "头面部", "头", "刺痛", 2, "CT", "疼痛呈针刺、鸡啄米样"));
        list.add(new Symptom(126, "头面部", "头", "游走痛", 2, "YZT",
                "四肢关节等处的疼痛部位时有转移而不固定"));
        list.add(new Symptom(127, "头面部", "头", "闷痛", 2, "MT", "带有紧闷不舒感的疼痛"));
        list.add(new Symptom(128, "头面部", "头", "空痛", 2, "KT", "带有空虚感的疼痛"));
        list.add(new Symptom(135, "头面部", "头", "夜间痛甚", 2, "YJTS", "夜晚疼痛加重"));
        list.add(new Symptom(145, "头面部", "头", "头晕", 2, "TY",
                "头脑旋晕，感觉自身或眼前景物旋转。又名头眩、头旋"));
        list.add(new Symptom(148, "头面部", "头", "头重", 2, "TZ", "自觉头部重坠"));
        list.add(new Symptom(156, "头面部", "头", "脑鸣", 2, "NM", "自觉头内鸣响有声。又名头中鸣响"));
        list.add(new Symptom(178, "头面部", "头", "头重脚轻感", 2, "TZJQG",
                "自觉头部沉重而下肢轻飘，有行走不稳的感觉"));
        list.add(new Symptom(182, "头面部", "头", "头蒙如裹", 2, "TMRG", "头部如被蒙裹的感觉"));
        list.add(new Symptom(522, "头面部", "头", "肢颤、头摇", 2, "ZCTY",
                "手足颤抖，头部摇晃不能自主"));
        list.add(new Symptom(133, "头面部", "头", "阴雨天疼痛加重", 2, "YYTTTJZ",
                "遇阴雨气候而疼痛加重"));
        list.add(new Symptom(436, "头面部", "头发", "发枯憔悴/结穗", 2, "FKQCJS",
                "头发无泽，枯干易折，多根头发黏连在一起，如穗状。"));
        list.add(new Symptom(437, "头面部", "头发", "发白易脱", 2, "FBYT",
                "头发容易脱落，或头发色白而枯槁"));
        list.add(new Symptom(90, "头面部", "牙", "牙痛", 2, "YT1", "自觉牙齿疼痛。又名齿痛"));
        list.add(new Symptom(353, "头面部", "牙", "齿衄", 2, "CN", "血自齿缝或牙龈渗出"));
        list.add(new Symptom(462, "头面部", "牙", "牙龈萎缩", 2, "YYWS", "龈肉萎缩"));
        list.add(new Symptom(463, "头面部", "牙", "牙齿松动", 2, "YCSD", "牙齿松，触之能摇动"));
        list.add(new Symptom(464, "头面部", "牙", "牙龈红肿", 2, "YYHZ", "牙龈色赤而肿起"));
        list.add(new Symptom(465, "头面部", "牙", "牙关紧闭", 2, "YGJB", "上下牙咬合而不张开"));
        list.add(new Symptom(472, "头面部", "牙", "牙齿焦黑", 2, "YCJH", "牙齿颜色焦黑"));
        list.add(new Symptom(93, "头面部", "咽喉", "咽喉痛", 2, "YHT", "自觉咽喉部位疼痛"));
        list.add(new Symptom(163, "头面部", "咽喉", "喉痒", 2, "HY", "自觉咽喉部作痒"));
        list.add(new Symptom(164, "头面部", "咽喉", "咽部异物感", 2, "YBYWG",
                "自觉咽喉部似有异物，吞之不下、吐之不出"));
        list.add(new Symptom(232, "头面部", "咽喉", "喉中哮鸣声", 2, "HZXMS",
                "呼吸时喉和肺部有尖锐鸣响声音"));
        list.add(new Symptom(243, "头面部", "咽喉", "咽干", 2, "YG", "咽喉部有干燥感觉"));
        list.add(new Symptom(246, "头面部", "咽喉", "无热而饮多", 2, "WREYD", "并不发热而喝水多"));
        list.add(new Symptom(257, "头面部", "咽喉", "吞食梗塞", 2, "TSGS",
                "吞食时咽喉或胸骨后有堵塞感，吞咽食物不畅甚至完全不能吞下"));
        list.add(new Symptom(394, "头面部", "咽喉", "喉中痰鸣、痰壅", 2, "HZTMTY",
                "随呼吸而喉部有痰声鸣响，或咽喉部有痰壅堵"));
        list.add(new Symptom(466, "头面部", "咽喉", "咽喉红肿", 2, "YHHZ", "咽喉部颜色红而肿起"));
        list.add(new Symptom(467, "头面部", "咽喉", "咽喉嫩红不肿", 2, "YHNHBZ",
                "咽喉部颜色红而不肿"));
        list.add(new Symptom(468, "头面部", "咽喉", "咽喉肿不红", 2, "YHZBH",
                "咽喉部颜色不红而肿起"));
        list.add(new Symptom(470, "头面部", "咽喉", "咽喉白膜", 2, "YHBM", "咽喉部出现白色膜片状物"));
        list.add(new Symptom(86, "头面部", "眼", "目痛", 2, "MT2", "自觉眼睛疼痛"));
        list.add(new Symptom(147, "头面部", "眼", "眼胀及胀痛", 2, "YZJZT", "眼内作胀而痛"));
        list.add(new Symptom(149, "头面部", "眼", "眼花", 2, "YH",
                "自觉视物旋转动荡，或眼前如有蚊蝇、雪花飞舞。又名目眩"));
        list.add(new Symptom(150, "头面部", "眼", "一物看成两物", 2, "YWKCLW",
                "视一物成二物而不清"));
        list.add(new Symptom(151, "头面部", "眼", "眼干涩", 2, "YGS",
                "眼内乏液，自觉眼内干燥、滞涩不适"));
        list.add(new Symptom(152, "头面部", "眼", "视物模糊", 2, "SWMH",
                "视物模糊不清。或称视物昏花、视力减退、视物不清"));
        list.add(new Symptom(153, "头面部", "眼", "畏光", 2, "WG",
                "眼畏惧看光亮，遇光则涩痛、流泪、难睁"));
        list.add(new Symptom(154, "头面部", "眼", "暴盲", 2, "BM", "突然目不视物"));
        list.add(new Symptom(155, "头面部", "眼", "眼眵多", 2, "YCD", "眼部有较多眼粪堆积"));
        list.add(new Symptom(351, "头面部", "眼", "眼出血", 2, "YCX", "白睛出现片状高出的红色出血斑"));
        list.add(new Symptom(439, "头面部", "眼", "眼睑淡白", 2, "YJDB", "眼睑颜色淡白少红"));
        list.add(new Symptom(440, "头面部", "眼", "眼睑下垂", 2, "YJXC", "上眼睑下垂，不能张目"));
        list.add(new Symptom(441, "头面部", "眼", "眼窝凹陷", 2, "YWAX", "眼眶凹进，眼球深陷"));
        list.add(new Symptom(442, "头面部", "眼", "眼周黯黑", 2, "YZAH", "眼周围显暗黑色"));
        list.add(new Symptom(443, "头面部", "眼", "眼突", 2, "YT2", "眼球突出"));
        list.add(new Symptom(444, "头面部", "眼", "直视上窜", 2, "ZSSC", "眼球向上凝视而不能转动"));
        list.add(new Symptom(445, "头面部", "眼", "目黄", 2, "MH3", "白睛发黄"));
        list.add(new Symptom(446, "头面部", "眼", "目赤无所苦", 2, "MCWSK",
                "白睛颜色偏红而无明显痛苦"));
        list.add(new Symptom(447, "头面部", "眼", "目赤睑肿", 2, "MCJZ", "眼球及眼睑色赤而肿"));
        list.add(new Symptom(449, "头面部", "眼", "睡后露睛", 2, "SHLJ", "入睡后上睑未闭而眼球外露"));
        list.add(new Symptom(458, "头面部", "眼", "口眼斜", 2, "KYWX", "口眼向一侧偏斜"));
        list.add(new Symptom(209, "头面部", "声音", "懒言", 2, "LY", "精神疲倦而不想讲活"));
        list.add(new Symptom(372, "头面部", "声音", "声低", 2, "SD", "说话声音轻迟低微，难以听清"));
        list.add(new Symptom(374, "头面部", "声音", "新病失声", 2, "XBSS",
                "新起、突然声音嘶哑，甚至不能发声。即新起的声音嘶哑"));
        list.add(new Symptom(375, "头面部", "声音", "久病失声", 2, "JBSS",
                "长期、经常声音嘶哑，甚至不能发声。即长期的声音嘶哑"));
        list.add(new Symptom(390, "头面部", "声音", "谵语", 2, "ZY",
                "神识欠清甚至完全不清而胡言乱语，语无伦次，声高有力"));
        list.add(new Symptom(392, "头面部", "声音", "语言不利", 2, "YYBL",
                "舌动不灵，说话不流利，吐词不清。又称言謇、语謇、语言謇涩"));

        //胸部
        list.add(new Symptom(167, "胸部", "乳房", "乳房胀", 2, "RFZ", "乳房有作胀的感觉。简称乳胀"));
        list.add(new Symptom(476, "胸部", "乳房", "少乳或无乳汁", 1, "SRHWRZ",
                "妇女哺乳期乳汁很少甚至无乳"));
        list.add(new Symptom(615, "胸部", "乳房", "乳房结块", 1, "RFJK", "触诊乳房内有结节肿块"));
        list.add(new Symptom(
                1000,
                "胸部",
                "乳房",
                "乳腺小叶增生",
                1,
                "rxxyzs",
                "1、一侧或双侧乳房胀痛或隐痛， 月经前及情绪波动时痛甚。2、单侧或双侧大小不等， 呈片状 盘状 颗粒状 条索状， 质地柔韧， 边界不清，与皮肤无粘连 有触痛的乳房肿块; 3、乳腺铝靶 X 线摄片见较均匀密度增高影"));
        list.add(new Symptom(97, "胸部", "乳房", "乳房痛", 2, "RFT", "自觉一侧或两侧乳房疼痛"));
        list.add(new Symptom(98, "胸部", "胁肋", "胁痛", 2, "XT3",
                "自觉一侧或两侧腋中线肋缘内或略下处疼痛"));
        list.add(new Symptom(134, "胸部", "胁肋", "气行觉舒", 2, "QXJS",
                "得嗳气、肠鸣、矢气等而疼痛减轻"));
        list.add(new Symptom(173, "胸部", "胁肋", "胁胀", 2, "XZ", "自觉一侧或两侧胁部胀满不舒"));
        list.add(new Symptom(168, "胸部", "心脏", "心悸", 2, "XJ", "自觉心脏跳动不安"));
        list.add(new Symptom(169, "胸部", "心脏", "心慌", 2, "XH", "心胸慌乱而难以自持的感觉"));
        list.add(new Symptom(170, "胸部", "心脏", "怔忡", 2, "ZZ", "自觉心脏剧烈跳动不安"));
        list.add(new Symptom(171, "胸部", "心脏", "惊悸", 2, "JJ",
                "因稍有惊骇而自觉心跳明显，或心动不安而恐惧易惊"));
        list.add(new Symptom(122, "胸部", "胸部", "胀痛或窜痛", 2, "ZTHCT",
                "胸或腹部胀闷而且疼痛，或疼痛部位走串不定"));
        list.add(new Symptom(94, "胸部", "胸部", "胸骨后痛", 2, "XGHT", "自觉疼痛部位在胸骨后"));
        list.add(new Symptom(95, "胸部", "胸部", "胸痛", 2, "XT1", "泛指整个胸部疼痛"));
        list.add(new Symptom(96, "胸部", "胸部", "心痛", 2, "XT", "自觉左乳头偏下的胸内疼痛"));
        list.add(new Symptom(122, "胸部", "胸部", "胀痛或窜痛", 2, "ZTHCT",
                "胸或腹部胀闷而且疼痛，或疼痛部位走串不定"));
        list.add(new Symptom(172, "胸部", "胸部", "胸闷", 2, "XM", "自觉胸部痞塞满闷"));
        list.add(new Symptom(126, "胸部", "胸部", "游走痛", 2, "YZT", "四肢关节等处的疼痛部"));
        list.add(new Symptom(123, "胸部", "胸部", "绞痛", 2, "JT1", "心胸或腹部疼痛剧烈，状如绞割"));
        list.add(new Symptom(124, "胸部", "胸部", "固定痛", 2, "GDT", "疼痛部位固定不移"));
        list.add(new Symptom(125, "胸部", "胸部", "刺痛", 2, "CT", "疼痛呈针刺、鸡啄米样"));
        list.add(new Symptom(127, "胸部", "胸部", "闷痛", 2, "MT", "带有紧闷不舒感的疼痛"));
        list.add(new Symptom(135, "胸部", "胸部", "夜间痛甚", 2, "YJTS", "夜晚疼痛加重"));
        list.add(new Symptom(130, "胸部", "胸部", "冷痛", 2, "LT", "疼痛且有冷凉感而喜暖"));
        list.add(new Symptom(133, "胸部", "胸部", "阴雨天疼痛加重", 2, "YYTTTJZ",
                "遇阴雨气候而疼痛加重"));
        list.add(new Symptom(136, "胸部", "胸部", "活动痛缓；不动痛甚", 2, "HDTHBDTS",
                "适当活动后疼痛缓解，没有活动时疼痛加重。"));
        list.add(new Symptom(131, "胸部", "胸部", "酸重痛", 2, "SZT1", "带有酸软、沉重感的疼痛"));
        list.add(new Symptom(140, "胸部", "胸部", "牵掣痛", 2, "QCT",
                "痛处有抽掣感，一处牵挛到另一处疼痛。又名引痛、彻痛"));
        list.add(new Symptom(165, "胸部", "胸部", "气梗堵感", 2, "QGDG",
                "胸部或脘腹部突然有短暂的憋气或气流梗堵觉"));

        //腹部
        list.add(new Symptom(45, "腹部", "腹部", "脘腹腰背冷", 2, "WFYBL", "腰、脘、腹等部位有冷感"));
        list.add(new Symptom(55, "腹部", "腹部", "喜凉恶热", 2, "XLER",
                "喜欢凉爽，或得寒凉则病情缓解、减轻，遇温热则加重"));
        list.add(new Symptom(56, "腹部", "腹部", "喜温恶凉", 2, "XWEL",
                "喜欢温热，或得温热则病情缓解、减轻，遇寒凉则加重"));
        list.add(new Symptom(100, "腹部", "腹部", "脘腹痛", 2, "WFT", "自觉剑突下、脐上的脘腹部疼痛"));
        list.add(new Symptom(101, "腹部", "腹部", "右上腹痛", 2, "YSFT", "自觉脘腹部的右侧疼痛"));
        list.add(new Symptom(103, "腹部", "腹部", "脐腹痛", 2, "QFT", "自觉脐周围的腹部疼痛"));
        list.add(new Symptom(104, "腹部", "腹部", "少腹痛", 2, "SFT", "自觉脐下左或右侧的腹部疼痛"));
        list.add(new Symptom(105, "腹部", "腹部", "小腹痛", 2, "XFT", "自觉脐下中间的小腹部位疼痛"));
        list.add(new Symptom(106, "腹部", "腹部", "腹痛", 2, "FT",
                "泛指未能明确腹部某具体部位的整个腹部疼痛"));
        list.add(new Symptom(123, "腹部", "腹部", "绞痛", 2, "JT1", "心胸或腹部疼痛剧烈，状如绞割"));
        list.add(new Symptom(130, "腹部", "腹部", "冷痛", 2, "LT", "疼痛且有冷凉感而喜暖"));
        list.add(new Symptom(131, "腹部", "腹部", "酸重痛", 2, "SZT1", "带有酸软、沉重感的疼痛"));
        list.add(new Symptom(136, "腹部", "腹部", "活动痛缓；不动痛甚", 2, "HDTHBDTS",
                "适当活动后疼痛缓解，没有活动时疼痛加重。"));
        list.add(new Symptom(137, "腹部", "腹部", "活动加剧", 2, "HDJJ", "活动时症状加重"));
        list.add(new Symptom(141, "腹部", "腹部", "得食痛缓", 2, "DSTH", "进食后疼痛缓解"));
        list.add(new Symptom(142, "腹部", "腹部", "进食痛甚", 2, "JSTS", "进食后疼痛加重"));
        list.add(new Symptom(174, "腹部", "腹部", "脘痞胀", 2, "WPZ", "自觉脘腹部胀满、痞闷不舒"));
        list.add(new Symptom(175, "腹部", "腹部", "胃脘嘈杂", 2, "WWCZ",
                "脘腹部似饥非饥、似痛非痛、似辣非辣，嘈杂不舒的感觉"));
        list.add(new Symptom(176, "腹部", "腹部", "腹胀", 2, "FZ", "自觉整个腹部胀满不舒"));
        list.add(new Symptom(177, "腹部", "腹部", "小腹胀", 2, "XFZ", "自觉脐下小腹部胀满不舒"));
        list.add(new Symptom(398, "腹部", "腹部", "胃部振水音", 2, "WBZSY",
                "听到胃脘部有水振动的声音"));
        list.add(new Symptom(399, "腹部", "腹部", "肠鸣漉漉", 2, "CMLL", "听到腹内漉漉鸣响有声"));
        list.add(new Symptom(400, "腹部", "腹部", "肠鸣亢进", 2, "CMKJ", "听诊肠鸣音增多、声音增强"));
        list.add(new Symptom(401, "腹部", "腹部", "肠鸣减弱", 2, "CMJR", "听诊肠鸣音减少、声音减弱"));
        list.add(new Symptom(402, "腹部", "腹部", "肠鸣消失", 2, "CMXS", "听诊未闻及肠鸣音"));
        list.add(new Symptom(477, "腹部", "腹部", "腹露青筋", 2, "FLQJ", "腹壁静脉曲张，青筋显露"));
        list.add(new Symptom(478, "腹部", "腹部", "腹膨隆", 2, "FPL", "腹部膨大隆起"));
        list.add(new Symptom(479, "腹部", "腹部", "腹硬满", 2, "FYM", "腹内满实，扪之坚硬如板状"));
        list.add(new Symptom(480, "腹部", "腹部", "舟状腹", 2, "ZZF", "腹部凹陷如舟状"));
        list.add(new Symptom(621, "腹部", "腹部", "痛喜按或按之舒", 2, "TXAHAZS",
                "疼痛处喜揉按，或按摩而疼痛减轻"));
        list.add(new Symptom(622, "腹部", "腹部", "痛拒按或压痛甚", 2, "TJAHYTS",
                "疼痛处不能按压，或按压时疼痛加重"));
        list.add(new Symptom(623, "腹部", "腹部", "脘腹部肿块", 2, "WFBZK",
                "检查发现剑突下脘腹部有肿块"));
        list.add(new Symptom(624, "腹部", "腹部", "小腹部肿块", 2, "XFBZK",
                "检查发现脐下小腹部有肿块"));
        list.add(new Symptom(625, "腹部", "腹部", "少腹部肿块", 2, "SFBZK",
                "检查发现脐下左或右侧有肿块"));
        list.add(new Symptom(626, "腹部", "腹部", "腹内包块", 2, "FNBK",
                "除外肝、胆、脾肿大和脘腹、小腹、少腹等部位肿块后的腹部肿块"));
        list.add(new Symptom(124, "腹部", "腹部", "固定痛", 2, "GDT", "疼痛部位固定不移"));
        list.add(new Symptom(125, "腹部", "腹部", "刺痛", 2, "CT", "疼痛呈针刺、鸡啄米样"));
        list.add(new Symptom(127, "腹部", "腹部", "闷痛", 2, "MT", "带有紧闷不舒感的疼痛"));
        list.add(new Symptom(122, "腹部", "腹部", "胀痛或窜痛", 2, "ZTHCT",
                "胸或腹部胀闷而且疼痛，或疼痛部位走串不定"));
        list.add(new Symptom(140, "腹部", "腹部", "牵掣痛", 2, "QCT",
                "痛处有抽掣感，一处牵挛到另一处疼痛。又名引痛、彻痛"));
        list.add(new Symptom(135, "腹部", "腹部", "夜间痛甚", 2, "YJTS", "夜晚疼痛加重"));
        list.add(new Symptom(337, "腹部", "生殖部", "遗精", 0, "YJ", "不因性交而泄出精液"));
        list.add(new Symptom(338, "腹部", "生殖部", "滑精", 0, "HJ",
                "不因性交、无梦或清醒时精液自行泄出"));
        list.add(new Symptom(339, "腹部", "生殖部", "阳痿", 0, "YW",
                "阴茎痿软，勃起不坚，不能进行正常性交"));
        list.add(new Symptom(340, "腹部", "生殖部", "早泄", 0, "ZX", "性交之始甚至尚未性交，即行排精"));
        list.add(new Symptom(341, "腹部", "生殖部", "性欲衰退", 0, "XYST",
                "性交的欲望减退或完全没有"));
        list.add(new Symptom(342, "腹部", "生殖部", "阳强易举", 0, "YQYJ",
                "阴茎时时容易勃起，不易痿软"));
        list.add(new Symptom(343, "腹部", "生殖部", "精液稀少/畸形", 0, "JYXSJX",
                "泄精量少，或精液检验畸形精子多"));
        list.add(new Symptom(344, "腹部", "生殖部", "精液清冷", 0, "JYQL",
                "自觉泄出的精液有清冷感。简称精冷"));
        list.add(new Symptom(347, "腹部", "生殖部", "不育", 0, "BY", "女方久不受孕是因男方原因所致"));
        list.add(new Symptom(485, "腹部", "生殖部", "阴部湿疹", 2, "YBSZ", "会阴部有湿性疹子"));
        list.add(new Symptom(486, "腹部", "生殖部", "阴囊或睾丸肿", 0, "YNHGWZ",
                "阴囊肿大或睾丸肿大"));
        list.add(new Symptom(487, "腹部", "生殖部", "阴部潮湿/湿烂", 2, "YBCSSL",
                "会阴部出现溃烂流黏液"));
        list.add(new Symptom(121, "腹部", "生殖部", "痛经", 1, "TJ",
                "月经来潮前或行经时小腹及腰部疼痛"));
        list.add(new Symptom(316, "腹部", "生殖部", "经行不畅", 1, "JXBC", "月经流出不畅"));
        list.add(new Symptom(317, "腹部", "生殖部", "月经提前", 1, "YJTQ",
                "上次月经来潮到下次月经来潮，间隔不到21天"));
        list.add(new Symptom(318, "腹部", "生殖部", "月经推迟", 1, "YJTC",
                "上次月经来潮到下次月经来潮，间隔超过35天"));
        list.add(new Symptom(319, "腹部", "生殖部", "月经错乱", 1, "YJCL",
                "月经有时提前，有时推迟，没有准确时间"));
        list.add(new Symptom(320, "腹部", "生殖部", "月经量多", 1, "YJLD", "月经出血量多"));
        list.add(new Symptom(321, "腹部", "生殖部", "月经量少", 1, "YJLS", "月经出血量少"));
        list.add(new Symptom(322, "腹部", "生殖部", "经闭", 1, "JB", "已近2个月没有来月经"));
        list.add(new Symptom(323, "腹部", "生殖部", "阴道流血淋漓", 1, "YDLXLL",
                "阴道出血淋漓不断"));
        list.add(new Symptom(324, "腹部", "生殖部", "阴道出血如崩", 1, "YDCXRB", "阴道出血量很多"));
        list.add(new Symptom(325, "腹部", "生殖部", "月经稀淡", 1, "YJXD", "月经质稀薄、色偏淡"));
        list.add(new Symptom(326, "腹部", "生殖部", "月经深红", 1, "YJSH", "月经呈深红色"));
        list.add(new Symptom(327, "腹部", "生殖部", "月经紫黯", 1, "YJZA", "月经颜色紫暗不泽"));
        list.add(new Symptom(328, "腹部", "生殖部", "月经夹块", 1, "YJJK", "月经中夹有血块"));
        list.add(new Symptom(329, "腹部", "生殖部", "经期延长", 1, "JQYC", "月经行经期超过7天"));
        list.add(new Symptom(330, "腹部", "生殖部", "经间期出血", 1, "JJQCX",
                "约于两次月经的中间阶段，阴道有出血现象"));
        list.add(new Symptom(332, "腹部", "生殖部", "带下多而稀", 1, "DXDEX",
                "阴道内流出多量清稀的液体"));
        list.add(new Symptom(333, "腹部", "生殖部", "带下多而黏", 1, "DXDEN",
                "阴道内流出多量黏稠的液体"));
        list.add(new Symptom(334, "腹部", "生殖部", "带下色黄气臭", 1, "DXSHQC",
                "阴道内流出色黄而气臭的液体"));
        list.add(new Symptom(335, "腹部", "生殖部", "带下夹血", 1, "DXJX",
                "阴道内流出的液体中夹有血液"));
        list.add(new Symptom(336, "腹部", "生殖部", "带下色白气腥", 1, "DXSBQX",
                "阴道内流出色白而有腥气的液体"));
        list.add(new Symptom(346, "腹部", "生殖部", "不孕", 1, "BY2",
                "育龄期女子有性生活、未避孕而2年未受孕"));
        list.add(new Symptom(348, "腹部", "生殖部", "滑胎、堕胎", 1, "HTDT",
                "妊娠12周内胚胎自然殒堕，为堕胎。妊娠12-28周胎儿成形自然殒堕为小产，堕胎或小产连续发生3次或3次以上为滑胎"));
        list.add(new Symptom(363, "腹部", "生殖部", "出血色深红", 2, "CXSSH", "所出之血呈深红色"));
        list.add(new Symptom(368, "腹部", "生殖部", "阴道流血", 1, "YDLX", "泛指阴道出血"));
        list.add(new Symptom(369, "腹部", "生殖部", "恶露不下或不畅", 1, "ELBXHBC",
                "分娩后阴道流出的血和浊液极少，或流出不畅"));
        list.add(new Symptom(481, "腹部", "生殖部", "子宫下垂", 1, "ZGXC",
                "阴道壁甚至子宫脱出于阴道口"));
        list.add(new Symptom(1004, "腹部", "生殖部", "子宫肌瘤", 1, "zgjl",
                "1、月经改变 腹部包块 腹痛腰酸 盆腔压迫等症；2、2 次以上盆腔 B 超诊断为子宫肌瘤；3、妇科双合诊检查发现子宫增大 质硬不平 可触及结节或肿块等"));
        list.add(new Symptom(107, "腹部", "生殖器", "阴器痛", 2, "YQT",
                "自觉阴茎、阴囊内或女子阴户疼痛"));
        list.add(new Symptom(190, "腹部", "生殖器", "阴部瘙痒", 2, "YBSY", "会阴部瘙痒的感觉"));
        list.add(new Symptom(488, "腹部", "生殖器", "外阴干燥", 2, "WYGZ",
                "外阴部皮肤干燥而枯涩不润"));
        list.add(new Symptom(489, "腹部", "生殖器", "阴器收缩", 2, "YQSS",
                "阴茎、睾丸、阴囊或阴阜等阴器向内收缩"));
        list.add(new Symptom(490, "腹部", "生殖器", "阴部坠胀", 2, "YBZZ",
                "会阴部常有重坠下沉，胀满不舒"));
        list.add(new Symptom(297, "腹部", "小便", "新病尿频", 2, "XBNP",
                "新近出现小便次数频繁，时时欲解小便"));
        list.add(new Symptom(298, "腹部", "小便", "长期尿频", 2, "CQNP",
                "经常小便次数频繁，时时欲解小便"));
        list.add(new Symptom(299, "腹部", "小便", "排尿无力", 2, "PNWL", "自觉将小便排出的力量不够"));
        list.add(new Symptom(300, "腹部", "小便", "夜尿多", 2, "YND", "夜间小便次数增多"));
        list.add(new Symptom(301, "腹部", "小便", "尿短黄", 2, "NDH", "小便色比正常黄，尿量减少"));
        list.add(new Symptom(302, "腹部", "小便", "尿清长", 2, "NQC", "小便量多色清"));
        list.add(new Symptom(303, "腹部", "小便", "尿潴留", 2, "NCL",
                "小腹（膀胱）部膨胀（有尿）而小便不能排出"));
        list.add(new Symptom(304, "腹部", "小便", "尿少", 2, "NS", "尿量减少（身体不肿）"));
        list.add(new Symptom(305, "腹部", "小便", "排尿灼热", 2, "PNZR", "排尿时尿道有灼热感"));
        list.add(new Symptom(306, "腹部", "小便", "排尿涩痛", 2, "PNST", "小便排出不畅而有疼痛感"));
        list.add(new Symptom(307, "腹部", "小便", "新起小便淋漓", 2, "XQXBLL",
                "新起的排尿点滴不尽"));
        list.add(new Symptom(308, "腹部", "小便", "尿黄褐", 2, "NHH", "尿深黄呈褐色如浆油"));
        list.add(new Symptom(309, "腹部", "小便", "小便特多", 2, "XBTD", "小便量特别多"));
        list.add(new Symptom(310, "腹部", "小便", "遗尿", 2, "YN", "睡眠中不自主地排尿"));
        list.add(new Symptom(311, "腹部", "小便", "余尿不尽", 2, "YNBJ", "小便完时仍有余尿滴沥不尽"));
        list.add(new Symptom(312, "腹部", "小便", "小便失禁", 2, "XBSJ",
                "小便不能随意控制而自行尿出"));
        list.add(new Symptom(313, "腹部", "小便", "小便浑浊/如脂膏", 2, "XBHZRZG",
                "小便浑浊不清"));
        list.add(new Symptom(314, "腹部", "小便", "尿路砂石", 2, "NLSS", "有砂石随小便排出"));
        list.add(new Symptom(315, "腹部", "小便", "尿后滴浊液", 2, "NHDZY", "小便后滴白色浑浊液体"));
        list.add(new Symptom(356, "腹部", "小便", "尿血", 2, "NX", "血随小便排出而尿呈红色"));

        //四肢部
        list.add(new Symptom(48, "四肢部", "关节", "关节冷", 2, "GJL", "某些关节凉、怕冷"));
        list.add(new Symptom(117, "四肢部", "关节", "关(骨)节痛", 2, "GGJT", "泛指关节、骨骼疼痛"));
        list.add(new Symptom(403, "四肢部", "关节", "关节内作响", 2, "GJNZX", "活动时关节内作响"));
        list.add(new Symptom(529, "四肢部", "关节", "关节晨僵/僵硬", 2, "GJCJJY",
                "晨起时关节僵硬，不能活动"));
        list.add(new Symptom(530, "四肢部", "关节", "关节红", 2, "GJH", "关节部位颜色红"));
        list.add(new Symptom(531, "四肢部", "关节", "关节肿", 2, "GJZ", "关节部位肿起"));
        list.add(new Symptom(532, "四肢部", "关节", "关节/骨骼畸形", 2, "GJGGJX", "关节出现畸形"));
        list.add(new Symptom(115, "四肢部", "关节", "膝痛", 2, "XT2", "自觉膝关节疼痛"));
        list.add(new Symptom(523, "四肢部", "关节", "关节、肢体活动不利", 2, "GJZTHDBL",
                "关节或肢体活动不灵活，甚至不能活动。又名肢体僵硬"));
        list.add(new Symptom(587, "四肢部", "脉", "脉浮", 2, "MF", "脉位浅表，轻手按之即得，重按则减"));
        list.add(new Symptom(588, "四肢部", "脉", "脉沉", 2, "MC1",
                "脉位较深，轻手不应，重手按之明显"));
        list.add(new Symptom(590, "四肢部", "脉", "脉牢", 2, "ML",
                "脉沉取使得，搏动有力，势大形长，脉道较硬"));
        list.add(new Symptom(591, "四肢部", "脉", "脉迟", 2, "MC2", "脉动慢，每分钟不满60次"));
        list.add(new Symptom(592, "四肢部", "脉", "脉数", 2, "MS1", "脉动快，每分钟超过90次"));
        list.add(new Symptom(593, "四肢部", "脉", "脉疾", 2, "MJ", "脉动快，每分钟超过120次"));
        list.add(new Symptom(595, "四肢部", "脉", "脉细", 2, "MX1", "脉体细小，应指明显"));
        list.add(new Symptom(597, "四肢部", "脉", "脉实", 2, "MS2", "脉搏跳动有力"));
        list.add(new Symptom(598, "四肢部", "脉", "脉虚", 2, "MX2", "脉搏跳动无力"));
        list.add(new Symptom(601, "四肢部", "脉", "脉微", 2, "MW", "脉体极小，搏动极无力，似有似无"));
        list.add(new Symptom(602, "四肢部", "脉", "脉缓", 2, "MH2",
                "脉动不快不徐而和缓，或略显力量不足、稍微偏慢"));
        list.add(new Symptom(603, "四肢部", "脉", "脉弦", 2, "MX", "脉体较硬而欠和缓，如按琴弦"));
        list.add(new Symptom(604, "四肢部", "脉", "脉紧", 2, "MJ1",
                "脉体不大，但脉动有力而有弹指之感"));
        list.add(new Symptom(605, "四肢部", "脉", "脉滑", 2, "MH1", "脉动应指有流利园滑之感"));
        list.add(new Symptom(606, "四肢部", "脉", "脉涩", 2, "MS", "脉动应指有滞涩不畅之感"));
        list.add(new Symptom(607, "四肢部", "脉", "脉濡", 2, "MR1", "脉浮细无力"));
        list.add(new Symptom(608, "四肢部", "脉", "脉促", 2, "MC3", "脉动快而有歇止，止无定数"));
        list.add(new Symptom(609, "四肢部", "脉", "脉结", 2, "MJ2", "脉动慢而有歇止，止无定数"));
        list.add(new Symptom(610, "四肢部", "脉", "脉代", 2, "MD1", "脉动慢而有歇止，止有定数"));
        list.add(new Symptom(611, "四肢部", "脉", "尺脉弱", 2, "CMR",
                "尺部脉跳动欠明显，较寸、关部力量偏弱"));
        list.add(new Symptom(44, "四肢部", "四肢", "四肢凉", 2, "SZL",
                "手脚凉。简称肢凉，又名手足厥冷、手足逆冷"));
        list.add(new Symptom(46, "四肢部", "四肢", "下肢冷甚", 2, "XZLS", "下肢特别冷"));
        list.add(new Symptom(119, "四肢部", "四肢", "四肢或肢体痛", 2, "SZHZTT",
                "泛指四肢的疼痛，未能明确疼痛是在筋骨或肌肉者"));
        list.add(new Symptom(139, "四肢部", "四肢", "转筋挛痛", 2, "ZJLT",
                "手、小腿等处筋肉拘急僵硬而痛"));
        list.add(new Symptom(186, "四肢部", "四肢", "肢体肌肤麻木", 2, "ZTJFMM",
                "皮肤甚或肌肉发麻，感觉不灵"));
        list.add(new Symptom(519, "四肢部", "四肢", "肢体抽搐", 2, "ZTCC",
                "不自主地四肢挛急与弛张间作，伸缩交替，动作有力。简称抽搐、抽痉"));
        list.add(new Symptom(520, "四肢部", "四肢", "四肢麻木", 2, "SZMM",
                "手足部皮肤或肌肉发麻，感觉不灵"));
        list.add(new Symptom(521, "四肢部", "四肢", "瘛疭", 2, "QZ",
                "手指或下肢不自主地一伸一缩的轻微运动，动作缓慢无力。又名手足蠕动"));
        list.add(new Symptom(524, "四肢部", "四肢", "肢体拘急", 2, "ZTJJ",
                "手足或腰背等处筋肉拘挛不舒，收紧而难以屈伸"));
        list.add(new Symptom(525, "四肢部", "四肢", "肢体瘫痪", 2, "ZTTH",
                "肢体瘫废，不能随意活动，主指截瘫"));
        list.add(new Symptom(527, "四肢部", "四肢", "肢体萎软", 2, "ZTWR",
                "肢体痿软、松弛无力，运动功能减退"));
        list.add(new Symptom(528, "四肢部", "四肢", "肌肉萎缩", 2, "JRWS", "肌肉消瘦、萎缩"));
        list.add(new Symptom(534, "四肢部", "四肢", "肢体血肿", 2, "ZTXZ",
                "肢体出现红色或暗红色高起的血性肿块"));
        list.add(new Symptom(537, "四肢部", "四肢", "下肢脉络曲张", 2, "XZMLQZ",
                "下肢静脉充盈曲张，青筋显露"));
        list.add(new Symptom(618, "四肢部", "四肢", "结节或肿块", 2, "JJHZK", "肢体出现结节或肿块"));
        list.add(new Symptom(133, "四肢部", "四肢", "阴雨天疼痛加重", 2, "YYTTTJZ",
                "遇阴雨气候而疼痛加重"));
        list.add(new Symptom(130, "四肢部", "四肢", "冷痛", 2, "LT", "疼痛且有冷凉感而喜暖"));
        list.add(new Symptom(136, "四肢部", "四肢", "活动痛缓；不动痛甚", 2, "HDTHBDTS",
                "适当活动后疼痛缓解，没有活动时疼痛加重。"));
        list.add(new Symptom(140, "四肢部", "四肢", "牵掣痛", 2, "QCT",
                "痛处有抽掣感，一处牵挛到另一处疼痛。又名引痛、彻痛"));
        list.add(new Symptom(131, "四肢部", "四肢", "酸重痛", 2, "SZT1", "带有酸软、沉重感的疼痛"));
        list.add(new Symptom(135, "四肢部", "四肢", "夜间痛甚", 2, "YJTS", "夜晚疼痛加重"));
        list.add(new Symptom(109, "四肢部", "四肢", "肌肉疼痛", 2, "JRTT", "自觉疼痛部位在肌肉内"));
        list.add(new Symptom(116, "四肢部", "指/趾/掌", "足跟痛", 2, "ZGT",
                "自觉足跟部疼痛，着地负重时疼痛明显"));
        list.add(new Symptom(118, "四肢部", "指/趾/掌", "指或趾关节痛", 2, "ZHZGJT",
                "自觉手指或足趾关节疼痛"));
        list.add(new Symptom(502, "四肢部", "指/趾/掌", "肝掌", 2, "GZ",
                "手掌大小鱼际及手指充血、潮红"));
        list.add(new Symptom(504, "四肢部", "指/趾/掌", "指端青紫", 2, "ZDQZ",
                "手指或足趾颜色变暗呈青紫色"));
        list.add(new Symptom(505, "四肢部", "指/趾/掌", "指甲淡白", 2, "ZJDB",
                "指甲颜色浅淡无血色"));
        list.add(new Symptom(517, "四肢部", "指/趾/掌", "两手握固", 2, "LSWG", "两手紧握不张开"));

        //背部
        list.add(new Symptom(111, "背部", "背部", "背痛", 2, "BT1", "自觉背部疼痛"));
        list.add(new Symptom(112, "背部", "背部", "肩痛", 2, "JT2", "自觉肩部疼痛"));
        list.add(new Symptom(133, "背部", "背部", "阴雨天疼痛加重", 2, "YYTTTJZ",
                "遇阴雨气候而疼痛加重"));
        list.add(new Symptom(185, "背部", "背部", "项背拘急", 2, "XBJJ",
                "项部和背部有牵强板滞、活动不灵的不适感"));
        list.add(new Symptom(192, "背部", "背部", "背胀", 2, "BZ", "背部有作胀的感觉"));
        list.add(new Symptom(135, "背部", "背部", "夜间痛甚", 2, "YJTS", "夜晚疼痛加重"));
        list.add(new Symptom(140, "背部", "背部", "牵掣痛", 2, "QCT",
                "痛处有抽掣感，一处牵挛到另一处疼痛。又名引痛、彻痛"));
        list.add(new Symptom(136, "背部", "背部", "活动痛缓；不动痛甚", 2, "HDTHBDTS",
                "适当活动后疼痛缓解，没有活动时疼痛加重。"));
        list.add(new Symptom(109, "背部", "背部", "肌肉疼痛", 2, "JRTT", "自觉疼痛部位在肌肉内"));
        list.add(new Symptom(130, "背部", "背部", "冷痛", 2, "LT", "疼痛且有冷凉感而喜暖"));
        list.add(new Symptom(131, "背部", "背部", "酸重痛", 2, "SZT1", "带有酸软、沉重感的疼痛"));

        //腰股部
        list.add(new Symptom(264, "腰股部", "大便", "新起腹泻", 2, "XQFX",
                "新起大便次数增多，便质稀薄不成形"));
        list.add(new Symptom(265, "腰股部", "大便", "经常腹泻", 2, "JCFX",
                "经常大便次数增多，便质稀薄不成形"));
        list.add(new Symptom(266, "腰股部", "大便", "五更腹泻", 2, "WGFX",
                "约于黎明时腹痛、肠鸣，欲解大便，且大便稀薄。又名黎明泄"));
        list.add(new Symptom(267, "腰股部", "大便", "新病便秘", 2, "XBBM",
                "新起排便时间延长，便次减少，便质干燥"));
        list.add(new Symptom(268, "腰股部", "大便", "经常便秘", 2, "JCBM",
                "经常排便时间延长，便次减少，便质干燥"));
        list.add(new Symptom(269, "腰股部", "大便", "大便干结", 2, "DBGJ",
                "大便水分减少而干燥硬结，不易排出"));
        list.add(new Symptom(270, "腰股部", "大便", "经常便溏", 2, "JCBT",
                "经常大便稀软不成形，如溏状。简称便溏"));
        list.add(new Symptom(271, "腰股部", "大便", "新起便稀", 2, "XQBX", "新起大便稀薄、水分多"));
        list.add(new Symptom(272, "腰股部", "大便", "大便如水样/米泔水样", 2, "DBRSYMGSY",
                "大便清稀如水。又名水泻"));
        list.add(new Symptom(273, "腰股部", "大便", "大便如蛋汤", 2, "DBRDT",
                "大便稀水，夹有粪片，水粪杂下，如蛋汤、鸭粪"));
        list.add(new Symptom(274, "腰股部", "大便", "大便如黄糜", 2, "DBRHM",
                "大便色黄质稀，不成形而腐臭，如小米粥样"));
        list.add(new Symptom(275, "腰股部", "大便", "大便有黏液", 2, "DBYNY", "大便夹有黏液"));
        list.add(new Symptom(276, "腰股部", "大便", "大便有脓血/如鱼脑", 2, "DBYNXRYN",
                "大便夹有脓血"));
        list.add(new Symptom(277, "腰股部", "大便", "大便先干后稀", 2, "DBXGHX",
                "先排出的大便尚干而成形，后排出的大便则稀而不成形"));
        list.add(new Symptom(278, "腰股部", "大便", "大便时溏时结", 2, "DBSTSJ",
                "大便有时干硬燥结，有时稀烂如溏状。又名溏结不调"));
        list.add(new Symptom(279, "腰股部", "大便", "大便色灰白", 2, "DBSHB",
                "大便颜色灰白不带黄色"));
        list.add(new Symptom(280, "腰股部", "大便", "大便黑如柏油", 2, "DBHRBY",
                "大便质稀色黑而有光亮，如柏油状"));
        list.add(new Symptom(281, "腰股部", "大便", "大便腥腐臭气", 2, "DBXFCQ",
                "大便气腥腐臭秽难闻"));
        list.add(new Symptom(282, "腰股部", "大便", "完谷不化", 2, "WGBH",
                "大便中有多量未消化的食物。又名下利清谷"));
        list.add(new Symptom(283, "腰股部", "大便", "大便细扁", 2, "DBXB", "大便排出变小，或呈扁形"));
        list.add(new Symptom(284, "腰股部", "大便", "大便排虫", 2, "DBPC", "蛔虫随大便排出"));
        list.add(new Symptom(285, "腰股部", "大便", "泻势急迫", 2, "XSJP",
                "泻势很急迫，欲解大便就马上泻出。又名泻下如注"));
        list.add(new Symptom(286, "腰股部", "大便", "排便无力", 2, "PBWL",
                "自觉将大便排出的力量不够"));
        list.add(new Symptom(287, "腰股部", "大便", "排便不爽", 2, "PBBS",
                "排便不通畅，欲解不解，便意未尽，乏舒畅感"));
        list.add(new Symptom(291, "腰股部", "大便", "里急后重", 2, "LJHZ",
                "腹内疼痛，窘迫欲便，肛门重坠，刚欲泻出则突然肛缩，而便出不爽"));
        list.add(new Symptom(292, "腰股部", "大便", "大便失禁", 2, "DBSJ",
                "大便不能随意控制而自行排出"));
        list.add(new Symptom(293, "腰股部", "大便", "腹痛欲泻", 2, "FTYX",
                "腹部时有疼痛，痛则欲排大便，大便质稀"));
        list.add(new Symptom(357, "腰股部", "大便", "便血", 2, "BX",
                "血从肛门排出，可为大便前、后有血滴出，或大便表面、大便中见有血液"));
        list.add(new Symptom(108, "腰股部", "肛门", "肛门痛", 2, "GMT", "自觉肛门部位疼痛"));
        list.add(new Symptom(288, "腰股部", "肛门", "肛门灼热", 2, "GMZR", "肛门部有灼热的感觉"));
        list.add(new Symptom(289, "腰股部", "肛门", "肛门坠胀", 2, "GMZZ",
                "肛门部时常有重坠下沉、要排大便的感觉"));
        list.add(new Symptom(294, "腰股部", "肛门", "矢气多", 2, "SQD",
                "肠道内气体从肛门排出，频频发出声响"));
        list.add(new Symptom(296, "腰股部", "肛门", "矢气甚臭", 2, "SQSC",
                "经肛门排出的气体，其气特别臭，有的臭如败卵"));
        list.add(new Symptom(482, "腰股部", "肛门", "脱肛", 2, "TG", "肛管甚至直肠脱出于肛门外"));
        list.add(new Symptom(484, "腰股部", "肛门", "痔疮", 2, "ZC",
                "肛门齿线处有肉（血管球或皮瓣等）突起"));
        list.add(new Symptom(109, "腰股部", "腰", "肌肉疼痛", 2, "JRTT", "自觉疼痛部位在肌肉内"));
        list.add(new Symptom(113, "腰股部", "腰", "腰痛", 2, "YT",
                "自觉疼痛部位在腰部两边或腰左右侧处"));
        list.add(new Symptom(114, "腰股部", "腰", "腰脊痛", 2, "YJT",
                "自觉疼痛部位在腰部偏中间脊椎处"));
        list.add(new Symptom(120, "腰股部", "腰", "腰痛连足", 2, "YTLZ", "自觉腰痛牵掣到下肢疼痛"));
        list.add(new Symptom(140, "腰股部", "腰", "牵掣痛", 2, "QCT",
                "痛处有抽掣感，一处牵挛到另一处疼痛。又名引痛、彻痛"));
        list.add(new Symptom(184, "腰股部", "腰", "腰膝酸软", 2, "YXSR", "腰部、膝部酸软无力的感觉"));
        list.add(new Symptom(130, "腰股部", "腰", "冷痛", 2, "LT", "疼痛且有冷凉感而喜暖"));
        list.add(new Symptom(131, "腰股部", "腰", "酸重痛", 2, "SZT1", "带有酸软、沉重感的疼痛"));
        list.add(new Symptom(133, "腰股部", "腰", "阴雨天疼痛加重", 2, "YYTTTJZ",
                "遇阴雨气候而疼痛加重"));
        list.add(new Symptom(136, "腰股部", "腰", "活动痛缓；不动痛甚", 2, "HDTHBDTS",
                "适当活动后疼痛缓解，没有活动时疼痛加重。"));
        list.add(new Symptom(135, "腰股部", "腰", "夜间痛甚", 2, "YJTS", "夜晚疼痛加重"));

        //病因
        list.add(new Symptom(12, "病因", "病因", "嗜食肥甘厚味", 2, "SSFGHW", "喜食高脂、高糖食品"));
        list.add(new Symptom(13, "病因", "病因", "新近感受风寒", 2, "XJGSFH", "新病与感受风寒有关"));
        list.add(new Symptom(14, "病因", "病因", "感受暑热火邪", 2, "GSSRHX",
                "起病与感受火热或暑热有关"));
        list.add(new Symptom(15, "病因", "病因", "环境潮湿", 2, "HJCS",
                "发病与季节、气候环境潮湿有关"));
        list.add(new Symptom(16, "病因", "病因", "环境干燥", 2, "HJGZ",
                "发病与季节、气候环境干燥有关"));
        list.add(new Symptom(17, "病因", "病因", "淋雨下水", 2, "LYXS", "起病与淋雨、下水等有关"));
        list.add(new Symptom(18, "病因", "病因", "饮食不慎", 2, "YSBS",
                "发病与饮食不慎、不洁、过饱等有关"));
        list.add(new Symptom(19, "病因", "病因", "活动或劳累病重", 2, "HDHLLBZ",
                "病情常因活动或劳累而加重"));
        list.add(new Symptom(20, "病因", "病因", "病情与情志有关", 2, "BQYQZYG",
                "病情因情志刺激而发，或病情轻重与情绪明显相关"));
        list.add(new Symptom(21, "病因", "病因", "外伤所致", 2, "WSSZ", "病情与外伤有明显关系"));
        list.add(new Symptom(22, "病因", "病因", "新产、流产、手术", 1, "XCLCSS",
                "新近作过手术，或生育、流产未超过2月，或病情与之明显相关"));
        list.add(new Symptom(24, "病因", "病因", "大量或持续出血", 2, "DLHCXCX",
                "当前病变与曾经大出血有关，或有慢性持续出血的病史"));
        list.add(new Symptom(34, "病因", "病因", "感受暑热而发热", 2, "GSSREFR",
                "在暑季感受了暑热而自觉发热"));
        list.add(new Symptom(1002, "病因", "病因", "高血脂", 2, "gxz",
                "1、胆固醇高2、甘油三酯高3、血黏度高4、低密度脂蛋白高"));
        list.add(new Symptom(1003, "病因", "病因", "脂肪肝", 2, "zfg",
                "1、肝区胀痛、腹胀、恶心、食欲不振、嗳气2、B超提示：脂肪肝"));
        list.add(new Symptom(1005, "病因", "病因", "高尿酸", 2, "gns", "尿酸高"));

        return list;
    }

    public static List<Symptom> initSymptomEn() {
        List<Symptom> list = new ArrayList<>();
        //全身
        list.add(new Symptom(63, "Whole Body", "Sweating", "spontaneous sweating", 2, "ZH", "清醒时出汗，活动时汗出明显"));
        list.add(new Symptom(64, "Whole Body", "Sweating", "night sweating", 2, "DH", "睡后出汗，醒则汗止"));
        list.add(new Symptom(65, "Whole Body", "Sweating", "new contraction without sweating", 2, "XBWH", "新发疾病，没有出过汗"));
        list.add(new Symptom(66, "Whole Body", "Sweating", "new contraction with sweating", 2, "XBYH", "新发疾病，已经出过汗"));
        list.add(new Symptom(67, "Whole Body", "Sweating", "exuberant fever without sweating", 2, "ZRWH", "高烧不退而没有出汗"));
        list.add(new Symptom(69, "Whole Body", "Sweating", "high fever with perfuse sweating", 2, "RSHD", "发热明显，出汗多"));
        list.add(new Symptom(70, "Whole Body", "Sweating", "perfuse sweating due to summer days", 2, "STHD", "暑天气候炎热而出汗多"));
        list.add(new Symptom(73, "Whole Body", "Sweating", "cold sweating", 2, "LHLL",
                "汗出清稀如水而身凉。又名冷汗"));
        list.add(new Symptom(75, "Whole Body", "Sweating", "perfuse sweating due to severe disease", 2, "BZDH", "病重危重时大汗出"));
        list.add(new Symptom(77, "Whole Body", "Sweating", "hemi-lateral sweating", 2, "BSHC",
                "半边或半截肢体不出汗。又名半身无汗"));
        list.add(new Symptom(78, "Whole Body", "Sweating", "incomplete sweating", 2, "HCBC", "虽有一点汗出，但汗出欠通畅"));
        list.add(new Symptom(79, "Whole Body", "Sweating", "debility sweating or easy sweating", 2, "CXHHYCH",
                "体质虚弱而容易出汗"));
        list.add(new Symptom(361, "Whole Body", "Bleeding", "light red bleeding", 2, "CXQD",
                "所出之血质较稀薄、颜色较淡"));
        list.add(new Symptom(364, "Whole Body", "Bleeding", "dark purple bleeding with clots", 2, "CXSACK",
                "所出之血呈紫暗色、成块状"));
        list.add(new Symptom(366, "Whole Body", "Bleeding", "chronic bleeding", 2, "MXCX",
                "泛指慢性、小量、持续的出血"));
        list.add(new Symptom(494, "Whole Body", "Mental State", "curling up", 2, "XTJW",
                "睡或坐时，身体喜欢卷曲"));
        list.add(new Symptom(179, "Whole Body", "Mental State", "lassitude", 2, "JDFL",
                "自觉疲倦而全身没有气力"));
        list.add(new Symptom(194, "Whole Body", "Mental State", "mental fatigue", 2, "JSPJ", "mental fatigue"));
        list.add(new Symptom(200, "Whole Body", "Mental State", "easy sorrow and cry", 2, "SBYK",
                "容易出现悲哀情绪而好哭流泪"));
        list.add(new Symptom(201, "Whole Body", "Mental State", "vexation", 2, "XF", "自觉情绪烦乱"));
        list.add(new Symptom(202, "Whole Body", "Mental State", "dysphoria", 2, "FZ2", "因情绪烦乱而躁动不安"));
        list.add(new Symptom(203, "Whole Body", "Mental State", "easy anger and irritation", 2, "JZYN",
                "性情急、情绪躁动，容易发怒"));
        list.add(new Symptom(204, "Whole Body", "Mental State", "susceptible to shock due to timidity", 2, "DQYJ",
                "胆量小而容易产生惊恐、害怕情绪"));
        list.add(new Symptom(205, "Whole Body", "Mental State", "depressed, anxious or unsociable", 2, "QZYYHYLGP",
                "情绪低落、不高兴，忧愁、考虑某事物而难以分散精力"));
        list.add(new Symptom(206, "Whole Body", "Mental State", "susceptible to sigh", 2, "XTQ",
                "常不自觉地在平常呼吸之后有延长和间歇的深长呼吸。又名太息"));
        list.add(new Symptom(207, "Whole Body", "Mental State", "impulsiveness", 2, "QXYJD", "情绪容易激动"));
        list.add(new Symptom(371, "Whole Body", "Mental State", "frequent yawn", 2, "XHQ", "时常打呵欠"));
        list.add(new Symptom(407, "Whole Body", "Mental State", "slipped into a coma", 2, "JRHM", "逐渐出现神识不清"));
        list.add(new Symptom(408, "Whole Body", "Mental State", "faint", 2, "YD",
                "先有头晕感，然后仆倒而不省人事"));
        list.add(new Symptom(409, "Whole Body", "Mental State", "sudden faint", 2, "TRHP",
                "无预感而突然仆倒，不省人事"));
        list.add(new Symptom(410, "Whole Body", "Mental State", "restless dysphoria", 2, "ZRBN", "神情烦躁，躁动不安"));
        list.add(new Symptom(411, "Whole Body", "Mental State", "obnubilation", 2, "SZCL",
                "哭笑无常、默默独语等，精神错乱失常"));
        list.add(new Symptom(412, "Whole Body", "Mental State", "derangement", 2, "SZKL",
                "打人毁物、裸体叫骂等，精神狂乱失常"));
        list.add(new Symptom(413, "Whole Body", "Mental State", "dull-witted", 2, "SZCD",
                "神志痴呆，反应不灵，意识欠清（给你发的中有解释）"));
        list.add(new Symptom(414, "Whole Body", "Mental State", "coma and drooling foaming", 2, "HMTXM",
                "神识不清而有痰涎从口中溢出"));
        list.add(new Symptom(1012, "Whole Body", "Mental State", "spiritlessness improves after activity", 2, "SPDHSS", ""));
        list.add(new Symptom(41, "Whole Body", "Skin", "often aversion to wind", 2, "JCEF", "经常有怕风怕冷的感觉"));
        list.add(new Symptom(42, "Whole Body", "Skin", "inclined to cold", 2, "RYGM", "经常、容易患感冒"));
        list.add(new Symptom(188, "Whole Body", "Skin", "itchy skin; pruritus", 2, "PFSY", "皮肤瘙痒的感觉"));
        list.add(new Symptom(360, "Whole Body", "Skin", "purpura", 2, "ZB",
                "皮肤出现不高出肤面的青紫色或紫红色斑块，不伴发热症状"));
        list.add(new Symptom(475, "Whole Body", "Skin", "thyromegaly", 2, "JZXZD",
                "颈部结喉处（甲状腺）肿大"));
        list.add(new Symptom(499, "Whole Body", "Skin", "yellowing of the body", 2, "SH1", "身体（包括面部）皮肤发黄"));
        list.add(new Symptom(500, "Whole Body", "Skin", "skin pigmentation", 2, "PFSSCZ",
                "皮肤颜色变褐、暗黑"));
        list.add(new Symptom(501, "Whole Body", "Skin", "scaly skin", 2, "JFJC",
                "皮肤干燥粗糙呈褐色，状若鱼鳞"));
        list.add(new Symptom(506, "Whole Body", "Skin", "chickenpox", 2, "SD1",
                "小儿皮肤出现粉红色斑丘疹，很快变成椭圆形小水疱"));
        list.add(new Symptom(507, "Whole Body", "Skin", "break out in a rash", 2, "CZ", "皮肤出现红色或紫红色、粟粒状疹点"));
        list.add(new Symptom(508, "Whole Body", "Skin", "rubella", 2, "FZ1", "皮肤骤然出现细小淡红色的疹点"));
        list.add(new Symptom(509, "Whole Body", "Skin", "wheal", 2, "FT1", "皮肤骤然出现隆起的斑丘团块"));
        list.add(new Symptom(511, "Whole Body", "Skin", "carbuncle, furuncle, prickly heat", 2, "YJFZ", "肌肤生痈、疖、痱子等"));
        list.add(new Symptom(512, "Whole Body", "Skin", "vesicle", 2, "PZ", "皮肤出现水疱状疹点"));
        list.add(new Symptom(535, "Whole Body", "Skin", "hair loss", 2, "MFTL", "头发甚至毫毛脱落"));
        list.add(new Symptom(538, "Whole Body", "Skin", "stigmata", 2, "PFHB",
                "皮肤出现不高出肤面的红色斑块"));
        list.add(new Symptom(616, "Whole Body", "Skin", "poor skin elasticity", 2, "PFTXC",
                "以手牵起皮肤，放开后不即刻恢复"));
        list.add(new Symptom(617, "Whole Body", "Skin", "thick, hard and swelling skin", 2, "PFCHYZ",
                "皮肤变得粗糙、肿厚，甚至变硬"));
        list.add(new Symptom(620, "Whole Body", "Skin", "dry skin", 2, "PFGZ", "皮肤干燥而枯涩不润"));
        list.add(new Symptom(110, "Whole Body", "Body", "body pain", 2, "ST", "泛指没有明确部位的身体疼痛"));
        list.add(new Symptom(180, "Whole Body", "Body", "qi dragging sensation", 2, "QXZG", "自觉气往下坠"));
        list.add(new Symptom(181, "Whole Body", "Body", "qi uprushing sensation", 2, "QSCG", "自觉似有气流向上冲顶"));
        list.add(new Symptom(183, "Whole Body", "Body", "sour and heavy body", 2, "STSKZ",
                "身体酸软沉重的感觉"));
        list.add(new Symptom(189, "Whole Body", "Body", "muscular twitching", 2, "JTRMR",
                "筋肉偶尔不自主的抽掣跳动"));
        list.add(new Symptom(295, "Whole Body", "Body", "no flatus", 2, "SQW",
                "没有肠道内气体从肛门排出时发出的声响"));
        list.add(new Symptom(433, "Whole Body", "Body", "fontanel protrusion", 2, "XMTQ", "小儿囟门凸起"));
        list.add(new Symptom(434, "Whole Body", "Body", "sunken fontanel", 2, "XMAX", "小儿囟门凹陷"));
        list.add(new Symptom(483, "Whole Body", "Body", "visceroptosis", 2, "NZXC",
                "胃、肝、肾等内脏的位置下垂"));
        list.add(new Symptom(495, "Whole Body", "Body", "repeated edema", 2, "FFSZ",
                "长期或反复发作的水肿，按之凹陷不能即起"));
        list.add(new Symptom(496, "Whole Body", "Body", "new contraction edema", 2, "XQSZ",
                "突然起、时间短的水肿，按之凹陷不能即起"));
        list.add(new Symptom(497, "Whole Body", "Body", "edema", 2, "SZ", "泛指未标明新久等的水肿"));
        list.add(new Symptom(498, "Whole Body", "Body", "localized edema", 2, "JXXSZ",
                "某局部出现不对称的水肿"));
        list.add(new Symptom(503, "Whole Body", "Body", "telangiectasis", 2, "SZHL",
                "某些局部皮肤的细小血管充盈，如红丝缠绕"));
        list.add(new Symptom(510, "Whole Body", "Body", "miliaria alba", 2, "BNP",
                "胸颈部皮肤出现晶莹如粟，高出皮肤，内含浆液，擦破流水的疱疹"));
        list.add(new Symptom(513, "Whole Body", "Body", "skin ulceration", 2, "HBKL", "患病处肌肤出现溃烂"));
        list.add(new Symptom(514, "Whole Body", "Body", "red swollen skin", 2, "HBHZ", "患病处肌肤色红而肿起"));
        list.add(new Symptom(515, "Whole Body", "Body", "pus or abscess", 2, "NZHLN",
                "肢体肌肤出现脓肿或流脓液"));
        list.add(new Symptom(516, "Whole Body", "Body", "opisthotonos", 2, "JGFZ", "项部拘急强硬向后仰。"));
        list.add(new Symptom(518, "Whole Body", "Body", "startle", 2, "JT", "突然受惊而肢体跳动"));
        list.add(new Symptom(526, "Whole Body", "Body", "hemiplegia", 2, "BSBS",
                "一侧肢体不能随意活动。又名偏瘫"));
        list.add(new Symptom(536, "Whole Body", "Body", "scaling or chapping", 2, "TXHJL",
                "脚或手、口唇等处脱屑或出现皲裂"));
        list.add(new Symptom(539, "Whole Body", "Body", "exudate resinosis", 2, "SYLZS",
                "创面或患处有脂样液体渗出"));
        list.add(new Symptom(247, "Whole Body", "Appetite", "aversion to food or loss of appetite", 2, "NDES",
                "无饥饿、无要求进食之感，可食可不食，甚至厌恶进食"));
        list.add(new Symptom(249, "Whole Body", "Appetite", "no appetite for a long time", 2, "JBYS1",
                "长期不想进食。又名食欲不振、纳谷不香"));
        list.add(new Symptom(250, "Whole Body", "Appetite", "frequent low food intake", 2, "CQSS", "长期实际进食量减少"));
        list.add(new Symptom(251, "Whole Body", "Appetite", "poor appetite", 2, "JSWW", "进食无欣快感，口中无味"));
        list.add(new Symptom(252, "Whole Body", "Appetite", "stuffiness and distention after eating", 2, "SHPZ", "进食后脘腹部有痞胀感"));
        list.add(new Symptom(253, "Whole Body", "Appetite", "hunger without appetite", 2, "JBYS",
                "虽有饥饿感，但不欲进食或进食不多"));
        list.add(new Symptom(254, "Whole Body", "Appetite", "large food intake with rapid hungering", 2, "DSYJ",
                "食欲亢进，每次进食量多，且容易饥饿。又名消谷善饥"));
        list.add(new Symptom(255, "Whole Body", "Appetite", "distaste for greasy food", 2, "YYN", "不愿吃油腻食品、闻油烟气味"));
        list.add(new Symptom(256, "Whole Body", "Appetite", "parorexia", 2, "SSYW",
                "喜欢吃盐、木炭、谷、米等特殊物质"));
        list.add(new Symptom(195, "Whole Body", "Sleep", "insomnia", 2, "SM", "不易入睡或睡后易醒，睡眠时间减少"));
        list.add(new Symptom(196, "Whole Body", "Sleep", "dreamfulness", 2, "DM", "睡后容易作梦、做梦多"));
        list.add(new Symptom(197, "Whole Body", "Sleep", "light sleep", 2, "SMBS", "睡眠不深，容易醒、时时醒"));
        list.add(new Symptom(198, "Whole Body", "Sleep", "forgetfulness", 2, "JW", "记忆力减退"));
        list.add(new Symptom(199, "Whole Body", "Sleep", "somnolence", 2, "SS",
                "想睡，容易入睡，睡眠时间增加。又名多寐"));
        list.add(new Symptom(26, "Whole Body", "Body Temperature", "new contraction low-fever", 2, "XQWFR", "新起有轻微发烧感觉"));
        list.add(new Symptom(27, "Whole Body", "Body Temperature", "slight aversion to wind-cold", 2, "WEFH",
                "新起有轻微怕冷的感觉，避风可缓。又名恶风"));
        list.add(new Symptom(28, "Whole Body", "Body Temperature", "new contraction strong aversion to cold", 2, "XQEHZ",
                "新起自觉怕冷严重，得温不解"));
        list.add(new Symptom(29, "Whole Body", "Body Temperature", "new contraction high-fever", 2, "XQFRZ", "新起自觉发烧重"));
        list.add(new Symptom(30, "Whole Body", "Body Temperature", "high fever with slight aversion to co1d", 2, "FRZEHQ",
                "新起自觉发烧且有轻微怕冷"));
        list.add(new Symptom(31, "Whole Body", "Body Temperature", "fever with aversion to cold", 2, "EHFR", "新起既感到怕冷，又有发烧感"));
        list.add(new Symptom(32, "Whole Body", "Body Temperature", "shiver", 2, "HZ", "自觉寒冷甚，且躯体颤抖"));
        list.add(new Symptom(33, "Whole Body", "Body Temperature", "exuberant fever", 2, "ZR", "高热持续不退。又名高热"));
        list.add(new Symptom(35, "Whole Body", "Body Temperature", "hiding fever", 2, "SRBY",
                "自觉发热，初扪热并不明显，久之则明显烫手"));
        list.add(new Symptom(36, "Whole Body", "Body Temperature", "tidal fever", 2, "CR", "按时发热或按时热甚"));
        list.add(new Symptom(37, "Whole Body", "Body Temperature", "alternating chills and fever", 2, "WLHR",
                "恶寒与发热交替出现。又名寒热往来"));
        list.add(new Symptom(38, "Whole Body", "Body Temperature", "fever aggravated at night", 2, "SRYS", "夜间发热明显，较白天为甚"));
        list.add(new Symptom(39, "Whole Body", "Body Temperature", "fever", 2, "FR",
                "泛指体温升高，除微发热、壮热、潮热、身热夜甚等之外的发热"));
        list.add(new Symptom(40, "Whole Body", "Body Temperature", "feeling hot without fever", 2, "ZJFR", "自觉发烧"));
        list.add(new Symptom(43, "Whole Body", "Body Temperature", "frequent aversion to cold", 2, "JCWL", "长期怕冷。简称畏冷"));
        list.add(new Symptom(47, "Whole Body", "Body Temperature", "hemi-lateral cold", 2, "BCHL", "半边身体较凉"));
        list.add(new Symptom(49, "Whole Body", "Body Temperature", "low heat for a long time", 2, "JYDR", "长期有轻微发热"));
        list.add(new Symptom(50, "Whole Body", "Body Temperature", "fever in hands and feet centers", 2, "SZXS", "手足心发热"));
        list.add(new Symptom(51, "Whole Body", "Body Temperature", "bone-steaming fever", 2, "GZFR",
                "发热似从骨中蒸发而出。简称骨蒸"));
        list.add(new Symptom(52, "Whole Body", "Body Temperature", "fever  after exertion", 2, "LLHFR", "活动劳累则有发热感"));
        list.add(new Symptom(53, "Whole Body", "Body Temperature", "fever due to dysphoria", 2, "FZFR",
                "情绪烦躁时则觉身体躁热。简称烦热"));
        list.add(new Symptom(62, "Whole Body", "Body Temperature", "cold in mouth and nose", 2, "KBQL", "口鼻呼出的气体有凉觉"));
        list.add(new Symptom(359, "Whole Body", "Body Temperature", "fever with macula and papule", 2, "SRBZ",
                "皮肤出现红色斑块或斑点，并有发热或胸腹部烫手"));
        list.add(new Symptom(612, "Whole Body", "Body Temperature", "cold limbs but hot body", 2, "ZJESZ",
                "四肢尤其是下肢冷，但胸腹部烫手"));
        list.add(new Symptom(613, "Whole Body", "Body Temperature", "cold limbs and cold body", 2, "ZJQSL",
                "四肢尤其是下肢冷，胸腹部亦偏凉"));
        list.add(new Symptom(1006, "Whole Body", "Body Temperature", "intermittent fever", 2, "zfhr", ""));
        list.add(new Symptom(491, "Whole Body", "Physique", "weak physique", 2, "STSR", "身体历来比较虚弱"));
        list.add(new Symptom(492, "Whole Body", "Physique", "emaciation", 2, "XTXS", "身体消瘦，体重减轻"));
        list.add(new Symptom(493, "Whole Body", "Physique", "obesity", 2, "XTFP", "身体肥胖，体重增加"));
        list.add(new Symptom(541, "Whole Body", "Physique", "gait disturbance", 2, "BTBW", "走路不稳"));
        list.add(new Symptom(1001, "Whole Body", "Physique", "obesity", 2, "fp",
                "1、体形肥胖 2、超重或BMI>24 3、腰围男性大于85，女性大于80"));
        list.add(new Symptom(540, "Whole Body", "Lump", "round / soft or cystic masses", 2, "ZKYRHRNZ",
                "肿块形态圆，按之软"));
        list.add(new Symptom(619, "Whole Body", "Lump", "hard and unsmooth lump", 2, "ZKZYBP",
                "肿块质地坚硬，边界凹凸不平"));

        //头面部
        list.add(new Symptom(161, "Head and Face", "Nose", "nasal congestion with clear snivel", 2, "BSLQT",
                "新起鼻腔堵塞不通气，或流清涕"));
        list.add(new Symptom(193, "Head and Face", "Nose", "rhinocnesmus", 2, "BY1", "鼻腔瘙痒感"));
        list.add(new Symptom(352, "Head and Face", "Nose", "epistaxis", 2, "BN", "鼻中出血"));
        list.add(new Symptom(370, "Head and Face", "Nose", "sneezing", 2, "PT", "新起打喷嚏"));
        list.add(new Symptom(393, "Head and Face", "Nose", "constant snoring/ sound asleep", 2, "hSBZHS",
                "神识不清，鼾声不停"));
        list.add(new Symptom(452, "Head and Face", "Nose", "nares flaring", 2, "BYSD", "鼻翼随呼吸而张开与缩陷"));
        list.add(new Symptom(453, "Head and Face", "Nose", "dry mouth and nose", 2, "BCGZ", "鼻孔和口唇干燥"));
        list.add(new Symptom(87, "Head and Face", "Ear", "ear pain", 2, "ET", "自觉耳廓或耳道内疼痛"));
        list.add(new Symptom(157, "Head and Face", "Ear", "abrupt tinnitus", 2, "EBM", "新起、突然耳内鸣响"));
        list.add(new Symptom(158, "Head and Face", "Ear", "lasting tinnitus", 2, "EJM", "经常、长期耳内鸣响"));
        list.add(new Symptom(159, "Head and Face", "Ear", "new- contraction deafness", 2, "XBSCL",
                "新起、突然听力下降甚至丧失"));
        list.add(new Symptom(451, "Head and Face", "Ear", "swelling ear with pus", 2, "EZLN", "外耳道肿胀，流出脓液"));
        list.add(new Symptom(471, "Head and Face", "Ear", "dry ear", 2, "ELGK", "dry ear"));
        list.add(new Symptom(233, "Head and Face", "Respiration", "new contraction asthma", 2, "XBQC",
                "新起疾病出现呼吸困难、迫促"));
        list.add(new Symptom(234, "Head and Face", "Respiration", "long illness with asthma", 2, "JBQC", "病久而长期出现呼吸困难"));
        list.add(new Symptom(235, "Head and Face", "Respiration", "asthma", 2, "QC", "泛指未分新久的呼吸困难"));
        list.add(new Symptom(236, "Head and Face", "Respiration", "short of breath", 2, "QD",
                "自觉呼吸短促而不相接续，气少不够用。又名短气"));
        list.add(new Symptom(237, "Head and Face", "Respiration", "panting with inability to lie flat", 2, "CBNW",
                "因呼吸困难而不能平卧，卧则气喘加重"));
        list.add(new Symptom(387, "Head and Face", "Respiration", "belching", 2, "AQ",
                "胃中气体上出咽喉所发出的一种长而缓的声音"));
        list.add(new Symptom(388, "Head and Face", "Respiration", "belching with sour acid", 2, "AQSS", "嗳出的气体有酸馊味"));
        list.add(new Symptom(389, "Head and Face", "Respiration", "hiccup; hiccough", 2, "EN",
                "不自主的从咽喉发出一种“呃、呃”作响的短促冲击声"));
        list.add(new Symptom(397, "Head and Face", "Respiration", "weak breath", 2, "QXWR", "呼吸时口鼻气息很微弱"));
        list.add(new Symptom(474, "Head and Face", "Neck", "venous engorgement", 2, "JMNZ", "颈部两侧静脉充盈显露"));
        list.add(new Symptom(210, "Head and Face", "Cough", "Cough", 2, "KS1",
                "气流通过喉部而发出“咳、咳”的声音"));
        list.add(new Symptom(211, "Head and Face", "Cough", "new contraction cough", 2, "XBKS", "新起疾病出现咳嗽"));
        list.add(new Symptom(212, "Head and Face", "Cough", "paroxysmal cough", 2, "ZFQK", "突然出现一阵阵地咳嗽"));
        list.add(new Symptom(213, "Head and Face", "Cough", "dry cough", 2, "GK", "咳而无痰"));
        list.add(new Symptom(349, "Head and Face", "Cough", "hemoptysis", 2, "KX", "血液随咳嗽而咯出。又名咯血"));
        list.add(new Symptom(92, "Head and Face", "Mouth", "stomalgia", 2, "KQT", "自觉口腔内疼痛。简称口痛"));
        list.add(new Symptom(187, "Head and Face", "Mouth", "numbness of mouth and tongue", 2, "KSFM",
                "口腔或舌体发麻，感觉减退"));
        list.add(new Symptom(238, "Head and Face", "Mouth", "no thirst", 2, "KBK", "口不干而不想饮水"));
        list.add(new Symptom(239, "Head and Face", "Mouth", "thirst", 2, "KK", "口中干燥而欲饮水"));
        list.add(new Symptom(240, "Head and Face", "Mouth", "thirst for cold drink", 2, "KYYL", "口干而欲饮冷水"));
        list.add(new Symptom(241, "Head and Face", "Mouth", "thirst for warm drink", 2, "KYYR", "口干而欲饮热水"));
        list.add(new Symptom(242, "Head and Face", "Mouth", "thirst without desire to drink", 2, "KBYY", "虽口干而不欲饮水"));
        list.add(new Symptom(258, "Head and Face", "Mouth", "bitter taste in mouth", 2, "KK1", "自觉口中有苦味"));
        list.add(new Symptom(259, "Head and Face", "Mouth", "bland taste in mouth", 2, "KD", "味觉减退，自觉口中发淡而无味"));
        list.add(new Symptom(260, "Head and Face", "Mouth", "sweet taste in mouth", 2, "KT1", "自觉口中有甜味"));
        list.add(new Symptom(261, "Head and Face", "Mouth", "salty taste in mouth", 2, "KX1", "自觉口中有咸味"));
        list.add(new Symptom(262, "Head and Face", "Mouth", "sticky sensation in mouth", 2, "KNN", "自觉口中黏腻不爽"));
        list.add(new Symptom(263, "Head and Face", "Mouth", "sour taste in mouth", 2, "KS",
                "自觉口中有酸味，甚至闻到酸腐气味"));
        list.add(new Symptom(404, "Head and Face", "Mouth", "fetid mouth odor", 2, "KC", "口中有臭秽气"));
        list.add(new Symptom(415, "Head and Face", "Mouth", "angular salivation", 2, "KJLX", "时常有涎液从口角流出"));
        list.add(new Symptom(454, "Head and Face", "Mouth", "pale lips", 2, "CD", "咀唇颜色浅淡而少红"));
        list.add(new Symptom(456, "Head and Face", "Mouth", "purple lips", 2, "CZ1", "咀唇颜色紫暗"));
        list.add(new Symptom(459, "Head and Face", "Mouth", "oral ulceration", 2, "KQCL", "口腔黏膜有红色溃烂面"));
        list.add(new Symptom(460, "Head and Face", "Mouth", "oral erosion", 2, "KQML", "口腔黏膜溃烂而颜色不红"));
        list.add(new Symptom(85, "Head and Face", "Face", "facial pain", 2, "MT1", "自觉颜面部疼痛"));
        list.add(new Symptom(88, "Head and Face", "Face", "parotid swelling and pain", 2, "SZT", "自觉耳前下方面颊处肿起疼痛"));
        list.add(new Symptom(435, "Head and Face", "Face", "bloated face and eyelid", 2, "MJFZ", "面部和眼睑浮肿"));
        list.add(new Symptom(418, "Head and Face", "Complexion", "lusterless complexion", 2, "MSSH", "面部颜色缺少光泽"));
        list.add(new Symptom(419, "Head and Face", "Complexion", "sallow complexion", 2, "MSWH", "面部颜色偏黄而不润泽"));
        list.add(new Symptom(420, "Head and Face", "Complexion", "orange-like yellowish complexion", 2, "MHRJ", "面色黄而鲜明如橘色"));
        list.add(new Symptom(421, "Head and Face", "Complexion", "yellowish, dark complexion", 2, "MHSA", "面色黄而晦暗如烟熏"));
        list.add(new Symptom(422, "Head and Face", "Complexion", "opaque complexion", 2, "MSHA",
                "面部颜色较黑而暗，缺乏光泽"));
        list.add(new Symptom(423, "Head and Face", "Complexion", "plain white complexion", 2, "MSDB", "面部颜色较白而缺乏光泽"));
        list.add(new Symptom(424, "Head and Face", "Complexion", "pale white complexion", 2, "MSCB", "面部颜色很白而带青紫"));
        list.add(new Symptom(425, "Head and Face", "Complexion", "pallor", 2, "MSHB",
                "面部皮肤较润，颜色白而略有反光"));
        list.add(new Symptom(426, "Head and Face", "Complexion", "blackish complexion", 2, "MSLH", "面部颜色较黑而略有光泽"));
        list.add(new Symptom(428, "Head and Face", "Complexion", "hectic cheek", 2, "QH", "两颧部位显红色"));
        list.add(new Symptom(350, "Head and Face", "Vomiting", "haematemesis", 2, "OX", "血液经口随呕吐而出。又名吐血"));
        list.add(new Symptom(365, "Head and Face", "Vomiting", "bleeding with indigestive food", 2, "XZJBXHSW",
                "吐出的血中含有不消化的食物"));
        list.add(new Symptom(377, "Head and Face", "Vomiting", "nausea", 2, "EX", "自觉胃部有气上逆而欲呕"));
        list.add(new Symptom(378, "Head and Face", "Vomiting", "Vomiting", 2, "OT", "胃内容物从口中吐出"));
        list.add(new Symptom(379, "Head and Face", "Vomiting", "retching", 2, "GO", "有呕吐动作、声音而无物吐出"));
        list.add(new Symptom(380, "Head and Face", "Vomiting", "turbid and sour vomitus", 2, "OTSS",
                "呕吐物为酸水，或酸水自胃中上至咽喉，随即吞咽而下。又名吞酸"));
        list.add(new Symptom(381, "Head and Face", "Vomiting", "bitter vomitus", 2, "OTKS", "呕吐物为苦水"));
        list.add(new Symptom(382, "Head and Face", "Vomiting", "dilute vomitus", 2, "OTQS", "呕吐物为清水"));
        list.add(new Symptom(383, "Head and Face", "Vomiting", "vomiting sputum and", 2, "OTTX", "呕吐物为痰涎"));
        list.add(new Symptom(384, "Head and Face", "Vomiting", "vomiting foul and indigestive food", 2, "OTSSHSS",
                "呕吐物为馊食或未消化的食物"));
        list.add(new Symptom(385, "Head and Face", "Vomiting", "vomiting ascaris", 2, "OTHC", "呕吐出蛔虫"));
        list.add(new Symptom(386, "Head and Face", "Vomiting", "bowel-like vomitus", 2, "TFYW", "呕吐物如粪样有臭秽气"));
        list.add(new Symptom(91, "Head and Face", "Tongue", "glossalgia", 2, "ST1", "自觉舌体疼痛"));
        list.add(new Symptom(354, "Head and Face", "Tongue", "tongue bleeding", 2, "SN", "舌体上有出血斑点"));
        list.add(new Symptom(544, "Head and Face", "Tongue", "pale tongue", 2, "SDB",
                "比正常舌色浅，白色偏多红色偏少（给你发的中为舌淡）"));
        list.add(new Symptom(545, "Head and Face", "Tongue", "pale and enlarged tongue", 2, "SDP", "舌色浅淡，舌体胖大"));
        list.add(new Symptom(546, "Head and Face", "Tongue", "pale and purple tongue", 2, "SDZ", "舌色浅淡而带紫色"));
        list.add(new Symptom(547, "Head and Face", "Tongue", "red tongue", 2, "SH",
                "比正常舌色红，甚至呈鲜红色（给你发的中为舌赤）"));
        list.add(new Symptom(548, "Head and Face", "Tongue", "crimson tongue", 2, "SJ", "舌色深红呈绛色"));
        list.add(new Symptom(549, "Head and Face", "Tongue", "small red tongue", 2, "SHNX", "舌色红，舌体较小，舌质嫩"));
        list.add(new Symptom(550, "Head and Face", "Tongue", "red fat tongue", 2, "SHP", "舌色红，舌体胖大"));
        list.add(new Symptom(551, "Head and Face", "Tongue", "dark red tongue", 2, "SAH", "舌红而暗"));
        list.add(new Symptom(553, "Head and Face", "Tongue", "tongue red at the tip", 2, "SJH", "舌尖部色红"));
        list.add(new Symptom(556, "Head and Face", "Tongue", "prickly tongue", 2, "SQMC",
                "舌尖等处有红色点状突起如刺，摸之棘手"));
        list.add(new Symptom(557, "Head and Face", "Tongue", "enlarged tongue", 2, "STPD",
                "舌体比正常舌体大而厚，盈口满嘴"));
        list.add(new Symptom(558, "Head and Face", "Tongue", "thin tongue", 2, "STSX", "舌体较正常舌体小而薄"));
        list.add(new Symptom(559, "Head and Face", "Tongue", "fissured tongue", 2, "SYLW", "舌面出现裂纹、裂沟"));
        list.add(new Symptom(560, "Head and Face", "Tongue", "purple and dark tongue", 2, "SZA", "舌体色紫而无光泽"));
        list.add(new Symptom(561, "Head and Face", "Tongue", "spotted tongue", 2, "SYBD", "舌体有紫色斑点条纹"));
        list.add(new Symptom(562, "Head and Face", "Tongue", "teeth-marked tongue", 2, "SBCY", "舌体边缘有牙齿压迫的痕迹"));
        list.add(new Symptom(563, "Head and Face", "Tongue", "flaccid tongue", 2, "STWR",
                "舌体软弱无力，不能随意伸缩回旋"));
        list.add(new Symptom(564, "Head and Face", "Tongue", "deviated tongue", 2, "STWX",
                "伸舌时舌体偏向一侧，或左或右"));
        list.add(new Symptom(566, "Head and Face", "Tongue", "trembling tongue", 2, "STCD",
                "伸舌时舌体颤动，或舌体时时伸出口外，或舌舐口唇四周"));
        list.add(new Symptom(567, "Head and Face", "Tongue", "stiff tongue", 2, "STQY",
                "舌体失其柔和，卷伸不利，或板硬强直，不能转动"));
        list.add(new Symptom(568, "Head and Face", "Tongue", "white tongue as mirror", 2, "SBRJ",
                "舌色白光，白如镜，毫无血色"));
        list.add(new Symptom(569, "Head and Face", "Tongue", "red tongue as mirror", 2, "SHRJ", "舌色红光，红如镜"));
        list.add(new Symptom(570, "Head and Face", "Tongue", "dark purple tongue", 2, "SJSZ", "舌色深红带紫，呈绛紫色"));
        list.add(new Symptom(571, "Head and Face", "Tongue", "fester tongue body", 2, "STKL", "舌体出现溃烂"));
        list.add(new Symptom(572, "Head and Face", "Tongue", "dry tongue body", 2, "STGZ", "舌体干燥乏津"));
        list.add(new Symptom(573, "Head and Face", "Tongue", "sublingual vein", 2, "SXLMQZ",
                "舌下络脉充盈粗胀、纽曲"));
        list.add(new Symptom(575, "Head and Face", "Tongue", "white coating", 2, "STB", "舌苔色白"));
        list.add(new Symptom(576, "Head and Face", "Tongue", "white tongue coating like powder", 2, "TBRJF",
                "舌苔色白，干燥，如白粉堆积舌面"));
        list.add(new Symptom(577, "Head and Face", "Tongue", "putrid and grimy tongue coating", 2, "STFG",
                "舌苔疏松、垢秽不洁，如豆腐渣堆积舌面，揩之可去"));
        list.add(new Symptom(579, "Head and Face", "Tongue", "yellow tongue coating", 2, "STH", "舌苔呈黄色"));
        list.add(new Symptom(580, "Head and Face", "Tongue", "grey and black tongue coating", 2, "STHH", "舌苔呈灰黑色"));
        list.add(new Symptom(581, "Head and Face", "Tongue", "yellow with white tongue coating", 2, "STHBXJ",
                "舌苔呈黄白相兼颜色"));
        list.add(new Symptom(582, "Head and Face", "Tongue", "greasy tongue coating", 2, "STN",
                "舌腻细腻、致密，如涂油腻之状，紧贴舌面，不易揩去"));
        list.add(new Symptom(583, "Head and Face", "Tongue", "peeling, few or no tongue coating", 2, "TBSW",
                "舌苔剥落，或舌苔少，或无舌苔"));
        list.add(new Symptom(584, "Head and Face", "Tongue", "thick tongue coating", 2, "TH", "不能透过舌苔见到舌体"));
        list.add(new Symptom(585, "Head and Face", "Tongue", "lubricating tongue coating", 2, "STRH",
                "舌苔干湿适中而润，或津多而滑"));
        list.add(new Symptom(586, "Head and Face", "Tongue", "dry tongue coating", 2, "STGZ1", "舌苔干燥乏津"));
        list.add(new Symptom(215, "Head and Face", "Phlegm", "spit", 2, "TT", "咯出痰液"));
        list.add(new Symptom(216, "Head and Face", "Phlegm", "excessive and thick phlegm", 2, "TDZC", "咯多量黏稠痰"));
        list.add(new Symptom(217, "Head and Face", "Phlegm", "scant and thick phlegm", 2, "TSZC", "咯少量黏稠痰"));
        list.add(new Symptom(218, "Head and Face", "Phlegm", "sticky phlegm and hard to spit", 2, "TNNK", "痰黏稠而难以咯出"));
        list.add(new Symptom(219, "Head and Face", "Phlegm", "excessive and thin phlegm", 2, "TDZX", "咯多量稀薄痰"));
        list.add(new Symptom(220, "Head and Face", "Phlegm", "scant and thin phlegm", 2, "TSZX", "咯少量稀薄痰"));
        list.add(new Symptom(222, "Head and Face", "Phlegm", "excessive foam phlegm", 2, "PMTD", "咯多量稀薄的泡沫状痰液"));
        list.add(new Symptom(223, "Head and Face", "Phlegm", "rusty expectoration", 2, "TXST", "咯出的痰呈铁锈色"));
        list.add(new Symptom(224, "Head and Face", "Phlegm", "white phlegm", 2, "TSB", "咯出的痰呈白色"));
        list.add(new Symptom(226, "Head and Face", "Phlegm", "green phlegm", 2, "TSL", "咯出的痰呈呈绿色"));
        list.add(new Symptom(227, "Head and Face", "Phlegm", "yellow phlegm", 2, "TSH", "咯出的痰呈黄色"));
        list.add(new Symptom(228, "Head and Face", "Phlegm", "stinking phlegm", 2, "XCT", "咯出的痰有腥臭气"));
        list.add(new Symptom(229, "Head and Face", "Phlegm", "purulent phlegm", 2, "NXT", "咯出的痰稠厚呈脓状"));
        list.add(new Symptom(230, "Head and Face", "Phlegm", "blood in phlegm", 2, "TZDX", "咯出的痰中夹有血液或血丝"));
        list.add(new Symptom(231, "Head and Face", "Phlegm", "slippery phlegm and easy to spit", 2, "THYK", "痰滑而容易咯出"));
        list.add(new Symptom(81, "Head and Face", "Head", "parietal headache", 2, "DDT", "自觉头顶部疼痛"));
        list.add(new Symptom(82, "Head and Face", "Head", "migraine", 2, "PTT", "自觉头的左或右半边疼痛"));
        list.add(new Symptom(83, "Head and Face", "Head", "occipital pain", 2, "HTT", "自觉后头、枕部疼痛"));
        list.add(new Symptom(84, "Head and Face", "Head", "headache and painful stiff nape", 2, "TXQT",
                "自觉头痛而项部有拘急牵强不适感"));
        list.add(new Symptom(124, "Head and Face", "Head", "fixed pain", 2, "GDT", "疼痛部位固定不移"));
        list.add(new Symptom(125, "Head and Face", "Head", "tingling pain", 2, "CT", "疼痛呈针刺、鸡啄米样"));
        list.add(new Symptom(126, "Head and Face", "Head", "wandering pain", 2, "YZT",
                "四肢关节等处的疼痛部位时有转移而不固定"));
        list.add(new Symptom(127, "Head and Face", "Head", "stuffy pain", 2, "MT", "带有紧闷不舒感的疼痛"));
        list.add(new Symptom(128, "Head and Face", "Head", "empty pain", 2, "KT", "带有空虚感的疼痛"));
        list.add(new Symptom(135, "Head and Face", "Head", "pain worsens at night", 2, "YJTS", "夜晚疼痛加重"));
        list.add(new Symptom(145, "Head and Face", "Head", "dizziness", 2, "TY",
                "头脑旋晕，感觉自身或眼前景物旋转。又名头眩、头旋"));
        list.add(new Symptom(148, "Head and Face", "Head", "heaviness of head", 2, "TZ", "自觉头部重坠"));
        list.add(new Symptom(156, "Head and Face", "Head", "ringing in head", 2, "NM", "自觉头内鸣响有声。又名头中鸣响"));
        list.add(new Symptom(178, "Head and Face", "Head", "heavy head and light feet", 2, "TZJQG",
                "自觉头部沉重而下肢轻飘，有行走不稳的感觉"));
        list.add(new Symptom(182, "Head and Face", "Head", "heavy head as if wrapped", 2, "TMRG", "头部如被蒙裹的感觉"));
        list.add(new Symptom(522, "Head and Face", "Head", "trembling limbs, shaking head", 2, "ZCTY",
                "手足颤抖，头部摇晃不能自主"));
        list.add(new Symptom(133, "Head and Face", "Head", "pain worsens in cloudy and rainy days", 2, "YYTTTJZ",
                "遇阴雨气候而疼痛加重"));
        list.add(new Symptom(436, "Head and Face", "Hair", "dry and withered hair", 2, "FKQCJS",
                "头发无泽，枯干易折，多根头发黏连在一起，如穗状。"));
        list.add(new Symptom(437, "Head and Face", "Hair", "grey hair with hair loss", 2, "FBYT",
                "头发容易脱落，或头发色白而枯槁"));
        list.add(new Symptom(90, "Head and Face", "Teeth", "toothache", 2, "YT1", "自觉牙齿疼痛。又名齿痛"));
        list.add(new Symptom(353, "Head and Face", "Teeth", "gum bleeding", 2, "CN", "血自齿缝或牙龈渗出"));
        list.add(new Symptom(462, "Head and Face", "Teeth", "receding gum", 2, "YYWS", "龈肉萎缩"));
        list.add(new Symptom(463, "Head and Face", "Teeth", "loose tooth", 2, "YCSD", "牙齿松，触之能摇动"));
        list.add(new Symptom(464, "Head and Face", "Teeth", "red swollen gums", 2, "YYHZ", "牙龈色赤而肿起"));
        list.add(new Symptom(465, "Head and Face", "Teeth", "clenched jaw", 2, "YGJB", "上下牙咬合而不张开"));
        list.add(new Symptom(472, "Head and Face", "Teeth", "blackening of the teeth", 2, "YCJH", "牙齿颜色焦黑"));
        list.add(new Symptom(93, "Head and Face", "Throat", "sore throat", 2, "YHT", "自觉咽喉部位疼痛"));
        list.add(new Symptom(163, "Head and Face", "Throat", "itchy throat", 2, "HY", "自觉咽喉部作痒"));
        list.add(new Symptom(164, "Head and Face", "Throat", "pharyngeal paraesthesia", 2, "YBYWG",
                "自觉咽喉部似有异物，吞之不下、吐之不出"));
        list.add(new Symptom(232, "Head and Face", "Throat", "wheezing in throat", 2, "HZXMS",
                "呼吸时喉和肺部有尖锐鸣响声音"));
        list.add(new Symptom(243, "Head and Face", "Throat", "dry throat", 2, "YG", "咽喉部有干燥感觉"));
        list.add(new Symptom(246, "Head and Face", "Throat", "polydipsia without hot sensation", 2, "WREYD", "并不发热而喝水多"));
        list.add(new Symptom(257, "Head and Face", "Throat", "blockage when swallowing", 2, "TSGS",
                "吞食时咽喉或胸骨后有堵塞感，吞咽食物不畅甚至完全不能吞下"));
        list.add(new Symptom(394, "Head and Face", "Throat", "pleghm ringing or distending in throat", 2, "HZTMTY",
                "随呼吸而喉部有痰声鸣响，或咽喉部有痰壅堵"));
        list.add(new Symptom(466, "Head and Face", "Throat", "red swollen throat", 2, "YHHZ", "咽喉部颜色红而肿起"));
        list.add(new Symptom(467, "Head and Face", "Throat", "light red throat without swelling", 2, "YHNHBZ",
                "咽喉部颜色红而不肿"));
        list.add(new Symptom(468, "Head and Face", "Throat", "swollen but not red throat", 2, "YHZBH",
                "咽喉部颜色不红而肿起"));
        list.add(new Symptom(470, "Head and Face", "Throat", "laryngopharyngeal tunica albuginea", 2, "YHBM", "咽喉部出现白色膜片状物"));
        list.add(new Symptom(86, "Head and Face", "Eyes", "eye pain", 2, "MT2", "自觉眼睛疼痛"));
        list.add(new Symptom(147, "Head and Face", "Eyes", "eye distention and distending pain", 2, "YZJZT", "眼内作胀而痛"));
        list.add(new Symptom(149, "Head and Face", "Eyes", "blurred vision", 2, "YH",
                "自觉视物旋转动荡，或眼前如有蚊蝇、雪花飞舞。又名目眩"));
        list.add(new Symptom(150, "Head and Face", "Eyes", "seeing one thing as two", 2, "YWKCLW",
                "视一物成二物而不清"));
        list.add(new Symptom(151, "Head and Face", "Eyes", "dry eyes", 2, "YGS",
                "眼内乏液，自觉眼内干燥、滞涩不适"));
        list.add(new Symptom(152, "Head and Face", "Eyes", "blurred vision", 2, "SWMH",
                "视物模糊不清。或称视物昏花、视力减退、视物不清"));
        list.add(new Symptom(153, "Head and Face", "Eyes", "photophobia", 2, "WG",
                "眼畏惧看光亮，遇光则涩痛、流泪、难睁"));
        list.add(new Symptom(154, "Head and Face", "Eyes", "sudden blindness", 2, "BM", "突然目不视物"));
        list.add(new Symptom(155, "Head and Face", "Eyes", "too much eye gum", 2, "YCD", "眼部有较多眼粪堆积"));
        list.add(new Symptom(351, "Head and Face", "Eyes", "eye bleeding", 2, "YCX", "白睛出现片状高出的红色出血斑"));
        list.add(new Symptom(439, "Head and Face", "Eyes", "light white eyelid", 2, "YJDB", "眼睑颜色淡白少红"));
        list.add(new Symptom(440, "Head and Face", "Eyes", "drooping eyelid", 2, "YJXC", "上眼睑下垂，不能张目"));
        list.add(new Symptom(441, "Head and Face", "Eyes", "sunken socket", 2, "YWAX", "眼眶凹进，眼球深陷"));
        list.add(new Symptom(442, "Head and Face", "Eyes", "black around the eyes", 2, "YZAH", "眼周围显暗黑色"));
        list.add(new Symptom(443, "Head and Face", "Eyes", "protruding eyes", 2, "YT2", "眼球突出"));
        list.add(new Symptom(444, "Head and Face", "Eyes", "euthyphoria", 2, "ZSSC", "眼球向上凝视而不能转动"));
        list.add(new Symptom(445, "Head and Face", "Eyes", "yellow sclera", 2, "MH3", "白睛发黄"));
        list.add(new Symptom(446, "Head and Face", "Eyes", "red eyes without pain", 2, "MCWSK",
                "白睛颜色偏红而无明显痛苦"));
        list.add(new Symptom(447, "Head and Face", "Eyes", "red eyes with swelling eyelid", 2, "MCJZ", "眼球及眼睑色赤而肿"));
        list.add(new Symptom(449, "Head and Face", "Eyes", "sleeping with eyes half-closed", 2, "SHLJ", "入睡后上睑未闭而眼球外露"));
        list.add(new Symptom(458, "Head and Face", "Eyes", "deviation of the eyes and mouth", 2, "KYWX", "口眼向一侧偏斜"));
        list.add(new Symptom(209, "Head and Face", "Voice", "no desire to speak", 2, "LY", "精神疲倦而不想讲活"));
        list.add(new Symptom(372, "Head and Face", "Voice", "low voice", 2, "SD", "说话声音轻迟低微，难以听清"));
        list.add(new Symptom(374, "Head and Face", "Voice", "new contraction aphonia", 2, "XBSS",
                "新起、突然声音嘶哑，甚至不能发声。即新起的声音嘶哑"));
        list.add(new Symptom(375, "Head and Face", "Voice", "aphonia after prolonged illness", 2, "JBSS",
                "长期、经常声音嘶哑，甚至不能发声。即长期的声音嘶哑"));
        list.add(new Symptom(390, "Head and Face", "Voice", "delirious speech", 2, "ZY",
                "神识欠清甚至完全不清而胡言乱语，语无伦次，声高有力"));
        list.add(new Symptom(392, "Head and Face", "Voice", "slurred speech", 2, "YYBL",
                "舌动不灵，说话不流利，吐词不清。又称言謇、语謇、语言謇涩"));

        //胸部
        list.add(new Symptom(167, "Chest", "Breast", "distending pain of breasts", 2, "RFZ", "乳房有作胀的感觉。简称乳胀"));
        list.add(new Symptom(476, "Chest", "Breast", "scant or no breast milk", 1, "SRHWRZ",
                "妇女哺乳期乳汁很少甚至无乳"));
        list.add(new Symptom(615, "Chest", "Breast", "breast lump", 1, "RFJK", "触诊乳房内有结节肿块"));
        list.add(new Symptom(
                1000,
                "Chest",
                "Breast",
                "lobular hyperplasia of breast",
                1,
                "rxxyzs",
                "1、一侧或双侧乳房胀痛或隐痛， 月经前及情绪波动时痛甚。2、单侧或双侧大小不等， 呈片状 盘状 颗粒状 条索状， 质地柔韧， 边界不清，与皮肤无粘连 有触痛的乳房肿块; 3、乳腺铝靶 X 线摄片见较均匀密度增高影"));
        list.add(new Symptom(97, "Chest", "Breast", "breast pain", 2, "RFT", "自觉一侧或两侧乳房疼痛"));
        list.add(new Symptom(98, "Chest", "Ribs", "pain between ribs", 2, "XT3",
                "自觉一侧或两侧腋中线肋缘内或略下处疼痛"));
        list.add(new Symptom(134, "Chest", "Ribs", "comfort after promoting qi circulation", 2, "QXJS",
                "得嗳气、肠鸣、矢气等而疼痛减轻"));
        list.add(new Symptom(173, "Chest", "Ribs", "distention between ribs", 2, "XZ", "自觉一侧或两侧胁部胀满不舒"));
        list.add(new Symptom(168, "Chest", "Heart", "palpitation", 2, "XJ", "自觉心脏跳动不安"));
        list.add(new Symptom(169, "Chest", "Heart", "flusteredness", 2, "XH", "心胸慌乱而难以自持的感觉"));
        list.add(new Symptom(170, "Chest", "Heart", "severe palpitation", 2, "ZZ", "自觉心脏剧烈跳动不安"));
        list.add(new Symptom(171, "Chest", "Heart", "palpitation due to fright", 2, "JJ",
                "因稍有惊骇而自觉心跳明显，或心动不安而恐惧易惊"));
        list.add(new Symptom(122, "Chest", "Chest", "distending pain or scurrying pain", 2, "ZTHCT",
                "胸或腹部胀闷而且疼痛，或疼痛部位走串不定"));
        list.add(new Symptom(94, "Chest", "Chest", "pain in the back of breastbone", 2, "XGHT", "自觉疼痛部位在胸骨后"));
        list.add(new Symptom(95, "Chest", "Chest", "chest pain", 2, "XT1", "泛指整个胸部疼痛"));
        list.add(new Symptom(96, "Chest", "Chest", "heart pain", 2, "XT", "自觉左乳头偏下的胸内疼痛"));
        list.add(new Symptom(122, "Chest", "Chest", "distending pain or scurrying pain", 2, "ZTHCT",
                "胸或腹部胀闷而且疼痛，或疼痛部位走串不定"));
        list.add(new Symptom(172, "Chest", "Chest", "choking sensation in chest", 2, "XM", "自觉胸部痞塞满闷"));
        list.add(new Symptom(126, "Chest", "Chest", "wandering pain", 2, "YZT", "四肢关节等处的疼痛部"));
        list.add(new Symptom(123, "Chest", "Chest", "colicky pain/ colic", 2, "JT1", "心胸或腹部疼痛剧烈，状如绞割"));
        list.add(new Symptom(124, "Chest", "Chest", "fixed pain", 2, "GDT", "疼痛部位固定不移"));
        list.add(new Symptom(125, "Chest", "Chest", "tingling pain", 2, "CT", "疼痛呈针刺、鸡啄米样"));
        list.add(new Symptom(127, "Chest", "Chest", "stuffy pain", 2, "MT", "带有紧闷不舒感的疼痛"));
        list.add(new Symptom(135, "Chest", "Chest", "pain worsens at night", 2, "YJTS", "夜晚疼痛加重"));
        list.add(new Symptom(130, "Chest", "Chest", "cold pain", 2, "LT", "疼痛且有冷凉感而喜暖"));
        list.add(new Symptom(133, "Chest", "Chest", "pain worsens in cloudy and rainy days", 2, "YYTTTJZ",
                "遇阴雨气候而疼痛加重"));
        list.add(new Symptom(136, "Chest", "Chest", "pain relieves in activity or aggravates", 2, "HDTHBDTS",
                "适当活动后疼痛缓解，没有活动时疼痛加重。"));
        list.add(new Symptom(131, "Chest", "Chest", "aching and heavy pain", 2, "SZT1", "带有酸软、沉重感的疼痛"));
        list.add(new Symptom(140, "Chest", "Chest", "pulling pain", 2, "QCT",
                "痛处有抽掣感，一处牵挛到另一处疼痛。又名引痛、彻痛"));
        list.add(new Symptom(165, "Chest", "Chest", "qi blockage sensation", 2, "QGDG",
                "胸部或脘腹部突然有短暂的憋气或气流梗堵觉"));

        //腹部
        list.add(new Symptom(45, "Abdomen", "Abdomen", "cold in abdomen, waist and the back", 2, "WFYBL", "腰、脘、腹等部位有冷感"));
        list.add(new Symptom(55, "Abdomen", "Abdomen", "prefer cold with aversion to heat", 2, "XLER",
                "喜欢凉爽，或得寒凉则病情缓解、减轻，遇温热则加重"));
        list.add(new Symptom(56, "Abdomen", "Abdomen", "prefer warmth with aversion to cold", 2, "XWEL",
                "喜欢温热，或得温热则病情缓解、减轻，遇寒凉则加重"));
        list.add(new Symptom(100, "Abdomen", "Abdomen", "epigastrium pain", 2, "WFT", "自觉剑突下、脐上的脘腹部疼痛"));
        list.add(new Symptom(101, "Abdomen", "Abdomen", "right upper quadrant  pain", 2, "YSFT", "自觉脘腹部的右侧疼痛"));
        list.add(new Symptom(103, "Abdomen", "Abdomen", "perinavel pain", 2, "QFT", "自觉脐周围的腹部疼痛"));
        list.add(new Symptom(104, "Abdomen", "Abdomen", "bilateral lower abdominal pain", 2, "SFT", "自觉脐下左或右侧的腹部疼痛"));
        list.add(new Symptom(105, "Abdomen", "Abdomen", "middle lower abdominal pain", 2, "XFT", "自觉脐下中间的小腹部位疼痛"));
        list.add(new Symptom(106, "Abdomen", "Abdomen", "abdominal pain", 2, "FT",
                "泛指未能明确腹部某具体部位的整个腹部疼痛"));
        list.add(new Symptom(123, "Abdomen", "Abdomen", "colicky pain/ colic", 2, "JT1", "心胸或腹部疼痛剧烈，状如绞割"));
        list.add(new Symptom(130, "Abdomen", "Abdomen", "cold pain", 2, "LT", "疼痛且有冷凉感而喜暖"));
        list.add(new Symptom(131, "Abdomen", "Abdomen", "aching and heavy pain", 2, "SZT1", "带有酸软、沉重感的疼痛"));
        list.add(new Symptom(136, "Abdomen", "Abdomen", "pain relieves in activity or aggravates", 2, "HDTHBDTS",
                "适当活动后疼痛缓解，没有活动时疼痛加重。"));
        list.add(new Symptom(137, "Abdomen", "Abdomen", "worsen after activity", 2, "HDJJ", "活动时症状加重"));
        list.add(new Symptom(141, "Abdomen", "Abdomen", "pain-relieves after eating", 2, "DSTH", "进食后疼痛缓解"));
        list.add(new Symptom(142, "Abdomen", "Abdomen", "pain worsens after eating", 2, "JSTS", "进食后疼痛加重"));
        list.add(new Symptom(174, "Abdomen", "Abdomen", "epigastric stuffiness", 2, "WPZ", "自觉脘腹部胀满、痞闷不舒"));
        list.add(new Symptom(175, "Abdomen", "Abdomen", "epigastric upset", 2, "WWCZ",
                "脘腹部似饥非饥、似痛非痛、似辣非辣，嘈杂不舒的感觉"));
        list.add(new Symptom(176, "Abdomen", "Abdomen", "abdominal distention", 2, "FZ", "自觉整个腹部胀满不舒"));
        list.add(new Symptom(177, "Abdomen", "Abdomen", "middle lower abdominal distention", 2, "XFZ", "自觉脐下小腹部胀满不舒"));
        list.add(new Symptom(398, "Abdomen", "Abdomen", "splashing sound in stomach", 2, "WBZSY",
                "听到胃脘部有水振动的声音"));
        list.add(new Symptom(399, "Abdomen", "Abdomen", "water-like borborygmus", 2, "CMLL", "听到腹内漉漉鸣响有声"));
        list.add(new Symptom(400, "Abdomen", "Abdomen", "high-tune borborygmus", 2, "CMKJ", "听诊肠鸣音增多、声音增强"));
        list.add(new Symptom(401, "Abdomen", "Abdomen", "decreased borborygmus", 2, "CMJR", "听诊肠鸣音减少、声音减弱"));
        list.add(new Symptom(402, "Abdomen", "Abdomen", "borborygmus disappearance", 2, "CMXS", "听诊未闻及肠鸣音"));
        list.add(new Symptom(477, "Abdomen", "Abdomen", "venous engorgement on abdomen", 2, "FLQJ", "腹壁静脉曲张，青筋显露"));
        list.add(new Symptom(478, "Abdomen", "Abdomen", "abdominal distention", 2, "FPL", "腹部膨大隆起"));
        list.add(new Symptom(479, "Abdomen", "Abdomen", "abdominal fullness", 2, "FYM", "腹内满实，扪之坚硬如板状"));
        list.add(new Symptom(480, "Abdomen", "Abdomen", "boat-shaped abdomen", 2, "ZZF", "腹部凹陷如舟状"));
        list.add(new Symptom(621, "Abdomen", "Abdomen", "pain relieves with pressure", 2, "TXAHAZS",
                "疼痛处喜揉按，或按摩而疼痛减轻"));
        list.add(new Symptom(622, "Abdomen", "Abdomen", "pain aggravates with pressure", 2, "TJAHYTS",
                "疼痛处不能按压，或按压时疼痛加重"));
        list.add(new Symptom(623, "Abdomen", "Abdomen", "epigastrium lump", 2, "WFBZK",
                "检查发现剑突下脘腹部有肿块"));
        list.add(new Symptom(624, "Abdomen", "Abdomen", "middle lower abdominal lump", 2, "XFBZK",
                "检查发现脐下小腹部有肿块"));
        list.add(new Symptom(625, "Abdomen", "Abdomen", "bilateral lower abdominal lump", 2, "SFBZK",
                "检查发现脐下左或右侧有肿块"));
        list.add(new Symptom(626, "Abdomen", "Abdomen", "abdominal mass", 2, "FNBK",
                "除外肝、胆、脾肿大和脘腹、小腹、少腹等部位肿块后的腹部肿块"));
        list.add(new Symptom(124, "Abdomen", "Abdomen", "fixed pain", 2, "GDT", "疼痛部位固定不移"));
        list.add(new Symptom(125, "Abdomen", "Abdomen", "tingling pain", 2, "CT", "疼痛呈针刺、鸡啄米样"));
        list.add(new Symptom(127, "Abdomen", "Abdomen", "stuffy pain", 2, "MT", "带有紧闷不舒感的疼痛"));
        list.add(new Symptom(122, "Abdomen", "Abdomen", "distending pain or scurrying pain", 2, "ZTHCT",
                "胸或腹部胀闷而且疼痛，或疼痛部位走串不定"));
        list.add(new Symptom(140, "Abdomen", "Abdomen", "pulling pain", 2, "QCT",
                "痛处有抽掣感，一处牵挛到另一处疼痛。又名引痛、彻痛"));
        list.add(new Symptom(135, "Abdomen", "Abdomen", "pain worsens at night", 2, "YJTS", "夜晚疼痛加重"));
        list.add(new Symptom(337, "Abdomen", "Genitalia", "seminal emission", 0, "YJ", "不因性交而泄出精液"));
        list.add(new Symptom(338, "Abdomen", "Genitalia", "spontaneous seminal emission", 0, "HJ",
                "不因性交、无梦或清醒时精液自行泄出"));
        list.add(new Symptom(339, "Abdomen", "Genitalia", "impotence", 0, "YW",
                "阴茎痿软，勃起不坚，不能进行正常性交"));
        list.add(new Symptom(340, "Abdomen", "Genitalia", "premature ejaculation", 0, "ZX", "性交之始甚至尚未性交，即行排精"));
        list.add(new Symptom(341, "Abdomen", "Genitalia", "decreased sexuality", 0, "XYST",
                "性交的欲望减退或完全没有"));
        list.add(new Symptom(342, "Abdomen", "Genitalia", "priapism", 0, "YQYJ",
                "阴茎时时容易勃起，不易痿软"));
        list.add(new Symptom(343, "Abdomen", "Genitalia", "scanty/ deformed semen", 0, "JYXSJX",
                "泄精量少，或精液检验畸形精子多"));
        list.add(new Symptom(344, "Abdomen", "Genitalia", "clear and cold semen", 0, "JYQL",
                "自觉泄出的精液有清冷感。简称精冷"));
        list.add(new Symptom(347, "Abdomen", "Genitalia", "male infertility", 0, "BY", "女方久不受孕是因男方原因所致"));
        list.add(new Symptom(485, "Abdomen", "Genitalia", "genitalium eczema", 2, "YBSZ", "会阴部有湿性疹子"));
        list.add(new Symptom(486, "Abdomen", "Genitalia", "scrotum or testicular swelling", 0, "YNHGWZ",
                "阴囊肿大或睾丸肿大"));
        list.add(new Symptom(487, "Abdomen", "Genitalia", "genital wet / erosion", 2, "YBCSSL",
                "会阴部出现溃烂流黏液"));
        list.add(new Symptom(121, "Abdomen", "Genitalia", "dysmenorrheal", 1, "TJ",
                "月经来潮前或行经时小腹及腰部疼痛"));
        list.add(new Symptom(316, "Abdomen", "Genitalia", "amenorrhea", 1, "JXBC", "月经流出不畅"));
        list.add(new Symptom(317, "Abdomen", "Genitalia", "advanced menstruation", 1, "YJTQ",
                "上次月经来潮到下次月经来潮，间隔不到21天"));
        list.add(new Symptom(318, "Abdomen", "Genitalia", "delayed menstruation", 1, "YJTC",
                "上次月经来潮到下次月经来潮，间隔超过35天"));
        list.add(new Symptom(319, "Abdomen", "Genitalia", "irregular menstruation", 1, "YJCL",
                "月经有时提前，有时推迟，没有准确时间"));
        list.add(new Symptom(320, "Abdomen", "Genitalia", "hypermenorrhea; menorrhagia", 1, "YJLD", "月经出血量多"));
        list.add(new Symptom(321, "Abdomen", "Genitalia", "hypomenorrhea; scanty menstruation", 1, "YJLS", "月经出血量少"));
        list.add(new Symptom(322, "Abdomen", "Genitalia", "amenorrhea", 1, "JB", "已近2个月没有来月经"));
        list.add(new Symptom(323, "Abdomen", "Genitalia", "vaginal metrostaxis", 1, "YDLXLL",
                "阴道出血淋漓不断"));
        list.add(new Symptom(324, "Abdomen", "Genitalia", "vaginal metrorrhagia", 1, "YDCXRB", "阴道出血量很多"));
        list.add(new Symptom(325, "Abdomen", "Genitalia", "light red and thin menstrual flow", 1, "YJXD", "月经质稀薄、色偏淡"));
        list.add(new Symptom(326, "Abdomen", "Genitalia", "dark red menstrual flow", 1, "YJSH", "月经呈深红色"));
        list.add(new Symptom(327, "Abdomen", "Genitalia", "dark purple menstrual flow", 1, "YJZA", "月经颜色紫暗不泽"));
        list.add(new Symptom(328, "Abdomen", "Genitalia", "menstrual flow with blood clots", 1, "YJJK", "月经中夹有血块"));
        list.add(new Symptom(329, "Abdomen", "Genitalia", "prolonged menstruation; menostaxis", 1, "JQYC", "月经行经期超过7天"));
        list.add(new Symptom(330, "Abdomen", "Genitalia", "intermenstrual bleeding", 1, "JJQCX",
                "约于两次月经的中间阶段，阴道有出血现象"));
        list.add(new Symptom(332, "Abdomen", "Genitalia", "excessive and watery vaginal discharge", 1, "DXDEX",
                "阴道内流出多量清稀的液体"));
        list.add(new Symptom(333, "Abdomen", "Genitalia", "excessive and vicious vaginal discharge", 1, "DXDEN",
                "阴道内流出多量黏稠的液体"));
        list.add(new Symptom(334, "Abdomen", "Genitalia", "yellowish, stinking vaginal discharge", 1, "DXSHQC",
                "阴道内流出色黄而气臭的液体"));
        list.add(new Symptom(335, "Abdomen", "Genitalia", "vaginal discharge mingled with blood", 1, "DXJX",
                "阴道内流出的液体中夹有血液"));
        list.add(new Symptom(336, "Abdomen", "Genitalia", "white, stinking vaginal discharge", 1, "DXSBQX",
                "阴道内流出色白而有腥气的液体"));
        list.add(new Symptom(346, "Abdomen", "Genitalia", "female infertility", 1, "BY2",
                "育龄期女子有性生活、未避孕而2年未受孕"));
        list.add(new Symptom(348, "Abdomen", "Genitalia", "habitual abortion", 1, "HTDT",
                "妊娠12周内胚胎自然殒堕，为堕胎。妊娠12-28周胎儿成形自然殒堕为小产，堕胎或小产连续发生3次或3次以上为滑胎"));
        list.add(new Symptom(363, "Abdomen", "Genitalia", "dark red bleeding", 2, "CXSSH", "所出之血呈深红色"));
        list.add(new Symptom(368, "Abdomen", "Genitalia", "vaginal bleeding", 1, "YDLX", "泛指阴道出血"));
        list.add(new Symptom(369, "Abdomen", "Genitalia", "lochia retention", 1, "ELBXHBC",
                "分娩后阴道流出的血和浊液极少，或流出不畅"));
        list.add(new Symptom(481, "Abdomen", "Genitalia", "hysteroptosis", 1, "ZGXC",
                "阴道壁甚至子宫脱出于阴道口"));
        list.add(new Symptom(1004, "Abdomen", "Genitalia", "uterine fibroid /hysteromyoma", 1, "zgjl",
                "1、月经改变 腹部包块 腹痛腰酸 盆腔压迫等症；2、2 次以上盆腔 B 超诊断为子宫肌瘤；3、妇科双合诊检查发现子宫增大 质硬不平 可触及结节或肿块等"));
        list.add(new Symptom(107, "Abdomen", "Genital Organ", "genital pain", 2, "YQT",
                "自觉阴茎、阴囊内或女子阴户疼痛"));
        list.add(new Symptom(190, "Abdomen", "Genital Organ", "vaginal pruritus", 2, "YBSY", "会阴部瘙痒的感觉"));
        list.add(new Symptom(488, "Abdomen", "Genital Organ", "vulvae dryness", 2, "WYGZ",
                "外阴部皮肤干燥而枯涩不润"));
        list.add(new Symptom(489, "Abdomen", "Genital Organ", "genital contraction", 2, "YQSS",
                "阴茎、睾丸、阴囊或阴阜等阴器向内收缩"));
        list.add(new Symptom(490, "Abdomen", "Genital Organ", "pudendal sagging and distention", 2, "YBZZ",
                "会阴部常有重坠下沉，胀满不舒"));
        list.add(new Symptom(297, "Abdomen", "Urination", "new contraction frequent urination", 2, "XBNP",
                "新近出现小便次数频繁，时时欲解小便"));
        list.add(new Symptom(298, "Abdomen", "Urination", "long-term frequent urination", 2, "CQNP",
                "经常小便次数频繁，时时欲解小便"));
        list.add(new Symptom(299, "Abdomen", "Urination", "weakness in urination", 2, "PNWL", "自觉将小便排出的力量不够"));
        list.add(new Symptom(300, "Abdomen", "Urination", "frequent urination at night", 2, "YND", "夜间小便次数增多"));
        list.add(new Symptom(301, "Abdomen", "Urination", "short and yellow urine", 2, "NDH", "小便色比正常黄，尿量减少"));
        list.add(new Symptom(302, "Abdomen", "Urination", "clear and profuse urine", 2, "NQC", "小便量多色清"));
        list.add(new Symptom(303, "Abdomen", "Urination", "urinary retention", 2, "NCL",
                "小腹（膀胱）部膨胀（有尿）而小便不能排出"));
        list.add(new Symptom(304, "Abdomen", "Urination", "scanty urine", 2, "NS", "尿量减少（身体不肿）"));
        list.add(new Symptom(305, "Abdomen", "Urination", "burning pain in urination", 2, "PNZR", "排尿时尿道有灼热感"));
        list.add(new Symptom(306, "Abdomen", "Urination", "difficulty and pain in urination", 2, "PNST", "小便排出不畅而有疼痛感"));
        list.add(new Symptom(307, "Abdomen", "Urination", "new contraction dribbling urination", 2, "XQXBLL",
                "新起的排尿点滴不尽"));
        list.add(new Symptom(308, "Abdomen", "Urination", "dark yellow urine", 2, "NHH", "尿深黄呈褐色如浆油"));
        list.add(new Symptom(309, "Abdomen", "Urination", "profuse urine", 2, "XBTD", "小便量特别多"));
        list.add(new Symptom(310, "Abdomen", "Urination", "enuresis", 2, "YN", "睡眠中不自主地排尿"));
        list.add(new Symptom(311, "Abdomen", "Urination", "dribble of urine", 2, "YNBJ", "小便完时仍有余尿滴沥不尽"));
        list.add(new Symptom(312, "Abdomen", "Urination", "urinary incontinence", 2, "XBSJ",
                "小便不能随意控制而自行尿出"));
        list.add(new Symptom(313, "Abdomen", "Urination", "turbid urine", 2, "XBHZRZG",
                "小便浑浊不清"));
        list.add(new Symptom(314, "Abdomen", "Urination", "urolithiasis", 2, "NLSS", "有砂石随小便排出"));
        list.add(new Symptom(315, "Abdomen", "Urination", "dribble of turbid urine", 2, "NHDZY", "小便后滴白色浑浊液体"));
        list.add(new Symptom(356, "Abdomen", "Urination", "hematuria", 2, "NX", "血随小便排出而尿呈红色"));

        //四肢部
        list.add(new Symptom(48, "Limbs", "Joint", "cold in joints", 2, "GJL", "某些关节凉、怕冷"));
        list.add(new Symptom(117, "Limbs", "Joint", "joint (bone) pain", 2, "GGJT", "泛指关节、骨骼疼痛"));
        list.add(new Symptom(403, "Limbs", "Joint", "rattle in joints", 2, "GJNZX", "活动时关节内作响"));
        list.add(new Symptom(529, "Limbs", "Joint", "joint stiffness", 2, "GJCJJY",
                "晨起时关节僵硬，不能活动"));
        list.add(new Symptom(530, "Limbs", "Joint", "red joints", 2, "GJH", "关节部位颜色红"));
        list.add(new Symptom(531, "Limbs", "Joint", "joint swelling", 2, "GJZ", "关节部位肿起"));
        list.add(new Symptom(532, "Limbs", "Joint", "joint / skeletal deformity", 2, "GJGGJX", "关节出现畸形"));
        list.add(new Symptom(115, "Limbs", "Joint", "keen pain", 2, "XT2", "自觉膝关节疼痛"));
        list.add(new Symptom(523, "Limbs", "Joint", "inhibited joint and body movement", 2, "GJZTHDBL",
                "关节或肢体活动不灵活，甚至不能活动。又名肢体僵硬"));
        list.add(new Symptom(587, "Limbs", "Pulse", "floating pulse", 2, "MF", "脉位浅表，轻手按之即得，重按则减"));
        list.add(new Symptom(588, "Limbs", "Pulse", "deep pulse", 2, "MC1",
                "脉位较深，轻手不应，重手按之明显"));
        list.add(new Symptom(590, "Limbs", "Pulse", "firm pulse", 2, "ML",
                "脉沉取使得，搏动有力，势大形长，脉道较硬"));
        list.add(new Symptom(591, "Limbs", "Pulse", "slow pulse", 2, "MC2", "脉动慢，每分钟不满60次"));
        list.add(new Symptom(592, "Limbs", "Pulse", "rapid pulse", 2, "MS1", "脉动快，每分钟超过90次"));
        list.add(new Symptom(593, "Limbs", "Pulse", "swift pulse", 2, "MJ", "脉动快，每分钟超过120次"));
        list.add(new Symptom(595, "Limbs", "Pulse", "thready pulse", 2, "MX1", "脉体细小，应指明显"));
        list.add(new Symptom(597, "Limbs", "Pulse", "replete pulse", 2, "MS2", "脉搏跳动有力"));
        list.add(new Symptom(598, "Limbs", "Pulse", "feeble pulse", 2, "MX2", "脉搏跳动无力"));
        list.add(new Symptom(601, "Limbs", "Pulse", "faint pulse", 2, "MW", "脉体极小，搏动极无力，似有似无"));
        list.add(new Symptom(602, "Limbs", "Pulse", "moderate pulse", 2, "MH2",
                "脉动不快不徐而和缓，或略显力量不足、稍微偏慢"));
        list.add(new Symptom(603, "Limbs", "Pulse", "wiry pulse", 2, "MX", "脉体较硬而欠和缓，如按琴弦"));
        list.add(new Symptom(604, "Limbs", "Pulse", "tight pulse", 2, "MJ1",
                "脉体不大，但脉动有力而有弹指之感"));
        list.add(new Symptom(605, "Limbs", "Pulse", "slippery pulse", 2, "MH1", "脉动应指有流利园滑之感"));
        list.add(new Symptom(606, "Limbs", "Pulse", "unsmooth pulse", 2, "MS", "脉动应指有滞涩不畅之感"));
        list.add(new Symptom(607, "Limbs", "Pulse", "soggy pulse", 2, "MR1", "脉浮细无力"));
        list.add(new Symptom(608, "Limbs", "Pulse", "irregular-rapid pulse", 2, "MC3", "脉动快而有歇止，止无定数"));
        list.add(new Symptom(609, "Limbs", "Pulse", "irregularly intermittent pulse", 2, "MJ2", "脉动慢而有歇止，止无定数"));
        list.add(new Symptom(610, "Limbs", "Pulse", "regularly intermittent pulse", 2, "MD1", "脉动慢而有歇止，止有定数"));
        list.add(new Symptom(611, "Limbs", "Pulse", "weak pulse at cun position", 2, "CMR",
                "尺部脉跳动欠明显，较寸、关部力量偏弱"));
        list.add(new Symptom(44, "Limbs", "Limbs", "cold in four limbs", 2, "SZL",
                "手脚凉。简称肢凉，又名手足厥冷、手足逆冷"));
        list.add(new Symptom(46, "Limbs", "Limbs", "cold in lower limbs", 2, "XZLS", "下肢特别冷"));
        list.add(new Symptom(119, "Limbs", "Limbs", "four limbs or body pain", 2, "SZHZTT",
                "泛指四肢的疼痛，未能明确疼痛是在筋骨或肌肉者"));
        list.add(new Symptom(139, "Limbs", "Limbs", "tendon spasm and pulling pain", 2, "ZJLT",
                "手、小腿等处筋肉拘急僵硬而痛"));
        list.add(new Symptom(186, "Limbs", "Limbs", "numbness of skin and body", 2, "ZTJFMM",
                "皮肤甚或肌肉发麻，感觉不灵"));
        list.add(new Symptom(519, "Limbs", "Limbs", "convulsion", 2, "ZTCC",
                "不自主地四肢挛急与弛张间作，伸缩交替，动作有力。简称抽搐、抽痉"));
        list.add(new Symptom(520, "Limbs", "Limbs", "numb limbs", 2, "SZMM",
                "手足部皮肤或肌肉发麻，感觉不灵"));
        list.add(new Symptom(521, "Limbs", "Limbs", "convulsion", 2, "QZ",
                "手指或下肢不自主地一伸一缩的轻微运动，动作缓慢无力。又名手足蠕动"));
        list.add(new Symptom(524, "Limbs", "Limbs", "limb spasm", 2, "ZTJJ",
                "手足或腰背等处筋肉拘挛不舒，收紧而难以屈伸"));
        list.add(new Symptom(525, "Limbs", "Limbs", "paralysis", 2, "ZTTH",
                "肢体瘫废，不能随意活动，主指截瘫"));
        list.add(new Symptom(527, "Limbs", "Limbs", "flaccid limbs", 2, "ZTWR",
                "肢体痿软、松弛无力，运动功能减退"));
        list.add(new Symptom(528, "Limbs", "Limbs", "muscle atrophy", 2, "JRWS", "肌肉消瘦、萎缩"));
        list.add(new Symptom(534, "Limbs", "Limbs", "limb hematoma", 2, "ZTXZ",
                "肢体出现红色或暗红色高起的血性肿块"));
        list.add(new Symptom(537, "Limbs", "Limbs", "varicose vein in low limbs", 2, "XZMLQZ",
                "下肢静脉充盈曲张，青筋显露"));
        list.add(new Symptom(618, "Limbs", "Limbs", "nodule or lump", 2, "JJHZK", "肢体出现结节或肿块"));
        list.add(new Symptom(133, "Limbs", "Limbs", "pain worsens in cloudy and rainy days", 2, "YYTTTJZ",
                "遇阴雨气候而疼痛加重"));
        list.add(new Symptom(130, "Limbs", "Limbs", "cold pain", 2, "LT", "疼痛且有冷凉感而喜暖"));
        list.add(new Symptom(136, "Limbs", "Limbs", "pain relieves in activity or aggravates", 2, "HDTHBDTS",
                "适当活动后疼痛缓解，没有活动时疼痛加重。"));
        list.add(new Symptom(140, "Limbs", "Limbs", "pulling pain", 2, "QCT",
                "痛处有抽掣感，一处牵挛到另一处疼痛。又名引痛、彻痛"));
        list.add(new Symptom(131, "Limbs", "Limbs", "aching and heavy pain", 2, "SZT1", "带有酸软、沉重感的疼痛"));
        list.add(new Symptom(135, "Limbs", "Limbs", "pain worsens at night", 2, "YJTS", "夜晚疼痛加重"));
        list.add(new Symptom(109, "Limbs", "Limbs", "muscular pain", 2, "JRTT", "自觉疼痛部位在肌肉内"));
        list.add(new Symptom(116, "Limbs", "Finger/Toe/Palm", "heel pain", 2, "ZGT",
                "自觉足跟部疼痛，着地负重时疼痛明显"));
        list.add(new Symptom(118, "Limbs", "Finger/Toe/Palm", "fingers or toes pain", 2, "ZHZGJT",
                "自觉手指或足趾关节疼痛"));
        list.add(new Symptom(502, "Limbs", "Finger/Toe/Palm", "palmar erythema", 2, "GZ",
                "手掌大小鱼际及手指充血、潮红"));
        list.add(new Symptom(504, "Limbs", "Finger/Toe/Palm", "cyanoderma in fingers", 2, "ZDQZ",
                "手指或足趾颜色变暗呈青紫色"));
        list.add(new Symptom(505, "Limbs", "Finger/Toe/Palm", "pale nails", 2, "ZJDB",
                "指甲颜色浅淡无血色"));
        list.add(new Symptom(517, "Limbs", "Finger/Toe/Palm", "fisted  hands", 2, "LSWG", "两手紧握不张开"));

        //背部
        list.add(new Symptom(111, "Back", "Back", "back pain", 2, "BT1", "自觉背部疼痛"));
        list.add(new Symptom(112, "Back", "Back", "shoulder pain", 2, "JT2", "自觉肩部疼痛"));
        list.add(new Symptom(133, "Back", "Back", "pain worsens in cloudy and rainy days", 2, "YYTTTJZ",
                "遇阴雨气候而疼痛加重"));
        list.add(new Symptom(185, "Back", "Back", "spasm of nape and back", 2, "XBJJ",
                "项部和背部有牵强板滞、活动不灵的不适感"));
        list.add(new Symptom(192, "Back", "Back", "distention in the back", 2, "BZ", "背部有作胀的感觉"));
        list.add(new Symptom(135, "Back", "Back", "pain worsens at night", 2, "YJTS", "夜晚疼痛加重"));
        list.add(new Symptom(140, "Back", "Back", "pulling pain", 2, "QCT",
                "痛处有抽掣感，一处牵挛到另一处疼痛。又名引痛、彻痛"));
        list.add(new Symptom(136, "Back", "Back", "pain relieves in activity or aggravates", 2, "HDTHBDTS",
                "适当活动后疼痛缓解，没有活动时疼痛加重。"));
        list.add(new Symptom(109, "Back", "Back", "muscular pain", 2, "JRTT", "自觉疼痛部位在肌肉内"));
        list.add(new Symptom(130, "Back", "Back", "cold pain", 2, "LT", "疼痛且有冷凉感而喜暖"));
        list.add(new Symptom(131, "Back", "Back", "aching and heavy pain", 2, "SZT1", "带有酸软、沉重感的疼痛"));

        //腰股部
        list.add(new Symptom(264, "Waist and Thigh", "Bowel", "new contraction diarrhea", 2, "XQFX",
                "新起大便次数增多，便质稀薄不成形"));
        list.add(new Symptom(265, "Waist and Thigh", "Bowel", "frequent diarrhea", 2, "JCFX",
                "经常大便次数增多，便质稀薄不成形"));
        list.add(new Symptom(266, "Waist and Thigh", "Bowel", "predawn diarrhea", 2, "WGFX",
                "约于黎明时腹痛、肠鸣，欲解大便，且大便稀薄。又名黎明泄"));
        list.add(new Symptom(267, "Waist and Thigh", "Bowel", "new contraction constipation", 2, "XBBM",
                "新起排便时间延长，便次减少，便质干燥"));
        list.add(new Symptom(268, "Waist and Thigh", "Bowel", "frequent constipation", 2, "JCBM",
                "经常排便时间延长，便次减少，便质干燥"));
        list.add(new Symptom(269, "Waist and Thigh", "Bowel", "dry bowel movement", 2, "DBGJ",
                "大便水分减少而干燥硬结，不易排出"));
        list.add(new Symptom(270, "Waist and Thigh", "Bowel", "frequent loose bowel movements", 2, "JCBT",
                "经常大便稀软不成形，如溏状。简称便溏"));
        list.add(new Symptom(271, "Waist and Thigh", "Bowel", "new contraction diarrhea", 2, "XQBX", "新起大便稀薄、水分多"));
        list.add(new Symptom(272, "Waist and Thigh", "Bowel", "watery bowel/ rice water-like bowel", 2, "DBRSYMGSY",
                "大便清稀如水。又名水泻"));
        list.add(new Symptom(273, "Waist and Thigh", "Bowel", "egg soup-like bowel", 2, "DBRDT",
                "大便稀水，夹有粪片，水粪杂下，如蛋汤、鸭粪"));
        list.add(new Symptom(274, "Waist and Thigh", "Bowel", "millet-like bowel", 2, "DBRHM",
                "大便色黄质稀，不成形而腐臭，如小米粥样"));
        list.add(new Symptom(275, "Waist and Thigh", "Bowel", "bowel with mucus", 2, "DBYNY", "大便夹有黏液"));
        list.add(new Symptom(276, "Waist and Thigh", "Bowel", "bowel with pus /blood/ fish brain like", 2, "DBYNXRYN",
                "大便夹有脓血"));
        list.add(new Symptom(277, "Waist and Thigh", "Bowel", "dry bowel movement which turned loose", 2, "DBXGHX",
                "先排出的大便尚干而成形，后排出的大便则稀而不成形"));
        list.add(new Symptom(278, "Waist and Thigh", "Bowel", "erratic loose and dry bowel movements", 2, "DBSTSJ",
                "大便有时干硬燥结，有时稀烂如溏状。又名溏结不调"));
        list.add(new Symptom(279, "Waist and Thigh", "Bowel", "grey bowel", 2, "DBSHB",
                "大便颜色灰白不带黄色"));
        list.add(new Symptom(280, "Waist and Thigh", "Bowel", "bowel as black  as tar", 2, "DBHRBY",
                "大便质稀色黑而有光亮，如柏油状"));
        list.add(new Symptom(281, "Waist and Thigh", "Bowel", "stinking, rot and foul bowel", 2, "DBXFCQ",
                "大便气腥腐臭秽难闻"));
        list.add(new Symptom(282, "Waist and Thigh", "Bowel", "undigested food in bowel", 2, "WGBH",
                "大便中有多量未消化的食物。又名下利清谷"));
        list.add(new Symptom(283, "Waist and Thigh", "Bowel", "thin and flat bowel", 2, "DBXB", "大便排出变小，或呈扁形"));
        list.add(new Symptom(284, "Waist and Thigh", "Bowel", "parasite elimination in defecation", 2, "DBPC", "蛔虫随大便排出"));
        list.add(new Symptom(285, "Waist and Thigh", "Bowel", "urgent diarrhea", 2, "XSJP",
                "泻势很急迫，欲解大便就马上泻出。又名泻下如注"));
        list.add(new Symptom(286, "Waist and Thigh", "Bowel", "weakness in defecation", 2, "PBWL",
                "自觉将大便排出的力量不够"));
        list.add(new Symptom(287, "Waist and Thigh", "Bowel", "incomplete bowel movements", 2, "PBBS",
                "排便不通畅，欲解不解，便意未尽，乏舒畅感"));
        list.add(new Symptom(291, "Waist and Thigh", "Bowel", "tenesmus", 2, "LJHZ",
                "腹内疼痛，窘迫欲便，肛门重坠，刚欲泻出则突然肛缩，而便出不爽"));
        list.add(new Symptom(292, "Waist and Thigh", "Bowel", "bowel incontinence", 2, "DBSJ",
                "大便不能随意控制而自行排出"));
        list.add(new Symptom(293, "Waist and Thigh", "Bowel", "abdominal pain about to diarrhea", 2, "FTYX",
                "腹部时有疼痛，痛则欲排大便，大便质稀"));
        list.add(new Symptom(357, "Waist and Thigh", "Bowel", "bloody bowel", 2, "BX",
                "血从肛门排出，可为大便前、后有血滴出，或大便表面、大便中见有血液"));
        list.add(new Symptom(108, "Waist and Thigh", "Anus", "proctagra", 2, "GMT", "自觉肛门部位疼痛"));
        list.add(new Symptom(288, "Waist and Thigh", "Anus", "anal burning sensation", 2, "GMZR", "肛门部有灼热的感觉"));
        list.add(new Symptom(289, "Waist and Thigh", "Anus", "anal sagging distention", 2, "GMZZ",
                "肛门部时常有重坠下沉、要排大便的感觉"));
        list.add(new Symptom(294, "Waist and Thigh", "Anus", "frequent flatus", 2, "SQD",
                "肠道内气体从肛门排出，频频发出声响"));
        list.add(new Symptom(296, "Waist and Thigh", "Anus", "stinking flatus", 2, "SQSC",
                "经肛门排出的气体，其气特别臭，有的臭如败卵"));
        list.add(new Symptom(482, "Waist and Thigh", "Anus", "prolapse of rectum", 2, "TG", "肛管甚至直肠脱出于肛门外"));
        list.add(new Symptom(484, "Waist and Thigh", "Anus", "hemorrhoids", 2, "ZC",
                "肛门齿线处有肉（血管球或皮瓣等）突起"));
        list.add(new Symptom(109, "Waist and Thigh", "Waist", "muscular pain", 2, "JRTT", "自觉疼痛部位在肌肉内"));
        list.add(new Symptom(113, "Waist and Thigh", "Waist", "lumbago; lumbar pain", 2, "YT",
                "自觉疼痛部位在腰部两边或腰左右侧处"));
        list.add(new Symptom(114, "Waist and Thigh", "Waist", "pain along spinal column", 2, "YJT",
                "自觉疼痛部位在腰部偏中间脊椎处"));
        list.add(new Symptom(120, "Waist and Thigh", "Waist", "referred foot pain due to lumber pain", 2, "YTLZ", "自觉腰痛牵掣到下肢疼痛"));
        list.add(new Symptom(140, "Waist and Thigh", "Waist", "pulling pain", 2, "QCT",
                "痛处有抽掣感，一处牵挛到另一处疼痛。又名引痛、彻痛"));
        list.add(new Symptom(184, "Waist and Thigh", "Waist", "sore and weak waist and knees", 2, "YXSR", "腰部、膝部酸软无力的感觉"));
        list.add(new Symptom(130, "Waist and Thigh", "Waist", "cold pain", 2, "LT", "疼痛且有冷凉感而喜暖"));
        list.add(new Symptom(131, "Waist and Thigh", "Waist", "aching and heavy pain", 2, "SZT1", "带有酸软、沉重感的疼痛"));
        list.add(new Symptom(133, "Waist and Thigh", "Waist", "pain worsens in cloudy and rainy days", 2, "YYTTTJZ",
                "遇阴雨气候而疼痛加重"));
        list.add(new Symptom(136, "Waist and Thigh", "Waist", "pain relieves in activity or aggravates", 2, "HDTHBDTS",
                "适当活动后疼痛缓解，没有活动时疼痛加重。"));
        list.add(new Symptom(135, "Waist and Thigh", "Waist", "pain worsens at night", 2, "YJTS", "夜晚疼痛加重"));

        //病因
        list.add(new Symptom(12, "Cause", "Cause", "indulgent to fatty, sweet diet", 2, "SSFGHW", "喜食高脂、高糖食品"));
        list.add(new Symptom(13, "Cause", "Cause", "newly affected by wind-cold", 2, "XJGSFH", "新病与感受风寒有关"));
        list.add(new Symptom(14, "Cause", "Cause", "affected by summer-heat", 2, "GSSRHX",
                "起病与感受火热或暑热有关"));
        list.add(new Symptom(15, "Cause", "Cause", "damp environment", 2, "HJCS",
                "发病与季节、气候环境潮湿有关"));
        list.add(new Symptom(16, "Cause", "Cause", "dry environment", 2, "HJGZ",
                "发病与季节、气候环境干燥有关"));
        list.add(new Symptom(17, "Cause", "Cause", "exposure to rain and water", 2, "LYXS", "起病与淋雨、下水等有关"));
        list.add(new Symptom(18, "Cause", "Cause", "Careless diet", 2, "YSBS",
                "发病与饮食不慎、不洁、过饱等有关"));
        list.add(new Symptom(19, "Cause", "Cause", "disease worsens after exertion", 2, "HDHLLBZ",
                "病情常因活动或劳累而加重"));
        list.add(new Symptom(20, "Cause", "Cause", "emotion-related illness", 2, "BQYQZYG",
                "病情因情志刺激而发，或病情轻重与情绪明显相关"));
        list.add(new Symptom(21, "Cause", "Cause", "trauma caused", 2, "WSSZ", "病情与外伤有明显关系"));
        list.add(new Symptom(22, "Cause", "Cause", "new birth, abortion and surgery", 1, "XCLCSS",
                "新近作过手术，或生育、流产未超过2月，或病情与之明显相关"));
        list.add(new Symptom(24, "Cause", "Cause", "massive or persistent bleeding", 2, "DLHCXCX",
                "当前病变与曾经大出血有关，或有慢性持续出血的病史"));
        list.add(new Symptom(34, "Cause", "Cause", "fever induced by summer-heat", 2, "GSSREFR",
                "在暑季感受了暑热而自觉发热"));
        list.add(new Symptom(1002, "Cause", "Cause", "hyperlipemia", 2, "gxz",
                "1、胆固醇高2、甘油三酯高3、血黏度高4、低密度脂蛋白高"));
        list.add(new Symptom(1003, "Cause", "Cause", "fatty liver", 2, "zfg",
                "1、肝区胀痛、腹胀、恶心、食欲不振、嗳气2、B超提示：脂肪肝"));
        list.add(new Symptom(1005, "Cause", "Cause", "high uric acid", 2, "gns", "尿酸高"));

        return list;
    }
}
