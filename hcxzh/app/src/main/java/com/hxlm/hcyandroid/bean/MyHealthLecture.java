package com.hxlm.hcyandroid.bean;

import java.util.List;

/**
 * 我预约的讲座，在个人中心页面的讲座页面中使用
 *
 * @author Administrator
 */
public class MyHealthLecture {
    private int id;
    private long createDate;
    private long modifyDate;
    private String status;// 预约状态
    private double actualPayment;// 实际支付金额
    private HealthLecture lecture;// 讲座对象
    private int reserveNums;// 预约数量
    private List<ReserveSn> reserveSns;
    private OrderItem orderItem;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public OrderItem getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(OrderItem orderItem) {
        this.orderItem = orderItem;
    }

    public int getReserveNums() {
        return reserveNums;
    }

    public void setReserveNums(int reserveNums) {
        this.reserveNums = reserveNums;
    }

    public List<ReserveSn> getReserveSns() {
        return reserveSns;
    }

    public void setReserveSns(List<ReserveSn> reserveSns) {
        this.reserveSns = reserveSns;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(long modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getActualPayment() {
        return actualPayment;
    }

    public void setActualPayment(double actuaPayment) {
        this.actualPayment = actuaPayment;
    }

    public HealthLecture getLecture() {
        return lecture;
    }

    public void setLecture(HealthLecture lecture) {
        this.lecture = lecture;
    }

}
