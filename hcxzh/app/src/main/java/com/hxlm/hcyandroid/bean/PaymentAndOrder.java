package com.hxlm.hcyandroid.bean;

public class PaymentAndOrder {
    private Payment payment;
    private OrderItem order;

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public OrderItem getOrder() {
        return order;
    }

    public void setOrder(OrderItem order) {
        this.order = order;
    }

}
