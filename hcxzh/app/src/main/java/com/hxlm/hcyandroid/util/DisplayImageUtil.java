package com.hxlm.hcyandroid.util;

import android.graphics.Bitmap;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class DisplayImageUtil {

    public static DisplayImageOptions options;

    public static void setDisplayImage() {


        options = new DisplayImageOptions.Builder()
                //.showImageOnLoading(R.drawable.reservation_icon_bg)            //加载图片时的图片
                // .showImageForEmptyUri(R.drawable.reservation_icon_bg)         //没有图片资源时的默认图片
                //.showImageOnFail(R.drawable.reservation_icon_bg)              //加载失败时的图片
                .cacheInMemory(true)                               //启用内存缓存
                .cacheOnDisk(true)                                 //启用外存缓存
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)               //因为默认是ARGB_8888， 使用RGB_565会比使用ARGB_8888少消耗2倍的内存
                .considerExifParams(true)                          //启用EXIF和JPEG图像格式
                .displayer(new RoundedBitmapDisplayer(0))         //设置显示风格这里是圆角矩形
                .build();
        /**
         * 图片缩放
         *  EXACTLY :图像将完全按比例缩小的目标大小

         EXACTLY_STRETCHED:图片会缩放到目标大小完全

         IN_SAMPLE_INT:图像将被二次采样的整数倍

         IN_SAMPLE_POWER_OF_2:图片将降低2倍，直到下一减少步骤，使图像更小的目标大小

         NONE:图片不会调整
         */
    }

}
