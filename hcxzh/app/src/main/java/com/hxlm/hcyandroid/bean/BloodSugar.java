package com.hxlm.hcyandroid.bean;

public class BloodSugar {
    private int id;
    private long createDate;
    private long modifyData;
    private String type;
    private double levels;
    private boolean isAbnormity;

    public BloodSugar() {
        super();
        // TODO Auto-generated constructor stub
    }

    public BloodSugar(int id, long createDate, long modifyData, String type,
                      double levels, boolean isAbnormity) {
        super();
        this.id = id;
        this.createDate = createDate;
        this.modifyData = modifyData;
        this.type = type;
        this.levels = levels;
        this.isAbnormity = isAbnormity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getModifyData() {
        return modifyData;
    }

    public void setModifyData(long modifyData) {
        this.modifyData = modifyData;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getLevels() {
        return levels;
    }

    public void setLevels(double levels) {
        this.levels = levels;
    }

    public boolean isAbnormity() {
        return isAbnormity;
    }

    public void setAbnormity(boolean isAbnormity) {
        this.isAbnormity = isAbnormity;
    }

    @Override
    public String toString() {
        return "BloodSugar [id=" + id + ", createDate=" + createDate
                + ", modifyData=" + modifyData + ", type=" + type + ", levels="
                + levels + ", isAbnormity=" + isAbnormity + "]";
    }

}
