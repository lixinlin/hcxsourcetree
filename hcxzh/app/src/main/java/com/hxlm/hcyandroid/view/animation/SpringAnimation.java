package com.hxlm.hcyandroid.view.animation;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.TranslateAnimation;
import com.hxlm.hcyandroid.view.controlview.ImageButtonExtend;

/**
 * 分布菜单加载和伸缩动画
 * @Description: 分布菜单加载和伸缩动画

 * @File: SpringAnimation.java

 * @Package com.spring.menu.animation

 * @Author Hanyonglu

 * @Date 2012-10-25 下午12:18:39

 * @Version V1.0
 */
public class SpringAnimation extends ZoomAnimation {
	private static int[] size;
	private static int xOffset = 210;
	private static int yOffset = -15;
	public static final int	DURATION = 300;
	
	/**
	 * 构造器
	 * @param direction
	 * @param duration
	 * @param view
	 */
	public SpringAnimation(Direction direction, long duration, View view) {
		super(direction, duration, new View[] { view });
		//SpringAnimation.xOffset = SpringAnimation.size[0] / 2 - 30;//宽度除以2-30
		SpringAnimation.xOffset = SpringAnimation.size[0] / 2-56;//宽度除以2-30
	}

	/**
	 * 开始显示动画效果
	 * @param viewgroup
	 * @param direction
	 * @param size
	 */
	public static void startAnimations(ViewGroup viewgroup,
			Direction direction, int[] size) {
		SpringAnimation.size = size;
		
		switch (direction) {
		case HIDE:
			startShrinkAnimations(viewgroup);
			break;
		case SHOW:
			startEnlargeAnimations(viewgroup);
			break;
		}
	}

	/**
	 * 开始呈现菜单
	 * @param viewgroup
	 */
	private static void startEnlargeAnimations(ViewGroup viewgroup) {
		for (int i = 0; i < viewgroup.getChildCount(); i++) {
			if (viewgroup.getChildAt(i) instanceof ImageButtonExtend) {
				ImageButtonExtend inoutimagebutton = (ImageButtonExtend) viewgroup
						.getChildAt(i);
				SpringAnimation animation = new SpringAnimation(
						Direction.HIDE, DURATION, inoutimagebutton);
//				animation.setStartOffset((i * 200)
//						/ (-1 + viewgroup.getChildCount()));
				/**
				 * 可以通过设置Animation的startOffset来控制Animation的运行顺序——同时或按顺序运行动画。
				 * 默认情况下，Animation是同时开始的，但可以通过设置startOffset属性来指定动画在*ms后开始运行。
				 */
				animation.setStartOffset((i * 200)
						/ (-1+ viewgroup.getChildCount()));
				/**
				 * 设置动画的同时并利用动画中的插入器（interpolator）来实现弹力。
				 * OvershootInterpolator：表示向前甩一定值后再回到原来位置。
				 AnticipateOvershootInterpolator：表示开始的时候向后然后向前甩一定值后返回最后的值。

				 当然还有其它的插入器，简要了解下其作用：

				 AnticipateInterpolator：表示开始的时候向后然后向前甩。
				 BounceInterpolator：表示动画结束的时候弹起。
				 OvershootInterpolator：表示向前甩一定值后再回到原来位置。
				 CycleInterpolator：表示动画循环播放特定的次数，速率改变沿着正弦曲线。
				 DecelerateInterpolator：表示在动画开始的地方快然后慢。
				 LinearInterpolator：表示以常量速率改变。


				 */
				animation.setInterpolator(new OvershootInterpolator(4F));
				inoutimagebutton.startAnimation(animation);
			}
		}
	}

	/**
	 * 开始隐藏菜单
	 * @param viewgroup
	 */
	private static void startShrinkAnimations(ViewGroup viewgroup) {
		for (int i = 0; i < viewgroup.getChildCount(); i++) {
			if (viewgroup.getChildAt(i) instanceof ImageButtonExtend) {
				ImageButtonExtend inoutimagebutton = (ImageButtonExtend) viewgroup
						.getChildAt(i);
				SpringAnimation animation = new SpringAnimation(
						Direction.SHOW, DURATION,
						inoutimagebutton);
//				animation.setStartOffset((100 * ((-1 + viewgroup
//						.getChildCount()) - i))
//						/ (-1 + viewgroup.getChildCount()));
				animation.setStartOffset((100 * ((-1 + viewgroup
						.getChildCount()) - i))
						/ (-1 + viewgroup.getChildCount()));
				//animation.setInterpolator(new AnticipateOvershootInterpolator(2F));
				animation.setInterpolator(new LinearInterpolator());
				inoutimagebutton.startAnimation(animation);
			}
		}
	}

	//隐藏动画类 移动位置
	@Override
	protected void addShrinkAnimation(View[] views) {
		// TODO Auto-generated method stub
		MarginLayoutParams mlp = (MarginLayoutParams) views[0].
				getLayoutParams();
		addAnimation(new TranslateAnimation(
				xOffset + -mlp.leftMargin, 
				0F,yOffset + mlp.bottomMargin, 0F));
	}

	//显示动画类 移动位置
	@Override
	protected void addEnlargeAnimation(View[] views) {
		// TODO Auto-generated method stub
		MarginLayoutParams mlp = (MarginLayoutParams) views[0].
				getLayoutParams();
		addAnimation(new TranslateAnimation(
				0F, xOffset + -mlp.leftMargin, 
				0F,yOffset + mlp.bottomMargin));
	}
}
