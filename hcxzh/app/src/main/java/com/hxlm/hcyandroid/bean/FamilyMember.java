package com.hxlm.hcyandroid.bean;

import java.io.Serializable;
import java.util.List;

public class FamilyMember implements Serializable {
    private long createDate;
    private int id;
    private boolean isLocked;
    private long loginDate;
    private String memberImage;
    private long modifyDate;
    private String username;
    private List<ChildMember> mengberchild;

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isLocked() {
        return isLocked;
    }

    public void setLocked(boolean isLocked) {
        this.isLocked = isLocked;
    }

    public long getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(long loginDate) {
        this.loginDate = loginDate;
    }

    public String getMemberImage() {
        return memberImage;
    }

    public void setMemberImage(String memberImage) {
        this.memberImage = memberImage;
    }

    public long getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(long modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<ChildMember> getMengberchild() {
        return mengberchild;
    }

    public void setMengberchild(List<ChildMember> mengberchild) {
        this.mengberchild = mengberchild;
    }

}
