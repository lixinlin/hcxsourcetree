package com.hxlm.hcyandroid.util;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;

import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.health.AbstractBaseActivity;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.BaseApplication;
import com.hxlm.hcyandroid.Constant;
import com.loopj.android.http.AsyncHttpClient;

import java.util.concurrent.Executors;

public class AsyncHttpClientUtil {
    private static AsyncHttpClient client;

    private static int getVersion() {
        Context context = BaseApplication.getContext();
        int versionCode = 0;
        String packageName = context.getPackageName();
        try {
            versionCode = context.getPackageManager().getPackageInfo(
                    packageName, 0).versionCode;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionCode;
    }

    public static AsyncHttpClient getClient() {

        if (client == null) {
            client = new AsyncHttpClient();
            client.setThreadPool(Executors.newFixedThreadPool(3));
            client.setMaxConnections(3);
            client.setTimeout(20000);
            if (BaseActivity.deviceInfo != null)
                client.setUserAgent(BaseActivity.deviceInfo.getUserAgent());
            if (AbstractBaseActivity.deviceInfo != null)
                client.setUserAgent(AbstractBaseActivity.deviceInfo.getUserAgent());
            client.addHeader("Cookie", "token=" + HcyHttpClient.getCookie("token")
                    + ";JSESSIONID=" + HcyHttpClient.getCookie("JSESSIONID"));
            if (Constant.BASE_URL.equals(Constant.TEST_SYSTEM_URL)) {
                client.addHeader("version", Constant.TEST_SYSTEM_HEADER + getVersion());
            } else if (Constant.BASE_URL.equals(Constant.PRODUCTION_SYSTEM_URL)) {
                client.addHeader("version", Constant.PRODUCTION_SYSTEM_HEADER + getVersion());
            }

        }
        return client;
    }
}
