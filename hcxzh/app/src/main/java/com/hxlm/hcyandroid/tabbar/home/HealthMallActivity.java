package com.hxlm.hcyandroid.tabbar.home;

import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.android.utils.Logger;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.tabbar.healthmall.LoginInterface;
import com.hxlm.hcyandroid.tabbar.healthmall.PayInterface;
import com.hxlm.hcyandroid.util.WebViewUtil;

import java.util.HashMap;

public class HealthMallActivity extends BaseActivity {
    private final String tag = getClass().getSimpleName();
    private WebView wv;
    private ProgressBar progressBar;

    private LinearLayout linear_request_error;//网络请求错误提示页面
    private ImageView img_request_again;
    TitleBarView titleBar;
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case LoginInterface.LOGIN:
                    final String aurl = (String) msg.obj;

                    Logger.i(tag, "aurl = " + aurl);

                    LoginControllor.requestLogin(HealthMallActivity.this, new OnCompleteListener() {
                        @Override
                        public void onComplete() {
                            WebViewUtil.syncCookie(HealthMallActivity.this);
//                            wv.loadUrl(aurl);
                            if (Constant.isEnglish) {
                                HashMap<String, String> header = new HashMap<>();
                                header.put("language", "us-en");
                                wv.loadUrl(aurl, header);
                            }else {
                                wv.loadUrl(aurl);
                            }
                        }
                    });
                    break;
            }
        }
    };

    @Override
    public void initViews() {
         titleBar = new TitleBarView();
        titleBar.init(this, "和畅商城", titleBar, 1);

        progressBar=(ProgressBar)findViewById(R.id.progressbar);
        wv = (WebView) findViewById(R.id.wv);
        linear_request_error=(LinearLayout)findViewById(R.id.linearmy_request_error);
        img_request_again=(ImageView)findViewById(R.id.img_request_again);



        wv.addJavascriptInterface(new LoginInterface(handler), "loginInterface");

        wv.addJavascriptInterface(new PayInterface(HealthMallActivity.this), "payInterface");

        String url = Constant.BASE_URL + "/mobileIndex.html";//测试版

       new WebViewUtil().setWebViewInit(wv,progressBar,this,url);
        //new WebViewUtil().setWebView(wv, this);



    }

    @Override
    public void initDatas() {

    }

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_webview);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN
                && keyCode == KeyEvent.KEYCODE_BACK) {
            if (wv.canGoBack()) {
                wv.goBack();
            } else {
                this.finish();
            }
        }
        return true;
    }
}
