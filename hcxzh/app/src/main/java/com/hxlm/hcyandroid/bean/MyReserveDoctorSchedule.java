package com.hxlm.hcyandroid.bean;

public class MyReserveDoctorSchedule {
    private int id;
    private long pauseDate;
    private String pauseTime;
    private String startTime;
    private String endTime;
    private MyReserveDoctorPause doctorPause;
    private int openNum; // 总数量
    private int closeNum; // 已经预约的数量，剩余数量为openNum-closeNum
    private int productId;


    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getPauseDate() {
        return pauseDate;
    }

    public void setPauseDate(long pauseDate) {
        this.pauseDate = pauseDate;
    }

    public String getPauseTime() {
        return pauseTime;
    }

    public void setPauseTime(String pauseTime) {
        this.pauseTime = pauseTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public MyReserveDoctorPause getDoctorPause() {
        return doctorPause;
    }

    public void setDoctorPause(MyReserveDoctorPause doctorPause) {
        this.doctorPause = doctorPause;
    }

    public int getOpenNum() {
        return openNum;
    }

    public void setOpenNum(int openNum) {
        this.openNum = openNum;
    }

    public int getCloseNum() {
        return closeNum;
    }

    public void setCloseNum(int closeNum) {
        this.closeNum = closeNum;
    }

}
