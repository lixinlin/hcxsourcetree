package com.hxlm.hcyandroid.bean;

/**
 * 科室
 *
 * @author Administrator
 */
public class Department {
    private int id;
    private long createDate;
    private long modifyDate;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(long modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Department [id=" + id + ", createDate=" + createDate
                + ", modifyDate=" + modifyDate + ", name=" + name + "]";
    }


}
