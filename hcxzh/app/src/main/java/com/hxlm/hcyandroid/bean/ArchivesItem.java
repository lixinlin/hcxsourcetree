package com.hxlm.hcyandroid.bean;

import android.support.annotation.NonNull;

import com.hxlm.hcyandroid.tabbar.sicknesscheckecg.UploadFailedData;

/**
 * Created by lixinlin on 2018/10/15.
 */

public class ArchivesItem implements Comparable<ArchivesItem> {
    private int itemYype;//选组不同布局
    private String timeDate;
    private String timeMin;
    private String textOne;//上商
    private String textTwo;//经络
    private String textThree;//经络
    private String textFour;//经络

    private String quarter; //季度描述
    private String year;  //季度年
    private int backgroundId; //季度背景

//    最新记录
    private String bloodPressure;
    private String bodyTemperature;
    private String ecg;
    private String jlbs;
    private String oxygen;
    private String tzbs;
    private String zfbs;
    private String reportUrl;

    //   经络、体质 跳转 参数
    private String subject_sn;
    //   脏腑 跳转 参数
    private String cust_id;
    private String physique_id;
//    心电
private UploadFailedData uploadFailedData;
    private String path;//心电路径
    private String yearNum;
    private String quaterNum;

    private Long createDate;

    //和缓病例id
    private String medicRecordId;

    public ArchivesItem() {
    }

    public String getReportUrl() {
        return reportUrl;
    }

    public void setReportUrl(String reportUrl) {
        this.reportUrl = reportUrl;
    }

    public String getMedicRecordId() {
        return medicRecordId;
    }

    public void setMedicRecordId(String medicRecordId) {
        this.medicRecordId = medicRecordId;
    }

    public Long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Long createDate) {
        this.createDate = createDate;
    }

    public String getYearNum() {
        return yearNum;
    }

    public void setYearNum(String yearNum) {
        this.yearNum = yearNum;
    }

    public String getQuaterNum() {
        return quaterNum;
    }

    public void setQuaterNum(String quaterNum) {
        this.quaterNum = quaterNum;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public UploadFailedData getUploadFailedData() {
        return uploadFailedData;
    }

    public void setUploadFailedData(UploadFailedData uploadFailedData) {
        this.uploadFailedData = uploadFailedData;
    }

    public String getCust_id() {
        return cust_id;
    }

    public void setCust_id(String cust_id) {
        this.cust_id = cust_id;
    }

    public String getPhysique_id() {
        return physique_id;
    }

    public void setPhysique_id(String physique_id) {
        this.physique_id = physique_id;
    }

    public String getSubject_sn() {
        return subject_sn;
    }

    public void setSubject_sn(String subject_sn) {
        this.subject_sn = subject_sn;
    }

    public String getBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(String bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public String getBodyTemperature() {
        return bodyTemperature;
    }

    public void setBodyTemperature(String bodyTemperature) {
        this.bodyTemperature = bodyTemperature;
    }

    public String getEcg() {
        return ecg;
    }

    public void setEcg(String ecg) {
        this.ecg = ecg;
    }

    public String getJlbs() {
        return jlbs;
    }

    public void setJlbs(String jlbs) {
        this.jlbs = jlbs;
    }

    public String getOxygen() {
        return oxygen;
    }

    public void setOxygen(String oxygen) {
        this.oxygen = oxygen;
    }

    public String getTzbs() {
        return tzbs;
    }

    public void setTzbs(String tzbs) {
        this.tzbs = tzbs;
    }

    public String getZfbs() {
        return zfbs;
    }

    public void setZfbs(String zfbs) {
        this.zfbs = zfbs;
    }

    public ArchivesItem(int itemYype) {
        this.itemYype = itemYype;
    }

    public int getItemYype() {
        return itemYype;
    }

    public void setItemYype(int itemYype) {
        this.itemYype = itemYype;
    }

    public String getTimeDate() {
        return timeDate;
    }

    public void setTimeDate(String timeDate) {
        this.timeDate = timeDate;
    }

    public String getTimeMin() {
        return timeMin;
    }

    public void setTimeMin(String timeMin) {
        this.timeMin = timeMin;
    }

    public String getTextOne() {
        return textOne;
    }

    public void setTextOne(String textOne) {
        this.textOne = textOne;
    }

    public String getTextTwo() {
        return textTwo;
    }

    public void setTextTwo(String textTwo) {
        this.textTwo = textTwo;
    }

    public String getTextThree() {
        return textThree;
    }

    public void setTextThree(String textThree) {
        this.textThree = textThree;
    }

    public String getTextFour() {
        return textFour;
    }

    public void setTextFour(String textFour) {
        this.textFour = textFour;
    }

    public String getQuarter() {
        return quarter;
    }

    public void setQuarter(String quarter) {
        this.quarter = quarter;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getBackgroundId() {
        return backgroundId;
    }

    public void setBackgroundId(int backgroundId) {
        this.backgroundId = backgroundId;
    }

    @Override
    public int compareTo(@NonNull ArchivesItem o) {
        Long r = o.getCreateDate() - this.createDate;
        int result = 0;
        if(r>0){
            result = 1;
        }else {
            result = -1;
        }
        return result ;
    }
}
