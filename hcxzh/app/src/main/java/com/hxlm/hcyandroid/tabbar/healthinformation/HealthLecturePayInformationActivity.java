package com.hxlm.hcyandroid.tabbar.healthinformation;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.order.CashCardPayView;
import com.hxlm.android.hcy.order.PayView;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.*;
import com.hxlm.hcyandroid.bean.HealthLecture;
import com.hxlm.hcyandroid.util.SharedPreferenceUtil;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class HealthLecturePayInformationActivity extends BaseActivity {

    private PayView payView;
    private CashCardPayView cashCardPayView;

    public static Activity activity;
   // private LectureManager lectureManager;


    private HealthLecture healthLecture; // 从上一个页面传递过来的讲座对象
    private double totalMoney; // 需要支付的金额
    private double lectureTotalMoney;// 从上一个页面传递过来的数据 讲座的费用
    private int count; // 数量


    private int orderId;

    private DecimalFormat decimalFormat;// 钱的数目，小数点保留两位
//    private Handler handler = new Handler() {
//        public void handleMessage(android.os.Message msg) {
//
//            switch (msg.what) {
////                case MessageType.MESSAGE_RESERVE_FREE_LECTURE:
////
////                    BroadcastUtil.sendTypeToPayResult(
////                            HealthLecturePayInformationActivity.this,
////                            ReserveType.TYPE_LECTURE);
////
////                    if (msg.arg1 == ResponseStatus.SUCCESS) {
////
////                        // 现金卡支付
////                        // 讲座
////                        SharedPreferenceUtil.saveString("healthlecturenum", "2");
////
////                        Intent intent = new Intent(HealthLecturePayInformationActivity.this,
////                                PaySuccessActivity.class);
////                        startActivity(intent);
////                    } else if (msg.arg1 == ResponseStatus.UNLOGIN) {
////                        ToastUtil.invokeShortTimeToast(HealthLecturePayInformationActivity.this,
////                                HealthLecturePayInformationActivity.this
////                                        .getString(R.string.unlogin));
////                        SharedPreferenceUtil.logoff();
////
////                        LoginControllor.requestLogin(HealthLecturePayInformationActivity.this, new OnCompleteListener() {
////                            @Override
////                            public void onComplete() {
////                                if (waittingDialog == null) {
////                                    waittingDialog = DialogUtils
////                                            .displayWaitDialog(HealthLecturePayInformationActivity.this);
////                                }
////                                if (!waittingDialog.isShowing()) {
////                                    waittingDialog.show();
////                                }
////                                lectureManager.reserveHealthLecture(
////                                        healthLecture.getId() + "", count,
////                                        lectureTotalMoney);
////                            }
////                        });
////
////
////                    } else {
////                        ToastUtil.invokeShortTimeToast(
////                                HealthLecturePayInformationActivity.this, "预约失败");
////                    }
////                    break;
//                //收费讲座
//                case MessageType.MESSAGE_RESERVE_NOT_FREE_LECTURE:
//                    if (msg.arg1 == ResponseStatus.SUCCESS) {
//                        orderId = (Integer) msg.obj;
//
//                        reserveNotFreeLecture(orderId);
//                    } else if (msg.arg1 == ResponseStatus.UNLOGIN) {
//                        ToastUtil.invokeShortTimeToast(
//                                HealthLecturePayInformationActivity.this,
//                                HealthLecturePayInformationActivity.this
//                                        .getString(R.string.unlogin));
//                        SharedPreferenceUtil.logoff();
//
//                        LoginControllor.requestLogin(HealthLecturePayInformationActivity.this, new OnCompleteListener() {
//                            @Override
//                            public void onComplete() {
//
//                                lectureManager.reserveHealthLecture(
//                                        healthLecture.getId() + "", count,
//                                        lectureTotalMoney);
//                            }
//                        });
//
//
//                    } else if (msg.arg1 == ResponseStatus.RESERVE_OVER_LIMIT) {
//                        ToastUtil.invokeShortTimeToast(
//                                HealthLecturePayInformationActivity.this,
//                                "您预约的数量已超过系统限制");
//                    } else {
//                        ToastUtil.invokeShortTimeToast(
//                                HealthLecturePayInformationActivity.this, "预约失败");
//                    }
//                    break;
//                case MessageType.MESSAGE_QUERY_REMOTE_FAILED:
//                    ToastUtil.invokeShortTimeToast(
//                            HealthLecturePayInformationActivity.this,
//                            HealthLecturePayInformationActivity.this
//                                    .getString(R.string.check_net));
//                    break;
//            }
//        }
//    };


    @Override
    public void setContentView() {
        setContentView(R.layout.activity_health_lecture_pay_information);
        activity = HealthLecturePayInformationActivity.this;
    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, "支付信息", titleBar, 1);

    }

    @Override
    public void initDatas() {
        decimalFormat = new DecimalFormat("#.##");

        payView = (PayView) findViewById(R.id.pv_lecture);

        cashCardPayView = (CashCardPayView) findViewById(R.id.ccpv_lecture);

        ImageView iv_health_lecture_doctor_icon = (ImageView) findViewById(R.id.iv_health_lecture_doctor_icon);
        TextView tv_health_lecture_title = (TextView) findViewById(R.id.tv_health_lecture_title);
        TextView tv_health_lecture_name = (TextView) findViewById(R.id.tv_health_lecture_name);
        TextView tv_health_lecture_address = (TextView) findViewById(R.id.tv_health_lecture_address);
        TextView tv_money_to_pay_up = (TextView) findViewById(R.id.tv_money_to_pay_up);


        TextView tv_count = (TextView) findViewById(R.id.tv_count_jiangzuo);
        // 就诊时间
        TextView tv_reservation_time = (TextView) findViewById(R.id.tv_reservation_time);


        //lectureManager = new LectureManager(handler);

        Bundle build = getIntent().getBundleExtra("bundle");
        healthLecture = (HealthLecture) build.getSerializable("healthLecture");
        lectureTotalMoney = getIntent().getDoubleExtra("totalMoney", 0);
        count = getIntent().getIntExtra("count", 0);
        orderId=getIntent().getIntExtra("orderId",0);//订单id

        tv_health_lecture_title.setText(healthLecture.getTitle());
        tv_health_lecture_name.setText(healthLecture.getTalker());

        SimpleDateFormat format1 = new SimpleDateFormat("yyyy.MM.dd");
        long beginDate = healthLecture.getBeginDate();
        String str_beginDate = format1.format(new Date(beginDate));
        long endDate = healthLecture.getEndDate();
        String str_endDate = format1.format(new Date(endDate));
        SimpleDateFormat format2 = new SimpleDateFormat("HH:mm");
        long lectureDate = healthLecture.getLectureDate();
        String str_lectureDate = format2.format(new Date(lectureDate));
        String area = healthLecture.getArea();
        String timeAndLocale = str_beginDate + "-" + str_endDate + "日 "
                + str_lectureDate;

        // 就诊时间
        tv_reservation_time.setText(timeAndLocale);
        tv_health_lecture_address.setText(area);// 就诊地址


        tv_count.setText("X" + count + "");
        tv_money_to_pay_up.setText("¥" + lectureTotalMoney);

        ImageLoader.getInstance().displayImage(healthLecture.getPicture(),
                iv_health_lecture_doctor_icon);


    }

    @Override
    protected void onResume() {
        super.onResume();
        totalMoney = lectureTotalMoney;
        //---------------------------新加的

        String productsStr = null;//根据ProductId查询不同的商品类别所对应的现金卡
        String itemIdsStr = null;//id
        String descStr = null;//名称
        double moneySum = 0;//总价格


        moneySum = totalMoney;
        itemIdsStr = "";
        descStr = healthLecture.getTitle();
        productsStr = String.valueOf(healthLecture.getProductId());


        if (Constant.DEBUG) {
            Log.i("Log.i", "orderId-->" + orderId);
        }
        //支付需要的数据
        payView.init(ReserveType.TYPE_LECTURE, moneySum, itemIdsStr, descStr, orderId);

        payView.setOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete() {
                //使用积分卡完成支付
                SharedPreferenceUtil.saveString("healthlecturenum", "2");
            }
        });

        cashCardPayView.init(productsStr, payView);


    }



}
