package com.hxlm.hcyandroid.bean;

public class Subject {
    private int id;
    private long createDate;
    private long modifyDate;
    private String name;
    private String introduction;
    private String picture;
    private String subject_sn;


    public String getSubject_sn() {
        return subject_sn;
    }

    public void setSubject_sn(String subject_sn) {
        this.subject_sn = subject_sn;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(long modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
