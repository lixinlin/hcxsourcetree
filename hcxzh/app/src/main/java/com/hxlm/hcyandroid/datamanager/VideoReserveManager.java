package com.hxlm.hcyandroid.datamanager;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.hxlm.hcyandroid.MessageType;
import com.hxlm.hcyandroid.ResponseStatus;
import com.hxlm.hcyandroid.bean.MyVideoReserve;
import com.hxlm.hcyandroid.bean.VideoAccount;
import com.hxlm.hcyandroid.util.SharedPreferenceUtil;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

public class VideoReserveManager extends ReserveBaseManager {

    public VideoReserveManager(Handler handler) {
        super(handler);
    }

    /**
     * 获取我的视频预约列表
     */
    public void getMyVideoReserve(Integer pageNumber) {
        String method = METHOD_GET;
        String id = SharedPreferenceUtil.getMemberId();
        String url = "/member/doctorSubscribe/list_video/" + id + ".jhtml";
        RequestParams params = new RequestParams();
        params.put("id", id);
        params.put("pageNumber", pageNumber);
        queryRemoteWithHeader(method, url, params, new OnSuccessListener() {

            @Override
            public void onSuccess(String content) {
                List<MyVideoReserve> myReserves = new ArrayList<MyVideoReserve>();
                JSONObject jo = JSON.parseObject(content);
                int status = jo.getInteger("status");
                int totalPages = -1;
                if (status == ResponseStatus.SUCCESS) {
                    JSONObject data = jo.getJSONObject("data");
                    JSONArray ja_content = data.getJSONArray("content");
                    if (ja_content != null) {
                        myReserves = JSON.parseArray(ja_content.toJSONString(),
                                MyVideoReserve.class);
                    }
                    totalPages = data.getInteger("totalPages");
                }
                Message message = Message.obtain();
                message.arg1 = status;
                message.arg2 = totalPages;
                message.what = MessageType.MESSAGE_GET_VIDEO_RESERVES;
                message.obj = myReserves;
                mHandler.sendMessage(message);
            }
        });
    }

    /**
     * 获取视频子账户
     *
     * @param memberDoctorInfoId 用户预定表id
     */
    public void getVideoAccount(int memberDoctorInfoId) {
        String method = METHOD_GET;
        String url = "/video_call_account/videoAccount.jhtml";
        RequestParams params = new RequestParams();
        params.put("memberDoctorInfoId", memberDoctorInfoId);
        queryRemoteWithHeader(method, url, params, new OnSuccessListener() {

            @Override
            public void onSuccess(String content) {
                VideoAccount account = new VideoAccount();
                JSONObject jo = JSON.parseObject(content);
                Log.d("VideoReserveManager", "content = " + content);
                int status = jo.getInteger("status");
                if (status == ResponseStatus.SUCCESS) {
                    JSONObject data = jo.getJSONObject("data");
                    account = JSON.parseObject(data.toJSONString(),
                            VideoAccount.class);
                }
                Message message = Message.obtain();
                message.arg1 = status;
                message.what = MessageType.MESSAGE_GET_VIDEO_ACCOUNT;
                message.obj = account;
                mHandler.sendMessage(message);
            }
        });
    }


}
