package com.hxlm.hcyandroid.bean;

import com.hxlm.android.hcy.order.Card;

import java.io.Serializable;
import java.util.List;

public class ReserveDoctorResponse implements Serializable {
    private List<Card> cashcardCodes;//现金卡
    private OrderItem order;//订单

    public List<Card> getCashcardCodes() {
        return cashcardCodes;
    }

    public void setCashcardCodes(List<Card> cashcardCodes) {
        this.cashcardCodes = cashcardCodes;
    }

    public OrderItem getOrder() {
        return order;
    }

    public void setOrder(OrderItem order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return "ReserveDoctorResponse [cashcardCodes=" + cashcardCodes
                + ", order=" + order + "]";
    }


}
