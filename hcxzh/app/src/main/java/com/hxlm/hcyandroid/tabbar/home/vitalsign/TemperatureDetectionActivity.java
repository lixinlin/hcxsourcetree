package com.hxlm.hcyandroid.tabbar.home.vitalsign;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.hcy.ky3h.R;
import com.hxlm.android.comm.AbstractDeviceActivity;
import com.hxlm.android.comm.AbstractIOSession;
import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.comm.Error;
import com.hxlm.android.comm.Error_English;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.report.UploadManager;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;
import com.hxlm.android.health.device.message.temperature.TemperatureDataMessage;
import com.hxlm.android.health.device.model.ChargePal;
import com.hxlm.hcyandroid.CheckedDataType;
import com.hxlm.hcyandroid.bean.CheckStep;
import com.hxlm.hcyandroid.util.ToastUtil;

import java.util.List;

/**
 * 体温检测
 *
 * @author l
 */
public class TemperatureDetectionActivity extends AbstractDeviceActivity implements
        OnClickListener {

    private ListView lv_tips;
    private List<CheckStep> steps;
    private ImageView iv_tiwen_Noequipment;// 非设备检测
    private LinearLayout ll_temperature_info;// 检测体温、
    private TextView tv_start_test;// 开始检测
    private LinearLayout linear_check;//设备检测
    private TextView tv_now_temp;// 当前体温
    private TextView tv_tiwen_now;// 体温摄氏度
    private TextView tv_restart_jiance;// 重新检测
    private ImageView iv_tiwen_submit;// 提交
    private ImageView iv_tem_use_specification;// 使用规范
    private String strtw;
    private Context context;
    private double wendu;// 当前温度
    private String isNormal;// 是否正常
    private TextView tv_device_connect_status;//设备接入状态
    private ImageView iv_device_connect_status;//设备接入图标

    private Dialog waittingDialog;
    private UploadManager uploadManager;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_temperature_detection);
        context = TemperatureDetectionActivity.this;
    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, "体温检测", titleBar, 1);
        iv_tiwen_Noequipment = (ImageView) findViewById(R.id.iv_tiwen_Noequipment);
        iv_tiwen_Noequipment.setOnClickListener(this);

        linear_check = (LinearLayout) findViewById(R.id.linear_check);
        ll_temperature_info = (LinearLayout) findViewById(R.id.ll_temperature_info);
        tv_start_test = (TextView) findViewById(R.id.tv_start_test);
        tv_now_temp = (TextView) findViewById(R.id.tv_now_temp);
        tv_tiwen_now = (TextView) findViewById(R.id.tv_tiwen_now);
        tv_restart_jiance = (TextView) findViewById(R.id.tv_restart_jiance);
        iv_tiwen_submit = (ImageView) findViewById(R.id.iv_tiwen_submit);
        iv_tem_use_specification = (ImageView) findViewById(R.id.iv_tem_use_specification);
        iv_device_connect_status = (ImageView) findViewById(R.id.iv_device_connect_status);
        tv_device_connect_status = (TextView) findViewById(R.id.tv_device_connect_status);
        iv_tem_use_specification.setOnClickListener(this);

        ll_temperature_info.setOnClickListener(this);
        iv_tiwen_submit.setOnClickListener(this);
    }

    @Override
    public void initDatas() {
        uploadManager = new UploadManager();

        AbstractIOSession ioSession = new ChargePal().getIOSession(this);
        if (ioSession != null) {
            ioSession.connect();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            // 非设备检测
            case R.id.iv_tiwen_Noequipment:
                Intent intent = new Intent(TemperatureDetectionActivity.this,
                        TemperatureDetectionWriteActivity.class);
                startActivity(intent);
                break;

            // 开始检测
            case R.id.ll_temperature_info:
                tv_start_test.setVisibility(View.GONE);
                iv_tiwen_Noequipment.setVisibility(View.GONE);
                linear_check.setVisibility(View.VISIBLE);
                iv_tiwen_submit.setVisibility(View.VISIBLE);

                break;
            // 提交
            case R.id.iv_tiwen_submit:
                strtw = tv_tiwen_now.getText().toString();
                // wendu = Double.parseDouble(strtw);
                wendu = 36.0;
                if (TextUtils.isEmpty(strtw)) {
                    ToastUtil.invokeShortTimeToast(context, "当前体温不能为空");
                } else {

                    if (wendu > 0 && wendu < 36) {
                        ToastUtil.invokeShortTimeToast(context, "请您输入正确的体温范围");
                    } else if (wendu >= 36 && wendu < 37.3) {
                        isNormal = "正常";
                    } else if (wendu >= 37.3 && wendu <= 38) {
                        // ToastUtil.invokeShortTimeToast(context, "您当前的温度属于低热");
                        isNormal = "偏低";
                    } else if (wendu >= 38.1 && wendu <= 39) {
                        // ToastUtil.invokeShortTimeToast(context, "您当前的温度属于中度发热");
                        isNormal = "偏高";
                    } else if (wendu >= 39.1 && wendu <= 41) {
                        // ToastUtil.invokeShortTimeToast(context, "您当前的温度属于高热");
                        isNormal = "偏高";
                    } else if (wendu > 41) {
                        // ToastUtil.invokeShortTimeToast(context, "您当前的温度属于超高热");
                        isNormal = "偏高";
                    }

                    // 输入正常值才上传数据
                    uploadManager.uploadCheckedData(CheckedDataType.TEMPERATURE, wendu,0,
                            new AbstractDefaultHttpHandlerCallback(this) {
                                @Override
                                protected void onResponseSuccess(Object obj) {
                                    new TemperatureDialog(context, "您当前体温" + wendu + "ºC", isNormal).show();
                                }
                            });
                }

                break;
            // 使用规范
            case R.id.iv_tem_use_specification:
                Intent intentuse = new Intent(TemperatureDetectionActivity.this,
                        TemperatureUseActivity.class);
                startActivity(intentuse);

                break;
        }
    }

    @Override
    public void onConnectFailed(Error error) {
        Toast.makeText(this, error.getDesc(), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onConnectFailedEnglist(Error_English error) {
        Toast.makeText(this, error.getDesc(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnected() {
        Toast.makeText(TemperatureDetectionActivity.this, "设备已连接", Toast.LENGTH_SHORT).show();
        tv_device_connect_status.setText("已接入");
        iv_device_connect_status.setImageResource(R.drawable.ecg_connect);
    }

    @Override
    public void onDisconnected() {
        Toast.makeText(TemperatureDetectionActivity.this, "设备已断开", Toast.LENGTH_SHORT).show();
        tv_device_connect_status.setText("未接入");
    }

    @Override
    public void onExceptionCaught(Throwable e) {
        Toast.makeText(TemperatureDetectionActivity.this,
                "连接出现异常：" + e.getMessage(), Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onMessageReceived(AbstractMessage message) {
        switch ((HealthDeviceMessageType) message.getMessageType()) {
            case TEMPERATURE_DATA:
                TemperatureDataMessage temperatureDataMessage = (TemperatureDataMessage) message;
                if (temperatureDataMessage.getTemperatureState() == 1) {
                    Toast.makeText(TemperatureDetectionActivity.this, "体温探头滑落",
                            Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public class TemperatureDialog extends AlertDialog implements
            OnClickListener {

        Context context;
        String tishi;
        String isnormal;

        String str_tishi = "";

        public TemperatureDialog(Context context, String tishi, String isnormal) {
            super(context);
            this.context = context;
            this.tishi = tishi;
            this.isnormal = isnormal;

        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.temperaturedetectionwrite_submit_prompt);

            TextView tv_breath_prompt_tishi = (TextView) findViewById(R.id.tv_breath_prompt_tishi);
            tv_breath_prompt_tishi.setText(tishi);

            // 体温
            TextView tv_is_normal = (TextView) findViewById(R.id.tv_is_normal);

            // 确定
            ImageView image_submit = (ImageView) findViewById(R.id.image_submit);
            image_submit.setOnClickListener(this);

            if ("正常".equals(isnormal)) {
                str_tishi = "您当前体温值正常";
                SpannableStringBuilder style = new SpannableStringBuilder(
                        str_tishi);
                style.setSpan(
                        new ForegroundColorSpan(getResources().getColor(
                                R.color.submit_normal)), 6, 8,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tv_is_normal.setText(style);

            } else if ("偏高".equals(isnormal)) {
                str_tishi = "体温值偏高。若排除外界影响因素如情绪激动、精神紧张、进食、运动、怀孕（特指适龄女性）等情况外，伴有头痛、乏力等不适症状发生，请及时到医院就诊。";
                SpannableStringBuilder style = new SpannableStringBuilder(
                        str_tishi);
                style.setSpan(
                        new ForegroundColorSpan(getResources().getColor(
                                R.color.submit_not_normal)), 3, 5,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tv_is_normal.setText(style);

            } else if ("偏低".equals(isnormal)) {
                str_tishi = "体温值偏低。若连续2周体温皆37.3°以上，需警惕肺部及慢性感染所致，请及时到医院就诊。";
                SpannableStringBuilder style = new SpannableStringBuilder(
                        str_tishi);
                style.setSpan(
                        new ForegroundColorSpan(getResources().getColor(
                                R.color.submit_not_normal)), 3, 5,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                tv_is_normal.setText(style);
            }

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                // 确定
                case R.id.image_submit:
                    tv_start_test.setVisibility(View.VISIBLE);
                    linear_check.setVisibility(View.GONE);
                    this.dismiss();

                    break;

            }
        }
    }
}
