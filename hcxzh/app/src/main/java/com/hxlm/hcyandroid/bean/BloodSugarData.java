package com.hxlm.hcyandroid.bean;

public class BloodSugarData {

    public static final String BLOOD_SUGAR_TYPE_EMPTY = "empty";
    public static final String BLOOD_SUGAR_TYPE_FULL = "full";

    //凌晨
    public static final String BLOOD_SUGAR_TYPE_BEFORE_DAWN = "beforeDawn";
    //早餐前
    public static final String BLOOD_SUGAR_TYPE_BEFORE_BREAKFAST = "beforeBreakfast";
    //早餐后
    public static final String BLOOD_SUGAR_TYPE_AFTER_BREAKFAST = "afterBreakfast";
    //午餐前
    public static final String BLOOD_SUGAR_TYPE_BEFORE_LUNCH = "beforeLunch";
    //午餐后
    public static final String BLOOD_SUGAR_TYPE_AFTER_LUNCH = "afterLunch";
    //晚餐前
    public static final String BLOOD_SUGAR_TYPE_BEFORE_DINNER = "beforeDinner";
    //晚餐后
    public static final String BLOOD_SUGAR_TYPE_AFTER_DINNER = "afterDinner";
    //睡前
    public static final String BLOOD_SUGAR_TYPE_BEFORE_SLEEP = "beforeSleep";

    private String type;
    private double levels;
    private boolean isAbnormity;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getLevels() {
        return levels;
    }

    public void setLevels(double levels) {
        this.levels = levels;
    }

    public boolean isAbnormity() {
        return isAbnormity;
    }

    public void setAbnormity(boolean isAbnormity) {
        this.isAbnormity = isAbnormity;
    }

    @Override
    public String toString() {
        return "BloodSugarData [type=" + type + ", levels=" + levels
                + ", isAbnormity=" + isAbnormity + "]";
    }


}
