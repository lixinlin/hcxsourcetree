package com.hxlm.hcyandroid.bean;

import java.io.Serializable;

public class HealthAdvisor implements Serializable {
    private int id;
    private long createDate;
    private long modifyDate;
    private int order;// 排序
    private String name;// 姓名
    private String gender;// 性别
    private long birth;// 生日
    private String graduated;// 毕业院校
    private String rank;// 职称
    private String level;// 级别
    private String skill;// 专长
    private String image;// 头像
    private String introduction;// 介绍
    private boolean isYanHuang;// 是否是炎黄正式员工
    private String advisorCategory;// 顾问分类
    private String serviceLevel;// 服务级别
    private int serviceOrder;// 星级
    private int serviceNum;// 可服务人数
    private int servicedNum;// 已服务人数
    private String phone;// 联系方式

    public int getServiceOrder() {
        return serviceOrder;
    }

    public void setServiceOrder(int serviceOrder) {
        this.serviceOrder = serviceOrder;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(long modifyDate) {
        this.modifyDate = modifyDate;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public long getBirth() {
        return birth;
    }

    public void setBirth(long birth) {
        this.birth = birth;
    }

    public String getGraduated() {
        return graduated;
    }

    public void setGraduated(String graduated) {
        this.graduated = graduated;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public boolean isYanHuang() {
        return isYanHuang;
    }

    public void setYanHuang(boolean isYanHuang) {
        this.isYanHuang = isYanHuang;
    }

    public String getAdvisorCategory() {
        return advisorCategory;
    }

    public void setAdvisorCategory(String advisorCategory) {
        this.advisorCategory = advisorCategory;
    }

    public String getServiceLevel() {
        return serviceLevel;
    }

    public void setServiceLevel(String serviceLevel) {
        this.serviceLevel = serviceLevel;
    }

    public int getServiceNum() {
        return serviceNum;
    }

    public void setServiceNum(int serviceNum) {
        this.serviceNum = serviceNum;
    }

    public int getServicedNum() {
        return servicedNum;
    }

    public void setServicedNum(int servicedNum) {
        this.servicedNum = servicedNum;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "HealthAdvisor{" +
                "id=" + id +
                ", createDate=" + createDate +
                ", modifyDate=" + modifyDate +
                ", order=" + order +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", birth=" + birth +
                ", graduated='" + graduated + '\'' +
                ", rank='" + rank + '\'' +
                ", level='" + level + '\'' +
                ", skill='" + skill + '\'' +
                ", image='" + image + '\'' +
                ", introduction='" + introduction + '\'' +
                ", isYanHuang=" + isYanHuang +
                ", advisorCategory='" + advisorCategory + '\'' +
                ", serviceLevel='" + serviceLevel + '\'' +
                ", serviceOrder=" + serviceOrder +
                ", serviceNum=" + serviceNum +
                ", servicedNum=" + servicedNum +
                ", phone='" + phone + '\'' +
                '}';
    }
}
