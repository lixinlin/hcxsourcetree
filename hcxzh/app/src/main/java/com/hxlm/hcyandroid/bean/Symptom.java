package com.hxlm.hcyandroid.bean;

import java.io.Serializable;

public class Symptom implements Serializable {
    private int id;
    private String personPart;//症状总类
    private String part;//症状分类
    private String desc;//症状表现
    private int sexType;
    private String code;
    private String info;
    private boolean isChoosed;
    private int imageId;
    private String dregree;

    public Symptom(int id, String personPart, String part, String desc,
                   int sexType, String code, String info, boolean isChoosed,
                   int imageId, String dregree) {
        super();
        this.id = id;
        this.personPart = personPart;
        this.part = part;
        this.desc = desc;
        this.sexType = sexType;
        this.code = code;
        this.info = info;
        this.isChoosed = isChoosed;
        this.imageId = imageId;
        this.dregree = dregree;
    }

    public Symptom(int id, String personPart, String part, String desc,
                   int sexType, String code, String info) {
        super();
        this.id = id;
        this.personPart = personPart;
        this.part = part;
        this.desc = desc;
        this.sexType = sexType;
        this.code = code;
        this.info = info;
        this.isChoosed = false;
        this.imageId = -1;
        this.dregree = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDregree() {
        return dregree;
    }

    public void setDregree(String dregree) {
        this.dregree = dregree;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public boolean isChoosed() {
        return isChoosed;
    }

    public void setChoosed(boolean isChoosed) {
        this.isChoosed = isChoosed;
    }

    public String getPersonPart() {
        return personPart;
    }

    public void setPersonPart(String personPart) {
        this.personPart = personPart;
    }

    public String getPart() {
        return part;
    }

    public void setPart(String part) {
        this.part = part;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getSexType() {
        return sexType;
    }

    public void setSexType(int sexType) {
        this.sexType = sexType;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

}
