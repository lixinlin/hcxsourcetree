package com.hxlm.hcyandroid.datamanager;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.asynchttp.HcyHttpResponseHandler;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.MessageType;
import com.hxlm.hcyandroid.ResponseStatus;
import com.hxlm.hcyandroid.util.ACache;
import com.loopj.android.http.RequestParams;
import org.apache.http.Header;

import java.io.File;

/**
 * 所有业务逻辑管理器的基类，用于提供一些公共的变量和方法 Created by Zhenyu on 2015/8/22.
 */
public class BaseManager {
    protected static final String METHOD_GET = "get";

    protected final String tag;

    protected final Handler mHandler;

    public BaseManager() {
        tag  = this.getClass().getSimpleName();
        mHandler = null;
    }

    public BaseManager(Handler handler) {
        this.mHandler = handler;
        tag = this.getClass().getSimpleName();
    }

    /**
     * 创建本地文件存储需要的路径。
     *
     * @param filePath 本地文件的存储路径
     * @return boolean 路径创建成功返回true，失败返回false；
     */
    protected final boolean createPath(String filePath) {
        int cutPosition = filePath.lastIndexOf("/");
        String path = filePath.substring(0, cutPosition);
        File localPath = new File(path);

        return localPath.exists() || localPath.mkdirs();
    }

    /**
     * 不带cookie的请求后台的方法,默认不进行缓存
     *
     * @param method            请求方法（post，get）
     * @param url               请求路径
     * @param params            参数
     * @param onSuccessListener 请求成功的回调
     */
    protected void queryRemoteWithoutHeader(String method, String url, RequestParams params,
                                            final OnSuccessListener onSuccessListener) {
        queryRemoteWithoutHeader(method, url, params, onSuccessListener, false);
    }

    /**
     * 不带cookie的请求后台的方法
     *
     * @param method            请求方式
     * @param url               请求的url
     * @param params            参数
     * @param onSuccessListener 请求成功的回调
     * @param isCache           是否使用缓存
     */
    protected void queryRemoteWithoutHeader(String method, String url, RequestParams params,
                                            final OnSuccessListener onSuccessListener, boolean isCache) {
        //判断是否使用了缓存 true为使用缓存，false为不使用缓存
        if (isCache) {
            String cacheJson = ACache.getAsString(url + params.toString());//读取缓存的string

            //如果缓存得到的数据为空
            if (TextUtils.isEmpty(cacheJson)) {
                //请求网络并缓存
                baseQueryRemoteWithoutHeader(method, url, params,
                        onSuccessListener, true);
            } else {
                //否则直接得到缓存数据
                onSuccessListener.onSuccess(cacheJson);
            }
        }
        //不使用缓存,正常请求网络
        else {
            baseQueryRemoteWithoutHeader(method, url, params,
                    onSuccessListener, false);
        }
    }

    /**
     * 基本的不带cookie的请求后台的方法
     *
     * @param method            请求方式
     * @param url               请求地址
     * @param params            请求参数
     * @param onSuccessListener 成功回调
     * @param isCache           如果请求成功，是否缓存到本地
     */
    private void baseQueryRemoteWithoutHeader(String method, final String url, final RequestParams params,
                                              final OnSuccessListener onSuccessListener, final boolean isCache) {

        int httpMethod;
        if (METHOD_GET.equals(method)) {
            httpMethod = HcyHttpClient.METHOD_GET;
        } else {
            httpMethod = HcyHttpClient.METHOD_POST;
        }

        HcyHttpResponseHandler responseHandler = new HcyHttpResponseHandler(null) {
            @Override
            public void onSuccess(int arg0, Header[] arg1, String arg2) {
                onSuccessListener.onSuccess(arg2);
                if (Constant.DEBUG) {
                    Log.i("Log.i", "success content = " + arg2);
                }

                //如果要求缓存
                if (isCache) {
                    JSONObject jo = JSON.parseObject(arg2);
                    int status = jo.getInteger("status");
                    //如果请求到数据
                    if (status == ResponseStatus.SUCCESS) {
                        ACache.put(url + params.toString(), arg2);//缓存数据
                    }
                }
            }

            @Override
            public void onFailure(int arg0, Header[] arg1, String arg2, Throwable arg3) {
                queryRemoteFailed();
            }

            @Override
            protected Object contentParse(String content) {
                return null;
            }
        };

        HcyHttpClient.submit(httpMethod, url, params, responseHandler);
    }

    /**
     * 带cookie的请求后台的方法
     *
     * @param method            请求方法（post，get）
     * @param url               请求地址
     * @param params            请求参数
     * @param onSuccessListener 调用成功的回调
     */
    protected void queryRemoteWithHeader(String method, String url, RequestParams params,
                                         final OnSuccessListener onSuccessListener) {

        int httpMethod;
        if (METHOD_GET.equals(method)) {
            httpMethod = HcyHttpClient.METHOD_GET;
        } else {
            httpMethod = HcyHttpClient.METHOD_POST;
        }

        HcyHttpResponseHandler responseHandler = new HcyHttpResponseHandler(null) {
            @Override
            public void onSuccess(int arg0, Header[] arg1, String arg2) {
                onSuccessListener.onSuccess(arg2);

                if (Constant.DEBUG) {
                    Log.i("Log.i", "success content = " + arg2);
                }
            }

            @Override
            public void onFailure(int arg0, Header[] arg1, String arg2, Throwable arg3) {

                queryRemoteFailed();
            }

            @Override
            protected Object contentParse(String content) {
                return null;
            }
        };

        HcyHttpClient.submit(httpMethod, url, params, responseHandler);
    }

    protected void queryRemoteFailed() {

        Message message = Message.obtain();
        message.what = MessageType.MESSAGE_QUERY_REMOTE_FAILED;
        mHandler.sendMessage(message);
    }

    //接口回调
    public interface OnSuccessListener {
        void onSuccess(String content);
    }
}
