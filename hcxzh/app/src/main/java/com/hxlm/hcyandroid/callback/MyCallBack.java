package com.hxlm.hcyandroid.callback;

/**
 * 回调接口
 *
 * @author l
 */
public class MyCallBack {
    public interface OnLoginListener {
        void onLogin();
    }

    public interface OnChangeListener {
        void onChange();
    }

    public interface OnChoosedMemberListener {
        void onChoosedMember();
    }

    public interface OnCreateNewPasswordSuccessListener {
        void onCreateNewPasswordSuccess();
    }

    public interface OnBackClickListener {
        void onBackClicked();
    }

    public interface OnLoginTrueLinstener {
        void onLoginTrue();
    }


}
