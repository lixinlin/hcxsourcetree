package com.hxlm.hcyandroid.datamanager;

import android.os.Handler;
import android.os.Message;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.hxlm.hcyandroid.MessageType;
import com.hxlm.hcyandroid.ResponseStatus;
import com.hxlm.hcyandroid.bean.Advertisement;

import java.util.ArrayList;
import java.util.List;

/**
 * 广告接口
 *
 * @author l
 */
public class AdvertisementManager extends BaseManager {

    public AdvertisementManager(Handler handler) {
        super(handler);
    }

    /**
     * 获取广告接口
     */
    public void getAdvertisement() {
        String method = METHOD_GET;
        String url = "/ad/index.jhtml";
        queryRemoteWithoutHeader(method, url, null, new OnSuccessListener() {

            @Override
            public void onSuccess(String content) {
                List<Advertisement> informations = new ArrayList<Advertisement>();
                JSONObject jo = JSON.parseObject(content);
                int status = jo.getInteger("status");
                if (status == ResponseStatus.SUCCESS) {
                    JSONArray data = jo.getJSONArray("data");
                    informations = JSON.parseArray(data.toJSONString(),
                            Advertisement.class);
                }
                Message message = Message.obtain();
                message.what = MessageType.MESSAGE_GET_ADVERTISEMENT;
                message.arg1 = status;
                message.obj = informations;
                mHandler.sendMessage(message);
            }
        });
    }

}
