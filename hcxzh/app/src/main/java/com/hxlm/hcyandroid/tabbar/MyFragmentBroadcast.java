package com.hxlm.hcyandroid.tabbar;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyphone.MainActivity;

/**
 * Created by l on 2016/10/28.
 * 健康商城支付成功，点击按钮跳转到我的页面
 */
public class MyFragmentBroadcast extends BroadcastReceiver {
    private String myFragment="";

    private Handler handler;
    public MyFragmentBroadcast(Handler handler)
    {
        this.handler=handler;
    }
    //设置action
    public static final String ACTION="com.hxlm.hcyandroid.tabbar.intent.action.MyFragmentBroadcast";
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle=new Bundle();

        myFragment = intent.getStringExtra("MyFragment");//要跳转到健康档案
        if(Constant.DEBUG)
        {
            Log.i("Log.i","广播接收到的myFragment-->"+myFragment);
        }


        bundle.putString("MyFragment", myFragment);

        Message message = new Message();
        message.what = MainActivity.MY_FRAGMENT;
        message.setData(bundle);
        handler.sendMessage(message);

    }
}
