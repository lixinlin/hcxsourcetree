package com.hxlm.hcyandroid.bean;

public class VideoAccount {
    private int id;
    private String doctorFriendlyName;// 医生账号名
    private String doctorSubAccountSid;// 医生子账户
    private String doctorSubToken;// 医生子账户Token
    private String doctorVoipAccount;// 医生VOIP账号
    private String doctorVoipPwd;// 医生VOIP账号密码
    private String friendlyName;// 用户账户名
    private String subAccountSid;// 用户子账户
    private String subToken;// 用户子账户Token
    private String voipAccount;// 用户VOIP账号
    private String voipPwd;// 用户VOIP账号密码

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDoctorFriendlyName() {
        return doctorFriendlyName;
    }

    public void setDoctorFriendlyName(String doctorFriendlyName) {
        this.doctorFriendlyName = doctorFriendlyName;
    }

    public String getDoctorSubAccountSid() {
        return doctorSubAccountSid;
    }

    public void setDoctorSubAccountSid(String doctorSubAccountSid) {
        this.doctorSubAccountSid = doctorSubAccountSid;
    }

    public String getDoctorSubToken() {
        return doctorSubToken;
    }

    public void setDoctorSubToken(String doctorSubToken) {
        this.doctorSubToken = doctorSubToken;
    }

    public String getDoctorVoipAccount() {
        return doctorVoipAccount;
    }

    public void setDoctorVoipAccount(String doctorVoipAccount) {
        this.doctorVoipAccount = doctorVoipAccount;
    }

    public String getDoctorVoipPwd() {
        return doctorVoipPwd;
    }

    public void setDoctorVoipPwd(String doctorVoipPwd) {
        this.doctorVoipPwd = doctorVoipPwd;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public void setFriendlyName(String friendlyName) {
        this.friendlyName = friendlyName;
    }

    public String getSubAccountSid() {
        return subAccountSid;
    }

    public void setSubAccountSid(String subAccountSid) {
        this.subAccountSid = subAccountSid;
    }

    public String getSubToken() {
        return subToken;
    }

    public void setSubToken(String subToken) {
        this.subToken = subToken;
    }

    public String getVoipAccount() {
        return voipAccount;
    }

    public void setVoipAccount(String voipAccount) {
        this.voipAccount = voipAccount;
    }

    public String getVoipPwd() {
        return voipPwd;
    }

    public void setVoipPwd(String voipPwd) {
        this.voipPwd = voipPwd;
    }

    @Override
    public String toString() {
        return "VideoAccount{" +
                "id=" + id +
                ", doctorFriendlyName='" + doctorFriendlyName + '\'' +
                ", doctorSubAccountSid='" + doctorSubAccountSid + '\'' +
                ", doctorSubToken='" + doctorSubToken + '\'' +
                ", doctorVoipAccount='" + doctorVoipAccount + '\'' +
                ", doctorVoipPwd='" + doctorVoipPwd + '\'' +
                ", friendlyName='" + friendlyName + '\'' +
                ", subAccountSid='" + subAccountSid + '\'' +
                ", subToken='" + subToken + '\'' +
                ", voipAccount='" + voipAccount + '\'' +
                ", voipPwd='" + voipPwd + '\'' +
                '}';
    }
}
