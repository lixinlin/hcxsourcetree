package com.hxlm.hcyandroid.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 订单  获取我的讲座接口中包含的数据
 *
 * @author Administrator
 */
public class OrderItem implements Serializable {
    private int id;
    private long createDate;
    private long modifyDate;
    private String sn;// 订单号
    private String orderStatus;// 订单状态
    private String paymentStatus;// 付款状态
    private String shippingStatus;
    private double fee;
    private double freight;
    private double totalAmount;
    private double amountPaid;// 已付款金额
    private double refundPay;//已退款金额
    private double amountPayable;// 剩余付款金额
    private int point;
    private String consignee;
    private String areaName;
    private String address;
    private String zipCode;
    private String phone;
    private String memo;
    private String paymentMethodName;
    private String shippingMethodName;
    private FamilyMember member;
    private List<OrderItems> orderItems;
    private String name;


    public double getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(double amountPaid) {
        this.amountPaid = amountPaid;
    }

    public double getRefundPay() {
        return refundPay;
    }

    public void setRefundPay(double refundPay) {
        this.refundPay = refundPay;
    }

    public double getAmountPayable() {
        return amountPayable;
    }

    public void setAmountPayable(double amountPayable) {
        this.amountPayable = amountPayable;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(long modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getShippingStatus() {
        return shippingStatus;
    }

    public void setShippingStatus(String shippingStatus) {
        this.shippingStatus = shippingStatus;
    }

    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public double getFreight() {
        return freight;
    }

    public void setFreight(double freight) {
        this.freight = freight;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public String getPaymentMethodName() {
        return paymentMethodName;
    }

    public void setPaymentMethodName(String paymentMethodName) {
        this.paymentMethodName = paymentMethodName;
    }

    public String getShippingMethodName() {
        return shippingMethodName;
    }

    public void setShippingMethodName(String shippingMethodName) {
        this.shippingMethodName = shippingMethodName;
    }

    public FamilyMember getMember() {
        return member;
    }

    public void setMember(FamilyMember member) {
        this.member = member;
    }

    public List<OrderItems> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItems> orderItems) {
        this.orderItems = orderItems;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "OrderItem [id=" + id + ", createDate=" + createDate
                + ", modifyDate=" + modifyDate + ", sn=" + sn
                + ", orderStatus=" + orderStatus + ", paymentStatus="
                + paymentStatus + ", shippingStatus=" + shippingStatus
                + ", fee=" + fee + ", freight=" + freight + ", totalAmount="
                + totalAmount + ", amountPaid=" + amountPaid + ", refundPay="
                + refundPay + ", amountPayable=" + amountPayable + ", point="
                + point + ", consignee=" + consignee + ", areaName=" + areaName
                + ", address=" + address + ", zipCode=" + zipCode + ", phone="
                + phone + ", memo=" + memo + ", paymentMethodName="
                + paymentMethodName + ", shippingMethodName="
                + shippingMethodName + ", member=" + member + ", orderItems="
                + orderItems + ", name=" + name + "]";
    }


}
