package com.hxlm.hcyandroid.bean;

public class ReserveSn {
    private long createDate;
    private int id;
    private long modifyDate;
    private String reserveSn;

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(long modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getReserveSn() {
        return reserveSn;
    }

    public void setReserveSn(String reserveSn) {
        this.reserveSn = reserveSn;
    }

}
