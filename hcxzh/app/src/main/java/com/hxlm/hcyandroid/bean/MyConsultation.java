package com.hxlm.hcyandroid.bean;

import java.io.Serializable;
import java.util.List;

public class MyConsultation implements Serializable {
    private int id;
    private long createDate;
    private long modifyDate;
    private String content;
    private ReplyUserConsultation replyUserConsultations;
    private List<String> userConsultationImages;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(long modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ReplyUserConsultation getReplyUserConsultations() {
        return replyUserConsultations;
    }

    public void setReplyUserConsultations(
            ReplyUserConsultation replyUserConsultations) {
        this.replyUserConsultations = replyUserConsultations;
    }

    public List<String> getUserConsultationImages() {
        return userConsultationImages;
    }

    public void setUserConsultationImages(List<String> userConsultationImages) {
        this.userConsultationImages = userConsultationImages;
    }

}
