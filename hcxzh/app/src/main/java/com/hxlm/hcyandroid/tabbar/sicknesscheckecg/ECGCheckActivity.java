package com.hxlm.hcyandroid.tabbar.sicknesscheckecg;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.hcy.ky3h.R;
import com.hxlm.android.comm.AbstractDeviceActivity;
import com.hxlm.android.comm.AbstractIOSession;
import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.comm.Error;
import com.hxlm.android.comm.Error_English;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.report.UploadManager;
import com.hxlm.android.hcy.user.ChildMember;
import com.hxlm.android.hcy.user.ChooseMemberDialog;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;
import com.hxlm.android.health.device.message.ecg.EcgDataMessage;
import com.hxlm.android.health.device.message.ecg.EcgWaveOutputCommand;
import com.hxlm.android.health.device.message.ecg.EcgWaveQueueMessage;
import com.hxlm.android.health.device.model.ChargePal;
import com.hxlm.android.health.device.model.HcyPhone;
import com.hxlm.android.health.device.view.ECGDrawWaveManager;
import com.hxlm.android.utils.Logger;
import com.hxlm.hcyandroid.CheckedDataType;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyphone.MainActivity;
import com.hxlm.hcyandroid.ResponseStatus;
import com.hxlm.hcyandroid.bean.ECGData;
import com.hxlm.hcyandroid.tabbar.MyHealthFileBroadcast;
import com.hxlm.hcyandroid.tabbar.sicknesscheck.ECGUseSpecificationkActivity;
import com.hxlm.hcyandroid.util.ToastUtil;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 心电心率检测
 *
 * @author l
 */
public class ECGCheckActivity extends AbstractDeviceActivity implements OnClickListener {
    private final static String TAG = "ECGCheckActivity";
    private final static int REMAIN_SECONDS_COUNT = 60;

    private HcyPhone hcyPhone;
    private ECGDrawWaveManager ecgDrawWaveManager;
    private SurfaceView ecgSurfaceView;
    private ImageView iv_ecg_connect_status;
    private TextView tv_ecg_connect_status;

    private TextView tv_start_check;// 开始检测
    private LinearLayout linear_checkecg;//显示心电数据
    private TextView tv_ecg_data;// 当前心率
    private ImageView iv_restart_check;// 重新检测

    private ArrayList<Integer> list = new ArrayList<>();

    private int averageHeartRate = 0;// 平均心率
    private UploadManager uploadManager;
    private String subjectSn;

    // 倒计时
    private LinearLayout linear_jishi;// 开始计时
    private TextView tv_time_miao;//

    private TextView tv_time_end;// 计时结束
    private Timer timer;
    // 计时器
    private TimerTask timerTask;
    private int remainSeconds;// 剩余秒数

    private File ecgDataFile;

    private long createDate=0;//用户测试时间戳

    private List<UploadFailedData> listFailes=new ArrayList<>();//保存上传失败数据
    private ECGUploadFaileManager faileManager;//保存失败心电数据的Manager

    private Handler timerHandler = new Handler(new Handler.Callback() {
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case -10:
                    remainSeconds--;
                    Log.i(TAG, remainSeconds + "");
                    tv_time_miao.setText(String.valueOf(remainSeconds));
                    if (remainSeconds <= 0) {
                        stopTimer();
                        // 计时结束
                        linear_jishi.setVisibility(View.GONE);
                        tv_time_end.setVisibility(View.VISIBLE);

                        //计时结束之后，提交数据
                        submitECG();
                    }
                    break;
            }
            return false;
        }
    });

    @Override
    public void setContentView() {
        //保持背光常亮的设置方法
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_ecg_check);

        faileManager=new ECGUploadFaileManager();

        hcyPhone = new HcyPhone();

        if (hcyPhone.getPowerStatus() != HcyPhone.PowerStatus.UNSUPPORT) {
            try {
                hcyPhone.setPowerStatus(HcyPhone.PowerStatus.ON);
                ioSession = hcyPhone.getIOSession(this);

            } catch (IOException e) {
                ioSession = new ChargePal().getIOSession(this);
                onExceptionCaught(e);
            }
        } else {
            ioSession = new ChargePal().getIOSession(this);
        }
    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.ecg_check_title), titleBar, 1);

        ecgSurfaceView = (SurfaceView) findViewById(R.id.sv_ecg);
        ecgSurfaceView.setZOrderOnTop(true);

        iv_ecg_connect_status = (ImageView) findViewById(R.id.iv_ecg_connect_status);
        tv_ecg_connect_status = (TextView) findViewById(R.id.tv_ecg_connect_status);

        tv_start_check = (TextView) findViewById(R.id.tv_start_check);
        linear_checkecg = (LinearLayout) findViewById(R.id.linear_checkecg);
        tv_ecg_data = (TextView) findViewById(R.id.tv_ecg_data);
        iv_restart_check = (ImageView) findViewById(R.id.iv_restart_check_ecg);

        ImageView iv_use_specification = (ImageView) findViewById(R.id.iv_use_specification);
        iv_use_specification.setOnClickListener(this);

        //倒计时
        linear_jishi = (LinearLayout) findViewById(R.id.linear_jishi);
        tv_time_miao = (TextView) findViewById(R.id.tv_time_miao);
        tv_time_end = (TextView) findViewById(R.id.tv_time_end);

        //tv_time_miao.setText(String.valueOf(REMAIN_SECONDS_COUNT));

        tv_start_check.setOnClickListener(this);//开始检测
        iv_restart_check.setOnClickListener(this);//倒计时
    }

    // 开始计时
    private void startTimer() {
        remainSeconds = REMAIN_SECONDS_COUNT;

        if (timer == null) {
            timer = new Timer();
        }

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                timerHandler.sendEmptyMessage(-10);
            }
        }, 1000, 1000);
    }

    // 停止计时
    private void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }

        if (timerTask != null) {
            timerTask.cancel();
            timerTask = null;
        }

        remainSeconds = 0;
    }

    @Override
    public void initDatas() {
        uploadManager = new UploadManager();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_start_check:

                if (ioSession == null) {
                    //ToastUtil.invokeShortTimeToast(ECGCheckActivity.this, "心电监测设备未找到！");
                    iv_ecg_connect_status.setImageResource(R.drawable.ecg_not_connect);
                    tv_ecg_connect_status.setText(getString(R.string.ECGCheckActivity_weijieru));
                } else {
                    createDate=System.currentTimeMillis();

                    if (ioSession.status != AbstractIOSession.Status.CONNECTED) {
                        iv_ecg_connect_status.setImageResource(R.drawable.ecg_not_connect);
                        tv_ecg_connect_status.setText(getString(R.string.ECGCheckActivity_lianjiezhong));
                       // ToastUtil.invokeShortTimeToast(this, "开始连接心电监测设备......");
                        ioSession.connect();
                    }
                }
                break;
            //重新检测
            case R.id.iv_restart_check_ecg:
                ioSession.sendMessage(new EcgWaveOutputCommand(true));

                createDate=System.currentTimeMillis();

                if (!ecgDrawWaveManager.isRunning()) {
                    ecgDrawWaveManager.startDraw();
                }
                tv_ecg_data.setText("");

                linear_checkecg.setVisibility(View.VISIBLE);
                tv_start_check.setVisibility(View.GONE);

                list.clear();

                // 当前的倒计时没有结束，又点击重新检测，先将当前的time设置为0
                if (remainSeconds > 0) {
                    // 先停止计时
                    stopTimer();
                }

                tv_time_miao.setText(String.valueOf(REMAIN_SECONDS_COUNT));
                // 在重新计时
                startTimer();

                tv_time_end.setVisibility(View.GONE);
                linear_jishi.setVisibility(View.VISIBLE);
                break;
            // 使用规范
            case R.id.iv_use_specification:
                Intent intent = new Intent(ECGCheckActivity.this, ECGUseSpecificationkActivity.class);
                startActivity(intent);

                break;
            default:
                break;
        }
    }

    //计时结束，提交数据
    private void submitECG() {
        //心电数据是否为空
        if (ecgDrawWaveManager == null) {
            ToastUtil.invokeShortTimeToast(this, getString(R.string.ECGCheckActivity_jiancewanzhi));
        } else {
            ioSession.sendMessage(new EcgWaveOutputCommand(false));

            int sum = 0;
            // 遍历求和
            for (Integer aList : list) {
                sum += aList;
                averageHeartRate = sum / list.size();
            }

            if (averageHeartRate < 60) {
                subjectSn = "XLGH";//心率过缓
            } else if (averageHeartRate > 100) {
                subjectSn = "XLGS";//心率过速
            } else {
                subjectSn = "XLZC";//心率正常
            }

            if (ecgDrawWaveManager.isRecordOn()) {
                ecgDataFile = new File(ecgDrawWaveManager.getDataFileName());

                //首先就保存到本地
                Logger.i("ECGReviewActivity","检测完先保存在本地File-->"+ecgDataFile.getAbsolutePath());


                if (!ecgDataFile.exists() || list.size() == 0) {
                    ToastUtil.invokeShortTimeToast(this, getString(R.string.ECGCheckActivity_jianceshuju));

                } else {
                    //先删除心电图界面
                    ecgDrawWaveManager.stopDraw();//停止线程

                    LoginControllor.requestLogin(ECGCheckActivity.this, new OnCompleteListener() {
                        @Override
                        public void onComplete() {
                            List<ChildMember> childMembers = LoginControllor.getLoginMember().getMengberchild();
                            int size=childMembers.size();
                            if(true){
//上传文件
                                uploadManager.uploadFile(ecgDataFile, "file", new AbstractDefaultHttpHandlerCallback(ECGCheckActivity.this) {
                                    @Override
                                    protected void onResponseSuccess(Object obj) {
                                        final String path = (String) obj;
                                        if(Constant.DEBUG)
                                        {
                                            Log.i("ECGReviewActivity","上传的path-->"+path);
                                        }

                                        //path去除http://
                                        int indexdelete=path.indexOf("/");
                                        String strDeletePath=path.substring(indexdelete+2);

                                        if(Constant.DEBUG) {
                                            Log.i("ECGReviewActivity", "去除http:的path-->" + strDeletePath);
                                        }

                                        //替换保存在本地的文件名
                                        ECGRenameToFile ecgRenameToFile=new ECGRenameToFile();
                                        ecgRenameToFile.renameToFile(ecgDataFile,path);

                                        // 输入正常值才上传数据
                                        uploadManager.uploadCheckedData(CheckedDataType.ECG,
                                                new ECGData(strDeletePath, averageHeartRate, subjectSn),
                                                createDate,
                                                new AbstractDefaultHttpHandlerCallback(ECGCheckActivity.this) {
                                                    @Override
                                                    protected void onResponseSuccess(Object obj) {
                                                        new ECGWriteDialog(ECGCheckActivity.this,
                                                                getString(R.string.ECGCheckActivity_nindangqianping) + averageHeartRate).show();
                                                        list.clear();
                                                    }

                                                    //上传文件成功 但上传数据失败 返回错误数据 状态2
                                                    @Override
                                                    protected void onResponseError(int errorCode, String errorDesc) {
                                                        super.onResponseError(errorCode, errorDesc);

                                                        //不是未登录导致的
                                                        if(errorCode != ResponseStatus.UNLOGIN)
                                                        {
                                                            //保存未上传数据
                                                            saveStatus2(2, LoginControllor.getLoginMember().getId(), LoginControllor.getChoosedChildMember().getId(),
                                                                    CheckedDataType.ECG,new ECGData(path,averageHeartRate,subjectSn),
                                                                    createDate);


                                                        }
                                                        if(Constant.DEBUG)
                                                            Log.i("ECGReviewActivity","onResponseError状态2--");
                                                    }

                                                    //上传文件成功 但上传数据失败  服务器错误 状态2
                                                    @Override
                                                    protected void onHttpError(int httpErrorCode) {
                                                        super.onHttpError(httpErrorCode);
                                                        saveStatus2(2, LoginControllor.getLoginMember().getId(), LoginControllor.getChoosedChildMember().getId(),
                                                                CheckedDataType.ECG,new ECGData(path,averageHeartRate,subjectSn),
                                                                createDate);
                                                        if(Constant.DEBUG)
                                                            Log.i("ECGReviewActivity","onHttpError状态2--");
                                                    }

                                                    //上传文件成功 但上传数据失败 网络没连接 状态2
                                                    @Override
                                                    protected void onNetworkError() {
                                                        super.onNetworkError();
                                                        saveStatus2(2, LoginControllor.getLoginMember().getId(), LoginControllor.getChoosedChildMember().getId(),
                                                                CheckedDataType.ECG,new ECGData(path,averageHeartRate,subjectSn),
                                                                createDate);

                                                        if(Constant.DEBUG)
                                                            Log.i("ECGReviewActivity","onNetworkError状态2--");
                                                    }
                                                });
                                    }
                                    //上传文件失败 返回错误数据 状态1
                                    @Override
                                    protected void onResponseError(int errorCode, String errorDesc) {
                                        super.onResponseError(errorCode, errorDesc);
                                        //不是未登录导致的
                                        if(errorCode != ResponseStatus.UNLOGIN)
                                        {

                                            //保存未上传数据
                                            saveStatus1(1,ecgDataFile, LoginControllor.getLoginMember().getId(),
                                                    LoginControllor.getChoosedChildMember().getId(), CheckedDataType.ECG,
                                                    new ECGData(null,averageHeartRate,subjectSn),createDate
                                            );

                                        }
                                        if(Constant.DEBUG)
                                            Log.i("ECGReviewActivity","onResponseError状态1--");
                                    }

                                    //上传文件失败 服务器错误 状态1
                                    @Override
                                    protected void onHttpError(int httpErrorCode) {
                                        super.onHttpError(httpErrorCode);
                                        //保存未上传数据
                                        saveStatus1(1,ecgDataFile, LoginControllor.getLoginMember().getId(),
                                                LoginControllor.getChoosedChildMember().getId(), CheckedDataType.ECG,
                                                new ECGData(null,averageHeartRate,subjectSn),createDate
                                        );
                                        if(Constant.DEBUG)
                                            Log.i("ECGReviewActivity","onHttpError状态1--");
                                    }

                                    //上传文件失败 网络没连接 状态1
                                    @Override
                                    protected void onNetworkError() {
                                        super.onNetworkError();
                                        saveStatus1(1,ecgDataFile, LoginControllor.getLoginMember().getId(),
                                                LoginControllor.getChoosedChildMember().getId(), CheckedDataType.ECG,
                                                new ECGData(null,averageHeartRate,subjectSn),createDate
                                        );
                                        if(Constant.DEBUG)
                                            Log.i("ECGReviewActivity","onNetworkError状态1--");
                                    }
                                });
                            }else {
                                new ChooseMemberDialog(ECGCheckActivity.this, new OnCompleteListener() {
                                    @Override
                                    public void onComplete() {
                                        //上传文件
                                        uploadManager.uploadFile(ecgDataFile, "file", new AbstractDefaultHttpHandlerCallback(ECGCheckActivity.this) {
                                            @Override
                                            protected void onResponseSuccess(Object obj) {
                                                final String path = (String) obj;
                                                if(Constant.DEBUG)
                                                {
                                                    Log.i("ECGReviewActivity","上传的path-->"+path);
                                                }

                                                //path去除http://
                                                int indexdelete=path.indexOf("/");
                                                String strDeletePath=path.substring(indexdelete+2);

                                                if(Constant.DEBUG) {
                                                    Log.i("ECGReviewActivity", "去除http:的path-->" + strDeletePath);
                                                }

                                                //替换保存在本地的文件名
                                                ECGRenameToFile ecgRenameToFile=new ECGRenameToFile();
                                                ecgRenameToFile.renameToFile(ecgDataFile,path);

                                                // 输入正常值才上传数据
                                                uploadManager.uploadCheckedData(CheckedDataType.ECG,
                                                        new ECGData(strDeletePath, averageHeartRate, subjectSn),
                                                        createDate,
                                                        new AbstractDefaultHttpHandlerCallback(ECGCheckActivity.this) {
                                                            @Override
                                                            protected void onResponseSuccess(Object obj) {
                                                                new ECGWriteDialog(ECGCheckActivity.this,
                                                                        getString(R.string.ECGCheckActivity_nindangqianping) + averageHeartRate).show();
                                                                list.clear();
                                                            }

                                                            //上传文件成功 但上传数据失败 返回错误数据 状态2
                                                            @Override
                                                            protected void onResponseError(int errorCode, String errorDesc) {
                                                                super.onResponseError(errorCode, errorDesc);

                                                                //不是未登录导致的
                                                                if(errorCode != ResponseStatus.UNLOGIN)
                                                                {
                                                                    //保存未上传数据
                                                                    saveStatus2(2, LoginControllor.getLoginMember().getId(), LoginControllor.getChoosedChildMember().getId(),
                                                                            CheckedDataType.ECG,new ECGData(path,averageHeartRate,subjectSn),
                                                                            createDate);


                                                                }
                                                                if(Constant.DEBUG)
                                                                    Log.i("ECGReviewActivity","onResponseError状态2--");
                                                            }

                                                            //上传文件成功 但上传数据失败  服务器错误 状态2
                                                            @Override
                                                            protected void onHttpError(int httpErrorCode) {
                                                                super.onHttpError(httpErrorCode);
                                                                saveStatus2(2, LoginControllor.getLoginMember().getId(), LoginControllor.getChoosedChildMember().getId(),
                                                                        CheckedDataType.ECG,new ECGData(path,averageHeartRate,subjectSn),
                                                                        createDate);
                                                                if(Constant.DEBUG)
                                                                    Log.i("ECGReviewActivity","onHttpError状态2--");
                                                            }

                                                            //上传文件成功 但上传数据失败 网络没连接 状态2
                                                            @Override
                                                            protected void onNetworkError() {
                                                                super.onNetworkError();
                                                                saveStatus2(2, LoginControllor.getLoginMember().getId(), LoginControllor.getChoosedChildMember().getId(),
                                                                        CheckedDataType.ECG,new ECGData(path,averageHeartRate,subjectSn),
                                                                        createDate);

                                                                if(Constant.DEBUG)
                                                                    Log.i("ECGReviewActivity","onNetworkError状态2--");
                                                            }
                                                        });
                                            }
                                            //上传文件失败 返回错误数据 状态1
                                            @Override
                                            protected void onResponseError(int errorCode, String errorDesc) {
                                                super.onResponseError(errorCode, errorDesc);
                                                //不是未登录导致的
                                                if(errorCode != ResponseStatus.UNLOGIN)
                                                {

                                                    //保存未上传数据
                                                    saveStatus1(1,ecgDataFile, LoginControllor.getLoginMember().getId(),
                                                            LoginControllor.getChoosedChildMember().getId(), CheckedDataType.ECG,
                                                            new ECGData(null,averageHeartRate,subjectSn),createDate
                                                    );

                                                }
                                                if(Constant.DEBUG)
                                                    Log.i("ECGReviewActivity","onResponseError状态1--");
                                            }

                                            //上传文件失败 服务器错误 状态1
                                            @Override
                                            protected void onHttpError(int httpErrorCode) {
                                                super.onHttpError(httpErrorCode);
                                                //保存未上传数据
                                                saveStatus1(1,ecgDataFile, LoginControllor.getLoginMember().getId(),
                                                        LoginControllor.getChoosedChildMember().getId(), CheckedDataType.ECG,
                                                        new ECGData(null,averageHeartRate,subjectSn),createDate
                                                );
                                                if(Constant.DEBUG)
                                                    Log.i("ECGReviewActivity","onHttpError状态1--");
                                            }

                                            //上传文件失败 网络没连接 状态1
                                            @Override
                                            protected void onNetworkError() {
                                                super.onNetworkError();
                                                saveStatus1(1,ecgDataFile, LoginControllor.getLoginMember().getId(),
                                                        LoginControllor.getChoosedChildMember().getId(), CheckedDataType.ECG,
                                                        new ECGData(null,averageHeartRate,subjectSn),createDate
                                                );
                                                if(Constant.DEBUG)
                                                    Log.i("ECGReviewActivity","onNetworkError状态1--");
                                            }
                                        });

                                    }
                                }).show();
                            }

                        }
                    });
                }
            }
        }
    }

    //保存状态1数据
    private void saveStatus1(int status, File file, int MemberId, int MemberChildId, int Datatype, ECGData ecgData, long acreateDate)
    {
        //保存未上传数据
        UploadFailedData uploadFailedData=new UploadFailedData();
        uploadFailedData.setStatus(status);
        uploadFailedData.setFile(file);
        uploadFailedData.setMemberId(MemberId);
        uploadFailedData.setMemberChildId(MemberChildId);
        uploadFailedData.setDatatype(Datatype);
        uploadFailedData.setEcgData(ecgData);
        uploadFailedData.setCreateDate(acreateDate);
        listFailes.add(uploadFailedData);

        faileManager.saveECGFaileDataToSp(listFailes);
    }
    //保存状态2数据
    private void saveStatus2(int status, int MemberId, int MemberChildId, int Datatype, ECGData ecgData, long acreateDate)
    {
        UploadFailedData uploadFailedData=new UploadFailedData();
        uploadFailedData.setStatus(status);
        uploadFailedData.setMemberId(MemberId);
        uploadFailedData.setMemberChildId(MemberChildId);
        uploadFailedData.setDatatype(Datatype);
        uploadFailedData.setEcgData(ecgData);
        uploadFailedData.setCreateDate(acreateDate);
        listFailes.add(uploadFailedData);

        faileManager.saveECGFaileDataToSp(listFailes);
    }

    @Override
    protected void onConnected() {
        //ToastUtil.invokeShortTimeToast(this, "设备已连接");

        iv_ecg_connect_status.setImageResource(R.drawable.ecg_connect);
        tv_ecg_connect_status.setText(getString(R.string.ECGCheckActivity_yijieru));

        //连接之后开始检测心电
        linear_checkecg.setVisibility(View.VISIBLE);
        tv_start_check.setVisibility(View.GONE);
        iv_restart_check.setVisibility(View.VISIBLE);
        tv_time_end.setVisibility(View.GONE);
        linear_jishi.setVisibility(View.VISIBLE);

        // 开始计时
        startTimer();
    }

    @Override
    protected void onConnectFailed(Error error) {
        if (hcyPhone.getPowerStatus() == HcyPhone.PowerStatus.ON) {
            try {
                hcyPhone.setPowerStatus(HcyPhone.PowerStatus.OFF);
            } catch (IOException e) {
                onExceptionCaught(e);
            }

            Toast.makeText(ECGCheckActivity.this, error.getDesc(), Toast.LENGTH_SHORT).show();

            ioSession = new ChargePal().getIOSession(this);
            ioSession.connect();
        } else {
            Toast.makeText(ECGCheckActivity.this, error.getDesc(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onConnectFailedEnglist(Error_English error_english) {
        if (hcyPhone.getPowerStatus() == HcyPhone.PowerStatus.ON) {
            try {
                hcyPhone.setPowerStatus(HcyPhone.PowerStatus.OFF);
            } catch (IOException e) {
                onExceptionCaught(e);
            }

            Toast.makeText(ECGCheckActivity.this, error_english.getDesc(), Toast.LENGTH_SHORT).show();

            ioSession = new ChargePal().getIOSession(this);
            ioSession.connect();
        } else {
            Toast.makeText(ECGCheckActivity.this, error_english.getDesc(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDisconnected() {
       // ToastUtil.invokeShortTimeToast(ECGCheckActivity.this, "设备已断开");
        iv_ecg_connect_status.setImageResource(R.drawable.ecg_not_connect);
        tv_ecg_connect_status.setText(getString(R.string.ECGCheckActivity_weijieru));

        //未连接显示
        linear_checkecg.setVisibility(View.GONE);
        tv_start_check.setVisibility(View.VISIBLE);
        iv_restart_check.setVisibility(View.GONE);

        if (timer != null) {
            //关闭计时器
            stopTimer();
        }
        linear_jishi.setVisibility(View.GONE);
        tv_time_end.setVisibility(View.VISIBLE);


        if (ecgDrawWaveManager != null) {
            ecgDrawWaveManager.stopDraw();
            ecgDrawWaveManager = null;
        }

        try {
            hcyPhone.setPowerStatus(HcyPhone.PowerStatus.OFF);
        } catch (IOException e) {
            onExceptionCaught(e);
        }
    }

    @Override
    protected void onExceptionCaught(Throwable e) {
        Log.e(TAG, e.getMessage());
    }

    @Override
    protected void onMessageReceived(AbstractMessage message) {
        switch ((HealthDeviceMessageType) message.getMessageType()) {
            case ECG_WAVE:
                if (ioSession.status == AbstractIOSession.Status.CONNECTED) {
                    EcgWaveQueueMessage ecgWaveQueueMessage = (EcgWaveQueueMessage) message;

                    try {
                        ecgDrawWaveManager = new ECGDrawWaveManager(ecgSurfaceView, ecgWaveQueueMessage,
                                Constant.BASE_PATH);
                    } catch (IOException e) {
                        onExceptionCaught(e);
                    }
                    if (ecgDrawWaveManager != null) {
                        ecgDrawWaveManager.setUsingFilter(false);
                        ecgDrawWaveManager.startDraw();
                    }
                }
                break;

            case ECG_DATA:
                EcgDataMessage ecgDataMessage = (EcgDataMessage) message;
                if (ecgDataMessage.getConnection() == 1) {
                    Toast.makeText(ECGCheckActivity.this, getString(R.string.ECGCheckActivity_daolianxianlu), Toast.LENGTH_SHORT).show();
                }
                if (ecgDataMessage.getSignalQuality() == 1) {
                    Toast.makeText(ECGCheckActivity.this, getString(R.string.ECGCheckActivity_xindianxinhao), Toast.LENGTH_SHORT).show();
                }
                if (ecgDataMessage.getHeartRate() > 20) {
                    list.add(ecgDataMessage.getHeartRate());
                }
                if (tv_ecg_data.getVisibility() == View.VISIBLE) {
                    tv_ecg_data.setText(String.valueOf(ecgDataMessage.getHeartRate()));
                }
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            hcyPhone.setPowerStatus(HcyPhone.PowerStatus.OFF);
        } catch (IOException e) {
            onExceptionCaught(e);
        }
    }

    private class ECGWriteDialog extends AlertDialog implements
            OnClickListener {

        Context context;
        String tishi;

        ECGWriteDialog(Context context, String tishi) {
            super(context);
            this.context = context;
            this.tishi = tishi;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.ecgwrite_submit_prompt);

            TextView tv_breath_prompt_tishi = (TextView) findViewById(R.id.tv_breath_prompt_tishi);
            tv_breath_prompt_tishi.setText(tishi);


            // 返回检测
            TextView text_back = (TextView) findViewById(R.id.text_back);
            text_back.setOnClickListener(this);

            //查看档案
            TextView text_see_dangan = (TextView) findViewById(R.id.text_see_dangan);
            text_see_dangan.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                // 返回检测
                case R.id.text_back:
                    this.dismiss();
                    break;

                //查看档案
                case R.id.text_see_dangan:
                    this.dismiss();
                    // 动态注册广播使用隐士Intent
                    Intent intent = new Intent(MyHealthFileBroadcast.ACTION);
                    intent.putExtra("ArchivesFragment", "3");
                    intent.putExtra("otherReport", "true");
                    intent.putExtra("Jump", 5);
                    ECGCheckActivity.this.sendBroadcast(intent);

                    Intent intent2 = new Intent(ECGCheckActivity.this, MainActivity.class);

                    intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//清除MainActivity之前所有的activity
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP); //沿用之前的MainActivity
                    startActivity(intent2);
                    break;
            }
        }
    }

//    //替换文件名
//    private void renameToFile(String pathFile)
//    {
//        //-----------------获取保存在本地的文件名与上传之后返回的文件名，进行替换
//        //保存在本地的文件名
//        String filePath=ecgDataFile.getAbsolutePath();//保存路径
//        int intPath=filePath.lastIndexOf("/");
//        //获取保存的文件名
//        String strFileName=filePath.substring(intPath+1);
//        Log.i("ECGReviewActivity","保存在本地的文件名-->"+strFileName);
//
//        //得到上传之后返回的文件名
//        int intPath2=pathFile.lastIndexOf("/");
//        String strPathName=pathFile.substring(intPath2+1);
//        Log.i("ECGReviewActivity","上传之后返回的文件名-->"+strPathName);
//
//        //保存的文件路径
//        String strToRename=Constant.BASE_PATH+ECGDrawWaveManager.ECG_FILE_PATH+"/"+strPathName;
//        //替换文件名
//        File fileToRename=new File(strToRename);
//        if(ecgDataFile.exists()&&!fileToRename.exists())
//        {
//            //进行替换
//            if(ecgDataFile.renameTo(fileToRename))
//            {
//                Log.i("ECGReviewActivity","修改成功");
//            }else
//            {
//                Log.i("ECGReviewActivity","修改失败");
//            }
//        }
//    }
}
