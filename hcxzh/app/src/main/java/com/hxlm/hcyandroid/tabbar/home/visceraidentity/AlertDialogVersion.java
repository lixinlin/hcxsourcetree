package com.hxlm.hcyandroid.tabbar.home.visceraidentity;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hcy.ky3h.R;

/**
 * 脏腑选择症状或是疾病超出5项给出提示
 *
 * @author l
 */
public class AlertDialogVersion extends Dialog {

    private View.OnClickListener l;
    private Context context;

    public AlertDialogVersion(Context context) {
        super(context);
        this.context = context;
    }

    public Dialog createAlartDialog(String titletxt, String msg) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.alert_version, null);
        final Dialog dialog = new Dialog(context, R.style.alertdialog);
        dialog.setCancelable(true);
        dialog.setContentView(v);
        TextView tvtext = (TextView) v.findViewById(R.id.tv_text);
        tvtext.setText(titletxt);
        TextView tvcloce = v.findViewById(R.id.tv_sure);
        if (l == null) {
            tvcloce.setOnClickListener(defaultLinstener(dialog));
        } else {
            tvcloce.setOnClickListener(l);
        }
        return dialog;
    }

    public void setDetermineOnClickListener(
            View.OnClickListener l) {
        this.l = l;
    }

    private View.OnClickListener defaultLinstener(
            final Dialog dialog) {
        View.OnClickListener listener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    // 点击确定
                    case R.id.tv_sure:
                        dialog.dismiss();
                        break;
                    default:
                        break;
                }
            }
        };
        return listener;
    }
}