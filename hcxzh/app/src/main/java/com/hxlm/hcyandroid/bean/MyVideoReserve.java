package com.hxlm.hcyandroid.bean;

public class MyVideoReserve {
    private int id;
    private String status;
    private double actualPayment;
    private MyReserveDoctorSchedule doctorSchedule;
    //private MyVideoReserveDoctor doctor;
    private ReserveDoctor doctor;
    private OrderItem orderItem;


    public ReserveDoctor getDoctor() {
        return doctor;
    }

    public void setDoctor(ReserveDoctor doctor) {
        this.doctor = doctor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getActualPayment() {
        return actualPayment;
    }

    public void setActualPayment(double actualPayment) {
        this.actualPayment = actualPayment;
    }

    public MyReserveDoctorSchedule getDoctorSchedule() {
        return doctorSchedule;
    }

    public void setDoctorSchedule(MyReserveDoctorSchedule doctorSchedule) {
        this.doctorSchedule = doctorSchedule;
    }


    public OrderItem getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(OrderItem orderItem) {
        this.orderItem = orderItem;
    }

}
