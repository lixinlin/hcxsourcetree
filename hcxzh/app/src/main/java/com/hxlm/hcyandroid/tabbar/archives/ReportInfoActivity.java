package com.hxlm.hcyandroid.tabbar.archives;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.util.WebViewUtil;

public class ReportInfoActivity extends BaseActivity {

    private ProgressBar progressBar;
    private WebView wv_content;
    private String subject_sn;
    private int memberChildId;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_meridian_report);
    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        String title = extras.getString("title");
        titleBar.init(ReportInfoActivity.this, title, titleBar, 1);
        titleBar.setIv_familiesUnable();
    }

    //初始化界面
    public void initview() {
        wv_content = (WebView) findViewById(R.id.wv_content);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);
    }

    @Override
    public void initDatas() {
        initview();
        Intent intent = getIntent();
        String strUrl = intent.getStringExtra("strurl4");
        subject_sn = intent.getStringExtra("subjectId");
        memberChildId = LoginControllor.getChoosedChildMember().getId();
        if (TextUtils.isEmpty(strUrl)) {
            strUrl = "/member/service/reshow.jhtml?sn=" + subject_sn + "&device=1";
        }
        new WebViewUtil().setWebViewInit(wv_content, progressBar, this, Constant.BASE_URL+strUrl);

    }
}
