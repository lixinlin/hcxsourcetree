package com.hxlm.hcyandroid.bean;

import android.support.annotation.NonNull;

/**
 * Created by lipengcheng on 2017/12/4.
 */

public class Task{
    private int id;
    private long createDate;
    private long modifyDate;
    private String advice;
    private String type;
    private String action;
    private Subject subject;
    private boolean isDone;

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }
    public void setIsDone(boolean isDone){
        this.isDone = isDone;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public void setModifyDate(long modifyDate) {
        this.modifyDate = modifyDate;
    }

    public void setAdvice(String advice) {
        this.advice = advice;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public int getId() {
        return id;
    }

    public long getCreateDate() {
        return createDate;
    }

    public long getModifyDate() {
        return modifyDate;
    }

    public String getAdvice() {
        return advice;
    }

    public String getType() {
        return type;
    }

    public String getAction() {
        return action;
    }

    public Subject getSubject() {
        return subject;
    }



    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", createDate=" + createDate +
                ", modifyDate=" + modifyDate +
                ", advice='" + advice + '\'' +
                ", type='" + type + '\'' +
                ", action='" + action + '\'' +
                ", subject=" + subject +

                ", isDone=" + isDone +
                '}';
    }
}
