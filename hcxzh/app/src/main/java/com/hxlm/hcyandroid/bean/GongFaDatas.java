package com.hxlm.hcyandroid.bean;

import android.content.Context;

import com.hcy.ky3h.R;
import com.hxlm.hcyandroid.BaseApplication;
import com.hxlm.hcyandroid.Constant;


public class GongFaDatas {

    public static final String[] GONGFA_GROUP_NAME = {
            BaseApplication.getContext().getString(R.string.gongfa_all),
            BaseApplication.getContext().getString(R.string.gongfa_ready),
            BaseApplication.getContext().getString(R.string.gongfa_type_1),
            BaseApplication.getContext().getString(R.string.gongfa_type_2),
            BaseApplication.getContext().getString(R.string.gongfa_type_3),
            BaseApplication.getContext().getString(R.string.gongfa_type_4),
            BaseApplication.getContext().getString(R.string.gongfa_type_5),
            BaseApplication.getContext().getString(R.string.gongfa_type_6),
            BaseApplication.getContext().getString(R.string.gongfa_type_7),
            BaseApplication.getContext().getString(R.string.gongfa_type_8)
    };
    public static final String[] mGongFaTypeIcons = {
            BaseApplication.getContext().getString(R.string.gongfa_all),
            BaseApplication.getContext().getString(R.string.gongfa_ready),
            BaseApplication.getContext().getString(R.string.gongfa_type1),
            BaseApplication.getContext().getString(R.string.gongfa_type2),
            BaseApplication.getContext().getString(R.string.gongfa_type3),
            BaseApplication.getContext().getString(R.string.gongfa_type4),
            BaseApplication.getContext().getString(R.string.gongfa_type5),
            BaseApplication.getContext().getString(R.string.gongfa_type6),
            BaseApplication.getContext().getString(R.string.gongfa_type7),
            BaseApplication.getContext().getString(R.string.gongfa_type8),
    };


    // 定义功法图片的展现分组，每子数组的第一元素表示在内容数组中的起始位置，第二元素表示分组中内容个数
    /**
     * {0, 35}全部, {0, 2}预备, {2, 2}第一式, {4, 5}第二式, {9, 5}第三式,
     {14, 5}第四式, {19, 5}第五式, {24, 5}第六式, {29, 4}第七式, {33, 2}第八式
     */
    public static final int[][] GONGFA_GROUP = {{0, 35}, {0, 2}, {2, 2}, {4, 5}, {9, 5},
            {14, 5}, {19, 5}, {24, 5}, {29, 4}, {33, 2}};


    // 功法 大图
    public static final int[] GONG_FA_BIG_IMAGE = {
            R.drawable.gf_tp_yubeidongzuo_0,
            R.drawable.gf_tp_yubeidongzuo_1,//预备动作

            R.drawable.gf_tp_qishi_0,
            R.drawable.gf_tp_qishi_1,//第一式 起式

            R.drawable.gf_tp_1_0,
            R.drawable.gf_tp_1_1,//第2式
            R.drawable.gf_tp_1_2,//第2式
            R.drawable.gf_tp_1_3,//第2式
            R.drawable.gf_tp_1_4,//第2式

            R.drawable.gf_tp_2_0,
            R.drawable.gf_tp_2_1,//第3式
            R.drawable.gf_tp_2_2,//第3式
            R.drawable.gf_tp_2_3,//第3式
            R.drawable.gf_tp_2_4,//第3式

            R.drawable.gf_tp_3_0,
            R.drawable.gf_tp_3_1,//第4式
            R.drawable.gf_tp_3_2,//第4式
            R.drawable.gf_tp_3_3,//第4式
            R.drawable.gf_tp_3_4,//第4式

            R.drawable.gf_tp_4_0,
            R.drawable.gf_tp_4_1,//第5式
            R.drawable.gf_tp_4_2,//第5式
            R.drawable.gf_tp_4_3,//第5式
            R.drawable.gf_tp_4_4,//第5式

            R.drawable.gf_tp_5_0,
            R.drawable.gf_tp_5_1,//第6式
            R.drawable.gf_tp_5_2,//第6式
            R.drawable.gf_tp_5_3,//第6式
            R.drawable.gf_tp_5_4,//第6式

            R.drawable.gf_tp_6_0,
            R.drawable.gf_tp_6_1, //第7式
            R.drawable.gf_tp_6_2, //第7式
            R.drawable.gf_tp_6_3, //第7式

            R.drawable.gf_tp_shoushi_0,
            R.drawable.gf_tp_shoushi_1 //第8式 收式
    };

    public static final String[] GONG_FA_BIG_NAME = {
            "gf_tp_yubeidongzuo_0",
            "gf_tp_yubeidongzuo_1",//预备动作

            "gf_tp_qishi_0",
            "gf_tp_qishi_1",//第一式 起式

            "gf_tp_1_0",
            "gf_tp_1_1",//第2式
            "gf_tp_1_2",//第2式
            "gf_tp_1_3",//第2式
            "gf_tp_1_4",//第2式

            "gf_tp_2_0",
            "gf_tp_2_1",//第3式
            "gf_tp_2_2",//第3式
            "gf_tp_2_3",//第3式
            "gf_tp_2_4",//第3式

            "gf_tp_3_0",
            "gf_tp_3_1",//第4式
            "gf_tp_3_2",//第4式
            "gf_tp_3_3",//第4式
            "gf_tp_3_4",//第4式

            "gf_tp_4_0",
            "gf_tp_4_1",//第5式
            "gf_tp_4_2",//第5式
            "gf_tp_4_3",//第5式
            "gf_tp_4_4",//第5式

            "gf_tp_5_0",
            "gf_tp_5_1",//第6式
            "gf_tp_5_2",//第6式
            "gf_tp_5_3",//第6式
            "gf_tp_5_4",//第6式

            "gf_tp_6_0",
            "gf_tp_6_1", //第7式
            "gf_tp_6_2", //第7式
            "gf_tp_6_3", //第7式

            "gf_tp_shoushi_0",
            "gf_tp_shoushi_1" //第8式 收式
    };

    // 功法 说明
    public static final String[] GONG_FA_EXPLAIN = {
            "",
            Constant.yubeidongzuo,// 预备动作

            "",
            Constant.qishi,// 第一式 起式

            "",//封面对应的
            Constant.diyishi_jianzhichangtian_1,// 第2式
            Constant.diyishi_jianzhichangtian_2,// 第2式
            Constant.diyishi_jianzhichangtian_3,// 第2式
            "",//箭之长天经络图

            "",//封面
            Constant.diershi_haidianlaoyue_1,// 第3式
            Constant.diershi_haidianlaoyue_2,// 第3式
            Constant.diershi_haidianlaoyue_3,// 第3式
            "",//海底捞月经络图

            "",//封面
            Constant.disanshi_taijiyunshou_1,// 第4式
            Constant.disanshi_taijiyunshou_2,// 第4式
            Constant.disanshi_taijiyunshou_3,// 第4式
            "",//太极云手经络图

            "",//封面
            Constant.disishi_gaoshanliushui_1,// 第5式
            Constant.disishi_gaoshanliushui_2,// 第5式
            Constant.disishi_gaoshanliushui_3,// 第5式
            "",//高山流水经络图

            "",//封面
            Constant.diwushi_wushentanhai_1,// 第6式
            Constant.diwushi_wushentanhai_2,// 第6式
            Constant.diwushi_wushentanhai_3,// 第6式
            "",//附身探海经络图

            "",//封面
            Constant.diliushi_fushenbaoyue_1, // 第7式
            Constant.diliushi_fushenbaoyue_2, // 第7式
            "",//附身抱月经络图

            "",
            Constant.shoushi // 第8式 收式
    };

    // 功法名 预备动作，舒肝理肺。。。。
    public static final String[] GONG_FA_NAME = {
            "",//预备动作-->增加一次为纯文字介绍图片
            BaseApplication.getContext().getString(R.string.gongfa_ready),//预备动作

            "",//增加一次为纯文字介绍图片
            BaseApplication.getContext().getString(R.string.gongfa_data_qishi),//起式-->第一式，起式

            "",//增加一次为纯文字介绍图片
            BaseApplication.getContext().getString(R.string.gongfa_data_name_jianzhichangtian),//-->第二式，剑指长天title
            BaseApplication.getContext().getString(R.string.gongfa_data_name_jianzhichangtian),//-->第二式，剑指长天title
            BaseApplication.getContext().getString(R.string.gongfa_data_name_jianzhichangtian),//-->第二式，剑指长天title
            BaseApplication.getContext().getString(R.string.gongfa_data_name_jianzhichangtian),//-->第二式，剑指长天title

            "",//增加一次为纯文字介绍图片
            BaseApplication.getContext().getString(R.string.gongfa_data_name_haidilaoyue),//-->第三式，海底捞月title
            BaseApplication.getContext().getString(R.string.gongfa_data_name_haidilaoyue),//-->第三式，海底捞月title
            BaseApplication.getContext().getString(R.string.gongfa_data_name_haidilaoyue),//-->第三式，海底捞月title
            BaseApplication.getContext().getString(R.string.gongfa_data_name_haidilaoyue),//-->第三式，海底捞月title

            "",//增加一次为纯文字介绍图片
            BaseApplication.getContext().getString(R.string.gongfa_data_name_taijiyunshou),//-->第四式，太极云手title
            BaseApplication.getContext().getString(R.string.gongfa_data_name_taijiyunshou),//-->第四式，太极云手title
            BaseApplication.getContext().getString(R.string.gongfa_data_name_taijiyunshou),//-->第四式，太极云手title
            BaseApplication.getContext().getString(R.string.gongfa_data_name_taijiyunshou),//-->第四式，太极云手title

            "",//增加一次为纯文字介绍图片
            BaseApplication.getContext().getString(R.string.gongfa_data_name_gaoshanliushui),//-->第五式，高山流水title
            BaseApplication.getContext().getString(R.string.gongfa_data_name_gaoshanliushui),//-->第五式，高山流水title
            BaseApplication.getContext().getString(R.string.gongfa_data_name_gaoshanliushui),//-->第五式，高山流水title
            BaseApplication.getContext().getString(R.string.gongfa_data_name_gaoshanliushui),//-->第五式，高山流水title

            "",//增加一次为纯文字介绍图片
            BaseApplication.getContext().getString(R.string.gongfa_data_name_fushentanhai),//-->第六式，俯身探海title
            BaseApplication.getContext().getString(R.string.gongfa_data_name_fushentanhai),//-->第六式，俯身探海title
            BaseApplication.getContext().getString(R.string.gongfa_data_name_fushentanhai),//-->第六式，俯身探海title
            BaseApplication.getContext().getString(R.string.gongfa_data_name_fushentanhai),//-->第六式，俯身探海title

            "",//增加一次为纯文字介绍图片
            BaseApplication.getContext().getString(R.string.gongfa_data_name_fushenbaoyue),//-->第七式，俯身抱月title
            BaseApplication.getContext().getString(R.string.gongfa_data_name_fushenbaoyue),//-->第七式，俯身抱月title
            BaseApplication.getContext().getString(R.string.gongfa_data_name_fushenbaoyue),//-->第七式，俯身抱月title

            "",//增加一次为纯文字介绍图片
            BaseApplication.getContext().getString(R.string.gongfa_data_shoushi)//收式 title-->第八式，收式
    };
}
