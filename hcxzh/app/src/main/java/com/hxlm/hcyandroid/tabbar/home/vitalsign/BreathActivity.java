package com.hxlm.hcyandroid.tabbar.home.vitalsign;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.report.UploadManager;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.user.UserManager;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.CheckedDataType;
import com.hxlm.hcyandroid.util.ToastUtil;
import com.hxlm.hcyandroid.view.ContainsEmojiEditText;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 呼吸检测
 *
 * @author l
 */
public class BreathActivity extends BaseActivity implements OnClickListener {

    private ListView lv_tips_breath;
    private List<BreathData> list;

    // 倒计时
    private LinearLayout linear_jishi;// 开始计时
    private TextView tv_time_miao;// 30秒
    private TextView tv_time_s;// 尾数s
    private TextView tv_time_end;// 计时结束

    private ContainsEmojiEditText etBreathRate;// 呼吸次数
    private ImageView iv_StartDetection;// 开始检测
    private ImageView iv_breath_submit;// 提交
    private ImageView iv_breath_restart;// 重新检测
    private ImageView iv_use_specification;//使用规范
    private int time;// 计时
    private Context context;

    private Timer timer;
    // 计时器
    private TimerTask timerTask;

    private int breathnum;// 呼吸次数
    private String isNormal = "";// 是否正常

    private Handler handler = new Handler(new Handler.Callback() {
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case -10:
                    tv_time_miao.setText(String.valueOf(time));
                    if (time <= 0) {
                        stopTimer();
                        // 计时结束
                        linear_jishi.setVisibility(View.GONE);
                        tv_time_end.setVisibility(View.VISIBLE);
                    }
                    break;
            }
            return false;
        }
    });

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_breath);
        context = BreathActivity.this;

    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.breath_title), titleBar, 1);
        linear_jishi = (LinearLayout) findViewById(R.id.linear_jishi);
        tv_time_miao = (TextView) findViewById(R.id.tv_time_miao);
        tv_time_s = (TextView) findViewById(R.id.tv_time_s);
        etBreathRate = (ContainsEmojiEditText) findViewById(R.id.etBreathRate);
        iv_StartDetection = (ImageView) findViewById(R.id.iv_StartDetection);
        iv_StartDetection.setOnClickListener(this);
        iv_breath_submit = (ImageView) findViewById(R.id.iv_breath_submit);
        iv_breath_submit.setOnClickListener(this);
        iv_breath_restart = (ImageView) findViewById(R.id.iv_breath_restart);
        iv_breath_restart.setOnClickListener(this);
        tv_time_end = (TextView) findViewById(R.id.tv_time_end);
        iv_use_specification = (ImageView) findViewById(R.id.iv_use_specification);

        iv_use_specification.setOnClickListener(this);
    }

    // 开始计时
    public void startTimer() {
        time = 30;
        if (timer == null) {
            timer = new Timer();
        }
        if (timerTask == null) {
            timerTask = new TimerTask() {

                @Override
                public void run() {
                    time--;
                    Message message = handler.obtainMessage(-10);
                    handler.sendMessage(message);
                }
            };
        }

        if (timer != null)
            timer.schedule(timerTask, 1000, 1000);
    }


    // 停止计时
    public void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }

        if (timerTask != null) {
            timerTask.cancel();
            timerTask = null;
        }

        time = 0;
    }

    @Override
    public void initDatas() {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            // 开始检测
            case R.id.iv_StartDetection:

                iv_StartDetection.setVisibility(View.GONE);
                iv_breath_submit.setVisibility(View.VISIBLE);
                iv_breath_restart.setVisibility(View.VISIBLE);

                linear_jishi.setVisibility(View.VISIBLE);
                tv_time_end.setVisibility(View.GONE);

                // 开始计时
                startTimer();

                break;
            // 提交
            case R.id.iv_breath_submit:

                if (time > 0) {
                    ToastUtil.invokeShortTimeToast(BreathActivity.this,
                            getString(R.string.breath_tips_do_not_submit));
                } else {
                    // 得到呼吸次数
                    String numBreathRate = etBreathRate.getText().toString();


                    // 如果呼吸次数为空
                    if (TextUtils.isEmpty(numBreathRate)) {
                        ToastUtil.invokeShortTimeToast(context, getString(R.string.breath_tips_no_input_count));
                    } else {

                        // 改成整数
                        breathnum = Integer.parseInt(numBreathRate);
                        breathnum = breathnum * 2;
                        new UploadManager().uploadCheckedData(CheckedDataType.BREATH, breathnum,0,
                                new AbstractDefaultHttpHandlerCallback(BreathActivity.this) {
                                    @Override
                                    protected void onResponseSuccess(Object obj) {
                                        //得到当前用户的年龄
                                        int age = new UserManager().getAge(LoginControllor.getChoosedChildMember());

                                        //年龄大于等于18为成年人，小于18为未成年人
                                        if (age >= 18) {
                                            if (breathnum >= 32 && breathnum <= 40) {
                                                isNormal = getString(R.string.breath_normal);
                                            } else {
                                                isNormal = getString(R.string.breath_unNormal);
                                            }
                                        } else if (age >= 0 && age < 18) {
                                            if (breathnum >= 60 && breathnum <= 80) {
                                                isNormal = getString(R.string.breath_normal);
                                            } else {
                                                isNormal = getString(R.string.breath_unNormal);
                                            }
                                        }

                                        // 提示一个dialog
                                        BreathDialog breathDialog2 = new BreathDialog(BreathActivity.this,
                                                getString(R.string.breath_jielv) + breathnum + getString(R.string.bpc_unit_count), isNormal);
                                        breathDialog2.show();
                                    }
                                });
//                        LoginControllor.requestLogin(BreathActivity.this, new OnCompleteListener() {
//                            @Override
//                            public void onComplete() {
//                                new ChooseMemberDialog(BreathActivity.this, new OnCompleteListener() {
//                                    @Override
//                                    public void onComplete() {
//
//                                    }
//                                }).show();
//                            }
//                        });


                    }
                }
                break;
            // 重新检测
            case R.id.iv_breath_restart:
                etBreathRate.setText("");
                // 当前的倒计时没有结束，又点击重新检测，先将当前的time设置为0
                if (time > 0) {
                    // 先停止计时
                    stopTimer();

                    tv_time_miao.setText("30");
                    // 在重新计时
                    startTimer();

                    tv_time_end.setVisibility(View.GONE);
                    linear_jishi.setVisibility(View.VISIBLE);

                }
                // 当前的计时已经结束
                else {

                    tv_time_miao.setText("30");

                    // 重新开始计时
                    startTimer();
                    tv_time_end.setVisibility(View.GONE);
                    linear_jishi.setVisibility(View.VISIBLE);

                }

                break;
            //使用规范
            case R.id.iv_use_specification:
                Intent intent = new Intent(BreathActivity.this, BreathUseActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    // 实体类
    class BreathData {
        private int id;
        private String text;

        public BreathData(int id, String text) {
            super();
            this.id = id;
            this.text = text;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

    }

    public class BreathDialog extends AlertDialog implements
            OnClickListener {

        Context context;
        String tishi;
        String isnormal;

        public BreathDialog(Context context, String tishi, String isnormal) {
            super(context);
            this.context = context;
            this.tishi = tishi;
            this.isnormal = isnormal;

        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.breath_submit_prompt);

            TextView tv_breath_prompt_tishi = (TextView) findViewById(R.id.tv_breath_prompt_tishi);
            tv_breath_prompt_tishi.setText(tishi);

            // 是否正常
            TextView tv_is_normal = (TextView) findViewById(R.id.tv_is_normal);

            // 确定
            ImageView image_submit = (ImageView) findViewById(R.id.image_submit);
            image_submit.setOnClickListener(this);

            if (getString(R.string.breath_normal).equals(isnormal)) {
                tv_is_normal.setText(getString(R.string.breath_normal));
                tv_is_normal.setTextColor(getResources().getColor(
                        R.color.submit_normal));
            } else if (getString(R.string.breath_unNormal).equals(isnormal)) {
                tv_is_normal.setText(getString(R.string.breath_unNormal));
                tv_is_normal.setTextColor(getResources().getColor(
                        R.color.submit_not_normal));
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                // 确定
                case R.id.image_submit:
                    etBreathRate.setText("");
                    this.dismiss();
                    break;

            }
        }
    }
}
