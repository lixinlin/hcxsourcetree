package com.hxlm.hcyandroid.bean;

import java.io.Serializable;

/**
 * 地区
 *
 * @author dell
 */
public class Area implements Serializable {
    private int id;
    private long createDate;
    private long modifyDate;
    private String name;
    private String fullName;
    private Area parent;

	
	
	/*public Area(int id, long createDate, long modifyDate, String name,
            String fullName, Area parent) {
		super();
		this.id = id;
		this.createDate = createDate;
		this.modifyDate = modifyDate;
		this.name = name;
		this.fullName = fullName;
		this.parent = parent;
	}*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(long modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Area getParent() {
        return parent;
    }

    public void setParent(Area parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        return "Area [id=" + id + ", createDate=" + createDate
                + ", modifyDate=" + modifyDate + ", name=" + name
                + ", fullName=" + fullName + ", parent=" + parent + "]";
    }


}
