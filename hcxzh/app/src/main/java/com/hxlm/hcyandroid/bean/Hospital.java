package com.hxlm.hcyandroid.bean;

import java.io.Serializable;

/**
 * 医院
 *
 * @author Administrator
 */
public class Hospital implements Serializable {
    private int id;
    private String name;
    private Area area;//所属地区
    private boolean isPrivate;// 是否私人医院
    private String devices;// 主要设备
    private String photo;//医院图片
    private String star;//私人医院星级
    private String importantDisease;// 重点专病
    private String attributeValue0;//属性几级几等


    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public String getDevices() {
        return devices;
    }

    public void setDevices(String devices) {
        this.devices = devices;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public String getImportantDisease() {
        return importantDisease;
    }

    public void setImportantDisease(String importantDisease) {
        this.importantDisease = importantDisease;
    }

    public String getAttributeValue0() {
        return attributeValue0;
    }

    public void setAttributeValue0(String attributeValue0) {
        this.attributeValue0 = attributeValue0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Hospital [id=" + id + ", name=" + name + ", area=" + area
                + ", isPrivate=" + isPrivate + ", devices=" + devices
                + ", photo=" + photo + ", star=" + star + ", importantDisease="
                + importantDisease + ", attributeValue0=" + attributeValue0
                + "]";
    }


}

