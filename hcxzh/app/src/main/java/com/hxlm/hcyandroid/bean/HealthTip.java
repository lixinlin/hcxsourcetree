package com.hxlm.hcyandroid.bean;

public class HealthTip {
    private int id;
    private long createDate;
    private long modifyDate;
    private int order;
    private String content;
    private String title;
    private HealthAdvisor healthAdvisor;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(long modifyDate) {
        this.modifyDate = modifyDate;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public HealthAdvisor getHealthAdvisor() {
        return healthAdvisor;
    }

    public void setHealthAdvisor(HealthAdvisor healthAdvisor) {
        this.healthAdvisor = healthAdvisor;
    }

}
