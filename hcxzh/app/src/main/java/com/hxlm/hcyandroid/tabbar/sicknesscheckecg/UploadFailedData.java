package com.hxlm.hcyandroid.tabbar.sicknesscheckecg;

import com.hxlm.hcyandroid.bean.ECGData;

import java.io.File;

/**
 * Created by l on 2016/10/21.
 * 心电检测完之后上传失败数据
 */
public class UploadFailedData {
    private int status;//上传失败状态 上传文件失败 状态1  / 上传文件成功 但上传数据失败 状态2
    private int memberId;
    private int memberChildId;
    private int datatype;//心电类型 固定
    private File file;//状态1 上传失败状态

    private ECGData ecgData;
    private long createDate;//用户测试时间戳，长度为13位

    private int order;//序列号


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public int getMemberChildId() {
        return memberChildId;
    }

    public void setMemberChildId(int memberChildId) {
        this.memberChildId = memberChildId;
    }

    public int getDatatype() {
        return datatype;
    }

    public void setDatatype(int datatype) {
        this.datatype = datatype;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public ECGData getEcgData() {
        return ecgData;
    }

    public void setEcgData(ECGData ecgData) {
        this.ecgData = ecgData;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return "UploadFailedData{" +
                "status=" + status +
                ", memberId=" + memberId +
                ", memberChildId=" + memberChildId +
                ", datatype=" + datatype +
                ", file=" + file +
                ", ecgData=" + ecgData +
                ", createDate=" + createDate +
                ", order=" + order +
                '}';
    }
}
