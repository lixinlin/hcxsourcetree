package com.hxlm.hcyandroid.bean;

import java.io.Serializable;

/**
 * 子成员
 *
 * @author Administrator
 */
public class ChildMember implements Serializable {
    private long createDate;
    private int id;
    private boolean isMedicare;
    private String mobile;
    private long modifyDate;
    private String name;

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isMedicare() {
        return isMedicare;
    }

    public void setMedicare(boolean isMedicare) {
        this.isMedicare = isMedicare;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public long getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(long modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
