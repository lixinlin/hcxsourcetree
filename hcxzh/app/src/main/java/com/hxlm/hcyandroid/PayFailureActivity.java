package com.hxlm.hcyandroid;


import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.view.TitleBarView;

/**
 * 支付失败
 *
 * @author dell
 */
public class PayFailureActivity extends BaseActivity implements OnClickListener {


    private ImageView ivpayagain;


    @Override
    public void setContentView() {
        setContentView(R.layout.activity_pay_failure);

    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.pay_success_title), titleBar, 1);
        ivpayagain = (ImageView) findViewById(R.id.ivpayagain);
        ivpayagain.setOnClickListener(this);
    }

    @Override
    public void initDatas() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            //重新支付
            case R.id.ivpayagain:
                this.finish();
                break;
            default:
                break;
        }
    }

}

