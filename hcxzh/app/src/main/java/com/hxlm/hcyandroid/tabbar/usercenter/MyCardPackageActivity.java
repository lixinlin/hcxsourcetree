package com.hxlm.hcyandroid.tabbar.usercenter;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.bean.CardArrayDataBean;
import com.hxlm.android.hcy.bean.CardListBean;
import com.hxlm.android.hcy.order.CardManager;
import com.hxlm.android.hcy.order.CardPackageAdapter;
import com.hxlm.android.hcy.order.card.CardDetailsActivity;
import com.hxlm.android.hcy.order.card.CardUnActivateDetailsActivity;
import com.hxlm.android.hcy.order.card.InputAddCardActivity;
import com.hxlm.android.hcy.order.erweima.Constant;
import com.hxlm.android.hcy.order.erweima.activity.CaptureActivity;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * 我的卡包
 *
 * @author l
 */
public class MyCardPackageActivity extends BaseActivity implements
        OnClickListener {
    private RecyclerView rvCard;
    private TextView tvCardEmpty;
    private Bundle bundle;
    private String statusOne = "1";
    private String statusTwo = "2";

    private List<CardArrayDataBean> list;
    private CardListBean cardListBean;
    private CardArrayDataBean cardArrayDataBean;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_my_card_package);
    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.my_card_package_wodekabao), titleBar, 1);
        RelativeLayout rl_start_bind_new_card = findViewById(R.id.rl_start_bind_new_card);
        rl_start_bind_new_card.setOnClickListener(this);
//        lv_cards = (ListView) findViewById(R.id.lv_cards);
        tvCardEmpty = findViewById(R.id.tv_card_empty);
        rvCard = findViewById(R.id.rv_card_package);
        rvCard.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    public void initDatas() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_start_bind_new_card:
//                startQrCode();
                startActivity(new Intent(MyCardPackageActivity.this, InputAddCardActivity.class));
                break;
            default:
                break;
        }
    }

    private void startQrCode() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            // 申请权限
            ActivityCompat.requestPermissions(MyCardPackageActivity.this, new String[]{Manifest.permission.CAMERA}, Constant.REQ_PERM_CAMERA);
            return;
        }
        // 二维码扫码
//                startActivity(new Intent(this, BindNewCardActivity.class));
        startActivity(new Intent(this, CaptureActivity.class));
    }
    CardPackageAdapter cardPackageAdapter = null;
    @Override
    protected void onResume() {
        super.onResume();
        list = new ArrayList<>();
//        new CardManager().getCards(new AbstractDefaultHttpHandlerCallback(this) {
//            @Override
//            protected void onResponseSuccess(Object obj) {
                new CardManager().getCards(new AbstractDefaultHttpHandlerCallback(this) {
                    @Override
                    protected void onResponseSuccess(Object obj) {
//                        JSONObject jsonObject = JSON.parseObject((String) obj);
//                        String attr_data = jsonObject.getString("attr_data");
//                        List<CardArrayDataBean> cardrray = JSON.parseArray(attr_data, CardArrayDataBean.class);
//
//                        String data = jsonObject.getString("data");
//                        List<Card> cards = JSON.parseArray(data, Card.class);

                        cardListBean = (CardListBean) obj;
                        setData();

                        if (list.size() != 0  ) {

                            if (cardPackageAdapter == null){
                                cardPackageAdapter = new CardPackageAdapter(MyCardPackageActivity.this,true);
                                rvCard.setAdapter(cardPackageAdapter);
                                Log.e("retrofit","===卡包列表list=="+list.toString());
                                cardPackageAdapter.setData(list);
                                cardPackageAdapter.setOnItemClick(new CardPackageAdapter.OnItemClick() {
                                    @Override
                                    public void onItem(int position, CardArrayDataBean object) {

                                            CardArrayDataBean card = (CardArrayDataBean) object;
                                            // status 为1 未激活 为2 已激活
                                            if (statusTwo.equals(card.getStatus())) {
                                                //已经被激活跳转
                                                Intent intent1 = new Intent(MyCardPackageActivity.this, CardDetailsActivity.class);
                                                Log.e("retrofit", "==已激活卡的卡号===" + card.getCard_no());
                                                intent1.putExtra("CARD_NO1", card.getCard_no());
                                                intent1.putExtra("CARD_NAME1", card.getCard_name());
                                                intent1.putExtra("CARD_DESC1",card.getDescription());
                                                startActivity(intent1);
                                            } else if (statusOne.equals(card.getStatus())) {
                                                //未被激活跳转
                                                Intent intent = new Intent(MyCardPackageActivity.this, CardUnActivateDetailsActivity.class);
                                                intent.putExtra("CARD_NO", card.getCard_no());
                                                intent.putExtra("CARD_NAME", card.getCard_name());
                                                intent.putExtra("CARD_DESC", card.getDescription());
                                                startActivity(intent);
                                            }
                                    }
                                });
                            }else {
                                cardPackageAdapter.setData(list);
                                cardPackageAdapter.notifyDataSetChanged();
                            }

                        }else {
                            rvCard.setVisibility(View.GONE);
                            tvCardEmpty.setVisibility(View.VISIBLE);
                        }
////                        lv_cards.setAdapter(new CardsListAdapter(MyCardPackageActivity.this, (List<Card>) obj, true));
                    }
                });
            }

    private void setData(){
        if (list != null){
            list.clear();
        }

        List<CardListBean.DataBean> data = cardListBean.getData();
        for (int i = 0; i < data.size(); i++) {
            CardArrayDataBean cardArrayDataBean = new CardArrayDataBean();
            CardListBean.DataBean dataBean = data.get(i);
            cardArrayDataBean.setCard_name(dataBean.getCashcard().getName());
            cardArrayDataBean.setCard_no(dataBean.getCode());
            cardArrayDataBean.setDescription(dataBean.getCashcard().getIntroduction());
            cardArrayDataBean.setAmount(dataBean.getCashcard().getAmount());
            cardArrayDataBean.setBalance(dataBean.getBalance());
            cardArrayDataBean.setBeginDate(dataBean.getCashcard().getBeginDate());
            cardArrayDataBean.setExpiredTime(dataBean.getCashcard().getExpiredTime());
            cardArrayDataBean.setEndDate(dataBean.getCashcard().getEndDate());
            cardArrayDataBean.setDated(dataBean.isIsDated());
            cardArrayDataBean.setBindDate(dataBean.getBindDate());
            cardArrayDataBean.setTag(1);
            list.add(cardArrayDataBean);
        }
        List<CardListBean.AttrDataBean> attr_data = cardListBean.getAttr_data();
        for (int i = 0; i < attr_data.size(); i++) {
            CardArrayDataBean cardArrayDataBean = new CardArrayDataBean();
            CardListBean.AttrDataBean attrDataBean = attr_data.get(i);
            cardArrayDataBean.setDescription(attrDataBean.getDescription());
            cardArrayDataBean.setCard_name(attrDataBean.getCard_name());
            cardArrayDataBean.setExprise_time(attrDataBean.getExprise_time());
            cardArrayDataBean.setCard_no(attrDataBean.getCard_no());
            cardArrayDataBean.setStatus(attrDataBean.getStatus());
            cardArrayDataBean.setTag(2);
            list.add(cardArrayDataBean);
        }

    }
//        });


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //扫描结果回调
        if (requestCode == Constant.REQ_QR_CODE && resultCode == RESULT_OK) {
            bundle = data.getExtras();
        }

        String scanResult = bundle.getString(Constant.INTENT_EXTRA_KEY_QR_SCAN);
        //扫描出的信息  跳转至添加页面展示
//        Intent intent = new Intent(MyCardPackageActivity.this,ScanningAddCardActivity.class);
//        startActivity(intent);
//        tvResult.setText(scanResult);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constant.REQ_PERM_CAMERA:
                // 摄像头权限申请
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // 获得授权
                    startQrCode();
                } else {
                    // 被禁止授权
                    Toast.makeText(MyCardPackageActivity.this, getString(R.string.my_card_package_qingzhiquanxianzxdk), Toast.LENGTH_LONG).show();
                } break;
        }
    }

}
