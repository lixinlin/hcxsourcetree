package com.hxlm.hcyandroid.tabbar.home_jczs;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.bean.HealthInformationCategory;
import com.hxlm.hcyandroid.datamanager.HealthInformationManager;
import com.hxlm.hcyandroid.tabbar.healthinformation.HealthInformationFragment;
import com.hxlm.hcyandroid.tabbar.healthinformation.HealthLectureFragment;

import java.util.List;

/**
 * Created by l on 2016/10/19.
 * 健康资讯
 */
public class InformationActivity extends FragmentActivity implements View.OnClickListener {

    private LinearLayout ll_buttons;
    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;
    private HealthInformationFragment newFragment;// 最新资讯
    private HealthLectureFragment lectureFragment;// 健康讲座
    private FrameLayout fl_content;
    private RelativeLayout rlLayout;
    public static final int NEW_INFORMATION_TAG = -2;
    private final int HEALTH_LECTURE_TAG = -3;
    private List<HealthInformationCategory> categories;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.information_fragment_activity);
        context = InformationActivity.this;
        initViews();
    }


    protected void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.info_title), titleBar, 1);
        ll_buttons = (LinearLayout) findViewById(R.id.linner_buttons);

        fl_content = (FrameLayout) findViewById(R.id.fl_content);

        addRelativeLayout(getString(R.string.info_tag_new_information), NEW_INFORMATION_TAG);
        if (!Constant.isEnglish) {
            addRelativeLayout(getString(R.string.info_tag_health_lecture), HEALTH_LECTURE_TAG);
        }

        setDefult();

        initData();// 请求接口
    }

    private void initData() {
        new HealthInformationManager().getHealthInformationCategory(new AbstractDefaultHttpHandlerCallback(InformationActivity.this) {
            @Override
            protected void onResponseSuccess(Object obj) {
                categories = (List<HealthInformationCategory>)obj;
                for (HealthInformationCategory category : categories) {
                    addRelativeLayout(category.getName(), category.getId());
                }
            }

            @Override
            protected void onHttpError(int httpErrorCode) {
                super.onHttpError(httpErrorCode);
            }
        });

    }

    // 进入该界面默认展示最新资讯
    private void setDefult() {
        // 第一次默认选中时的颜色
        TextView textview = (TextView) ll_buttons.getChildAt(0);
        textview.setTextColor(getResources().getColor(R.color.a01a0f2));

        fragmentManager = getSupportFragmentManager();
        transaction = fragmentManager.beginTransaction();
        newFragment = new HealthInformationFragment();
        newFragment.setId(NEW_INFORMATION_TAG);
        transaction.replace(R.id.fl_content, newFragment);
        transaction.commit();
    }

    private void addRelativeLayout(String text, int tag) {

        // 使用代码实现最新资讯
        /*
         * rlLayout = new RelativeLayout(context); LinearLayout.LayoutParams
		 * paramsNews = new LinearLayout.LayoutParams(0,
		 * LinearLayout.LayoutParams.MATCH_PARENT, 1);
		 * rlLayout.setLayoutParams(paramsNews);
		 * rlLayout.setBackgroundResource(R.drawable.report_radio_button_no_bg);
		 * rlLayout.setId(11);// 设置id rlLayout.setTag(tag);
		 */

        // 给rlLayout添加TextView组件
        TextView textView = new TextView(context);
        LinearLayout.LayoutParams rlParamstextNews = new LinearLayout.LayoutParams(
                0, LinearLayout.LayoutParams.MATCH_PARENT, 1);

        // rlParamstextNews.addRule(RelativeLayout.CENTER_IN_PARENT);
        textView.setLayoutParams(rlParamstextNews);
        textView.setText(text);
        textView.setGravity(Gravity.CENTER);
        textView.setTextColor(getResources().getColor(R.color.shenhuise));
        textView.setTextSize(14);
        textView.setPadding(10, 0, 10, 0);

        ll_buttons.addView(textView);
        textView.setTag(tag);
        textView.setOnClickListener(this);

		/*
		 * rlLayout.addView(textView);
		 * 
		 * // 给rlLayout添加ImageView组件 ImageView imageViewNews = new
		 * ImageView(context); RelativeLayout.LayoutParams rlParamstextimageNews
		 * = new RelativeLayout.LayoutParams(
		 * RelativeLayout.LayoutParams.WRAP_CONTENT,
		 * RelativeLayout.LayoutParams.MATCH_PARENT);
		 * rlParamstextimageNews.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		 * imageViewNews.setImageResource(R.drawable.health_icon);
		 * imageViewNews.setLayoutParams(rlParamstextimageNews);
		 * 
		 * rlLayout.addView(imageViewNews);
		 * 
		 * // 将RelativeLayout添加到LinearLayout中 ll_buttons.addView(rlLayout);
		 * 
		 * // 添加监听 rlLayout.setOnClickListener(this);
		 */
    }

    @Override
    public void onClick(View v) {
        // 点击按钮进行切换
        fragmentManager = getSupportFragmentManager();

        transaction = fragmentManager.beginTransaction();
        for (int i = 0; i < ll_buttons.getChildCount(); i++) {

            TextView textView = (TextView) ll_buttons.getChildAt(i);
            textView.setTextColor(getResources().getColor(R.color.shenhuise));
        }
        TextView textView = (TextView) v;
        // 选中时的颜色
        textView.setTextColor(getResources().getColor(R.color.a01a0f2));
        // v.setBackgroundResource(R.drawable.report_radio_button_checked_bg);

        switch ((Integer) v.getTag()) {
            // 最新资讯
            case NEW_INFORMATION_TAG:
                newFragment = new HealthInformationFragment();
                newFragment.setId(NEW_INFORMATION_TAG);
                transaction.replace(R.id.fl_content, newFragment);
                transaction.commit();
                break;
            // 健康讲座
            case HEALTH_LECTURE_TAG:
                if (lectureFragment == null) {
                    lectureFragment = new HealthLectureFragment();
                }
                transaction.replace(R.id.fl_content, lectureFragment);
                transaction.commit();
                break;
            default:
                break;
        }

        int viewTag = (Integer) v.getTag();
        if (categories != null) {
            for (HealthInformationCategory category : categories) {
                if (category.getId() == viewTag) {
                    newFragment = new HealthInformationFragment();
                    newFragment.setId(category.getId());
                    transaction.replace(R.id.fl_content, newFragment);
                    transaction.commit();
                }
            }
        }
    }
}