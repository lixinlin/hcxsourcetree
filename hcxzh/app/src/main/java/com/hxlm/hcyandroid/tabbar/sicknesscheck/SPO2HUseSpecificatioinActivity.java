package com.hxlm.hcyandroid.tabbar.sicknesscheck;

import android.widget.ListView;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.bean.CheckStep;

import java.util.ArrayList;
import java.util.List;

/**
 * 血氧使用规范
 *
 * @author l
 */
public class SPO2HUseSpecificatioinActivity extends BaseActivity {

    private ListView lv_tips;
    private List<CheckStep> steps;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_spo2_huse_specificatioin);
    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.spo2_title), titleBar, 1);
        lv_tips = findViewById(R.id.lv_tips);

    }

    @Override
    public void initDatas() {

        steps = new ArrayList<CheckStep>();
        steps.add(new CheckStep(R.drawable.check_step_number_bg2, "", getString(R.string.bpuf_tips)));
        steps.add(new CheckStep(R.drawable.check_step_number_bg3, "", getString(R.string.bpuf_tips)));
        steps.add(new CheckStep(R.drawable.check_step_number_bg4, "", getString(R.string.bpuf_tips)));
        steps.add(new CheckStep(R.drawable.check_step_number_bg5, "", getString(R.string.bpuf_tips)));
        steps.add(new CheckStep(R.drawable.check_step_number_bg6, "", getString(R.string.bpuf_tips)));
        lv_tips.setAdapter(new StepAdapter(this, steps));


    }

}
