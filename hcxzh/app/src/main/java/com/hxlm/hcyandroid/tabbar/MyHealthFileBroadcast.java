package com.hxlm.hcyandroid.tabbar;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyphone.MainActivity;


/**
 * 在家庭成员或者其他页面点击查看当前用户档案，跳转健康档案界面
 * @author l
 *
 */
public class MyHealthFileBroadcast extends BroadcastReceiver{

	private String archivesFragment="";
	private String otherReport = "";// 是否从其他界面跳转而来
	private int jump=0;//需要跳转的想应界面
	private String tag;

	private Handler handler;
	public MyHealthFileBroadcast(Handler handler)
	{
		this.handler=handler;
	}

	//设置action
	public static final String ACTION="com.hxlm.hcyandroid.tabbar.intent.action.MyHealthFileBroadcast";
	@Override
	public void onReceive(Context arg0, Intent intent) {

		Bundle bundle=new Bundle();

		archivesFragment = intent.getStringExtra("ArchivesFragment");//要跳转到健康档案
		otherReport = intent.getStringExtra("otherReport");
		jump=intent.getIntExtra("Jump",0);
		tag=intent.getStringExtra("tag");



		bundle.putString("ArchivesFragment", archivesFragment);
		bundle.putString("otherReport", otherReport);
		bundle.putInt("Jump",jump);
		bundle.putString("tag",tag);



		if(Constant.DEBUG)
		{
			Log.i("Log.i", "广播启动了,接收到的数据是ArchivesFragment-->"+archivesFragment+"\notherReport-->"+otherReport);
		}


		Message message = Message.obtain();
		message.what = MainActivity.MY_HEALTH_FILT;
		message.setData(bundle);
		handler.sendMessage(message);
	}

}