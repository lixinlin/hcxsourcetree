package com.hxlm.hcyandroid.tabbar.healthmall;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.webkit.JavascriptInterface;
import com.hxlm.android.hcy.order.OtherPayActivity;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.ReserveType;
import com.hxlm.hcyandroid.util.BroadcastUtil;

public class PayInterface {
    private Context context;

    public PayInterface(Context context) {
        super();
        this.context = context;
    }

    @JavascriptInterface
    public void pay(String paymentSn, double money, String title, String desc) {
        if (Constant.DEBUG) {
            Log.i("Log.i", "paymentSn = " + paymentSn);
            Log.i("Log.i", "money = " + money);
            Log.i("Log.i", "title = " + title);
            Log.i("Log.i", "desc = " + desc);
        }

        //从健康商城跳转
        BroadcastUtil.BroadreserveID = ReserveType.HEALTH_MALL;
        //从健康商城跳转
        BroadcastUtil.sendTypeToPayResult(context,
                ReserveType.TYPE_MALL);

        Intent intent = new Intent(context, OtherPayActivity.class);
        intent.putExtra("reserveType", ReserveType.TYPE_MALL);
        intent.putExtra("title", title.length()>20?title.substring(0,20)+"等":title);
        intent.putExtra("desc", desc.length()>20?desc.substring(0,20)+"等":desc);
        intent.putExtra("money", money);
        intent.putExtra("paymentSn", paymentSn);
        context.startActivity(intent);
    }
}
