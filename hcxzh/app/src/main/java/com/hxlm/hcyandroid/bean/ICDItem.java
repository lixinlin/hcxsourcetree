package com.hxlm.hcyandroid.bean;

import java.io.Serializable;

public class ICDItem implements Serializable{
    private String name;
    private String sp;
    private String micd;
    private String mtji;
    private int sex;
    private boolean isChoosed;

    public ICDItem(String name, String sp, String micd, String mtji, int sex) {
        super();
        this.name = name;
        this.sp = sp;
        this.micd = micd;
        this.mtji = mtji;
        this.sex = sex;
        this.isChoosed = false;
    }

    public ICDItem(String name, String sp, String micd, String mtji, int sex,
            boolean isChoosed) {
        this.name = name;
        this.sp = sp;
        this.micd = micd;
        this.mtji = mtji;
        this.sex = sex;
        this.isChoosed = isChoosed;
    }


    public boolean isChoosed() {
        return isChoosed;
    }


    public void setChoosed(boolean isChoosed) {
        this.isChoosed = isChoosed;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSp() {
        return sp;
    }

    public void setSp(String sp) {
        this.sp = sp;
    }

    public String getMicd() {
        return micd;
    }

    public void setMicd(String micd) {
        this.micd = micd;
    }

    public String getMtji() {
        return mtji;
    }

    public void setMtji(String mtji) {
        this.mtji = mtji;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }


    @Override
    public String toString() {
        return "ICDItem [name=" + name + ", sp=" + sp + ", micd=" + micd
                + ", mtji=" + mtji + ", sex=" + sex + ", isChoosed="
                + isChoosed + "]";
    }


    public boolean getIsChoosed() {
        return this.isChoosed;
    }


    public void setIsChoosed(boolean isChoosed) {
        this.isChoosed = isChoosed;
    }


}
