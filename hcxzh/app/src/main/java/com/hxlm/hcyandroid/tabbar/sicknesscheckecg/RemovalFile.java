package com.hxlm.hcyandroid.tabbar.sicknesscheckecg;

import android.util.Log;
import com.hxlm.hcyandroid.bean.ECGReport;

import java.util.ArrayList;

/**
 * Created by juzhang on 2016/10/23.
 * 实现两个ArrayList合并，并去掉重复数据的算法
 */
public class RemovalFile {

    //方法三：遍历列表，对比数据  ecgReportListFaile  list1  ECGReports list2
    public static ArrayList addArrayList(ArrayList<ECGReport> list1, ArrayList<ECGReport> list2) {

        Log.i("ECGReviewActivity","list1的个数-->"+list1.size());
        for(int i=0;i<list1.size();i++)
        {
            Log.i("ECGReviewActivity","list1的Item-->"+list1.get(i));
        }

        Log.i("ECGReviewActivity","list2的个数-->"+list2.size());
        ArrayList list3 = new ArrayList();
        if (list1 == null || list1.size() == 0) {
            list3 = list2;
        } else if (list2 == null || list2.size() == 0) {
            list3 = list1;
        } else {
            for (int i = 0; i < list1.size(); i++) {// 遍历list1
                boolean isExist = false;
                for (int j = 0; j < list2.size(); j++) {
                    //如果path相同，跳出，如果file文件名相同，也要跳出
                    if (list2.get(j).getPath().equals(list1.get(i).getUploadFailedData().getEcgData().getPath())) {
                        isExist = true;// 找到相同项，跳出本层循环
                        break;
                    }
                }
                if (!isExist) {// 不相同，加入list3中
                    list3.add(list1.get(i));
                }
            }

            for (int k = 0; k < list2.size(); k++) {
                list3.add(list2.get(k));
            }


        }
        return list3;
    }
}
