package com.hxlm.hcyandroid.bean;

/**
 * 血压数据
 *
 * @author Administrator
 */
public class BloodPressureData {
    private String highPressure;//收缩压
    private String lowPressure;//舒张压
    private String pulse;//脉搏

    public String getHighPressure() {
        return highPressure;
    }

    public void setHighPressure(String highPressure) {
        this.highPressure = highPressure;
    }

    public String getLowPressure() {
        return lowPressure;
    }

    public void setLowPressure(String lowPressure) {
        this.lowPressure = lowPressure;
    }

    public String getPulse() {
        return pulse;
    }

    public void setPulse(String pulse) {
        this.pulse = pulse;
    }

}
