package com.hxlm.hcyandroid.bean;

import java.io.Serializable;

public class DoctorSchedule implements Serializable {
    private int id;
    private long pauseDate;
    private String pauseTime;
    private String startTime;//开始时间
    private String endTime;//结束时间
    private DoctorPause doctorPause;
    private int openNum; // 总数量
    private int closeNum; // 已经预约的数量，剩余数量为openNum-closeNum
    private String status;//排班状态
    private boolean isSubscribe;//// 当前用户是否预约
    private int productId;//商品类型id用于在支付的时候获取该类型的现金卡

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public boolean isSubscribe() {
        return isSubscribe;
    }

    public void setSubscribe(boolean isSubscribe) {
        this.isSubscribe = isSubscribe;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getPauseDate() {
        return pauseDate;
    }

    public void setPauseDate(long pauseDate) {
        this.pauseDate = pauseDate;
    }

    public String getPauseTime() {
        return pauseTime;
    }

    public void setPauseTime(String pauseTime) {
        this.pauseTime = pauseTime;
    }

    public DoctorPause getDoctorPause() {
        return doctorPause;
    }

    public void setDoctorPause(DoctorPause doctorPause) {
        this.doctorPause = doctorPause;
    }

    public int getOpenNum() {
        return openNum;
    }

    public void setOpenNum(int openNum) {
        this.openNum = openNum;
    }

    public int getCloseNum() {
        return closeNum;
    }

    public void setCloseNum(int closeNum) {
        this.closeNum = closeNum;
    }

    @Override
    public String toString() {
        return "DoctorSchedule{" +
                "id=" + id +
                ", pauseDate=" + pauseDate +
                ", pauseTime='" + pauseTime + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", doctorPause=" + doctorPause +
                ", openNum=" + openNum +
                ", closeNum=" + closeNum +
                ", status='" + status + '\'' +
                ", isSubscribe=" + isSubscribe +
                ", productId=" + productId +
                '}';
    }
}
