package com.hxlm.hcyandroid.bean;

public class Version {

    private boolean isUpdate;
    private String downurl;
    private String releaseContent;// 版本内容
    private boolean isEnforcement;// 是否强制0否,1强制

    public boolean isUpdate() {
        return isUpdate;
    }

    public void setUpdate(boolean update) {
        isUpdate = update;
    }

    public boolean getIsUpdate() {
        return isUpdate;
    }
    public void setIsUpdate(boolean isUpdate) {
        this.isUpdate = isUpdate;
    }

    public boolean isEnforcement() {
        return isEnforcement;
    }

    public void setEnforcement(boolean enforcement) {
        isEnforcement = enforcement;
    }

    public boolean getIsEnforcement() {
        return isEnforcement;
    }

    public void setIsEnforcement(boolean enforcement) {
        isEnforcement = enforcement;
    }

    public String getReleaseContent() {
        return releaseContent;
    }

    public void setReleaseContent(String releaseContent) {
        this.releaseContent = releaseContent;
    }

    public Version() {
        isUpdate = false;
    }



    public String getDownurl() {
        return downurl;
    }

    public void setDownurl(String downurl) {
        this.downurl = downurl;
    }
}
