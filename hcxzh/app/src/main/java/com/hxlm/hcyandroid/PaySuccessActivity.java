package com.hxlm.hcyandroid;


import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import com.hcy.ky3h.R;
import com.hcy_futejia.activity.FtjYueYaoActivity;
import com.hxlm.android.hcy.content.YueYaoStaUpdateBroadcast;
import com.hxlm.android.hcy.order.OtherPayActivity;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.callback.MyCallBack.OnBackClickListener;
import com.hxlm.hcyandroid.tabbar.MyFragmentBroadcast;
import com.hxlm.hcyandroid.tabbar.healthinformation.HealthLectureDetailsActivity;
import com.hxlm.hcyandroid.tabbar.healthinformation.HealthLecturePayInformationActivity;
import com.hxlm.hcyandroid.tabbar.usercenter.MyHealthLectureActivity;
import com.hxlm.hcyandroid.util.SharedPreferenceUtil;
import com.hxlm.hcyphone.MainActivity;

/**
 * 支付成功
 *
 * @author dell
 */
public class PaySuccessActivity extends BaseActivity implements OnClickListener {

    private ImageView ivpaysure;
    private int reserveType;
    private int reserveID = -1;//设置一个类型ID用于区分是从哪里跳转的

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_pay_success);
    }

    @Override
    public void initViews() {

        reserveType = PayTypeBroadcastReceiver.reserveType;
        reserveID = PayTypeBroadcastReceiver.reserveID;
        if (Constant.DEBUG) {
            Log.i("Log.i", "PaySuccessActivity reserveType-->" + reserveType);
            Log.i("Log.i", "PaySuccessActivity reserveID-->" + reserveID);
        }
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.pay_success_title), titleBar, 1, new OnBackClickListener() {

            @Override
            public void onBackClicked() {
                toNextActivity();
            }
        });
        ivpaysure = (ImageView) findViewById(R.id.ivpaysure);
        ivpaysure.setOnClickListener(this);

    }

    @Override
    public void initDatas() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            // 确认完成
            case R.id.ivpaysure:

                toNextActivity();
                break;
            default:
                break;
        }
    }

    //跳转到下一个页面
    public void toNextActivity() {
        Intent intent = new Intent();
        switch (reserveType) {
            //健康讲座
            case ReserveType.TYPE_LECTURE:
                //健康讲座
                if (reserveID == ReserveType.LECTURE) {
                    PaySuccessActivity.this.finish();

                    //判断用户是从哪里跳过来的
                    String strhealthlecturenum = SharedPreferenceUtil.getString("healthlecturenum");
                    if ("1".equals(strhealthlecturenum)) {
                        HealthLectureDetailsActivity.activity.finish();
                        SharedPreferenceUtil.saveString("healthlecturenum", "");
                    } else if ("2".equals(strhealthlecturenum)) {
                        HealthLectureDetailsActivity.activity.finish();
                        HealthLecturePayInformationActivity.activity.finish();
                        SharedPreferenceUtil.saveString("healthlecturenum", "");
                    } else if ("3".equals(strhealthlecturenum)) {
                        HealthLectureDetailsActivity.activity.finish();
                        HealthLecturePayInformationActivity.activity.finish();
                        OtherPayActivity.activity.finish();
                        SharedPreferenceUtil.saveString("healthlecturenum", "");
                    }
                } else if (reserveID == ReserveType.MY_LECTURE) {
                    intent.setClass(this, MyHealthLectureActivity.class);
                    startActivity(intent);
                }


                break;
            //健康商城
            case ReserveType.TYPE_MALL:
                //跳转个人中心
                // 动态注册广播使用隐士Intent
                Intent intentmall=new Intent(MyFragmentBroadcast.ACTION);
                intentmall.putExtra("MyFragment", "myFrag");
                PaySuccessActivity.this.sendBroadcast(intentmall);

                Intent intent2 = new Intent(PaySuccessActivity.this, MainActivity.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//清除MainActivity之前所有的activity
                intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP); //沿用之前的MainActivity
                startActivity(intent2);



                break;
            //乐药
            case ReserveType.TYPE_STATIC_RESOURCE:
                //跳转乐药
                if(reserveID==ReserveType.YUEYAO)
                {
                    //当跳转乐药时，这里有一个问题，就是不能刷新当前页面的某个乐药的状态，同时将底部的金额清空，针对这种情况，暂定使用广播机制
                    Intent intentBroadYueYao=new Intent(YueYaoStaUpdateBroadcast.ACTION);
                    PaySuccessActivity.this.sendBroadcast(intentBroadYueYao);

                    intent.setClass(this, FtjYueYaoActivity.class);
                    startActivity(intent);
                }else if(reserveID==ReserveType.MY_YUEYAO)
                {
//                    intent.setClass(this, MyMusicsActivity.class);
//                    startActivity(intent);
                }

                break;
//            //预约挂号
//            case ReserveType.TYPE_REGISTRATION:
//                //跳转预约挂号
//                if (reserveID == ReserveType.REGISTRATION) {
//                    intent.setClass(this, ReserveActivity.class);
//                    startActivity(intent);
//                }
//                //跳转我的预约挂号
//                else if (reserveID == ReserveType.MY_REGISTRATION) {
//                    intent.setClass(this, MyReservationActivity.class);
//                    startActivity(intent);
//                }
//
//                break;
//            //视频预约
//            case ReserveType.TYPE_VIDEO:
//                //跳转视频预约
//                if (reserveID == ReserveType.VIDEO) {
//                    intent.setClass(this, VideoConsultActivity.class);
//                    startActivity(intent);
//                }
//                //跳转我的视频预约
//                else if (reserveID == ReserveType.MY_VIDEO) {
//                    intent.setClass(this, MyVideoReservationActivity.class);
//                    startActivity(intent);
//                }
//
//                break;
            //体质门诊
//            case ReserveType.TYPE_BODY_DIAGNOSE:
//                //跳转体质门诊
//                if (reserveID == ReserveType.BODY_DIAGNOSE) {
//                    intent.setClass(this, BodyDiagnoseActivity2.class);
//                    startActivity(intent);
//                }
//                //跳转我的预约挂号
//                else if (reserveID == ReserveType.My_BODY_DIAGNOSE) {
//                    intent.setClass(this, MyReservationActivity.class);
//                    startActivity(intent);
//                }
//
//                break;
            default:
                break;
        }
        this.finish();
    }
}
