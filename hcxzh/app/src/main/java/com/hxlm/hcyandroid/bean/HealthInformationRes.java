package com.hxlm.hcyandroid.bean;

import java.util.List;

/**
 * Created by JQ on 2017/7/21.
 */

public class HealthInformationRes {

//    @SerializedName("data")
    private List<HealthInformation> data;

    public List<HealthInformation> getData() {
        return data;
    }

    public void setData(List<HealthInformation> data) {
        this.data = data;
    }
}
