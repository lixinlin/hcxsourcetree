package com.hxlm.hcyandroid.tabbar.usercenter;

import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.AbstractBaseActivity;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.user.UserManager;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.util.ToastUtil;

/**
 * Created by l on 2016/6/13.
 * 意见反馈
 */
public class FeedbackActivity extends AbstractBaseActivity implements View.OnClickListener,TextWatcher{
    private ImageView mYijianButton;
    private EditText mYijianPingjia;

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_feedback);
    }

    @Override
    protected void initViews() {

        TitleBarView titleBarView = new TitleBarView();
        titleBarView.init(this, getString(R.string.myfrag_yijianfankui), titleBarView, 1);

        mYijianButton = (ImageView) findViewById(R.id.yijian_commit);
        mYijianPingjia = (EditText) findViewById(R.id.yijian_pingjia);
        int maxText = 50;
        mYijianPingjia.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxText)});
        mYijianButton.setOnClickListener(this);
        mYijianPingjia.addTextChangedListener(this);
        mYijianButton.setClickable(false);
    }

    @Override
    protected void initDatas() {

    }

    @Override
    public void onClick(View view) {

        if(TextUtils.isEmpty(mYijianPingjia.getText().toString()))
        {
            ToastUtil.invokeShortTimeToast(FeedbackActivity.this,getString(R.string.feadback_qingtichuninde));
        }else
        {
            String strContent=mYijianPingjia.getText().toString();
          new UserManager().submitFeedback(strContent, new AbstractDefaultHttpHandlerCallback(this) {
              @Override
              protected void onResponseSuccess(Object obj) {
                  new AlertDialogCloce(FeedbackActivity.this).createAlartDialog(getString(R.string.feadback_nindeyijian), getString(R.string.feadback_ganxienintichu)).show();

              }
          });
        }

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (TextUtils.isEmpty(mYijianPingjia.getText().toString())) {
            mYijianButton.setImageResource(R.drawable.yijian_tijiao1);
            mYijianButton.setClickable(false);
        } else {
            mYijianButton.setImageResource(R.drawable.yijian_tijiao2);
            mYijianButton.setClickable(true);
        }
    }

    // 提交成功关闭提示
     class AlertDialogCloce extends Dialog {

        private View.OnClickListener l;
        private Context context;

        public AlertDialogCloce(Context context) {
            super(context);
            this.context = context;
        }

        public Dialog createAlartDialog(String titletxt, String msg) {
            LayoutInflater inflater = LayoutInflater.from(context);
            View v = inflater.inflate(R.layout.alert_dialg_cloce, null);
            final Dialog dialog = new Dialog(context, R.style.alertdialog);
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setContentView(v);

            TextView text_title=(TextView)v.findViewById(R.id.text_title);
            TextView text_content=(TextView)v.findViewById(R.id.text_content);

            text_title.setText(titletxt);
            text_content.setText(msg);

            TextView tvcloce =  v.findViewById(R.id.tvcloce);
            if (l == null) {
                tvcloce.setOnClickListener(defaultLinstener(dialog));
            } else {
                tvcloce.setOnClickListener(l);

            }
            return dialog;
        }

        public void setDetermineOnClickListener(
                View.OnClickListener l) {
            this.l = l;
        }

        private View.OnClickListener defaultLinstener(
                final Dialog dialog) {
            return new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    switch (v.getId()) {
                        // 点击确定
                        case R.id.tvcloce:

                            FeedbackActivity.this.finish();
                            break;
                        default:
                            break;
                    }
                }
            };
        }
    }
}