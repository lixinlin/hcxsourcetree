package com.hxlm.hcyandroid.datamanager;

import android.os.Handler;
import android.os.Message;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.hcyandroid.MessageType;
import com.hxlm.hcyandroid.ResponseStatus;
import com.hxlm.hcyandroid.bean.HealthLecture;
import com.hxlm.hcyandroid.bean.MyHealthLecture;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

public class LectureManager extends BaseManager {

    public LectureManager(Handler handler) {
        super(handler);
    }

    /**
     * 获取讲座列表的接口
     */
    public void getLectures() {
        String method = METHOD_GET;
        String url = "/lecture/list.jhtml";
        RequestParams params = new RequestParams();//无参数
        queryRemoteWithoutHeader(method, url, params, new OnSuccessListener() {

            @Override
            public void onSuccess(String content) {
                List<HealthLecture> lectures = new ArrayList<HealthLecture>();
                JSONObject jo = JSON.parseObject(content);
                int status = jo.getInteger("status");
                if (status == ResponseStatus.SUCCESS) {
                    JSONArray ja = jo.getJSONArray("data");
                    lectures = JSON.parseArray(ja.toJSONString(),
                            HealthLecture.class);
                }
                Message message = Message.obtain();
                message.what = MessageType.MESSAGE_GET_LECTURE;
                message.arg1 = status;
                message.obj = lectures;
                mHandler.sendMessage(message);
            }
        });
    }

    /**
     * 预约健康讲座的接口
     *
     * @param lectureId   健康讲座id
     * @param reserveNums 预约数量
     * @param totalFee    总金额
     */
    public void reserveHealthLecture(String lectureId, int reserveNums,
                                     final double totalFee) {
        String method = METHOD_GET;
        String url = "/member/lecture/reserve.jhtml";

        //String memberId = SharedPreferenceUtil.getString("memberId");
        int memberId = LoginControllor.getLoginMember().getId();
        RequestParams params = new RequestParams();
        params.put("lectureId", lectureId);
        params.put("memberId", memberId);
        params.put("reserveNums", reserveNums);
        params.put("totalFee", totalFee);
        queryRemoteWithHeader(method, url, params, new OnSuccessListener() {

            @Override
            public void onSuccess(String content) {
                if (totalFee == 0) {
                    JSONObject jo = JSON.parseObject(content);
                    int status = jo.getInteger("status");

                    Message message = Message.obtain();
                    message.what = MessageType.MESSAGE_RESERVE_FREE_LECTURE;
                    message.arg1 = status;
                    mHandler.sendMessage(message);
                }
                // 收费讲座
                else {
                    int orderId = 0;
                    JSONObject jo = JSON.parseObject(content);
                    int status = jo.getInteger("status");
                    if (status == ResponseStatus.SUCCESS) {
                        JSONObject data = jo.getJSONObject("data");
                        JSONObject order = data.getJSONObject("order");
                        orderId = order.getIntValue("id");
                    }
                    Message message = Message.obtain();
                    message.what = MessageType.MESSAGE_RESERVE_NOT_FREE_LECTURE;
                    message.arg1 = status;
                    message.obj = orderId;
                    mHandler.sendMessage(message);
                }
            }
        });
    }

    /**
     * 获取当前用户的健康讲座
     */
    public void getMyLectures() {
        String method = METHOD_GET;
        //String memberId = SharedPreferenceUtil.getString("memberId");
        int memberId = LoginControllor.getLoginMember().getId();
        String url = "/member/lecture/list/" + memberId + ".jhtml";
        RequestParams params = new RequestParams();
        params.put("memberId", memberId);
        queryRemoteWithHeader(method, url, params, new OnSuccessListener() {

            @Override
            public void onSuccess(String content) {
                List<MyHealthLecture> myHealthLectures = new ArrayList<MyHealthLecture>();
                JSONObject jo = JSON.parseObject(content);
                int status = jo.getInteger("status");
                if (status == ResponseStatus.SUCCESS) {
                    JSONArray data = jo.getJSONArray("data");
                    myHealthLectures = JSON.parseArray(data.toJSONString(),
                            MyHealthLecture.class);
                }
                Message message = Message.obtain();
                message.what = MessageType.MESSAGE_GET_MY_LECTURE;
                message.arg1 = status;
                message.obj = myHealthLectures;
                mHandler.sendMessage(message);
            }
        });
    }

    /**
     * 取消预约的接口
     *
     * @param id
     * @param reserveType
     */
    public void cancle(final int id, int reserveType) {
        String method = METHOD_GET;
        String url = "/member/doctorSubscribe/cancel/" + id + ".jhtml";
        RequestParams params = new RequestParams();
        params.put("id", id);
        params.put("type", reserveType);
        queryRemoteWithHeader(method, url, params, new OnSuccessListener() {

            @Override
            public void onSuccess(String content) {
                String data = "";
                JSONObject jo = JSON.parseObject(content);
                int status = jo.getInteger("status");
                if (status == ResponseStatus.SUCCESS || status == 1) {
                    data = jo.getString("data");
                }
                Message message = Message.obtain();
                message.arg1 = status;
                message.arg2 = id;
                message.what = MessageType.MESSAGE_CANCLE_ORDER;
                message.obj = data;
                mHandler.sendMessage(message);
            }
        });
    }

    /**
     * 判断预约是否取消
     *
     * @param id   预约id
     * @param type 预约类型
     */
    public void checkIsCancled(int id, int type) {
        String url = "/member/doctorSubscribe/checkStatus/" + id + ".jhtml";
        String method = METHOD_GET;
        RequestParams params = new RequestParams();
        params.put("id", id);
        params.put("type", type);
        queryRemoteWithHeader(method, url, params, new OnSuccessListener() {

            @Override
            public void onSuccess(String content) {
                boolean isCancled = false;
                JSONObject jo = JSON.parseObject(content);
                int status = jo.getInteger("status");
                if (status == ResponseStatus.SUCCESS) {
                    isCancled = jo.getBoolean("data");
                }
                Message message = Message.obtain();
                message.arg1 = status;
                message.obj = isCancled;
                message.what = MessageType.MESSAGE_CHECK_IS_CANCLED;
                mHandler.sendMessage(message);
            }
        });
    }


}
