package com.hxlm.hcyandroid.tabbar.sicknesscheck;

import android.widget.ListView;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.bean.CheckStep;

import java.util.ArrayList;
import java.util.List;

/**
 * 血压使用规范
 *
 * @author l
 */
public class BloodPressureUseSpecificationActivity extends BaseActivity {

    private ListView lv_tips;
    private List<CheckStep> steps;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_blood_pressure_use_specification);

    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.bpc_title), titleBar, 1);
        lv_tips = findViewById(R.id.lv_tips);


    }

    @Override
    public void initDatas() {
        steps = new ArrayList<CheckStep>();
        steps.add(new CheckStep(R.drawable.check_step_number_bg1, "", getString(R.string.bpuf_tips)));
        steps.add(new CheckStep(R.drawable.check_step_number_bg2, "", getString(R.string.bpuf_tips)));
        steps.add(new CheckStep(R.drawable.check_step_number_bg3, "", getString(R.string.bpuf_tips)));
        steps.add(new CheckStep(R.drawable.check_step_number_bg4, "", getString(R.string.bpuf_tips)));
        steps.add(new CheckStep(R.drawable.check_step_number_bg5, "", getString(R.string.bpuf_tips)));
        steps.add(new CheckStep(R.drawable.check_step_number_bg6, "", getString(R.string.bpuf_tips)));
        lv_tips.setAdapter(new StepAdapter(this, steps));

    }


}
