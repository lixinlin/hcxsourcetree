package com.hxlm.hcyandroid.view;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.hcy.ky3h.R;
import com.hxlm.hcyandroid.BaseApplication;

/**
 * 疾病程度
 *
 * @author dell
 */
public class ChooseDiseaseDegreeDialog extends AlertDialog implements
        View.OnClickListener {
    private TextView tv_Light;//轻度
    private TextView tv_Moderate;//中度
    private TextView tv_Severe;//重度
    private String strDegree;//程度
    private OnChoosedListener listener;

    public ChooseDiseaseDegreeDialog(Context context, OnChoosedListener onChoosedListener) {
        super(context);
        listener = onChoosedListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_choose_diseasedegree);
        tv_Light = (TextView) findViewById(R.id.tv_Light);
        tv_Moderate = (TextView) findViewById(R.id.tv_Moderate);
        tv_Severe = (TextView) findViewById(R.id.tv_Severe);
        tv_Light.setOnClickListener(this);
        tv_Moderate.setOnClickListener(this);
        tv_Severe.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_Light:
                strDegree = BaseApplication.getContext().getString(R.string.dialog_choose_degree_light);
                listener.ChooseDiseaseDegree(strDegree);
                this.dismiss();
                break;
            case R.id.tv_Moderate:
                strDegree = BaseApplication.getContext().getString(R.string.dialog_choose_degree_middle);
                listener.ChooseDiseaseDegree(strDegree);
                this.dismiss();
                break;
            case R.id.tv_Severe:
                strDegree = BaseApplication.getContext().getString(R.string.dialog_choose_degree_heavy);
                listener.ChooseDiseaseDegree(strDegree);
                this.dismiss();
                break;
            default:
                break;
        }
    }

    public interface OnChoosedListener {
        void ChooseDiseaseDegree(String strDegree);

    }

}
