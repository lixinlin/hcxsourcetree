package com.hxlm.hcyandroid;

public class ReserveType {
    public static final int TYPE_LECTURE = 10;// 讲座
    public static final int TYPE_REGISTRATION = 20;// 挂号
    public static final int TYPE_VIDEO = 30;// 视频
    public static final int TYPE_CARD = 40;// 购卡
    public static final int TYPE_STATIC_RESOURCE = 50;//静态资源
    public static final int TYPE_MALL = 60;//健康商城中支付
    public static final int TYPE_BODY_DIAGNOSE = 70;//体质门诊，用于支付成功或支付页面之后的跳转逻辑，支付接口中仍然使用TYPE_REGISTRATION

    //定义变量，用于区分是从个人中心打开继续支付界面，还是从辅助就医，专家咨询或者体质门诊里面打开的
    //健康讲座
    public static final int LECTURE = 100001;
    public static final int MY_LECTURE = 100002;

    //预约挂号
    public static final int REGISTRATION = 100003;
    public static final int MY_REGISTRATION = 100004;


    //视频预约
    public static final int VIDEO = 100005;
    public static final int MY_VIDEO = 100006;

    //体质门诊
    public static final int BODY_DIAGNOSE = 100007;
    public static final int My_BODY_DIAGNOSE = 100008;

    //乐药
    public static final int YUEYAO=100009;
    public static  final int MY_YUEYAO=100010;

    //健康商城
    public static final int HEALTH_MALL=100011;
}
