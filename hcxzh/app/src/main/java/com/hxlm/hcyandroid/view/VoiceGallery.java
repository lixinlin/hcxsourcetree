package com.hxlm.hcyandroid.view;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.Toast;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.content.GongFaActivity;
import com.hxlm.android.utils.Logger;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.bean.GongFaDatas;

import java.io.File;
import java.io.IOException;

public class VoiceGallery extends Gallery {
    /**
     * 支持自动滚动
     */
    public final static int CAN_AUTO_SCROLL = 0;
    /**
     * 不支持自动滚动
     */
    public final static int CANNOT_AUTO_SCROLL = 1;
    private final static int DEFAULT_SCROLL_DURATION = 6000;
    private final static String MUSIC_PATH = Constant.SHIFANYIN_PATH;     //存放示范音下载的地址
    private final String tag = getClass().getSimpleName();
    public int nowPlayingGF = 0;

    public boolean isPlayingDemo = false;
    private Handler handler;
    private Context mContext;

    /**
     * 值大于0,则不设置自动滑动(默认为0)
     */
    private volatile int mType;
    private boolean isResume = false;

    private MediaPlayer player;
    private Runnable scrollTask;

    private int endPosion = 35;

    public VoiceGallery(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        init();
    }

    public void init() {
        if (isResume) {
            return;
        }
        isResume = true;
        if (handler == null) {
            handler = new Handler();
        }
        // 设置tag,如果tag值大于0,则不设置自动滑动(默认为0)
        Object obj = getTag();

        if (obj != null) {
            mType = Integer.valueOf(String.valueOf(obj));
        } else {
            mType = CAN_AUTO_SCROLL;
        }
        if (mType == CAN_AUTO_SCROLL) {
            newScrollTask();
        }
    }

    private void newScrollTask() {
        if (scrollTask != null) {
            handler.removeCallbacks(scrollTask);
        }
        scrollTask = new ScrollTask();
        new Thread(scrollTask).run();
    }

    private void pauseScrollTask() {
        if (scrollTask != null) {
            handler.removeCallbacks(scrollTask);
        }
        nowPlayingGF = nowPlayingGF - 1;
        onKeyDown(KeyEvent.KEYCODE_DPAD_LEFT, null);
    }

    public void stop() {
        isResume = false;
        handler.removeCallbacksAndMessages(null);
        scrollTask = null;

        stopVoice();
    }

    public void pause() {
        if (player != null) {
            player.seekTo(0);
            player.pause();
        }
        pauseScrollTask();
    }

    public void start() {
        newScrollTask();
    }

    public void stopVoice() {
        if (player != null) {
            player.release();
            player = null;
        }
    }

    public void onDestroy() {
        isResume = false;
        if (handler != null)
            handler = null;
        scrollTask = null;
    }

    public boolean isSoundEffectsEnabled() {
        return false;
    }

    public boolean performItemClick(View view, int position, long id) {
        Adapter ad = getAdapter();
        if (ad instanceof BaseAdapter) {
            BaseListAdapter adapter = (BaseListAdapter) ad;
            int count;
            count = adapter.getRealCount();
            if (count != 0) {
                position = position % count;
            }
        }

        return super.performItemClick(view, position, id);
    }

    public int getType() {
        return this.mType;
    }

    public void setType(int type) {
        this.mType = type;
        if (CAN_AUTO_SCROLL == type) {
            newScrollTask();
        }
    }

    public int getEndPosion() {
        return endPosion;
    }

    public void setEndPosion(int endPosion) {
        this.endPosion = endPosion;
    }

    public final void recoverAutoScroll() {
        if (scrollTask != null) {
            handler.removeCallbacks(scrollTask);
        }

        newScrollTask();
    }

    private class ScrollTask implements Runnable {
        @Override
        public void run() {
            if (mType != CAN_AUTO_SCROLL) {
                return;
            }

            if (nowPlayingGF != 0) {
                onKeyDown(KeyEvent.KEYCODE_DPAD_RIGHT, null);
            }

            int duration = DEFAULT_SCROLL_DURATION;
            if (nowPlayingGF < endPosion) {

                if (isPlayingDemo) {
                    Log.d(tag, "nowPlayingGF = " + nowPlayingGF);
                    Log.d(tag, "shifanyin File = " + MUSIC_PATH + "/" + GongFaDatas.GONG_FA_BIG_NAME[nowPlayingGF] + ".mp3");

                    File file = new File(MUSIC_PATH + "/" + GongFaDatas.GONG_FA_BIG_NAME[nowPlayingGF] + ".mp3");

                    if (file.exists()) {
                        Uri uri = Uri.fromFile(file);

                        try {
                            if (player != null) {
                                player.reset();
                                player.setDataSource(mContext, uri);
                                player.prepare();
                            } else {
                                player = MediaPlayer.create(mContext, uri);
                                player.prepare();
                            }
                        } catch (IllegalArgumentException | SecurityException | IllegalStateException | IOException e) {
                            Logger.e(tag, e);
                        }
                        duration = player.getDuration();
                        player.start();
                    }else {
                        Toast.makeText(mContext,
                                mContext.getString(R.string.gongfa_download_tips),
                                Toast.LENGTH_LONG).show();
                    }
                }
                scrollTask = new ScrollTask();
                handler.postDelayed(scrollTask, duration);
                Log.i(tag, "duration = " + duration);

                nowPlayingGF++;
            }
        }
    }
}
