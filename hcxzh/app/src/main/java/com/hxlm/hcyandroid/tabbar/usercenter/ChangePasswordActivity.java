package com.hxlm.hcyandroid.tabbar.usercenter;

import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.user.Member;
import com.hxlm.android.hcy.user.UserManager;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.util.StatusBarUtils;
import com.hxlm.hcyandroid.util.ToastUtil;
import com.hxlm.hcyandroid.view.ContainsEmojiEditText;

/**
 * 修改密码
 *
 * @author l
 */
public class ChangePasswordActivity extends BaseActivity implements OnClickListener {

    private ContainsEmojiEditText et_old_password;
    private ContainsEmojiEditText et_new_password;
    private ContainsEmojiEditText et_confirm_new_password;
    private String mobile;

    @Override
    public void setContentView() {

        StatusBarUtils.setWindowStatusBarColor(this,R.color.color_1e82d2);
        setContentView(R.layout.activity_change_password);
    }

    @Override
    public void initViews() {
        TextView tv_account = (TextView) findViewById(R.id.tv_account_number);
        TextView tv_account_phone = (TextView) findViewById(R.id.tv_account_phone);
        et_old_password = (ContainsEmojiEditText) findViewById(R.id.et_old_password);
        et_new_password = (ContainsEmojiEditText) findViewById(R.id.et_new_password);
        et_confirm_new_password = (ContainsEmojiEditText) findViewById(R.id.et_confirm_new_password);
        Button bt_commit = (Button) findViewById(R.id.bt_commit);
        LinearLayout linear_back = (LinearLayout) findViewById(R.id.linear_back);
        linear_back.setOnClickListener(this);
        Member member = LoginControllor.getLoginMember();
        mobile = member.getUserName();
        if (!TextUtils.isEmpty(mobile)) {
            if(mobile.length()>20){
                mobile = "";
            }
            tv_account_phone.setText(mobile);
        }
        if (!TextUtils.isEmpty(member.getName())) {
            tv_account.setText(member.getName());
        }
//        tv_account.setText(SharedPreferenceUtil.getUserName());
//        tv_account_phone.setText(SharedPreferenceUtil.getUserPhone());
        bt_commit.setOnClickListener(this);
    }

    @Override
    public void initDatas() {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_commit:
                String oldPassword = et_old_password.getText().toString();
                String newPassword = et_new_password.getText().toString();
                String confirmPassword = et_confirm_new_password.getText().toString();

                if (TextUtils.isEmpty(oldPassword) || TextUtils.isEmpty(newPassword) ||
                        TextUtils.isEmpty(confirmPassword)) {
                    ToastUtil.invokeShortTimeToast(ChangePasswordActivity.this, getString(R.string.cp_qingshuru));
                } else if (!(confirmPassword.equals(newPassword))) {
                    ToastUtil.invokeShortTimeToast(ChangePasswordActivity.this, getString(R.string.cp_ninshuru));

                } else {
                    new UserManager().modifyPassword(oldPassword, newPassword, confirmPassword,
                            new AbstractDefaultHttpHandlerCallback(this) {
                                @Override
                                protected void onResponseSuccess(Object obj) {

                                    ToastUtil.invokeShortTimeToast(ChangePasswordActivity.this, getString(R.string.cp_xiugai));
//                                    LoginControllor.logout();
                                    LoginControllor.setAutoLoginByMiMa(false,mobile,et_old_password.getText().toString().trim());
//                                    Intent intent = new Intent(ChangePasswordActivity.this, LoginActivity.class);
//                                    startActivity(intent);
                                    ChangePasswordActivity.this.finish();
                                    //startActivity(new Intent(ChangePasswordActivity.this, MainActivityJDF.class));
                                }
                            });
                }
                break;
            case R.id.linear_back:
                ChangePasswordActivity.this.finish();
                break;
            default:
                break;
        }
    }
}
