package com.hxlm.hcyandroid.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * 健康资讯
 *
 * @author Administrator
 */
public class HealthInformation implements Parcelable {

//    @SerializedName("seoDescription")
    private String seoDescription;
//    @SerializedName("id")
    private int id;
//    @SerializedName("createDate")
    private long createDate;
//    @SerializedName("modifyDate")
    private long modifyDate;
//    @SerializedName("title")
    private String title;
//    @SerializedName("author")
    private String author;
//    @SerializedName("picture")
    private String picture;
//    @SerializedName("path")
    private String path;


    public HealthInformation() {
        super();
        // TODO Auto-generated constructor stub
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(seoDescription);
        dest.writeInt(id);
        dest.writeLong(createDate);
        dest.writeLong(modifyDate);
        dest.writeString(title);
        dest.writeString(author);
        dest.writeString(path);
        dest.writeString(picture);
    }

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    public static final Creator<HealthInformation> CREATOR = new Creator<HealthInformation>() {

        @Override
        public HealthInformation createFromParcel(Parcel source) {
            return new HealthInformation(source);
        }

        @Override
        public HealthInformation[] newArray(int size) {
            return new HealthInformation[size];
        }

    };

    public HealthInformation(Parcel in) {
        seoDescription = in.readString();
        id = in.readInt();
        createDate = in.readLong();
        modifyDate = in.readLong();
        title = in.readString();
        author = in.readString();
        path = in.readString();
        picture = in.readString();
    }

    public String getSeoDescription() {
        return seoDescription;
    }

    public void setSeoDescription(String seoDescription) {
        this.seoDescription = seoDescription;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(long modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

}
