package com.hxlm.hcyandroid.view;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.hcy.ky3h.R;

public class UploadIconDialog extends AlertDialog implements
        View.OnClickListener {

    private TextView tv_from_album;
    private TextView tv_take_photo;
    private TextView tv_cancel;
    private OnOptionClickedListener listener;

    public UploadIconDialog(Context context,
                            OnOptionClickedListener onOptionClickedListener) {
        super(context);
        this.listener = onOptionClickedListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_choose_img);
        tv_from_album = (TextView) findViewById(R.id.tv_from_album);
        tv_take_photo = (TextView) findViewById(R.id.tv_take_photo);
        tv_cancel = (TextView) findViewById(R.id.tv_cancel);

        tv_from_album.setOnClickListener(this);
        tv_take_photo.setOnClickListener(this);
        tv_cancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_from_album:
                listener.chooseFromAlbum();
                dismiss();
                break;
            case R.id.tv_take_photo:
                listener.getFromCamera();
                dismiss();
                break;
            case R.id.tv_cancel:
                dismiss();
                break;
        }
    }

    public interface OnOptionClickedListener {
        void chooseFromAlbum();

        void getFromCamera();
    }

}
