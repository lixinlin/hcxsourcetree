package com.hxlm.hcyandroid.bean;



import com.hxlm.hcyandroid.tabbar.sicknesscheckecg.UploadFailedData;

import java.io.File;
import java.util.Date;

public class ECGReport implements Comparable<ECGReport> {
    private int id;
    private long createDate;
    private long modifyDate;
    private String path;
    private int heartRate;
    private File file;
    private String content;//心电建议
    private Date createTime;

    private Conclusion subject;
    //-----------------------
    private UploadFailedData uploadFailedData;

    public UploadFailedData getUploadFailedData() {
        return uploadFailedData;
    }

    public void setUploadFailedData(UploadFailedData uploadFailedData) {
        this.uploadFailedData = uploadFailedData;
    }
    //----------------


    @Override
    public String toString() {
        return "ECGReport{" +
                "id=" + id +
                ", createDate=" + createDate +
                ", modifyDate=" + modifyDate +
                ", path='" + path + '\'' +
                ", heartRate=" + heartRate +
                ", file=" + file +
                ", content='" + content + '\'' +
                ", subject=" + subject +
                ", uploadFailedData=" + uploadFailedData +
                '}';
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(long modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int heartRate) {
        this.heartRate = heartRate;
    }

    public Conclusion getSubject() {
        return subject;
    }

    public void setSubject(Conclusion subject) {
        this.subject = subject;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public int compareTo(ECGReport ecgReport) {
        return (ecgReport.getModifyDate()<this.getModifyDate()?-1:
                (ecgReport.getModifyDate()==this.getModifyDate()?0:1));
    }
}
