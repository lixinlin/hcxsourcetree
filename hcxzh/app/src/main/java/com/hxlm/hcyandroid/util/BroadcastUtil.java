package com.hxlm.hcyandroid.util;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.hxlm.hcyandroid.Constant;

public class BroadcastUtil {

    //设置一个类型ID用于区分是从哪里跳转的
    public static int BroadreserveID = -1;


    /**
     * 跳转到支付成功，发送广播，在支付成功页面判断支付类型
     *
     * @param context     上下文对象
     * @param reserveType 支付类型
     */
    public static void sendTypeToPayResult(Context context, int reserveType) {
        Intent intent = new Intent();
        intent.setAction(Constant.OTHER_PAY_SEND_TYPE_ACTION);
        intent.putExtra("reserveType", reserveType);
        intent.putExtra("reserveID", BroadreserveID);
        context.sendBroadcast(intent);

        Log.e("Log.e", "BroadcastUtil reserveType-->" + reserveType);
    }
}
