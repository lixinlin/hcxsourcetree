package com.hxlm.hcyandroid.tabbar.usercenter;

import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.order.CardManager;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.util.ToastUtil;
import com.hxlm.hcyandroid.view.ContainsEmojiEditText;

/**
 * 绑定新卡
 *
 * @author l
 */
public class BindNewCardActivity extends BaseActivity implements
        OnClickListener {
    private ContainsEmojiEditText et_card_number;
    private CardManager cardManager;


    @Override
    public void setContentView() {
        setContentView(R.layout.activity_bind_new_card);
    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, "绑定新卡", titleBar, 1);

        et_card_number = (ContainsEmojiEditText) findViewById(R.id.et_card_number);
        Button bt_bind = (Button) findViewById(R.id.bt_bind);
        bt_bind.setOnClickListener(this);
    }

    @Override
    public void initDatas() {
        cardManager = new CardManager();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_bind:
                String card_number = et_card_number.getText().toString();
                if (TextUtils.isEmpty(card_number)) {
                    ToastUtil.invokeShortTimeToast(BindNewCardActivity.this, "卡号不能为空");
                } else {
                    cardManager.bindCard(card_number, new AbstractDefaultHttpHandlerCallback(this) {
                        @Override
                        protected void onResponseSuccess(Object obj) {
                            BindNewCardActivity.this.finish();
                        }
                    });
                }

                break;
        }
    }

}
