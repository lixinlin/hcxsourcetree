package com.hxlm.hcyandroid.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.hxlm.hcyandroid.BaseApplication;


/**
 * 用户数据偏好设置帮助类（主要存储用户信息）
 * Created by JQ on 16/12/06.
 */
public class UserPreferenceHelper {
    private static UserPreferenceHelper mPreferenceHelper;
    private SharedPreferences mPreference;
    private SharedPreferences.Editor mPreferenceEditor;

    @SuppressLint("CommitPrefEdits")
    private UserPreferenceHelper() {
        mPreference = BaseApplication.getContext().getSharedPreferences("user_preference", Context.MODE_PRIVATE);
        mPreferenceEditor = mPreference.edit();
    }

    public static UserPreferenceHelper getInstance() {
        if (mPreferenceHelper == null) {
            mPreferenceHelper = new UserPreferenceHelper();
        }
        return mPreferenceHelper;
    }

    public int getInt(String key, int defaultValue) {
        return mPreference.getInt(key, defaultValue);
    }

    public void putInt(String key, int value) {
        mPreferenceEditor.putInt(key, value);
        mPreferenceEditor.commit();
    }

    public String getString(String str, String defaultValue) {
        return mPreference.getString(str, defaultValue);
    }

    public void putString(String key, String value) {
        mPreferenceEditor.putString(key, value);
        mPreferenceEditor.commit();
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        return mPreference.getBoolean(key, defaultValue);
    }

    public void putBoolean(String key, boolean value) {
        mPreferenceEditor.putBoolean(key, value);
        mPreferenceEditor.commit();
    }

    public float getFloat(String key, float defaultValue) {
        return mPreference.getFloat(key, defaultValue);
    }

    public void putFloat(String key, float value) {
        mPreferenceEditor.putFloat(key, value);
        mPreferenceEditor.commit();
    }

    public long getLong(String key, long defaultValue) {
        return mPreference.getLong(key, defaultValue);
    }

    public void putLong(String key, long value) {
        mPreferenceEditor.putLong(key, value);
        mPreferenceEditor.commit();
    }

    /**
     * 清空用户偏好设置数据
     */
    public void clearAllData() {
        mPreferenceEditor.clear().commit();
    }

}
