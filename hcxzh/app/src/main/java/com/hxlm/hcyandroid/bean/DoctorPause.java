package com.hxlm.hcyandroid.bean;

import java.io.Serializable;


public class DoctorPause implements Serializable {
    private int id;
    private String name;//名称
    private String type;// 类型（bespoke:挂号  video:视频）
    private double price;
    private String status;
    private Hospital hospital;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Hospital getHospital() {
        return hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "DoctorPause [id=" + id + ", name=" + name + ", type=" + type
                + ", price=" + price + ", status=" + status + ", hospital="
                + hospital + "]";
    }


}
