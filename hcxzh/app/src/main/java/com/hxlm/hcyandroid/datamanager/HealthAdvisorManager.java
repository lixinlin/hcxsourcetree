package com.hxlm.hcyandroid.datamanager;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.hxlm.android.utils.Logger;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.MessageType;
import com.hxlm.hcyandroid.ResponseStatus;
import com.hxlm.hcyandroid.bean.HealthAdvisor;
import com.hxlm.hcyandroid.bean.HealthAdvisorCategory;
import com.hxlm.hcyandroid.bean.HealthAdvisorLevel;
import com.hxlm.hcyandroid.bean.HealthTip;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.CheckedOutputStream;

public class HealthAdvisorManager extends BaseManager {

    public HealthAdvisorManager(Handler handler) {
        super(handler);
    }

    /**
     * 获取会员的健康顾问分类
     */
    public void getMemberHealthAdviserCategory() {
        String method = METHOD_GET;
        String url = "/member/healthAdvisor/categoryList.jhtml";
        RequestParams params = new RequestParams();
        queryRemoteWithHeader(method, url, params, new OnSuccessListener() {

            @Override
            public void onSuccess(String content) {
                List<HealthAdvisorCategory> healthAdviserCategories = new ArrayList<HealthAdvisorCategory>();
                JSONObject jo = JSON.parseObject(content);
                int status = jo.getInteger("status");
                if (status == ResponseStatus.SUCCESS) {
                    JSONArray data = jo.getJSONArray("data");
                    healthAdviserCategories = JSON.parseArray(
                            data.toJSONString(), HealthAdvisorCategory.class);
                }
                Message message = Message.obtain();
                message.arg1 = status;
                message.what = MessageType.MESSAGE_GET_HEALTH_ADVISER_CATEGORY;
                message.obj = healthAdviserCategories;
                mHandler.sendMessage(message);
            }
        });
    }

    /**
     * 设置健康顾问
     *
     * @param advisorId 健康顾问id
     */
    public void setMemberHealthAdviser(int advisorId) {
        String method = METHOD_GET;
        String url = "/member/healthAdvisor/setAdvisor.jhtml";
        RequestParams params = new RequestParams();
        params.put("advisorId", advisorId);
        queryRemoteWithHeader(method, url, params, new OnSuccessListener() {

            @Override
            public void onSuccess(String content) {
                JSONObject jo = JSON.parseObject(content);
                int status = jo.getInteger("status");
                Message message = Message.obtain();
                message.what = MessageType.MESSAGE_SET_HEALTH_ADVISOR;
                message.arg1 = status;
                mHandler.sendMessage(message);
            }
        });
    }

    /**
     * 根据健康顾问分类获取当前成员的健康顾问列表
     *
     * @param categoryId 健康顾问分类
     */
    public void getHealthAdvisorsByCategory(int categoryId, Integer pageNum) {
        String method = METHOD_GET;
        String url = "/member/healthAdvisor/healthAdvisorList.jhtml";
        RequestParams params = new RequestParams();
        params.put("categoryId", categoryId);
        params.put("pageNum", pageNum);
        queryRemoteWithHeader(method, url, params, new OnSuccessListener() {

            @Override
            public void onSuccess(String content) {
                List<HealthAdvisor> healthAdvisors = new ArrayList<HealthAdvisor>();
                int totalPages = -1;
                JSONObject jo = JSON.parseObject(content);
                int status = jo.getInteger("status");
                if (status == ResponseStatus.SUCCESS) {
                    JSONObject data = jo.getJSONObject("data");
                    totalPages = data.getInteger("totalPages");
                    JSONArray ja_content = data.getJSONArray("content");
                    healthAdvisors = JSON.parseArray(ja_content.toJSONString(),
                            HealthAdvisor.class);
                    for (int i = 0; i < ja_content.size(); i++) {
                        String isYanHuang = ja_content.getJSONObject(i)
                                .getString("isYanHuang");
                        if (!TextUtils.isEmpty(isYanHuang) && isYanHuang.equals("true")) {
                            healthAdvisors.get(i).setYanHuang(true);
                        } else {
                            healthAdvisors.get(i).setYanHuang(false);
                        }
                    }
                }
                Message message = Message.obtain();
                message.what = MessageType.MESSAGE_GET_HEALTH_ADVISORS_BY_CATEGORY;
                message.arg1 = status;
                message.arg2 = totalPages;
                message.obj = healthAdvisors;
                mHandler.sendMessage(message);
            }
        });
    }

    /**
     * 根据健康顾问分类id获取当前用户选择的健康顾问
     *
     * @param categoryId 健康顾问分类id
     */
    public void getSelectedHealthAdvisorByCategory(Integer categoryId) {
        String method = METHOD_GET;
        String url = "/member/healthAdvisor/getAdvisor.jhtml";
        RequestParams params = new RequestParams();
        if (categoryId != null) {
            params.put("categoryId", categoryId);
        }
        queryRemoteWithHeader(method, url, params, new OnSuccessListener() {

            @Override
            public void onSuccess(String content) {

                List<HealthAdvisor> healthAdvisors = new ArrayList<HealthAdvisor>();
                JSONObject jo = JSON.parseObject(content);
                int status = jo.getInteger("status");
                if (status == ResponseStatus.SUCCESS) {
                    JSONArray data = jo.getJSONArray("data");
                    if (data != null) {
                        healthAdvisors = JSON.parseArray(data.toJSONString(),
                                HealthAdvisor.class);
                    }
                }
                Message message = Message.obtain();
                message.what = MessageType.MESSAGE_GET_SELECTED_HEALTH_ADVISOR_BY_CATEGORY;
                message.arg1 = status;
                message.obj = healthAdvisors;
                mHandler.sendMessage(message);
            }
        });
    }

    /**
     * 获取该成员的健康顾问等级
     */
    public void getHealthAdvisorLevel() {
        String method = METHOD_GET;
        String url = "/member/healthAdvisor/serviceLevelList.jhtml";
        RequestParams params = new RequestParams();
        Logger.i("Log.i","获得星级--->"+ Constant.BASE_URL+url);
        queryRemoteWithHeader(method, url, params, new OnSuccessListener() {

            @Override
            public void onSuccess(String content) {
                HealthAdvisorLevel level = null;
                JSONObject jo = JSON.parseObject(content);
                int status = jo.getInteger("status");
                if (status == ResponseStatus.SUCCESS) {
                    JSONObject data = jo.getJSONObject("data");
                    if (data != null) {
                        level = JSON.parseObject(data.toJSONString(),
                                HealthAdvisorLevel.class);
                    }
                }
                Message message = Message.obtain();
                message.what = MessageType.MESSAGE_GET_HEALTH_ADVISOR_LEVEL;
                message.arg1 = status;
                message.obj = level;
                mHandler.sendMessage(message);
            }
        });
    }

    public void getHealthTips(Integer pageNumber) {
        String method = METHOD_GET;
        String url = "/member/healthAdvisor/healthTipList.jhtml";
        RequestParams params = new RequestParams();
        if (pageNumber != null) {
            params.put("pageNumber", pageNumber);
        }
        queryRemoteWithHeader(method, url, params, new OnSuccessListener() {

            @Override
            public void onSuccess(String content) {
                List<HealthTip> healthTips = new ArrayList<HealthTip>();
                JSONObject jo = JSON.parseObject(content);
                int status = jo.getInteger("status");
                int totalPages = -1;
                if (status == ResponseStatus.SUCCESS) {
                    JSONObject data = jo.getJSONObject("data");
                    JSONArray ja_content = data.getJSONArray("content");
                    healthTips = JSON.parseArray(ja_content.toJSONString(), HealthTip.class);
                    totalPages = data.getInteger("totalPages");
                }
                Message message = Message.obtain();
                message.what = MessageType.MESSAGE_GET_HEALTH_TIPS;
                message.arg1 = status;
                message.arg2 = totalPages;
                message.obj = healthTips;
                mHandler.sendMessage(message);
            }
        });
    }

}
