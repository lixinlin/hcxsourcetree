package com.hxlm.hcyandroid;

import android.content.Intent;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.util.WebViewUtil;
import com.hcy.ky3h.R;

/**
 * 广告地址
 *
 * @author l
 */
public class AdvertisementActivity extends BaseActivity {

    private ProgressBar progressBar;
    private WebView wv_report;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_advertisement);
    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(AdvertisementActivity.this, getString(R.string.advertisement_title), titleBar, 1);
        wv_report = (WebView) findViewById(R.id.wv_contentNews);
        progressBar=(ProgressBar)findViewById(R.id.progressbar);
       // new WebViewUtil().setWebView(wv_report, this);


        Intent intent = getIntent();
        String url = intent.getStringExtra("newsUrl");
      //  wv_report.loadUrl(url);


        //-----------------重新定义
        new WebViewUtil().setWebViewInit(wv_report,progressBar,this,url);

    }

    @Override
    public void initDatas() {

    }

}
