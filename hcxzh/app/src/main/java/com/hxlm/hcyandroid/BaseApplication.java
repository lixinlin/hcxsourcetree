package com.hxlm.hcyandroid;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.multidex.MultiDex;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.BaseAdapter;

import com.hcy.ky3h.collectdata.CDRequestUtils;
import com.hcy_futejia.utils.DateUtil;
import com.hhmedic.android.sdk.HHDoctor;
import com.hhmedic.android.sdk.config.HHSDKOptions;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.ijiu.IJiuManager;
import com.hxlm.android.hcy.user.ChildMember;
import com.hxlm.android.hcy.user.Member;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.utils.CrashHandler;
import com.hxlm.android.hcy.utils.SpUtils;
import com.hxlm.android.utils.ConstantHealth;
import com.hxlm.database.GreenDaoManager;
import com.hxlm.hcyandroid.util.LanguageUtil;
import com.jiudaifu.moxademo.jiuliaoData.ActiviteInfo;
import com.jiudaifu.moxademo.jiuliaoData.ErrorLog;
import com.jiudaifu.moxademo.jiuliaoData.I9DataHttp;
import com.jiudaifu.moxademo.jiuliaoData.I9DataJDF;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.telink.TelinkApplication;

import com.umeng.commonsdk.UMConfigure;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;


public class BaseApplication extends TelinkApplication {
    private Member currentMember;
    private ChildMember chooseChildMember;
    public static int screenWidth;
    public static int screenHeight;
    private static BaseApplication mContext;
    public static Context instance;
    private BaseAdapter adapter;
    private Timer timer;
    private Date date;
    private long lastTime;
    private I9DataHttp i9DataHttp;
    private List<I9DataHttp> i9DataHttpList;
    private static Map<String,Activity> destoryMap = new HashMap<>();
    private long startTime;

    public Member getCurrentMember() {
        return currentMember;
    }

    public void setCurrentMember(Member currentMember) {
        this.currentMember = currentMember;
    }

    public ChildMember getChooseChildMember() {
        return chooseChildMember;
    }

    public void setChooseChildMember(ChildMember chooseChildMember) {
        this.chooseChildMember = chooseChildMember;
    }

    public static BaseApplication getInstance() {
        return mContext;
    }

    public static Context getContext() {
        return instance;
    }

    /**
     * 将要销毁的activity添加到集合
     * @param activity
     * @param activityName
     */
    public static void addDestoryActivity(Activity activity,String activityName) {
        destoryMap.put(activityName,activity);
    }

    /**
     * 销毁activity
     * @param activityName
     */
    public static void destoryActivity(String activityName) {
        Set<String> keySet=destoryMap.keySet();
        for (String key:keySet){
            destoryMap.get(key).finish();
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        instance = getApplicationContext();
        GreenDaoManager.setDatabase(mContext);
        //注册activity生命周期监听回调
        registerActivityLifecycleCallbacks(callbacks);

        startTime = System.currentTimeMillis();
        SpUtils.put(instance,"startTime",startTime);

        if (LanguageUtil.getInstance().getAppLocale(instance).getLanguage().equals("en")){
            Constant.isEnglish = true;
            ConstantHealth.isEnglish = true;
        }else {
            Constant.isEnglish = false;
            ConstantHealth.isEnglish = false;
        }

//        Bugly.init(this,"9085919193",true);
//        CrashReport.initCrashReport(getApplicationContext(), "9085919193", false);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                this)
                .threadPoolSize(3)

                // 线程池内加载的数量
                .threadPriority(Thread.NORM_PRIORITY - 2)

                // 设置线程的优先级
                .denyCacheImageMultipleSizesInMemory()

                // 当同一个Uri获取不同大小的图片，缓存到内存时，只缓存一个。默
                .diskCacheSize(10 * 1024 * 1024)

                //    .discCacheFileNameGenerator(new Md5FileNameGenerator())
                // 将保存的时候的URI名称用MD5 加密
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .diskCacheFileCount(60) // 缓存的文件数量

                // .discCache(new UnlimitedDiscCache(cacheDir))//自定义缓存路径
                // .defaultDisplayImageOptions(DisplayImageOptions.createSimple())
                // .imageDownloader(new BaseImageDownloader(this,5 * 1000, 30 *
                // 1000)) // connectTimeout (5 s), readTimeout (30 s)超时时间
                .writeDebugLogs() // Remove for releaseapp
                .build();// 开始构建
        ImageLoader.getInstance().init(config);

        DisplayMetrics dm = getResources().getDisplayMetrics();

        screenWidth = dm.widthPixels;
        screenHeight = dm.heightPixels;

        /**
         * 设置组件化的Log开关
         * 参数: boolean 默认为false，如需查看LOG设置为true
         */
        UMConfigure.setLogEnabled(true);
//        捕获异常上传
        CrashHandler crashHandler = CrashHandler.getInstance();
        crashHandler.init(this);

//注册EventBus
        boolean registered = EventBus.getDefault().isRegistered(this);
        if (!registered) {
            EventBus.getDefault().register(this);
        }
//      和缓初始化
        initSDK();

    }

    ActivityLifecycleCallbacks callbacks = new ActivityLifecycleCallbacks() {
        @Override
        public void onActivityCreated(Activity activity, Bundle bundle) {
            //强制修改应用语言
            if (!LanguageUtil.getInstance().isSameWithSetting(activity)) {
                LanguageUtil.changeAppLanguage(activity,
                        LanguageUtil.getInstance().getAppLocale(activity));
                Log.e("TAG", "language = " + LanguageUtil.getInstance().getAppLocale(activity).getLanguage()
                        + ";country = " + LanguageUtil.getInstance().getAppLocale(activity).getCountry());
            }
        }

        @Override
        public void onActivityStarted(Activity activity) {

        }

        @Override
        public void onActivityResumed(Activity activity) {

        }

        @Override
        public void onActivityPaused(Activity activity) {

        }

        @Override
        public void onActivityStopped(Activity activity) {

        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {

        }
    };

    private void initSDK() {

        HHSDKOptions options = new HHSDKOptions("8249"); //productId是和缓分配的产品Id
        options.isDebug = false;
        options.dev = false;

        HHDoctor.init(getApplicationContext(), options);
    }

    //------------------------------------------------完全退出

    public BaseAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(BaseAdapter adapter) {
        this.adapter = adapter;
    }

    public static boolean isApkDebugable() {
        try {

            ApplicationInfo info = mContext.getApplicationInfo();
            return (info.flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
        } catch (Exception e) {

        }
        return false;
    }

    @Override
    public void onTerminate() {
        EventBus.getDefault().unregister(this);
        if (timer != null) {
            timer.cancel();
        }

        super.onTerminate();
    }

    /**
     * 接收艾灸激活事件
     */
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ActiviteInfo info) {
        new IJiuManager().sumbitActivitedData(info, new AbstractDefaultHttpHandlerCallback(mContext) {
            @Override
            protected void onResponseSuccess(Object obj) {
//                Log.d("ijiuMangager", "jihuo onResponseSuccess: ");
            }
        });
    }


    /**
     * 接收灸疗数据
     * 从第一个灸疗信息开始收集，每3分钟后提交一次，提交后清空，空集合不提交。
     * onTerminate中取消timer
     */
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onMessageEvent(I9DataHttp i9DataHttp) {


//       收集灸疗信息
        if (i9DataHttpList == null) {
            i9DataHttpList = new ArrayList<>();
        }
        if (timer == null) {
            timer = new Timer();
            timer.schedule(task, 3 * 60 * 1000, 3* 60 * 1000);
        }

        if(i9DataHttpList.size()>0){
            for (int i = 0; i < i9DataHttpList.size(); i++) {
                I9DataHttp data = i9DataHttpList.get(i);
                String deviceSn = i9DataHttp.getDeviceSn();
                String num = i9DataHttp.getNum();
                if (data.getDeviceSn().equals(deviceSn) && data.getNum().equals(num)) {
                    i9DataHttpList.remove(i);
                    i9DataHttpList.add(i9DataHttp);
                } else {
                    i9DataHttpList.add(i9DataHttp);
                }
            }
        }else {
            i9DataHttpList.add(i9DataHttp);
        }

    }

    TimerTask task = new TimerTask() {
        @Override
        public void run() {
            if (i9DataHttpList.size() > 0) {
                new Handler(Looper.getMainLooper()).post(runnable);
//                Log.d("baseApplication", "submitI9date: ");
            }
//            Log.d("baseApplication", "run: ");
        }
    };

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            submitI9date(i9DataHttpList);
            i9DataHttpList.clear();
        }
    };

    /**
     * 接收灸疗异常数据
     */
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ErrorLog info) {
        List<ErrorLog> errorLogs = new ArrayList<>();
        errorLogs.add(info);
        new IJiuManager().sumbitIJiuError(errorLogs, new AbstractDefaultHttpHandlerCallback(mContext) {
            @Override
            protected void onResponseSuccess(Object obj) {
                Log.d("ijiuMangager", "i9data onResponseSuccess: ");
            }
        });
    }

    /**
     * 接收到上传给JDF的灸疗数据
     */
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onMessageEvent(I9DataJDF info) {
        List<I9DataJDF> i9DataJDFList = new ArrayList<>();
        i9DataJDFList.add(info);

        new IJiuManager().sumbitDataforjiudaifutest(i9DataJDFList, new AbstractDefaultHttpHandlerCallback(mContext) {
            @Override
            protected void onResponseSuccess(Object obj) {
                Log.d("ijiuMangager", "I9DataJDF onResponseSuccess: ");
            }
        });
    }

    private void submitI9date(List<I9DataHttp> data) {
        new IJiuManager().sumbitDatatest(data, new AbstractDefaultHttpHandlerCallback(mContext) {
            @Override
            protected void onResponseSuccess(Object obj) {
            }
        });
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }

}
