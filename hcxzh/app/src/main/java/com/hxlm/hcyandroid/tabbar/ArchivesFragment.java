package com.hxlm.hcyandroid.tabbar;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hhmedic.activity.ViewDetailAct;
import com.hhmedic.android.sdk.HHDoctor;
import com.hhmedic.bean.MemberInfoBean;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.message.MessageManager;
import com.hxlm.android.hcy.report.AcricheNewBean;
import com.hxlm.android.hcy.report.ArchivesBean;
import com.hxlm.android.hcy.report.Case;
import com.hxlm.android.hcy.report.CaseManager;
import com.hxlm.android.hcy.report.QuaterReport;
import com.hxlm.android.hcy.report.RecordManager;
import com.hxlm.android.hcy.report.Report;
import com.hxlm.android.hcy.report.RiskManager;
import com.hxlm.android.hcy.user.HhUserManager;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.android.health.device.view.ECGDrawWaveManager;
import com.hxlm.hcyandroid.CheckedDataType;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.IdentityCategory;
import com.hxlm.hcyandroid.bean.ArchivesItem;
import com.hxlm.hcyandroid.bean.ECGReport;
import com.hxlm.hcyandroid.bean.VisceraIdentityResult;
import com.hxlm.hcyandroid.tabbar.archives.ECGReviewActivity;
import com.hxlm.hcyandroid.tabbar.archives.ReportInfoActivity;
import com.hxlm.hcyandroid.tabbar.usercenter.ArchivesBaogaoAdapter;
import com.hxlm.hcyandroid.util.SharedPreferenceUtil;
import com.hxlm.hcyandroid.util.StatusBarUtils;
import com.hxlm.hcyandroid.util.ToastUtil;
import com.hxlm.hcyandroid.util.WebViewUtil;
import com.hxlm.hcyandroid.view.LoadMoreListView;
import com.orhanobut.logger.Logger;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;


/**
 * 档案
 *
 * @author l
 */
public class ArchivesFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener {
    private DrawerLayout dl;
    private LinearLayout right_layout;
    private ImageView toorbar;
    public final static String CHILD_ID_BUNDLE_NAME = "memberChildId";
    private RadioButton rb_new, rb_meridian, rb_body, rb_viscera, rb_ECG,
            rb_blood_pressure, rb_SPO2H, rb_blood, rb_temperature,
            rb_breath, rb_quarterly, rb_case, rb_all;
    private RelativeLayout rl_no_data;
//    private FragmentManager fragmentManager;
//    private FragmentTransaction transaction;
//    private NewRecordFragment newFragment;// 最新
//    private MeridianRecordFragment meridianFragment;// 经络
//    private BodyRecordFragment bodyFragment;// 体质
//    private VisceraRecordFragment visceraFragment;// 脏腑
//    private PsychologicalRecordFragment psychologicalFragment;// 心理
//    private ECGFragment ecgFragment;//心电
//    private BloodPressureFragment bloodPressureFragment;//血压
//    private SPO2HFragment SPO2HFragment;//血氧
//    private TemperatureFragment temperatureFragment;//体温
//    private BreathFragment breathFragment;//呼吸
//    private BloodFragment bloodSugarFragment;//血糖


    //得到家庭成员存放的字段
    private String otherReport = "";

    private String title = "";
    private int memberChildId = 0;
    private int newMemberChildId = 0;
    private int jump = 0;
    private TextView tv_queding;
    private List<RadioButton> rbGroup;
    private LoadMoreListView lv_bao_gao;
    private ArchivesBaogaoAdapter archivesBaogaoAdapter;
    private RadioButton clickedRB;
    private RadioButton clickedRB2;
    private List<ArchivesItem> archivesItems;

    private RecordManager recordManager;
    private RiskManager riskManager;
    private CaseManager caseManager;
    private String tag;
    private String jianceTag;
    private int pageNumber;//当前加载页数
    private boolean noMore = false;//当前加载页数
    private List<Report> reportList;
    private List<VisceraIdentityResult> reportVisceraList;//脏腑
    private List<ECGReport> reportECGRList;//心电
    private AcricheNewBean acricheNewBean;//最新
    private List<ArchivesBean> archivesBeanList;//全量
    private List<Case> caseList;//最新
    private LinearLayout report_web_view;
    private ProgressBar progressBar;
    private WebView wv_report;
    int dataType;//h5report的类别标记
    //    private CircleImageView mIvFamilies;
    private String userToken;
    private String hhuid;
    private HhUserManager hhUserManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private final int LOAD_MESSAGE = 1020;
    public final String ECG_FILE_BUNDLE_NAME = "ecgFileName";
    public final String ECG_FILE_PATH = "/ECG";
    private boolean fresh = false;  //检测页面跳转的fresh= true

    private MessageManager messageManager = new MessageManager();//消息message

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case LOAD_MESSAGE:
                    String strLoadPathName = (String) msg.obj;
                    Intent intent = new Intent(getActivity(), ECGReviewActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(ECG_FILE_BUNDLE_NAME, strLoadPathName);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    break;
                default:
                    break;
            }
        }
    };

    public ArchivesFragment() {
        super();
    }

    public void getInstance(Bundle bundle) {
        otherReport = bundle.getString("otherReport");
        memberChildId = bundle.getInt("memberChildId", 0);
        jump = bundle.getInt("jump", 0);
        title = bundle.getString("title");

        jianceTag = bundle.getString("tag");
        if (!TextUtils.isEmpty(jianceTag)) {
            fresh = true;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        onCreate先于getview方法。
//        TitleBarView titleBar = new TitleBarView();
//        titleBar.init(getActivity(), "", titleBar, 0);
        //得到传递的数据

        initData();

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            //隐藏时使用
        } else {
            //显示时使用,不刷新，下拉刷新。
            newMemberChildId = LoginControllor.getChoosedChildMember().getId();
            if (memberChildId != 0 || newMemberChildId != 0) {
                if (memberChildId != newMemberChildId) {
                    memberChildId = newMemberChildId;
                    loadChangeMember();
                }
            }

            final TitleBarView titleBar = new TitleBarView();
            titleBar.init(getActivity(), getString2(R.string.archives_frag_jiankangdangan), titleBar, 0, new OnCompleteListener() {
                @Override
                public void onComplete() {
                    memberChildId = LoginControllor.getChoosedChildMember().getId();
                    // 进入该界面默认展示最新报告
                    loadChangeMember();
                }
            });

        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (fresh) {
            tag = jianceTag;
            refeshByTag(this.tag);
            fresh = false;
        }
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    private void initData() {
        recordManager = new RecordManager();
        tag = "all";
        pageNumber = 1;
        archivesItems = new ArrayList<>();
        if (memberChildId == 0) {
            memberChildId = LoginControllor.getChoosedChildMember().getId();
        }
//        getNewReport();
        getAllReport();
        riskManager = new RiskManager();
        caseManager = new CaseManager();

        reportVisceraList = new ArrayList<>();
        reportECGRList = new ArrayList<>();
        reportList = new ArrayList<>();
        hhUserManager = new HhUserManager();
        archivesBeanList = new ArrayList<>();

    }

    private void getNewReport() {
        recordManager.getReportListByTag(memberChildId, tag, IdentityCategory.MERIDIAN_IDENTITY, pageNumber + "",
                new AbstractDefaultHttpHandlerCallback(getActivity()) {
                    @Override
                    protected void onResponseSuccess(Object obj) {
                        acricheNewBean = (AcricheNewBean) obj;
                        archivesItems = parseNewData(acricheNewBean, true);
                        archivesBaogaoAdapter.setMyList(archivesItems);
                        lv_bao_gao.setEmptyView(rl_no_data);
                        lv_bao_gao.invalidate();
                        lv_bao_gao.postInvalidate();
                    }

                    @Override
                    protected void onResponseError(int errorCode, String errorDesc) {
                        super.onResponseError(errorCode, errorDesc);
                        lv_bao_gao.setEmptyView(rl_no_data);
                        lv_bao_gao.invalidate();
                        lv_bao_gao.postInvalidate();
                    }

                    @Override
                    protected void onHttpError(int httpErrorCode) {
                        super.onHttpError(httpErrorCode);
                        lv_bao_gao.setEmptyView(rl_no_data);
                        lv_bao_gao.invalidate();
                        lv_bao_gao.postInvalidate();
                    }
                });
    }

    public void getAllReport() {
        recordManager.getReportListByTag(memberChildId, tag, "", pageNumber + "",
                new AbstractDefaultHttpHandlerCallback(getActivity()) {
                    @Override
                    protected void onResponseSuccess(Object obj) {
                        archivesBeanList = (List<ArchivesBean>) obj;
                        if (archivesBeanList.size() == 0) {
                            archivesItems.clear();
                            archivesBaogaoAdapter.setMyList(archivesItems);
                        } else {
                            archivesItems = parseAllArchiveBean(archivesBeanList, true);
                            archivesBaogaoAdapter.setMyList(archivesItems);
                        }

                    }

                    @Override
                    protected void onHttpError(int httpErrorCode) {
                        super.onHttpError(httpErrorCode);
                        archivesItems.clear();
                        archivesBaogaoAdapter.setMyList(archivesItems);
                    }
                });
    }

    private ArchivesItem initQuaterReport(String year, int quater) {
        ArchivesItem currentQuart = new ArchivesItem(0);
        currentQuart.setYearNum(year);
        if (quater == 1) {
            currentQuart.setQuarter(getResources().getString(R.string.quater_report_one));
            String s = String.format(getResources().getString(R.string.quater_report_one_year), year, year);
            currentQuart.setYear(s);
            currentQuart.setBackgroundId(R.drawable.bg_quater_report_one);
            currentQuart.setQuaterNum("1");
        } else if (quater == 2) {
            currentQuart.setQuarter(getResources().getString(R.string.quater_report_two));
            String s = String.format(getResources().getString(R.string.quater_report_two_year), year, year);
            currentQuart.setYear(s);
            currentQuart.setBackgroundId(R.drawable.bg_quater_report_two);
            currentQuart.setQuaterNum("2");
        } else if (quater == 3) {
            currentQuart.setQuarter(getResources().getString(R.string.quater_report_three));
            String s = String.format(getResources().getString(R.string.quater_report_three_year), year, year);
            currentQuart.setYear(s);
            currentQuart.setBackgroundId(R.drawable.bg_quater_report_three);
            currentQuart.setQuaterNum("3");
        } else if (quater == 4) {
            currentQuart.setQuarter(getResources().getString(R.string.quater_report_four));
            String s = String.format(getResources().getString(R.string.quater_report_four_year), year, year);
            currentQuart.setYear(s);
            currentQuart.setBackgroundId(R.drawable.bg_quater_report_four);
            currentQuart.setQuaterNum("4");
        }
        return currentQuart;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.archives_fragment, container, false);

        final TitleBarView titleBar = new TitleBarView();

        titleBar.init(getActivity(), getString2(R.string.archives_frag_jiankangdangan), titleBar, 0, new OnCompleteListener() {

            @Override
            public void onComplete() {
                memberChildId = LoginControllor.getChoosedChildMember().getId();
                // 进入该界面默认展示最新报告
                loadChangeMember();
            }
        });
        clickedRB2 = new RadioButton(getActivity());
        clickedRB = clickedRB2;
        rb_new = view.findViewById(R.id.rb_new);// 最新
        rb_meridian = view.findViewById(R.id.rb_meridian);// 经络
        rb_body = view.findViewById(R.id.rb_body);// 体质
        rb_viscera = view.findViewById(R.id.rb_viscera);// 脏腑
        rb_ECG = view.findViewById(R.id.rb_ECG);// 心电
        rb_blood_pressure = view
                .findViewById(R.id.rb_blood_pressure);// 血压
        rb_SPO2H = view.findViewById(R.id.rb_SPO2H);// 血氧
        rb_blood = view.findViewById(R.id.rb_blood);// 血糖
        rb_temperature = view.findViewById(R.id.rb_temperature);// 体温

        rb_breath = view.findViewById(R.id.rb_breath);// 呼吸
        // 呼吸
        rb_quarterly = view.findViewById(R.id.rb_quarterly);
        rb_case = view.findViewById(R.id.rb_case);
        rb_all = view.findViewById(R.id.rb_all);
        rb_all.setChecked(true);
        view.findViewById(R.id.in_web_view);


        rbGroup = new ArrayList(12);
        rbGroup.add(rb_new);
        rbGroup.add(rb_meridian);
        rbGroup.add(rb_body);
        rbGroup.add(rb_viscera);
        rbGroup.add(rb_ECG);
        rbGroup.add(rb_blood_pressure);
        rbGroup.add(rb_SPO2H);
        rbGroup.add(rb_blood);
        rbGroup.add(rb_temperature);
        rbGroup.add(rb_breath);
        rbGroup.add(rb_quarterly);
        rbGroup.add(rb_case);
        rbGroup.add(rb_all);
        initRbGroup();

        dl = view.findViewById(R.id.right_drawer);
        toorbar = view.findViewById(R.id.iv_toorbal);
        right_layout = view.findViewById(R.id.right_layout);
        tv_queding = view.findViewById(R.id.tv_queding);
        lv_bao_gao = view.findViewById(R.id.lv_bao_gao);
        swipeRefreshLayout = view.findViewById(R.id.sw);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refesh();
            }
        });
        report_web_view = view.findViewById(R.id.report_web_view);
        report_web_view.setVisibility(View.GONE);
        progressBar = view.findViewById(R.id.progressbar);
        wv_report = view.findViewById(R.id.wv_report);
        rl_no_data = view.findViewById(R.id.rl_no_data);

        archivesBaogaoAdapter = new ArchivesBaogaoAdapter(getActivity());
        lv_bao_gao.setAdapter(archivesBaogaoAdapter);
        lv_bao_gao.setEmptyView(rl_no_data);
        lv_bao_gao.setOnItemClickListener(this);
        lv_bao_gao.setOnLoadMoreListener(new LoadMoreListView.OnLoadMoreListener() {
            @Override
            public void onloadMore() {
                loadMore();
            }
        });
        dl.setDrawerShadow(R.drawable.bg2_d_an, GravityCompat.END);
//        dl.setScrimColor(Color.TRANSPARENT);
        dl.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        dl.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // 打开手势滑动
                dl.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                if(Constant.isEnglish){
                    CharSequence charSequence = autoSplitText(rb_body);
                    rb_body.setText(charSequence);
                }
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                // 关闭手势滑动
                dl.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                toorbar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
        toorbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!dl.isDrawerOpen(right_layout)) {
                    dl.openDrawer(right_layout);
                    toorbar.setVisibility(View.INVISIBLE);
                }
            }
        });

        tv_queding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeRightLayout();
            }
        });
        return view;
    }

    private void closeRightLayout() {
        if (dl.isDrawerOpen(right_layout)) {
            dl.closeDrawer(right_layout);
        }
    }

    private void loadMore() {
        pageNumber++;
        if (!noMore) {
            switch (tag) {
                case "all":
                    recordManager.getReportListByTag(memberChildId, tag, "", pageNumber + "",
                            new AbstractDefaultHttpHandlerCallback(getActivity()) {
                                @Override
                                protected void onResponseSuccess(Object obj) {
                                    archivesBeanList = (List<ArchivesBean>) obj;
                                    if (archivesBeanList.size() == 0) {
                                        noMore = true;
                                    } else {
                                        archivesItems = parseAllArchiveBean(archivesBeanList, false);
                                        archivesBaogaoAdapter.setMyList(archivesItems);
                                    }

                                }
                            });
                    break;
                //体质
                case "body":
                    recordManager.getReportListByTag(memberChildId, tag, IdentityCategory.BODY_IDENTITY, pageNumber + "",
                            new AbstractDefaultHttpHandlerCallback(getActivity()) {
                                @Override
                                protected void onResponseSuccess(Object obj) {
                                    reportList = (List<Report>) obj;
                                    if (reportList.size() == 0) {
                                        noMore = true;
                                    } else {
                                        archivesItems = parseData(reportList, false);
                                        archivesBaogaoAdapter.setMyList(archivesItems);
                                    }
                                }
                            });

                    break;
                //心率
                case "rate":
                    recordManager.getReportListByTag(memberChildId, tag, "", pageNumber + "",
                            new AbstractDefaultHttpHandlerCallback(getActivity()) {
                                @Override
                                protected void onResponseSuccess(Object obj) {
                                    reportECGRList = (List<ECGReport>) obj;
                                    if (reportList.size() == 0) {
                                        noMore = true;
                                    } else {
                                        archivesItems = parseECGRData(reportECGRList, false);
                                        archivesBaogaoAdapter.setMyList(archivesItems);
                                    }

                                }
                            });
                    break;
                //经络
                case "meridian":
                    recordManager.getReportListByTag(memberChildId, tag, IdentityCategory.MERIDIAN_IDENTITY, pageNumber + "",
                            new AbstractDefaultHttpHandlerCallback(getActivity()) {
                                @Override
                                protected void onResponseSuccess(Object obj) {
                                    reportList = (List<Report>) obj;
                                    if (reportList.size() == 0) {
                                        noMore = true;
                                    } else {
                                        archivesItems = parseData(reportList, false);
                                        archivesBaogaoAdapter.setMyList(archivesItems);
                                    }
                                }
                            });
                    break;
                //脏腑
                case "viscera":
                    riskManager.getIdentityReportList(memberChildId + "", pageNumber + "",
                            new AbstractDefaultHttpHandlerCallback(getActivity()) {
                                @Override
                                protected void onResponseSuccess(Object obj) {
                                    reportVisceraList = (List<VisceraIdentityResult>) obj;
                                    if (reportList.size() == 0) {
                                        noMore = true;
                                    } else {
                                        archivesItems = parseVisceraData(reportVisceraList, false);
                                        archivesBaogaoAdapter.setMyList(archivesItems);
                                    }
                                }
                            });
                    break;
                //病例
                case "case":
                    caseManager.getCaseList(memberChildId,
                            new AbstractDefaultHttpHandlerCallback(getActivity()) {
                                @Override
                                protected void onResponseSuccess(Object obj) {
                                    caseList = (List<Case>) obj;
                                    if (caseList.size() == 0) {
                                        noMore = true;
                                    } else {
                                        archivesItems = parseCaseData(caseList, false);
                                        archivesBaogaoAdapter.setMyList(archivesItems);
                                    }
                                }
                            });
                    break;
                //血压
                case "blood_pressure":
                    break;
                //血糖
                case "blood":
                    break;
                //呼吸
                case "breath":
                    break;
                //血氧
                case "xy":
                    break;
                //体温
                case "tw":
                    break;
                default:
                    break;
            }
        }
        //加载完成后隐藏ViewFooter
        lv_bao_gao.setLoadCompleted();
    }

    private void refesh() {
        memberChildId = LoginControllor.getChoosedChildMember().getId();
        switch (tag) {
            case "all":
                noMore = false;
                recordManager.getReportListByTag(memberChildId, tag, "", 1 + "",
                        new AbstractDefaultHttpHandlerCallback(getActivity()) {
                            @Override
                            protected void onResponseSuccess(Object obj) {
                                archivesBeanList = (List<ArchivesBean>) obj;
                                if (archivesBeanList.size() == 0) {
                                    archivesItems.clear();
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                } else {
                                    archivesItems = parseAllArchiveBean(archivesBeanList, true);
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                }

                            }

                            @Override
                            protected void onHttpError(int httpErrorCode) {
                                super.onHttpError(httpErrorCode);
                                archivesItems.clear();
                                archivesBaogaoAdapter.setMyList(archivesItems);
                            }
                        });
                break;
            case "new":
                noMore = false;
                getNewReport();
                break;
            //体质
            case "body":
                noMore = false;
                recordManager.getReportListByTag(memberChildId, tag, IdentityCategory.BODY_IDENTITY, "1",
                        new AbstractDefaultHttpHandlerCallback(getActivity()) {
                            @Override
                            protected void onResponseSuccess(Object obj) {
                                reportList = (List<Report>) obj;
                                if (reportList.size() == 0) {
                                } else {
                                    archivesItems = parseData(reportList, true);
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                }
                            }
                        });

                break;
            //心率
            case "rate":
                noMore = false;
                recordManager.getReportListByTag(memberChildId, tag, "", "1",
                        new AbstractDefaultHttpHandlerCallback(getActivity()) {
                            @Override
                            protected void onResponseSuccess(Object obj) {
                                reportECGRList = (List<ECGReport>) obj;
                                if (reportList.size() == 0) {
                                } else {
                                    archivesItems = parseECGRData(reportECGRList, true);
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                }

                            }
                        });
                break;
            //经络
            case "meridian":
                noMore = false;
                recordManager.getReportListByTag(memberChildId, tag, IdentityCategory.MERIDIAN_IDENTITY, "1",
                        new AbstractDefaultHttpHandlerCallback(getActivity()) {
                            @Override
                            protected void onResponseSuccess(Object obj) {
                                reportList = (List<Report>) obj;
                                if (reportList.size() == 0) {
                                } else {
                                    archivesItems = parseData(reportList, true);
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                }
                            }
                        });
                break;
            //脏腑
            case "viscera":
                noMore = false;
                riskManager.getIdentityReportList(memberChildId + "", "1",
                        new AbstractDefaultHttpHandlerCallback(getActivity()) {
                            @Override
                            protected void onResponseSuccess(Object obj) {
                                reportVisceraList = (List<VisceraIdentityResult>) obj;
                                if (reportList.size() == 0) {
                                } else {
                                    archivesItems = parseVisceraData(reportVisceraList, true);
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                }
                            }
                        });
                break;
            //病例
            case "case":
                noMore = false;
                caseManager.getCaseList(memberChildId,
                        new AbstractDefaultHttpHandlerCallback(getActivity()) {
                            @Override
                            protected void onResponseSuccess(Object obj) {
                                caseList = (List<Case>) obj;
                                if (caseList.size() == 0) {
                                } else {
                                    archivesItems = parseCaseData(caseList, true);
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                }
                            }
                        });
                break;
            //血压
            case "blood_pressure":
                break;
            //血糖
            case "blood":
                break;
            //呼吸
            case "breath":
                break;
            //血氧
            case "xy":
                break;
            //体温
            case "tw":
                break;
            default:
                break;
        }
        swipeRefreshLayout.setRefreshing(false);
    }

    private void loadChangeMember() {
        memberChildId = LoginControllor.getChoosedChildMember().getId();
//        切换用户按tag查询
        pageNumber = 1;
        switch (tag) {
            case "all":
                recordManager.getReportListByTag(memberChildId, tag, "", pageNumber + "",
                        new AbstractDefaultHttpHandlerCallback(getActivity()) {
                            @Override
                            protected void onResponseSuccess(Object obj) {
                                archivesBeanList = (List<ArchivesBean>) obj;
                                if (archivesBeanList.size() == 0) {
                                    archivesItems.clear();
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                } else {
                                    archivesItems = parseAllArchiveBean(archivesBeanList, true);
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                }
                            }

                            @Override
                            protected void onHttpError(int httpErrorCode) {
                                super.onHttpError(httpErrorCode);
                                archivesItems.clear();
                                archivesBaogaoAdapter.setMyList(archivesItems);
                            }
                        });
                break;
            case "new":
                recordManager.getReportListByTag(memberChildId, tag, IdentityCategory.MERIDIAN_IDENTITY, pageNumber + "",
                        new AbstractDefaultHttpHandlerCallback(getActivity()) {
                            @Override
                            protected void onResponseSuccess(Object obj) {
                                acricheNewBean = (AcricheNewBean) obj;
                                archivesItems = parseNewData(acricheNewBean, true);
                                archivesBaogaoAdapter.setMyList(archivesItems);
                            }

                            @Override
                            protected void onHttpError(int httpErrorCode) {
                                super.onHttpError(httpErrorCode);
                                archivesItems.clear();
                                archivesBaogaoAdapter.setMyList(archivesItems);
                            }
                        });
                break;

            //体质
            case "body":
                recordManager.getReportListByTag(memberChildId, tag, IdentityCategory.BODY_IDENTITY, pageNumber + "",
                        new AbstractDefaultHttpHandlerCallback(getActivity()) {
                            @Override
                            protected void onResponseSuccess(Object obj) {
                                reportList = (List<Report>) obj;
                                if (reportList.size() == 0) {
                                    noMore = true;
                                    archivesItems.clear();
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                } else {
                                    archivesItems = parseData(reportList, true);
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                }
                            }
                        });

                break;
            //心率
            case "rate":
                recordManager.getReportListByTag(memberChildId, tag, "", pageNumber + "",
                        new AbstractDefaultHttpHandlerCallback(getActivity()) {
                            @Override
                            protected void onResponseSuccess(Object obj) {
                                reportECGRList = (List<ECGReport>) obj;
                                if (reportECGRList.size() == 0) {
                                    noMore = true;
                                    archivesItems.clear();
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                } else {
                                    archivesItems = parseECGRData(reportECGRList, true);
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                }

                            }
                        });
                break;
            //经络
            case "meridian":
                recordManager.getReportListByTag(memberChildId, tag, IdentityCategory.MERIDIAN_IDENTITY, pageNumber + "",
                        new AbstractDefaultHttpHandlerCallback(getActivity()) {
                            @Override
                            protected void onResponseSuccess(Object obj) {
                                reportList = (List<Report>) obj;
                                if (reportList.size() == 0) {
                                    noMore = true;
                                    archivesItems.clear();
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                } else {
                                    archivesItems = parseData(reportList, true);
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                }
                            }
                        });
                break;
            //脏腑
            case "viscera":
                riskManager.getIdentityReportList(memberChildId + "", pageNumber + "",
                        new AbstractDefaultHttpHandlerCallback(getActivity()) {
                            @Override
                            protected void onResponseSuccess(Object obj) {
                                reportVisceraList = (List<VisceraIdentityResult>) obj;
                                if (reportVisceraList.size() == 0) {
                                    noMore = true;
                                    archivesItems.clear();
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                } else {
                                    archivesItems = parseVisceraData(reportVisceraList, true);
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                }
                            }
                        });
                break;
            //病例
            case "case":
                caseManager.getCaseList(memberChildId,
                        new AbstractDefaultHttpHandlerCallback(getActivity()) {
                            @Override
                            protected void onResponseSuccess(Object obj) {
                                caseList = (List<Case>) obj;
                                if (caseList.size() == 0) {
                                    noMore = true;
                                    archivesItems.clear();
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                } else {
                                    archivesItems = parseCaseData(caseList, true);
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                }
                            }
                        });
                break;
            //血压
            case "blood_pressure":
                dataType = CheckedDataType.BLOOD_PRESSURE;
                loadUrl(dataType);
                break;
            //血糖
            case "blood":
                dataType = CheckedDataType.BLOOD_SUGAR;
                loadUrl(dataType);
                break;
            //呼吸
            case "breath":
                dataType = CheckedDataType.BREATH;
                loadUrl(dataType);
                break;
            //血氧
            case "xy":
                dataType = CheckedDataType.SPO2H;
                loadUrl(dataType);
                break;
            //体温
            case "tw":
                dataType = CheckedDataType.TEMPERATURE;
                loadUrl(dataType);
                break;
            case "quaterly":
                report_web_view.setVisibility(View.GONE);
                pageNumber = 1;
                recordManager.getReportListByTag(memberChildId, tag, "", pageNumber + "",
                        new AbstractDefaultHttpHandlerCallback(getActivity()) {
                            @Override
                            protected void onResponseSuccess(Object obj) {
                                List<QuaterReport> quaterReports = (List<QuaterReport>) obj;
                                if (quaterReports.size() > 0) {
                                    archivesItems = parseQuaterData(quaterReports, true);
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                } else {
                                    archivesItems.clear();
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                }
                            }

                            @Override
                            protected void onHttpError(int httpErrorCode) {
                                super.onHttpError(httpErrorCode);
                                archivesItems.clear();
                                archivesBaogaoAdapter.setMyList(archivesItems);
                            }
                        });
                break;
            default:
                break;
        }

        //加载完成后隐藏ViewFooter
        lv_bao_gao.setLoadCompleted();
    }

    private void initRbGroup() {
        for (RadioButton rb : rbGroup) {
            rb.setOnClickListener(this);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = null;
        if (archivesItems.get(position).getItemYype() == 0) {
//            阶段报告
            ArchivesItem item = archivesItems.get(0);
            String strurl4 = "/member/service/reports.jhtml?memberChildId=" + memberChildId + "&quarter=" + item.getQuaterNum() + "&year=" + item.getYearNum();
            intent = new Intent(getActivity(), ReportInfoActivity.class);
            intent.putExtra("strurl4", strurl4);
            startActivity(intent);
        } else {
//            H5报告
            switch (tag) {
                case "all":
                    itemClickOnAll(archivesItems.get(position), position);
                    break;
                case "new":
                    itemClickOnNew(archivesItems.get(position));
                    break;
                case "meridian":
                    int itemYype = archivesItems.get(position).getItemYype();
                    if (itemYype != 1) {
                        intent = new Intent(getActivity(), ReportInfoActivity.class);
                        intent.putExtra("subjectId", archivesItems.get(position).getSubject_sn());
                        intent.putExtra("title",getString2(R.string.report_title_jl ));
                        startActivity(intent);
                    }

                    break;
                case "body":
                    if (archivesItems.get(position).getItemYype() != 1) {
                        intent = new Intent(getActivity(), ReportInfoActivity.class);
                        intent.putExtra("subjectId", archivesItems.get(position).getSubject_sn());
                        intent.putExtra("title",getString2(R.string.report_title_tz ));
                        startActivity(intent);
                    }
                    break;
                case "viscera":
                    if (archivesItems.get(position).getItemYype() != 1) {
                        intent = new Intent(getActivity(), ReportInfoActivity.class);
                        String cust_id = archivesItems.get(position).getCust_id();
                        String physique_id = archivesItems.get(position).getPhysique_id();
//                        String url = Constant.BASE_URL + "/member/service/zf_report.jhtml?cust_id=" + cust_id
//                                + "&physique_id=" + physique_id + "&device=1";
                        String url = archivesItems.get(position).getReportUrl();
                        intent.putExtra("strurl4", url);
                        intent.putExtra("title",getString2(R.string.report_title_zf));
                        startActivity(intent);
                    }
                    break;
                case "rate":
                    if (archivesItems.get(position).getItemYype() != 1) {
                        try {
                            EGCItemJump(position, intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case "case":
                    ArchivesItem item = archivesItems.get(position);
                    if (item.getItemYype() != 1) {
                        /**
                         * 查看病历存档详情
                         */
                        if (TextUtils.isEmpty(userToken) || TextUtils.isEmpty(hhuid)) {
                            hhUserManager.getMemberInfo(new AbstractDefaultHttpHandlerCallback(getActivity()) {
                                @Override
                                protected void onResponseSuccess(Object obj) {
                                    MemberInfoBean memberInfoBean = (MemberInfoBean) obj;
                                    if (memberInfoBean != null) {
                                        userToken = memberInfoBean.getToken();
                                        hhuid = item.getMedicRecordId();
                                        Intent intent2 = new Intent(getActivity(), ViewDetailAct.class);
                                        String url = HHDoctor.getMedicDetailUrl(getActivity(), userToken, hhuid);
                                        intent2.putExtra("url", url);
                                        intent2.putExtra("title", getString2(R.string.archives_frag_binglicundang));
                                        startActivity(intent2);
                                    }
                                }
                            });
                        } else {
                            intent = new Intent(getActivity(), ViewDetailAct.class);
                            String url = HHDoctor.getMedicDetailUrl(getActivity(), userToken, hhuid);
                            intent.putExtra("url", url);
                            intent.putExtra("title", getString2(R.string.archives_frag_binglicundang));
                            startActivity(intent);
                        }


//                        intent = new Intent(getActivity(), ViewDetailAct.class);
//                        String url = HHDoctor.getMedicDetailUrl(getActivity(), "ECEEDCCD74B7D54BCF6690B7E26262B73F0D04F68EA2608F6783B874E4F50EEF", "1541041785333");
//                        http://test.hh-medic.com:443
//                        intent.putExtra("url", url);
//                        intent.putExtra("title", "病历存档详情");
//                        startActivity(intent);
                    }
                    break;
                default:

                    break;

            }
        }
    }

    private void itemClickOnNew(ArchivesItem archivesItem) {
        Intent intent = null;
        String textOne = archivesItem.getTextOne();
        if (getString2(R.string.archives_frag_jingluo).equalsIgnoreCase(textOne)){
            intent = new Intent(getActivity(), ReportInfoActivity.class);
            intent.putExtra("subjectId", archivesItem.getSubject_sn());
            intent.putExtra("title", getString2(R.string.report_title_jl));
            startActivity(intent);
        }else if ("meridian".equalsIgnoreCase(textOne)){
            intent = new Intent(getActivity(), ReportInfoActivity.class);
            intent.putExtra("subjectId", archivesItem.getSubject_sn());
            intent.putExtra("title", getString2(R.string.report_title_jl));
            startActivity(intent);
        }else if (getString2(R.string.archives_frag_tizhi).equalsIgnoreCase(textOne)){
            intent = new Intent(getActivity(), ReportInfoActivity.class);
            intent.putExtra("subjectId", archivesItem.getSubject_sn());
            intent.putExtra("title", getString2(R.string.report_title_tz));
            startActivity(intent);
        }else if ("constitution".equalsIgnoreCase(textOne)){
            intent = new Intent(getActivity(), ReportInfoActivity.class);
            intent.putExtra("subjectId", archivesItem.getSubject_sn());
            intent.putExtra("title", getString2(R.string.report_title_zx));
            startActivity(intent);
        }else if(getString2(R.string.archives_frag_zangfu).equalsIgnoreCase(textOne)){
            intent = new Intent(getActivity(), ReportInfoActivity.class);
//            String cust_id = archivesItem.getCust_id();
            String physique_id = archivesItem.getPhysique_id();
            String url = "/member/service/zf_report.jhtml?cust_id=" + memberChildId
                    + "&physique_id=" + physique_id + "&device=1";
            intent.putExtra("strurl4", url);
            intent.putExtra("title", getString2(R.string.report_title_zf));
            startActivity(intent);
        }else if ("Zang-fu Organs".equalsIgnoreCase(textOne)){
            intent = new Intent(getActivity(), ReportInfoActivity.class);
//            String cust_id2 = archivesItem.getCust_id();
            String physique_id2 = archivesItem.getPhysique_id();
            String url2 = "/member/service/zf_report.jhtml?cust_id=" + memberChildId
                    + "&physique_id=" + physique_id2 + "&device=1";
            intent.putExtra("strurl4", url2);
            intent.putExtra("title", getString2(R.string.report_title_zf));
            startActivity(intent);
        }else if (getString2(R.string.archives_frag_xinlv).equalsIgnoreCase(textOne)){
            try {
                newEGCItemJump(archivesItem);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if ("heart rate".equalsIgnoreCase(textOne)){
            try {
                newEGCItemJump(archivesItem);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if (getString2(R.string.archives_frag_xueya).equalsIgnoreCase(textOne)){
            dataType = CheckedDataType.BLOOD_PRESSURE;
            loadUrl2(dataType,getString2(R.string.report_title_xya));
        }else if ("blood pressure".equalsIgnoreCase(textOne)){
            dataType = CheckedDataType.BLOOD_PRESSURE;
            loadUrl2(dataType,getString2(R.string.report_title_xya));
        }else if (getString2(R.string.archives_frag_xueyang).equalsIgnoreCase(textOne)){
            dataType = CheckedDataType.SPO2H;
            loadUrl2(dataType,getString2(R.string.report_title_xy));
        }else if ("blood oxygen".equalsIgnoreCase(textOne)){
            dataType = CheckedDataType.SPO2H;
            loadUrl2(dataType,getString2(R.string.report_title_xy));
        }else if (getString2(R.string.archives_frag_tiwen).equalsIgnoreCase(textOne)){
            dataType = CheckedDataType.TEMPERATURE;
            loadUrl2(dataType,getString2(R.string.report_title_tw));
        }else if ("blood sugar".equalsIgnoreCase(textOne)){
            dataType = CheckedDataType.TEMPERATURE;
            loadUrl2(dataType,getString2(R.string.report_title_xt));
        }else if (getString2(R.string.archives_frag_xuetang).equalsIgnoreCase(textOne)){
            dataType = CheckedDataType.BLOOD_SUGAR;
            loadUrl2(dataType,getString2(R.string.report_title_xt));
        }else if ("body temp".equalsIgnoreCase(textOne)){
            dataType = CheckedDataType.BLOOD_SUGAR;
            loadUrl2(dataType,getString2(R.string.report_title_tw));
        }else if (getString2(R.string.archives_frag_huxi).equalsIgnoreCase(textOne)){
            dataType = CheckedDataType.BREATH;
            loadUrl2(dataType,getString2(R.string.report_title_hx));
        }else if ("Respiration".equalsIgnoreCase(textOne)){
            dataType = CheckedDataType.BREATH;
            loadUrl2(dataType,getString2(R.string.report_title_hx));
        }
    }

    private void itemClickOnAll(ArchivesItem archivesItem, int position) {
        Intent intent = null;
        String textOne = archivesItem.getTextOne();
        if(textOne==null){
            textOne = "";
        }
        switch (textOne) {
            case "经络":
                intent = new Intent(getActivity(), ReportInfoActivity.class);
                intent.putExtra("strurl4", archivesItem.getReportUrl());
                intent.putExtra("title", getString2(R.string.report_title_jl));
                startActivity(intent);
                break;
            case "体质":
                intent = new Intent(getActivity(), ReportInfoActivity.class);
                intent.putExtra("strurl4", archivesItem.getReportUrl());
                intent.putExtra("title", getString2(R.string.report_title_tz));
                startActivity(intent);
                break;
            case "脏腑"://todo
                intent = new Intent(getActivity(), ReportInfoActivity.class);
                String cust_id = archivesItem.getCust_id();
                String physique_id = archivesItem.getPhysique_id();
//     String url = "/member/service/zf_report.jhtml?cust_id=" + cust_id + "&physique_id=" + physique_id + "&device=1";
                String url = archivesItem.getReportUrl();
                intent.putExtra("strurl4", url);
                intent.putExtra("title", getString2(R.string.report_title_zf));
                startActivity(intent);
                break;
            case "心率":
                try {
                    EGCItemJump(position, intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case "血压":
                dataType = CheckedDataType.BLOOD_PRESSURE;
                loadUrl2(dataType,getString2(R.string.report_title_xya));
                break;
            case "血氧":
                dataType = CheckedDataType.SPO2H;
                loadUrl2(dataType,getString2(R.string.report_title_xy));
                break;
            case "体温":
                dataType = CheckedDataType.TEMPERATURE;
                loadUrl2(dataType,getString2(R.string.report_title_tw));
                break;
            case "血糖":
                dataType = CheckedDataType.BLOOD_SUGAR;
                loadUrl2(dataType,getString2(R.string.report_title_xt));
                break;
            //呼吸
            case "呼吸":
                dataType = CheckedDataType.BREATH;
                loadUrl2(dataType,getString2(R.string.report_title_hx));
                break;
            case "Meridian":
                intent = new Intent(getActivity(), ReportInfoActivity.class);
                intent.putExtra("strurl4", archivesItem.getReportUrl());
                intent.putExtra("title", getString2(R.string.report_title_jl));
                startActivity(intent);
                break;
            case "Constitution":
                intent = new Intent(getActivity(), ReportInfoActivity.class);
                intent.putExtra("strurl4", archivesItem.getReportUrl());
                intent.putExtra("title", getString2(R.string.report_title_zx));
                startActivity(intent);
                break;
            case "Zang-fu Organs":
                intent = new Intent(getActivity(), ReportInfoActivity.class);
                String cust_id2 = archivesItem.getCust_id();
                String physique_id2 = archivesItem.getPhysique_id();
//     String url = "/member/service/zf_report.jhtml?cust_id=" + cust_id + "&physique_id=" + physique_id + "&device=1";
                String url2 = archivesItem.getReportUrl();
                intent.putExtra("strurl4", url2);
                intent.putExtra("title", getString2(R.string.report_title_zf));
                startActivity(intent);
                break;
            case "Heart rate":
                try {
                    EGCItemJump(position, intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case "Blood Pressure":
                dataType = CheckedDataType.BLOOD_PRESSURE;
                loadUrl2(dataType,getString2(R.string.report_title_xya));
                break;
            case "Blood Oxygen":
                dataType = CheckedDataType.SPO2H;
                loadUrl2(dataType,getString2(R.string.report_title_xy));
                break;
            case "Body Temperature":
                dataType = CheckedDataType.TEMPERATURE;
                loadUrl2(dataType,getString2(R.string.report_title_tw));
                break;
            case "Blood Sugar":
                dataType = CheckedDataType.BLOOD_SUGAR;
                loadUrl2(dataType,getString2(R.string.report_title_xt));
                break;
            //呼吸
            case "Respiration":
                dataType = CheckedDataType.BREATH;
                loadUrl2(dataType,getString2(R.string.report_title_hx));
                break;
            default:

                break;

        }
    }


    // 进入该界面默认展示最新报告
    private void setDefult() {
        rb_all.setChecked(true);
    }

    @Override
    public void onClick(View v) {
        rb_all.setChecked(false);
        if (clickedRB.getId() == v.getId()) {
            if (clickedRB.isChecked()) {
                clickedRB.setChecked(false);
                clickedRB = clickedRB2;
                return;
            } else {
                clickedRB.setChecked(true);
            }
        } else {
            clickedRB.setChecked(false);
            ((RadioButton) v).setChecked(true);
        }
        clickedRB = (RadioButton) v;

        switch (v.getId()) {
            case R.id.rb_all:
                report_web_view.setVisibility(View.GONE);
                lv_bao_gao.setVisibility(View.VISIBLE);
                tag = "all";
                pageNumber = 1;
                noMore = false;
                recordManager.getReportListByTag(memberChildId, tag, "", pageNumber + "",
                        new AbstractDefaultHttpHandlerCallback(getActivity()) {
                            @Override
                            protected void onResponseSuccess(Object obj) {
                                archivesBeanList = (List<ArchivesBean>) obj;
                                if (archivesBeanList.size() == 0) {
                                    archivesItems.clear();
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                } else {
                                    archivesItems = parseAllArchiveBean(archivesBeanList, true);
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                }

                            }

                            @Override
                            protected void onHttpError(int httpErrorCode) {
                                super.onHttpError(httpErrorCode);
                                archivesItems.clear();
                                archivesBaogaoAdapter.setMyList(archivesItems);
                            }
                        });
                break;
            //最新
            case R.id.rb_new:
                report_web_view.setVisibility(View.GONE);
                lv_bao_gao.setVisibility(View.VISIBLE);
                tag = "new";
                pageNumber = 1;
                noMore = false;
                recordManager.getReportListByTag(memberChildId, tag, IdentityCategory.MERIDIAN_IDENTITY, pageNumber + "",
                        new AbstractDefaultHttpHandlerCallback(getActivity()) {
                            @Override
                            protected void onResponseSuccess(Object obj) {
                                acricheNewBean = (AcricheNewBean) obj;
                                archivesItems = parseNewData(acricheNewBean, true);
                                archivesBaogaoAdapter.setMyList(archivesItems);
                            }

                            @Override
                            protected void onHttpError(int httpErrorCode) {
                                super.onHttpError(httpErrorCode);
                                archivesItems.clear();
                                archivesBaogaoAdapter.setMyList(archivesItems);
                            }
                        });
                break;
            //体质
            case R.id.rb_body:
                lv_bao_gao.setVisibility(View.VISIBLE);
                report_web_view.setVisibility(View.GONE);
                tag = "body";
                pageNumber = 1;
                noMore = false;
                recordManager.getReportListByTag(memberChildId, tag, IdentityCategory.BODY_IDENTITY, pageNumber + "",
                        new AbstractDefaultHttpHandlerCallback(getActivity()) {
                            @Override
                            protected void onResponseSuccess(Object obj) {
                                reportList = (List<Report>) obj;
                                if (reportList.size() > 0) {
                                    archivesItems = parseData(reportList, true);
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                } else {
                                    archivesItems.clear();
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                }
                            }

                            @Override
                            protected void onHttpError(int httpErrorCode) {
                                super.onHttpError(httpErrorCode);
                                archivesItems.clear();
                                archivesBaogaoAdapter.setMyList(archivesItems);
                            }
                        });

                break;
            //心率
            case R.id.rb_ECG:
                lv_bao_gao.setVisibility(View.VISIBLE);
                report_web_view.setVisibility(View.GONE);
                tag = "rate";
                pageNumber = 1;
                noMore = false;
                recordManager.getReportListByTag(memberChildId, tag, "", pageNumber + "",
                        new AbstractDefaultHttpHandlerCallback(getActivity()) {
                            @Override
                            protected void onResponseSuccess(Object obj) {
                                reportECGRList = (List<ECGReport>) obj;
                                if (reportECGRList.size() > 0) {
                                    archivesItems = parseECGRData(reportECGRList, true);
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                } else {
                                    archivesItems.clear();
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                }
                            }

                            @Override
                            protected void onHttpError(int httpErrorCode) {
                                super.onHttpError(httpErrorCode);
                                archivesItems.clear();
                                archivesBaogaoAdapter.setMyList(archivesItems);
                            }
                        });
                break;
            //阶段报告（季度）
            case R.id.rb_quarterly:
                tag = "quaterly";
                lv_bao_gao.setVisibility(View.VISIBLE);
                report_web_view.setVisibility(View.GONE);
                pageNumber = 1;
                recordManager.getReportListByTag(memberChildId, tag, "", pageNumber + "",
                        new AbstractDefaultHttpHandlerCallback(getActivity()) {
                            @Override
                            protected void onResponseSuccess(Object obj) {
                                List<QuaterReport> quaterReports = (List<QuaterReport>) obj;
                                if (quaterReports.size() > 0) {
                                    archivesItems = parseQuaterData(quaterReports, true);
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                } else {
                                    archivesItems.clear();
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                }
                            }

                            @Override
                            protected void onHttpError(int httpErrorCode) {
                                super.onHttpError(httpErrorCode);
                                archivesItems.clear();
                                archivesBaogaoAdapter.setMyList(archivesItems);
                            }
                        });
                break;
            //经络
            case R.id.rb_meridian:
                lv_bao_gao.setVisibility(View.VISIBLE);
                report_web_view.setVisibility(View.GONE);
                tag = "meridian";
                pageNumber = 1;
                noMore = false;
                recordManager.getReportListByTag(memberChildId, tag, IdentityCategory.MERIDIAN_IDENTITY, pageNumber + "",
                        new AbstractDefaultHttpHandlerCallback(getActivity()) {
                            @Override
                            protected void onResponseSuccess(Object obj) {
                                reportList = (List<Report>) obj;
                                if (reportList.size() > 0) {
                                    archivesItems = parseData(reportList, true);
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                } else {
                                    archivesItems.clear();
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                }
                            }

                            @Override
                            protected void onHttpError(int httpErrorCode) {
                                super.onHttpError(httpErrorCode);
                                archivesItems.clear();
                                archivesBaogaoAdapter.setMyList(archivesItems);
                            }
                        });
                break;
            //脏腑
            case R.id.rb_viscera:
                tag = "viscera";
                lv_bao_gao.setVisibility(View.VISIBLE);
                report_web_view.setVisibility(View.GONE);
                pageNumber = 1;
                noMore = false;
                riskManager.getIdentityReportList(memberChildId + "", pageNumber + "",
                        new AbstractDefaultHttpHandlerCallback(getActivity()) {
                            @Override
                            protected void onResponseSuccess(Object obj) {
                                reportVisceraList = (List<VisceraIdentityResult>) obj;
                                if (reportVisceraList.size() > 0) {
                                    archivesItems = parseVisceraData(reportVisceraList, true);
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                } else {
                                    archivesItems.clear();
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                }
                            }

                            @Override
                            protected void onHttpError(int httpErrorCode) {
                                super.onHttpError(httpErrorCode);
                                archivesItems.clear();
                                archivesBaogaoAdapter.setMyList(archivesItems);
                            }
                        });
                break;
            //病例
            case R.id.rb_case:
                tag = "case";
                lv_bao_gao.setVisibility(View.VISIBLE);
                report_web_view.setVisibility(View.GONE);
                pageNumber = 1;
                noMore = false;
                int memberID = LoginControllor.getLoginMember().getId();
                caseManager.getCaseList(memberID,
                        new AbstractDefaultHttpHandlerCallback(getActivity()) {
                            @Override
                            protected void onResponseSuccess(Object obj) {
                                caseList = (List<Case>) obj;
                                if (caseList.size() > 0) {
                                    archivesItems = parseCaseData(caseList, true);
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                } else {
                                    archivesItems.clear();
                                    archivesBaogaoAdapter.setMyList(archivesItems);
                                }
                            }

                            @Override
                            protected void onHttpError(int httpErrorCode) {
                                super.onHttpError(httpErrorCode);
                                archivesItems.clear();
                                archivesBaogaoAdapter.setMyList(archivesItems);
                            }
                        });
                break;
            //血氧
            case R.id.rb_SPO2H:
                tag = "xy";
                dataType = CheckedDataType.SPO2H;
                loadUrl(dataType);

                break;
            //体温
            case R.id.rb_temperature:
                tag = "tw";
                dataType = CheckedDataType.TEMPERATURE;
                loadUrl(dataType);
                break;
            //血压
            case R.id.rb_blood_pressure:
                tag = "blood_pressure";
                dataType = CheckedDataType.BLOOD_PRESSURE;
                loadUrl(dataType);
                break;
            //血糖
            case R.id.rb_blood:
                tag = "blood";
                dataType = CheckedDataType.BLOOD_SUGAR;
                loadUrl(dataType);
                break;
            //呼吸
            case R.id.rb_breath:
                tag = "breath";
                dataType = CheckedDataType.BREATH;
                loadUrl(dataType);
                break;
            default:
                break;
        }

        closeRightLayout();

    }

    private void loadUrl(int dataType) {
        rl_no_data.setVisibility(View.GONE);
        lv_bao_gao.setVisibility(View.GONE);
        report_web_view.setVisibility(View.VISIBLE);
        if (TextUtils.isEmpty(memberChildId + "")) {
            memberChildId = Integer.parseInt(SharedPreferenceUtil.getCurrnetMemberId());
        }
        recordManager = new RecordManager();
        String url = recordManager.getHsitoryReportUrl(memberChildId + "", dataType);
        Log.d("loadUrl", "loadUrl: "+url);
        new WebViewUtil().setWebViewInit(wv_report, progressBar, getActivity(), url);
    }

    private void loadUrl2(int dataType,String title) {

        if (TextUtils.isEmpty(memberChildId + "")) {
            memberChildId = Integer.parseInt(SharedPreferenceUtil.getCurrnetMemberId());
        }
        recordManager = new RecordManager();
        String url = recordManager.getHsitoryReportUrlNOBaseUrl(memberChildId + "", dataType);
        Intent intent = new Intent(getActivity(), ReportInfoActivity.class);
        intent.putExtra("strurl4", url);
        intent.putExtra("title", title);
        startActivity(intent);
    }

    private List<ArchivesItem> parseQuaterData(List<QuaterReport> quaterReports, boolean clear) {
        if (clear) {
            if (archivesItems != null) {
                archivesItems.clear();
            }
        }

        if (quaterReports.size() > 0) {
            for (QuaterReport report : quaterReports) {
                switch (report.getQuarter()) {
                    case 1:
                        ArchivesItem quart1 = new ArchivesItem(0);
                        quart1.setQuarter(getResources().getString(R.string.quater_report_one));
                        String s1 = String.format(getResources().getString(R.string.quater_report_one_year), report.getYear(), report.getYear());
                        quart1.setYear(s1);
                        quart1.setBackgroundId(R.drawable.bg_quater_report_one);
                        archivesItems.add(quart1);
                        break;
                    case 2:
                        ArchivesItem quart2 = new ArchivesItem(0);
                        quart2.setQuarter(getResources().getString(R.string.quater_report_two));
                        String s2 = String.format(getResources().getString(R.string.quater_report_two_year), report.getYear(), report.getYear());
                        quart2.setYear(s2);
                        quart2.setBackgroundId(R.drawable.bg_quater_report_two);
                        archivesItems.add(quart2);
                        break;
                    case 3:
                        ArchivesItem quart3 = new ArchivesItem(0);
                        quart3.setQuarter(getResources().getString(R.string.quater_report_three));
                        String s3 = String.format(getResources().getString(R.string.quater_report_three_year), report.getYear(), report.getYear());
                        quart3.setYear(s3);
                        quart3.setBackgroundId(R.drawable.bg_quater_report_three);
                        archivesItems.add(quart3);
                        break;
                    case 4:
                        ArchivesItem quart4 = new ArchivesItem(0);
                        quart4.setQuarter(getResources().getString(R.string.quater_report_four));
                        String s4 = String.format(getResources().getString(R.string.quater_report_four_year), report.getYear(), report.getYear());
                        quart4.setYear(s4);
                        quart4.setBackgroundId(R.drawable.bg_quater_report_four);
                        archivesItems.add(quart4);
                        break;
                    default:
                        break;
                }
            }
        }
        return archivesItems;
    }

    private List<ArchivesItem> parseData(List<Report> reportList, boolean clear) {
        if (clear) {
            if (archivesItems != null) {
                archivesItems.clear();
            }
        }
        String monthDay = "";
//        本次加载衔接上次的日期
        if (archivesItems != null && archivesItems.size() != 0) {
            int itemYype = archivesItems.get(archivesItems.size() - 1).getItemYype();
            if (itemYype != 1) {
                Long createTime = archivesItems.get(archivesItems.size() - 1).getCreateDate();
                if (createTime != null) {
                    monthDay = getDate(createTime);
                }
                if (monthDay == null) {
                    monthDay = archivesItems.get(archivesItems.size() - 1).getTimeDate();
                }
                if (monthDay == null) {
                    monthDay = "";
                }
            }
        }
        if (reportList.size() > 0) {
            for (Report report : reportList) {
                Date createTime = report.getCreateTime();
                String mday = "";
                if (createTime != null) {
                    mday = getDate(createTime.getTime());
                }
                if (!monthDay.equals(mday)) {
                    monthDay = mday;
                    ArchivesItem itemDate = new ArchivesItem();
                    itemDate.setItemYype(1);
                    itemDate.setTimeDate(monthDay);
                    archivesItems.add(itemDate);
                }
                ArchivesItem item = new ArchivesItem();
                item.setItemYype(2);
                item.setTimeMin(getTime(report.getCreateDate()));
                item.setTimeDate(createTime + "");
                if (tag.equals("body")) {
                    item.setTextOne(getString(R.string.report_body));
                } else {
                    item.setTextOne(getString(R.string.report_meridian));
                }
                item.setTextTwo(report.getSubject().getName());
                item.setSubject_sn(report.getSubject().getSubject_sn());
                item.setCreateDate(report.getCreateDate());
                archivesItems.add(item);
            }
        }

        return archivesItems;
    }

    private List<ArchivesItem> parseNewData(AcricheNewBean acricheNewBean, boolean clear) {
        if (clear) {
            if (archivesItems != null)
                archivesItems.clear();
        }
        List<ArchivesItem> tempList = new ArrayList<>();
        //经络解析
        AcricheNewBean.JLBSBean jlbs = acricheNewBean.getJLBS();
        if (jlbs != null) {
            ArchivesItem itemjl = new ArchivesItem();
            itemjl.setItemYype(2);
            itemjl.setTimeDate(jlbs.getCreateTime() + "");
            itemjl.setTimeMin(getTime(jlbs.getCreateDate()));
            itemjl.setCreateDate(jlbs.getCreateDate());
            if (getActivity() != null)
                itemjl.setTextOne(getString(R.string.report_meridian));
            itemjl.setTextTwo(jlbs.getSubject().getName());
            itemjl.setSubject_sn(jlbs.getSubject().getSubject_sn());
            tempList.add(itemjl);
        }


        //体质解析
        AcricheNewBean.TZBSBean tzbs = acricheNewBean.getTZBS();
        if (tzbs != null) {
            ArchivesItem itemtz = new ArchivesItem();
            itemtz.setItemYype(2);
            itemtz.setTimeDate(tzbs.getCreateTime() + "");
            itemtz.setTimeMin(getTime(tzbs.getCreateDate()));
            itemtz.setCreateDate(tzbs.getCreateDate());
            if (getActivity() != null)
                itemtz.setTextOne(getString(R.string.report_body));
            itemtz.setTextTwo(tzbs.getSubject().getName());
            itemtz.setSubject_sn(tzbs.getSubject().getSubject_sn());
            tempList.add(itemtz);
        }


        //脏腑解析
        AcricheNewBean.ZFBSBean zfbs = acricheNewBean.getZFBS();
        if (zfbs != null) {
            ArchivesItem itemzfbs = new ArchivesItem();
            itemzfbs.setItemYype(2);
            itemzfbs.setTimeDate(zfbs.getCreateTime() + "");
            itemzfbs.setTimeMin(getTime(zfbs.getCreateDate()));
            itemzfbs.setCreateDate(zfbs.getCreateDate());
            if (getActivity() != null)
                itemzfbs.setTextOne(getString(R.string.report_viscera));
            itemzfbs.setTextTwo(zfbs.getCert_name());
            if (getActivity() != null)
                itemzfbs.setTextThree(getString(R.string.report_viscera));
            itemzfbs.setTextFour(zfbs.getIcd_name_str());
            itemzfbs.setCust_id(zfbs.getCust_id() + "");
            itemzfbs.setPhysique_id(zfbs.getPhysique_id());
            tempList.add(itemzfbs);
        }


        //血压解析
        AcricheNewBean.BloodPressureBean bloodPressure = acricheNewBean.getBloodPressure();
        if (bloodPressure != null) {
            ArchivesItem itembloodPressure = new ArchivesItem();
            itembloodPressure.setItemYype(2);
            itembloodPressure.setTimeDate(bloodPressure.getCreateDate() + "");
            itembloodPressure.setTimeMin(getTime(bloodPressure.getCreateDate()));
            itembloodPressure.setCreateDate(bloodPressure.getCreateDate());
            if (getActivity() != null) {
                itembloodPressure.setTextOne(getString(R.string.archives_rightdraw_xueya));
                itembloodPressure.setTextTwo(getString(R.string.report_height_pressure) + bloodPressure.getHighPressure() + "-" + getString(R.string.report_low_pressure) + bloodPressure.getLowPressure());
            }

            tempList.add(itembloodPressure);
        }


        //体温解析
        AcricheNewBean.BodyTemperatureBean temperature = acricheNewBean.getBodyTemperature();
        if (temperature != null) {
            ArchivesItem itemTemperature = new ArchivesItem();
            itemTemperature.setItemYype(2);
            itemTemperature.setTimeDate(temperature.getCreateDate() + "");
            itemTemperature.setTimeMin(getTime(temperature.getCreateDate()));
            itemTemperature.setCreateDate(temperature.getCreateDate());
            if (getActivity() != null)
                itemTemperature.setTextOne(getString(R.string.archives_rightdraw_tiwen));
            itemTemperature.setTextTwo(temperature.getTemperature() + "");
            tempList.add(itemTemperature);
        }


        //血氧解析
        AcricheNewBean.OxygenBean oxygen = acricheNewBean.getOxygen();
        if (oxygen != null) {
            ArchivesItem itemoxygen = new ArchivesItem();
            itemoxygen.setItemYype(2);
            itemoxygen.setTimeDate(oxygen.getCreateDate() + "");
            itemoxygen.setTimeMin(getTime(oxygen.getCreateDate()));
            itemoxygen.setCreateDate(oxygen.getCreateDate());
            if (getActivity() != null)
                itemoxygen.setTextOne(getString(R.string.report_SPO2H));
            itemoxygen.setTextTwo((int)(oxygen.getDensity()*100) + "%");
            tempList.add(itemoxygen);
        }


        //心电解析
        AcricheNewBean.EcgBean ecg = acricheNewBean.getEcg();
        if (ecg != null) {
            ArchivesItem itemecg = new ArchivesItem();
            itemecg.setItemYype(2);
            itemecg.setTimeDate(ecg.getCreateTime() + "");
            itemecg.setTimeMin(getTime(ecg.getCreateDate()));
            itemecg.setCreateDate(ecg.getCreateDate());
            if (getActivity() != null)
                itemecg.setTextOne(getString(R.string.archives_rightdraw_xinlv));
            itemecg.setTextTwo(ecg.getHeartRate()+"");
            itemecg.setPath(ecg.getPath());
            tempList.add(itemecg);
        }


//先加入集合再排序
        Collections.sort(tempList); // 按时间降序
        String monthDay = "d";
        for (ArchivesItem item : tempList) {
            String mday = getDate(item.getCreateDate());
            if (!mday.equals("")) {
                if (!monthDay.equals(mday)) {
                    monthDay = mday;
                    ArchivesItem itemDate = new ArchivesItem();
                    itemDate.setItemYype(1);
                    itemDate.setTextOne("");
                    itemDate.setTimeDate(monthDay);
                    archivesItems.add(itemDate);
                }
            }
            archivesItems.add(item);
        }
        //当前季度
        AcricheNewBean.ReportBean report = acricheNewBean.getReport();
        if (report != null) {
            ArchivesItem item = initQuaterReport(report.getYear(), report.getQuarter());
            archivesItems.add(0, item);
        }
        return archivesItems;
    }

    private List<ArchivesItem> parseVisceraData(List<VisceraIdentityResult> reportVisceraList, boolean clear) {
        if (clear) {
            if (archivesItems != null) {
                archivesItems.clear();
            }
        }
        String monthDay = "";
//        本次加载衔接上次的日期
        if (archivesItems != null && archivesItems.size() != 0) {
            int itemYype = archivesItems.get(archivesItems.size() - 1).getItemYype();
            if (itemYype != 1) {
                Long createTime = archivesItems.get(archivesItems.size() - 1).getCreateDate();
                if (createTime != null) {
                    monthDay = getDate(createTime);
                }
                if (monthDay == null) {
                    monthDay = archivesItems.get(archivesItems.size() - 1).getTimeDate();
                }
                if (monthDay == null) {
                    monthDay = "";
                }
            }
        }
        if (reportVisceraList.size() > 0) {
            for (VisceraIdentityResult report : reportVisceraList) {
                Date createTime = report.getCreateTime();
                String mday = "";
                if (createTime != null) {
                    mday = getDate(createTime.getTime());
                }
                if (!monthDay.equals(mday)) {
                    monthDay = mday;
                    ArchivesItem itemDate = new ArchivesItem();
                    itemDate.setItemYype(1);
                    itemDate.setTimeDate(monthDay);
                    archivesItems.add(itemDate);
                }

                ArchivesItem item = new ArchivesItem();
                item.setItemYype(2);
                item.setTimeMin(getTime(report.getCreateTime().getTime()));
                item.setTimeDate(report.getCreateTime() + "");
                item.setTextOne(getString(R.string.report_viscera));
                if (!TextUtils.isEmpty(report.getName())) {
                    item.setTextTwo(report.getName());
                } else {
                    item.setTextTwo(report.getName());
                }
//                item.setTextThree(getString(R.string.icdshi));
//                item.setTextFour(report.getIcd_name());
                item.setPhysique_id(report.getPhysique_id());
                item.setCust_id(report.getCust_id());
                item.setCreateDate(report.getCreateDate());
                item.setReportUrl(report.getLink());
                archivesItems.add(item);
            }
        }
        return archivesItems;
    }

    private List<ArchivesItem> parseECGRData(List<ECGReport> reportECGRList, boolean clear) {
        if (clear) {
            if (archivesItems != null)
                archivesItems.clear();
        }
        String monthDay = "";
//        本次加载衔接上次的日期
        if (archivesItems != null && archivesItems.size() != 0) {
            int itemYype = archivesItems.get(archivesItems.size() - 1).getItemYype();
            if (itemYype != 1) {
                Long createTime = archivesItems.get(archivesItems.size() - 1).getCreateDate();
                if (createTime != null) {
                    monthDay = getDate(createTime);
                }
                if (monthDay == null) {
                    monthDay = archivesItems.get(archivesItems.size() - 1).getTimeDate();
                }
                if (monthDay == null) {
                    monthDay = "";
                }
            }
        }
        if (reportECGRList.size() > 0) {
            for (ECGReport report : reportECGRList) {
                Date createTime = report.getCreateTime();
                String mday = "";
                if (createTime != null) {
                    mday = getDate(createTime.getTime());
                }
                if (!monthDay.equals(mday)) {
                    monthDay = mday;
                    ArchivesItem itemDate = new ArchivesItem();
                    itemDate.setItemYype(1);
                    itemDate.setTimeDate(monthDay);
                    archivesItems.add(itemDate);
                }

                ArchivesItem item = new ArchivesItem();
                item.setItemYype(2);
                item.setTimeMin(getTime(report.getCreateDate()));
                item.setTimeDate(report.getCreateTime() + "");
                item.setTextOne(getString(R.string.archives_rightdraw_xinlv));
                item.setTextTwo(report.getHeartRate()+"");
//                item.setTextTwo(report.getHeartRate()+getString(R.string.heart_rate_result_danwei));
//                item.setTextThree(getString(R.string.heart_rate_tip));
//                String content = report.getContent();
//                if (!TextUtils.isEmpty(content)) {
//                    item.setTextFour(content);
//                } else {
//                    item.setTextFour(getResources().getString(R.string.noECGSuggest));
//                }
                item.setPath(report.getPath());
                item.setUploadFailedData(report.getUploadFailedData());
                item.setCreateDate(report.getCreateDate());
                archivesItems.add(item);
            }
        }

        return archivesItems;
    }

    private List<ArchivesItem> parseCaseData(List<Case> caseList, boolean clear) {
        if (clear) {
            if (archivesItems != null) {
                archivesItems.clear();
            }
        }
        String monthDay = "";
//        本次加载衔接上次的日期
        if (archivesItems != null && archivesItems.size() != 0) {
            int itemYype = archivesItems.get(archivesItems.size() - 1).getItemYype();
            if (itemYype != 1) {
                Long createTime = archivesItems.get(archivesItems.size() - 1).getCreateDate();
                if (createTime != null) {
                    monthDay = getDate(createTime);
                }
                if (monthDay == null) {
                    monthDay = archivesItems.get(archivesItems.size() - 1).getTimeDate();
                }
                if (monthDay == null) {
                    monthDay = "";
                }
            }
        }
        if (caseList.size() > 0) {
            for (Case report : caseList) {
                Date createTime = report.getCreateTime();
                String mday = "";
                if (createTime != null) {
                    mday = getDate(createTime.getTime());
                }
                if (!monthDay.equals(mday)) {
                    monthDay = mday;
                    ArchivesItem itemDate = new ArchivesItem();
                    itemDate.setItemYype(1);
                    itemDate.setTimeDate(monthDay);
                    archivesItems.add(itemDate);
                }

                ArchivesItem item = new ArchivesItem();
                item.setItemYype(4);
                item.setTimeMin(getTime(report.getCreateTime().getTime()));
                item.setTimeDate(report.getCreateTime() + "");
                item.setTextOne(report.getDoctorDept());
                item.setTextTwo(report.getDoctorName());
                item.setTextThree(report.getMainSuit());
                item.setMedicRecordId(report.getMedicRecordId());
                item.setCreateDate(report.getCreateTime().getTime());
                archivesItems.add(item);
            }
        }
        return archivesItems;
    }

    private List<ArchivesItem> parseAllArchiveBean(List<ArchivesBean> archivesBeanList, boolean clear) {
        if (clear) {
            if (archivesItems != null) {
                archivesItems.clear();
            }
        }
        String monthDay = "";
//        本次加载衔接上次的日期
        if (archivesItems != null && archivesItems.size() != 0) {
            int itemYype = archivesItems.get(archivesItems.size() - 1).getItemYype();
            if (itemYype != 1) {
                Long createTime = archivesItems.get(archivesItems.size() - 1).getCreateDate();
                if (createTime != null) {
                    monthDay = getDate(createTime);
                }
                if (monthDay == null) {
                    monthDay = archivesItems.get(archivesItems.size() - 1).getTimeDate();
                }
                if (monthDay == null) {
                    monthDay = "";
                }
            }
        }
        if (archivesBeanList.size() > 0) {
            for (ArchivesBean bean : archivesBeanList) {
                Long createTime = bean.getCreateTime();
                String mday = "";
                if (createTime != null) {
                    mday = getDate(createTime);
                }
                if (bean.getTypeName().equals(getString2(R.string.archives_frag_baogao))) {
                    int quater = 1;
                    try {
                        quater = Integer.parseInt(bean.getName());
                    } catch (NumberFormatException e) {
                    }
                    ArchivesItem itemQuater = initQuaterReport(bean.getYear(), quater);
                    itemQuater.setReportUrl(bean.getLink());
                    archivesItems.add(itemQuater);
                    continue;
                } else if (!monthDay.equals(mday)) {
                    monthDay = mday;
                    ArchivesItem itemDate = new ArchivesItem();
                    itemDate.setItemYype(1);
                    itemDate.setTimeDate(monthDay);
                    archivesItems.add(itemDate);
                }
                ArchivesItem item = new ArchivesItem();
                item.setReportUrl(bean.getLink());
                item.setItemYype(2);
                item.setTimeMin(getTime(bean.getCreateTime()));
                item.setCreateDate(bean.getCreateTime());
                item.setTimeDate(getDate(bean.getCreateTime()));
                item.setTextOne(bean.getTypeName());
                if (getActivity() != null) {
                    if (getString(R.string.report_pressure).equals(bean.getTypeName())) {
                        String name = bean.getName();
                        if (!TextUtils.isEmpty(name)) {
                            String[] split = name.split("-");
                            String str = getString(R.string.report_height_pressure) + split[1] + " " + getString(R.string.report_low_pressure) + split[0];
                            item.setTextTwo(str);
                        }
                    } else {
                        item.setTextTwo(bean.getName());
                    }
                }
                item.setPath(bean.getLink());
                archivesItems.add(item);
            }
        }
        return archivesItems;
    }

    private String getTime(Long time) {
        String format = "";
        try {
            format = new SimpleDateFormat("HH:mm").format(new Date(time));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return format;
    }

    private String getDate(Long time) {
        String format = "";
        try {
            format = new SimpleDateFormat("MM月dd日").format(new Date(time));
            Log.d("archivesGetDate", "getDate: ");
            if(Constant.isEnglish){
                Log.d("archivesGetDate", "getDate: is English");
               format =  format.replace("月","-");
               format =  format.replace("日"," ");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return format;
    }

    private void EGCItemJump(int position, Intent intentEGC) {
        ArchivesItem report = archivesItems.get(position);
        //判断是否含有未上传心电数据
        if (report.getUploadFailedData() != null) {
            if (report.getUploadFailedData().getStatus() > 0) {
                ToastUtil.invokeShortTimeToast(getActivity(), getString2(R.string.archives_frag_qingninxianscxdsj));
            }
        } else {
            String path = report.getPath();
            //得到上传之后返回的文件名
            int intPath1 = path.lastIndexOf("/") + 1;
            String strPathName = path.substring(intPath1);
            if (Constant.DEBUG) {
                Log.i("ECGReviewActivity", "上传之后返回的文件名-->" + strPathName);
            }
            //判断该文件是否存在
            //保存的文件路径
            String strToRename = Constant.BASE_PATH + ECG_FILE_PATH + "/" + strPathName;
            File file = new File(strToRename);
            //如果该文件存在 直接使用本地的文件否则就保存在本地
            if (file.exists()) {
                if (Constant.DEBUG) {
                    Log.i("ECGReviewActivity", "文件存在的路径-->" + file.getAbsolutePath());
                }
                intentEGC = new Intent(getActivity(), ECGReviewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(ECG_FILE_BUNDLE_NAME, strPathName);
                intentEGC.putExtras(bundle);
                intentEGC.putExtra("title", getString2(R.string.report_title_xl));
                startActivity(intentEGC);

            } else {
                //下载ecg文件
                dowmLoadECGPath(report.getPath());
            }
        }
    }

    private void newEGCItemJump(ArchivesItem report) {
        //判断是否含有未上传心电数据
        if (report.getUploadFailedData() != null) {
            if (report.getUploadFailedData().getStatus() > 0) {
                ToastUtil.invokeShortTimeToast(getActivity(), getString2(R.string.archives_frag_qingninxianscxdsj));
            }
        } else {
            String path = report.getPath();
            //得到上传之后返回的文件名
            int intPath1 = path.lastIndexOf("/") + 1;
            String strPathName = path.substring(intPath1);
            if (Constant.DEBUG) {
                Log.i("ECGReviewActivity", "上传之后返回的文件名-->" + strPathName);
            }
            //判断该文件是否存在
            //保存的文件路径
            String strToRename = Constant.BASE_PATH + ECG_FILE_PATH + "/" + strPathName;
            File file = new File(strToRename);
            //如果该文件存在 直接使用本地的文件否则就保存在本地
            if (file.exists()) {
                if (Constant.DEBUG) {
                    Log.i("ECGReviewActivity", "文件存在的路径-->" + file.getAbsolutePath());
                }
                Intent intentEGC = new Intent(getActivity(), ECGReviewActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(ECG_FILE_BUNDLE_NAME, strPathName);
                intentEGC.putExtras(bundle);
                intentEGC.putExtra("title", getString2(R.string.report_title_xl));
                startActivity(intentEGC);

            } else {
                //下载ecg文件
                dowmLoadECGPath(report.getPath());
            }
        }
    }

    //下载文件到本地
    private void dowmLoadECGPath(final String strPath) {
        new Thread(new Runnable() {
            @Override
            public void run() {

                if (Constant.DEBUG) {
                    Log.i("ECGReviewActivity", "urlstr---->" + strPath);
                }
                try {
                    URL url = new URL(strPath);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestMethod("POST");
                    connection.setRequestProperty("Charset", "UTF-8");
                    connection.connect();
                    int file_leng = connection.getContentLength();
                    if (Constant.DEBUG) {
                        Logger.i("ECGReviewActivity", "file length---->" + file_leng);
                    }
                    BufferedInputStream bin = new BufferedInputStream(connection.getInputStream());

                    //保存到本地路径
                    //得到上传之后返回的文件名
                    int intPath2 = strPath.lastIndexOf("/") + 1;
                    String strLoadPathName = strPath.substring(intPath2);
                    if (Constant.DEBUG) {
                        Logger.i("ECGReviewActivity", "下载之后返回的文件名-->" + strLoadPathName);
                    }

                    String filePath = Constant.BASE_PATH + ECGDrawWaveManager.ECG_FILE_PATH + "/" + strLoadPathName;
                    File file = new File(filePath);
                    if (!file.getParentFile().exists()) {
                        file.getParentFile().mkdirs();
                    }
                    OutputStream out = new FileOutputStream(file);
                    int size = 0;
                    int len = 0;
                    byte[] buf = new byte[1024];
                    while ((size = bin.read(buf)) != -1) {
                        len += size;
                        out.write(buf, 0, size);
                    }
                    if (Constant.DEBUG) {
                        Logger.i("ECGReviewActivity", "文件长度-->" + file.length() + "-->len-->" + len);
                    }
                    Message message = handler.obtainMessage();
                    message.what = LOAD_MESSAGE;
                    message.obj = strLoadPathName;
                    handler.sendMessage(message);


                    bin.close();
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }).start();

    }

    private int ECGstatus() {
        int status = 0;
        String path = Constant.BASE_PATH + "/ECG" + "/";
        File file = new File(path);
        if (file.exists()) {
            File[] files = file.listFiles();
            if (files.length > 0) {
                return 2;
            }
        }
        return status;
    }

    private void refeshByTag(String tag) {
        rb_all.setChecked(false);
        clickedRB.setChecked(false);
        switch (tag) {
            //血氧
            case "xy":
                clickedRB = rb_SPO2H;
                clickedRB.setChecked(true);
                dataType = CheckedDataType.SPO2H;
                loadUrl(dataType);
                break;
            //血压
            case "blood_pressure":
                clickedRB = rb_blood_pressure;
                clickedRB.setChecked(true);
                dataType = CheckedDataType.BLOOD_PRESSURE;
                loadUrl(dataType);
                break;
            case "blood":
                clickedRB = rb_blood;
                clickedRB.setChecked(true);
                dataType = CheckedDataType.BLOOD_SUGAR;
                loadUrl(dataType);
                break;
            default:
                break;
        }
    }

    private String getString2(int id) {
        if (getActivity() != null) {
            return getString(id);
        } else {
            return "";
        }
    }

    private CharSequence autoSplitText(final RadioButton rb) {
        final CharSequence rawCharSequence = rb.getText();
        final String rawText = rawCharSequence.toString(); //原始文本
        final Paint tvPaint = rb.getPaint(); //paint，包含字体等信息
        final float tvWidth = rb.getWidth() - rb.getPaddingLeft() - rb.getPaddingRight(); //控件可用宽度

        //将原始文本按行拆分
        String[] rawTextLines = rawText.replaceAll("\r", "").split("\n");
        StringBuilder sbNewText = new StringBuilder();
        for (String rawTextLine : rawTextLines) {
            if (tvPaint.measureText(rawTextLine) <= tvWidth) {
                //如果整行宽度在控件可用宽度之内，就不处理了
                sbNewText.append(rawTextLine);
            } else {
                //如果整行宽度超过控件可用宽度，则按字符测量，在超过可用宽度的前一个字符处手动换行
                float lineWidth = 0;
                for (int cnt = 0; cnt != rawTextLine.length(); ++cnt) {
                    char ch = rawTextLine.charAt(cnt);
                    lineWidth += tvPaint.measureText(String.valueOf(ch));
                    if (lineWidth <= tvWidth) {
                        sbNewText.append(ch);
                    } else {
                        if (cnt - 2 >= 0 && rawTextLine.charAt(cnt - 1) >= 'A' && rawTextLine.charAt(cnt - 1) <= 'z' && rawTextLine.charAt(cnt - 2) >= 'A' && rawTextLine.charAt(cnt - 2) <= 'z') {
                            sbNewText.deleteCharAt(sbNewText.length() - 1);
                            sbNewText.append("-\n");
                            lineWidth = 0;
                            cnt -= 2;
                        } else {
                            sbNewText.append("\n");
                            lineWidth = 0;
                            --cnt;
                        }
                    }
                }
            }
            sbNewText.append("\n");
        }

//        //把结尾多余的\n去掉
//        if (!rawText.endsWith("\n")) {
//            sbNewText.deleteCharAt(sbNewText.length() - 1);
//        }
//        //使用TextView设置的Span格式
//        SpannableString sp = new SpannableString(sbNewText.toString());
//        if (rawCharSequence instanceof Spanned) {
//            TextUtils.copySpansFrom((Spanned) rawCharSequence, 0, rawCharSequence.length(), null, sp, 0);
//        }

        return sbNewText;
    }

}
