package com.hxlm.hcyandroid.bean;

/**
 * 步骤提示  虚拟数据
 *
 * @author Administrator
 */
public class CheckStep {

    private int stepNumber;
    private String stepImageUrl;
    private String stepText;

    public CheckStep(int stepNumber, String stepImageUrl, String stepText) {
        super();
        this.stepNumber = stepNumber;
        this.stepImageUrl = stepImageUrl;
        this.stepText = stepText;
    }

    public int getStepNumber() {
        return stepNumber;
    }

    public void setStepNumber(int stepNumber) {
        this.stepNumber = stepNumber;
    }

    public String getStepImageUrl() {
        return stepImageUrl;
    }

    public void setStepImageUrl(String stepImageUrl) {
        this.stepImageUrl = stepImageUrl;
    }

    public String getStepText() {
        return stepText;
    }

    public void setStepText(String stepText) {
        this.stepText = stepText;
    }
}
