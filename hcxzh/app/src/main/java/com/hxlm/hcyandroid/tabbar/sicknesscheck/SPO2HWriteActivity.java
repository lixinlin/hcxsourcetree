package com.hxlm.hcyandroid.tabbar.sicknesscheck;


import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.report.UploadManager;
import com.hxlm.android.hcy.user.ChildMember;
import com.hxlm.android.hcy.user.ChooseMemberDialog;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.CheckedDataType;
import com.hxlm.hcyandroid.tabbar.MyHealthFileBroadcast;
import com.hxlm.hcyandroid.util.ToastUtil;
import com.hxlm.hcyandroid.view.ContainsEmojiEditText;
import com.hxlm.hcyphone.MainActivity;

import java.util.List;

public class SPO2HWriteActivity extends BaseActivity implements OnClickListener {

    private Context context;
    private ContainsEmojiEditText et_xueyang;// 当前血氧
    private double xueyang;
    private ImageView iv_save_xueyang;// 保存
    private String isNormal;// 是否正常
    private UploadManager uploadManager;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_spo2_hwrite);
        context = SPO2HWriteActivity.this;

    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.spo2_title), titleBar, 1);
        et_xueyang = (ContainsEmojiEditText) findViewById(R.id.et_xueyang);
        iv_save_xueyang = (ImageView) findViewById(R.id.iv_save_xueyang);
        iv_save_xueyang.setOnClickListener(this);
    }

    @Override
    public void initDatas() {
        uploadManager = new UploadManager();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_save_xueyang:
                String str_xueyang = et_xueyang.getText().toString().trim();

                if (!TextUtils.isEmpty(str_xueyang)) {
                    if (!"0".equals(str_xueyang)) {
                        xueyang = Double.parseDouble(str_xueyang);

                        if (xueyang <= 0 || xueyang > 100) {
                            ToastUtil.invokeShortTimeToast(context, getString(R.string.spo2_write_xueyangfanwei));
                        } else {

                            if (xueyang >= 95 && xueyang <= 100) {
                                isNormal = getString(R.string.spo2_write_isNormal_zhengchang);
                            } else if (xueyang >= 0 && xueyang < 95) {
                                isNormal = getString(R.string.spo2_write_isNormal_piandi);
                            }

                            LoginControllor.requestLogin(SPO2HWriteActivity.this, new OnCompleteListener() {
                                @Override
                                public void onComplete() {
                                    List<ChildMember> childMembers = LoginControllor.getLoginMember().getMengberchild();
                                    int size = childMembers.size();
                                    if (true) {
                                        // 输入正常值才上传数据
                                        uploadManager.uploadCheckedData(CheckedDataType.SPO2H, String.valueOf(xueyang / 100), 0,
                                                new AbstractDefaultHttpHandlerCallback(SPO2HWriteActivity.this) {
                                                    @Override
                                                    protected void onResponseSuccess(Object obj) {
                                                        new SPO2HWriteDialog(context, getString(R.string.spo2_write_dangqianxueyang_value) + xueyang, isNormal).show();
                                                    }
                                                });
                                    } else {
                                        new ChooseMemberDialog(SPO2HWriteActivity.this, new OnCompleteListener() {
                                            @Override
                                            public void onComplete() {

                                                // 输入正常值才上传数据
                                                uploadManager.uploadCheckedData(CheckedDataType.SPO2H, String.valueOf(xueyang / 100), 0,
                                                        new AbstractDefaultHttpHandlerCallback(SPO2HWriteActivity.this) {
                                                            @Override
                                                            protected void onResponseSuccess(Object obj) {
                                                                new SPO2HWriteDialog(context, getString(R.string.spo2_write_dangqianxueyang_value) + xueyang, isNormal).show();
                                                            }
                                                        });
                                            }
                                        }).show();
                                    }

                                }
                            });
                        }
                    }else {
                        ToastUtil.invokeShortTimeToast(context,getString(R.string.bpw_tips_input_error));
                    }
                } else {
                    ToastUtil.invokeShortTimeToast(context, getString(R.string.bsc_tips_not_input));
                }
                break;

            default:
                break;
        }
    }

    public class SPO2HWriteDialog extends AlertDialog implements
            OnClickListener {

        Context context;
        String tishi;
        String isnormal;

        String str_tishi = "";

        public SPO2HWriteDialog(Context context, String tishi, String isnormal) {
            super(context);
            this.context = context;
            this.tishi = tishi;
            this.isnormal = isnormal;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.spo2hwrite_submit_prompt);

            TextView tv_breath_prompt_tishi = (TextView) findViewById(R.id.tv_breath_prompt_tishi);
            tv_breath_prompt_tishi.setText(tishi);

            // 血氧
            TextView tv_is_normal = (TextView) findViewById(R.id.tv_is_normal);

            // 返回检测
            TextView text_back = (TextView) findViewById(R.id.text_back);
            text_back.setOnClickListener(this);

            //查看档案
            TextView text_see_dangan = (TextView) findViewById(R.id.text_see_dangan);
            text_see_dangan.setOnClickListener(this);

//            if ("正常".equals(isnormal)) {
//                str_tishi = "血氧饱和度正常";
//                SpannableStringBuilder style = new SpannableStringBuilder(
//                        str_tishi);
//                style.setSpan(
//                        new ForegroundColorSpan(getResources().getColor(
//                                R.color.submit_normal)), 5, 7,
//                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//                tv_is_normal.setText(style);
//
//            } else if ("偏低".equals(isnormal)) {
//                str_tishi = "血氧饱和度偏低。建议监测，若持续偏低，请及时到医院就诊。";
//                SpannableStringBuilder style = new SpannableStringBuilder(
//                        str_tishi);
//                style.setSpan(
//                        new ForegroundColorSpan(getResources().getColor(
//                                R.color.submit_not_normal)), 5, 7,
//                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//                tv_is_normal.setText(style);
//            }

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                // 返回检测
                case R.id.text_back:
                    this.dismiss();
                    SPO2HWriteActivity.this.finish();
                    break;
                //查看档案
                case R.id.text_see_dangan:
                    this.dismiss();
                    // 动态注册广播使用隐士Intent
                    Intent intent = new Intent(MyHealthFileBroadcast.ACTION);
                    intent.putExtra("ArchivesFragment", "3");
                    intent.putExtra("otherReport", "true");
                    intent.putExtra("Jump", 7);
                    intent.putExtra("tag","xy");
                    SPO2HWriteActivity.this.sendBroadcast(intent);

                    Intent intent2 = new Intent(SPO2HWriteActivity.this, MainActivity.class);

                    intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//清除MainActivity之前所有的activity
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP); //沿用之前的MainActivity
                    startActivity(intent2);
                    break;
            }
        }

    }

}
