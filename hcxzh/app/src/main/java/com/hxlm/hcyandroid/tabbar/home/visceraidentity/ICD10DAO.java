package com.hxlm.hcyandroid.tabbar.home.visceraidentity;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.hxlm.hcyandroid.bean.ICDItem;
import com.hxlm.hcyandroid.util.LanguageUtil;
import com.hxlm.hcyandroid.util.ToastUtil;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ICD10DAO {
    private ICD10OpenHelper openHelper;
    private Context context;

    public ICD10DAO(Context context) {
        super();
        this.context = context;
        openHelper = new ICD10OpenHelper(context);
    }

    public List<ICDItem> queryByNameAndSex(String content, boolean isMan) {
        int sexFromActivity = 0;
        if (!isMan) {

        }
        List<ICDItem> items = new ArrayList<ICDItem>();
        SQLiteDatabase db = openHelper.getWritableDatabase();
        Cursor cursor = null;
        if (LanguageUtil.getInstance().getAppLocale(context).getLanguage().equals("en")){
             cursor = db.rawQuery("select * from ICD10 where content_en like ? limit 200", new String[]{"%" + content + "%"});
        }else {
            cursor = db.rawQuery("select * from ICD10 where content like ? limit 200", new String[]{"%" + content + "%"});
        }
        while (cursor.moveToNext()) {
            int sex = cursor.getInt(cursor.getColumnIndex("SEX"));
            if (sex == sexFromActivity || sex == 2) {
                String desc = "";
                if (LanguageUtil.getInstance().getAppLocale(context).getLanguage().equals("en")){
                     desc = cursor.getString(cursor.getColumnIndex("content_en"));
                }else {
                     desc = cursor.getString(cursor.getColumnIndex("content"));
                }
                String[] descs = desc.split("_");
                String name = descs[0];
                String sp = "";
                if (descs.length == 2) {
                    sp = descs[1];
                }
                String micd = cursor.getString(cursor.getColumnIndex("MICD"));
                String mtji = cursor.getString(cursor.getColumnIndex("MTJI"));

                ICDItem item = new ICDItem(name, sp, micd, mtji, sex);
                items.add(item);
                Log.d("icd10", "queryByNameAndSex: "+item.toString());
            }
        }
        cursor.close();
        db.close();
        return items;
    }
}
