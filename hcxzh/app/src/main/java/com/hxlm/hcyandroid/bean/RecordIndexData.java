package com.hxlm.hcyandroid.bean;

import java.util.Date;

public class RecordIndexData {
    private String category;
    private String result;
    private long time;
    private int imageResourceId;
    private Date createTime;

    public RecordIndexData(int imageResourceId, String category, String result, long time) {
        super();
        this.imageResourceId = imageResourceId;
        this.category = category;
        this.result = result;
        this.time = time;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public int getImageResourceId() {
        return imageResourceId;
    }


    public void setImageResourceId(int imageResourceId) {
        this.imageResourceId = imageResourceId;
    }


    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "RecordIndexData{" +
                "category='" + category + '\'' +
                ", result='" + result + '\'' +
                ", time=" + time +
                ", imageResourceId=" + imageResourceId +
                ", createTime=" + createTime +
                '}';
    }
}
