package com.hxlm.hcyandroid.tabbar.usercenter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.bean.ArchivesItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lixinlin on 2018/10/15.
 */

public class ArchivesBaogaoAdapter extends BaseAdapter {

    private Context mContext;

    // 4种不同的布局
    public static final int VALUE_QUATER_IMAGE = 0;
    public static final int VALUE_TIME_TIP = 1;
    public static final int VALUE_BAOGAO_TEXT = 2;
    public static final int VALUE_BAOGAO_TEXT_B = 3;
    public static final int VALUE_BAOGAO_TEXT_C = 4;

    private LayoutInflater mInflater;

    private List<ArchivesItem> myList = new ArrayList<>();

    public ArchivesBaogaoAdapter(Context context) {
        this.mContext = context;
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setMyList(List<ArchivesItem> myList) {
        this.myList = myList;
        this.notifyDataSetChanged();
    }
    /**
     * 根据数据源的position返回需要显示的的layout的type
     * type的值必须从0开始
     */
    @Override
    public int getItemViewType(int position) {

        ArchivesItem baogao = myList.get(position);
        int type = baogao.getItemYype();
        return type;
    }

    /**
     * 返回所有的layout的数量
     */
    @Override
    public int getViewTypeCount() {
        return 5;
    }


    @Override
    public int getCount() {
        return myList.size();
    }

    @Override
    public Object getItem(int arg0) {
        return myList.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup arg2) {

        ArchivesItem msg = myList.get(position);
        boolean lineGone = false;
        if(position+1< myList.size()){
            ArchivesItem nextMsg = myList.get(position+1);
            if(nextMsg!=null){
                if(nextMsg.getItemYype()==1||nextMsg.getItemYype()==0){
                    lineGone = true;
                }
            }
        }else{
            lineGone = true;
        }

        boolean lineUpGone = false;
        if(position-1>=0){
            ArchivesItem nextMsg = myList.get(position-1);
            if(nextMsg!=null){
                if(nextMsg.getItemYype()==0){
                    lineUpGone = true;
                }
            }
        }

        int type = getItemViewType(position);
        ViewHolderTime holderTime = null;
        ViewHolderBaogao holderBaogao = null;
        ViewHolderBaogaoB holderBaogaoB = null;
        ViewHolderBaogaoC holderBaogaoC = null;
        ViewHoldeYueDuBaogao holdeYueDuBaogao = null;

        if (convertView == null) {
            switch (type) {
                //日期
                case VALUE_TIME_TIP:
                    holderTime = new ViewHolderTime();
                    convertView = mInflater.inflate(R.layout.archives_baogao_time_item, null);
                    holderTime.tvTimeTip = convertView.findViewById(R.id.tv_time_tip);
                    convertView.setTag(holderTime);
                    break;
                // 报告
                case VALUE_BAOGAO_TEXT:
                    holderBaogao = new ViewHolderBaogao();
                    convertView = mInflater.inflate(R.layout.archives_baogao_text_item, null);
                    holderBaogao.textOne = convertView.findViewById(R.id.tv_text_one);
                    holderBaogao.textTwo = convertView.findViewById(R.id.tv_text_two);
                    holderBaogao.textUnit = convertView.findViewById(R.id.tv_text_unit);
                    holderBaogao.tv_time_min = convertView.findViewById(R.id.tv_time);
                    holderBaogao.iv_left_dot = convertView.findViewById(R.id.iv_left_dot);
                    holderBaogao.line = convertView.findViewById(R.id.view_line2);
                    holderBaogao.line1 = convertView.findViewById(R.id.view_line1);
                    holderBaogao.iv_one = convertView.findViewById(R.id.iv_one);
                    convertView.setTag(holderBaogao);
                    break;
                // 报告
                case VALUE_BAOGAO_TEXT_B:
                    holderBaogaoB = new ViewHolderBaogaoB();
                    convertView = mInflater.inflate(R.layout.archives_baogao_text_item_b, null);
                    holderBaogaoB.textOne = (TextView) convertView.findViewById(R.id.tv_text_one);
                    holderBaogaoB.textTwo = (TextView) convertView.findViewById(R.id.tv_text_two);
                    holderBaogaoB.textThree = (TextView) convertView.findViewById(R.id.tv_text_three);
                    holderBaogaoB.textFour = (TextView) convertView.findViewById(R.id.tv_text_four);
                    holderBaogaoB.tv_time_min = convertView.findViewById(R.id.tv_time);
                    holderBaogaoB.line = convertView.findViewById(R.id.view_line2);
                    holderBaogaoB.line1 = convertView.findViewById(R.id.view_line1);
                    convertView.setTag(holderBaogaoB);
                    break;
                //月份
                case VALUE_QUATER_IMAGE:
                    holdeYueDuBaogao = new ViewHoldeYueDuBaogao();
                    convertView = mInflater.inflate(R.layout.archives_baogao_yuefen_item, null);
                    holdeYueDuBaogao.quater = convertView.findViewById(R.id.tv_quater);
                    holdeYueDuBaogao.date = convertView.findViewById(R.id.tv_date);
                    holdeYueDuBaogao.background = convertView.findViewById(R.id.ll_background);
                    convertView.setTag(holdeYueDuBaogao);
                    break;
                //病例
                case VALUE_BAOGAO_TEXT_C:
                    holderBaogaoC = new ViewHolderBaogaoC();
                    convertView = mInflater.inflate(R.layout.archives_baogao_text_item_c, null);
                    holderBaogaoC.textOne = convertView.findViewById(R.id.tv_text_one);
                    holderBaogaoC.textTwo = convertView.findViewById(R.id.tv_text_two);
                    holderBaogaoC.textThree = convertView.findViewById(R.id.tv_text_three);
                    holderBaogaoC.line = convertView.findViewById(R.id.view_line2);
                    holderBaogaoC.line1 = convertView.findViewById(R.id.view_line1);
                    convertView.setTag(holderBaogaoC);
                    break;
                default:
                    break;
            }

        } else {
            switch (type) {
                case VALUE_TIME_TIP:
                    holderTime = (ViewHolderTime) convertView.getTag();
                    break;
                case VALUE_BAOGAO_TEXT:
                    holderBaogao = (ViewHolderBaogao) convertView.getTag();
                    break;
                case VALUE_BAOGAO_TEXT_B:
                    holderBaogaoB = (ViewHolderBaogaoB) convertView.getTag();
                    break;
                case VALUE_QUATER_IMAGE:
                    holdeYueDuBaogao = (ViewHoldeYueDuBaogao) convertView.getTag();
                    break;
                case VALUE_BAOGAO_TEXT_C:
                    holderBaogaoC = (ViewHolderBaogaoC) convertView.getTag();
                    break;
                default:
                    break;
            }
        }


        switch (type) {

            case VALUE_TIME_TIP:
                holderTime.tvTimeTip.setText(msg.getTimeDate());
                break;
            case VALUE_BAOGAO_TEXT:
                holderBaogao.textUnit.setVisibility(View.INVISIBLE);
                holderBaogao.textUnit.setVisibility(View.VISIBLE);
                String textOne = msg.getTextOne();
                holderBaogao.textOne.setText(textOne);
                //设置图标
                holderBaogao.iv_one.setVisibility(View.INVISIBLE);
                if (mContext.getString(R.string.archives_frag_xueya).equalsIgnoreCase(textOne)){
                    holderBaogao.iv_one.setImageResource(R.drawable.dangan_xueya);
                }else if (mContext.getString(R.string.archives_frag_xinlv).equalsIgnoreCase(textOne)){
                    holderBaogao.iv_one.setImageResource(R.drawable.dangan_xinlv);
                }else if (mContext.getString(R.string.archives_frag_xuetang).equalsIgnoreCase(textOne)){
                    holderBaogao.iv_one.setImageResource(R.drawable.dangan_xuetang);
                }else if (mContext.getString(R.string.archives_frag_huxi).equalsIgnoreCase(textOne)){
                    holderBaogao.iv_one.setImageResource(R.drawable.dangan_huxi);
                }else if (mContext.getString(R.string.archives_frag_tiwen).equalsIgnoreCase(textOne)){
                    holderBaogao.iv_one.setImageResource(R.drawable.dangan_tiwen);
                }else if (mContext.getString(R.string.archives_frag_jingluo).equalsIgnoreCase(textOne)){
                    holderBaogao.iv_one.setImageResource(R.drawable.dangan_jingluo);
                }else if (mContext.getString(R.string.archives_frag_tizhi).equalsIgnoreCase(textOne)){
                    holderBaogao.iv_one.setImageResource(R.drawable.dangan_tizhi);
                }else if (mContext.getString(R.string.archives_frag_xueyang).equalsIgnoreCase(textOne)){
                    holderBaogao.iv_one.setImageResource(R.drawable.dangan_xueyang);
                }else if (mContext.getString(R.string.archives_frag_zangfu).equalsIgnoreCase(textOne)){
                    holderBaogao.iv_one.setImageResource(R.drawable.dangan_zangfu);
                }
                holderBaogao.iv_one.setVisibility(View.VISIBLE);
                    //添加单位
                if(mContext.getString(R.string.archives_rightdraw_xueya).equals(textOne)){
                    holderBaogao.textUnit.setText("(mmHg)");
                }
                else if (mContext.getString(R.string.archives_rightdraw_xinlv).equals(textOne)){
                    holderBaogao.textUnit.setText("("+mContext.getString(R.string.bpc_unit_count)+")");
                }
                else if (mContext.getString(R.string.archives_rightdraw_xuetang).equals(textOne)){
                    holderBaogao.textUnit.setText("(mmol/L)");
                }
                else if (mContext.getString(R.string.archives_rightdraw_huxi).equals(textOne)){
                    holderBaogao.textUnit.setText("("+mContext.getString(R.string.abg_ci)+"/30s)");
                }
                else if (mContext.getString(R.string.archives_rightdraw_tiwen).equals(textOne)){
                    holderBaogao.textUnit.setText("(℃)");
                }else {
                    if (Constant.isEnglish) {
                        holderBaogao.textUnit.setVisibility(View.GONE);
                    }else {
                        holderBaogao.textUnit.setVisibility(View.INVISIBLE);
                    }
                }
                String textTwo = msg.getTextTwo();
                if(!TextUtils.isEmpty(textTwo)){
                    if(textTwo.lastIndexOf(",")==textTwo.length()-1){
                        textTwo = textTwo.substring(0,textTwo.length()-1);
                    }
                }
                holderBaogao.textTwo.setText(textTwo);

                if(msg.getTimeMin()==null||msg.getTimeMin().equals("")){
                    holderBaogao.iv_left_dot.setVisibility(View.VISIBLE);
                    holderBaogao.tv_time_min.setVisibility(View.GONE);
                }else {
                    holderBaogao.iv_left_dot.setVisibility(View.GONE);
                    holderBaogao.tv_time_min.setText(msg.getTimeMin());
                    holderBaogao.tv_time_min.setVisibility(View.VISIBLE);
                }

                if(lineGone){
                    holderBaogao.line.setVisibility(View.INVISIBLE);
                }else {
                    holderBaogao.line.setVisibility(View.VISIBLE);
                }
                if(lineUpGone){
                    holderBaogao.line1.setVisibility(View.INVISIBLE);
                }else {
                    holderBaogao.line1.setVisibility(View.VISIBLE);
                }


                break;
            case VALUE_BAOGAO_TEXT_B:
                holderBaogaoB.textOne.setText(msg.getTextOne());
                String textTwo2 = msg.getTextTwo();
                if(!TextUtils.isEmpty(textTwo2)){
                    if(textTwo2.lastIndexOf(",")==textTwo2.length()-1){
                        textTwo = textTwo2.substring(0,textTwo2.length()-1);
                    }
                }
                holderBaogaoB.textTwo.setText(textTwo2);
                holderBaogaoB.textThree.setText(msg.getTextThree());
                String textFour = msg.getTextFour();
                if(!TextUtils.isEmpty(textFour)){
                    if(textFour.lastIndexOf(",")==textFour.length()-1){
                        textFour = textFour.substring(0,textFour.length()-1);
                    }
                }
                holderBaogaoB.textFour.setText(textFour);
                holderBaogaoB.tv_time_min.setText(msg.getTimeMin());
                if(lineGone){
                    holderBaogaoB.line.setVisibility(View.INVISIBLE);
                }else {
                    holderBaogaoB.line.setVisibility(View.VISIBLE);
                }
                if(lineUpGone){
                    holderBaogaoB.line1.setVisibility(View.INVISIBLE);
                }else {
                    holderBaogaoB.line1.setVisibility(View.VISIBLE);
                }

                break;
            case VALUE_QUATER_IMAGE:
                holdeYueDuBaogao.quater.setText(msg.getQuarter());
                holdeYueDuBaogao.date.setText(msg.getYear());
                if (msg.getBackgroundId() != 0) {
                    holdeYueDuBaogao.background.setBackground(mContext.getResources().getDrawable(msg.getBackgroundId()));
                }
                break;
            case VALUE_BAOGAO_TEXT_C:
                holderBaogaoC.textOne.setText(msg.getTextOne());
                holderBaogaoC.textTwo.setText(msg.getTextTwo());
                holderBaogaoC.textThree.setText(msg.getTextThree());
                if(lineGone){
                    holderBaogaoC.line.setVisibility(View.INVISIBLE);
                }else {
                    holderBaogaoC.line.setVisibility(View.VISIBLE);
                }
                if(lineUpGone){
                    holderBaogaoC.line1.setVisibility(View.INVISIBLE);
                }else {
                    holderBaogaoC.line1.setVisibility(View.VISIBLE);
                }
                break;
            default:
                break;
        }

        return convertView;
    }

    class ViewHolderTime {
        private TextView tvTimeTip;// 时间
    }

    class ViewHolderBaogao {
        private View line; //需要隐藏的线条
        private View line1; //需要隐藏的线条
        private TextView textOne;
        private TextView textTwo;
        private TextView textUnit;
        private TextView tv_time_min;// 时间
        private ImageView iv_left_dot;// 时间
        private ImageView iv_one;
    }

    class ViewHolderBaogaoB {
        private View line; //需要隐藏的线条
        private View line1; //需要隐藏的线条
        private TextView textOne;
        private TextView textTwo;
        private TextView textThree;
        private TextView textFour;
        private TextView tv_time_min;// 时间
    }

    class ViewHoldeYueDuBaogao {
        private TextView quater;
        private TextView date;
        private LinearLayout background;

    }

    class ViewHolderBaogaoC{
        private View line; //需要隐藏的线条
        private View line1; //需要隐藏的线条
        private TextView textOne; //科室
        private TextView textTwo; //医生
        private TextView textThree; //主诉
    }

}
