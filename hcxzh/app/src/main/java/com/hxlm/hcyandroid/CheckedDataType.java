package com.hxlm.hcyandroid;

public class CheckedDataType {
    public static final int ECG = 10;//心电图
    public static final int SPO2H = 20;//血氧
    public static final int BLOOD_PRESSURE = 30;//血压
    public static final int TEMPERATURE = 40;//体温
    public static final int BREATH = 50;//呼吸
    public static final int BLOOD_SUGAR = 60;//血糖
}
