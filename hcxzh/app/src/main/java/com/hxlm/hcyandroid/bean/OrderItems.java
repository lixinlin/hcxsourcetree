package com.hxlm.hcyandroid.bean;

import java.io.Serializable;

/**
 * OrderItem中的数据
 *
 * @author Administrator
 */
public class OrderItems implements Serializable {
    private int id;
    private long createDate;
    private long modifyDate;
    private String sn;// 订单号
    private String name;
    private String fullName;
    private double price;
    private String weight;
    private String thumbnail;
    private boolean isGift;
    private int quantity;
    private double subtotal;
    private double totalWeight;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    public long getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(long modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public boolean isGift() {
        return isGift;
    }

    public void setGift(boolean isGift) {
        this.isGift = isGift;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public double getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(double totalWeight) {
        this.totalWeight = totalWeight;
    }

    @Override
    public String toString() {
        return "OrderItems [id=" + id + ", createDate=" + createDate
                + ", modifyDate=" + modifyDate + ", sn=" + sn + ", name="
                + name + ", fullName=" + fullName + ", price=" + price
                + ", weight=" + weight + ", thumbnail=" + thumbnail
                + ", isGift=" + isGift + ", quantity=" + quantity
                + ", subtotal=" + subtotal + ", totalWeight=" + totalWeight
                + "]";
    }


}
