package com.hxlm.hcyandroid.tabbar.usercenter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.*;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.*;
import com.hxlm.hcyandroid.bean.HealthLecture;
import com.hxlm.hcyandroid.bean.MyHealthLecture;
import com.hxlm.hcyandroid.datamanager.LectureManager;
import com.hxlm.hcyandroid.tabbar.healthinformation.HealthLecturePayInformationActivity;
import com.hxlm.hcyandroid.util.*;
import com.hxlm.hcyandroid.view.ConfirmDialog;
import com.hxlm.hcyandroid.view.ShowReserveSnDialog;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MyHealthLectureActivity extends BaseActivity {

    private ListView lv_myLecture;
    private List<MyHealthLecture> myHealthLectures = new ArrayList<MyHealthLecture>();
    private RelativeLayout rl_no_order;
    private Dialog waittingDialog;
    private MyHealthLectureAdapter adapter;
    private LectureManager lectureManager;

    protected ImageLoader imageLoader = ImageLoader.getInstance();
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

    private ConfirmDialog downloadDialog;// 取消预约确认

    private boolean isCancle = false;// 点击支付按钮判断当前的订单是否已经取消
    private MyHealthLecture lectureToPay;


    private String strYearNow;//当前年
    private String strMouthNow;//当前月
    private String strDayNow;//当前日

    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (waittingDialog != null && waittingDialog.isShowing()) {
                waittingDialog.dismiss();
            }
            switch (msg.what) {
                // 点击支付按钮判断该订单是否已经取消
                case MessageType.MESSAGE_CHECK_IS_CANCLED:
                    if (msg.arg1 == ResponseStatus.SUCCESS) {
                        boolean isCancled = (Boolean) msg.obj;
                        if (isCancled) {
                            ToastUtil.invokeShortTimeToast(
                                    MyHealthLectureActivity.this, getString(R.string.my_health_lecture_gaijianguoyiquxiao));
                            if (adapter != null) {
                                adapter.notifyDataSetChanged();
                            }
                        } else {
                            continuePay();
                        }
                    }
                    break;

                case MessageType.MESSAGE_GET_MY_LECTURE:
                    if (msg.arg1 == ResponseStatus.SUCCESS) {
                        myHealthLectures = (List<MyHealthLecture>) msg.obj;
                        if (myHealthLectures.size() > 0) {
                            rl_no_order.setVisibility(View.GONE);
                            lv_myLecture.setVisibility(View.VISIBLE);
                            adapter = new MyHealthLectureAdapter(
                                    MyHealthLectureActivity.this, myHealthLectures,
                                    handler, waittingDialog);
                            lv_myLecture.setAdapter(adapter);
                        } else {
                            rl_no_order.setVisibility(View.VISIBLE);
                            lv_myLecture.setVisibility(View.GONE);
                        }
                    } else if (msg.arg1 == ResponseStatus.UNLOGIN) {
                        ToastUtil.invokeShortTimeToast(
                                MyHealthLectureActivity.this,
                                MyHealthLectureActivity.this
                                        .getString(R.string.unlogin));
                        SharedPreferenceUtil.logoff();

                        LoginControllor.requestLogin(MyHealthLectureActivity.this, new OnCompleteListener() {
                            @Override
                            public void onComplete() {

                                queryRemote();
                            }
                        });


                    } else {
                        ToastUtil.invokeShortTimeToast(
                                MyHealthLectureActivity.this,  getString(R.string.my_health_lecture_huoquwodejzlbsb));
                    }
                    break;
                case MessageType.MESSAGE_CANCLE_ORDER:
                    downloadDialog.dismiss();
                    if (msg.arg1 == ResponseStatus.SUCCESS || msg.arg1 == 1) {
                        ToastUtil.invokeShortTimeToast(
                                MyHealthLectureActivity.this, (String) msg.obj);

                        if (msg.arg1 == ResponseStatus.SUCCESS && adapter != null) {
                            for (MyHealthLecture myHealthLecture : myHealthLectures) {
                                if (myHealthLecture.getId() == msg.arg2) {
                                    myHealthLecture.setStatus("cancelled");
                                }
                            }
                            adapter.notifyDataSetChanged();


                        }
                    } else if (msg.arg1 == ResponseStatus.UNLOGIN) {
                        ToastUtil.invokeShortTimeToast(
                                MyHealthLectureActivity.this,
                                MyHealthLectureActivity.this
                                        .getString(R.string.unlogin));
                        SharedPreferenceUtil.logoff();

                        LoginControllor.requestLogin(MyHealthLectureActivity.this, new OnCompleteListener() {
                            @Override
                            public void onComplete() {

                                queryRemote();
                            }
                        });

                    } else {
                        ToastUtil.invokeShortTimeToast(
                                MyHealthLectureActivity.this,  getString(R.string.my_health_lecture_quxiaojiankangjzsb));
                    }
                    break;

                case MessageType.MESSAGE_QUERY_REMOTE_FAILED:
                    ToastUtil.invokeShortTimeToast(MyHealthLectureActivity.this,
                            MyHealthLectureActivity.this
                                    .getString(R.string.check_net));

                    break;
            }
        }
    };

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_my_health_lecture);

    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this,  getString(R.string.my_health_lecture_jiankangjiangzuo), titleBar, 1);
        lv_myLecture = (ListView) findViewById(R.id.lv_mylecture);
        rl_no_order = (RelativeLayout) findViewById(R.id.rl_no_order);

        // 滑动暂停下载
        lv_myLecture.setOnScrollListener(new PauseOnScrollListener(imageLoader,
                true, false));
    }

    @Override
    public void initDatas() {
        lectureManager = new LectureManager(handler);

        // 获取当前系统时间
        SimpleDateFormat formatNow = new SimpleDateFormat("yyyy年MM月dd日");
        long longNow = System.currentTimeMillis();
        String dateNow = formatNow.format(new Date(longNow));
        // 截取当前年
        int indexyearNow = dateNow.indexOf("年");
        strYearNow = dateNow.substring(0, indexyearNow);
        // 截取当前月
        int indexMouthNow = dateNow.indexOf("月");
        strMouthNow = dateNow.substring(indexyearNow + 1, indexMouthNow);
        // 截取当前日
        int indesDayNow = dateNow.indexOf("日");
        strDayNow = dateNow.substring(indexMouthNow + 1, indesDayNow);

    }

    @Override
    protected void onResume() {
        super.onResume();
        queryRemote();
    }

    // 请求健康讲座数据
    public void queryRemote() {
        if (waittingDialog == null) {
            waittingDialog = DialogUtils
                    .displayWaitDialog(MyHealthLectureActivity.this);
        }
        if (!waittingDialog.isShowing()) {
            waittingDialog.show();
        }
        lectureManager.getMyLectures();
    }

    // 适配器
    class MyHealthLectureAdapter extends BaseAdapter {
        LectureHolder holder = null;
        private Context context;
        private List<MyHealthLecture> list;
        private Handler mHandler;
        LayoutInflater inflater;
        private Dialog waittingDialog;
        private LectureManager lectureManager;

        public MyHealthLectureAdapter(Context context,
                                      List<MyHealthLecture> list, Handler mHandler,
                                      Dialog waittingDialog) {
            this.context = context;
            this.list = list;
            inflater = LayoutInflater.from(context);
            this.mHandler = mHandler;
            this.waittingDialog = waittingDialog;
            lectureManager = new LectureManager(mHandler);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int arg0) {
            return arg0;
        }

        @Override
        public View getView(int position, View view, ViewGroup arg2) {

            if (view == null) {
                holder = new LectureHolder();
                view = inflater.inflate(R.layout.my_health_lecture_list_item,
                        null);

                holder.iv_health_lecture_doctor_icon_lecture = (ImageView) view
                        .findViewById(R.id.iv_health_lecture_doctor_icon_lecture);
                holder.tvdidanid = (TextView) view
                        .findViewById(R.id.tvdingdanid_lecture);
                holder.tvispay = (TextView) view
                        .findViewById(R.id.tvispay_lecture);
                holder.tvlectureTitle = (TextView) view
                        .findViewById(R.id.tvlectureTitle_lecture);
                holder.tvlectureName = (TextView) view
                        .findViewById(R.id.tvlectureName_lecture);
                holder.tvlectureTime_lecture = (TextView) view
                        .findViewById(R.id.tvlectureTime_lecture);
                holder.tvlectureAddressLecture = (TextView) view
                        .findViewById(R.id.tvlectureAddressLecture_lecture);
                holder.tvmoney = (TextView) view
                        .findViewById(R.id.tvmoney_lecture);
                holder.btnpaynow = (Button) view
                        .findViewById(R.id.btnpaynow_lecture);
                holder.tv_count = (TextView) view
                        .findViewById(R.id.tv_count_lecture);
                holder.bt_reserve_sn = (Button) view
                        .findViewById(R.id.bt_reserve_sn_lecture);
                holder.bt_cancle_reserve = (Button) view
                        .findViewById(R.id.cancle_reserve_lecture);
                holder.text1 = (TextView) view.findViewById(R.id.text1_lecture);

                holder.tv_paid = (TextView) view.findViewById(R.id.tv_paid);
                holder.linear_residual_payment = (LinearLayout) view
                        .findViewById(R.id.linear_residual_payment);
                holder.tv_residual_payment = (TextView) view
                        .findViewById(R.id.tv_residual_payment);

                view.setTag(holder);
            } else {
                holder = (LectureHolder) view.getTag();
            }

            MyHealthLecture myHealthLecture = list.get(position);
            HealthLecture healthLecture = myHealthLecture.getLecture();

            // 继续支付
            holder.btnpaynow.setOnClickListener(new OnPayClickListener(context,
                    list, position));
            // 查看预约码
            holder.bt_reserve_sn
                    .setOnClickListener(new OnCheckReserveSnClickListener(
                            context, list, position));
            // 取消预约
            holder.bt_cancle_reserve
                    .setOnClickListener(new OnCancleClickListener(context,
                            list, position));


            if (myHealthLecture.getOrderItem() != null
                    && !TextUtils.isEmpty(myHealthLecture.getOrderItem()
                    .getSn())) {
                holder.text1.setText( getString(R.string.my_health_lecture_dingdanhao));
                holder.tvdidanid
                        .setText(myHealthLecture.getOrderItem().getSn());
            } else {

                // 设置支付状态为空
                holder.tvispay.setText("");
                holder.text1.setText( getString(R.string.my_health_lecture_mianfeijiangzuo));
                holder.tvdidanid.setText("");
                holder.tvmoney.setText("¥ "
                        + myHealthLecture.getActualPayment());
            }
            holder.tvlectureTitle.setText(healthLecture.getTitle());
            holder.tvlectureName.setText(healthLecture.getTalker());

            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .showImageOnLoading(R.drawable.reservation_doctor_icon_bg)
                            // 加载图片时的图片
                    .showImageForEmptyUri(R.drawable.reservation_doctor_icon_bg)
                            // 没有图片资源时的默认图片
                    .showImageOnFail(R.drawable.reservation_doctor_icon_bg)
                            // 加载失败时的图片
                    .cacheInMemory(true)
                            // 启用内存缓存
                    .cacheOnDisk(true)
                            // 启用外存缓存
                    .imageScaleType(ImageScaleType.EXACTLY)
                    .bitmapConfig(Bitmap.Config.RGB_565) // 因为默认是ARGB_8888，
                            // 使用RGB_565会比使用ARGB_8888少消耗2倍的内存
                    .considerExifParams(true) // 启用EXIF和JPEG图像格式
                    .displayer(new RoundedBitmapDisplayer(0)) // 设置显示风格这里是圆角矩形
                    .build();

            // 图片
            ImageLoader.getInstance().displayImage(healthLecture.getPicture(),
                    holder.iv_health_lecture_doctor_icon_lecture, options,
                    animateFirstListener);

            SimpleDateFormat format1 = new SimpleDateFormat("yyyy.MM.dd");
            long beginDate = healthLecture.getBeginDate();
            String str_beginDate = format1.format(new Date(beginDate));
            long endDate = healthLecture.getEndDate();
            String str_endDate = format1.format(new Date(endDate));
            SimpleDateFormat format2 = new SimpleDateFormat("HH:mm");
            long lectureDate = healthLecture.getLectureDate();
            String str_lectureDate = format2.format(new Date(lectureDate));
            String area = healthLecture.getArea();
            String timeAndLocale = str_beginDate + "-" + str_endDate + "日 "
                    + str_lectureDate;

            holder.tvlectureTime_lecture.setText(timeAndLocale);// 讲座时间
            holder.tvlectureAddressLecture.setText(area);// 讲座地址

            holder.tv_count.setText(myHealthLecture.getReserveNums() + "");

            // 预约状态
            String status = "";
            if (!TextUtils.isEmpty(status)) {
                status = "";
            }
            status = myHealthLecture.getStatus();


            //获取当前系统日期，如果讲座结束日期小于当前系统日期，则将取消预约按钮隐藏
            SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日");
            String dateYuYue = format.format(new Date(endDate));
            //截取年
            int indexyear = dateYuYue.indexOf("年");
            String strYear = dateYuYue.substring(0, indexyear);
            //截取月
            int indexMouth = dateYuYue.indexOf("月");
            String strMouth = dateYuYue.substring(indexyear + 1, indexMouth);
            //截取日
            int indesDay = dateYuYue.indexOf("日");
            String strDay = dateYuYue.substring(indexMouth + 1, indesDay);
            //Log.i("Log.i", "预约时间为"+strYear+"-"+strMouth+"-"+strDay);



            // 订单的支付状态
            if (myHealthLecture.getOrderItem() != null
                    && myHealthLecture.getOrderItem().getPaymentStatus() != null) {

                String payStatus = myHealthLecture.getOrderItem()
                        .getPaymentStatus();
                //未付款
                if ("unpaid".equals(payStatus)) {
                    holder.tvispay.setText( getString(R.string.my_health_lecture_weifukuan));
                    holder.tvispay.setTextColor(getResources().getColor(
                            R.color.ff6f5e));

                    holder.linear_residual_payment.setVisibility(View.GONE);
                    holder.tv_paid.setText( getString(R.string.my_health_lecture_shijifukuan));
                    holder.tvmoney.setText("¥ "
                            + myHealthLecture.getActualPayment());

                    // 判断预约状态是否为取消
                    if (MyHealthLecturePayStatus.IS_CANCELLED.equals(status)) {
                        holder.tvispay.setText( getString(R.string.my_health_lecture_yiquxiao));
                        holder.tvispay.setTextColor(getResources().getColor(
                                R.color.ff6f5e));

                        // 继续支付
                        holder.btnpaynow.setVisibility(View.GONE);
                        // 查看预约码
                        holder.bt_reserve_sn.setVisibility(View.GONE);
                        // 取消预约
                        holder.bt_cancle_reserve.setVisibility(View.GONE);
                    } else {
                        // 继续支付
                        holder.btnpaynow.setVisibility(View.VISIBLE);
                        // 查看预约码
                        holder.bt_reserve_sn.setVisibility(View.GONE);
                        // 取消预约
                        holder.bt_cancle_reserve.setVisibility(View.GONE);
                    }

                } else if ("partialPayment".equals(payStatus)) {
                    holder.tvispay.setText( getString(R.string.my_health_lecture_yibufenfukuan));
                    holder.tvispay.setTextColor(getResources().getColor(
                            R.color.ff6f5e));

                    holder.linear_residual_payment.setVisibility(View.VISIBLE);
                    holder.tv_paid.setText( getString(R.string.my_health_lecture_yifukuan));
                    holder.tvmoney.setText("¥ "
                            + myHealthLecture.getOrderItem().getAmountPaid());
                    holder.tv_residual_payment
                            .setText("¥ "
                                    + myHealthLecture.getOrderItem()
                                    .getAmountPayable());

                    // 判断预约状态是否为取消
                    if (MyHealthLecturePayStatus.IS_CANCELLED.equals(status)) {
                        holder.tvispay.setText( getString(R.string.my_health_lecture_tuikuanzhong));
                        holder.tvispay.setTextColor(getResources().getColor(
                                R.color.a69ca00));
                        // 不显示剩余金额
                        holder.linear_residual_payment.setVisibility(View.GONE);
                        holder.tv_paid.setText( getString(R.string.my_health_lecture_tuikuanjine));
                        holder.tvmoney.setText("¥ " + 0);

                        // 继续支付
                        holder.btnpaynow.setVisibility(View.GONE);
                        // 查看预约码
                        holder.bt_reserve_sn.setVisibility(View.GONE);
                        // 取消预约
                        holder.bt_cancle_reserve.setVisibility(View.GONE);
                    } else {
                        // 继续支付
                        holder.btnpaynow.setVisibility(View.VISIBLE);
                        // 查看预约码
                        holder.bt_reserve_sn.setVisibility(View.GONE);
                        //预约年份小于当前年份
                        if (Integer.parseInt(strYear) < Integer.parseInt(strYearNow)) {
                            //取消预约
                            holder.bt_cancle_reserve.setVisibility(View.GONE);
                        }
                        //年份等于当前年份
                        else if (Integer.parseInt(strYear) == Integer.parseInt(strYearNow)) {
                            //预约月份小于当前月份
                            if (Integer.parseInt(strMouth) < Integer.parseInt(strMouthNow)) {

                                //取消预约
                                holder.bt_cancle_reserve.setVisibility(View.GONE);
                            }
                            //预约月份等于当前月份
                            else if (Integer.parseInt(strMouth) == Integer.parseInt(strMouthNow)) {
                                //预约日小于当前日
                                if (Integer.parseInt(strDay) < Integer.parseInt(strDayNow)) {
                                    //取消预约
                                    holder.bt_cancle_reserve.setVisibility(View.GONE);
                                } else {
                                    //取消预约
                                    holder.bt_cancle_reserve.setVisibility(View.VISIBLE);

                                }

                            }
                            //预约月份大于当前月份
                            else {
                                //取消预约
                                holder.bt_cancle_reserve.setVisibility(View.VISIBLE);

                            }
                        }

                        //预约年份大于当前年份
                        else {
                            //取消预约
                            holder.bt_cancle_reserve.setVisibility(View.VISIBLE);

                        }
                    }

                } else if ("paid".equals(payStatus)) {

                    holder.tvispay.setText( getString(R.string.my_health_lecture_yiquanefukuan));
                    holder.tvispay.setTextColor(getResources().getColor(
                            R.color.a5db4fb));

                    holder.linear_residual_payment.setVisibility(View.GONE);
                    holder.tv_paid.setText( getString(R.string.my_health_lecture_shijifukuan2));
                    holder.tvmoney.setText("¥ "
                            + myHealthLecture.getActualPayment());

                    // 判断预约状态是否为取消
                    if (MyHealthLecturePayStatus.IS_CANCELLED.equals(status)) {

                        holder.tvispay.setText( getString(R.string.my_health_lecture_tuikuanzhong));
                        holder.tvispay.setTextColor(getResources().getColor(
                                R.color.a69ca00));
                        // 不显示剩余金额
                        holder.linear_residual_payment.setVisibility(View.GONE);
                        holder.tv_paid.setText( getString(R.string.my_health_lecture_tuikuanjine));
                        holder.tvmoney.setText("¥ " + 0);

                        // 继续支付
                        holder.btnpaynow.setVisibility(View.GONE);
                        // 查看预约码
                        holder.bt_reserve_sn.setVisibility(View.GONE);
                        // 取消预约
                        holder.bt_cancle_reserve.setVisibility(View.GONE);


                    } else {
                        // 继续支付
                        holder.btnpaynow.setVisibility(View.GONE);
                        // 查看预约码
                        holder.bt_reserve_sn.setVisibility(View.VISIBLE);
                        //预约年份小于当前年份
                        if (Integer.parseInt(strYear) < Integer.parseInt(strYearNow)) {
                            //取消预约
                            holder.bt_cancle_reserve.setVisibility(View.GONE);
                        }
                        //年份等于当前年份
                        else if (Integer.parseInt(strYear) == Integer.parseInt(strYearNow)) {
                            //预约月份小于当前月份
                            if (Integer.parseInt(strMouth) < Integer.parseInt(strMouthNow)) {

                                //取消预约
                                holder.bt_cancle_reserve.setVisibility(View.GONE);
                            }
                            //预约月份等于当前月份
                            else if (Integer.parseInt(strMouth) == Integer.parseInt(strMouthNow)) {
                                //预约日小于当前日
                                if (Integer.parseInt(strDay) < Integer.parseInt(strDayNow)) {
                                    //取消预约
                                    holder.bt_cancle_reserve.setVisibility(View.GONE);
                                } else {
                                    //取消预约
                                    holder.bt_cancle_reserve.setVisibility(View.VISIBLE);

                                }

                            }
                            //预约月份大于当前月份
                            else {
                                //取消预约
                                holder.bt_cancle_reserve.setVisibility(View.VISIBLE);

                            }
                        }

                        //预约年份大于当前年份
                        else {
                            //取消预约
                            holder.bt_cancle_reserve.setVisibility(View.VISIBLE);

                        }
                    }

                } else if ("partialRefunds".equals(payStatus)) {
                    holder.tvispay.setText( getString(R.string.my_health_lecture_yibufentuikuan));
                    holder.tvispay.setTextColor(getResources().getColor(
                            R.color.a999999));

                    holder.linear_residual_payment.setVisibility(View.GONE);
                    holder.tv_paid.setText( getString(R.string.my_health_lecture_tuikuanjine));
                    holder.tvmoney.setText("¥ "
                            + myHealthLecture.getOrderItem().getRefundPay());

                    // 继续支付
                    holder.btnpaynow.setVisibility(View.GONE);
                    // 查看预约码
                    holder.bt_reserve_sn.setVisibility(View.GONE);
                    // 取消预约
                    holder.bt_cancle_reserve.setVisibility(View.GONE);

                } else if ("refunded".equals(payStatus)) {
                    holder.tvispay.setText( getString(R.string.my_health_lecture_yituikuan));
                    holder.tvispay.setTextColor(getResources().getColor(
                            R.color.a999999));

                    holder.linear_residual_payment.setVisibility(View.GONE);
                    holder.tv_paid.setText( getString(R.string.my_health_lecture_tuikuanjine));
                    holder.tvmoney.setText("¥ "
                            + myHealthLecture.getOrderItem().getRefundPay());

                    // 继续支付
                    holder.btnpaynow.setVisibility(View.GONE);
                    // 查看预约码
                    holder.bt_reserve_sn.setVisibility(View.GONE);
                    // 取消预约
                    holder.bt_cancle_reserve.setVisibility(View.GONE);
                }
            }

            if (myHealthLecture.getOrderItem() == null
                    || TextUtils
                    .isEmpty(myHealthLecture.getOrderItem().getSn())) {
                holder.bt_reserve_sn.setVisibility(View.VISIBLE);
                holder.bt_cancle_reserve.setVisibility(View.GONE);
                holder.btnpaynow.setVisibility(View.GONE);

            }




            return view;
        }



    }

    class LectureHolder {

        TextView tv_paid;// 显示实际付款等文字
        LinearLayout linear_residual_payment;// 剩余付款
        TextView tv_residual_payment;// 剩余付款

        ImageView iv_health_lecture_doctor_icon_lecture;// 图片
        TextView tvdidanid;
        TextView tvispay;
        TextView tvlectureTitle;
        TextView tvlectureName;
        TextView tvlectureTime_lecture;// 讲座时间
        TextView tvlectureAddressLecture;
        TextView tvmoney;
        Button btnpaynow;
        Button bt_reserve_sn;
        Button bt_cancle_reserve;
        TextView tv_count;
        TextView text1;

    }

    // 继续支付
    class OnPayClickListener implements OnClickListener {

        private int position;
        private List<MyHealthLecture> list;
        private Context context;

        public OnPayClickListener(Context context, List<MyHealthLecture> list,
                                  int position) {
            super();
            this.list = list;
            this.position = position;
            this.context = context;
        }

        @Override
        public void onClick(View v) {

            lectureToPay = list.get(position);
            // 点击继续支付请求接口，判断当前是否已经取消预约
            if (waittingDialog == null) {
                waittingDialog = DialogUtils
                        .displayWaitDialog(MyHealthLectureActivity.this);
            }
            if (!waittingDialog.isShowing()) {
                waittingDialog.show();
            }

            lectureManager.checkIsCancled(lectureToPay.getId(),
                    ReserveType.TYPE_LECTURE);

            // HealthLecture healthLecture = myHealthLecture.getLecture();
            // Intent intent = new Intent(context,
            // HealthLecturePayInformationActivity.class);
            // intent.putExtra("orderId",
            // myHealthLecture.getOrderItem().getId());
            // intent.putExtra("healthLecture", healthLecture);
            // intent.putExtra("totalMoney",
            // myHealthLecture.getActualPayment());
            // intent.putExtra("count", myHealthLecture.getReserveNums());
            // intent.putExtra("repay", true);
            // context.startActivity(intent);
        }
    }

    class OnCheckReserveSnClickListener implements OnClickListener {

        private int position;
        private List<MyHealthLecture> list;
        private Context context;

        public OnCheckReserveSnClickListener(Context context,
                                             List<MyHealthLecture> list, int position) {
            super();
            this.list = list;
            this.position = position;
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            MyHealthLecture myHealthLecture = list.get(position);
            new ShowReserveSnDialog(context, myHealthLecture).show();
        }
    }

    // 取消预约
    class OnCancleClickListener implements OnClickListener {

        private int position;
        private List<MyHealthLecture> list;
        private Context context;

        public OnCancleClickListener(Context context,
                                     List<MyHealthLecture> list, int position) {
            super();
            this.list = list;
            this.position = position;
            this.context = context;
        }

        @Override
        public void onClick(View v) {

            // 初始化确认下载的Dialog
            downloadDialog = new ConfirmDialog(MyHealthLectureActivity.this);

            downloadDialog.setTextResourceId(R.string.cancle_yuyue);

            downloadDialog.setCancelListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    downloadDialog.dismiss();
                }
            });

            // 确认
            downloadDialog.setOkListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    MyHealthLecture myHealthLecture = list.get(position);
                    if (waittingDialog == null) {
                        waittingDialog = DialogUtils.displayWaitDialog(context);
                    }
                    if (!waittingDialog.isShowing()) {
                        waittingDialog.show();
                    }
                    lectureManager.cancle(myHealthLecture.getId(),
                            ReserveType.TYPE_LECTURE);
                }
            });

            downloadDialog.show();

        }

    }

    //继续支付
    private void continuePay() {
        // 从我的健康讲座跳转支付界面
        BroadcastUtil.BroadreserveID = ReserveType.MY_LECTURE;

        //从我的健康讲座跳转
        BroadcastUtil.sendTypeToPayResult(MyHealthLectureActivity.this,
                ReserveType.TYPE_LECTURE);


        HealthLecture healthLecture = lectureToPay.getLecture();
        Intent intent = new Intent(MyHealthLectureActivity.this,
                HealthLecturePayInformationActivity.class);
        intent.putExtra("orderId", lectureToPay.getOrderItem().getId());

        Bundle bundle = new Bundle();
        bundle.putSerializable("healthLecture", healthLecture);
        intent.putExtra("bundle", bundle);

        intent.putExtra("totalMoney", lectureToPay.getActualPayment());
        intent.putExtra("count", lectureToPay.getReserveNums());
        intent.putExtra("repay", true);


        MyHealthLectureActivity.this.startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 在退出程序的时候清楚内存缓存和硬盘缓存，这个看情况而定
        imageLoader.clearMemoryCache();
        imageLoader.clearDiskCache();
    }
}
