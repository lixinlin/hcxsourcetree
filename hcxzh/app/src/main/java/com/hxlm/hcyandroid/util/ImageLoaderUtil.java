package com.hxlm.hcyandroid.util;

import android.content.Context;
import android.widget.ImageView;
import com.hcy.ky3h.R;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

public class ImageLoaderUtil {
    private static int defultImageOnLoadingId = R.drawable.health_information_defult;
    private static int defultImageForEmptyUriId = R.drawable.health_information_defult;
    private static int defultImageOnFailId = R.drawable.health_information_defult;

    public static void displayImage(Context context, ImageView imageView,
                                    String url, Integer imageOnLoadingId, Integer imageForEmptyUriId,
                                    Integer imageOnFailId) {
        if (imageOnLoadingId != null || !imageOnLoadingId.equals("")) {
            defultImageOnLoadingId = imageOnLoadingId;
        }
        if (imageForEmptyUriId != null || !imageForEmptyUriId.equals("")) {
            defultImageForEmptyUriId = imageForEmptyUriId;
        }
        if (imageOnFailId != null || !imageOnFailId.equals("")) {
            defultImageOnFailId = imageOnFailId;
        }
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(defultImageOnLoadingId)
                .showImageForEmptyUri(defultImageForEmptyUriId)
                .showImageOnFail(defultImageOnFailId).cacheOnDisk(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .cacheInMemory(false).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                context).denyCacheImageMultipleSizesInMemory()
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .memoryCache(new LruMemoryCache(8 * 1024 * 1024))
                .diskCacheSize(50 * 1024 * 1024).threadPoolSize(3).build();

        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
        imageLoader.displayImage(url, imageView, options);
    }
}
