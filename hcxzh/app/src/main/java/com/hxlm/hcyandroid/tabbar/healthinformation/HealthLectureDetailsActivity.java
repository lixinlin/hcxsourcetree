package com.hxlm.hcyandroid.tabbar.healthinformation;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.*;
import com.hxlm.hcyandroid.bean.HealthLecture;
import com.hxlm.hcyandroid.datamanager.LectureManager;
import com.hxlm.hcyandroid.util.*;

import java.text.DecimalFormat;

public class HealthLectureDetailsActivity extends BaseActivity implements
        OnClickListener {

    private TextView tv_moey_to_pay_down;// 金额
    private TextView tv_leave_number; // 剩余数量
    private ImageView iv_plus;// 加按钮
    private TextView tv_count;// 数量
    private ImageView iv_minus;// 减按钮

    private int count = 1; // 预约数量
    private int countBeLeft; // 剩余数量
    private double price; // 单价
    private double totalMoney; // 总价
    private int reserveLimit; // 限制预约的数量

    private TextView tv_health_deatil_yuyue;
    private ProgressBar progressBar;
    private WebView wv_health_lecture_detail;
    private HealthLecture healthLecture;
    private LectureManager lectureManager;
    private Dialog waittingDialog;

    public static Activity activity;

    private DecimalFormat decimalFormat;//钱的数目，小数点保留两位

    private int orderId;//订单号

    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (waittingDialog != null && waittingDialog.isShowing()) {
                waittingDialog.dismiss();
            }
            switch (msg.what) {
                case MessageType.MESSAGE_RESERVE_FREE_LECTURE:
                    //从健康讲座跳转支付界面
                    BroadcastUtil.BroadreserveID = ReserveType.LECTURE;
                    BroadcastUtil.sendTypeToPayResult(
                            HealthLectureDetailsActivity.this,
                            ReserveType.TYPE_LECTURE);

                    if (msg.arg1 == ResponseStatus.SUCCESS) {

                        //免费讲座
                        SharedPreferenceUtil.saveString("healthlecturenum", "1");

                        Intent intent = new Intent(
                                HealthLectureDetailsActivity.this,
                                PaySuccessActivity.class);
                        startActivity(intent);
                    } else if (msg.arg1 == ResponseStatus.UNLOGIN) {
                        ToastUtil.invokeShortTimeToast(
                                HealthLectureDetailsActivity.this,
                                HealthLectureDetailsActivity.this
                                        .getString(R.string.unlogin));
                        SharedPreferenceUtil.logoff();

                        LoginControllor.requestLogin(HealthLectureDetailsActivity.this, new OnCompleteListener() {
                            @Override
                            public void onComplete() {
                                if (waittingDialog == null) {
                                    waittingDialog = DialogUtils
                                            .displayWaitDialog(HealthLectureDetailsActivity.this);
                                }
                                if (!waittingDialog.isShowing()) {
                                    waittingDialog.show();
                                }
                                lectureManager.reserveHealthLecture(
                                        healthLecture.getId() + "", count,
                                        0);
                            }
                        });

                    } else if (msg.arg1 == ResponseStatus.RESERVE_OVER_LIMIT) {
                        ToastUtil.invokeShortTimeToast(
                                HealthLectureDetailsActivity.this, "您预约的数量已超过系统限制");
                    } else {
                        ToastUtil.invokeShortTimeToast(
                                HealthLectureDetailsActivity.this, "预约失败");
                    }
                    break;
                //收费讲座
                case MessageType.MESSAGE_RESERVE_NOT_FREE_LECTURE:
                    if (msg.arg1 == ResponseStatus.SUCCESS) {
                        orderId = (Integer) msg.obj;

                        //从健康讲座跳转支付界面
                        BroadcastUtil.BroadreserveID = ReserveType.LECTURE;
                        //从健康讲座跳转
                        BroadcastUtil.sendTypeToPayResult(HealthLectureDetailsActivity.this,
                                ReserveType.TYPE_LECTURE);

                        Intent i = new Intent(HealthLectureDetailsActivity.this,
                                HealthLecturePayInformationActivity.class);

                        Bundle bundle = new Bundle();
                        bundle.putSerializable("healthLecture", healthLecture);
                        i.putExtra("bundle", bundle);

                        i.putExtra("totalMoney", totalMoney);
                        i.putExtra("count", count);
                        i.putExtra("orderId",orderId);
                        startActivity(i);

                    } else if (msg.arg1 == ResponseStatus.UNLOGIN) {
                        ToastUtil.invokeShortTimeToast(
                                HealthLectureDetailsActivity.this,
                                HealthLectureDetailsActivity.this
                                        .getString(R.string.unlogin));
                        SharedPreferenceUtil.logoff();

                        LoginControllor.requestLogin( HealthLectureDetailsActivity.this, new OnCompleteListener() {
                            @Override
                            public void onComplete() {

                                lectureManager.reserveHealthLecture(
                                        healthLecture.getId() + "", count,
                                        totalMoney);
                            }
                        });


                    } else if (msg.arg1 == ResponseStatus.RESERVE_OVER_LIMIT) {
                        ToastUtil.invokeShortTimeToast(
                                HealthLectureDetailsActivity.this,
                                "您预约的数量已超过系统限制");
                    } else {
                        ToastUtil.invokeShortTimeToast(
                                HealthLectureDetailsActivity.this, "预约失败");
                    }
                    break;
                case MessageType.MESSAGE_QUERY_REMOTE_FAILED:
                    ToastUtil.invokeShortTimeToast(
                            HealthLectureDetailsActivity.this,
                            HealthLectureDetailsActivity.this
                                    .getString(R.string.check_net));
                    break;
            }
        }
    };

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_health_lecture_details);
        activity = HealthLectureDetailsActivity.this;
    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, "健康讲座", titleBar, 1);

    }

    //初始化数据
    public void initview() {
        decimalFormat = new DecimalFormat("#.##");
        tv_moey_to_pay_down = (TextView) findViewById(R.id.tv_total_to_pay);
        tv_leave_number = (TextView) findViewById(R.id.tv_leave_number);
        iv_plus = (ImageView) findViewById(R.id.iv_plus);
        tv_count = (TextView) findViewById(R.id.tv_count);
        iv_minus = (ImageView) findViewById(R.id.iv_minus);
        tv_health_deatil_yuyue = (TextView) findViewById(R.id.tv_health_deatil_yuyue);

        wv_health_lecture_detail = (WebView) findViewById(R.id.wv_health_lecture_detail);
        progressBar=(ProgressBar)findViewById(R.id.progressbar);
//        wv_health_lecture_detail.getSettings().setJavaScriptEnabled(true);
//        wv_health_lecture_detail.getSettings().setUseWideViewPort(true);
//        wv_health_lecture_detail.getSettings().setLoadWithOverviewMode(true);
//        wv_health_lecture_detail.setWebViewClient(new WebViewClient() {
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                view.loadUrl(url);
//                return true;
//            }
//        });

        tv_count.setText(count + "");

        tv_health_deatil_yuyue.setOnClickListener(this);
        iv_plus.setOnClickListener(this);
        iv_minus.setOnClickListener(this);
    }

    @Override
    public void initDatas() {
        initview();
        lectureManager = new LectureManager(handler);
        Bundle build = getIntent().getBundleExtra("bundleweb");
        healthLecture = (HealthLecture) build.getSerializable("healthweb");
        //healthLecture = getIntent().getParcelableExtra("healthLecture");
        reserveLimit = healthLecture.getReserveLimit();
        String path = healthLecture.getPath();

        //wv_health_lecture_detail.loadUrl(path);
        //-----------------重新定义
        new WebViewUtil().setWebViewInit(wv_health_lecture_detail,progressBar,HealthLectureDetailsActivity.this,path);


        price = healthLecture.getPrice();
        totalMoney = price * count;
        tv_moey_to_pay_down.setText("预约金额：  ¥" + decimalFormat.format(totalMoney));
        countBeLeft = healthLecture.getLaveListeners();
        tv_leave_number.setText("剩余 ： " + countBeLeft + "位");

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_health_deatil_yuyue:

                LoginControllor.requestLogin(HealthLectureDetailsActivity.this, new OnCompleteListener() {
                    @Override
                    public void onComplete() {

                        if (healthLecture != null && healthLecture.getPrice() != 0) {

                            //收费讲座
                            //生成订单id
                            lectureManager.reserveHealthLecture(healthLecture.getId() + "",
                                    count, totalMoney);




                        } else if (healthLecture != null && healthLecture.getPrice() == 0) {
                            if (waittingDialog == null) {
                                waittingDialog = DialogUtils.displayWaitDialog(HealthLectureDetailsActivity.this);
                            }
                            if (!waittingDialog.isShowing()) {
                                waittingDialog.show();
                            }
                            lectureManager.reserveHealthLecture(healthLecture.getId() + "",
                                    count, 0);
                        }

                    }
                });


                break;
            case R.id.iv_plus:
                if (reserveLimit != 0) {
                    if (count < countBeLeft && count < reserveLimit) {
                        count++;
                        tv_count.setText(count + "");
                        totalMoney = price * count;
                        tv_moey_to_pay_down.setText("预约金额：  ¥" + decimalFormat.format(totalMoney));
                    }
                } else {
                    if (count < countBeLeft) {
                        count++;
                        tv_count.setText(count + "");
                        totalMoney = price * count;
                        tv_moey_to_pay_down.setText("预约金额：  ¥" + decimalFormat.format(totalMoney));
                    }
                }
                break;
            case R.id.iv_minus:
                if (count > 1) {
                    count--;
                    tv_count.setText(count + "");
                    totalMoney = price * count;
                    tv_moey_to_pay_down.setText("预约金额：  ¥" + decimalFormat.format(totalMoney));
                }
                break;
        }
    }
}
