package com.hxlm.hcyandroid.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;
import com.hxlm.hcyandroid.Constant;

import java.io.*;

public class FileUtils {
    public static String SDPATH = Environment.getExternalStorageDirectory() + "/formats/";


    //不存在目录的时候创建目录
    public static void IsDictory() {
        File fileDir = new File(SDPATH);
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }
    }

    /**
     * read file
     * from /data/data/PACKAGE_NAME/files
     */
    public static String read(Context context, String fileName) {
        try {
            FileInputStream in = context.openFileInput(fileName);
            return readInStream(in);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String readInStream(InputStream inStream) {
        try {
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[512];
            int length;
            while ((length = inStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, length);
            }

            outStream.close();
            inStream.close();
            return outStream.toString();
        } catch (IOException e) {
            if (Constant.ISSHOW)
                Log.i("FileTest", e.getMessage());
        }
        return null;
    }

    public static void deleteDir() {
        File dir = new File(SDPATH);
        if (!dir.exists() || !dir.isDirectory())
            return;

        for (File file : dir.listFiles()) {
            if (file.isFile())
                file.delete();
            else if (file.isDirectory())
                deleteDir();
        }
        dir.delete();
    }

    public static void delFile(String fileName) {
        File file = new File(SDPATH + fileName);
        if (file.isFile()) {
            file.delete();
        }
        file.exists();
    }

    public static void saveBitmap(Bitmap bm, String picName) {
        try {
            IsDictory();
//
//            if (!isFileExist("")) {
//                File tempf = createSDDir("");
//            }
            File f = new File(SDPATH, picName + ".jpeg");
            if (f.exists()) {
                f.delete();
            }
            FileOutputStream out = new FileOutputStream(f);
            bm.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}