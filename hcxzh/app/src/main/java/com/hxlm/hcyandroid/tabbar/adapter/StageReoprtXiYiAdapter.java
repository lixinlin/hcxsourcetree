package com.hxlm.hcyandroid.tabbar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

/**
 * Created by lixinlin on 2018/11/6.
 */

public class StageReoprtXiYiAdapter<T> extends BaseAdapter {
    private List<T> mList;
    private Context context;
    private LayoutInflater inflater;
    public StageReoprtXiYiAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public void setmList(List<T> mList) {
        this.mList = mList;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if(mList!=null){
            return mList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if(mList != null){
            return mList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }
}
