package com.hxlm.hcyandroid.bean;

import java.io.Serializable;
import java.util.List;

public class ReserveDoctor implements Serializable {

    public static final int PROFESSIONALLEVEL_DIRECTOR = 1; // 主任
    public static final int PROFESSIONALLEVEL_DEPUTY_DIRECTOR = 2; // 副主任
    public static final int PROFESSIONALLEVEL_DOCTOR = 3; // 医师

    private int id;
    private Hospital hospital; // 所属医院
    private String name; // 医生姓名
    private int professionalLevel; // 专家职称（1:主任医生2：副主任医生3：医师）
    private String rank;// 学术职称
    private String skill; // 专长
    private String introduction;//介绍
    private String url;// 医生介绍地址
    private String photo;
    private String pauseStatus;//该医生是否停诊 （normal 正常，stop 停诊 ）
    private HospitalSpeciality hospitalSpeciality;// // 医院-科室

    private List<DoctorPause> doctorPauses;// 医生出诊方式，根据该集合算出价格区间
    private List<DoctorSchedule> doctorSchedules;// 医生排班


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPauseStatus() {
        return pauseStatus;
    }

    public void setPauseStatus(String pauseStatus) {
        this.pauseStatus = pauseStatus;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public HospitalSpeciality getHospitalSpeciality() {
        return hospitalSpeciality;
    }

    public void setHospitalSpeciality(HospitalSpeciality hospitalSpeciality) {
        this.hospitalSpeciality = hospitalSpeciality;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public List<DoctorSchedule> getDoctorSchedules() {
        return doctorSchedules;
    }

    public void setDoctorSchedules(List<DoctorSchedule> doctorSchedules) {
        this.doctorSchedules = doctorSchedules;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Hospital getHospital() {
        return hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProfessionalLevel() {
        return professionalLevel;
    }

    public void setProfessionalLevel(int professionalLevel) {
        this.professionalLevel = professionalLevel;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public List<DoctorPause> getDoctorPauses() {
        return doctorPauses;
    }

    public void setDoctorPauses(List<DoctorPause> doctorPauses) {
        this.doctorPauses = doctorPauses;
    }

    @Override
    public String toString() {
        return "ReserveDoctor [id=" + id + ", hospital=" + hospital + ", name="
                + name + ", professionalLevel=" + professionalLevel + ", rank="
                + rank + ", skill=" + skill + ", introduction=" + introduction
                + ", url=" + url + ", photo=" + photo + ", pauseStatus="
                + pauseStatus + ", hospitalSpeciality=" + hospitalSpeciality
                + ", doctorPauses=" + doctorPauses + ", doctorSchedules="
                + doctorSchedules + "]";
    }


}
