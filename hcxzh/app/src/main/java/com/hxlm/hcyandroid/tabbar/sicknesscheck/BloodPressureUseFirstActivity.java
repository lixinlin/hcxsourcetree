package com.hxlm.hcyandroid.tabbar.sicknesscheck;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import com.hcy.ky3h.R;
import com.hcy_futejia.activity.FtjBloodPressureCheckActivity;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.bean.CheckStep;
import com.hxlm.hcyandroid.util.SharedPreferenceUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 血压首次进入界面
 *
 * @author l
 */
public class BloodPressureUseFirstActivity extends BaseActivity implements OnClickListener {

    private ListView lv_tips;
    private List<CheckStep> steps;

    private ImageView iv_now_check;//立即检测
    private ImageView iv_no_prompt;//今后不再提示


    @Override
    public void setContentView() {
        setContentView(R.layout.activity_blood_pressure_use_first);

    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.bpc_title), titleBar, 1);
        lv_tips = (ListView) findViewById(R.id.lv_tips);
        iv_now_check = (ImageView) findViewById(R.id.iv_now_check);
        iv_now_check.setOnClickListener(this);
        iv_no_prompt = (ImageView) findViewById(R.id.iv_no_prompt);
        iv_no_prompt.setOnClickListener(this);

        steps = new ArrayList<CheckStep>();
        steps.add(new CheckStep(R.drawable.check_step_number_bg1, "", getString(R.string.bpuf_tips)));
        steps.add(new CheckStep(R.drawable.check_step_number_bg2, "", getString(R.string.bpuf_tips)));
        steps.add(new CheckStep(R.drawable.check_step_number_bg3, "", getString(R.string.bpuf_tips)));
        steps.add(new CheckStep(R.drawable.check_step_number_bg4, "", getString(R.string.bpuf_tips)));
        steps.add(new CheckStep(R.drawable.check_step_number_bg5, "", getString(R.string.bpuf_tips)));
        steps.add(new CheckStep(R.drawable.check_step_number_bg6, "", getString(R.string.bpuf_tips)));

        lv_tips.setAdapter(new StepAdapter(this, steps));
    }

    @Override
    public void initDatas() {


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            //立即检测
            case R.id.iv_now_check:
                BloodPressureUseFirstActivity.this.finish();
                Intent intent = new Intent(BloodPressureUseFirstActivity.this, FtjBloodPressureCheckActivity.class);
                startActivity(intent);

                break;
            //今后不再提示
            case R.id.iv_no_prompt:
                BloodPressureUseFirstActivity.this.finish();
                SharedPreferenceUtil.saveString("BloodPressure", "0");
                Intent intent2 = new Intent(BloodPressureUseFirstActivity.this, FtjBloodPressureCheckActivity.class);
                startActivity(intent2);
                break;
            default:
                break;
        }

    }

}
