package com.hxlm.hcyandroid.util;

import android.app.Activity;

import java.util.Stack;

/**
 * 应用管理器
 * Created by JQ on 16/11/30.
 */
public class AppManager {

    private Stack<Activity> activityStack;
    private static AppManager instance;

    private AppManager() {
        if (activityStack == null) {
            activityStack = new Stack<>();
        }
    }

    public static AppManager getInstance() {
        if (instance == null) {
            instance = new AppManager();
        }
        return instance;
    }

    /**
     * 添加Activity到堆栈
     */
    public void addActivity(Activity activity) {
        activityStack.add(activity);
    }

    /**
     * 获取当前Activity（堆栈中最后一个压入的）
     */
    public Activity currentActivity() {
        if (!activityStack.isEmpty()) {
            return activityStack.lastElement();
        }
        return null;
    }

    /**
     * 结束指定的Activity
     */
    public void finishActivity(Activity activity) {
        if (activity != null) {
            activityStack.remove(activity);
            activity.finish();
        }
    }

    /**
     * 结束指定类名的Activity
     */
    public void finishActivity(Class<?> cls) {
        Stack<Activity> deleteActivityStack = new Stack<>();

        for (Activity activity : activityStack) {
            if (activity.getClass().equals(cls)) {
                deleteActivityStack.add(activity);
            }
        }

        activityStack.removeAll(deleteActivityStack);
        for (Activity activity : deleteActivityStack) {
            activity.finish();
        }
    }

    /**
     * 结束所有Activity
     */
    public void finishAllActivity() {
        for (int i = 0, size = activityStack.size(); i < size; i++) {
            if (null != activityStack.get(i)) {
                activityStack.get(i).finish();
            }
        }
        activityStack.clear();
    }

    /**
     * 退出应用程序
     */
    public void AppExit() {
        try {
            finishAllActivity();
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 应用程序是否退出
     * @return true : 应用已经退出
     */
    public boolean isAppExit() {
        return currentActivity() == null;
    }

}
