package com.hxlm.hcyandroid;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class PayTypeBroadcastReceiver extends BroadcastReceiver {

    public static int reserveType = -1;
    public static int reserveID = -1;//设置一个类型ID用于区分是从哪里跳转的

    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();
        if (action.equals(Constant.OTHER_PAY_SEND_TYPE_ACTION)) {
            reserveType = intent.getIntExtra("reserveType", -1);
            reserveID = intent.getIntExtra("reserveID", -1);
        }
        if (Constant.ISSHOW)
            Log.e("Log.e", "PayTypeBroadcastReceiver reserveType--->" + reserveType);
    }

}
