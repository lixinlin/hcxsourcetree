package com.hxlm.hcyandroid.tabbar.home_jczs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.hcy.ky3h.R;
import com.hcy_futejia.activity.FtjTuWenActivity;
import com.hcy_futejia.activity.FtjVisceraIdentityActivity;
import com.hcy_futejia.activity.FtjYueYaoActivity;
import com.hxlm.android.hcy.AbstractBaseActivity;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.content.GongFaActivity;
import com.hxlm.android.hcy.user.ChildMember;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.user.Member;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.android.hcy.voicediagnosis.BianShiActivity;
import com.hxlm.android.utils.Logger;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.tabbar.healthmall.LoginInterface;
import com.hxlm.hcyandroid.tabbar.healthmall.PayInterface;
import com.hxlm.hcyandroid.util.NetWorkUtils;
import com.hxlm.hcyphone.harmonypackage.CombingStateActivity;
import com.jiudaifu.moxademo.activity.MainActivityJDF;

import java.util.HashMap;

/**
 * Created by l on 2016/10/14.
 */
public class WebViewActivity extends AbstractBaseActivity implements View.OnClickListener {
    private final static String TAG = "WebViewActivity";
    private WebView home_wv;
    private ProgressBar progressBar;

    private String url;//url
    private String title;//标题
    private String type;//类型
    private String fangType;//处方类型
    private String advice;//处方类型

    private SharedPreferences sp;

    private int memberChildId;

    private LinearLayout linear_request_error;//网络请求错误提示页面
    private ImageView img_request_again;//重新加载
    private TitleBarView titleBar;


    //private Dialog waittingDialog;

    private boolean loadError = false;//是否加载失败
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case LoginInterface.LOGIN:
                    final String aurl = (String) msg.obj;

                    LoginControllor.requestLogin(WebViewActivity.this, new OnCompleteListener() {
                        @Override
                        public void onComplete() {
                            syncCookie(WebViewActivity.this);
                            if (Constant.isEnglish) {
                                HashMap<String, String> header = new HashMap<>();
                                header.put("language", "us-en");
                                home_wv.loadUrl(aurl, header);
                            }else {
                                home_wv.loadUrl(aurl);
                            }
                        }
                    });
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    protected void setContentView() {
        setContentView(R.layout.layout_home_webview);
        sp = getSharedPreferences("pub", Context.MODE_PRIVATE);
    }

    @Override
    protected void initViews() {

        Intent intent = WebViewActivity.this.getIntent();
        title = intent.getStringExtra("title");
        type = intent.getStringExtra("type");
        fangType = intent.getStringExtra("fangType");
        url = intent.getStringExtra("url");
        advice = intent.getStringExtra("advice");
        if (Constant.DEBUG) {
            Log.i("Log.i", "type--->" + type);
            Log.i("Log.i", "url--->" + url);
        }

        titleBar = new TitleBarView();
        home_wv = (WebView) findViewById(R.id.home_wv);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);

        progressBar.setMax(100);

        linear_request_error = (LinearLayout) findViewById(R.id.linearmy_request_error);
        img_request_again = (ImageView) findViewById(R.id.img_request_again);

        img_request_again.setOnClickListener(this);


        home_wv.addJavascriptInterface(new LoginInterface(handler), "loginInterface");

        home_wv.addJavascriptInterface(new PayInterface(WebViewActivity.this), "payInterface");

        setWebView(home_wv, WebViewActivity.this);
        LoginControllor.requestLogin(this, new OnCompleteListener() {
            @Override
            public void onComplete() {
                //先确保登录状态在访问网页
                //基本设置
                setInit();
            }
        });


    }

    //基本设置
    private void setInit() {
        //判断是否连接网络
        if (NetWorkUtils.isNetworkConnected(WebViewActivity.this)) {
            linear_request_error.setVisibility(View.GONE);
            home_wv.setVisibility(View.VISIBLE);
            loadError = false;

            //点击选择家庭成员
            titleBar.init(this, title, titleBar, 1);
            titleBar.setIv_familiesUnable();

            //默认显示
            ChildMember member = LoginControllor.getChoosedChildMember();

            if (member == null) {
                Member loginMember = LoginControllor.getLoginMember();
                if(loginMember !=null){
                    memberChildId = loginMember.getId();
                }
            } else {
                memberChildId = member.getId();
            }
            //如果类型不为空，选择字成员之后，重新加载
            if (!TextUtils.isEmpty(type)) {
                String strTypeUrlDefault = "";
                //和畅包
                if (type.contains("/member/service/home/1/")) {
                    strTypeUrlDefault = Constant.BASE_URL + type + memberChildId + ".jhtml?isnew=1";
                    if(Constant.DEBUG)
                    Log.d(TAG, "setInit: ");
                }
                //处方：sn脏腑调理
                else if (type.contains("member/service/view/fang/sn")) {
                    strTypeUrlDefault = Constant.BASE_URL + type + memberChildId
                            + ".jhtml?type=" + fangType;
                }
                //处方：JLBS：经络辨识
                else if (type.contains("/member/service/view/fang/JLBS/")) {
                    strTypeUrlDefault = Constant.BASE_URL + type + memberChildId
                            + ".jhtml?type=" + fangType;
                    Log.d(TAG, "onComplete: " + strTypeUrlDefault);
                }
                //处方：TZBS：体质辨识
                else if (type.contains("/member/service/view/fang/TZBS/")) {
                    strTypeUrlDefault = Constant.BASE_URL + type + memberChildId
                            + ".jhtml?type=" + fangType;
                }

                //报告
                else if (type.contains("/member/service/zf_report.jhtml?")) {
                    strTypeUrlDefault = Constant.BASE_URL + type
                            + "cust_id=" + memberChildId
                            + "&physique_id=" + "171204145316853673"
                            + "&device=1";
                }
                //一吃
                else if (type.contains("/member/service/zf_chufang/")) {
                    strTypeUrlDefault = Constant.BASE_URL + type
                            + memberChildId + "/1.jhtml";
                } else {
                    strTypeUrlDefault = Constant.BASE_URL + type + memberChildId + ".jhtml";
                }

                if (Constant.isEnglish) {
                    Log.e("retrofit","===English和畅包===");
                    HashMap<String, String> header = new HashMap<>();
                    header.put("language", "us-en");
                    home_wv.loadUrl(strTypeUrlDefault, header);
                }else {
                    Log.e("retrofit","===Chinese和畅包===");
                    home_wv.loadUrl(strTypeUrlDefault);
                }
//                home_wv.loadUrl(strTypeUrlDefault);


            } else {
                String stringUrl = Constant.BASE_URL + url;
//                home_wv.loadUrl(stringUrl);
                if (Constant.isEnglish) {
                    HashMap<String, String> header = new HashMap<>();
                    header.put("language", "us-en");
                    home_wv.loadUrl(stringUrl, header);
                }else {
                    home_wv.loadUrl(stringUrl);
                }
            }


        } else {
            titleBar.init(this, title, titleBar, 1);
            linear_request_error.setVisibility(View.VISIBLE);
            home_wv.setVisibility(View.GONE);
        }
    }

    @Override
    protected void initDatas() {

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN
                && keyCode == KeyEvent.KEYCODE_BACK) {
            if (home_wv.canGoBack()) {
                if (titleBar.tv_title.getText().toString().equals(getString(R.string.web_kunlun_insurance))) {
                    home_wv.goBack();
                    titleBar.tv_title.setText(getString(R.string.web_health_insurance));
                } else {
                    home_wv.goBack();
//                    titleBar.tv_title.setText(getString(R.string.main_harmony_package));
                }
            } else {
                this.finish();
            }
        }


        return true;
    }


    // 设置cookie
    public static void syncCookie(Context context) {
        try {
            CookieSyncManager.createInstance(context);
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);
            cookieManager.removeAllCookie();

            String cookieUrl = Constant.PRODUCTION_SYSTEM_COOKIE;
            if (Constant.BASE_URL.equals(Constant.TEST_SYSTEM_URL)) {
                cookieUrl = Constant.TEST_SYSTEM_COOKIE;
            }
            cookieManager.setCookie(cookieUrl, "JSESSIONID=" + HcyHttpClient.getCookie("JSESSIONID"));
            cookieManager.setCookie(cookieUrl, "token=" + HcyHttpClient.getCookie("token"));
            CookieSyncManager.getInstance().sync();

        } catch (Exception e) {
            Logger.e(TAG, e);
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    public void setWebView(final WebView webView, final Context context) {
        webView.setWebChromeClient(new WebChromeClient());
        final WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);

        syncCookie(context);

        if (Build.VERSION.SDK_INT >= 19) {
            settings.setLoadsImagesAutomatically(true);
        } else {
            settings.setLoadsImagesAutomatically(false);
        }


        webView.setWebChromeClient(new WebChromeClient() {

            //显示进度条
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);

                Logger.i("Log.i", "newProgress-->" + newProgress);
                progressBar.setProgress(newProgress);
                if (newProgress == 100) {
                    progressBar.setVisibility(View.GONE);
                }
            }


            /**
             * 当WebView加载之后，返回 HTML 页面的标题 Title
             * @param view
             * @param title
             */
            @Override
            public void onReceivedTitle(WebView view, String title) {
                super.onReceivedTitle(view, title);
                if (Constant.DEBUG) {
                    Log.i("Log.i", "网页标题title-->" + title);
                }

                //判断标题 title 中是否包含有“error”字段，如果包含“error”字段，则设置加载失败，显示加载失败的视图
                if (!TextUtils.isEmpty(title) && (title.toLowerCase().contains("error") || title.contains("页面不存在") || title.contains("找不到网页"))) {

                    loadError = true;
                }
            }
        });

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                if (waittingDialog != null && waittingDialog.isShowing()) {
//                    waittingDialog.dismiss();
//                }
                Log.i("Log.i", "shouldOverrideUrl-->" + url);
                if (Constant.DEBUG) {
                    Log.i("Log.i", "shouldOverrideUrl-->" + url);
                }

                //判断是否含有拨打电话
                if (!TextUtils.isEmpty(url) && url.startsWith("tel:")) {

                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_DIAL);//表示打开拨打电话窗口，但还未拨出电话
                    intent.setData(Uri.parse(url));
                    startActivity(intent);
                    return true;// 返回: return true;webview处理url是根据程序来执行的。

                }
                //检测跳转，根据不同的拦截指令，跳转不同的检测界面
                else if (url.contains(DetectionJump.DETECTIOB_JUMP_URL)||url.contains(DetectionJump.DETECTIOB_JUMP_URL2)) {
                    Intent intent = null;
                    //截取斜杠后的字符串
                    int inedx = url.lastIndexOf("/");
                    String strJump = url.substring(inedx + 1);
                    //判断属于那种跳转
                    //慢病检测
                    if (DetectionJump.MAN_BING.equals(strJump)) {

                    }
                    //图文咨询
                    else if (DetectionJump.TU_WEN.equals(strJump)) {
                        LoginControllor.requestLogin(context, new OnCompleteListener() {
                            @Override
                            public void onComplete() {
                                Intent intent = new Intent(context,
                                        FtjTuWenActivity.class);
                                SharedPreferences.Editor editor = sp.edit();
                                editor.putBoolean("pubsort", true);
                                editor.commit();
                                startActivity(intent);

                            }
                        });
                    }
                    //耳穴处方
                    else if (DetectionJump.ER_XUE.equals(strJump)) {
                        intent = new Intent(context, FtjYueYaoActivity.class);
                        intent.putExtra("title", getString(R.string.web_leluoyi));
                        startActivity(intent);
                    }
                    //音乐处方
                    else if (DetectionJump.YIN_YUE.equals(strJump)) {
                        intent = new Intent(context, FtjYueYaoActivity.class);
                        intent.putExtra("title", getString(R.string.web_leyao));
                        startActivity(intent);
                    }
                    //进入运动
                    else if (DetectionJump.YUN_DONG.equals(strJump)) {
                        intent = new Intent(context, GongFaActivity.class);
                        intent.putExtra("advice",advice);
                        startActivity(intent);
                    }
                    //体质辨识
                    else if (DetectionJump.TI_ZHI.equals(strJump)) {
                        intent = new Intent(context, OneClockActivity.class);
                        startActivity(intent);
                    }
                    //闻音辨识
                    else if (DetectionJump.WEN_YIN.equals(strJump)) {
                        intent = new Intent(context, BianShiActivity.class);
                        startActivity(intent);

                    }
                    //脏腑辨识
                    else if (DetectionJump.ZANG_FU.equals(strJump)) {
                        intent = new Intent(context, FtjVisceraIdentityActivity.class);
                        startActivity(intent);
                    }
                    //干预方案
                    else if (DetectionJump.GAN_YU_FANG_AN.equals(strJump)) {
                        intent = new Intent(context, CombingStateActivity.class);
                        intent.putExtra("OneSit",strJump);
                        startActivity(intent);
                        return true;
                    }
                    //干预
                    else if (DetectionJump.GAN_YU.equals(strJump)){
                        intent = new Intent(context, CombingStateActivity.class);
                        intent.putExtra("OneSit",strJump);
                        startActivity(intent);
                        return true;
                    }
                    //进入灸疗
                    else if (DetectionJump.JIU_LIAO.equals(strJump)) {
                        intent = new Intent(context, MainActivityJDF.class);
                        ChildMember choosedChildMember = LoginControllor.getChoosedChildMember();
                        if (choosedChildMember != null) {
                            intent.putExtra("userid", choosedChildMember.getId());
                        }
                        startActivity(intent);
                    }


                    WebViewActivity.this.finish();
                    return true;
                } else {
                    //不拦截H5的跳转行为，仅修改title
                    Log.i("url", url);
                    if (url.contains("JLBS")) {
                        title = getString(R.string.web_jingluo_report);
                    } else if (url.contains("zf_report")) {
                        title = getString(R.string.web_zangfu_report);
                    } else if (url.contains("tiyan/TZBS")) {
                        title = getString(R.string.web_medical_report);
                    } else if (url.contains("yibao/TZBS")) {
                        title = getString(R.string.web_health_insurance);
                    } else if (url.contains("mall.kunlunhealth.com/")) {
                        title = getString(R.string.web_kunlun_insurance);
                    }
                    titleBar.init(WebViewActivity.this, title, titleBar, 1);
                    titleBar.linear_back.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (home_wv.canGoBack()) {
                                if (getString(R.string.web_kunlun_insurance).equals(titleBar.tv_title.getText().toString())) {
                                    home_wv.goBack();
                                    titleBar.tv_title.setText(getString(R.string.web_health_insurance));
                                } else {
                                    home_wv.goBack();
//                                    titleBar.tv_title.setText(getString(R.string.main_harmony_package));
                                }
                            } else {
                                WebViewActivity.this.finish();
                            }
                        }
                    });
//                    webView.loadUrl(url);
                    if (Constant.isEnglish) {
                        HashMap<String, String> header = new HashMap<>();
                        header.put("language", "us-en");
                        webView.loadUrl(url, header);
                    }else {
                        webView.loadUrl(url);
                    }
                    titleBar.setIv_familiesUnable();
                }


                /**
                 * 这里主要说它的返回值的问题：
                 1、 默认返回：return super.shouldOverrideUrlLoading(view, url); 这个返回的方法会调用父类方法，也就是跳转至手机浏览器，平时写webview一般都在方法里面写 webView.loadUrl(url);  然后把这个返回值改成下面的false。
                 2、返回: return true;  webview处理url是根据程序来执行的。
                 3、返回: return false; webview处理url是在webview内部执行。
                 */

                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
//                if (waittingDialog == null) {
//                    waittingDialog = DialogUtils.displayWaitDialog(WebViewActivity.this);
//                }
//                if (waittingDialog != null && !waittingDialog.isShowing()) {
//                    waittingDialog.show();
//                }
                super.onPageStarted(view, url, favicon);
                progressBar.setVisibility(View.VISIBLE);//显示progressbar
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                progressBar.setVisibility(View.GONE);
                Logger.i("Log.i", "onPageFinished-->" + url);
                //判断是否加载错误
                if (loadError == true) {
                    titleBar.init(WebViewActivity.this, title, titleBar, 1);

                    linear_request_error.setVisibility(View.VISIBLE);
                    home_wv.setVisibility(View.GONE);
                } else {
                    linear_request_error.setVisibility(View.GONE);
                    home_wv.setVisibility(View.VISIBLE);
                    loadError = false;
                }


                if (!settings.getLoadsImagesAutomatically()) {
                    settings.setLoadsImagesAutomatically(true);
                }
//                if (waittingDialog != null && waittingDialog.isShowing()) {
//                    waittingDialog.dismiss();
//                }
                super.onPageFinished(view, url);
            }

            /**
             * 页面加载错误时执行的方法，但是在6.0以下，有时候会不执行这个方法
             * @param view
             * @param errorCode
             * @param description
             * @param failingUrl
             */
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);

                loadError = true;
                if (Constant.DEBUG) {
                    Log.i("Log.i", "onReceivedError-->errorCode-->" + errorCode);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            //重新加载
            case R.id.img_request_again:
                //基本设置
                setInit();
                break;
            default:
                break;
        }
    }

}