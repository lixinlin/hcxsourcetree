package com.hxlm.hcyphone.manager;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.asynchttp.HcyHttpResponseHandler;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.user.SharedPreferenceTempSave;
import com.hxlm.hcyandroid.bean.Task;
import com.hxlm.hcyandroid.datamanager.BaseManager;
import com.loopj.android.http.RequestParams;

import net.sourceforge.simcpux.Constants;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by lipengcheng on 2017/12/4.
 */

public class TaskManager extends BaseManager {
    Handler handler;
    private List<Task> todo_List;
    public static final int TASK_MESSAGE = 1;

    public TaskManager(Handler hanlder) {
        super(hanlder);
        this.handler = hanlder;
    }

    public TaskManager() {
    }

    public void getUserTask(Context context, int memberChildId, AbstractDefaultHttpHandlerCallback callback) {
        String url = "/member/new_ins/tips.jhtml";
        RequestParams params = new RequestParams();
        params.put("memberChildId", memberChildId);
        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {

            @Override
            public Object contentParse(String content) {
                Task task_zfbs;
                Task task_jlbs;
                Task task_tzbs;
                if (content != null && !content.equals("")) {

                    try {
                        com.alibaba.fastjson.JSONObject data = JSON.parseObject(content);
//
                        //脏腑辨识
                        com.alibaba.fastjson.JSONObject zfbs = data.getJSONObject("zfbs");
                        if (zfbs == null) {
                            task_zfbs = new Task();
                            task_zfbs.setModifyDate(111);
                        } else {
                            task_zfbs = JSON.parseObject(zfbs.toJSONString(), Task.class);
                        }
                        task_zfbs.setType("zfbs");
                        //经络辨识
                        com.alibaba.fastjson.JSONObject jlbs = data.getJSONObject("jlbs");
                        if (jlbs == null) {
                            task_jlbs = new Task();
                            task_jlbs.setModifyDate(111);
                        } else {
                            task_jlbs = JSON.parseObject(jlbs.toJSONString(), Task.class);
                        }

                        task_jlbs.setType("jlbs");
                        //体质辨识
                        com.alibaba.fastjson.JSONObject tzbs = data.getJSONObject("tzbs");
                        if (tzbs == null) {
                            task_tzbs = new Task();
                            task_tzbs.setModifyDate(111);
                        } else {
                            task_tzbs = JSON.parseObject(tzbs.toJSONString(), Task.class);
                        }
                        task_tzbs.setType("tzbs");

                        com.alibaba.fastjson.JSONArray todolist = data.getJSONArray("todolist");
                        todo_List = new ArrayList<>();
                        todo_List = JSON.parseArray(todolist.toJSONString(), Task.class);

                        if (todo_List == null || todo_List.size() == 0) {
                            todo_List = new ArrayList<>();
                            if (task_jlbs.getModifyDate() == 111) {
                                todo_List.add(task_jlbs);
                            }
                            if (task_tzbs.getModifyDate() == 111) {
                                todo_List.add(task_tzbs);
                            }
                            if (task_zfbs.getModifyDate() == 111) {
                                todo_List.add(task_zfbs);
                            }
                        }
                        return todo_List;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return null;
            }
        }, LoginControllor.MEMBER, callback.getContext());


    }

    public void getUserTask_ftj( int memberChildId, AbstractDefaultHttpHandlerCallback callback) {
        String url = "/member/new_ins/newTips.jhtml";
        RequestParams params = new RequestParams();
        params.put("memberChildId", memberChildId);
        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            public Object contentParse(String content) {

                if (content != null && !content.equals("")) {
                    try {
                        com.alibaba.fastjson.JSONObject data = JSON.parseObject(content);
                        com.alibaba.fastjson.JSONArray todolist = data.getJSONArray("todolist");
                        todo_List = new ArrayList<>();
                        todo_List = JSON.parseArray(todolist.toJSONString(), Task.class);

                        if (todo_List == null || todo_List.size() == 0) {
                            todo_List = new ArrayList<>();
                        }
                        return todo_List;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return null;
            }
        }, LoginControllor.MEMBER, callback.getContext());


    }

    /**
     * 完成任务后提交完成状态
     * @param context
     * @param memberChildId
     * @param confId  运动id
     * @param callback
     */
    public void submitTaskCompleted(Context context,int memberChildId,int confId , AbstractDefaultHttpHandlerCallback callback){
        String url = "/member/new_ins/addExerciseInfo.jhtml";
        RequestParams params = new RequestParams();
        params.put("memberChildId", memberChildId);
        params.put("confId", confId);
        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {

            @Override
            protected Object contentParse(String content) {
                Log.d("http:", "contentParse: "+content);
                return content;

            }
        } ,LoginControllor.MEMBER, callback.getContext());

    }


}
