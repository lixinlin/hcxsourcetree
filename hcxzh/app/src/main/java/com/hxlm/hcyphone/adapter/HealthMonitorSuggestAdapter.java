package com.hxlm.hcyphone.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.hcy.ky3h.R;
import com.hxlm.hcyphone.widget.ViewHolder;

import java.util.List;

/**
 * Created by lixinlin on 2017/6/20.
 */
public class HealthMonitorSuggestAdapter extends BaseAdapter {
    private Context mContext;
    private List<String> mDatas;

    public HealthMonitorSuggestAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setmDatas(List<String> mDatas) {
        this.mDatas = mDatas;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (mDatas != null) {
            return mDatas.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (mDatas != null) {
            return mDatas.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (mDatas != null) {
            return position;
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_body_monitor_suggest, null);
        }
        ImageView ivIcon = ViewHolder.get(convertView, R.id.iv_item_image);
        TextView tvName = ViewHolder.get(convertView, R.id.tv_item_name);
        TextView tvFrequency = ViewHolder.get(convertView, R.id.tv_item_frequency);
        String monitorName = mDatas.get(position);
        switch (monitorName) {
            case "XL":
                ivIcon.setImageResource(R.drawable.heart_rate);
                tvName.setText("心率监测");
                tvFrequency.setText("一周一次");
                break;
            case "XYa":
                ivIcon.setImageResource(R.drawable.blood_pressure);
                tvName.setText("血压监测");
                tvFrequency.setText("一周一次");
                break;
            case "XT":
                ivIcon.setImageResource(R.drawable.blood_glucose);
                tvName.setText("血糖监测");
                tvFrequency.setText("一周一次");
                break;
            case "XYang":
                ivIcon.setImageResource(R.drawable.blood_oxygen);
                tvName.setText("血氧监测");
                tvFrequency.setText("一周一次");
                break;
            case "HX":
                ivIcon.setImageResource(R.drawable.breathe);
                tvName.setText("呼吸监测");
                tvFrequency.setText("一周一次");
                break;
            case "TW":
                ivIcon.setImageResource(R.drawable.thermometer);
                tvName.setText("体温监测");
                tvFrequency.setText("一周一次");
                break;

        }
        return convertView;
    }
}
