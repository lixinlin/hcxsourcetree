package com.hxlm.hcyphone.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.hcy.ky3h.R;
import com.hxlm.hcyphone.widget.ViewHolder;

import java.util.List;

/**
 * Created by lixinlin on 2017/6/19.
 */
public class HealthDetectionSuggestAdapter extends BaseAdapter {
    private Context mContext;
    private List<String> mDatas;


    public HealthDetectionSuggestAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setmDatas(List<String> mDatas) {
        this.mDatas = mDatas;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (mDatas != null) {
            return mDatas.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (mDatas != null) {
            return mDatas.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        if (mDatas != null) {
            return position;
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_health_detection_suggest, null);
        }
        TextView tvName = ViewHolder.get(convertView, R.id.tv_item_name);
        String detectionName = mDatas.get(position);
        tvName.setText(detectionName);
        return convertView;
    }
}
