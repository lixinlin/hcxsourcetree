package com.hxlm.hcyphone.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hxlm.hcyandroid.bean.HealthInformation;
import com.hxlm.hcyphone.utils.GlideApp;


import java.text.SimpleDateFormat;
import java.util.List;

/**
 * @author Lirong
 * @date 2019/1/7.
 * @description
 */

public class HealthCultureAdapter extends RecyclerView.Adapter<HealthCultureAdapter.HealthCultureViewHolder>{

    private Context context;
    private List<HealthInformation> list;

    public HealthCultureAdapter(Context context, List<HealthInformation> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public HealthCultureViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.health_culture_item, parent, false);
        return new HealthCultureViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull HealthCultureViewHolder holder, int position) {
        HealthInformation healthInformation = list.get(position);
        holder.tv_information_title.setText(healthInformation.getTitle());
        String desc = healthInformation.getSeoDescription();
        if (!TextUtils.isEmpty(desc)) {
            holder.tv_information_desc.setText(desc);
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        holder.tv_information_date.setText(format.format(healthInformation
                .getCreateDate()));
        if (context != null) {
            GlideApp.with(context)
                    .load(healthInformation.getPicture())
                    .placeholder(R.drawable.health_information_defult)
                    .error(R.drawable.health_information_defult)
                    .fallback(R.drawable.health_information_defult)
                    .into(holder.iv_information_icon);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick.onItem(holder.getPosition(),list.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    private OnItemClick onItemClick;

    public void setOnItemClick(OnItemClick onItemClick) {
        this.onItemClick = onItemClick;
    }

    public interface OnItemClick{
        void onItem(int position,HealthInformation healthInformation);
    }

    class HealthCultureViewHolder extends RecyclerView.ViewHolder{

        private final ImageView iv_information_icon;
        private final TextView tv_information_title;
        private final TextView tv_information_date;
        private final TextView tv_information_desc;

        public HealthCultureViewHolder(View itemView) {
            super(itemView);
            iv_information_icon = itemView.findViewById(R.id.iv_information_icon);
            tv_information_title = itemView.findViewById(R.id.tv_information_title);
            tv_information_date = itemView.findViewById(R.id.tv_information_date);
            tv_information_desc = itemView.findViewById(R.id.tv_information_desc);
        }
    }

}
