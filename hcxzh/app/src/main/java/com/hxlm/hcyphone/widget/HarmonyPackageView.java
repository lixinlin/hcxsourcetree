package com.hxlm.hcyphone.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.hcy.ky3h.R;
import com.hxlm.hcyandroid.util.DensityUtil;

/**
 * 仪表盘
 * Created by JQ on 2017/4/19.
 */

public class HarmonyPackageView extends View {

    //画笔
    private Paint paintBackground;
    private Paint paintText;
    private RectF stateRectF;

    private int START_ARC = 132;
    private int DURING_ARC = 276;
    private int OFFSET = 15;
    private float mCenterX = 0;
    private float mCenterY = 0;
    private Paint paintProgressBackground;
    private int mStateColor;
    private String mStateText;

    private int mStateNum;
    float percent;

    private Context mContext;
    private int mWidth, mHight;

    float oldPercent = 0f;

    public HarmonyPackageView(Context context) {
        this(context, null);
        init(context);
    }

    public HarmonyPackageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        init(context);
    }

    public HarmonyPackageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        initAttr();
        initPaint();
    }

    private void initPaint() {

        //外圈背景画笔
        paintBackground = new Paint();
        paintBackground.setAntiAlias(true);
        paintBackground.setStyle(Paint.Style.FILL);
        paintBackground.setStrokeWidth(2);
        paintBackground.setColor(getResources().getColor(R.color.white));
        paintBackground.setDither(true);

        //速度条背景画笔
        paintProgressBackground = new Paint();
        paintProgressBackground.setAntiAlias(true);
        paintProgressBackground.setStrokeWidth(DensityUtil.dip2px(mContext, 6));
        paintProgressBackground.setStyle(Paint.Style.STROKE);
        paintProgressBackground.setStrokeCap(Paint.Cap.ROUND);
        paintProgressBackground.setColor(getResources().getColor(R.color.color_61D179));
        paintProgressBackground.setDither(true);

        //状态文字画笔
        paintText = new Paint();
        paintText.setAntiAlias(true);
        paintText.setStrokeWidth(1);
        paintText.setStyle(Paint.Style.FILL);//实心画笔
        paintText.setDither(true);
        paintText.setColor(getResources().getColor(R.color.color_61D179));
        paintText.setTextSize(DensityUtil.dip2px(mContext, 22));

    }

    private void initShader() {
        updateOval();
    }

    private void initAttr() {
        mCenterX = mWidth / 2;
        mCenterY = mHight / 2;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = getWidth();
        mHight = getHeight();
        initShader();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int realWidth = startMeasure(widthMeasureSpec);
        int realHeight = startMeasure(heightMeasureSpec);

        setMeasuredDimension(realWidth, realHeight);
    }


    private int startMeasure(int msSpec) {
        int result = 0;
        int mode = MeasureSpec.getMode(msSpec);
        int size = MeasureSpec.getSize(msSpec);
        if (mode == MeasureSpec.EXACTLY) {
            result = size;
        } else {
            result = (int) DensityUtil.dip2px(mContext, 100);
        }
        return result;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.percent = percent / 100f;

        canvas.translate(mWidth / 2, mHight / 2);//移动坐标原点到中心

        //绘制健康状态粗圆弧
        drawProgress(canvas, percent);
        //绘制健康状态Text
        drawPowerText(canvas);
    }

    /**
     * 绘制进度条
     *
     * @param canvas
     * @param percent
     */
    private void drawProgress(Canvas canvas, float percent) {
        paintProgressBackground.setColor(mStateColor);
        canvas.drawArc(stateRectF, START_ARC, DURING_ARC, false, paintProgressBackground);
        if (percent > 1.0f) {
            percent = 1.0f; //限制进度条在弹性的作用下不会超出
        }
        if (!(percent <= 0.0f)) {
            canvas.drawArc(stateRectF, START_ARC, percent * DURING_ARC, false, paintProgressBackground);
        }
    }

    /**
     * 绘制状态text值
     *
     * @param canvas
     */
    public void drawPowerText(Canvas canvas) {
        paintText.setColor(mStateColor);

        if (mStateText == null) {
            mStateText = "健康";
        }
        int measureText = (int) paintText.measureText(mStateText);

        //绘制中间文字
        canvas.drawText(mStateText, mCenterX - measureText / 2, mCenterY + DensityUtil.dip2px(mContext, 10), paintText);
    }

    /**
     * 根据计算显示颜色值
     */
    public void stateColor() {
        if (mStateNum == 1) {
            mStateColor = mContext.getResources().getColor(R.color.color_61D179);
            mStateText = "健康";
        } else if (mStateNum == 2) {
            mStateColor = mContext.getResources().getColor(R.color.color_D1D465);
            mStateText = "轻度";
        } else if (mStateNum == 3) {
            mStateColor = mContext.getResources().getColor(R.color.color_DDB15E);
            mStateText = "重度";
        } else if (mStateNum == 4) {
            mStateColor = mContext.getResources().getColor(R.color.color_EB8D31);
            mStateText = "轻度";
        } else if (mStateNum == 5) {
            mStateColor = mContext.getResources().getColor(R.color.color_DA5A2F);
            mStateText = "中度";
        } else if (mStateNum == 6) {
            mStateColor = mContext.getResources().getColor(R.color.color_B63E3E);
            mStateText = "重度";
        } else {
            mStateColor = mContext.getResources().getColor(R.color.color_BFBFBF);
            mStateText = "请登录";
        }
    }

    private void updateOval() {

        stateRectF = new RectF((-mWidth / 2) + OFFSET + getPaddingLeft(), getPaddingTop() - (mHight / 2) + OFFSET,
                (mWidth / 2) - getPaddingRight() - OFFSET,
                (mWidth / 2) - getPaddingBottom() - OFFSET);
    }

    //每次view被隐藏在显示出来之后percent都会变成一个0.000000x的值，原因未知，暂时采取暴力方式处理。
    @Override
    protected void onVisibilityChanged(View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);
        percent = oldPercent;
    }

    /**
     * 设置状态值
     *
     * @param percent
     */
    public void setPercent(int percent) {
        this.mStateNum = percent;
        stateColor();
        invalidate();
    }
}
