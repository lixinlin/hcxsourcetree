package com.hxlm.hcyphone.okhttp;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.hxlm.android.utils.Logger;
import com.hxlm.hcyandroid.BaseApplication;
import com.hxlm.hcyandroid.Constant;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * http请求
 * Created by JQ on 2017/7/21.
 */
public class HttpClient {

    private OkHttpClient mOkHttpClient;
    private static HttpClient mHttpClient;

    private final static int TIMEOUT_CONNECTION = 20;// 超时连接时间
    private final static int TIMEOUT_READ = 30; // 读超时
    private final static int TIMEOUT_WRITE = 10;// 写超时

    private final static String MEDIA_JSON = "application/json";
    private String LANGUAGE ;
    private static final MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json; charset=utf-8");

    private HttpClient() {
        if (mOkHttpClient == null) {
            mOkHttpClient = getOkHttpClient();
        }
    }

    public static HttpClient getInstance() {
        if (mHttpClient == null) {
            mHttpClient = new HttpClient();
        }
        return mHttpClient;
    }

    private OkHttpClient getOkHttpClient() {
//        InputStream inputStream = null;
//        try {
//            inputStream = SimbaApplication.getContext().getAssets().open("simbaqa.51biaoshi.com.cer");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT_CONNECTION, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT_READ, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT_WRITE, TimeUnit.SECONDS);

//        if (null != inputStream) {
//            HttpsHelper.SSLParams sslParams = HttpsHelper.getSslSocketFactory(null, null, inputStream);
//            builder.sslSocketFactory(sslParams.sSLSocketFactory, sslParams.trustManager);
//        }

        return builder.build();
    }

    /**
     * http get 请求
     *
     * @param url      url
     * @param params   请求参数
     * @param callback 请求响应回调
     */
    public void httpGet(String url, Map<String, Object> params, Callback callback) {
        if (null == params) {
            params = new HashMap<>();
        }
        url = processUrl(url, params);
        if (Constant.isEnglish) {
            LANGUAGE = "us-en";
        }else {
            LANGUAGE = "";
        }
        Request request = new Request.Builder()
                .url(url)
                .get()
                .header("language", LANGUAGE)
                .header("Accept", MEDIA_JSON)
                .addHeader("Content-Type", MEDIA_JSON)
                .addHeader("version", Constant.PRODUCTION_SYSTEM_HEADER + getVersionCode())
                .build();

        mOkHttpClient.newCall(request).enqueue(callback);
    }

    /**
     * http post 请求
     *
     * @param url      url
     * @param params   请求参数
     * @param callback 请求响应回调
     */
    public void httpPost(String url, Map<String, Object> params, Callback callback) {
        if (null == params) {
            params = new HashMap<>();
        }
        if (Constant.isEnglish) {
            LANGUAGE = "us-en";
        }else {
            LANGUAGE = "";
        }
        String strParams = JSON.toJSONString(params);
        Request request = new Request.Builder()
                .url(url)
                .header("language", LANGUAGE)
                .post(RequestBody.create(MEDIA_TYPE_JSON, strParams))
                .header("Accept", MEDIA_JSON)
                .addHeader("Content-Type", MEDIA_JSON)
                .addHeader("version", Constant.PRODUCTION_SYSTEM_HEADER + getVersionCode())
                .build();
        mOkHttpClient.newCall(request).enqueue(callback);
    }

    /**
     * 文件上传
     */
    public void httpPostFiles(String url, List<File> files, String mediaType, Callback callback) {
        MultipartBody.Builder multipartBuilder = new MultipartBody.Builder();
        multipartBuilder.setType(MultipartBody.FORM);
        if (files != null) {
            for (File file : files) {
                if (file.exists()) {
                    RequestBody fileBody = RequestBody.create(MediaType.parse(mediaType), file);
                    multipartBuilder.addFormDataPart(file.getName(), file.getName(), fileBody);
                }
            }
        }

        RequestBody requestBody = multipartBuilder.build();
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();

        mOkHttpClient.newCall(request).enqueue(callback);
    }

    /**
     * 加工url
     *
     * @param url    url
     * @param params 请求参数
     * @return 带请求参数的url
     */
    private String processUrl(String url, Map<String, Object> params) {
        StringBuilder sb = new StringBuilder(url);
        if (sb.indexOf("?") < 0) {
            sb.append('?');
        }

        if (params != null) {
            for (String key : params.keySet()) {
                sb.append('&');
                sb.append(key);
                sb.append('=');
                sb.append(params.get(key));
            }
        }
        return sb.toString().replace("?&", "?");
    }

    //得到当前版本号
    private static int getVersionCode() {
        Context context = BaseApplication.getContext();
        int versionCode = 0;
        String packageName = context.getPackageName();
        try {
            versionCode = context.getPackageManager().getPackageInfo(packageName, 0).versionCode;
            if (Constant.DEBUG) {
                Log.i("Log.i", "versionCode--->" + versionCode);
            }

        } catch (PackageManager.NameNotFoundException e) {
            Logger.i("hcy", e.getMessage());
        }
        return versionCode;
    }

}
