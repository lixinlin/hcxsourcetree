package com.hxlm.hcyphone.okhttp;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.UnknownHostException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * http请求响应回调处理
 * Created by JQ on 2017/7/21.
 */
public abstract class HttpCallbackHandle<T> implements Callback {

    private Handler handler;
    private Class<T> tClass;

    private final static int SUCCESS = 0;// 成功标识
    private final static int FAILURE = 1;// 失败标识

    @SuppressWarnings("unchecked")
    public HttpCallbackHandle() {
        handler = new ResponseHandle(this);

        Type t = getClass().getGenericSuperclass();
        if (t instanceof ParameterizedType) {
            Type[] p = ((ParameterizedType) t).getActualTypeArguments();
            tClass = (Class<T>) p[0];
        }
    }

    @Override
    public void onResponse(Call call, Response response) throws IOException {
        if (response.isSuccessful()) {
            String respResult = response.body().string();

            if (!TextUtils.isEmpty(respResult)) {

                Log.e("输出的结果为：", respResult);

                JSONObject cr = JSON.parseObject(respResult);

                if (cr != null) {
                    if (cr.containsKey(HttpCallbackCode.ERROR_CODE) && !"0".equals(cr.get(HttpCallbackCode.ERROR_CODE))) {// 有错误日志信息

                        sendFailMessage(FAILURE, (String) cr.get(HttpCallbackCode.ERROR_MSG));

                    } else {// 正常
                        Object obj = JSON.parseObject(respResult, tClass);
                        handler.sendMessage(obtainMessage(SUCCESS, obj));
                    }
                }

                //Gson解析
//                JsonParser jsonParser = new JsonParser();
//                JsonElement jsonElement = jsonParser.parse(respResult);
//                JsonObject jsonObject = jsonElement.getAsJsonObject();
//                if (jsonObject.has("errorCode")) {
//                    int errorCode = jsonObject.get("errorCode").getAsInt();
//                    String errorMessage = jsonObject.get("errorMsg").getAsString();
//
//                    if (0 != errorCode) {
//                        sendFailMessage(errorCode, errorMessage);
//                    } else {
//                        sendSuccessMessage(respResult);
//                    }
//                } else {
//                    sendSuccessMessage(respResult);
//                }

            } else {
                sendFailMessage(500, "服务器开小差了");
            }
        } else {
            int code = response.code();
            sendFailMessage(code, "服务器开小差了");
        }
    }

    private void sendSuccessMessage(String respResult) {
        Gson gson = new Gson();
        T t = gson.fromJson(respResult, tClass);

//        Object obj = JSON.parseObject(respResult, tClass);
        handler.sendMessage(obtainMessage(SUCCESS, t));
    }

    private void sendFailMessage(int errorCode, String errorMessage) {
        CommonResponse commonResponse = new CommonResponse();
        commonResponse.setErrorCode(errorCode + "");
        commonResponse.setErrorMsg(errorMessage);

        handler.sendMessage(obtainMessage(FAILURE, commonResponse));
    }

    @Override
    public void onFailure(Call call, IOException e) {
        String errorMessage = networkInfo(e);
        CommonResponse commonResponse = new CommonResponse();
        commonResponse.setErrorMsg(errorMessage);

        handler.sendMessage(obtainMessage(FAILURE, commonResponse));
    }

    /**
     * 响应成功(UI Thread)
     *
     * @param obj 数据对象
     */
    public abstract void onSuccess(T obj);

    /**
     * 响应失败
     *
     * @param errorCode    错误码
     * @param errorMessage 出错信息
     */
    public abstract void onFailure(String errorCode, String errorMessage);

    /**
     * 获取网络错误信息
     */
    private static String networkInfo(Exception e) {
        String info = "网络异常，请求超时";
        if (e instanceof UnknownHostException || e instanceof ConnectException) {
            return "网络连接失败，请检查网络设置";
        } else if (e instanceof SocketException) {
            return "网络异常，读取数据超时";
        }
        return info;
    }

    private static class ResponseHandle extends Handler {

        private WeakReference<HttpCallbackHandle> weak = null;

        ResponseHandle(HttpCallbackHandle callback) {
            weak = new WeakReference<>(callback);
        }

        @Override
        public void handleMessage(Message msg) {
            HttpCallbackHandle callback = weak.get();
            if (callback != null) {
                callback.dealResponse(msg);
            }
        }

    }

    private Message obtainMessage(int messageId, Object data) {
        return Message.obtain(handler, messageId, data);
    }

    /**
     * 响应成功失败消息处理
     */
    @SuppressWarnings("unchecked")
    private void dealResponse(Message msg) {
        int flag = msg.what;

        if (flag == SUCCESS) {
            onSuccess((T) msg.obj);
        } else if (flag == FAILURE) {
            CommonResponse commonResponse = (CommonResponse) msg.obj;
            onFailure(commonResponse.getErrorCode(), commonResponse.getErrorMsg());
        }
    }

}
