package com.hxlm.hcyphone.harmonypackage;

import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.report.RecordManager;
import com.hxlm.android.hcy.user.ChildMember;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.utils.DeviceUtils;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.bean.RecordIndexData;
import com.hxlm.hcyandroid.tabbar.home.vitalsign.BreathActivity;
import com.hxlm.hcyandroid.tabbar.home.vitalsign.BreathUseFirstActivity;
import com.hxlm.hcyandroid.tabbar.home.vitalsign.TemperatureDetectionWriteActivity;
import com.hxlm.hcyandroid.tabbar.sicknesscheck.BloodPressureCheckActivity;
import com.hxlm.hcyandroid.tabbar.sicknesscheck.BloodPressureUseFirstActivity;
import com.hxlm.hcyandroid.tabbar.sicknesscheck.BloodSugarCheckActivity;
import com.hxlm.hcyandroid.tabbar.sicknesscheck.BloodSugarUseFirstActivity;
import com.hxlm.hcyandroid.tabbar.sicknesscheck.ECGUseFirstActivity;
import com.hxlm.hcyandroid.tabbar.sicknesscheck.SPO2HCheckActivity;
import com.hxlm.hcyandroid.tabbar.sicknesscheck.SPO2HUseFirstActivity;
import com.hxlm.hcyandroid.tabbar.sicknesscheckecg.ECGCheckActivity;
import com.hxlm.hcyandroid.util.SharedPreferenceUtil;
import com.hxlm.hcyphone.adapter.HealthDetectionSuggestAdapter;
import com.hxlm.hcyphone.adapter.HealthMonitorSuggestAdapter;
import com.hxlm.hcyphone.manager.MeasureInfoManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * 一测
 * Created by JQ on 2017/6/19.
 */
public class MeasureInfoActivity extends BaseActivity implements OnCompleteListener {
    private static final String TAG = "MeasureInfoActivity";
    private TextView tvHeartRateDetectionDate;
    private TextView tvHeartRateDetectionValue;
    private TextView tvBloodPressureDetectionDate;
    private TextView tvBloodPressureDetectionValue;
    private TextView tvBloodGlucoseDetectionDate;
    private TextView tvBloodGlucoseDetectionValue;
    private TextView tvBloodOxygenDetectionDate;
    private TextView tvBloodOxygenDetectionValue;
    private TextView tvBreatheDetectionDate;
    private TextView tvBreatheDetectionValue;
    private TextView tvThermometerDetectionDate;
    private TextView tvThermometerDetectionValue;
    private TextView tvMonitorSuggest;
    private TextView tvDetectionSuggest;
    private ListView lvMonitorSuggest;
    private ListView lvDetectionSuggest;
    private RecordManager mRecordManager;
    private MeasureInfoManager mMeasureInfoManager;
    private int memberChildId;
    private List<RecordIndexData> mRecordIndexdatas;
    private HealthMonitorSuggestAdapter healthMonitorSuggestAdapter;
    private HealthDetectionSuggestAdapter healthDetectionSuggestAdapter;
    private ScrollView scrollview;
    private Runnable runnable;
    private RelativeLayout mRlHeartRate;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_measure_info);
    }

    @Override
    public void initViews() {
        //初始化titleBar
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.MeasureInfoActivity_tizhengjianjiance), titleBar, 1, this);

        //手机制造商
        String deviceManufacturer = DeviceUtils.getDeviceManufacturer();

        //近期检测控件
        mRlHeartRate = findViewById(R.id.rl_heart_rate);
        tvHeartRateDetectionDate = findViewById(R.id.tv_heart_rate_detection_date);
        tvHeartRateDetectionValue = findViewById(R.id.tv_heart_rate_detection_value);
        tvBloodPressureDetectionDate = findViewById(R.id.tv_blood_pressure_detection_date);
        tvBloodPressureDetectionValue = findViewById(R.id.tv_blood_pressure_detection_value);
        tvBloodGlucoseDetectionDate = findViewById(R.id.tv_blood_glucose_detection_date);
        tvBloodGlucoseDetectionValue = findViewById(R.id.tv_blood_glucose_detection_value);
        tvBloodOxygenDetectionDate = findViewById(R.id.tv_blood_oxygen_detection_date);
        tvBloodOxygenDetectionValue = findViewById(R.id.tv_blood_oxygen_detection_value);
        tvBreatheDetectionDate = findViewById(R.id.tv_breathe_detection_date);
        tvBreatheDetectionValue = findViewById(R.id.tv_breathe_detection_value);
        tvThermometerDetectionDate = findViewById(R.id.tv_thermometer_detection_date);
        tvThermometerDetectionValue = findViewById(R.id.tv_thermometer_detection_value);

        //监测建议控件
        tvMonitorSuggest = findViewById(R.id.tv_monitor_suggest);
        lvMonitorSuggest = findViewById(R.id.lv_monitor_suggest);
        lvMonitorSuggest.setAdapter(healthMonitorSuggestAdapter);

        //检测建议控件
        tvDetectionSuggest = findViewById(R.id.tv_detection_suggest);
        lvDetectionSuggest = findViewById(R.id.lv_detection_suggest);
        lvDetectionSuggest.setAdapter(healthDetectionSuggestAdapter);

        scrollview = findViewById(R.id.scrollView_test);

        if (deviceManufacturer.equals("HC03")) {
            mRlHeartRate.setVisibility(View.VISIBLE);
        } else {
            mRlHeartRate.setVisibility(View.GONE);
        }
    }

    @Override
    public void initDatas() {

        fetchChildMemberId();
        mRecordManager = new RecordManager();
        mMeasureInfoManager = new MeasureInfoManager();
        healthMonitorSuggestAdapter = new HealthMonitorSuggestAdapter(this);
        healthDetectionSuggestAdapter = new HealthDetectionSuggestAdapter(this);

        runnable = new Runnable() {
            @Override
            public void run() {
                scrollview.scrollTo(0, 0);// 改变滚动条的位置
            }
        };
        new Handler().postDelayed(runnable, 20);
    }

    /**
     * 获取选择的用户id;
     */

    private void fetchChildMemberId() {
        ChildMember choosedChildMember = LoginControllor.getChoosedChildMember();
        if (choosedChildMember == null) {
            try {
                memberChildId = LoginControllor.getLoginMember().getMengberchild().get(0).getId();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            memberChildId = choosedChildMember.getId();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("retrofit","===MeasureInfoActivity===OnResume====");
        getLastDataOnResumeOrMemberChange();
    }

    /**
     * 在onResume中调用，确保展示的是最新的信息；
     */
    private void getLastDataOnResumeOrMemberChange() {
        //获取最新检测记录
        mRecordManager.oldgetRecordIndex(memberChildId, new AbstractDefaultHttpHandlerCallback(MeasureInfoActivity.this) {
            @Override
            protected void onResponseSuccess(Object obj) {
                mRecordIndexdatas = (List<RecordIndexData>) obj;
                Log.e("retrofit","===一测列表数据==="+mRecordIndexdatas.toString());
                handleResponseRecord(mRecordIndexdatas);
            }
        });
        //获取体征监测建议
//        mMeasureInfoManager.getMontiorSuggest(memberChildId, new AbstractDefaultHttpHandlerCallback() {
//            @Override
//            protected void onResponseSuccess(Object obj) {
//                List<String> ms = (List<String>) obj;
//                handleResponseMontiorSuggest(ms);
//            }
//        });
        List<String> monitorSuggest = new ArrayList();
        monitorSuggest.add("XL");
        monitorSuggest.add("XYa");
        monitorSuggest.add("XT");
        healthMonitorSuggestAdapter.setmDatas(monitorSuggest);
        String msStr = getString(R.string.measure_info_msstr);
        tvMonitorSuggest.setText(msStr);
        //获取体检建议
//        mMeasureInfoManager.getDetectionSuggest(memberChildId, new AbstractDefaultHttpHandlerCallback() {
//            @Override
//            protected void onResponseSuccess(Object obj) {
//                List<String> ms = (List<String>) obj;
//                handleResponseDetectionSuggest(ms);
//            }
//        });
        List<String> detectionSuggest = new ArrayList();
        detectionSuggest.add(getString(R.string.measure_info_gangongneng));
        detectionSuggest.add(getString(R.string.measure_info_feigongneng));
        healthDetectionSuggestAdapter.setmDatas(detectionSuggest);
        String dsStr = getString(R.string.measure_info_dsstr);
        tvDetectionSuggest.setText(dsStr);
    }

    /**
     * 将获取的体检建议展示在布局
     *
     * @param ds 获取的检测记录；
     */
    private void handleResponseDetectionSuggest(List<String> ds) {
        healthDetectionSuggestAdapter.setmDatas(ds);
        String dsStr = getString(R.string.measure_info_dsstr);
        tvDetectionSuggest.setText(dsStr);
    }

    /**
     * 将获取的监测建议展示在布局
     *
     * @param ms 获取的检测记录；
     */
    private void handleResponseMontiorSuggest(List<String> ms) {
        healthMonitorSuggestAdapter.setmDatas(ms);
        String msStr = getString(R.string.measure_info_msstr);
        tvMonitorSuggest.setText(msStr);
    }

    /**
     * 将获取的最新检测记录展示在布局
     *
     * @param mRecordIndexdatas 获取的检测记录；
     */
    private void handleResponseRecord(List<RecordIndexData> mRecordIndexdatas) {
        String value = getString(R.string.measure_info_shangweijiance);
        tvHeartRateDetectionValue.setText(value);
        tvHeartRateDetectionDate.setText("");
        tvBloodPressureDetectionValue.setText(value);
        tvBloodPressureDetectionDate.setText("");
        tvBloodOxygenDetectionValue.setText(value);
        tvBloodOxygenDetectionDate.setText("");
        tvThermometerDetectionValue.setText(value);
        tvThermometerDetectionDate.setText("");
        tvBloodGlucoseDetectionValue.setText(value);
        tvBloodGlucoseDetectionDate.setText("");
        tvBreatheDetectionValue.setText(value);
        tvBreatheDetectionDate.setText("");
        if (mRecordIndexdatas != null && mRecordIndexdatas.size() > 0) {
            for (RecordIndexData data : mRecordIndexdatas) {
                String category = data.getCategory();
                if (category.equalsIgnoreCase(getString(R.string.MeasureInfoActivity_xinlv).trim())){
                    tvHeartRateDetectionValue.setText(data.getResult());
                    tvHeartRateDetectionDate.setText(getTodayOrYesterday(data.getTime()).trim());
                }else if (category.equalsIgnoreCase(getString(R.string.MeasureInfoActivity_xueya))){
                    tvBloodPressureDetectionValue.setText(data.getResult());
                    tvBloodPressureDetectionDate.setText(getTodayOrYesterday(data.getTime()).trim());
                }else if (category.equalsIgnoreCase(getString(R.string.MeasureInfoActivity_xueyang))){
                    tvBloodOxygenDetectionValue.setText(data.getResult());
                    tvBloodOxygenDetectionDate.setText(getTodayOrYesterday(data.getTime()).trim());
                }else if (category.equalsIgnoreCase(getString(R.string.MeasureInfoActivity_tiwen))){
                    tvThermometerDetectionValue.setText(data.getResult());
                    tvThermometerDetectionDate.setText(getTodayOrYesterday(data.getTime()).trim());
                }else if (category.equalsIgnoreCase(getString(R.string.MeasureInfoActivity_xuetang))){
                    tvBloodGlucoseDetectionValue.setText(data.getResult());
                    tvBloodGlucoseDetectionDate.setText(getTodayOrYesterday(data.getTime()).trim());
                }else if (category.equalsIgnoreCase(getString(R.string.MeasureInfoActivity_huxi))){
                    tvBreatheDetectionValue.setText(data.getResult());
                    tvBreatheDetectionDate.setText(getTodayOrYesterday(data.getTime()).trim());
                }

            }
        }
    }

    private String getTodayOrYesterday(long date) {//date 是存储的时间戳
        //所在时区时8，系统初始时间是1970-01-01 80:00:00，注意是从八点开始，计算的时候要加回去
        int offSet = Calendar.getInstance().getTimeZone().getRawOffset();
        long today = (System.currentTimeMillis() + offSet) / 86400000;
        long start = (date + offSet) / 86400000;
        long intervalTime = start - today;
        //-2:前天,-1：昨天,0：今天,1：明天,2：后天
        String strDes = "";
        if (intervalTime == 0) {
            strDes = getString(R.string.measure_info_strDes_jintian);
        } else if (intervalTime == -1) {
            strDes = getString(R.string.measure_info_strDes_zuotian);
        } else if (intervalTime == -2) {
            strDes = getString(R.string.measure_info_strDes_onetian);
        } else if (intervalTime == -3) {
            strDes = getString(R.string.measure_info_strDes_twotian);
        } else if (intervalTime == -4) {
            strDes = getString(R.string.measure_info_strDes_threetian);
        } else if (intervalTime == -5) {
            strDes = getString(R.string.measure_info_strDes_fourtian);
        } else if (intervalTime == -6) {
            strDes = getString(R.string.measure_info_strDes_fivetian);
        } else if (intervalTime == -7) {
            strDes = getString(R.string.measure_info_strDes_sixtian);
        } else {
            strDes = getString(R.string.measure_info_strDes_sixtian);
        }
        return strDes;
    }

    public void doDetection(View view) {
        switch (view.getId()) {
            case R.id.iv_detection_heart_rate:
                //是否需要进入心电首次进入界面 0表示不显示
                if ("0".equals(SharedPreferenceUtil.getECG())) {
                    //控制是否显示两条线
                    startActivity(new Intent(this, ECGCheckActivity.class));
                    // startActivity(new Intent(this, ECGCompareActivity.class));
                } else {
                    startActivity(new Intent(this, ECGUseFirstActivity.class));
                }
                break;
            case R.id.iv_detection_blood_pressure:
                //是否需要进入血压首次进入界面 0表示不显示
                if ("0".equals(SharedPreferenceUtil.getBloodPressure())) {
                    startActivity(new Intent(this, BloodPressureCheckActivity.class));
                } else {
                    startActivity(new Intent(this, BloodPressureUseFirstActivity.class));
                }

                break;
            case R.id.iv_detection_blood_glucose:
                //是否需要进入血糖首次进入界面 0表示不显示
                if ("0".equals(SharedPreferenceUtil.getBloodSugar())) {
                    startActivity(new Intent(this, BloodSugarCheckActivity.class));
                } else {
                    startActivity(new Intent(this, BloodSugarUseFirstActivity.class));
                }
                break;
            case R.id.iv_detection_blood_oxygen:
                //是否需要进入血氧首次进入界面 0表示不显示
                if ("0".equals(SharedPreferenceUtil.getSPO2H())) {
                    startActivity(new Intent(this, SPO2HCheckActivity.class));
                } else {
                    startActivity(new Intent(this, SPO2HUseFirstActivity.class));
                }
                break;
            case R.id.iv_detection_breathe:
                //是否需要进入呼吸首次进入界面 0表示不显示
                if ("0".equals(SharedPreferenceUtil.getBreath())) {
                    startActivity(new Intent(this, BreathActivity.class));

                } else {
                    startActivity(new Intent(this, BreathUseFirstActivity.class));
                }
                break;
            case R.id.iv_detection_thermometer:
                //是否需要进入体温首次进入界面 0表示不显示
//                if ("0".equals(SharedPreferenceUtil.getTemperature())) {
//                    startActivity(new Intent(this, TemperatureDetectionActivity.class));
//
//                } else {
//                    startActivity(new Intent(this, TemperatureUserFirstActivity.class));
//                }
                startActivity(new Intent(this, TemperatureDetectionWriteActivity.class));

                break;
            default:
                break;

        }
    }

    /**
     * TitleBar选择用户回调
     * 用于完成用户切换时拉取信息
     */
    @Override
    public void onComplete() {
//        fetchChildMemberId();
        getLastDataOnResumeOrMemberChange();
    }
}
