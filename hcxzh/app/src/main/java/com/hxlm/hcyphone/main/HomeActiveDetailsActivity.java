package com.hxlm.hcyphone.main;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.util.WebViewUtil;

public class HomeActiveDetailsActivity extends BaseActivity {

    private String indexLink;
    private WebView wbDetails;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_active_details);

        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.home_active_details_title), titleBar, 1);
        wbDetails = findViewById(R.id.wb_home_details);
        progressBar = findViewById(R.id.progressbar);

        indexLink = getIntent().getStringExtra("indexLink");

        new WebViewUtil().setWebViewInit(wbDetails, progressBar,this, Constant.BASE_URL+indexLink);

    }

    @Override
    public void setContentView() {

    }

    @Override
    public void initViews() {

    }

    @Override
    public void initDatas() {

    }
}
