package com.hxlm.hcyphone.bean;

import java.util.List;

/**
 * @author Lirong
 * @date 2018/12/14.
 * @description  首页图片bean
 */

public class IndexPicBean {

        /**
         * id : 1
         * createDate : 1544603830737
         * modifyDate : 1544603830737
         * type : 1
         * picurl : /resources/service/hechangxing/dong.png
         * title : 和畅包
         * link : /member/service/view/fang/JLBS/1/yidai
         */

        private int id;
        private long createDate;
        private long modifyDate;
        private int type;
        private String picurl;
        private String title;
        private String link;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public long getCreateDate() {
            return createDate;
        }

        public void setCreateDate(long createDate) {
            this.createDate = createDate;
        }

        public long getModifyDate() {
            return modifyDate;
        }

        public void setModifyDate(long modifyDate) {
            this.modifyDate = modifyDate;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getPicurl() {
            return picurl;
        }

        public void setPicurl(String picurl) {
            this.picurl = picurl;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

    @Override
    public String toString() {
        return "IndexPicBean{" +
                "id=" + id +
                ", createDate=" + createDate +
                ", modifyDate=" + modifyDate +
                ", type=" + type +
                ", picurl='" + picurl + '\'' +
                ", title='" + title + '\'' +
                ", link='" + link + '\'' +
                '}';
    }
}
