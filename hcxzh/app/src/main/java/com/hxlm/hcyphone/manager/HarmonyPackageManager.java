package com.hxlm.hcyphone.manager;

import android.content.Context;
import android.os.Handler;

import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.asynchttp.HcyHttpResponseHandler;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.hcyandroid.datamanager.BaseManager;
import com.loopj.android.http.RequestParams;

/**
 * Created by lipengcheng on 2017/12/5.
 */

public class HarmonyPackageManager extends BaseManager {
    Handler handler;

    public static final int CURRENT_STATE_MESSAGE = 111;

    public HarmonyPackageManager(Handler handler) {
        super(handler);
        this.handler = handler;
    }

    public HarmonyPackageManager() {
    }

    public void getCurrentState(Context context, int memberChildId, AbstractDefaultHttpHandlerCallback callback) {
        String method = METHOD_GET;
        String url = "/member/new_ins/current.jhtml";
        RequestParams params = new RequestParams();
        params.put("memberChildId", memberChildId);
        params.put("isnew", 1);
        HcyHttpClient.submitAutoLogin(HcyHttpClient.METHOD_GET, url, params, new HcyHttpResponseHandler(callback) {

            @Override
            public Object contentParse(String content) {

                if (content != null && !content.equals("")) {
                    try {
                        return content;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return "";
            }
        }, LoginControllor.MEMBER, callback.getContext());

    }

    {
//        LoginControllor.requestLogin(context, new OnCompleteListener() {
//        @Override
//        public void onComplete() {
//            queryRemoteWithHeader(method, url, params, new OnSuccessListener() {
//                @Override
//                public void onSuccess(String content) {
//                    com.alibaba.fastjson.JSONObject jo = JSON.parseObject(content);
//                    int status = jo.getInteger("status");
//                    if (status == ResponseStatus.SUCCESS) {
////                    int state  = jo.getInteger("num");
////                    String name = jo.getString("name");jo.getString("data")
//                        Message msg = Message.obtain();
//                        msg.what = CURRENT_STATE_MESSAGE;
//                        msg.obj = content;
//                        handler.sendMessage(msg);
//                    }
//                }
//            });
//        }
//    });
    }
}
