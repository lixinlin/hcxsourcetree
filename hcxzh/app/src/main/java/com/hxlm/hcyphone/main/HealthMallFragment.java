package com.hxlm.hcyphone.main;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.android.utils.Logger;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.tabbar.healthmall.LoginInterface;
import com.hxlm.hcyandroid.tabbar.healthmall.PayInterface;
import com.hxlm.hcyandroid.util.StatusBarUtils;
import com.hxlm.hcyandroid.util.WebViewUtil;
import com.hxlm.hcyphone.BaseFragment;

import java.util.HashMap;

/**
 *  和畅商场
 * Created by JQ on 2017/11/28.
 */

public class HealthMallFragment extends BaseFragment {

    private final String tag = getClass().getSimpleName();
    private WebView wv;
    private ProgressBar progressBar;
    private WebViewUtil webViewUtil;
    private LinearLayout linear_request_error;//网络请求错误提示页面
    private ImageView img_request_again;
    TitleBarView titleBar;
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case LoginInterface.LOGIN:
                    final String aurl = (String) msg.obj;

                    Logger.i(tag, "aurl = " + aurl);

                    LoginControllor.requestLogin(getActivity(), new OnCompleteListener() {
                        @Override
                        public void onComplete() {
                            WebViewUtil.syncCookie(getActivity());
//                            wv.loadUrl(aurl);
                            if (Constant.isEnglish) {
                                HashMap<String, String> header = new HashMap<>();
                                header.put("language", "us-en");
                                wv.loadUrl(aurl, header);
                            }else {
                                wv.loadUrl(aurl);
                            }
                        }
                    });
                    break;
            }
        }
    };

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            //隐藏时使用
        } else {
            //显示时使用
        }
    }

    @Override
    protected int getContentViewResource() {
        return R.layout.fragment_health_mall;
    }

    @Override
    protected void initView(View view) {
        titleBar = new TitleBarView();
        titleBar.init(getActivity(), getString(R.string.health_mall_title), titleBar, 0);

        progressBar=(ProgressBar)view.findViewById(R.id.progressbar);
        wv = (WebView) view.findViewById(R.id.wv);
        linear_request_error=(LinearLayout)view.findViewById(R.id.linearmy_request_error);
        img_request_again=(ImageView)view.findViewById(R.id.img_request_again);

        wv.addJavascriptInterface(new LoginInterface(handler), "loginInterface");

        wv.addJavascriptInterface(new PayInterface(getActivity()), "payInterface");

        String url = Constant.BASE_URL + "/mobileIndex.html";//测试版

        webViewUtil = new WebViewUtil();
        webViewUtil.setWebViewInit(wv,progressBar,getActivity(),url);
    }

    @Override
    protected void initStartup() {

    }

    @Override
    protected void initData() {

    }
}
