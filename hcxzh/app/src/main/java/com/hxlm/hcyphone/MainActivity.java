package com.hxlm.hcyphone;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.ContentObserver;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.location.LocationClient;
import com.hcy.ky3h.R;
import com.hcy.ky3h.collectdata.CDRequestUtils;
import com.hcy_futejia.activity.FtjSettingActivity;
import com.hcy_futejia.fragment.FtjHomeFragment;
import com.hcy_futejia.fragment.FtjMyFragment;
import com.hcy_futejia.utils.DateUtil;
import com.hcy_futejia.utils.DialogUtils;
import com.hcy_futejia.utils.DownloadManagerPro;
import com.hcy_futejia.utils.PackageUtils;
import com.hjq.permissions.OnPermission;
import com.hjq.permissions.Permission;
import com.hjq.permissions.XXPermissions;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.message.MessageService;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.BaseApplication;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.MessageType;
import com.hxlm.hcyandroid.bean.Version;
import com.hxlm.hcyandroid.datamanager.AdvertisementManager;
import com.hxlm.hcyandroid.datamanager.UpdateManager;
import com.hxlm.hcyandroid.tabbar.ArchivesFragment;
import com.hxlm.hcyandroid.tabbar.MyFragmentBroadcast;
import com.hxlm.hcyandroid.tabbar.MyHealthFileBroadcast;
import com.hxlm.hcyandroid.tabbar.home_jczs.MenuActiviy;
import com.hxlm.hcyandroid.util.AppManager;
import com.hxlm.hcyandroid.util.SharedPreferenceUtil;
import com.hxlm.hcyandroid.util.StatusBarUtils;
import com.hxlm.hcyphone.main.HealthMallFragment;

import java.io.File;
import java.util.List;
import java.util.Map;

public class MainActivity extends FragmentActivity implements OnCheckedChangeListener, View.OnClickListener {
    private static final String TAG = "MainActivityJDF";
    public static Activity activity;
    // 控件的宽度
    public static int screenWidth;
    private static boolean isEx = false;
    private RadioButton rb1, rb2, rb3, rb4;
    private AdvertisementManager advertisementManager;

    //点击健康咨询
    private ImageView menuOverlay;
    // 顺时针旋转动画
    private Animation animRotateClockwise;
    // 逆时针旋转动画
    private Animation animRotateAntiClockwise;


    //选中 drawableTop在代码中设置图片
    private Drawable drawable1;
    private Drawable drawable2;
    private Drawable drawable3;
    private Drawable drawable4;

    private Drawable drawable1_no_click;
    private Drawable drawable2_no_click;
    private Drawable drawable3_no_click;
    private Drawable drawable4_no_click;

    private Button btn1, btn2, btn3, btn4;

    private String otherReport;
    private String tag;//对应档案中要查看的报告种类
    private int memberChildId;
    private int jump = 0;//跳转对应的档案

    private static boolean isPopup = true;

    private LocationClient mLocationClient = null;
    private String city; // 城市

    private MyHealthFileBroadcast healthFileBroadcast = null;
    private MyFragmentBroadcast myFragmentBroadcast = null;
    public static final int MY_HEALTH_FILT = 1200;// 在家庭成员或者其他页面点击查看当前用户档案，跳转健康档案界面
    public static final int MY_FRAGMENT = 1500;//跳转到我的


    private SharedPreferences sp;
    Handler handler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            isEx = false;
            switch (msg.what) {
                case MessageType.MESSAGE_UPDATE_CITY:
                    mLocationClient.stop();
                    city = "";
                    break;
                // 在家庭成员或者其他页面点击查看当前用户档案，跳转健康档案界面
                case MY_HEALTH_FILT:
                    // 获取bundle对象的值
                    Bundle bundle = msg.getData();
                    String str = bundle.getString("ArchivesFragment");
                    if ("3".equals(str)) {
                        otherReport = bundle.getString("otherReport");
                        tag = bundle.getString("tag");
                        jump = bundle.getInt("Jump");
                        Log.e("pressure","===aaa==="+bundle.getString("aaa"));
                        //从我的家庭成员查看档案跳转过来
                        LoginControllor.chooseChildMember(MainActivity.this, new OnCompleteListener() {
                            @Override
                            public void onComplete() {

                                setDrawableTopAndColorTabBtn2();

                                Bundle bundle = new Bundle();
                                bundle.putString("otherReport", "");
                                bundle.putString("tag", tag);
                                bundle.putInt("memberChildId", memberChildId);
                                bundle.putInt("jump", jump);

                                if (archivesFragment == null) {
                                    archivesFragment = new ArchivesFragment();
                                }
//                                archivesFragment.setArguments(bundle);
                                archivesFragment.getInstance(bundle);
                                changeFragment(archivesFragment, 3).commitAllowingStateLoss();
                            }
                        });


                    }
                    if (Constant.DEBUG) {
                        Log.i("Log.i", "我收到了");
                    }
                    break;
                //健康商城支付成功，点击按钮跳转到我的页面
                case MY_FRAGMENT:
                    // 获取bundle对象的值
                    Bundle bundlemy = msg.getData();
                    String myFragmentStr = bundlemy.getString("MyFragment");

                    if ("myFrag".equals(myFragmentStr)) {
                        //跳转我的页面
                        setDrawableTopAndColorTabBtn4();
                        if (myFragment == null) {
//                            myFragment = new MyFragment();
                            myFragment = new FtjMyFragment();
                        }
                        changeFragment(myFragment, 1).commitAllowingStateLoss();
                    }
                    break;
            }
        }
    };
    private String province; // b 省

    private View mstatusBar;
    private View mViewTitle;
    private   long lastClickTime=0;//上次点击的时间
    private   int spaceTime = 500;//时间间隔
    private Fragment currentFragment;
    private ArchivesFragment archivesFragment;
    private FtjMyFragment myFragment;
    private FtjHomeFragment ftjhomeFragment;
    private HealthMallFragment healthMallFragment;
    private ProgressBar progressBar;
    private TextView tv_update;
    private Dialog dialog1;
    private DownloadManager downloadManager;
    private DownloadManagerPro downloadManagerPro;
    private DownloadChangeObserver downloadObserver;
    private CompleteReceiver completeReceiver;
    private long downloadId;
    private TextView tv_progress;
    private RelativeLayout rl_progress;

    private  boolean isAllowClick() {
        long currentTime = System.currentTimeMillis();//当前系统时间
        boolean isAllowClick;//是否允许点击
        if (currentTime - lastClickTime > spaceTime) {
            isAllowClick= true;
            lastClickTime = currentTime;
        } else {
            isAllowClick = false;
        }

        return isAllowClick;

    }


    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        downloadObserver = new DownloadChangeObserver();
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {//4.4 全透明状态栏
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {//5.0 全透明实现
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor("#ffffff"));//calculateStatusColor(Color.WHITE, (int) alphaValue)
        }

        AppManager.getInstance().addActivity(this);

        //顺时针旋转45度
        animRotateClockwise = AnimationUtils.loadAnimation(
                this, R.anim.rotate_open);

        //逆时针返回
        animRotateAntiClockwise = AnimationUtils.loadAnimation(
                this, R.anim.rotate_close);


        drawable1 = this.getResources().getDrawable(R.drawable.tab1_click);
        drawable2 = this.getResources().getDrawable(R.drawable.tab2_click);
        drawable3 = this.getResources().getDrawable(R.drawable.tab3_click);
        drawable4 = this.getResources().getDrawable(R.drawable.tab4_click);

        drawable1_no_click = this.getResources().getDrawable(R.drawable.tab1_noclick);
        drawable2_no_click = this.getResources().getDrawable(R.drawable.tab2_noclick);
        drawable3_no_click = this.getResources().getDrawable(R.drawable.tab3_noclick);
        drawable4_no_click = this.getResources().getDrawable(R.drawable.tab4_noclick);


        // 动态注册广播 防止重复注册多个广播 健康档案
        if (healthFileBroadcast == null) {
            healthFileBroadcast = new MyHealthFileBroadcast(handler);
            registerReceiver(healthFileBroadcast, new IntentFilter(
                    MyHealthFileBroadcast.ACTION));
        }

        // 动态注册广播 防止重复注册多个广播 跳转我的页面
        if (myFragmentBroadcast == null) {
            myFragmentBroadcast = new MyFragmentBroadcast(handler);
            registerReceiver(myFragmentBroadcast, new IntentFilter(
                    MyFragmentBroadcast.ACTION));
        }


        // 百度定位
//        updateCity();

        // 开启获取消息的服务在onCreate的时候进行开启
        this.startService(new Intent(this, MessageService.class));

        //判断是否有新版本
        UpdateManager updateManager = new UpdateManager(handler, MainActivity.this);

        updateManager.isUpdate(new AbstractDefaultHttpHandlerCallback(this) {
            @Override
            protected void onResponseSuccess(Object obj) {
                Version version = (Version) obj;
                if (version.getIsUpdate()) {
                    handler = new Handler();

                    completeReceiver = new CompleteReceiver();
                    /** register download success broadcast **/
                    registerReceiver(completeReceiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
                    if(version.isEnforcement()){
                        //强制更新
                        Map<String, Object> map = DialogUtils.setDialogProgressBar(MainActivity.this, version.getReleaseContent(), true, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                rl_progress.setVisibility(View.VISIBLE);
                                progressBar.setVisibility(View.VISIBLE);
                                downloadapk(version.getDownurl());
                                getCollectDownload();
                                tv_update.setVisibility(View.GONE);
                                tv_progress.setVisibility(View.VISIBLE);
                            }
                        });
                        progressBar = (ProgressBar) map.get("pb");
                        tv_update = (TextView) map.get("tv_dialog_update");
                        tv_progress = (TextView) map.get("tv_progress");
                        rl_progress = (RelativeLayout) map.get("rl_progress");

                    }else {
                        //可选更新
                        Map<String, Object> map = DialogUtils.setDialogProgressBar(MainActivity.this, version.getReleaseContent(), false, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                rl_progress.setVisibility(View.VISIBLE);
                                progressBar.setVisibility(View.VISIBLE);
                                downloadapk(version.getDownurl());
                                getCollectDownload();
                                dialog1.setCancelable(false);
                                tv_update.setVisibility(View.GONE);
                                tv_progress.setVisibility(View.VISIBLE);
                            }
                        });
                        progressBar = (ProgressBar) map.get("pb");
                        tv_update = (TextView) map.get("tv_dialog_update");
                        tv_progress = (TextView) map.get("tv_progress");
                        dialog1 = (Dialog) map.get("dialog");
                        rl_progress = (RelativeLayout) map.get("rl_progress");
                    }
                }
            }
        });
        activity = MainActivity.this;

        mViewTitle = findViewById(R.id.view_title);
        RadioGroup rg = (RadioGroup) findViewById(R.id.rg);
        rb1 = (RadioButton) findViewById(R.id.rb1);
        rb2 = (RadioButton) findViewById(R.id.rb2);
        rb3 = (RadioButton) findViewById(R.id.rb3);
        rb4 = (RadioButton) findViewById(R.id.rb4);
        rg.setOnCheckedChangeListener(this);

        //rb1.setChecked(true);//默认首页选中

        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);
        btn4 = (Button) findViewById(R.id.btn4);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        sp = getSharedPreferences("pub", Context.MODE_PRIVATE);

        //------------------------------------------------------
        //根据标识判断是否是第一次打开应用
        String strFirst = SharedPreferenceUtil.getString("isFirstLogin");
        //暂时取消引导页；
        strFirst = "false";
//        if (TextUtils.isEmpty(strFirst)) {
//            final MainFirstAplashDialog mainFirstAplashDialog = new MainFirstAplashDialog(MainActivity.this);
//            mainFirstAplashDialog.setOnListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    int tag = (int) view.getTag();
//                    switch (tag) {
//                        case R.id.img_isfirst:
//                            mainFirstAplashDialog.dismiss();
//                            break;
//                        default:
//                            break;
//                    }
//                }
//            });
//            mainFirstAplashDialog.setCanceledOnTouchOutside(false);
//            mainFirstAplashDialog.show();
//            //跳转第一次引导页
//            SharedPreferenceUtil.saveString("isFirstLogin", "false");
//        }


        //--------------------------------------------------------------
        //点击健康咨询按钮
        menuOverlay = (ImageView) findViewById(R.id.button_menu);

        //点击效果
        menuOverlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MenuActiviy.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        //-------------------------------结束-------------
        //默认首页选中
        setDrawableTopAndColorTabBtn1();
        if (ftjhomeFragment == null) {
            ftjhomeFragment = new FtjHomeFragment();
        }
        changeFragment(ftjhomeFragment, 1).commitAllowingStateLoss();

        screenWidth = BaseApplication.screenWidth;

        XXPermissions.with(this)
                .permission(Permission.Group.STORAGE)
                .request(new OnPermission() {

                    @Override
                    public void hasPermission(List<String> granted, boolean isAll) {

                    }

                    @Override
                    public void noPermission(List<String> denied, boolean quick) {

                    }
                });
    }

    /**
     * 数据埋点下载接口
     */
    private void getCollectDownload() {
        String dateToString = DateUtil.getDateToString(System.currentTimeMillis(), "yyyy-MM-dd HH:mm:ss");
        CDRequestUtils.getDownload(LoginControllor.getLoginMember().getId()+"","1","1", PackageUtils.getVersionName(MainActivity.this),dateToString,"");
    }

    private void downloadapk(String url) {
        tv_update.setClickable(false);
        File folder = Environment.getExternalStoragePublicDirectory("HCY");
        boolean b = (folder.exists() && folder.isDirectory()) ? true : folder.mkdirs();
        File apk = new File(folder,"hcy.apk");
        if(apk.exists()){
            apk.delete();
        }
        downloadManager = (DownloadManager)getSystemService(DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDestinationInExternalPublicDir("HCY", "hcy.apk");
        downloadId = downloadManager.enqueue(request);
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        int mainId = intent.getIntExtra("mainId", 0);
        if (mainId == 3) {
            Log.e("retro", "==mainId=3===");
            setDrawableTopAndColorTabBtn3();
//                changeFragment(new InformationFragment(), 2);
            changeFragment(new HealthMallFragment(), 3).commitAllowingStateLoss();
        }
    }


    private void exit() {
        if (!isEx) {
            isEx = true;
            Toast.makeText(this, getString(R.string.main_exit_app), Toast.LENGTH_SHORT).show();
            handler.sendEmptyMessageDelayed(0, 2000);
        } else {

            // 退出应用，下次进入首界面的时候不在跳转到知己档案
            SharedPreferenceUtil.removeArchivesFragment();

            // 删除用户信息
            LoginControllor.clearLastLoginInfo();

            // 停止服务
            stopService(new Intent(this, MessageService.class));

            this.sendBroadcast(new Intent().setAction(BaseActivity.ACTION_EXIT));
            finish();
            AppManager.getInstance().AppExit();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        // 动态注销广播 健康档案
        if (healthFileBroadcast != null) {
            unregisterReceiver(healthFileBroadcast);
            healthFileBroadcast = null;
        }

        // 动态注销广播 跳转我的页面
        if (myFragmentBroadcast != null) {
            unregisterReceiver(myFragmentBroadcast);
            myFragmentBroadcast = null;
        }
        try {
                unregisterReceiver(completeReceiver);
            if(downloadManager!=null){
                downloadManager.remove(downloadId);//取消下载
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        /** observer download change **/

        try {
            getContentResolver().registerContentObserver(DownloadManagerPro.CONTENT_URI, true, downloadObserver);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if(downloadObserver!=null){
            getContentResolver().unregisterContentObserver(downloadObserver);
        }

    }


    private FragmentTransaction changeFragment(Fragment targetFragment, int which) {
        if (which == 1) {
            mViewTitle.setVisibility(View.GONE);
        } else if (which == 2) {
            mViewTitle.setVisibility(View.GONE);
        } else {
            mViewTitle.setVisibility(View.VISIBLE);
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (!targetFragment.isAdded()) {
            if (currentFragment != null) {
                transaction.hide(currentFragment);
            }
            transaction.add(R.id.main_fragment, targetFragment, targetFragment.getClass().getName());
        } else {
            transaction.hide(currentFragment)
                    .show(targetFragment);
        }
        currentFragment = targetFragment;
        return transaction;

    }

    @Override
    public void onCheckedChanged(RadioGroup arg0, int checkedId) {
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn1:
                if(isAllowClick()) {
                    mViewTitle.setVisibility(View.GONE);
                    setDrawableTopAndColorTabBtn1();

                    if (ftjhomeFragment == null) {
                        ftjhomeFragment = new FtjHomeFragment();
                    }

                    changeFragment(ftjhomeFragment, 1).commitAllowingStateLoss();
                }

                break;
            case R.id.btn2:
                if(isAllowClick()){
                    LoginControllor.chooseChildMember(this, new OnCompleteListener() {
                        @Override
                        public void onComplete() {
                            setDrawableTopAndColorTabBtn2();
                            Bundle bundle = new Bundle();
                            bundle.putString("otherReport", "");
                            bundle.putInt("memberChildId", 0);
                            bundle.putString("title",getString(R.string.main_health_history));
                            bundle.putInt("jump", 0);

                            if (archivesFragment == null) {
                                archivesFragment = new ArchivesFragment();
                            }
                            StatusBarUtils.setWindowStatusBarColor(MainActivity.this,R.color.white);
                            archivesFragment.getInstance(bundle);
                            changeFragment(archivesFragment, 3).commitAllowingStateLoss();

                        }
                    });
                }

                break;
            case R.id.btn3:
                if(isAllowClick()){
                    setDrawableTopAndColorTabBtn3();
                    if(healthMallFragment == null){
                        healthMallFragment = new HealthMallFragment();
                    }
                    StatusBarUtils.setWindowStatusBarColor(MainActivity.this,R.color.white);
                    changeFragment(new HealthMallFragment(), 3).commitAllowingStateLoss();
                }
                break;
            case R.id.btn4:
                if(isAllowClick()){
                    setDrawableTopAndColorTabBtn4();
                    Bundle bundle = new Bundle();
                    bundle.putString("title",getString(R.string.main_tab_btn4_txt));
                    if(myFragment == null){
//                        myFragment = new MyFragment();
                        myFragment = new FtjMyFragment();
                    }
//                    myFragment.setArguments(bundle);
                    StatusBarUtils.setWindowStatusBarColor(MainActivity.this,R.color.new_title_bar_bg);
                    changeFragment(myFragment, 1).commitAllowingStateLoss();
                    myFragment.getInstance(bundle);
                }
                break;
            default:
                break;
        }
    }

    //设置切换时的效果
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void setDrawableTopAndColorTabBtn1() {

        //drawableTop在代码中设置图片
        btn1.setCompoundDrawablesRelativeWithIntrinsicBounds(null, drawable1, null, null);
        btn1.setTextColor(getResources().getColor(R.color.color_1E82D2));

        btn2.setCompoundDrawablesRelativeWithIntrinsicBounds(null, drawable2_no_click, null, null);
        btn2.setTextColor(getResources().getColor(R.color.black));
        btn3.setCompoundDrawablesRelativeWithIntrinsicBounds(null, drawable3_no_click, null, null);
        btn3.setTextColor(getResources().getColor(R.color.black));
        btn4.setCompoundDrawablesRelativeWithIntrinsicBounds(null, drawable4_no_click, null, null);
        btn4.setTextColor(getResources().getColor(R.color.black));
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void setDrawableTopAndColorTabBtn2() {
        //drawableTop在代码中设置图片
        btn2.setCompoundDrawablesRelativeWithIntrinsicBounds(null, drawable2, null, null);
        btn2.setTextColor(getResources().getColor(R.color.color_1E82D2));

        btn1.setCompoundDrawablesRelativeWithIntrinsicBounds(null, drawable1_no_click, null, null);
        btn1.setTextColor(getResources().getColor(R.color.black));
        btn3.setCompoundDrawablesRelativeWithIntrinsicBounds(null, drawable3_no_click, null, null);
        btn3.setTextColor(getResources().getColor(R.color.black));
        btn4.setCompoundDrawablesRelativeWithIntrinsicBounds(null, drawable4_no_click, null, null);
        btn4.setTextColor(getResources().getColor(R.color.black));
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void setDrawableTopAndColorTabBtn3() {

        //drawableTop在代码中设置图片
        btn3.setCompoundDrawablesRelativeWithIntrinsicBounds(null, drawable3, null, null);
        btn3.setTextColor(getResources().getColor(R.color.color_1E82D2));

        btn1.setCompoundDrawablesRelativeWithIntrinsicBounds(null, drawable1_no_click, null, null);
        btn1.setTextColor(getResources().getColor(R.color.black));
        btn2.setCompoundDrawablesRelativeWithIntrinsicBounds(null, drawable2_no_click, null, null);
        btn2.setTextColor(getResources().getColor(R.color.black));
        btn4.setCompoundDrawablesRelativeWithIntrinsicBounds(null, drawable4_no_click, null, null);
        btn4.setTextColor(getResources().getColor(R.color.black));
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void setDrawableTopAndColorTabBtn4() {

        //drawableTop在代码中设置图片
        btn4.setCompoundDrawablesRelativeWithIntrinsicBounds(null, drawable4, null, null);
        btn4.setTextColor(getResources().getColor(R.color.color_1E82D2));

        btn1.setCompoundDrawablesRelativeWithIntrinsicBounds(null, drawable1_no_click, null, null);
        btn1.setTextColor(getResources().getColor(R.color.black));
        btn2.setCompoundDrawablesRelativeWithIntrinsicBounds(null, drawable2_no_click, null, null);
        btn2.setTextColor(getResources().getColor(R.color.black));
        btn3.setCompoundDrawablesRelativeWithIntrinsicBounds(null, drawable3_no_click, null, null);
        btn3.setTextColor(getResources().getColor(R.color.black));
    }

    //点击加号按钮，健康咨询颜色变为蓝色
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void setDrawableTopAndColorTabBtnText() {

        btn1.setCompoundDrawablesRelativeWithIntrinsicBounds(null, drawable1_no_click, null, null);
        btn1.setTextColor(getResources().getColor(R.color.black));
        btn2.setCompoundDrawablesRelativeWithIntrinsicBounds(null, drawable2_no_click, null, null);
        btn2.setTextColor(getResources().getColor(R.color.black));
        btn3.setCompoundDrawablesRelativeWithIntrinsicBounds(null, drawable3_no_click, null, null);
        btn3.setTextColor(getResources().getColor(R.color.black));
        btn4.setCompoundDrawablesRelativeWithIntrinsicBounds(null, drawable4, null, null);
        btn4.setTextColor(getResources().getColor(R.color.black));

    }
    class DownloadChangeObserver extends ContentObserver {

        public DownloadChangeObserver(){
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange) {
            try {
                //正在下载时关闭应用会引发null
                if(downloadManagerPro==null){
                    downloadManagerPro = new DownloadManagerPro(downloadManager);
                }
                int[] bytesAndStatus = downloadManagerPro.getBytesAndStatus(downloadId);
                int progress = (int) (((float) bytesAndStatus[0] / bytesAndStatus[1]) * 100);
                progressBar.setProgress( progress);
                tv_progress.setText(String.format("%d%% %n",progress));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    class CompleteReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            // 下载完成
            installApk();
        }
    }

    private void installApk() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        File folder = Environment.getExternalStoragePublicDirectory("HCY");
        boolean b = (folder.exists() && folder.isDirectory()) ? true : folder.mkdirs();
        File file = new File(folder,"hcy.apk");
        boolean exists = file.exists();
        Uri uri = Uri.withAppendedPath(Uri.fromFile(folder), "hcy.apk");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if(!exists){
                return;
            }
            //创建Uri
            Uri apkUri = FileProvider.getUriForFile(this, "com.hcy.ky3h.fileProvider", file);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
        } else {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setDataAndType(uri, "application/vnd.android.package-archive");
        }
        startActivity(intent);
        this.finish();

    }

}
