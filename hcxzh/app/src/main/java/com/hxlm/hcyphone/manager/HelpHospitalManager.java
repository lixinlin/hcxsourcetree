package com.hxlm.hcyphone.manager;

import android.os.Handler;
import android.os.Message;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.hcyphone.bean.Doctor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lixinlin on 2017/6/22.
 */
public class HelpHospitalManager {
    /**
     * 获取建议医生
     *
     * @param memberChildId
     * @param callback
     */
    public void getSuggestDoctor(int memberChildId, AbstractDefaultHttpHandlerCallback callback) {
        Doctor doctor1 = new Doctor("李大夫", "http://10.1.71.35/doctor.png", "内科", "教授", "友谊医院");
        Doctor doctor2 = new Doctor("李大夫", "http://10.1.71.35/doctor.png", "内科", "教授", "友谊医院");
        List<Doctor> doctors = new ArrayList<>();
        doctors.add(doctor1);
        doctors.add(doctor2);

        Handler handler = new Handler(callback);
        Message message = handler.obtainMessage();
        message.what = 1001;
        message.obj = doctors;
        handler.sendMessage(message);

    }
}
