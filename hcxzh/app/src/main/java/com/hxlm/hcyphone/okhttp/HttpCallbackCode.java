/**
 *
 */
package com.hxlm.hcyphone.okhttp;

/**
 * Http响应code
 * Created by JQ on 2017/7/21.
 */
public class HttpCallbackCode {

    // 错误日志code、错误信息msg
    public final static String ERROR_CODE = "errorCode";
    public final static String CODE = "code";
    public final static String CODE_NORMAL = "200";
    public final static String ERROR_MSG = "errorMsg";

    public final static int SUCCESS_CODE = 0;
    public final static int FAILURE_CODE = 1;

    // 正常
    public final static int RESULT_SUC_CODE = 200;

    // 此用户不是博雅立方的销售
    public final static int SALE_NOT_EXIST = 700;

    // token过期，需重新登录
    public final static int TOKEN_EXPIRE_CODE = 400;

}
