package com.hxlm.hcyphone.harmonypackage;

import android.content.Intent;
import android.view.View;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.tabbar.home_jczs.WebViewActivity;

/**
 * 经络梳理
 * Created by lipengcheng on 2017/6/21.
 */
public class CombingMeridianActivity extends BaseActivity implements View.OnClickListener {

    @Override

    public void setContentView() {
        setContentView(R.layout.combing_eridian_activity);

    }

    @Override
    public void initViews() {
        //初始化titleBar
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.combing_meridian_title), titleBar, 1);
    }

    @Override
    public void initDatas() {

    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            //一贴
            case R.id.one_attach:
                intent = new Intent(this, WebViewActivity.class);
                intent.putExtra("title", getString(R.string.combing_meridian_citie_prescription));
                intent.putExtra("type", "/member/service/view/fang/JLBS/1/");
                intent.putExtra("fangType", "yitei");
                startActivity(intent);
                break;
            //一砭
            case R.id.one_bian:
                intent = new Intent(this, WebViewActivity.class);
                intent.putExtra("title", getString(R.string.combing_meridian_bianfa_prescription));
                intent.putExtra("type", "/member/service/view/fang/JLBS/1/");
                intent.putExtra("fangType", "yishan");
                startActivity(intent);
                break;
            //一刮
            case R.id.one_scrape:
                intent = new Intent(this, WebViewActivity.class);
                intent.putExtra("title", getString(R.string.combing_meridian_guasha_prescription));
                intent.putExtra("type", "/member/service/view/fang/JLBS/1/");
                intent.putExtra("fangType", "yigua");
                startActivity(intent);
                break;
            //一听
            case R.id.one_hear:
                intent = new Intent(this, WebViewActivity.class);
                intent.putExtra("title", getString(R.string.combing_meridian_music_prescription));
                intent.putExtra("type", "/member/service/view/fang/JLBS/1/");
                intent.putExtra("fangType", "yiting");
                startActivity(intent);
                break;
            //一灸
            case R.id.one_jiu:
                intent = new Intent(this, WebViewActivity.class);
                intent.putExtra("title", getString(R.string.combing_meridian_jiufa_prescription));
                intent.putExtra("type", "/member/service/view/fang/JLBS/1/");
                intent.putExtra("fangType", "yijiu");
                startActivity(intent);
                break;
            //一推
            case R.id.one_push:
                intent = new Intent(this, WebViewActivity.class);
                intent.putExtra("title", getString(R.string.combing_meridian_massage_prescription));
                intent.putExtra("type", "/member/service/view/fang/JLBS/1/");
                intent.putExtra("fangType", "yitui");
                startActivity(intent);
                break;
            //一戴
            case R.id.one_wear:
                intent = new Intent(this, WebViewActivity.class);
                intent.putExtra("title", getString(R.string.combing_meridian_ear_prescription));
                intent.putExtra("type", "/member/service/view/fang/JLBS/1/");
                intent.putExtra("fangType", "yidai");
                startActivity(intent);
                break;
            //一选
            case R.id.one_select:
                intent = new Intent(this, WebViewActivity.class);
                intent.putExtra("title", getString(R.string.combing_meridian_food_prescription));
                intent.putExtra("type", "/member/service/view/fang/JLBS/1/");
                intent.putExtra("fangType", "yixuan");
                startActivity(intent);
                break;
            //一站
            case R.id.one_station:
                intent = new Intent(this, WebViewActivity.class);
                intent.putExtra("title", getString(R.string.combing_meridian_sport_prescription));
                intent.putExtra("type", "/member/service/view/fang/JLBS/1/");
                intent.putExtra("fangType", "yizhan");
                startActivity(intent);
                break;
            default:
                break;

        }

    }
}
