package com.hxlm.hcyphone.harmonypackage;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;

/**
 * Created by lipengcheng on 2017/6/21.
 */
public class HelphospitalActivity extends BaseActivity implements View.OnClickListener/* ,OnCompleteListener*/ {
    private TextView tvMobile;

//    private ImageView ivDoctorImageTwo;
//    private ImageView ivDoctorImageOne;
//    private TextView tvDoctorNameOne;
//    private TextView tvDoctorNameTwo;
//    private TextView tvDoctorZheChengOne;
//    private TextView tvDoctorZheChengTwo;
//    private TextView tvDoctorKeShiOne;
//    private TextView tvDoctorKeShiTwo;
//    private TextView tvDoctorHospitalOne;
//    private TextView tvDoctorHospitalTwo;
//    private HelpHospitalManager helpHospitalManager;
//    private int memberChildId;
//    private AbstractDefaultHttpHandlerCallback callback;
//    private ImageView ivAppointmenRegister;
//    private ImageView ivFamousDoctor;

    @Override

    public void setContentView() {
        setContentView(R.layout.help_hospital_activity);

    }

    @Override
    public void initViews() {
        //初始化titleBar
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.home_auxiliary_treatment), titleBar, 1);

        tvMobile = findViewById(R.id.tv_help_hospital_mobile);
        tvMobile.setOnClickListener(this);

//        ivDoctorImageOne = (ImageView) findViewById(R.id.iv_doctor_image_one);
//        ivDoctorImageTwo = (ImageView) findViewById(R.id.iv_doctor_image_two);
//        tvDoctorNameOne = (TextView) findViewById(R.id.tv_doctor_name_one);
//        tvDoctorNameTwo = (TextView) findViewById(R.id.tv_doctor_name_two);
//        tvDoctorZheChengOne = (TextView) findViewById(R.id.tv_doctor_zhicheng_one);
//        tvDoctorZheChengTwo = (TextView) findViewById(R.id.tv_doctor_zhicheng_two);
//        tvDoctorKeShiOne = (TextView) findViewById(R.id.tv_doctor_keshi_one);
//        tvDoctorKeShiTwo = (TextView) findViewById(R.id.tv_doctor_keshi_two);
//        tvDoctorHospitalOne = (TextView) findViewById(R.id.tv_doctor_hospital_one);
//        tvDoctorHospitalTwo = (TextView) findViewById(R.id.tv_doctor_hospital_two);
//
//        ivAppointmenRegister = (ImageView) findViewById(R.id.appointmen_register);
//        ivAppointmenRegister.setOnClickListener(this);
//        ivFamousDoctor = (ImageView) findViewById(R.id.famous_doctor);
//        ivFamousDoctor.setOnClickListener(this);

    }

    @Override
    public void initDatas() {
//        fetchChildMemberId();
//        helpHospitalManager = new HelpHospitalManager();
//        callback = new AbstractDefaultHttpHandlerCallback(HelphospitalActivity.this) {
//            @Override
//            protected void onResponseSuccess(Object obj) {
//                List<Doctor> doctors = (List<Doctor>) obj;
//                if (doctors != null && doctors.size() > 0) {
//                    handleResponseSuggestDoctor(doctors);
//                }
//            }
//        };
//        helpHospitalManager.getSuggestDoctor(memberChildId, callback);


    }

//    private void handleResponseSuggestDoctor(List<Doctor> doctors) {
//        ImageLoader.getInstance().displayImage(doctors.get(0).getImageUrl(), ivDoctorImageOne);
//        ImageLoader.getInstance().displayImage(doctors.get(1).getImageUrl(), ivDoctorImageTwo);
//        tvDoctorNameOne.setText(doctors.get(0).getImageUrl());
//        tvDoctorNameTwo.setText(doctors.get(1).getImageUrl());
//        tvDoctorKeShiOne.setText(doctors.get(0).getKeShi());
//        tvDoctorKeShiTwo.setText(doctors.get(1).getKeShi());
//        tvDoctorZheChengOne.setText(doctors.get(0).getZhiCheng());
//        tvDoctorZheChengTwo.setText(doctors.get(1).getZhiCheng());
//        tvDoctorHospitalOne.setText(doctors.get(0).getHospital());
//        tvDoctorHospitalTwo.setText(doctors.get(1).getHospital());
//    }

    @Override
    public void onClick(View v) {
//        Intent intent = null;
        switch (v.getId()) {
            case R.id.tv_help_hospital_mobile:
                Intent intent1 = new Intent();
                intent1.setAction(Intent.ACTION_DIAL);//表示打开拨打电话窗口，但还未拨出电话
                intent1.setData(Uri.parse("tel:" + "4006776668"));
                startActivity(intent1);
                break;
//            case R.id.appointmen_register:
//                intent = new Intent(this, ReserveActivity.class);
//                intent.putExtra("isImportant", 0);
//                startActivity(intent);
//                break;
//                //名医导诊 （专病专科）
//            case R.id.famous_doctor:
//                intent = new Intent(this, ReserveActivity.class);
//                intent.putExtra("isImportant", 1);
//                startActivity(intent);
//                break;
        }

    }
    /**
     * 获取选择的用户id;
     */
//    private void fetchChildMemberId() {
//        ChildMember choosedChildMember = LoginControllor.getChoosedChildMember();
//        if (choosedChildMember == null) {
//            memberChildId = LoginControllor.getLoginMember().getMengberchild().get(0).getId();
//        } else {
//            memberChildId = choosedChildMember.getId();
//        }
//    }
//    /**
//     * TitleBar选择用户回调
//     * 用于完成用户切换时拉取信息
//     */
//    @Override
//    public void onComplete() {
//        fetchChildMemberId();
//        helpHospitalManager.getSuggestDoctor(memberChildId, callback);
//    }
}
