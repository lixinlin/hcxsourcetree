package com.hxlm.hcyphone.bean;

/**
 * Created by lixinlin on 2017/6/22.
 */
public class Doctor {
    private String name;
    private String imageUrl;
    private String keShi;
    private String zhiCheng;
    private String hospital;

    public Doctor() {
    }

    public Doctor(String name, String imageUrl, String keShi, String zhiCheng, String hospital) {
        this.name = name;
        this.imageUrl = imageUrl;
        this.keShi = keShi;
        this.zhiCheng = zhiCheng;
        this.hospital = hospital;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getKeShi() {
        return keShi;
    }

    public void setKeShi(String keShi) {
        this.keShi = keShi;
    }

    public String getZhiCheng() {
        return zhiCheng;
    }

    public void setZhiCheng(String zhiCheng) {
        this.zhiCheng = zhiCheng;
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "name='" + name + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", keShi='" + keShi + '\'' +
                ", zhiCheng='" + zhiCheng + '\'' +
                ", hospital='" + hospital + '\'' +
                '}';
    }
}
