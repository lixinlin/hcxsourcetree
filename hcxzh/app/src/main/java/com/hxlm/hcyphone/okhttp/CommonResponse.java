package com.hxlm.hcyphone.okhttp;


/**
 * Created by JQ on 2017/7/21.
 */
public class CommonResponse {

//    @SerializedName("errorCode")
    private String errorCode;

//    @SerializedName("errorCode")
    private String errorMsg;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

}
