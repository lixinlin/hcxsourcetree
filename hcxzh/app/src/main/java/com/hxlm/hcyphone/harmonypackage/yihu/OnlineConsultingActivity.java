package com.hxlm.hcyphone.harmonypackage.yihu;

import android.view.View;

import com.bigman.wmzx.customcardview.library.CardView;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.Constant;

/**
 * @author Lirong
 * @date 2018/12/4 0004.
 * @description 一呼
 */

public class OnlineConsultingActivity extends BaseActivity{

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_online_consulting);
//        实际使用的是CallSelectorAct
    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.yh_online_resule_text), titleBar, 1);

        CardView card_chengren = findViewById(R.id.card_chengren);
        CardView card_child = findViewById(R.id.card_child);
        if (Constant.isEnglish){
            card_chengren.setVisibility(View.GONE);
            card_child.setVisibility(View.GONE);
        }else {
            card_chengren.setVisibility(View.VISIBLE);
            card_child.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void initDatas() {

    }

}
