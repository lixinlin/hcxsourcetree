package com.hxlm.database.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;

import com.hxlm.database.ICD10;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "ICD10".
*/
public class ICD10Dao extends AbstractDao<ICD10, Void> {

    public static final String TABLENAME = "ICD10";

    /**
     * Properties of entity ICD10.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property Rowid = new Property(0, Long.class, "rowid", false, "ROWID");
        public final static Property SEX = new Property(1, int.class, "SEX", false, "SEX");
        public final static Property Content = new Property(2, String.class, "content", false, "CONTENT");
        public final static Property Content_en = new Property(3, String.class, "content_en", false, "CONTENT_EN");
        public final static Property MICD = new Property(4, String.class, "MICD", false, "MICD");
        public final static Property MTJI = new Property(5, String.class, "MTJI", false, "MTJI");
    }


    public ICD10Dao(DaoConfig config) {
        super(config);
    }
    
    public ICD10Dao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, ICD10 entity) {
        stmt.clearBindings();
 
        Long rowid = entity.getRowid();
        if (rowid != null) {
            stmt.bindLong(1, rowid);
        }
        stmt.bindLong(2, entity.getSEX());
 
        String content = entity.getContent();
        if (content != null) {
            stmt.bindString(3, content);
        }
 
        String content_en = entity.getContent_en();
        if (content_en != null) {
            stmt.bindString(4, content_en);
        }
 
        String MICD = entity.getMICD();
        if (MICD != null) {
            stmt.bindString(5, MICD);
        }
 
        String MTJI = entity.getMTJI();
        if (MTJI != null) {
            stmt.bindString(6, MTJI);
        }
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, ICD10 entity) {
        stmt.clearBindings();
 
        Long rowid = entity.getRowid();
        if (rowid != null) {
            stmt.bindLong(1, rowid);
        }
        stmt.bindLong(2, entity.getSEX());
 
        String content = entity.getContent();
        if (content != null) {
            stmt.bindString(3, content);
        }
 
        String content_en = entity.getContent_en();
        if (content_en != null) {
            stmt.bindString(4, content_en);
        }
 
        String MICD = entity.getMICD();
        if (MICD != null) {
            stmt.bindString(5, MICD);
        }
 
        String MTJI = entity.getMTJI();
        if (MTJI != null) {
            stmt.bindString(6, MTJI);
        }
    }

    @Override
    public Void readKey(Cursor cursor, int offset) {
        return null;
    }    

    @Override
    public ICD10 readEntity(Cursor cursor, int offset) {
        ICD10 entity = new ICD10( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // rowid
            cursor.getInt(offset + 1), // SEX
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // content
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // content_en
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // MICD
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5) // MTJI
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, ICD10 entity, int offset) {
        entity.setRowid(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setSEX(cursor.getInt(offset + 1));
        entity.setContent(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setContent_en(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setMICD(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setMTJI(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
     }
    
    @Override
    protected final Void updateKeyAfterInsert(ICD10 entity, long rowId) {
        // Unsupported or missing PK type
        return null;
    }
    
    @Override
    public Void getKey(ICD10 entity) {
        return null;
    }

    @Override
    public boolean hasKey(ICD10 entity) {
        // TODO
        return false;
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
}
