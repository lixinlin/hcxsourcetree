package com.hcy.ky3h.wxapi;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.hxlm.android.hcy.order.OtherPayActivity;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.PayFailureActivity;
import com.hxlm.hcyandroid.PaySuccessActivity;
import com.tencent.mm.sdk.constants.ConstantsAPI;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

import com.hcy.ky3h.R;
import net.sourceforge.simcpux.Constants;


//此类为支付回调类，必须放在包名下 wxapi包下
public class WXPayEntryActivity extends Activity implements IWXAPIEventHandler {

    private static final String TAG = "MicroMsg.SDKSample.WXPayEntryActivity";

    private IWXAPI api;
    private int reserveType;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pay_result_callback);

        api = WXAPIFactory.createWXAPI(this, Constants.APP_ID);

        api.handleIntent(getIntent(), this);
        reserveType = new OtherPayActivity().getReserveType();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        api.handleIntent(intent, this);
    }


    @Override
    public void onReq(BaseReq baseReq) {

    }

    //支付结果回调
    @Override
    public void onResp(final BaseResp resp) {
    	if(Constant.ISSHOW)
    	{
    		Log.i("Log.i", "支付成功支付成功支付成功支付成功支付成功支付成功");
    	}
    	

        if (resp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("支付结果");

            if (resp.errCode == 0) {
            	if(Constant.ISSHOW)
            	{
            		Log.i("Log.i", "支付成功支付成功支付成功支付成功支付成功支付成功");
            	}           	
                builder.setMessage("支付成功");
              
//        		DataManager.getInstance().createOrder(reserveType, orderId+"",
//						type, PaymentPluginId.PaymentPluginId_WX,
//						cardFees, balance, false, null, null,
//						op, op.getAbstractHttpHandler());
                Intent intent = new Intent(WXPayEntryActivity.this, PaySuccessActivity.class);
                startActivity(intent);
                Constant.LIST.clear();
                
                
            } else if (resp.errCode == -1) {
                //可能的原因：签名错误、未注册APPID、项目设置APPID不正确、注册的APPID与设置的不匹配、其他异常等
                builder.setMessage("支付失败");
                Intent intent = new Intent(WXPayEntryActivity.this, PayFailureActivity.class);
                startActivity(intent);
            } else {
                // 	无需处理。发生场景：用户不支付了，点击取消，返回APP。
                builder.setMessage("您取消了支付");
            }
            builder.setNegativeButton("关闭", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                            if((resp.errCode) == 0){
                            	Intent i = new Intent(WXPayEntryActivity.this,PaySuccessActivity.class);
            					i.putExtra("reserveType", reserveType);
            					startActivity(i);
                            }else {
                            	Intent i= new Intent(WXPayEntryActivity.this,PayFailureActivity.class);
                            	i.putExtra("reserveType", reserveType);
            					startActivity(i);
                            }
                        }
                    }

            );
            builder.show();

        }
    }
}