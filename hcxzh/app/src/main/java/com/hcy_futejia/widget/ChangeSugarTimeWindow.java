package com.hcy_futejia.widget;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hcy_futejia.widget.wheelview.OnWheelChangedListener;
import com.hcy_futejia.widget.wheelview.OnWheelScrollListener;
import com.hcy_futejia.widget.wheelview.WheelView;
import com.hcy_futejia.widget.wheelview.adapters.AbstractWheelTextAdapter1;
import com.hhmedic.android.sdk.module.gesturesview.commons.FinderView;

import java.util.ArrayList;

/**
 * @author Lirong
 * @date 2019/4/29.
 * @description
 */

public class ChangeSugarTimeWindow extends PopupWindow implements View.OnClickListener  {

    private Context context;

    private WheelView wvTime;
    private final TextView btn_birth_cancel;
    private final TextView btn_birth_finish;

    private String strTime;
    private int currentItem = 0;

    private int maxTextSize = 18;
    private int minTextSize = 15;
    private ArrayList<String> arrayTime = new ArrayList<String>();

    private OnTimeListener onTimeListener;
    private final CalendarTextAdapter calendarTextAdapter;

    public void setOnTimeListener(OnTimeListener onTimeListener){
        this.onTimeListener = onTimeListener;
    }

    public interface OnTimeListener {
        void onClick(String time);
    }

    public void initData(){
        arrayTime.add(context.getString(R.string.sugar_lingchen));
        arrayTime.add(context.getString(R.string.sugar_zaocanqian));
        arrayTime.add(context.getString(R.string.sugar_zaocanhou));
        arrayTime.add(context.getString(R.string.sugar_wucanqian));
        arrayTime.add(context.getString(R.string.sugar_wucanhou));
        arrayTime.add(context.getString(R.string.sugar_wancanqian));
        arrayTime.add(context.getString(R.string.sugar_wancanhou));
        arrayTime.add(context.getString(R.string.sugar_shuiqian));
    }

    public ChangeSugarTimeWindow(Context context, String strTime1) {
        super(context);
        this.context = context;
        this.strTime = strTime1;
        View view= View.inflate(context, R.layout.dialog_change_sugar_time,null);
        wvTime = view.findViewById(R.id.wvTime);
        btn_birth_cancel = view.findViewById(R.id.btn_birth_cancel);
        btn_birth_finish = view.findViewById(R.id.btn_birth_finish);

        //设置SelectPicPopupWindow的View
        this.setContentView(view);
        //设置SelectPicPopupWindow弹出窗体的宽
        this.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        //设置SelectPicPopupWindow弹出窗体的高
        this.setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        //设置SelectPicPopupWindow弹出窗体可点击
        this.setFocusable(true);
        //设置SelectPicPopupWindow弹出窗体动画效果
//		this.setAnimationStyle(R.style.AnimBottom);
        //实例化一个ColorDrawable颜色为半透明
        ColorDrawable dw = new ColorDrawable(0xb0000000);
        //设置SelectPicPopupWindow弹出窗体的背景
        this.setBackgroundDrawable(dw);

        btn_birth_cancel.setOnClickListener(this);
        btn_birth_finish.setOnClickListener(this);

        initData();
        for (int i = 0; i < arrayTime.size(); i++) {
            String s = arrayTime.get(i);
            if (strTime.equals(s)){
                currentItem = i;
            }
        }

        calendarTextAdapter = new CalendarTextAdapter(context, arrayTime, currentItem, maxTextSize, minTextSize);
        wvTime.setVisibleItems(5);
        wvTime.setViewAdapter(calendarTextAdapter);
        wvTime.setCurrentItem(currentItem);

        wvTime.addChangingListener(new OnWheelChangedListener() {
            @Override
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                String currentText = (String) calendarTextAdapter.getItemText(wheel.getCurrentItem());
                strTime = currentText;
                setTextviewSize(currentText, calendarTextAdapter);
            }
        });

        wvTime.addScrollingListener(new OnWheelScrollListener() {
            @Override
            public void onScrollingStarted(WheelView wheel) {

            }

            @Override
            public void onScrollingFinished(WheelView wheel) {
                String currentText = (String) calendarTextAdapter.getItemText(wheel.getCurrentItem());
                setTextviewSize(currentText, calendarTextAdapter);
            }
        });

    }

    @Override
    public void onClick(View v) {
        if (v == btn_birth_finish){
            if (onTimeListener != null){
                onTimeListener.onClick(strTime);
            }
        }else {
            dismiss();
        }
        dismiss();
    }

    /**
     * 设置字体大小
     *
     * @param curriteItemText
     * @param adapter
     */
    public void setTextviewSize(String curriteItemText,CalendarTextAdapter adapter) {
        ArrayList<View> arrayList = adapter.getTestViews();
        int size = arrayList.size();
        String currentText;
        for (int i = 0; i < size; i++) {
            TextView textvew = (TextView) arrayList.get(i);
            currentText = textvew.getText().toString();
            if (curriteItemText.equals(currentText)) {
                textvew.setTextSize(maxTextSize);
//				textvew.setTextColor(Color.parseColor("#ffa200"));
            } else {
                textvew.setTextSize(minTextSize);
//				textvew.setTextColor(Color.parseColor("#000000"));
            }
        }
    }

    private class CalendarTextAdapter extends AbstractWheelTextAdapter1 {
        ArrayList<String> list;

        protected CalendarTextAdapter(Context context, ArrayList<String> list, int currentItem, int maxsize, int minsize) {
            super(context, R.layout.item_birth_year, NO_RESOURCE, currentItem, maxsize, minsize);
            this.list = list;
            setItemTextResource(R.id.tempValue);
        }

        @Override
        public View getItem(int index, View cachedView, ViewGroup parent) {
            View view = super.getItem(index, cachedView, parent);
            TextView tv = view.findViewById(R.id.tempValue);
            setTextviewSize(tv.getText().toString(),this);
            return view;

        }

        @Override
        public int getItemsCount() {
            return list.size();
        }

        @Override
        protected CharSequence getItemText(int index) {
            return list.get(index) + "";
        }
    }

}
