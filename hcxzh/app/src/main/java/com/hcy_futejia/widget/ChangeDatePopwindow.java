package com.hcy_futejia.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hcy_futejia.widget.wheelview.OnWheelChangedListener;
import com.hcy_futejia.widget.wheelview.OnWheelScrollListener;
import com.hcy_futejia.widget.wheelview.WheelView;
import com.hcy_futejia.widget.wheelview.adapters.AbstractWheelTextAdapter1;
import com.hxlm.hcyandroid.Constant;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

/**
 * @author :  Chen.yuan
 * Email:   hubeiqiyuan2010@163.com
 * Date:    2016/7/28 17:37
 * Description:日期选择window
 */
public class ChangeDatePopwindow extends PopupWindow implements View.OnClickListener {

	private Context context;
	private WheelView wvYear;
	private WheelView wvMonth;
	private WheelView wvDay;

	private TextView btnSure;
	private TextView btnCancel;

	private ArrayList<String> arry_years = new ArrayList<String>();
	private ArrayList<String> arry_months = new ArrayList<String>();
	private ArrayList<String> arry_days = new ArrayList<String>();
	private CalendarTextAdapter mYearAdapter;
	private CalendarTextAdapter mMonthAdapter;
	private CalendarTextAdapter mDaydapter;

	private String month;
	private String day;

	private String currentYear = getYear();
	private String currentMonth = getMonth();
	private String currentDay = getDay();

	private int maxTextSize = 16;
	private int minTextSize = 14;

	private boolean issetdata = false;

	private String selectYear;
	private String selectMonth;
	private String selectDay;

	private OnBirthListener onBirthListener;
	private String y;
	private String m;
	private String d;

	public ChangeDatePopwindow(final Context context) {
		super(context);
		this.context = context;
		View view= View.inflate(context, R.layout.dialog_change_birth,null);
		wvYear =  view.findViewById(R.id.wv_birth_year);
		wvMonth =  view.findViewById(R.id.wv_birth_month);
		wvDay =  view.findViewById(R.id.wv_birth_day);
		btnSure = view.findViewById(R.id.btn_birth_finish);
		btnCancel =  view.findViewById(R.id.btn_birth_cancel);

		//设置SelectPicPopupWindow的View
		this.setContentView(view);
		//设置SelectPicPopupWindow弹出窗体的宽
		this.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
		//设置SelectPicPopupWindow弹出窗体的高
		this.setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
		//设置SelectPicPopupWindow弹出窗体可点击
		this.setFocusable(true);
		//设置SelectPicPopupWindow弹出窗体动画效果
//		this.setAnimationStyle(R.style.AnimBottom);
		//实例化一个ColorDrawable颜色为半透明
		ColorDrawable dw = new ColorDrawable(0xb0000000);
		//设置SelectPicPopupWindow弹出窗体的背景
		this.setBackgroundDrawable(dw);

		btnSure.setOnClickListener(this);
		btnCancel.setOnClickListener(this);

		if (!issetdata) {
			initData();
		}

		initYears();
//		Collections.sort(arry_years);
		mYearAdapter = new CalendarTextAdapter(context, arry_years, setYear(currentYear), maxTextSize, minTextSize);
		wvYear.setVisibleItems(5);
		wvYear.setViewAdapter(mYearAdapter);
		wvYear.setCurrentItem(setYear(currentYear));

		initMonths(Integer.parseInt(month));
		//setMonth(currentMonth)
		//currentText.length()==2?currentText.substring(0, 1):currentText.substring(0, 2)
		mMonthAdapter = new CalendarTextAdapter(context, arry_months, setMonth(currentMonth), maxTextSize, minTextSize);
		wvMonth.setVisibleItems(5);
		wvMonth.setViewAdapter(mMonthAdapter);
		wvMonth.setCurrentItem(setMonth(currentMonth));

//		if (currentYear.equals(getYear()) && currentMonth.equals(getMonth())){
//			initDays(Integer.parseInt(getDay()));
//		}else {
			initDays(Integer.parseInt(day));
//		}
		mDaydapter = new CalendarTextAdapter(context, arry_days, Integer.parseInt(currentDay) - 1, maxTextSize, minTextSize);
		wvDay.setVisibleItems(5);
		wvDay.setViewAdapter(mDaydapter);
		wvDay.setCurrentItem(Integer.parseInt(currentDay) - 1);

		wvYear.addChangingListener(new OnWheelChangedListener() {

			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				// TODO Auto-generated method stub
				String currentText = (String) mYearAdapter.getItemText(wheel.getCurrentItem());
				selectYear = currentText;
//				currentYear = currentText.substring(0, currentText.length()-1).toString();
				currentYear = currentText.substring(0,4).toString();

				setYear(currentYear);
				initMonths(Integer.parseInt(month));
				mMonthAdapter = new CalendarTextAdapter(context, arry_months, wvMonth.getCurrentItem(), maxTextSize, minTextSize);
				wvMonth.setVisibleItems(5);
				wvMonth.setViewAdapter(mMonthAdapter);
				wvMonth.setCurrentItem(wvMonth.getCurrentItem());
				setTextviewSize(currentText, mYearAdapter);
				Log.d("currentYear==",currentYear+"==="+month);
				calDays(currentYear, month);
			}
		});

		wvYear.addScrollingListener(new OnWheelScrollListener() {

			@Override
			public void onScrollingStarted(WheelView wheel) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onScrollingFinished(WheelView wheel) {
				// TODO Auto-generated method stub
				String currentText = (String) mYearAdapter.getItemText(wheel.getCurrentItem());
				setTextviewSize(currentText, mYearAdapter);
			}
		});

		wvMonth.addChangingListener(new OnWheelChangedListener() {


			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				Log.e("retrofit","====月份改变监听===="+wheel.getCurrentItem()+"=="+setMonth(getMonth()));
				// TODO Auto-generated method stub
				String currentText = (String) mMonthAdapter.getItemText(wheel.getCurrentItem());
				selectMonth = currentText;
				setMonth(currentText.substring(0, 1));
//				if (currentYear.equals(getYear()) && month.equals(getMonth())){
//					initDays(Integer.parseInt(getDay()));
//				}else {
					initDays(Integer.parseInt(day));
//				}
				mDaydapter = new CalendarTextAdapter(context, arry_days, wvDay.getCurrentItem(), maxTextSize, minTextSize);
				wvDay.setVisibleItems(5);
				wvDay.setViewAdapter(mDaydapter);
				wvDay.setCurrentItem(wvDay.getCurrentItem());
				setTextviewSize(currentText, mMonthAdapter);
				calDays(currentYear, month);
			}
		});

		wvMonth.addScrollingListener(new OnWheelScrollListener() {

			@Override
			public void onScrollingStarted(WheelView wheel) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onScrollingFinished(WheelView wheel) {
				// TODO Auto-generated method stub
				String currentText = (String) mMonthAdapter.getItemText(wheel.getCurrentItem());
				setTextviewSize(currentText, mMonthAdapter);
			}
		});

		wvDay.addChangingListener(new OnWheelChangedListener() {

			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				// TODO Auto-generated method stub
				String currentText = (String) mDaydapter.getItemText(wheel.getCurrentItem());
				setTextviewSize(currentText, mDaydapter);
				selectDay = currentText;
			}
		});

		wvDay.addScrollingListener(new OnWheelScrollListener() {

			@Override
			public void onScrollingStarted(WheelView wheel) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onScrollingFinished(WheelView wheel) {
				// TODO Auto-generated method stub
				String currentText = (String) mDaydapter.getItemText(wheel.getCurrentItem());
				setTextviewSize(currentText, mDaydapter);
			}
		});
	}


	public void initYears() {
		for (int i = 1950; i <= Integer.parseInt(getYear()); i++) {
			arry_years.add(i + context.getString(R.string.dialog_nian));
		}
	}

	public void initMonths(int months) {
		arry_months.clear();
		for (int i = 1; i <= months; i++) {
			if (i<9){

			}
			arry_months.add(i + context.getString(R.string.dialog_yue));
		}
	}

	public void initDays(int days) {
		arry_days.clear();
		for (int i = 1; i <= days; i++) {
			arry_days.add(i + context.getString(R.string.dialog_ri));
		}
	}

	private class CalendarTextAdapter extends AbstractWheelTextAdapter1 {
		ArrayList<String> list;

		protected CalendarTextAdapter(Context context, ArrayList<String> list, int currentItem, int maxsize, int minsize) {
			super(context, R.layout.item_birth_year, NO_RESOURCE, currentItem, maxsize, minsize);
			this.list = list;
			setItemTextResource(R.id.tempValue);
		}

		@Override
		public View getItem(int index, View cachedView, ViewGroup parent) {
			View view = super.getItem(index, cachedView, parent);
			return view;

		}

		@Override
		public int getItemsCount() {
			return list.size();
		}

		@Override
		protected CharSequence getItemText(int index) {
			return list.get(index) + "";
		}
	}

	public void setBirthdayListener(OnBirthListener onBirthListener) {
		this.onBirthListener = onBirthListener;
	}

	@Override
	public void onClick(View v) {

		if (v == btnSure) {
			if (onBirthListener != null) {
				if (Constant.isEnglish){
					if (selectYear.length() > 4) {
						selectYear = selectYear.substring(0, 4);
					}
					if (selectMonth.length() > 2) {
						selectMonth = selectMonth.substring(0, selectMonth.length() - 5);
					}
					if (selectDay.length() > 2) {
						selectDay = selectDay.substring(0, selectDay.length() - 3);
					}
					String m1 = "";
					String d1 = "";
					if (Integer.parseInt(selectMonth) <10){
						m1 = "0"+selectMonth;
					}else{
						m1 = selectMonth;
					}
					if (Integer.parseInt(selectDay) < 10){
						d1 = "0" + selectDay;
					}else{
						d1 = selectDay;
					}
//					Log.e("cy","===year==="+selectYear+"; ===month===="+selectMonth+"; ====day==="+selectDay);
					onBirthListener.onClick(selectYear,m1,d1);
				}else {
					onBirthListener.onClick(selectYear, selectMonth, selectDay);
				}
				Log.d("cy",""+selectYear+""+selectMonth+""+selectDay);
			}
		} else if (v == btnSure) {

		}  else {
			dismiss();
		}
		dismiss();

	}

	public interface OnBirthListener {
		void onClick(String year, String month, String day);
	}

	/**
	 * 设置字体大小
	 *
	 * @param curriteItemText
	 * @param adapter
	 */
	public void setTextviewSize(String curriteItemText, CalendarTextAdapter adapter) {
		ArrayList<View> arrayList = adapter.getTestViews();
		int size = arrayList.size();
		String currentText;
		for (int i = 0; i < size; i++) {
			TextView textvew = (TextView) arrayList.get(i);
			currentText = textvew.getText().toString();
			if (curriteItemText.equals(currentText)) {
				textvew.setTextSize(maxTextSize);
			} else {
				textvew.setTextSize(minTextSize);
			}
		}
	}

	public String getYear() {
		Calendar c = Calendar.getInstance();
		return c.get(Calendar.YEAR)+"";
	}

	public String getMonth() {
		Calendar c = Calendar.getInstance();
		return c.get(Calendar.MONTH) + 1+"";
	}

	public String getDay() {
		Calendar c = Calendar.getInstance();
		return c.get(Calendar.DAY_OF_MONTH)+"";
	}

	public void initData() {
		setDate(getYear(), getMonth(), getDay());
		this.currentYear = getYear() +"";
		this.currentDay = getDay()+"";
		this.currentMonth = getMonth()+"";
	}

	/**
	 * 设置年月日
	 *
	 * @param year
	 * @param month
	 * @param day
	 */
	public void setDate(String year, String month, String day) {
		selectYear = year;
		selectMonth = month;
		selectDay = day ;
		issetdata = true;
		this.currentYear = year;
		this.currentMonth = month;
		this.currentDay = day;
		if (year == getYear()) {
			this.month = getMonth();
		} else {
			this.month = 12+"";
		}
		if (year == getYear() && month == getMonth()){
			this.day = getDay();
		}
		calDays(year, month);
	}

	/**
	 * 设置年份
	 *
	 * @param year
	 */
	public int setYear(String year) {
		int yearIndex = arry_years.size() - 1;
		if (!year.equals(getYear())) {
			this.month = 12+"";
		} else {
			this.month = getMonth();
		}
		for (int i = Integer.parseInt(getYear());i< 1950; i--) {
			if (i == Integer.parseInt(year)) {
				return yearIndex;
			}
			yearIndex++;
		}
		return yearIndex;
	}

	/**
	 * 设置月份
	 *
	 * @param month
	 * @param month
	 * @return
	 */
	public int setMonth(String month) {
		int monthIndex = 0;
		calDays(currentYear, month);
		for (int i = 1; i < Integer.parseInt(this.month); i++) {
			if (Integer.parseInt(month) == i) {
				return monthIndex;
			} else {
				monthIndex++;
			}
		}
		return monthIndex;
	}

	/**
	 * 计算每月多少天
	 *
	 * @param month
	 * @param year
	 */
	public void calDays(String year, String month) {
		boolean leayyear = false;
		leayyear = Integer.parseInt(year) % 4 == 0 && Integer.parseInt(year) % 100 != 0;
		for (int i = 1; i <= 12; i++) {
			switch (Integer.parseInt(month)) {
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					this.day = 31+"";
					break;
				case 2:
					if (leayyear) {
						this.day = 29+"";
					} else {
						this.day = 28+"";
					}
					break;
				case 4:
				case 6:
				case 9:
				case 11:
					this.day = 30+"";
					break;
				default:
					break;
			}
		}
		if (year.equals( getYear()) && month .equals( getMonth())) {
			Log.i("retrofit","====生日弹窗111====="+year+"==="+getYear() +"==="+month+"==="+getMonth()+"==="+getDay());
			this.day = getDay();
		}

	}
}