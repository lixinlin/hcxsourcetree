package com.hcy_futejia.manager;

import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hxlm.android.hcy.asynchttp.AbstractHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.asynchttp.HcyHttpResponseHandler;
import com.hxlm.android.hcy.user.ChildMember;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.user.Member;
import com.hxlm.hcyandroid.util.Md5Util;
import com.loopj.android.http.RequestParams;

import java.util.List;

/**
 * @author Lirong
 * @date 2019/7/17.
 * @description
 */

public class UserEnglishManager {

    /**
     * 获取邮箱验证码
     *
     * @param email 邮箱
     */
    public void getEmailCodeCreate(String email, final AbstractHttpHandlerCallback callback) {
        String url = "/login/send/email.jhtml";
        RequestParams params = new RequestParams();
        params.put("email", email);
        params.put("type", 0);
        params.put("token", Md5Util.stringMD5(email+"ky3h.com"));

        HcyHttpClient.submit(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                Log.d("duanxin1", "contentParse: "+content);
                return content;
            }
        });
    }

    public void getEmailCodeChangePwd(String email, final AbstractHttpHandlerCallback callback) {
        String url = "/login/send/email.jhtml";
        RequestParams params = new RequestParams();
        params.put("email", email);
        params.put("type", 1);
        params.put("token", Md5Util.stringMD5(email+"ky3h.com"));

        HcyHttpClient.submit(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                Log.d("duanxin1", "contentParse: "+content);
                return content;
            }
        });
    }

    /**
     * 获取获取外国手机号验证码，注册
     *
     * @param phone 手机号
     */
    public void getPhoneCodeCreate(String phone, final AbstractHttpHandlerCallback callback) {
        String url = "/login/send/phone.jhtml";
        RequestParams params = new RequestParams();
        params.put("phone", phone);
        params.put("type", 0);
        params.put("token", Md5Util.stringMD5(phone+"ky3h.com"));

        HcyHttpClient.submit(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                Log.d("duanxin2", "contentParse: "+content);
                return content;
            }
        });
    }

    /**
     * 获取获取外国手机号验证码，修改密码
     *
     * @param phone 手机号
     */
    public void getPhoneCodeChangePwd(String phone, final AbstractHttpHandlerCallback callback) {
        String url = "/login/send/phone.jhtml";
        RequestParams params = new RequestParams();
        params.put("phone", phone);
        params.put("type", 1);
        params.put("token", Md5Util.stringMD5(phone+"ky3h.com"));

        HcyHttpClient.submit(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                Log.d("duanxin2", "contentParse: "+content);
                return content;
            }
        });
    }

    /**
     * 手机号注册
     * @param mobile  手机号
     * @param pwd  密码
     * @param code  验证码
     * @param callback
     */
    public void getPhoneLogin(String mobile, String pwd ,String code,AbstractHttpHandlerCallback callback){
        String url = "/login/register/phone.jhtml";
        RequestParams params = new RequestParams();
        params.put("phone",mobile);
        params.put("password",pwd);
        params.put("sex","");
        params.put("code",code);
        HcyHttpClient.submit(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {

                return content;
            }
        });
    }

    /**
     * 邮箱注册
     * @param email 邮箱
     * @param pwd  密码
     * @param code  验证码
     * @param callback
     */
    public void getEmailLogin(String email, String pwd ,String code,AbstractHttpHandlerCallback callback){
        String url = "/login/register/email.jhtml?";
        RequestParams params = new RequestParams();
        params.put("email",email);
        params.put("password",pwd);
        params.put("sex","");
        params.put("code",code);
        HcyHttpClient.submit(HcyHttpClient.METHOD_POST, url, params, new HcyHttpResponseHandler(callback) {
            @Override
            protected Object contentParse(String content) {
                return content;
            }
        });
    }

}
