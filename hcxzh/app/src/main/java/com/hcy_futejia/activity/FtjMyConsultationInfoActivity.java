package com.hcy_futejia.activity;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.bean.MyConsultation;
import com.hxlm.hcyandroid.bean.ReplyUserConsultation;
import com.hxlm.hcyandroid.view.MyGridView;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author Administrator
 */
public class FtjMyConsultationInfoActivity extends BaseActivity {

    private TextView tv_question_time;
    private TextView tv_question_content;
    private TextView tv_answer_time;
    private TextView tv_answer_content;
//    private TextView tv_doctor_answer;
    private MyGridView gv_images;
    private List<String> urls;
    private RelativeLayout rl_answer;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_ftj_my_consultation_info);
    }

    @Override
    public void initViews() {

        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.tuwen_title), titleBar, 1);
        tv_question_time = (TextView) findViewById(R.id.tv_question_time);
        tv_question_content = (TextView) findViewById(R.id.tv_question_content);
        tv_answer_time = (TextView) findViewById(R.id.tv_answer_time);
        tv_answer_content = (TextView) findViewById(R.id.tv_answer_content);
        rl_answer = findViewById(R.id.rl_answer);
//        tv_doctor_answer = (TextView) findViewById(R.id.tv_doctor_answer);
        gv_images = (MyGridView) findViewById(R.id.gv_images);

    }

    @Override
    public void initDatas() {
        MyConsultation myConsultation = (MyConsultation) getIntent()
                .getSerializableExtra("myConsultation");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        if (Constant.ISSHOW)
            Log.i("Log.i", "myConsultation = " + format.format(new Date(myConsultation
                    .getCreateDate())));
        tv_question_time.setText(format.format(new Date(myConsultation
                .getCreateDate())));
        tv_question_content.setText(myConsultation.getContent());
        urls = myConsultation.getUserConsultationImages();
        if (urls.size() != 0) {
            gv_images.setVisibility(View.VISIBLE);
            gv_images.setAdapter(new ConsultationImageAdapter(this, urls));
            gv_images.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    //点击展示大图
                    String url = (String)parent.getItemAtPosition(position);
                    Intent intent = new Intent(FtjMyConsultationInfoActivity.this, BigPhotoActivity.class);
                    intent.putExtra("photo",url);
                    startActivity(intent);
                }
            });
        }else{
            gv_images.setVisibility(View.GONE);
        }

        ReplyUserConsultation replyUserConsultation = myConsultation
                .getReplyUserConsultations();
        if (replyUserConsultation != null) {
            tv_answer_time.setText(format.format(replyUserConsultation
                    .getCreateDate()));
            tv_answer_content.setText(replyUserConsultation.getContent());
        } else {
            tv_answer_time.setVisibility(View.GONE);
            tv_answer_content.setVisibility(View.GONE);
            rl_answer.setVisibility(View.GONE);
//            tv_doctor_answer.setVisibility(View.GONE);
        }
    }

    class ConsultationImageAdapter extends BaseAdapter {

        private LayoutInflater inflater;
        private List<String> imageUrls;
        private Context context;
        private Holder holder;

        public ConsultationImageAdapter(Context context, List<String> imageUrls) {
            super();
            this.imageUrls = imageUrls;
            this.context = context;
        }

        @Override
        public int getCount() {
            return imageUrls.size();
        }

        @Override
        public Object getItem(int position) {
            return imageUrls.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                inflater = LayoutInflater.from(context);
                holder = new Holder();
                convertView = inflater.inflate(R.layout.item_published_grida,
                        null);
                holder.item_grida_image = (ImageView) convertView
                        .findViewById(R.id.item_grida_image);
                convertView.setTag(holder);
            } else {
                holder = (Holder) convertView.getTag();
            }
            String url = imageUrls.get(position);
            holder.item_grida_image.setTag(url);
            if (holder.item_grida_image.getTag() != null
                    && holder.item_grida_image.getTag().equals(url)) {
                ImageLoader.getInstance().displayImage(url,
                        holder.item_grida_image);
            }
            return convertView;
        }
    }

    public static class Holder {
        public ImageView item_grida_image;
    }

}
