package com.hcy_futejia.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hcy_futejia.utils.DateUtil;
import com.hcy_futejia.widget.ChangeDatePopwindow;
import com.hcy_futejia.widget.ChangeTimePopwindow;
import com.hcy_futejia.widget.RulerView;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.report.UploadManager;
import com.hxlm.android.hcy.user.ChildMember;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.CheckedDataType;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.bean.BloodPressureData;
import com.hxlm.hcyandroid.tabbar.MyHealthFileBroadcast;
import com.hxlm.hcyandroid.util.ToastUtil;
import com.hxlm.hcyphone.MainActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Administrator
 */
public class FtjBloodPressureWriteActivity extends BaseActivity implements
        View.OnClickListener{
    private static final String TAG = "FtjBloodPressureWriteAc";
    private RulerView rv_diya;
    private LinearLayout ll_date;
    private TextView tv_date;
    private LinearLayout ll_time;
    private TextView tv_time;
    private RulerView rv_gao_ya;
    private RulerView rv_xinlv;
    private TextView tv_submit;
    private LinearLayout ll_blood_pressure_write;

    private String isNormal;// 是否正常
    private UploadManager uploadManager;
    private Context context;

    private String strssy;
    private String strszy;
    private String strmb;

    private int mYear;
    private int mMonth;
    private int mDay;

    private int mHour;
    private int mMinute;
    private int mApm;

    String pattern = "yyyy-MM-dd hh:mm";

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_ftj_blood_pressure_write);
        context = FtjBloodPressureWriteActivity.this;
    }

    @Override
    public void initViews() {

        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.xueyaluru), titleBar, 1);

        ll_blood_pressure_write = findViewById(R.id.ll_blood_pressure_write);
        ll_date = findViewById(R.id.ll_date);
        tv_date = findViewById(R.id.tv_date);
        ll_time = findViewById(R.id.ll_time);
        tv_time = findViewById(R.id.tv_time);
        tv_submit = findViewById(R.id.tv_submit);

        ll_date.setOnClickListener(this);
        ll_time.setOnClickListener(this);
        tv_submit.setOnClickListener(this);

        rv_gao_ya = findViewById(R.id.rv_gao_ya);
        rv_diya = findViewById(R.id.rv_di_ya);
        rv_xinlv = findViewById(R.id.rv_xinlv);

        Calendar calendar = Calendar.getInstance();
        mYear = calendar.get(Calendar.YEAR);
        mMonth = calendar.get(Calendar.MONTH)+1;
        mDay = calendar.get(Calendar.DAY_OF_MONTH);
        if (mMonth < 10 && mDay < 10){
            tv_date.setText(mYear+"/0"+mMonth+"/0"+mDay);
        }else if (mMonth < 10 && mDay >= 10){
            tv_date.setText(mYear+"/0"+mMonth+"/"+mDay);
        }else if (mDay < 10 && mMonth >= 10){
            tv_date.setText(mYear+"/"+mMonth+"/0"+mDay);
        }else{
            tv_date.setText(mYear+"/"+mMonth+"/"+mDay);
        }

        mHour = calendar.get(Calendar.HOUR);
        mMinute = calendar.get(Calendar.MINUTE);
        mApm = calendar.get(Calendar.AM_PM);
        if (mHour < 10 && mMinute < 10){
            tv_time.setText("0"+mHour + ":" + "0"+mMinute);
        }else if (mHour < 10 && mMinute >=10){
            tv_time.setText("0"+mHour + ":" + mMinute);
        }else if (mHour >= 10 && mMinute <10){
            tv_time.setText(mHour + ":" + "0"+mMinute);
        }else {
            tv_time.setText(mHour + ":" + mMinute);
        }

        rv_diya.setOnChooseResulterListener(new RulerView.OnChooseResulterListener() {
            @Override
            public void onEndResult(String result) {
                Log.d(TAG, "舒张压===onEndResult: "+result);
                strszy = result;
            }

            @Override
            public void onScrollResult(String result) {
                Log.d(TAG, "舒张压===onScrollResult: "+result);
                strszy = result;
            }
        });

        rv_gao_ya.setOnChooseResulterListener(new RulerView.OnChooseResulterListener() {
            @Override
            public void onEndResult(String result) {
                Log.d(TAG, "收缩压===onEndResult: "+result);
                strssy = result;
            }

            @Override
            public void onScrollResult(String result) {
                Log.d(TAG, "收缩压===onScrollResult: "+result);
                strssy = result;
            }
        });

        rv_xinlv.setOnChooseResulterListener(new RulerView.OnChooseResulterListener() {
            @Override
            public void onEndResult(String result) {
                Log.d(TAG, "心率===onEndResult: "+result);
                strmb = result;
            }

            @Override
            public void onScrollResult(String result) {
                strmb = result;
                Log.d(TAG, "心率===onScrollResult: "+result);
            }
        });
    }

    @Override
    public void initDatas() {
        uploadManager = new UploadManager();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            //日期
            case R.id.ll_date:
                ChangeDatePopwindow changeDatePopwindow = new ChangeDatePopwindow(FtjBloodPressureWriteActivity.this);
                changeDatePopwindow.setDate(""+mYear + "", ""+mMonth + "", ""+ mDay + "");
                changeDatePopwindow.showAtLocation(ll_blood_pressure_write, Gravity.BOTTOM, 0, 0);
                changeDatePopwindow.setBirthdayListener(new ChangeDatePopwindow.OnBirthListener() {
                    @Override
                    public void onClick(String year, String month, String day) {
                        String y = "";
                        String m = "";
                        String d = "";
                        String strDate = "";
                        if (Constant.isEnglish){
                            tv_date.setText(year+"/"+month+"/"+day);
                        }else {
                            if (year.equals("" + mYear) || month.equals("" + mMonth) || day.equals("" + mDay)) {
                                if (year.equals("" + mYear)) {
                                    y = year + "年";
                                } else {
                                    y = year;
                                }
                                if (month.equals("" + mMonth)) {
                                    m = month + "月";
                                } else {
                                    m = month;
                                }
                                if (day.equals("" + mDay)) {
                                    d = day + "日";
                                } else {
                                    d = day;
                                }
                                strDate = y + m + d;
                            } else {
                                strDate = year + month + day;
                            }
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日");
                            try {
                                Date parse = simpleDateFormat.parse(strDate);
                                SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy/MM/dd");
                                String format = simpleDateFormat1.format(parse);
                                tv_date.setText(format);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                });
                break;
            //时间
            case R.id.ll_time:
                ChangeTimePopwindow changeTimePopwindow = new ChangeTimePopwindow(FtjBloodPressureWriteActivity.this);
                changeTimePopwindow.setDate("" +mApm, ""+mHour + "", ""+ mMinute + "");
                changeTimePopwindow.showAtLocation(ll_blood_pressure_write, Gravity.BOTTOM, 0, 0);
                changeTimePopwindow.setBirthdayListener(new ChangeTimePopwindow.OnBirthListener() {
                    @Override
                    public void onClick(String year, String month, String day) {
                        tv_time.setText(month+":"+day);
                    }
                });
                break;
            //提交
            case R.id.tv_submit:
                if (TextUtils.isEmpty(strssy)) {
                    ToastUtil.invokeShortTimeToast(context, getString(R.string.bpw_tips_shousuoya_not_empty));
                } else if (TextUtils.isEmpty(strszy)) {
                    ToastUtil.invokeShortTimeToast(context, getString(R.string.bpw_tips_shuzhangya_not_empty));
                }else if (TextUtils.isEmpty(strmb)) {
                    ToastUtil.invokeShortTimeToast(context, getString(R.string.bpw_tips_maibo_not_empty));
                }else {
                    if (Integer.parseInt(strssy) > 250) {
                        ToastUtil.invokeShortTimeToast(context, getString(R.string.bpc_range_gaoya));
                    } else if (Integer.parseInt(strszy) > 150) {
                        ToastUtil.invokeShortTimeToast(context, getString(R.string.bpc_range_diya));
                    } else if (Integer.parseInt(strmb) > 150) {
                        ToastUtil.invokeShortTimeToast(context, getString(R.string.bpc_range_maibo));
                    } else {
                        // 高压收缩压
                        int intssy = Integer.parseInt(strssy);
                        // 低压舒张压
                        int intszy = Integer.parseInt(strszy);

                        if ((intssy >= 180 && intszy >= 110)
                                || (intssy >= 180 || intszy >= 110)) {
                            isNormal = getString(R.string.bpc_isNormal1);
                        } else if (((intssy >= 160 && intssy <= 179) && (intszy >= 100 && intszy <= 109))
                                || ((intssy >= 160 && intssy <= 179) || (intszy >= 100 && intszy <= 109))) {

                            isNormal = getString(R.string.bpc_isNormal2);
                        } else if (((intssy >= 140 && intssy <= 159) && (intszy >= 90 && intszy <= 99))
                                || ((intssy >= 140 && intssy <= 159) || (intszy >= 90 && intszy <= 99))) {

                            isNormal = getString(R.string.bpc_isNormal3);
                        } else if (((intssy >= 120 && intssy <= 139) && (intszy >= 80 && intszy <= 89))
                                || ((intssy >= 120 && intssy <= 139) || (intszy >= 80 && intszy <= 89))) {
                            isNormal = getString(R.string.bpc_isNormal4);
                        } else if ((intssy >= 90 && intssy < 120)
                                && (intszy >= 60 && intszy < 80)) {
                            isNormal = getString(R.string.bpc_isNormal5);
                        } else if ((intssy > 0 && intssy < 90)
                                && (intszy > 0 && intszy < 60)
                                || ((intssy > 0 && intssy < 90) || (intszy > 0 && intszy < 60))) {
                            isNormal = getString(R.string.bpc_isNormal6);
                        }

                        LoginControllor.requestLogin(FtjBloodPressureWriteActivity.this, new OnCompleteListener() {
                            @Override
                            public void onComplete() {
                                List<ChildMember> childMembers = LoginControllor.getLoginMember().getMengberchild();
                                if (true) {
                                    BloodPressureData bloodPressureData = new BloodPressureData();
                                    bloodPressureData.setHighPressure(strssy);
                                    bloodPressureData.setLowPressure(strszy);
                                    bloodPressureData.setPulse(strmb);

                                    String strDate = tv_date.getText().toString();
                                    String strTime = tv_time.getText().toString();
                                    long stringToDate = DateUtil.getStringToDate(strDate + " "+strTime, pattern);
                                    // 输入正常值才上传数据
                                    uploadManager.uploadCheckedData(CheckedDataType.BLOOD_PRESSURE, bloodPressureData, stringToDate,
                                            new AbstractDefaultHttpHandlerCallback(FtjBloodPressureWriteActivity.this) {
                                                @Override
                                                protected void onResponseSuccess(Object obj) {
                                                    new BloodPressureDialog(FtjBloodPressureWriteActivity.this, getString(R.string.bpw_dialog_text_dangqianmaibo) + strmb
                                                            + getString(R.string.bpc_unit_count), strssy + "mmHg", strszy + "mmHg", isNormal).show();

                                                }
                                            });

                                }

                            }
                        });
                    }
                }
                break;
            default:
                break;
        }
    }




    public class BloodPressureDialog extends AlertDialog implements
            View.OnClickListener {

        Context context;
        String mb;
        String ssy;
        String szy;
        String isnormal;
        String str_tishi = "";



        public BloodPressureDialog(Context context, String mb, String ssy,
                                   String szy, String isnormal) {
            super(context);
            this.context = context;
            this.mb = mb;
            this.ssy = ssy;
            this.szy = szy;
            this.isnormal = isnormal;

        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.bloodpressure_submit_prompt);

            TextView tv_breath_prompt_tishi_mb = (TextView) findViewById(R.id.tv_breath_prompt_tishi_mb);
            tv_breath_prompt_tishi_mb.setText(mb);

            TextView tv_breath_prompt_tishi_ssy = (TextView) findViewById(R.id.tv_breath_prompt_tishi_ssy);
            tv_breath_prompt_tishi_ssy.setText(ssy);

            TextView tv_breath_prompt_tishi_szy = (TextView) findViewById(R.id.tv_breath_prompt_tishi_szy);
            tv_breath_prompt_tishi_szy.setText(szy);

            // 血压
            TextView tv_is_normal = (TextView) findViewById(R.id.tv_is_normal);
            //tv_is_normal.setText("脉搏：60－100次/分");

            //显示高压范围
            TextView text_gaoya=(TextView)findViewById(R.id.text_gaoya);
            text_gaoya.setText("90 < "+getString(R.string.bpw_text_range_gaoya)+"< 140");
            //显示低压范围
            TextView text_diya=(TextView)findViewById(R.id.text_diya);
            text_diya.setText("60 <"+getString(R.string.bpw_text_range_gaoya)+"< 90");


            // 返回检测
            TextView text_back = (TextView) findViewById(R.id.text_back);
            text_back.setOnClickListener(this);

            //查看档案
            TextView text_see_dangan=(TextView) findViewById(R.id.text_see_dangan);
            text_see_dangan.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                // 返回检测
                case R.id.text_back:
                    this.dismiss();
                    FtjBloodPressureWriteActivity.this.finish();
                    break;
                //查看档案
                case R.id.text_see_dangan:
                    this.dismiss();
                    // 动态注册广播使用隐士Intent
                    Intent intent=new Intent(MyHealthFileBroadcast.ACTION);
                    intent.putExtra("ArchivesFragment", "3");
                    intent.putExtra("otherReport", "true");
                    intent.putExtra("aaa","123");
                    intent.putExtra("Jump",6);
                    intent.putExtra("tag","blood_pressure");
                    FtjBloodPressureWriteActivity.this.sendBroadcast(intent);

                    Intent intent2 = new Intent(FtjBloodPressureWriteActivity.this, MainActivity.class);
//                    intent2.putExtra("mainId",2);
                    intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//清除MainActivity之前所有的activity
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP); //沿用之前的MainActivity
                    startActivity(intent2);
                    break;
                default:
                    break;

            }
        }

    }

}
