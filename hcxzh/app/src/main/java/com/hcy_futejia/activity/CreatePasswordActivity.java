package com.hcy_futejia.activity;

import android.content.Intent;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hcy_futejia.manager.UserEnglishManager;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.utils.SpUtils;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.BaseApplication;
import com.hxlm.hcyandroid.util.IsMobile;
import com.hxlm.hcyandroid.util.StatusBarUtils;
import com.hxlm.hcyandroid.util.ToastUtil;
import com.hxlm.hcyandroid.view.ContainsEmojiEditText;

/**
 * @author Administrator
 */
public class CreatePasswordActivity extends BaseActivity implements View.OnClickListener{

    private ContainsEmojiEditText et_pwd;
    private Button bt_next;
    private UserEnglishManager userEnglishManager;
    private int userType;
    private TextView tv_have_account;

    @Override
    public void setContentView() {
        StatusBarUtils.setWindowStatusBarColor(this, R.color.white);
        setContentView(R.layout.activity_create_passward);
    }

    @Override
    public void initViews() {
        et_pwd = findViewById(R.id.et_pwd);
        bt_next = findViewById(R.id.bt_next);
        tv_have_account = findViewById(R.id.tv_have_account);

        bt_next.setOnClickListener(this);
        tv_have_account.setOnClickListener(this);

        et_pwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s == null || s.length() == 0) {
                    bt_next.setBackgroundResource(R.drawable.yuanjiaojuxing);
                } else {
                    bt_next.setBackgroundResource(R.drawable.yuanjiaojuxing_login_blue);
                }
            }
        });
    }

    @Override
    public void initDatas() {


        userEnglishManager = new UserEnglishManager();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bt_next:
                String pwd = et_pwd.getText().toString().trim();
                if(!TextUtils.isEmpty(pwd)) {
                    if (pwd.length() >= 6 && pwd.length() <= 20) {
                        //userType 1 发送手机验证码  2发送邮箱验证码
                        String createAccount = (String) SpUtils.get(CreatePasswordActivity.this, "createAccount", "");
                        Intent intent = getIntent();
                        userType = intent.getIntExtra("userType", 0);
                        if (userType == 1) {
                            userEnglishManager.getPhoneCodeCreate(createAccount, new AbstractDefaultHttpHandlerCallback(CreatePasswordActivity.this) {
                                @Override
                                protected void onResponseSuccess(Object obj) {
                                    Log.e("retrofit", "====外国手机号获取验证码接口===" + obj.toString());
                                    toVerifyCodeActivity(pwd, 1);
                                }
                            });
                        } else if (userType == 2) {
                            userEnglishManager.getEmailCodeCreate(createAccount, new AbstractDefaultHttpHandlerCallback(CreatePasswordActivity.this) {
                                @Override
                                protected void onResponseSuccess(Object obj) {
                                    Log.e("retrofit", "====邮箱获取验证码接口===" + obj.toString());
                                    toVerifyCodeActivity(pwd, 2);
                                }
                            });
                        }
                    }else{
                        ToastUtil.invokeShortTimeToast(CreatePasswordActivity.this, getString(R.string.input_password));
                    }

                }else{
                    ToastUtil.invokeShortTimeToast(CreatePasswordActivity.this, getString(R.string.forget_pwd_mima_empty));
                }
                break;
            case R.id.tv_have_account:
                Intent intent = new Intent(CreatePasswordActivity.this, EnglishLoginActivity.class);
                startActivity(intent);
                CreatePasswordActivity.this.finish();
                break;
            default:
                break;
        }
    }

    private void toVerifyCodeActivity(String pwd,int codeType) {
        SpUtils.put(CreatePasswordActivity.this,"createPwd",pwd);
        Intent intent = new Intent(CreatePasswordActivity.this, FtjVerifyCodeActivity.class);
        intent.putExtra("codeType",codeType);
        startActivity(intent);
        BaseApplication.addDestoryActivity(CreatePasswordActivity.this,"passwordActivity");
    }
}
