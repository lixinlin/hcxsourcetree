package com.hcy_futejia.activity;

import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hcy_futejia.adapter.DiseaseListLeftAdapter;
import com.hcy_futejia.adapter.DiseaseListRightAdapter;
import com.hcy_futejia.utils.SingleListSymple;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.user.ChildMember;
import com.hxlm.android.hcy.user.ChooseMemberDialog;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.BaseApplication;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.bean.ICDItem;
import com.hxlm.hcyandroid.bean.Symptom;
import com.hxlm.hcyandroid.tabbar.home.visceraidentity.VisceraIdentityReportActivity;
import com.hxlm.hcyandroid.util.ToastUtil;

import net.sourceforge.simcpux.Constants;

import java.util.ArrayList;
import java.util.List;

public class HpiActivity extends BaseActivity implements View.OnClickListener {

    private ListView lv_left;
    private ListView lv_right;
    private ImageView iv_disease_add;
    private TextView tv_hpi_submit;

    private DiseaseListLeftAdapter leftAdapter;// 左侧adapter
    private DiseaseListRightAdapter rightAdapter;// 右侧adapter
    private ArrayList<Symptom> list;
    private List<Symptom> list_left;// 左边的list
    private List<ICDItem> list_right = new ArrayList<ICDItem>();
    private TextView v_1,v_2;


    // 接口调用参数
    private String symptomStr = "";
    private String icds = "";
    private int level = 2;
    private int zxNum;
    private int disNum;
    private int sex;
    private boolean isMan;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_hpi);
    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.vdl_hpi), titleBar, 1);

        lv_left = findViewById(R.id.lv_left);
        lv_right = findViewById(R.id.lv_right);
        iv_disease_add = findViewById(R.id.iv_disease_add);
        tv_hpi_submit = findViewById(R.id.tv_hpi_submit);
        v_1 = findViewById(R.id.v_1);
        v_2 = findViewById(R.id.v_2);

        tv_hpi_submit.setOnClickListener(this);
        iv_disease_add.setOnClickListener(this);

        Intent intent = getIntent();
//        list_left = (ArrayList<Symptom>) intent.getSerializableExtra("list_left");
        list_left = SingleListSymple.getSingleListSymple().getSymptomList();
        list_right = (ArrayList<ICDItem>) intent.getSerializableExtra("list_right");
        isMan = intent.getBooleanExtra("isMan", true);

        if (list_left.size() != 0){
            v_1.setVisibility(View.VISIBLE);
        }else{
            v_1.setVisibility(View.GONE);
        }

        if (list_right.size() != 0){
            v_2.setVisibility(View.VISIBLE);
        }else {
            v_2.setVisibility(View.GONE);
        }

        if (leftAdapter == null) {
            leftAdapter = new DiseaseListLeftAdapter(HpiActivity.this,
                    list_left,2);
            lv_left.setAdapter(leftAdapter);
        }else {
            leftAdapter.notifyDataSetChanged();
        }

        if (rightAdapter == null) {
            rightAdapter = new DiseaseListRightAdapter(
                    HpiActivity.this, list_right);
            lv_right.setAdapter(rightAdapter);
        } else {
            rightAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void initDatas() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_disease_add:
                Intent intent = new Intent(HpiActivity.this, FtjVisceraDiseaseListActivity.class);
                startActivity(intent);
                break;
            case R.id.tv_hpi_submit:
                leftAdapter.notifyDataSetChanged();
                rightAdapter.notifyDataSetChanged();
                if (list_left.size() > 0 || list_right.size() >0) {
                    Constants.needRefesh = true;
                    // 症状的参数格式: 12-0.7,15-1.0,17-1.2,45-1.0,45-0.7
                    for (Symptom symptom : list_left) {
                        int symptomId = symptom.getId();
                        String dregree = symptom.getDregree();
                        if (dregree.equals(getString(R.string.dialog_choose_degree_light))) {
                            dregree = "0.7";
                        } else if (dregree.equals(getString(R.string.dialog_choose_degree_middle))) {
                            dregree = "1.0";
                        } else if (dregree.equals(getString(R.string.dialog_choose_degree_heavy))) {
                            dregree = "1.5";
                        }
                        symptomStr = symptomStr + "," + symptomId + "-" + dregree;
                    }
                    if (!TextUtils.isEmpty(symptomStr)) {
                        symptomStr = symptomStr.substring(1, symptomStr.length());
                    }
                    // Icd10参数格式: B76.901,B77.901,B08.201,B08.201
                    for (ICDItem icd : list_right) {
                        String micd = icd.getMicd();
                        icds = icds + "," + micd;
                    }
                    if (!TextUtils.isEmpty(icds)) {
                        icds = icds.substring(1, icds.length());
                    }
                    zxNum = list_left.size();
                    disNum = list_right.size();
                    if (list_right.size() == 0) {
                        disNum = 5;
                    }

                    if (isMan) {
                        sex = 0;
                    } else {
                        sex = 1;
                    }

                    LoginControllor.requestLogin(HpiActivity.this, new OnCompleteListener() {
                        @Override
                        public void onComplete() {
                            List<ChildMember> childMembers = LoginControllor.getLoginMember().getMengberchild();
                            int size = childMembers.size();
                            if (true) {
                                String url = Constant.BASE_URL + "/member/service/fxpgReport.jhtml?cust_id="
                                        + LoginControllor.getChoosedChildMember().getId();
                                if (!TextUtils.isEmpty(symptomStr)) {
                                    url = url + "&symptomStr=" + symptomStr;
                                }
                                if (!TextUtils.isEmpty(icds)) {
                                    url = url + "&icds=" + icds;
                                }
                                url = url + "&level=2&zxNum=" + zxNum + "&disNum=" + disNum + "&sex=" + sex + "&device=1";

                                Log.i("Log.i", "url = " + url);

                                Intent intent = new Intent(HpiActivity.this, VisceraIdentityReportActivity.class);
                                intent.putExtra("url", url);
                                startActivity(intent);
                                BaseApplication.destoryActivity("VisceraIdentityActivity");
                                BaseApplication.destoryActivity("VisceraListActivity");
                                finish();

                            } else {
                                new ChooseMemberDialog(HpiActivity.this, new OnCompleteListener() {
                                    @Override
                                    public void onComplete() {
                                        String url = Constant.BASE_URL + "/member/service/fxpgReport.jhtml?cust_id="
                                                + LoginControllor.getChoosedChildMember().getId();
                                        if (!TextUtils.isEmpty(symptomStr)) {
                                            url = url + "&symptomStr=" + symptomStr;
                                        }
                                        if (!TextUtils.isEmpty(icds)) {
                                            url = url + "&icds=" + icds;
                                        }
                                        url = url + "&level=2&zxNum=" + zxNum + "&disNum=" + disNum + "&sex=" + sex + "&device=1";

                                        Log.i("Log.i", "url = " + url);

                                        Intent intent = new Intent(HpiActivity.this, VisceraIdentityReportActivity.class);
                                        intent.putExtra("url", url);
                                        startActivity(intent);
                                        BaseApplication.destoryActivity("VisceraIdentityActivity");
                                        BaseApplication.destoryActivity("VisceraListActivity");
                                        finish();

                                    }
                                }).show();
                            }

                        }
                    });
                }else {
                    ToastUtil.invokeShortTimeToast(HpiActivity.this,getString(R.string.ftj_viscera_identity_next_step_tips));
                }
                break;
            default:
                break;
        }
    }

}
