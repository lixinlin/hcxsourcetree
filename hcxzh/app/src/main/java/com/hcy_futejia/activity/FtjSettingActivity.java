package com.hcy_futejia.activity;

import android.app.Dialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import android.content.IntentFilter;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Path;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hcy.ky3h.R;
import com.hcy.ky3h.collectdata.CDRequestUtils;
import com.hcy_futejia.utils.DateUtil;
import com.hcy_futejia.utils.DialogUtils;
import com.hcy_futejia.utils.DownloadManagerPro;
import com.hcy_futejia.utils.PackageUtils;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.utils.SpUtils;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.bean.Version;
import com.hxlm.hcyandroid.bodydiagnose.IntroductionActivity;
import com.hxlm.hcyandroid.datamanager.UpdateManager;
import com.hxlm.hcyandroid.tabbar.home.visceraidentity.AlertDialogPrompt;
import com.hxlm.hcyandroid.tabbar.home.visceraidentity.AlertDialogVersion;
import com.hxlm.hcyandroid.tabbar.usercenter.ChangePasswordActivity;
import com.hxlm.hcyandroid.tabbar.usercenter.FeedbackActivity;
import com.hxlm.hcyandroid.util.AppManager;
import com.hxlm.hcyandroid.util.ToastUtil;
import com.umeng.socialize.UMShareAPI;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Map;

/**
 * @author Lirong
 */
public class FtjSettingActivity extends BaseActivity implements View.OnClickListener {

    private LinearLayout ll_feedback;
    private LinearLayout ll_about_us;
    private LinearLayout ll_version_update;
    private Button bt_change_password;
    private Button bt_exit_login;

    private Dialog dialog;
    private long downloadId;
    private DownloadManager downloadManager;
    private DownloadManagerPro downloadManagerPro;
    private DownloadChangeObserver downloadObserver;
    private CompleteReceiver       completeReceiver;
    private Handler handler;
    private static final String TAG = "FtjSettingActivity";
    private ProgressBar progressBar;
    private TextView tv_update;
    private Dialog dialog1;
    private TextView tv_progress;
    private RelativeLayout rl_progress;


    @Override
    public void setContentView() {
        setContentView(R.layout.activity_ftj_setting);
    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.setting_shezhi), titleBar, 1);
        ll_feedback = findViewById(R.id.ll_feedback);
        ll_about_us = findViewById(R.id.ll_about_us);
        ll_version_update = findViewById(R.id.ll_version_update);
        bt_change_password = findViewById(R.id.bt_change_password);
        bt_exit_login = findViewById(R.id.bt_exit_login);

        ll_feedback.setOnClickListener(this);
        ll_about_us.setOnClickListener(this);
        ll_version_update.setOnClickListener(this);
        bt_change_password.setOnClickListener(this);
        bt_exit_login.setOnClickListener(this);

    }

    @Override
    public void initDatas() {
        handler = new Handler();
        downloadObserver = new DownloadChangeObserver();
        completeReceiver = new CompleteReceiver();
        /** register download success broadcast **/
        registerReceiver(completeReceiver,
                new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()){
            //意见反馈
            case R.id.ll_feedback:
                LoginControllor.requestLogin(FtjSettingActivity.this, new OnCompleteListener() {
                    @Override
                    public void onComplete() {
                        Intent i = new Intent(FtjSettingActivity.this, FeedbackActivity.class);
                        startActivity(i);
                    }
                });
                break;
            //关于我们
            case R.id.ll_about_us:
                intent = new Intent(FtjSettingActivity.this, IntroductionActivity.class);
                intent.putExtra("title", getString(R.string.setting_guanyu));
                if (Constant.isEnglish) {
                    intent.putExtra("introduceCategory", Constant.ABOUT_ME_EN);
                } else {
                    intent.putExtra("introduceCategory", Constant.ABOUT_ME);

                }
                startActivity(intent);
                break;
            //检查版本
            case R.id.ll_version_update:
                check_update();
                break;
            //修改密码
            case R.id.bt_change_password:
                LoginControllor.requestLogin(FtjSettingActivity.this, new OnCompleteListener() {
                    @Override
                    public void onComplete() {
                        startActivity(new Intent(FtjSettingActivity.this, ChangePasswordActivity.class));
                    }
                });
                break;
            //退出登录
            case R.id.bt_exit_login:
                setDialog();
                break;
            default:
                break;
        }
    }

    private void check_update() {
        UpdateManager updateManager = new UpdateManager(this);
        updateManager.isUpdate(new AbstractDefaultHttpHandlerCallback(this) {
            @Override
            protected void onResponseSuccess(Object obj) {
                Version version = (Version) obj;

                if (version.getIsUpdate()) {
                    String releaseContent = version.getReleaseContent();
                    if(version.isEnforcement()){
                        //强制更新
                        Map<String, Object> map = DialogUtils.setDialogProgressBar(FtjSettingActivity.this, version.getReleaseContent(), true, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                rl_progress.setVisibility(View.VISIBLE);
                                progressBar.setVisibility(View.VISIBLE);
                                //下载apk   http://www.trinea.cn/android/android-downloadmanager/
                                downloadapk(version.getDownurl());
                                getCollectDownload();
                                tv_update.setVisibility(View.GONE);
                                tv_progress.setVisibility(View.VISIBLE);
                            }
                        });
                        progressBar = (ProgressBar) map.get("pb");
                        tv_update = (TextView) map.get("tv_dialog_update");
                        tv_progress = (TextView) map.get("tv_progress");
                        rl_progress = (RelativeLayout) map.get("rl_progress");
                    }else {
                        //可选更新
                        Map<String, Object> map = DialogUtils.setDialogProgressBar(FtjSettingActivity.this, version.getReleaseContent(), false, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                rl_progress.setVisibility(View.VISIBLE);
                                progressBar.setVisibility(View.VISIBLE);

                                //下载apk   http://www.trinea.cn/android/android-downloadmanager/
                                downloadapk(version.getDownurl());
                                getCollectDownload();
                                dialog1.setCancelable(false);
                                tv_update.setVisibility(View.GONE);
                                tv_progress.setVisibility(View.VISIBLE);
                            }
                        });
                        progressBar = (ProgressBar) map.get("pb");
                        tv_update = (TextView) map.get("tv_dialog_update");
                        tv_progress = (TextView) map.get("tv_progress");
                        dialog1 = (Dialog) map.get("dialog");
                        rl_progress = (RelativeLayout) map.get("rl_progress");
                    }


                }else {
                    AlertDialogVersion cloce = new AlertDialogVersion(
                            FtjSettingActivity.this);
                    Dialog dialogcloce = cloce.createAlartDialog(getString(R.string.tips_version), "");
                    dialogcloce.show();
                    dialogcloce.setCanceledOnTouchOutside(true);
                }

            }
        });
    }

    /**
     * 数据埋点下载接口
     */
    private void getCollectDownload() {
        String dateToString = DateUtil.getDateToString(System.currentTimeMillis(), "yyyy-MM-dd HH:mm:ss");
        CDRequestUtils.getDownload(LoginControllor.getLoginMember().getId()+"","1","1", PackageUtils.getVersionName(FtjSettingActivity.this),dateToString,"");
    }


    private long quitTime;
    private String pattern = "yyyy-MM-dd HH:mm:ss";

    /**
     * 注销
     */
    public void setDialog() {
        dialog = new Dialog(FtjSettingActivity.this, R.style.dialog);
        View view = LayoutInflater.from(FtjSettingActivity.this).inflate(R.layout.dialog_exit, null);
        dialog.setContentView(view);
        //初始化控件
        TextView tvCancel = view.findViewById(R.id.tvCancel);
        TextView tvConfirm = view.findViewById(R.id.tvConfirm);
        TextView tvTips = view.findViewById(R.id.tvTips);

        tvTips.setText(getString(R.string.setting_querenzhuxiao));


        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginControllor.logout();
                dialog.dismiss();
                getCollectAccess();

                AppManager.getInstance().finishAllActivity();
//                startActivity(new Intent(SettingActivity.this, LoginActivity.class));
                if (Constant.isEnglish){
                    startActivity(new Intent(FtjSettingActivity.this, EnglishLoginActivity.class));
                }else {
                    startActivity(new Intent(FtjSettingActivity.this, FtjLoginActivity.class));
                }
            }
        });

        dialog.show();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
    }

    /**
     * 数据埋点用户访问记录接口
     */
    private void getCollectAccess() {
        long startTime = (long) SpUtils.get(FtjSettingActivity.this, "startTime", 0L);
        quitTime = System.currentTimeMillis();
        String dateStart = DateUtil.getDateToString(startTime, pattern);
        String dateQuit = DateUtil.getDateToString(quitTime, pattern);
        CDRequestUtils.getAccess(LoginControllor.getLoginMember().getId()+"","1","1",dateStart,dateQuit,"1","");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(FtjSettingActivity.this).onActivityResult(requestCode, resultCode, data);
    }

    private void downloadapk(String url) {
        tv_update.setClickable(false);
        File folder = Environment.getExternalStoragePublicDirectory("HCY");///storage/emulated/0/HCY
        boolean b = (folder.exists() && folder.isDirectory()) || folder.mkdirs();
        File apk = new File(folder,"hcy.apk");
        if(apk.exists()){
            apk.delete();
        }
        downloadManager = (DownloadManager)getSystemService(DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDestinationInExternalPublicDir("HCY", "hcy.apk");
        downloadId = downloadManager.enqueue(request);

    }

    class DownloadChangeObserver extends ContentObserver {

        public DownloadChangeObserver(){
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange) {
            try {
                //正在下载时关闭应用会引发null
                if(downloadManagerPro==null){
                    downloadManagerPro = new DownloadManagerPro(downloadManager);
                }
                int[] bytesAndStatus = downloadManagerPro.getBytesAndStatus(downloadId);
                int progress = (int) (((float) bytesAndStatus[0] / bytesAndStatus[1]) * 100);
                progressBar.setProgress( progress);
                tv_progress.setText(String.format("%d%% %n",progress));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    class CompleteReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            // 下载完成
            installApk();
        }
    }

    private void installApk() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        File folder = Environment.getExternalStoragePublicDirectory("HCY");
        boolean b = (folder.exists() && folder.isDirectory()) || folder.mkdirs();
        File file = new File(folder,"hcy.apk");
        boolean exists = file.exists();
        Uri uri = Uri.withAppendedPath(Uri.fromFile(folder), "hcy.apk");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            if(!exists){
                return;
            }
            //创建Uri
            Uri apkUri = FileProvider.getUriForFile(this, "com.hcy.ky3h.fileProvider", file);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
        } else {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setDataAndType(uri, "application/vnd.android.package-archive");
        }
        startActivity(intent);
        this.finish();

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(completeReceiver);
            if(downloadManager!=null){
                downloadManager.remove(downloadId);//取消下载
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    @Override
    protected void onResume() {
        super.onResume();
        /** observer download change **/
        getContentResolver().registerContentObserver(DownloadManagerPro.CONTENT_URI, true,
                downloadObserver);
    }

    @Override
    protected void onPause() {
        super.onPause();
        getContentResolver().unregisterContentObserver(downloadObserver);
    }
}
