package com.hcy_futejia.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hcy_futejia.adapter.DiseaseListLeftAdapter;
import com.hcy_futejia.adapter.FtjClassifyMoreAdapter;
import com.hcy_futejia.adapter.FtjExpandableListAdapter;
import com.hcy_futejia.utils.SingleListSymple;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.BaseApplication;
import com.hxlm.hcyandroid.bean.Symptom;
import com.hxlm.hcyandroid.tabbar.home.visceraidentity.AlertDialogPrompt;
import com.hxlm.hcyandroid.util.LanguageUtil;
import com.hxlm.hcyandroid.util.ToastUtil;
import com.hxlm.hcyandroid.util.VisceraUtil;
import com.hxlm.hcyandroid.view.ChooseDiseaseDegreeDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 */
public class FtjVisceraIdentityActivity extends BaseActivity implements
        View.OnClickListener{

    private TextView tv_selected_symptoms;//已选症状

    private RelativeLayout rl_1;
    private RelativeLayout rl_2;
    private RadioButton rb_human_diagram;// 人体图解
    private RadioButton rb_symptomlist;// 症状列表

    private LinearLayout linear_humandiagram;// 人体图解
    private RelativeLayout linear_symptomlist;// 症状列表


    private LinearLayout ll_switch_sex;// 选择性别
    private ImageView iv_man;// 性别男
    private ImageView iv_woman;// 性别女
    private ImageView iv_switch_front_and_back;// 选择正面或是背面

    // 前面男
    private RelativeLayout rl_front_man;
    private ImageView iv_front_man_head;
    private ImageView iv_front_man_breast;
    private ImageView iv_front_man_right_arm;
    private ImageView iv_front_man_left_arm;
    private ImageView iv_front_man_abdomen;
    private ImageView iv_front_man_leg;

    // 背面男
    private RelativeLayout rl_back_man;
    private ImageView iv_back_man_head;
    private ImageView iv_back_man_breast;
    private ImageView iv_back_man_right_arm;
    private ImageView iv_back_man_left_arm;
    private ImageView iv_back_man_abdomen;
    private ImageView iv_back_man_leg;

    // 前面女
    private RelativeLayout rl_front_woman;
    private ImageView iv_front_woman_head;
    private ImageView iv_front_woman_breast;
    private ImageView iv_front_woman_right_arm;
    private ImageView iv_front_woman_left_arm;
    private ImageView iv_front_woman_abdomen;
    private ImageView iv_front_woman_leg;

    // 背面女
    private RelativeLayout rl_back_woman;
    private ImageView iv_back_woman_head;
    private ImageView iv_back_woman_breast;
    private ImageView iv_back_woman_right_arm;
    private ImageView iv_back_woman_left_arm;
    private ImageView iv_back_woman_abdomen;
    private ImageView iv_back_woman_leg;

    // 是否是前面
    private boolean isFront = true;
    // 是否是男
    private boolean isMan = true;

    // 二级菜单
    private ExpandableListView expandableListView;
    private FtjExpandableListAdapter adapter;
    private LinearLayout ll_child;

    public static int checkNum = 0; // 记录选中的条目数量
    private String clickedPartName = "";
    // public static int groupPositionItem = -1;// 标记1级数据位置
    public static int childPositionItem = -1;// 标记二级数据位置
    public static String groupPositionName;// 1级数据内容
    public static String childPositionName;// 2级数据内容
    public static String sanchildPositionName;

    // expandableListView自数据下的数据
    private ListView morelist;

    private List<Symptom> symptoms;//所有的症状及症状表现
    private List<Symptom> symptomsToBeDelete = new ArrayList<Symptom>();//用于存放不符合当前用户性别的症状及表现
    private Map<String, List<String>> personParts = new HashMap<String, List<String>>();//用来存放症状名称下的所有症状集合
    private List<String> partNames = new ArrayList<String>();//症状名称总类集合
    private List<Symptom> symptomsToShow = new ArrayList<Symptom>();//用于存放每个症状名称的症状表现

    private List<Symptom> list_right_data;
    public static FtjClassifyMoreAdapter moreAdapter;

    private boolean isFirst = true;
    private int index = -1;
//    public ImageView img_ishave;// 提交按钮

    public static FtjClassifyMoreAdapter.ViewHolder holder;
    private long lastClickTime = 0;//上次点击的时间
    private int spaceTime = 500;//时间间隔
    private TextView tv_next_step;

    private LinearLayout ll_pop;
    private RelativeLayout rl_button;
    private View v_black;
    private ListView lv_selected_symptom;

    private DiseaseListLeftAdapter leftAdapter;// 左侧adapter
    private TextView txt;
    private TextView tv_circle;

    private int groupPositionClick;
    private View lastSelectView;

    private boolean isAllowClick() {
        long currentTime = System.currentTimeMillis();//当前系统时间
        boolean isAllowClick;//是否允许点击
        if (currentTime - lastClickTime > spaceTime) {
            isAllowClick = true;
            lastClickTime = currentTime;
        } else {
            isAllowClick = false;
        }

        return isAllowClick;

    }

    @Override
    public void initViews() {

        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.viscera_title), titleBar, 1);
    }

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_ftj_viscera_identity);
    }

    //初始化控件
    public void initview() {

        rl_button = findViewById(R.id.rl_button);
        linear_humandiagram = (LinearLayout) findViewById(R.id.ll_body_diagram);
        linear_symptomlist = findViewById(R.id.rl_symptom_list);
        ll_pop = findViewById(R.id.ll_pop);
        ll_pop.setVisibility(View.GONE);
        lv_selected_symptom = findViewById(R.id.lv_selected_symptom);

        v_black = findViewById(R.id.v_black);
        v_black.setVisibility(View.GONE);

        ll_switch_sex = (LinearLayout) findViewById(R.id.ll_switch_sex);
        iv_man = (ImageView) findViewById(R.id.iv_man);
        iv_woman = (ImageView) findViewById(R.id.iv_woman);
        iv_switch_front_and_back = (ImageView) findViewById(R.id.iv_switch_front_and_back);

        rl_front_man = (RelativeLayout) findViewById(R.id.rl_front_man);
        iv_front_man_head = (ImageView) findViewById(R.id.iv_front_man_head);
        iv_front_man_breast = (ImageView) findViewById(R.id.iv_front_man_breast);
        iv_front_man_right_arm = (ImageView) findViewById(R.id.iv_front_man_right_arm);
        iv_front_man_left_arm = (ImageView) findViewById(R.id.iv_front_man_left_arm);
        iv_front_man_abdomen = (ImageView) findViewById(R.id.iv_front_man_abdomen);
        iv_front_man_leg = (ImageView) findViewById(R.id.iv_front_man_leg);

        rl_back_man = (RelativeLayout) findViewById(R.id.rl_back_man);
        iv_back_man_head = (ImageView) findViewById(R.id.iv_back_man_head);
        iv_back_man_breast = (ImageView) findViewById(R.id.iv_back_man_breast);
        iv_back_man_right_arm = (ImageView) findViewById(R.id.iv_back_man_right_arm);
        iv_back_man_left_arm = (ImageView) findViewById(R.id.iv_back_man_left_arm);
        iv_back_man_abdomen = (ImageView) findViewById(R.id.iv_back_man_abdomen);
        iv_back_man_leg = (ImageView) findViewById(R.id.iv_back_man_leg);

        rl_front_woman = (RelativeLayout) findViewById(R.id.rl_front_woman);
        iv_front_woman_head = (ImageView) findViewById(R.id.iv_front_woman_head);
        iv_front_woman_breast = (ImageView) findViewById(R.id.iv_front_woman_breast);
        iv_front_woman_right_arm = (ImageView) findViewById(R.id.iv_front_woman_right_arm);
        iv_front_woman_left_arm = (ImageView) findViewById(R.id.iv_front_woman_left_arm);
        iv_front_woman_abdomen = (ImageView) findViewById(R.id.iv_front_woman_abdomen);
        iv_front_woman_leg = (ImageView) findViewById(R.id.iv_front_woman_leg);

        rl_back_woman = (RelativeLayout) findViewById(R.id.rl_back_woman);
        iv_back_woman_head = (ImageView) findViewById(R.id.iv_back_woman_head);
        iv_back_woman_breast = (ImageView) findViewById(R.id.iv_back_woman_breast);
        iv_back_woman_right_arm = (ImageView) findViewById(R.id.iv_back_woman_right_arm);
        iv_back_woman_left_arm = (ImageView) findViewById(R.id.iv_back_woman_left_arm);
        iv_back_woman_abdomen = (ImageView) findViewById(R.id.iv_back_woman_abdomen);
        iv_back_woman_leg = (ImageView) findViewById(R.id.iv_back_woman_leg);

        expandableListView = (ExpandableListView) findViewById(R.id.listExpandableListView);
        morelist = (ListView) findViewById(R.id.lv_classify);
        ll_child = (LinearLayout) findViewById(R.id.ll_child);

        //---------------------------------------------------------------//
        rl_1 = (RelativeLayout) findViewById(R.id.rl_1);
        rl_2 = (RelativeLayout) findViewById(R.id.rl_2);
        rb_human_diagram = (RadioButton) findViewById(R.id.rb_body_diagram);
        rb_symptomlist = (RadioButton) findViewById(R.id.rb_symptoms_list);
        rb_human_diagram.setOnClickListener(this);
        rb_symptomlist.setOnClickListener(this);
        tv_selected_symptoms = findViewById(R.id.tv_selected_symptoms);
        tv_next_step = findViewById(R.id.tv_next_step);

        tv_next_step.setOnClickListener(this);

        //点击已选择症状
        tv_selected_symptoms.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //设置动画，从自身位置的最下端向上滑动了自身的高度，持续时间为500ms
                if (ll_pop.getVisibility() == View.GONE) {
                    if (leftAdapter == null) {
                        leftAdapter = new DiseaseListLeftAdapter(FtjVisceraIdentityActivity.this,
                                list_right_data,1);
                        leftAdapter.setView(tv_selected_symptoms);
                        lv_selected_symptom.setAdapter(leftAdapter);
                    }else {
                        leftAdapter.notifyDataSetChanged();
                    }
                    final TranslateAnimation ctrlAnimation = new TranslateAnimation(
                    TranslateAnimation.RELATIVE_TO_SELF, 0, TranslateAnimation.RELATIVE_TO_SELF, 0,
                    TranslateAnimation.RELATIVE_TO_SELF, 1, TranslateAnimation.RELATIVE_TO_SELF, 0);
                    //设置动画的过渡时间
                    ctrlAnimation.setDuration(400);
                    ll_pop.setVisibility(View.VISIBLE);
                    ll_pop.startAnimation(ctrlAnimation);
                    v_black.setVisibility(View.VISIBLE);
                    v_black.setFocusable(true);
                    v_black.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            v_black.setVisibility(View.GONE);
                            ll_pop.setVisibility(View.GONE);
                            moreAdapter.notifyDataSetChanged();
                            adapter.notifyDataSetChanged();
                        }
                    });
                }else {
                    v_black.setVisibility(View.GONE);
                    ll_pop.setVisibility(View.GONE);
                    v_black.setClickable(true);
                    moreAdapter.notifyDataSetChanged();
                    adapter.notifyDataSetChanged();
                }

            }
        });


//        radiobutton.setOnCheckedChangeListener(this);
        rb_human_diagram.setChecked(true);//默认显示人体图解


        //添加点击监听事件
        setListener();

    }

    @Override
    public void initDatas(){
        initview();//初始化控件
        if (LanguageUtil.getInstance().getAppLocale(this).getLanguage().equals("en")){
            symptoms = VisceraUtil.initSymptomEn();
        }else {
            symptoms = VisceraUtil.initSymptom();//得到症状集合
        }
        initSymptom();//得到数据
        initModle();// 给1级添加数据

        // 右侧的ListView
//        list_right_data = new ArrayList<Symptom>();
        list_right_data = SingleListSymple.getSingleListSymple().getSymptomList();

        tv_selected_symptoms.setText(getString(R.string.viscera_selected_symptom)+list_right_data.size()+"/5");

        //------------------------------默认显示全身的第一项------------------
        //是否是第一次
        if (isFirst) {
            moreAdapter = new FtjClassifyMoreAdapter(
                    FtjVisceraIdentityActivity.this, symptomsToShow);
            morelist.setAdapter(moreAdapter);
            isFirst = false;
        }

        //默认第一项“全身”展开相应的症状分类
        expandableListView.expandGroup(0);
        //默认全身的第一项显示
        if (symptomsToShow.size() > 0) {
            symptomsToShow.clear();
        }
        //获取第一项症状分类名称
        String childPartName = (String) adapter.getChild(0, 0);

        //添加每个症状名称的症状表现
        for (Symptom symptom : symptoms) {
            if (symptom.getPart().equals(childPartName)) {
                symptomsToShow.add(symptom);
            }
        }
        moreAdapter.notifyDataSetChanged();
        //---------------------------------------------显示-------------------
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (moreAdapter != null){
            moreAdapter.notifyDataSetChanged();
        }
        if (adapter != null){
            adapter.notifyDataSetChanged();
        }
    }

    /**
     * 设置选中改变宽度
     */
    private void setSelectedWidth(int symptomWidth, int bodyWidth) {
        ViewGroup.LayoutParams layoutParams1 = rb_symptomlist.getLayoutParams();
        layoutParams1.width = symptomWidth;
        rb_symptomlist.setLayoutParams(layoutParams1);
        ViewGroup.LayoutParams layoutParams = rb_human_diagram.getLayoutParams();
        layoutParams.width = bodyWidth;
        rb_human_diagram.setLayoutParams(layoutParams);
    }

    private void initSymptom() {

        for (Symptom symptom : symptoms) {
            String personPart = symptom.getPersonPart();//症状总类
            if (!partNames.contains(personPart)) {
                partNames.add(personPart);//添加症状总类
            }
            //如果是男性
            if (isMan) {
                if (symptom.getSexType() == 2 || symptom.getSexType() == 0) {

                    //如果Map里面包含键症状名称（症状总类）
                    if (personParts.containsKey(personPart)) {
                        //但是不包含症状分类
                        if (!personParts.get(personPart).contains(
                                symptom.getPart())) {
                            //给该症状添加症状分类
                            personParts.get(personPart).add(symptom.getPart());
                        }
                    }
                    //如果Map里面不包含键症状总类名称
                    else {
                        //创建症状分类集合添加症状分类
                        List<String> childParts = new ArrayList<String>();
                        childParts.add(symptom.getPart());
                        //存放键值
                        personParts.put(personPart, childParts);
                    }
                } else {
                    //添加只有女性才有的症状
                    symptomsToBeDelete.add(symptom);
                }
            } else {
                if (symptom.getSexType() == 2 || symptom.getSexType() == 1) {
                    if (personParts.containsKey(personPart)) {
                        if (!personParts.get(personPart).contains(
                                symptom.getPart())) {
                            personParts.get(personPart).add(symptom.getPart());
                        }
                    } else {
                        List<String> childParts = new ArrayList<String>();
                        childParts.add(symptom.getPart());
                        personParts.put(personPart, childParts);
                    }
                } else {
                    symptomsToBeDelete.add(symptom);
                }
            }
        }
        //删除当前不符合性别的症状及表现
        symptoms.removeAll(symptomsToBeDelete);
    }


    // onclick事件
    private void setListener() {

        iv_front_man_head.setOnClickListener(this);
        iv_front_man_breast.setOnClickListener(this);
        iv_front_man_right_arm.setOnClickListener(this);
        iv_front_man_left_arm.setOnClickListener(this);
        iv_front_man_abdomen.setOnClickListener(this);
        iv_front_man_leg.setOnClickListener(this);

        iv_back_man_head.setOnClickListener(this);
        iv_back_man_breast.setOnClickListener(this);
        iv_back_man_right_arm.setOnClickListener(this);
        iv_back_man_left_arm.setOnClickListener(this);
        iv_back_man_abdomen.setOnClickListener(this);
        iv_back_man_leg.setOnClickListener(this);

        iv_front_woman_head.setOnClickListener(this);
        iv_front_woman_breast.setOnClickListener(this);
        iv_front_woman_right_arm.setOnClickListener(this);
        iv_front_woman_left_arm.setOnClickListener(this);
        iv_front_woman_abdomen.setOnClickListener(this);
        iv_front_woman_leg.setOnClickListener(this);

        iv_back_woman_head.setOnClickListener(this);
        iv_back_woman_breast.setOnClickListener(this);
        iv_back_woman_right_arm.setOnClickListener(this);
        iv_back_woman_left_arm.setOnClickListener(this);
        iv_back_woman_abdomen.setOnClickListener(this);
        iv_back_woman_leg.setOnClickListener(this);

        ll_switch_sex.setOnClickListener(this);
        iv_switch_front_and_back.setOnClickListener(this);

        // 父类二级菜单
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                //症状总类名称
                adapter.setGroupSelection(groupPosition);
//
                adapter.notifyDataSetChanged();
                groupPositionName = partNames.get(groupPosition);
                groupPositionClick = groupPosition;
//                    左右侧列表联动
                if(!parent.isGroupExpanded(groupPosition)){
                    adapter.setChildSelection(0);
                    adapter.notifyDataSetChanged();
                    ll_child.setVisibility(View.VISIBLE);
                    clickedPartName = partNames.get(groupPosition);//症状总类
                    // groupPositionItem = groupPosition;// 标记1级下标
                    childPositionItem = 0;// 标记2级下标
                    symptomsToShow.clear();
                    //症状分类名称
                    String childPartName = (String) adapter.getChild(groupPosition,
                            0);

                    //添加每个症状名称的症状表现
                    for (Symptom symptom : symptoms) {
                        if (symptom.getPart().equals(childPartName)) {
                            symptomsToShow.add(symptom);
                        }
                    }
                    moreAdapter.notifyDataSetChanged();
                }


                return false;
            }
        });
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                try {
                    int count = expandableListView.getExpandableListAdapter().getGroupCount();
                    for (int i = 0 ; i < count; i++) {
                        if (groupPosition != i) {// 关闭其他分组
                            expandableListView.collapseGroup(i);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        // 子类二级菜单
        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                adapter.setChildSelection(childPosition);
                adapter.notifyDataSetChanged();
                ll_child.setVisibility(View.VISIBLE);
                clickedPartName = partNames.get(groupPosition);//症状总类
                // groupPositionItem = groupPosition;// 标记1级下标
                childPositionItem = childPosition;// 标记2级下标
                symptomsToShow.clear();
                //症状分类名称
                String childPartName = (String) adapter.getChild(groupPosition,
                        childPosition);

                //添加每个症状名称的症状表现
                for (Symptom symptom : symptoms) {
                    if (symptom.getPart().equals(childPartName)) {
                        symptomsToShow.add(symptom);
                    }
                }
                moreAdapter.notifyDataSetChanged();

                return true;
            }
        });

        // 子类二级菜单的子项 点击症状分类的症状表现
        morelist.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if (isAllowClick()) {
                    // 得到当前项
                    final Symptom symptom = symptomsToShow.get(position);
                    // 默认是false
                    boolean isChoosed = symptom.isChoosed();
                    txt = view.findViewById(R.id.moreitem_txt);
                    tv_circle = view.findViewById(R.id.tv_circle);
                    // 如果选中了
                    if (!isChoosed) {
                        int size = list_right_data.size();
                        if (size < 5) {

                            ChooseDiseaseDegreeDialog dialog = new ChooseDiseaseDegreeDialog(
                                    FtjVisceraIdentityActivity.this,
                                    new ChooseDiseaseDegreeDialog.OnChoosedListener() {

                                        @Override
                                        public void ChooseDiseaseDegree(String strDegree) {
                                            //如果症状不为空，则添加到集合中
                                            if (!TextUtils.isEmpty(strDegree)) {
                                                symptom.setChoosed(true);
                                                //症状程度
                                                symptom.setDregree(strDegree);
                                                list_right_data.add(symptom);
                                                moreAdapter.notifyDataSetChanged();


                                                ToastUtil.invokeShortTimeToast(FtjVisceraIdentityActivity.this, getString(R.string.viscera_identity_tips_yitianjia));
                                                txt.setTextColor(Color.parseColor("#ffa200"));
                                                tv_circle.setVisibility(View.VISIBLE);
                                                tv_selected_symptoms.setText(getString(R.string.viscera_selected_symptom)+list_right_data.size()+"/5");

                                                adapter.setSymptomsToShowData(list_right_data);
                                                adapter.setChildSelection(childPositionItem);
                                                adapter.notifyDataSetChanged();

                                            }


                                        }
                                    });

                            dialog.show();
                            dialog.setCanceledOnTouchOutside(true);


                        } else {
                            AlertDialogPrompt cloce = new AlertDialogPrompt(
                                    FtjVisceraIdentityActivity.this);
                            Dialog dialogcloce = cloce.createAlartDialog(getString(R.string.viscera_tips_max_symptoms), "");
                            dialogcloce.show();
                            dialogcloce.setCanceledOnTouchOutside(true);
                            txt.setTextColor(Color.parseColor("#8e8e93"));
                            tv_circle.setVisibility(View.GONE);
                            adapter.setSymptomsToShowData(list_right_data);
                            adapter.notifyDataSetChanged();

                        }
                    }
                    // 如果取消了
                    else {

                        symptom.setChoosed(false);
                        list_right_data.remove(symptom);
                        txt.setTextColor(Color.parseColor("#8e8e93"));
                        tv_circle.setVisibility(View.GONE);
                        tv_selected_symptoms.setText(getString(R.string.viscera_selected_symptom)+list_right_data.size()+"/5");
                        moreAdapter.notifyDataSetChanged();
                        adapter.notifyDataSetChanged();
                    }


                }
            }
        });
    }

    // 改变身体状态
    public void changeBody() {
        if (isFront && isMan) {
            rl_front_man.setVisibility(View.VISIBLE);
            rl_back_man.setVisibility(View.GONE);
            rl_front_woman.setVisibility(View.GONE);
            rl_back_woman.setVisibility(View.GONE);
        } else if (isFront && !isMan) {
            rl_front_man.setVisibility(View.GONE);
            rl_back_man.setVisibility(View.GONE);
            rl_front_woman.setVisibility(View.VISIBLE);
            rl_back_woman.setVisibility(View.GONE);
        } else if (!isFront && isMan) {
            rl_front_man.setVisibility(View.GONE);
            rl_back_man.setVisibility(View.VISIBLE);
            rl_front_woman.setVisibility(View.GONE);
            rl_back_woman.setVisibility(View.GONE);
        } else if (!isFront && !isMan) {
            rl_front_man.setVisibility(View.GONE);
            rl_back_man.setVisibility(View.GONE);
            rl_front_woman.setVisibility(View.GONE);
            rl_back_woman.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onPause() {
        checkNum = 0;
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    // 给1级添加数据
    private void initModle() {

        adapter = new FtjExpandableListAdapter(this, personParts, partNames);
        expandableListView.setAdapter(adapter);
    }


    // 添加默认图片
    public void setDefult() {
        symptomsToShow.clear();//清除症状表现
        if (moreAdapter != null) {
            moreAdapter.notifyDataSetChanged();//刷新数据
        }
        for (int i = 0; i < partNames.size(); i++) {
            expandableListView.collapseGroup(i);//默认关闭所有的group,不展开
        }
        iv_front_man_head.setImageResource(R.drawable.front_man_normal_head);
        iv_front_man_breast
                .setImageResource(R.drawable.front_man_normal_breast);
        iv_front_man_right_arm
                .setImageResource(R.drawable.front_man_normal_right_arm);
        iv_front_man_left_arm
                .setImageResource(R.drawable.front_man_normal_left_arm);
        iv_front_man_abdomen
                .setImageResource(R.drawable.front_man_normal_abdomen);
        iv_front_man_leg.setImageResource(R.drawable.front_man_normal_leg);

        iv_back_man_head.setImageResource(R.drawable.back_man_normal_head);
        iv_back_man_breast.setImageResource(R.drawable.back_man_normal_breast);
        iv_back_man_right_arm
                .setImageResource(R.drawable.back_man_normal_right_arm);
        iv_back_man_left_arm
                .setImageResource(R.drawable.back_man_normal_left_arm);
        iv_back_man_abdomen
                .setImageResource(R.drawable.back_man_normal_abdomen);
        iv_back_man_leg.setImageResource(R.drawable.back_man_normal_leg);

        iv_front_woman_head
                .setImageResource(R.drawable.front_woman_normal_head);
        iv_front_woman_breast
                .setImageResource(R.drawable.front_woman_normal_breast);
        iv_front_woman_right_arm
                .setImageResource(R.drawable.front_woman_normal_right_arm);
        iv_front_woman_left_arm
                .setImageResource(R.drawable.front_woman_normal_left_arm);
        iv_front_woman_abdomen
                .setImageResource(R.drawable.front_woman_normal_abdomen);
        iv_front_woman_leg.setImageResource(R.drawable.front_woman_normal_leg);

        iv_back_woman_head.setImageResource(R.drawable.back_woman_normal_head);
        iv_back_woman_breast
                .setImageResource(R.drawable.back_woman_normal_breast);
        iv_back_woman_right_arm
                .setImageResource(R.drawable.back_woman_normal_right_arm);
        iv_back_woman_left_arm
                .setImageResource(R.drawable.back_woman_normal_left_arm);
        iv_back_woman_abdomen
                .setImageResource(R.drawable.back_woman_normal_abdomen);
        iv_back_woman_leg.setImageResource(R.drawable.back_woman_normal_leg);
    }

    @Override
    public void onClick(View v) {
        String clickedPartName = "";
        setDefult();//添加默认图片
        switch (v.getId()) {
            //人体列表
            case R.id.rb_body_diagram:
                click_body();
                break;

            // 症状列表
            case R.id.rb_symptoms_list:
                clickSymptoms();
                break;

            // 选择性别
            case R.id.ll_switch_sex:
                isMan = !isMan;
                if (isMan) {
                    iv_man.setVisibility(View.VISIBLE);
                    iv_woman.setVisibility(View.INVISIBLE);
                } else {
                    iv_man.setVisibility(View.INVISIBLE);
                    iv_woman.setVisibility(View.VISIBLE);
                }

                //右侧数据清空
                list_right_data.clear();

                changeBody();//改变身体状态
                initSymptom();//重新获取数据
                adapter.notifyDataSetChanged();//刷新二级列表
                break;
            // 选择前面或者后面
            case R.id.iv_switch_front_and_back:
                isFront = !isFront;
                if (isFront) {
                    iv_switch_front_and_back.setImageResource(R.drawable.front);
                } else {
                    iv_switch_front_and_back.setImageResource(R.drawable.back);
                }
                changeBody();//改变身体状态
                break;

            case R.id.iv_front_man_head:
                iv_front_man_head
                        .setImageResource(R.drawable.front_man_pressed_head);
                clickedPartName = getString(R.string.viscera_iden_head);
                clickSymptoms();
                break;
            case R.id.iv_front_man_breast:
                iv_front_man_breast
                        .setImageResource(R.drawable.front_man_pressed_breast);
                clickedPartName = getString(R.string.viscera_iden_chest);
                clickSymptoms();
                break;
            case R.id.iv_front_man_right_arm:
                iv_front_man_right_arm
                        .setImageResource(R.drawable.front_man_pressed_right_arm);
                clickedPartName = getString(R.string.viscera_iden_limbs);
                clickSymptoms();
                break;
            case R.id.iv_front_man_left_arm:
                iv_front_man_left_arm
                        .setImageResource(R.drawable.front_man_pressed_left_arm);
                clickedPartName = getString(R.string.viscera_iden_limbs);
                clickSymptoms();
                break;
            case R.id.iv_front_man_abdomen:
                iv_front_man_abdomen
                        .setImageResource(R.drawable.front_man_pressed_abdomen);
                clickedPartName = getString(R.string.viscera_iden_abdomen);
                clickSymptoms();
                break;
            case R.id.iv_front_man_leg:
                iv_front_man_leg.setImageResource(R.drawable.front_man_pressed_leg);
                clickedPartName = getString(R.string.viscera_iden_limbs);
                clickSymptoms();
                break;
            case R.id.iv_back_man_head:
                iv_back_man_head.setImageResource(R.drawable.back_man_pressed_head);
                clickedPartName = getString(R.string.viscera_iden_head);
                clickSymptoms();
                break;
            case R.id.iv_back_man_breast:
                iv_back_man_breast
                        .setImageResource(R.drawable.back_man_pressed_breast);
                clickedPartName = getString(R.string.viscera_iden_head);
                clickSymptoms();
                break;
            case R.id.iv_back_man_right_arm:
                iv_back_man_right_arm
                        .setImageResource(R.drawable.back_man_pressed_right_arm);
                clickedPartName = getString(R.string.viscera_iden_limbs);
                clickSymptoms();
                break;
            case R.id.iv_back_man_left_arm:
                iv_back_man_left_arm
                        .setImageResource(R.drawable.back_man_pressed_left_arm);
                clickedPartName = getString(R.string.viscera_iden_limbs);
                clickSymptoms();
                break;
            case R.id.iv_back_man_abdomen:
                iv_back_man_abdomen
                        .setImageResource(R.drawable.back_man_pressed_abdomen);
                clickedPartName = getString(R.string.viscera_iden_waist);
                clickSymptoms();
                break;
            case R.id.iv_back_man_leg:
                iv_back_man_leg.setImageResource(R.drawable.back_man_pressed_leg);
                clickedPartName = getString(R.string.viscera_iden_limbs);
                clickSymptoms();
                break;
            case R.id.iv_front_woman_head:
                iv_front_woman_head
                        .setImageResource(R.drawable.front_woman_pressed_head);
                clickedPartName = getString(R.string.viscera_iden_head);
                clickSymptoms();
                break;
            case R.id.iv_front_woman_breast:
                iv_front_woman_breast
                        .setImageResource(R.drawable.front_woman_pressed_breast);
                clickedPartName = getString(R.string.viscera_iden_chest);
                clickSymptoms();
                break;
            case R.id.iv_front_woman_right_arm:
                iv_front_woman_right_arm
                        .setImageResource(R.drawable.front_woman_pressed_right_arm);
                clickedPartName = getString(R.string.viscera_iden_limbs);
                clickSymptoms();
                break;
            case R.id.iv_front_woman_left_arm:
                iv_front_woman_left_arm
                        .setImageResource(R.drawable.front_woman_pressed_left_arm);
                clickedPartName = getString(R.string.viscera_iden_limbs);
                clickSymptoms();
                break;
            case R.id.iv_front_woman_abdomen:
                iv_front_woman_abdomen
                        .setImageResource(R.drawable.front_woman_pressed_abdomen);
                clickedPartName = getString(R.string.viscera_iden_abdomen);
                clickSymptoms();
                break;
            case R.id.iv_front_woman_leg:
                iv_front_woman_leg
                        .setImageResource(R.drawable.front_woman_pressed_leg);
                clickedPartName = getString(R.string.viscera_iden_limbs);
                clickSymptoms();
                break;
            case R.id.iv_back_woman_head:
                iv_back_woman_head
                        .setImageResource(R.drawable.back_woman_pressed_head);
                clickedPartName = getString(R.string.viscera_iden_head);
                clickSymptoms();
                break;
            case R.id.iv_back_woman_breast:
                iv_back_woman_breast
                        .setImageResource(R.drawable.back_woman_pressed_breast);
                clickedPartName = getString(R.string.viscera_iden_back);
                clickSymptoms();
                break;
            case R.id.iv_back_woman_right_arm:
                iv_back_woman_right_arm
                        .setImageResource(R.drawable.back_woman_pressed_right_arm);
                clickedPartName = getString(R.string.viscera_iden_limbs);
                clickSymptoms();
                break;
            case R.id.iv_back_woman_left_arm:
                iv_back_woman_left_arm
                        .setImageResource(R.drawable.back_woman_pressed_left_arm);
                clickedPartName = getString(R.string.viscera_iden_limbs);
                clickSymptoms();
                break;
            case R.id.iv_back_woman_abdomen:
                iv_back_woman_abdomen
                        .setImageResource(R.drawable.back_woman_pressed_abdomen);
                clickedPartName = getString(R.string.viscera_iden_waist);
                clickSymptoms();
                break;
            case R.id.iv_back_woman_leg:
                iv_back_woman_leg
                        .setImageResource(R.drawable.back_woman_pressed_leg);
                clickedPartName = getString(R.string.viscera_iden_limbs);
                clickSymptoms();
                break;
            case R.id.tv_next_step:
                if (list_right_data.size() != 0) {
                    Intent intent = new Intent(FtjVisceraIdentityActivity.this,
                            FtjVisceraDiseaseListActivity.class);
                    intent.putExtra("isMan", isMan);
                    startActivity(intent);
                    BaseApplication.addDestoryActivity(FtjVisceraIdentityActivity.this, "VisceraIdentityActivity");
                }else {
                    ToastUtil.invokeShortTimeToast(FtjVisceraIdentityActivity.this,getString(R.string.ftj_viscera_identity_next_step_tips));
                }
                break;
            default:
                break;
        }
        if (!TextUtils.isEmpty(clickedPartName)) {
            for (int i = 0; i < partNames.size(); i++) {
                if (partNames.get(i).equals(clickedPartName)) {
                    rb_symptomlist.setChecked(true);
                    linear_humandiagram.setVisibility(View.GONE);//人体图解隐藏
                    linear_symptomlist.setVisibility(View.VISIBLE);//人体症状显示
                    expandableListView.expandGroup(i);//，根据用户点击人体的某个部位默认展开相应的症状分类
                    adapter.setGroupSelection(i);
                    adapter.setChildSelection(0);
                    clickSymptoms(i);
                    break;
                }
            }
        }
    }

    private void clickSymptoms() {
        rb_symptomlist.setChecked(true);
        rb_human_diagram.setChecked(false);
        rl_1.setVisibility(View.INVISIBLE);
        rl_2.setVisibility(View.VISIBLE);
        rb_human_diagram.setMaxLines(1);
        rb_human_diagram.setEllipsize(TextUtils.TruncateAt.END);
        linear_symptomlist.setVisibility(View.VISIBLE);
        linear_humandiagram.setVisibility(View.GONE);
        rl_button.setVisibility(View.VISIBLE);
    }

    private void clickSymptoms(int i) {
        rb_symptomlist.setChecked(true);
        rb_human_diagram.setChecked(false);
        rl_1.setVisibility(View.INVISIBLE);
        rl_2.setVisibility(View.VISIBLE);
        rb_human_diagram.setMaxLines(1);
        rb_human_diagram.setEllipsize(TextUtils.TruncateAt.END);
        linear_symptomlist.setVisibility(View.VISIBLE);
        linear_humandiagram.setVisibility(View.GONE);
        rl_button.setVisibility(View.VISIBLE);

        //获取第一项症状分类名称
        String childPartName = (String) adapter.getChild(i, 0);

        //添加每个症状名称的症状表现
        for (Symptom symptom : symptoms) {
            if (symptom.getPart().equals(childPartName)) {
                symptomsToShow.add(symptom);
            }
        }
        moreAdapter.notifyDataSetChanged();

    }

    private void click_body() {
        rb_symptomlist.setChecked(false);
        rb_human_diagram.setChecked(true);
        rl_1.setVisibility(View.VISIBLE);
        rl_2.setVisibility(View.INVISIBLE);
        rb_symptomlist.setMaxLines(1);
        rb_symptomlist.setEllipsize(TextUtils.TruncateAt.END);
        linear_humandiagram.setVisibility(View.VISIBLE);
        linear_symptomlist.setVisibility(View.GONE);
        rl_button.setVisibility(View.GONE);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (list_right_data != null){
            list_right_data.clear();
        }
    }
}
