package com.hcy_futejia.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.consult.ConsultationManager;
import com.hxlm.android.hcy.report.UploadManager;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.android.utils.Logger;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.callback.MyCallBack;
import com.hxlm.hcyandroid.tabbar.expertconsult.GetPicActivity;
import com.hxlm.hcyandroid.tabbar.expertconsult.PhotoActivity;
import com.hxlm.hcyandroid.util.Bimp;
import com.hxlm.hcyandroid.util.FileUtils;
import com.hxlm.hcyandroid.util.SharedPreferenceUtil;
import com.hxlm.hcyandroid.util.ToastUtil;
import com.hxlm.hcyandroid.view.ContainsEmojiEditText;
import com.hxlm.hcyandroid.view.TuWenCloseBackDialog;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Administrator
 */
public class FtjTuWenActivity extends BaseActivity implements View.OnClickListener {

    private ContainsEmojiEditText et_suggest;
    private TextView tv_length;
    private ImageView iv_upload_1;
    private GridView gv_img;
    private Button btn_submit;

    private Boolean sort;
    private Boolean biaoqingstate = false;
    private static final int TAKE_PICTURE = 0x000000;
    private ConsultationManager consultationManager;
    private UploadManager uploadManager;
    private GridAdapter adapter;
    private String path = "";
    private String content;
    private File file;
    private int imageCount = -1;
    private int commitImageSuccessCount = 0;
    private List<String> imageUrls = new ArrayList<>();

    private RelativeLayout bface_lay;
    private ImageButton face_btn;
    private ImageButton word_btn;
    private Bitmap userIconBitmap;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_ftj_tu_wen);
        SharedPreferences sp = getSharedPreferences("pub", Context.MODE_PRIVATE);
        // 第一次打开这个界面的时候设置为true
        sort = sp.getBoolean("pubsort", false);
    }

    @Override
    protected void onRestart() {
        // 再次返回该界面的时候，查看一下sort的状态
        SharedPreferences sp = getSharedPreferences("pub", Context.MODE_PRIVATE);
        sort = sp.getBoolean("pubsort", false);
        if (sort) {
            adapter.update();
        }

        super.onRestart();
    }

    @Override
    public void initViews() {
        et_suggest = findViewById(R.id.et_suggest);
        tv_length = findViewById(R.id.tv_length);
        iv_upload_1 = findViewById(R.id.iv_upload_1);
        gv_img = findViewById(R.id.gv_img);
        btn_submit = findViewById(R.id.btn_submit);

        TitleBarView title = new TitleBarView();
        title.init(this, getString(R.string.tuwen_title), title, 1, new MyCallBack.OnBackClickListener() {

            @Override
            public void onBackClicked() {
                if ((et_suggest.getText().toString().length() > 0)
                        || Bimp.drr.size() > 0) {

                    TuWenCloseBackDialog backDialog = new TuWenCloseBackDialog(FtjTuWenActivity.this, new TuWenCloseBackDialog.OnChoosedBackListener() {

                        @Override
                        public void onChoosed(int index) {
                            et_suggest.setText("");
                            Bimp.bmp.clear();
                            Bimp.drr.clear();
                            Bimp.max = 0;
                            FileUtils.deleteDir();
                            Bimp.act_bool = true;
                            FtjTuWenActivity.this.finish();
                        }
                    });
                    backDialog.show();

                } else {
                    Bimp.act_bool = true;
                    FtjTuWenActivity.this.finish();

                }
            }
        });

        btn_submit.setOnClickListener(this);
        iv_upload_1.setOnClickListener(this);

        et_suggest.setHorizontallyScrolling(false);
        et_suggest.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() <= 200){
                    tv_length.setText(s.length()+"/200");
                }
                if (TextUtils.isEmpty(et_suggest.getText().toString())) {
                    btn_submit.setBackgroundResource(R.drawable.tuwen_submit_grey);
                    btn_submit.setClickable(false);
                } else {
                    btn_submit.setBackgroundResource(R.drawable.tuwen_submit_blue);
                    btn_submit.setClickable(true);
                }
            }
        });

        init();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //在销毁的时候将tuwen设置为空
        SharedPreferenceUtil.saveString("tuwen", "");
    }

    @Override
    public void initDatas() {
        uploadManager = new UploadManager();
        consultationManager = new ConsultationManager();

    }

    private void init() {
        // 如果为true，则在gridview中显示图片
        if (sort) {
//            if (Bimp.bmp.size() != 0){
//                gv_img.setVisibility(View.VISIBLE);
//            }

            gv_img.setSelector(new ColorDrawable(Color.TRANSPARENT));
            adapter = new GridAdapter(this);
            adapter.update();
            gv_img.setAdapter(adapter);
            // 点击item项
            gv_img.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                    // 如果当前的图片小于9的话，如果当前的位置arg2=在文件夹里面保存的图片的数量，则提示继续选择拍照还是相册
                    if (arg2 == arg0.getChildCount() - 1) {
                        if (arg2 == Bimp.bmp.size()) {
                            // 点击显示上传拍照还是相册
                            AlertDialogSelect alert = new AlertDialogSelect(FtjTuWenActivity.this);
                            Dialog dialog = alert.createAlartDialog("", "");
                            dialog.show();
						/*
                         * new PopupWindows(TuWenActivity.this,
						 * noScrollgridview);
						 */
                        }
                    }
                    // 否则的话，点击该图片，直接查看大图
                    else {
                        Intent intent = new Intent(FtjTuWenActivity.this  ,
                                PhotoActivity.class);
                        intent.putExtra("ID", arg2);
                        startActivity(intent);
                    }
                }
            });
        }
        // 如果为false，则gridview隐藏
        else {
            gv_img.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            //图片上传
            case R.id.iv_upload_1:
                AlertDialogSelect alert = new AlertDialogSelect(FtjTuWenActivity.this);
                Dialog dialog = alert.createAlartDialog("", "");
                dialog.show();
                break;
            //提交
            case R.id.btn_submit:
                final AbstractDefaultHttpHandlerCallback handler = new AbstractDefaultHttpHandlerCallback(this) {
                    @Override
                    protected void onResponseSuccess(Object obj) {
                        new AlertDialogCloce(FtjTuWenActivity.this).createAlartDialog("", "").show();
                    }
                };

                content = et_suggest.getText().toString();
                if (!TextUtils.isEmpty(content)) {
                    if (Bimp.drr.size() > 0) {
                        imageCount = Bimp.drr.size();
                        for (int i = 0; i < Bimp.drr.size(); i++) {
                            String Str = Bimp.drr.get(i).substring(Bimp.drr.get(i).lastIndexOf("/") + 1,
                                    Bimp.drr.get(i).lastIndexOf("."));

                            file = new File(FileUtils.SDPATH + Str + ".jpeg");

                            uploadManager.uploadFile(file, "consultationImage", new AbstractDefaultHttpHandlerCallback(this) {
                                @Override
                                protected void onResponseSuccess(Object obj) {
                                    imageUrls.add((String) obj);
                                    commitImageSuccessCount++;

                                    if (imageCount == commitImageSuccessCount) {
                                        consultationManager.commitConsultation(content, imageUrls, handler);
                                    }
                                }
                            });
                        }
                    } else {
                        consultationManager.commitConsultation(content, null, handler);
                    }
                } else {
                    ToastUtil.invokeShortTimeToast(FtjTuWenActivity.this, getString(R.string.tuwen_illness_desc));
                }
                break;
            default:
                break;
        }
    }

    public String getString(String s) {
        String path = null;
        if (s == null) {
            return "";
        }
        for (int i = s.length() - 1; i > 0; i++) {
            s.charAt(i);
        }
        return path;
    }

    // 拍照保存到指定目录
    public void photo() {

        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            Intent getImageByCamera = new Intent("android.media.action.IMAGE_CAPTURE");

            try {
                startActivityForResult(getImageByCamera, TAKE_PICTURE);
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, "设备没有可用相机", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.user_info_sdkabukeyong), Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * 拍照后保存图片
      */
    private void saveImage(Bitmap bitmap, String savedImageDirPath) {
        try {
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(savedImageDirPath, false));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            bos.flush();
            bos.close();
            path = savedImageDirPath;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 从返回的数据中得到图片
      */
    private void getImageFromData(Intent data) {
        Uri selectedImageUri = data.getData();
        if (selectedImageUri == null) {
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                userIconBitmap = (Bitmap) bundle.get("data");

                File dir = new File(Constant.SAVED_IMAGE_DIR_PATH);

                if (!dir.exists()) {
                    dir.mkdirs();
                }

                saveImage(userIconBitmap, Constant.SAVED_IMAGE_DIR_PATH + "/" + System.currentTimeMillis() + ".jpg");
            }
        } else {

            try {
                userIconBitmap = getThumbnail(selectedImageUri, iv_upload_1.getWidth());
            } catch (IOException e) {
                Logger.e(getClass().getSimpleName(), e);
            }

            ContentResolver cr = getContentResolver();
            String[] projection = {MediaStore.Images.Media.DATA};
            Cursor cursor = cr.query(selectedImageUri, projection, null, null, null);
            if (cursor.moveToFirst()) {
                path = cursor.getString(cursor.getColumnIndex(projection[0]));
            }

            cursor.close();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            // 拍照返回，显示图片
            case TAKE_PICTURE:
                if (requestCode == TAKE_PICTURE && resultCode == RESULT_OK && data != null) {
                    getImageFromData(data);
                    Bimp.drr.add(path);
                }
                break;
            default:
                break;

        }
    }

    //根据uri获取指定大小的bitmap
    public Bitmap getThumbnail(Uri uri, int size) throws IOException {
        InputStream input = this.getContentResolver().openInputStream(uri);

        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inJustDecodeBounds = true;
        onlyBoundsOptions.inDither = true;//optional
        onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional

        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
        input.close();

        if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1))
            return null;
        int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ?
                onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

        double ratio = (originalSize > size) ? (originalSize / size) : 1.0;

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
        bitmapOptions.inDither = true;//optional
        bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional

        input = this.getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
        input.close();
        return bitmap;
    }

    private int getPowerOfTwoForSampleRatio(double ratio) {
        int k = Integer.highestOneBit((int) Math.floor(ratio));
        if (k == 0) {
            return 1;
        }
        else {return k;}
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && biaoqingstate) {

            bface_lay.setVisibility(View.GONE);
            face_btn.setVisibility(View.VISIBLE);
            word_btn.setVisibility(View.GONE);
            biaoqingstate = false;

            return false;

        }

        if (keyCode == KeyEvent.KEYCODE_BACK) {

            if ((et_suggest.getText().toString().length() > 0)
                    || Bimp.drr.size() > 0) {

                TuWenCloseBackDialog backDialog = new TuWenCloseBackDialog(FtjTuWenActivity.this, new TuWenCloseBackDialog.OnChoosedBackListener() {

                    @Override
                    public void onChoosed(int index) {
                        et_suggest.setText("");
                        Bimp.bmp.clear();
                        Bimp.drr.clear();
                        Bimp.max = 0;
                        FileUtils.deleteDir();
                        Bimp.act_bool = true;
                        FtjTuWenActivity.this.finish();
                    }
                });
                backDialog.show();

            } else {
                Bimp.act_bool = true;
                FtjTuWenActivity.this.finish();

            }
        }
        return super.onKeyDown(keyCode, event);
    }

    // gridview适配器
    public class GridAdapter extends BaseAdapter {
        Handler mHandler = new Handler(new Handler.Callback()  {
            @Override
            public boolean handleMessage(Message msg) {
                switch (msg.what) {
                    case 1:
                        adapter.notifyDataSetChanged();
                        break;
                    default:
                        break;
                }
                return false;
            }
        });
        private LayoutInflater inflater; // 视图容器
        private int selectedPosition = -1;// 选中的位置
        private boolean shape;

        public GridAdapter(Context context) {
            inflater = LayoutInflater.from(context);
        }

        public boolean isShape() {
            return shape;
        }

        public void setShape(boolean shape) {
            this.shape = shape;
        }

        public void update() {
            loading();
        }

        @Override
        public int getCount() {
            return Bimp.bmp.size()+1;
        }

        @Override
        public Object getItem(int arg0) {
            return null;
        }

        @Override
        public long getItemId(int arg0) {
            return 0;
        }

        public int getSelectedPosition() {
            return selectedPosition;
        }

        public void setSelectedPosition(int position) {
            selectedPosition = position;
        }

        /**
         * ListView Item设置
         */
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {

                convertView = inflater.inflate(R.layout.item_published_grida,
                        parent, false);
                holder = new ViewHolder();
                holder.image = (ImageView) convertView
                        .findViewById(R.id.item_grida_image);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

//            if (position != Bimp.bmp.size()) {
//                holder.image.setImageBitmap(Bimp.bmp.get(position));
//            }

            if (position < Bimp.bmp.size()) {
                holder.image.setImageBitmap(Bimp.bmp.get(position));
            }else {
                if (Bimp.bmp.size() <= 3) {
                    holder.image.setImageResource(R.mipmap.tu_wen_shangchuang);
                }else {
                    holder.image.setVisibility(View.GONE);
                }
            }

            return convertView;
        }

        public void loading() {
            new Thread(new Runnable() {
                public void run() {
                    while (true) {
                        if (Bimp.max == Bimp.drr.size()) {
                            Message message = new Message();
                            message.what = 1;
                            mHandler.sendMessage(message);
                            break;
                        } else {
                            try {
                                String path = Bimp.drr.get(Bimp.max);
                                Bitmap bm = Bimp.revitionImageSize(path);
                                Bimp.bmp.add(bm);
                                String newStr = path.substring(path.lastIndexOf("/") + 1, path.lastIndexOf("."));
                                FileUtils.saveBitmap(bm, "" + newStr);
                                Bimp.max += 1;
                                Message message = new Message();
                                message.what = 1;
                                mHandler.sendMessage(message);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }).start();
        }

        public class ViewHolder {
            public ImageView image;
        }
    }

    // 选择提示
    public class AlertDialogSelect extends Dialog {

        private View.OnClickListener l;
        private Context context;

        public AlertDialogSelect(Context context) {
            super(context);
            this.context = context;
        }

        public Dialog createAlartDialog(String titletxt, String msg) {
            LayoutInflater inflater = LayoutInflater.from(context);
            View v = inflater.inflate(R.layout.alert_dialog2, null);
            final Dialog dialog = new Dialog(context, R.style.alertdialog);
            dialog.setCancelable(true);
            dialog.setContentView(v);
            TextView tvxiangce = (TextView) v.findViewById(R.id.tvxiangce);
            TextView tvcamera = (TextView) v.findViewById(R.id.tvcamera);
            if (l == null) {
                tvxiangce.setOnClickListener(defaultLinstener(dialog));
                tvcamera.setOnClickListener(defaultLinstener(dialog));
            } else {
                tvxiangce.setOnClickListener(l);
                tvcamera.setOnClickListener(l);

            }
            return dialog;
        }

    private View.OnClickListener defaultLinstener(
            final Dialog dialog) {

        return new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.tvxiangce:
                        dialog.dismiss();
                        // 打开相册
                        // 跳转界面 不关闭当前界面
                        Intent intent = new Intent(FtjTuWenActivity.this,
                                GetPicActivity.class);
                        startActivity(intent);
                        break;

                    case R.id.tvcamera:
                        dialog.dismiss();
                        // 打开照相机
                        //动态开启相机权限
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            int hasWriteContactsPermission = checkSelfPermission(Manifest.permission.CAMERA);
                            if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                                requestPermissions(new String[]{Manifest.permission.CAMERA}, 1);
                                return;
                            }
                        }
                        photo();
                        break;
                    default:
                        break;
                }

            }
        };
    }
}

    // 提交成功关闭提示
    public class AlertDialogCloce extends Dialog {

        private View.OnClickListener l;
        private Context context;

        public AlertDialogCloce(Context context) {
            super(context);
            this.context = context;
        }

        public Dialog createAlartDialog(String titletxt, String msg) {
            LayoutInflater inflater = LayoutInflater.from(context);
            View v = inflater.inflate(R.layout.alert_dialg_cloce, null);
            final Dialog dialog = new Dialog(context, R.style.alertdialog);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setCancelable(true);
            dialog.setContentView(v);
            TextView tvcloce = v.findViewById(R.id.tvcloce);
            if (l == null) {
                tvcloce.setOnClickListener(defaultLinstener(dialog));
            } else {
                tvcloce.setOnClickListener(l);

            }
            return dialog;
        }

        public void setDetermineOnClickListener(
                View.OnClickListener l) {
            this.l = l;
        }

        private View.OnClickListener defaultLinstener(
                final Dialog dialog) {
            return new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    switch (v.getId()) {
                        // 点击确定
                        case R.id.tvcloce:
                            dialog.dismiss();
                            et_suggest.setText("");
                            Bimp.bmp.clear();
                            Bimp.drr.clear();
                            Bimp.max = 0;
                            FileUtils.deleteDir();
                            Bimp.act_bool = true;

                            FtjTuWenActivity.this.finish();
                            break;
                        default:
                            break;
                    }
                }
            };
        }
    }
}
