package com.hcy_futejia.activity;

import android.content.Intent;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.report.RecordManager;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.util.SharedPreferenceUtil;
import com.hxlm.hcyandroid.util.WebViewUtil;

public class BloodMonitorActivity extends BaseActivity  implements OnCompleteListener {

    private WebView wb;
    private int memberChildId = 0;
    private RecordManager recordManager;
    private ProgressBar progressBar;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_blood_monitor);
    }

    @Override
    public void initViews() {

        Intent intent = getIntent();
        String title = intent.getStringExtra("title");
        int blood = intent.getIntExtra("b", 0);

        //初始化titleBar
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, title, titleBar, 1, this);

        wb = findViewById(R.id.wb);
        progressBar = findViewById(R.id.progressbar);

        loadUrl(blood);

    }

    private void loadUrl(int dataType) {
        memberChildId = LoginControllor.getChoosedChildMember().getId();
        recordManager = new RecordManager();
        String url = recordManager.getHsitoryReportUrl(memberChildId + "", dataType);
        Log.e("retrofit","==血压血糖监测=="+url);
        new WebViewUtil().setWebViewInit(wb, progressBar, this, url);
    }

    @Override
    public void initDatas() {

    }

    @Override
    public void onComplete() {
        memberChildId = LoginControllor.getChoosedChildMember().getId();
    }
}
