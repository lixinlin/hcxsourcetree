package com.hcy_futejia.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hcy_futejia.adapter.QuestionAdapter;
import com.hcy_futejia.widget.SpacesItemDecoration;
import com.hxlm.android.hcy.AbstractBaseActivity;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.questionnair.Answer;
import com.hxlm.android.hcy.questionnair.Question;
import com.hxlm.android.hcy.questionnair.QuestionnaireManager;
import com.hxlm.android.hcy.questionnair.Result;
import com.hxlm.android.hcy.report.Report;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.tabbar.archives.ReportInfoActivity;

import net.sourceforge.simcpux.Constants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FtjBodyQuestionActivity extends AbstractBaseActivity implements View.OnClickListener {
    //    https://blog.csdn.net/liao5214/article/details/80929881
    private RecyclerView rec_quesion;
    private TextView tv_question_bufen;
    private TextView answer1;
    private TextView answer2;
    private TextView answer3;
    private TextView answer4;
    private TextView answer5;
    private QuestionnaireManager questionnaireManager;
    private LinearLayoutManager linearLayoutManager;
    private QuestionAdapter questionAdapter;
    private List<Question> questions;
    private List<Question> questions_shown;
    private String TAG = "ftj_quesiton";

    private int currentPosition;
    private int first_item_on_screen_tag;
    private int tv_bufen_bottom;
    private String categorySn;
    private Result result;
    private List<Answer> defaultAnswers;
    private List<Question> fenbuSortQuestions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    protected void setContentView() {
        setContentView(R.layout.activity_ftj_body_question);
    }

    @Override
    protected void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.home_physical_identification), titleBar, 1);

        tv_question_bufen = findViewById(R.id.tv_question_bufen);
        answer1 = findViewById(R.id.answer1);
        answer2 = findViewById(R.id.answer2);
        answer3 = findViewById(R.id.answer3);
        answer4 = findViewById(R.id.answer4);
        answer5 = findViewById(R.id.answer5);
        rec_quesion = findViewById(R.id.rcv_question);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rec_quesion.setLayoutManager(linearLayoutManager);
        rec_quesion.addItemDecoration(new SpacesItemDecoration(18));
        rec_quesion.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);
        rec_quesion.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == 0) {
                    View childAt = rec_quesion.getChildAt(0);
                    first_item_on_screen_tag = (int) childAt.getTag();
                    Log.d(TAG, "onScrollStateChanged: " + first_item_on_screen_tag);
                    if (first_item_on_screen_tag >= currentPosition) {
                        changeCurrentPostion(questions_shown.size() - 1);
                    }
//                    change_ui_bufen();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                Log.d(TAG, "onScrolled: ");
                changeCurrentPostion(questions_shown.size() - 1);
//                change_ui_bufen();
            }
        });
        answer1.setOnClickListener(this);
        answer2.setOnClickListener(this);
        answer3.setOnClickListener(this);
        answer4.setOnClickListener(this);
        answer5.setOnClickListener(this);
        int[] tv_question_bufen_location = new int[2];
        tv_question_bufen.getLocationOnScreen(tv_question_bufen_location);
        int tv_question_bufen_height = tv_question_bufen.getHeight();
        tv_bufen_bottom = tv_question_bufen_location[1] + tv_question_bufen_height;
    }

    private void changeCurrentPostion(int postion) {
        //适配器的postion和activity中的postion要一致。
        questionAdapter.setCurrentPosition(postion);
        currentPosition = postion;
        questionAdapter.notifyDataSetChanged();
        change_ui_bufen();
    }

    private void change_ui_bufen() {
        Question question_one = null;
        Question question_second = null;
        View childAt_0 = rec_quesion.getChildAt(0);
        View childAt_1 = rec_quesion.getChildAt(1);
        if (childAt_1 != null && childAt_0 != null) {
            //tv_bufen 的数据
            int[] tv_question_bufen_location = new int[2];
            tv_question_bufen.getLocationOnScreen(tv_question_bufen_location);
            int tv_question_bufen_height = tv_question_bufen.getHeight();
            tv_bufen_bottom = tv_question_bufen_location[1] + tv_question_bufen_height;
            //条目数据
            int second_tiem_in_screen = (int) childAt_1.getTag();
            question_second = questions_shown.get(second_tiem_in_screen);
            first_item_on_screen_tag = (int) childAt_0.getTag();
            question_one = questions_shown.get(first_item_on_screen_tag);

//            if (question_second.isChang_bufen()) {
            if (true) {
                int[] childAt_0_layout_location = new int[2];
                childAt_0.getLocationOnScreen(childAt_0_layout_location);
                if (childAt_0_layout_location[1] < tv_bufen_bottom+1 - childAt_0.getHeight() / 1) {
                    //childat_0与tv_bufen相交，改变tv_bufen,隐藏child_1的部分布局；
                    setFenBuTiele(question_second);
                    childAt_1.findViewById(R.id.tv_bufen).setVisibility(View.INVISIBLE);
                } else {
                    setFenBuTiele(question_one);
                    childAt_1.findViewById(R.id.tv_bufen).setVisibility(View.VISIBLE);
                }
            }
        }
        if (childAt_0 != null&&childAt_1 == null) {
            first_item_on_screen_tag = (int) childAt_0.getTag();
            question_one = questions_shown.get(first_item_on_screen_tag);
            if (question_one.isChang_bufen()) {
                childAt_0.findViewById(R.id.tv_bufen).setVisibility(View.GONE);
                setFenBuTiele(question_one);
            }
        }

    }

    private void setFenBuTiele(Question question) {
        switch (question.getClassifyId()) {
            case 0:
                tv_question_bufen.setText(getString(R.string.ftj_question_a_diyibufen));
                break;
            case 1:
                tv_question_bufen.setText(getString(R.string.ftj_question_a_dierbufen));
                break;
            case 2:
                tv_question_bufen.setText(getString(R.string.ftj_question_a_disanbufen));
                break;
            case 3:
                tv_question_bufen.setText(getString(R.string.ftj_question_a_disibufen));
                break;
        }

    }

    @Override
    protected void initDatas() {
        categorySn = getIntent().getStringExtra("categorySn");
        questionnaireManager = new QuestionnaireManager();

        String categorySn = QuestionnaireManager.SN_TZBS;
        // 调用接口，获取答案和问卷
        questionnaireManager.getQuestionnaires(categorySn, new AbstractDefaultHttpHandlerCallback(this) {
            @Override
            protected void onResponseSuccess(Object obj) {
                questions = (List<Question>) obj;
                questions_shown = new ArrayList<>();
                fenbuSortQuestions = pre_handle(FtjBodyQuestionActivity.this.questions);
                if (fenbuSortQuestions != null && fenbuSortQuestions.size() > 0) {
                    handle_questions(fenbuSortQuestions);
                }
                defaultAnswers = questionnaireManager.getDefaultAnswers();
                Collections.sort(defaultAnswers);
            }
        });
    }

    private List<Question> pre_handle(List<Question> questions) {
        //增加一些UI展示需要添加的控制字段
        List<Question> bufen_one = new ArrayList<>();
        List<Question> bufen_two = new ArrayList<>();
        List<Question> bufen_three = new ArrayList<>();
        List<Question> bufen_four = new ArrayList<>();
        for (int i = 0; i < questions.size(); i++) {
            Question question = questions.get(i);
            switch (question.getClassifyId()) {
                case 0:
                    bufen_one.add(question);
                    break;
                case 1:
                    bufen_two.add(question);
                    break;
                case 2:
                    bufen_three.add(question);
                    break;
                case 3:
                    bufen_four.add(question);
                    break;
            }
        }
        try {
            Collections.sort(bufen_one);
            Collections.sort(bufen_two);
            Collections.sort(bufen_three);
            Collections.sort(bufen_four);
            bufen_one.get(0).setChang_bufen(true);
            bufen_two.get(0).setChang_bufen(true);
            bufen_three.get(0).setChang_bufen(true);
            bufen_four.get(0).setChang_bufen(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<Question> fenbuSortQuestions = new ArrayList<>();
        fenbuSortQuestions.addAll(bufen_one);
        fenbuSortQuestions.addAll(bufen_two);
        fenbuSortQuestions.addAll(bufen_three);
        fenbuSortQuestions.addAll(bufen_four);
        for (int i = 0; i < fenbuSortQuestions.size(); i++) {
            fenbuSortQuestions.get(i).setNumber_ui(i + 1);
        }
        return fenbuSortQuestions;

    }

    private void handle_questions(List<Question> questions) {
        if (questionAdapter == null) {
            questions_shown.add(questions.get(0));
            change_ui_bufen();
            questionAdapter = new QuestionAdapter(this, questions_shown, questionnaireManager.getDefaultAnswers());
        }
        rec_quesion.setAdapter(questionAdapter);
        questionAdapter.setList(questions_shown);
        questionAdapter.setOnItemClickListener(new QuestionAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position, View itemLayout) {
                changeCurrentPostion(position);

            }
        });
    }

    private int count;

    @Override
    public void onClick(View v) {
        int chose = 99;
        switch (v.getId()) {
            case R.id.answer1:
                chose = 1;
                break;
            case R.id.answer2:
                chose = 2;
                break;
            case R.id.answer3:
                chose = 3;
                break;
            case R.id.answer4:
                chose = 4;
                break;
            case R.id.answer5:
                chose = 5;
                break;
        }
        if (currentPosition == 0 || currentPosition == count) {
            question_set_answer(questions_shown.get(questionAdapter.getCurrentPosition()), chose);
//            questions_shown.get(questionAdapter.getCurrentPosition()).setAnswer_int(chose);
            if (count < questions.size() - 1) {
                count++;
            } else {
                //提交
                saveAnswersAndGetReport();
            }
            questions_shown.add(fenbuSortQuestions.get(count));
            questionAdapter.setCurrentPosition(count);
            questionAdapter.notifyDataSetChanged();
            rec_quesion.scrollToPosition(count);
            currentPosition = count;//恢复到继续答题，非修改状态
        } else {
            question_set_answer(questions_shown.get(currentPosition), chose);
            questionAdapter.notifyDataSetChanged();
        }
    }

    private void question_set_answer(Question question, int chose) {
        //用于UI显示
        question.setAnswer_int(chose);
        //用于计算分值与提交
        List<Answer> answers = new ArrayList<>();
        if (question.getAnswers() != null) {
            answers = question.getAnswers();
            Collections.sort(answers);
        } else {
            answers = defaultAnswers;
        }
        Answer answer = answers.get(chose - 1);
        question.setAnswer(answer);
    }

    private boolean submited;

    private void saveAnswersAndGetReport() {
        //防止多次提交
        if (submited) {
            return;
        }
        submited = true;
        result = questionnaireManager.getResult(categorySn);
        //判断是否登录
        LoginControllor.requestLogin(FtjBodyQuestionActivity.this, new OnCompleteListener() {
            @Override
            public void onComplete() {
                questionnaireManager.saveAnswersAndGetReport(result.getSubjectSn(),
                        new AbstractDefaultHttpHandlerCallback(FtjBodyQuestionActivity.this) {
                            @Override
                            protected void onResponseSuccess(Object obj) {
                                Report report = (Report) obj;
                                Log.i("report", report.toString());
                                Constants.needRefesh = true;
                                if (categorySn.equals(QuestionnaireManager.SN_TZBS)) {
                                    Intent intent = new Intent(FtjBodyQuestionActivity.this, ReportInfoActivity.class);//ReportActivity
                                    intent.putExtra("subjectId", report.getSubject().getSubject_sn());
                                    intent.putExtra("title",getString(R.string.report_title_tz));
                                    startActivity(intent);
                                    finish();
                                }
                            }

                            @Override
                            protected void onNetworkError() {
                                super.onNetworkError();
                                submited = false;
                                Log.i("reporton","NetworkError" );
                            }

                            @Override
                            protected void onResponseError(int errorCode, String errorDesc) {
                                super.onResponseError(errorCode, errorDesc);
                                submited = false;
                                Log.i("reporton","onResponseError" );
                            }

                            @Override
                            protected void onHttpError(int httpErrorCode) {
                                super.onHttpError(httpErrorCode);
                                submited = false;
                                Log.d(TAG, "reporton: onHttpError");
                            }
                        }

                        );
            }

        });
    }


}
