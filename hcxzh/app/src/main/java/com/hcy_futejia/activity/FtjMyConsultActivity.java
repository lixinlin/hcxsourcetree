package com.hcy_futejia.activity;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.hcy.ky3h.R;
import com.hcy_futejia.adapter.FtjMyConsultationAdapter;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.consult.ConsultationManager;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.bean.MyConsultation;

import java.util.List;

/**
 * @author Lirong
 */
public class FtjMyConsultActivity extends BaseActivity implements AdapterView.OnItemClickListener {

    private RelativeLayout rl_no_data;
    private ListView lv_my_consults;
    private List<MyConsultation> myConsultations;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_ftj_my_consult);
    }

    @Override
    public void initViews() {

        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.my_consult_wodezixun), titleBar, 1);

        rl_no_data = findViewById(R.id.rl_no_data);
        lv_my_consults = findViewById(R.id.lv_my_consults);
        lv_my_consults.setOnItemClickListener(this);
    }

    @Override
    public void initDatas() {
        new ConsultationManager().getConsultation(new AbstractDefaultHttpHandlerCallback(this) {
            @Override
            protected void onResponseSuccess(Object obj) {
                myConsultations = (List<MyConsultation>) obj;
                if (myConsultations.size() != 0) {
                    rl_no_data.setVisibility(View.GONE);
                    lv_my_consults.setVisibility(View.VISIBLE);
                    lv_my_consults.setAdapter(new FtjMyConsultationAdapter(FtjMyConsultActivity.this, myConsultations));
                }else {
                    lv_my_consults.setVisibility(View.GONE);
                    rl_no_data.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent i = new Intent(FtjMyConsultActivity.this,
                FtjMyConsultationInfoActivity.class);
        i.putExtra("myConsultation", myConsultations.get(position));
        startActivity(i);
    }
}
