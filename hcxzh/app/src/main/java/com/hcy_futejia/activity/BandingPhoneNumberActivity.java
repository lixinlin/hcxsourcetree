package com.hcy_futejia.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.user.Member;
import com.hxlm.android.hcy.user.UserManager;
import com.hxlm.android.hcy.utils.MyCountTimer;
import com.hxlm.android.hcy.utils.SpUtils;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.util.IsMobile;
import com.hxlm.hcyandroid.util.ToastUtil;

public class BandingPhoneNumberActivity extends BaseActivity implements View.OnClickListener {

    private EditText et_input_phone_number;
    private EditText et_input_yzm;
    private TextView tv_get_yzm;
    private Button btn_confirm;
    private UserManager userManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_banding_phone_number);
    }

    @Override
    public void initViews() {

        TextView titleBar_tv = findViewById(R.id.tv_title);
        titleBar_tv.setText("关联手机号");
        LinearLayout ll_back = findViewById(R.id.linear_back_title);
        ll_back.setOnClickListener(this);
        et_input_phone_number = findViewById(R.id.et_input_phone_number);
        et_input_yzm = findViewById(R.id.et_input_yzm);
        tv_get_yzm = findViewById(R.id.tv_get_yzm);
        btn_confirm = findViewById(R.id.btn_confirm);
        tv_get_yzm.setOnClickListener(this);
        btn_confirm.setOnClickListener(this);
    }

    @Override
    public void initDatas() {
        userManager = new UserManager();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_get_yzm:
                getYZM();
                break;
            case R.id.btn_confirm:
                bind();
                break;
            case R.id.linear_back_title:
                this.finish();
                break;
            default:
                break;

        }
    }

    private void bind() {
        String yzm = et_input_yzm.getText().toString();
        String phone = et_input_phone_number.getText().toString();
        Member loginMember = LoginControllor.getLoginMember();
        String username = loginMember.getUsername();
        if (TextUtils.isEmpty(yzm) || TextUtils.isEmpty(phone)) {
            ToastUtil.invokeShortTimeToast(this, getString(R.string.login_phone_sms_empty));
        } else {
            //验证手机号
            boolean idMobile = IsMobile.isMobileNO(phone);
            if (idMobile) {
//                username = "oD48ct6A_KXo-Y2dNq7TUOvy2aM4"; 测试用的微信号
                new UserManager().bindPhoneNumber(this, phone,username, yzm, new AbstractDefaultHttpHandlerCallback(this) {
                    @Override
                    protected void onResponseSuccess(Object obj) {
                        loginMember.setUsername(phone);
                        BandingPhoneNumberActivity.this.finish();
                    }
                });
            } else {
                ToastUtil.invokeShortTimeToast(this, getString(R.string.login_phone_format_error));
            }
        }
    }

    public void getYZM() {
        String account = et_input_phone_number.getText().toString();
        if (!TextUtils.isEmpty(account)) {
            //验证手机号
            boolean idMobile = IsMobile.isMobileNO(account);
            if (idMobile) {
                new UserManager().getCaptchaAgain(account, new AbstractDefaultHttpHandlerCallback(this) {
                    @Override
                    protected void onResponseSuccess(Object obj) {
                        ToastUtil.invokeShortTimeToast(getContext(), getString(R.string.login_has_been_send));
                        MyCountTimer myCountTimer = new MyCountTimer(BandingPhoneNumberActivity.this, 60000, 1000, tv_get_yzm, 2);
                        myCountTimer.start();
                        tv_get_yzm.setTextColor(Color.parseColor("#9A9B9B"));
                        tv_get_yzm.setClickable(false);
                        myCountTimer.setCountTimerFinishListener(new MyCountTimer.OnCountTimerFinishListener() {
                            @Override
                            public void onTimerFinish() {
                                tv_get_yzm.setTextColor(Color.parseColor("#1E82D2"));
                                tv_get_yzm.setClickable(true);
                                tv_get_yzm.setText(getString(R.string.bt_get_test_number));
                            }
                        });
                    }
                });
            } else {
                ToastUtil.invokeShortTimeToast(this, getString(R.string.login_phone_format_error));
            }
        } else {
            ToastUtil.invokeShortTimeToast(this, this.getString(R.string.username_is_empty));
        }
    }

}
