package com.hcy_futejia.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.hcy.ky3h.R;
import com.hcy_futejia.adapter.FtjYueYaoListAdapter;
import com.hxlm.android.comm.AbstractDeviceActivity;
import com.hxlm.android.comm.AbstractIOSession;
import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.comm.Error;
import com.hxlm.android.comm.Error_English;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.content.ResourceItem;
import com.hxlm.android.hcy.content.YueYaoFirstSplashActivity;
import com.hxlm.android.hcy.content.YueYaoManager;
import com.hxlm.android.hcy.content.YueYaoStaUpdateBroadcast;
import com.hxlm.android.hcy.content.YueyaojiesuanActivity;
import com.hxlm.android.hcy.order.PayDialog;
import com.hxlm.android.hcy.report.RecordManager;
import com.hxlm.android.hcy.report.Report;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;
import com.hxlm.android.health.device.message.auricular.AuricularBlueCommand;
import com.hxlm.android.health.device.message.auricular.AuricularBlueResponseMessage;
import com.hxlm.android.health.device.model.AuricularBlueModel;
import com.hxlm.android.utils.Logger;
import com.hxlm.hcyandroid.BaseApplication;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.IdentityCategory;
import com.hxlm.hcyandroid.callback.MyCallBack;
import com.hxlm.hcyandroid.util.SharedPreferenceUtil;
import com.hxlm.hcyandroid.util.StatusBarUtils;
import com.hxlm.hcyandroid.util.ToastUtil;
import com.hxlm.hcyandroid.view.ConfirmDialog;
import com.hxlm.hcyandroid.view.SwitchButton;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lir
 */
public class FtjYueYaoActivity extends AbstractDeviceActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener{

    private final static String TAG = "YueYaoActivity";
    private static final int DEFAULT_TYPE = 0;
    private static final int NO_POSITION = -1;

    // 该sub type里数组的顺序和上面的tab图片一致
    private static final String[][] SUBTYPE_NAME = {
            {BaseApplication.getContext().getString(R.string.gongfa_gong_shao), BaseApplication.getContext().getString(R.string.gongfa_gong_zuo), BaseApplication.getContext().getString(R.string.gongfa_gong_shang), BaseApplication.getContext().getString(R.string.gongfa_gong_jia), BaseApplication.getContext().getString(R.string.gongfa_gong_da)},
            {BaseApplication.getContext().getString(R.string.gongfa_shang_shao), BaseApplication.getContext().getString(R.string.gongfa_shang_zuo), BaseApplication.getContext().getString(R.string.gongfa_shang_shang), BaseApplication.getContext().getString(R.string.gongfa_shang_you),BaseApplication.getContext().getString(R.string.gongfa_shang_tai)},
            {BaseApplication.getContext().getString(R.string.gongfa_jue_shao), BaseApplication.getContext().getString(R.string.gongfa_jue_pan), BaseApplication.getContext().getString(R.string.gongfa_jue_shang), BaseApplication.getContext().getString(R.string.gongfa_jue_tai), BaseApplication.getContext().getString(R.string.gongfa_jue_da)},
            {BaseApplication.getContext().getString(R.string.gongfa_zhi_shao), BaseApplication.getContext().getString(R.string.gongfa_zhi_pan), BaseApplication.getContext().getString(R.string.gongfa_zhi_shang), BaseApplication.getContext().getString(R.string.gongfa_zhi_you), BaseApplication.getContext().getString(R.string.gongfa_zhi_zhi)},
            {BaseApplication.getContext().getString(R.string.gongfa_yu_shao), BaseApplication.getContext().getString(R.string.gongfa_yu_zhi), BaseApplication.getContext().getString(R.string.gongfa_yu_shang), BaseApplication.getContext().getString(R.string.gongfa_yu_zhong), BaseApplication.getContext().getString(R.string.gongfa_yu_da)}};
    private static final String[][] SUBTYPE_SN = {
            {"JLBS-G4", "JLBS-G5", "JLBS-G3", "JLBS-G2", "JLBS-G1"},
            {"JLBS-S2", "JLBS-S5", "JLBS-S1", "JLBS-S4", "JLBS-S3"},
            {"JLBS-J4", "JLBS-J2", "JLBS-J3", "JLBS-J5", "JLBS-J1"},
            {"JLBS-Z3", "JLBS-Z1", "JLBS-Z2", "JLBS-Z4", "JLBS-Z5"},
            {"JLBS-Y3", "JLBS-Y4", "JLBS-Y2", "JLBS-Y5", "JLBS-Y1"}};


    private int typeIndex = DEFAULT_TYPE;// 默认 宫 的type
    private int subtypeIndex = DEFAULT_TYPE;// 当前显示第几个tab
    private TitleBarView mTitleView;


    private RadioButton[] yueyaoTypeBtns;
    private RadioButton[] yueyaoSubTypeBtns;
    private TextView mInfoTv;
    private TextView tv_appointment_money_to_pay_down;
    private ListView yueyaoItemsListView;
    private FtjYueYaoListAdapter yueYaoListAdapter;


    //--------------------------耳针仪
    private LinearLayout linear_bluetooth;
    private ImageButton imgbtn_bluetooth_state;//蓝牙连接状态
    private Button imgbtn_strenght_plus;//强度增大
    private Button imgbtn_strenght_reduce;//强度减小
    private TextView text_strength;//强度显示
    private SwitchButton switchButton;//电刺激开关按钮
    private ImageButton img_bluetooth_open;//蓝牙开
    private ImageButton img_bluetooth_close;//蓝牙关

    private boolean bluetoothState=false;//判断当前蓝牙是否处于连接状态

    private byte strength = 0;// 电流强度
    private String title;//显示标题

    private boolean isComeFromReport = false;
    private YueYaoManager yueYaoManager;
    private YueYaoStaUpdateBroadcast yaoStaUpdateBroadcast;//乐药支付成功之后，刷新当前页面的乐药状态

    private boolean isPlaying = false;//判断乐药是否正在播放
    private boolean isPause = false;//对于电话的监听判断
    private ResourceItem resourceItem;//记录当前播放的乐药
    private boolean moveTaskToBack;


    private final Handler serviceHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            AuricularBlueCommand blueCommand = new AuricularBlueCommand();

            switch (msg.what) {
                //播放停止
                case YueYaoManager.STOP:
                    Logger.d(TAG, "播放停止");
                    isPlaying=false;
                    yueYaoListAdapter.notifyDataSetChanged();

                    //ToastUtil.invokeShortTimeToast(YueYaoActivity.this,"播放停止isPlaying--"+isPlaying);

                    //是否是乐洛仪
                    if(getString(R.string.web_leluoyi).equals(title))
                    {
                        //关闭电刺激
                        setBlueToothClose();
                    }
                    break;
                //正在播放乐药
                case YueYaoManager.PLAYING:
                    Logger.d(TAG, "播放歌曲中");
                    isPlaying=true;
                    yueYaoListAdapter.notifyDataSetChanged();


                    //ToastUtil.invokeShortTimeToast(YueYaoActivity.this,"播放歌曲中isPlaying--"+isPlaying);

                    if(getString(R.string.web_leluoyi).equals(title))
                    {
                        //打开电刺激
                        setBlueToothOpen();
                    }
                    break;
                //乐药更新
                case YueYaoManager.REFRESH_LIST:
                    yueYaoListAdapter.notifyDataSetChanged();
                    break;
                //乐药下载完成
                case YueYaoManager.REFRESH_COMPLETE:
//                    if(moveTaskToBack&&yueYaoManager!=null&&yueYaoManager.getDownLoadingItemSize()==0){
//                        YueYaoActivity.this.finish();
//                    }
                    yueYaoListAdapter.notifyDataSetChanged();
                    break;

                //乐药支付成功之后，刷新当前页面的乐药状态
                case YueYaoStaUpdateBroadcast.IS_YUEYAO_STAUPDATE:

                    //刷新底部乐药的总价格
                    Log.e("price","====yueYaoManager.getPrice()==="+yueYaoManager.getPrice());
                    if (yueYaoManager.getPrice() != 0) {
                        ll_bottom_cart.setVisibility(View.VISIBLE);
                        tv_appointment_money_to_pay_down.setText("￥" +
                                Constant.decimalFormat.format(yueYaoManager.getPrice()));
                    }

                    //根据当前的typeIndex和subtypeIndex，重新请求数据
                    yueyaoTypeBtns[typeIndex].setChecked(true);
                    yueyaoTypeBtns[typeIndex].setTextSize(TypedValue.COMPLEX_UNIT_SP,24);
                    if (yueyaoSubTypeBtns[subtypeIndex].isChecked()) {
                        refreshYueyaoListView(SUBTYPE_SN[typeIndex][subtypeIndex]);
                    } else {
                        yueyaoSubTypeBtns[subtypeIndex].setChecked(true);
                    }
                    break;
                default:
                    break;
            }

            return false;
        }
    });
    private LinearLayout ll_bottom_cart;

    @Override
    public void setContentView() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        StatusBarUtils.setWindowStatusBarColor(this, R.color.white);
        setContentView(R.layout.activity_ftj_yue_yao);

        //添加Session
        ioSession = new AuricularBlueModel().getIOSession(this);


        //根据报告的类型进行跳转到相应的报告类型的乐药
        Intent intent = getIntent();
        if (intent != null) {
            setBaogaoType(intent.getStringExtra("baogaoType"));
        }

        yueYaoManager = new YueYaoManager(serviceHandler);


        // 动态注册广播 防止重复注册多个广播 乐药支付成功之后，刷新当前页面的乐药状态
        if (yaoStaUpdateBroadcast == null) {
            yaoStaUpdateBroadcast = new YueYaoStaUpdateBroadcast(serviceHandler);
            registerReceiver(yaoStaUpdateBroadcast, new IntentFilter(
                    YueYaoStaUpdateBroadcast.ACTION));
        }


    }

    @Override
    public void initViews() {
        title = getIntent().getStringExtra("title");

        if (mTitleView == null) {
            mTitleView = new TitleBarView();
            mTitleView.init(FtjYueYaoActivity.this, title, mTitleView, 1, new OnCompleteListener() {
                @Override
                public void onComplete() {
                    //选择完之后，根据子账户获取经络辨识结果，只需要第一条数据
                    getReportBySn(LoginControllor.getChoosedChildMember().getId());
                }
            }, new MyCallBack.OnBackClickListener() {
                @Override
                public void onBackClicked() {
                    //如点击返回发送关闭指令
                    //是否是乐洛仪
                    if(getString(R.string.web_leluoyi).equals(title))
                    {
                        //关闭电刺激
                        setBlueToothClose();
                    }
                    if(yueYaoManager.getDownLoadingItemSize()==0){
                        FtjYueYaoActivity.this.finish();
                    }else {
                        FtjYueYaoActivity.this.finish();
                    }

                    //去掉系统自带activity跳转动画
                    FtjYueYaoActivity.this.overridePendingTransition(0, 0);
                }
            });
        }

        //初始化数据
        initYueyaoListView();
        initTypeChoiceRg();

        LoginControllor.chooseChildMember(FtjYueYaoActivity.this, new OnCompleteListener() {
            @Override
            public void onComplete() {
                //选择完之后，根据子账户获取经络辨识结果，只需要第一条数据
                getReportBySn(LoginControllor.getChoosedChildMember().getId());
            }
        });
    }



    @Override
    protected void onResume() {
        super.onResume();
        moveTaskToBack = false;
//        yueyaoTypeBtns[typeIndex].setChecked(true);
//        if (yueyaoSubTypeBtns[subtypeIndex].isChecked()) {
//            refreshYueyaoListView(SUBTYPE_SN[typeIndex][subtypeIndex]);
//        }

        //刷新底部乐药的总价格
        Log.e("price","====yueYaoManager.getPrice()==="+yueYaoManager.getPrice());
        if (yueYaoManager.getPrice() != 0) {
            ll_bottom_cart.setVisibility(View.VISIBLE);
            tv_appointment_money_to_pay_down.setText("￥" +
                    Constant.decimalFormat.format(yueYaoManager.getPrice()));
        }else {
            ll_bottom_cart.setVisibility(View.GONE);
            tv_appointment_money_to_pay_down.setText("￥" +
                    Constant.decimalFormat.format(yueYaoManager.getPrice()));
        }



        //刷新蓝牙的状态
        if(getString(R.string.web_leluoyi).equals(title))
        {
            linear_bluetooth.setVisibility(View.VISIBLE);

            //进入就去连接
            if (ioSession == null) {
                ToastUtil.invokeShortTimeToast(FtjYueYaoActivity.this, "蓝牙初始化失败！");
            } else if (ioSession.status != AbstractIOSession.Status.CONNECTED) {
                ioSession.connect();
            }

            //------------------------------------------------------
            //根据标识判断是否是第一次打开评估页面
            String strFirst = SharedPreferenceUtil.getString("isFirstYueYao");
            if (TextUtils.isEmpty(strFirst)) {
                Intent intent = new Intent(FtjYueYaoActivity.this, YueYaoFirstSplashActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                //跳转第一次引导页
                SharedPreferenceUtil.saveString("isFirstYueYao", "false");
            }
            //--------------------------------------------------------------

        }else
        {
            linear_bluetooth.setVisibility(View.GONE);
        }
        try {
            refreshYueyaoListView(SUBTYPE_SN[typeIndex][subtypeIndex]);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        //当看不到当前页面时，删除保存的在结算购物车里面保存的乐药
        if(Constant.LIST_DELETE_YUEYAO.size()>0)
        {
            Constant.LIST_DELETE_YUEYAO.clear();
        }
    }
    @Override
    public void initDatas() {
        //添加电话监听命令，当有来电时关闭电流刺激，接听完打开电流刺激
        createPhoneListener();

    }

    /**
     * 按钮-监听电话
     */
    public void createPhoneListener() {
        TelephonyManager telephony = (TelephonyManager)getSystemService(
                Context.TELEPHONY_SERVICE);
        telephony.listen(new OnePhoneStateListener(),
                PhoneStateListener.LISTEN_CALL_STATE);
    }

    /**
     * 电话状态监听.
     * @author stephen
     *
     */
    class OnePhoneStateListener extends PhoneStateListener{
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            Logger.i(TAG, "[Listener]电话号码:"+incomingNumber);
            switch(state){
                case TelephonyManager.CALL_STATE_RINGING:
                    Logger.i(TAG, "[Listener]等待接电话:"+incomingNumber);
                    //等待接听电话停止播放音乐
                    if(isPlaying)
                    {
                        isPause=true;//当前正在等待接听
                        yueYaoManager.stopPlayer();
                    }

                    break;
                case TelephonyManager.CALL_STATE_IDLE:
                    Logger.i(TAG, "[Listener]电话挂断:"+incomingNumber);
                    //电话挂断，继续播放乐药
                    if(isPause&&!isPlaying)
                    {
                        isPause=false;//已挂断电话
                        yueYaoManager.playAudio(resourceItem);
                    }

                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    Logger.i(TAG, "[Listener]通话中:"+incomingNumber);
                    break;
                default:
                    break;
            }
            super.onCallStateChanged(state, incomingNumber);
        }
    }

    //通过结论编码获取报告
    private void getReportBySn(final int memberChildId) {
        new RecordManager().getReportBySn(memberChildId, IdentityCategory.MERIDIAN_IDENTITY,
                new AbstractDefaultHttpHandlerCallback(FtjYueYaoActivity.this) {
                    @Override
                    protected void onResponseSuccess(Object obj) {
                        List<Report> listReports = (List<Report>) obj;

                        //有数据
                        if (listReports != null && listReports.size() > 0) {

                            setBaogaoType(listReports.get(0).getSubject().getName());

                            //根据报告的类型进行跳转到相应的报告类型的乐药
                            yueyaoTypeBtns[typeIndex].setChecked(true);
                            yueyaoTypeBtns[typeIndex].setTextSize(TypedValue.COMPLEX_UNIT_SP,24);
                            if (yueyaoSubTypeBtns[subtypeIndex].isChecked()) {
                                refreshYueyaoListView(SUBTYPE_SN[typeIndex][subtypeIndex]);
                            } else {
                                yueyaoSubTypeBtns[subtypeIndex].setChecked(true);
                            }

                        } else {
                            typeIndex = DEFAULT_TYPE;// 默认 宫 的type
                            subtypeIndex = DEFAULT_TYPE;// 当前显示第几个tab
                            //默认选中从0开始
                            yueyaoTypeBtns[typeIndex].setChecked(true);
                            yueyaoTypeBtns[typeIndex].setTextSize(TypedValue.COMPLEX_UNIT_SP,24);
                            if (yueyaoSubTypeBtns[subtypeIndex].isChecked()) {
                                refreshYueyaoListView(SUBTYPE_SN[typeIndex][subtypeIndex]);
                            } else {
                                yueyaoSubTypeBtns[subtypeIndex].setChecked(true);
                            }
                        }
                    }
                });
    }

    private void setBaogaoType(String baogaoType) {
        if (!TextUtils.isEmpty(baogaoType)) {
            for (int i = 0; i < SUBTYPE_NAME.length; i++) {
                String[] aSubtypeNames = SUBTYPE_NAME[i];

                for (int j = 0; j < aSubtypeNames.length; j++) {
                    if (aSubtypeNames[j].equals(baogaoType)) {
                        typeIndex = i;
                        subtypeIndex = j;
                        isComeFromReport = true;
                        break;
                    }
                }
            }
        }
    }

    private void initYueyaoListView() {
        tv_appointment_money_to_pay_down =  findViewById(R.id.tv_appointment_moey_to_pay_down);
        TextView tv_pay = findViewById(R.id.tv_pay_away);
        tv_pay.setOnClickListener(this);

        //耳针仪
        linear_bluetooth =  findViewById(R.id.linear_bluetooth);
        imgbtn_bluetooth_state =  findViewById(R.id.imgbtn_bluetooth_state);
        imgbtn_strenght_plus =  findViewById(R.id.imgbtn_strenght_plus);
        text_strength =  findViewById(R.id.text_strength);
        imgbtn_strenght_reduce =  findViewById(R.id.imgbtn_strenght_reduce);
        switchButton =  findViewById(R.id.switch_button);
        img_bluetooth_open= findViewById(R.id.img_bluetooth_open);
        img_bluetooth_close= findViewById(R.id.img_bluetooth_close);
        ll_bottom_cart = findViewById(R.id.ll_bottom_cart);

        imgbtn_bluetooth_state.setOnClickListener(this);
        imgbtn_strenght_plus.setOnClickListener(this);
        imgbtn_strenght_reduce.setOnClickListener(this);
        switchButton.setOnCheckedChangeListener(this);
        img_bluetooth_open.setOnClickListener(this);
        img_bluetooth_close.setOnClickListener(this);
        text_strength.setText(String.valueOf(strength));

        mInfoTv = findViewById(R.id.info_tv);
        yueyaoItemsListView = findViewById(R.id.lv_music);

        yueYaoListAdapter = new FtjYueYaoListAdapter(this);
        yueYaoManager.setCartItems(Constant.LIST);
        yueyaoItemsListView.setAdapter(yueYaoListAdapter);

        Log.e("price","====yueYaoManager.getPrice()==="+yueYaoManager.getPrice());
        if (yueYaoManager.getPrice() != 0) {
            ll_bottom_cart.setVisibility(View.VISIBLE);
            tv_appointment_money_to_pay_down.setText( "￥" +
                    Constant.decimalFormat.format(yueYaoManager.getPrice()));
        }

        yueyaoItemsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final ResourceItem item = yueYaoListAdapter.getItem(position);
                resourceItem=item;

                boolean isAdded = false;

                //当在点击的时候，判断用户在乐药结算那一步是否删除了已经保存在购物车里的乐药，更新乐药状态
                if(Constant.LIST_DELETE_YUEYAO.size()>0)
                {
                    for(ResourceItem itemDelete:Constant.LIST_DELETE_YUEYAO)
                    {
                        if(itemDelete.getId()==item.getId())
                        {
                            item.setItemStatus(ResourceItem.NOT_ORDERED);
                        }
                    }

                }
                switch (item.getItemStatus()) {
                    case ResourceItem.NOT_ORDERED:
                        for (ResourceItem resourceItem : Constant.LIST) {
                            if (resourceItem.getId() == item.getId()) {
                                isAdded = true;
                            }
                        }
                        if (!isAdded) {
                            showPayDialog(position);
                        } else {
                            item.setItemStatus(ResourceItem.NOT_PAY);
                            Toast.makeText(FtjYueYaoActivity.this, getString(R.string.yueyao_tips_cart), Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case ResourceItem.NOT_DOWNLOAD:
                        showDownloadDialog(position);
                        break;

                    //当前停止点击播放乐药
                    case ResourceItem.STOP:
                        //判断是否是乐洛仪,如果是，判断是否连接乐洛仪，连接之后才能播放乐药
                        if(getString(R.string.web_leluoyi).equals(title))
                        {
                            if (ioSession != null&&bluetoothState) {

                                //播放下一首乐药电刺激强度选项归零
                                strength=0;
                                text_strength.setText(String.valueOf(strength));

                                yueYaoManager.playAudio(item);
                            }else
                            {
                                ToastUtil.invokeShortTimeToast(FtjYueYaoActivity.this, getString(R.string.yueyao_tips_open_bluetooth));
                            }
                        }else
                        {
                            yueYaoManager.playAudio(item);
                        }
                        break;

                    //当前播放，点击停止播放
                    case ResourceItem.PLAYING:
                        yueYaoManager.stopPlayer();
                        break;
                    case ResourceItem.NOT_PAY:
                        Toast.makeText(FtjYueYaoActivity.this, getString(R.string.yueyao_tips_add_cart),
                                Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        break;
                }
                yueYaoListAdapter.notifyDataSetChanged();
            }
        });
    }

    private void initTypeChoiceRg() {
        yueyaoTypeBtns = new RadioButton[5];
        yueyaoSubTypeBtns = new RadioButton[5];

        yueyaoTypeBtns[0] = findViewById(R.id.rb_type_1);
        yueyaoTypeBtns[1] = findViewById(R.id.rb_type_2);
        yueyaoTypeBtns[2] = findViewById(R.id.rb_type_3);
        yueyaoTypeBtns[3] = findViewById(R.id.rb_type_4);
        yueyaoTypeBtns[4] = findViewById(R.id.rb_type_5);

        yueyaoSubTypeBtns[0] = findViewById(R.id.rb_subtype_1);
        yueyaoSubTypeBtns[1] = findViewById(R.id.rb_subtype_2);
        yueyaoSubTypeBtns[2] = findViewById(R.id.rb_subtype_3);
        yueyaoSubTypeBtns[3] = findViewById(R.id.rb_subtype_4);
        yueyaoSubTypeBtns[4] = findViewById(R.id.rb_subtype_5);

        final RadioGroup yueyaoTypeRg = findViewById(R.id.rg_type);

        yueyaoTypeRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                for (int i = 0; i < yueyaoTypeBtns.length; i++) {
                    if (yueyaoTypeBtns[i].getId() == checkedId) {
                        typeIndex = i;
                    }
                }
                resetUi(typeIndex);

                for (int i = 0; i < yueyaoSubTypeBtns.length; i++) {
                    yueyaoSubTypeBtns[i].setText(SUBTYPE_NAME[typeIndex][i]);
                }

                if (isComeFromReport) {
                    isComeFromReport = false;
                } else {
                    subtypeIndex = DEFAULT_TYPE;
                }

                if (yueyaoSubTypeBtns[subtypeIndex].isChecked()) {
                    refreshYueyaoListView(SUBTYPE_SN[typeIndex][subtypeIndex]);
                } else {
                    yueyaoSubTypeBtns[subtypeIndex].setChecked(true);
                }
            }
        });

        final RadioGroup yueyaoSubtypeRg = findViewById(R.id.rg_subtype);
        yueyaoSubtypeRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                for (int i = 0; i < yueyaoSubTypeBtns.length; i++) {
                    if (yueyaoSubTypeBtns[i].getId() == checkedId) {
                        subtypeIndex = i;
                    }
                }
                refreshYueyaoListView(SUBTYPE_SN[typeIndex][subtypeIndex]);
            }
        });
    }

    private void resetUi(int index) {
        yueyaoTypeBtns[0].setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
        yueyaoTypeBtns[1].setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
        yueyaoTypeBtns[2].setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
        yueyaoTypeBtns[3].setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
        yueyaoTypeBtns[4].setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
        yueyaoTypeBtns[index].setTextSize(TypedValue.COMPLEX_UNIT_SP,24);

    }

    private void showDownloadDialog(final int position) {
        // 初始化确认下载的Dialog
        final ConfirmDialog downloadDialog = new ConfirmDialog(this);

        downloadDialog.setTextResourceId(R.string.downlistitem);

        downloadDialog.setCancelListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadDialog.dismiss();
            }
        });

        // 确认
        downloadDialog.setOkListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position != NO_POSITION) {
                    final ResourceItem resourceItem = yueYaoListAdapter.getItem(position);

                    downloadDialog.dismiss();

                    yueYaoManager.orderItem(resourceItem, new AbstractDefaultHttpHandlerCallback(FtjYueYaoActivity.this) {
                        @Override
                        protected void onResponseSuccess(Object obj) {
                            resourceItem.setItemStatus(ResourceItem.DOWNLOADING);
                            yueYaoManager.addDownloadingItem(resourceItem);
                            yueYaoListAdapter.notifyDataSetChanged();

                            yueYaoManager.downloadItemFile(resourceItem, FtjYueYaoActivity.this);
                        }
                    });
                }
            }
        });
        downloadDialog.show();
    }

    private void showPayDialog(final int position) {
        // 初始化确认购买的Dialog
        final PayDialog payDialog = new PayDialog(this);
        payDialog.setTextResourceId(R.string.paylistitem);
        payDialog.setTextPrice("￥:" + Constant.decimalFormat.format(yueYaoListAdapter.getItem(position).getPrice()));

        payDialog.setCancelListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payDialog.dismiss();
            }
        });

        // 确认
        payDialog.setOkListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResourceItem item = yueYaoListAdapter.getItem(position);

                item.setItemStatus(ResourceItem.NOT_PAY);

                payDialog.dismiss();

                Constant.LIST.add(yueYaoListAdapter.getItem(position));
                ll_bottom_cart.setVisibility(View.VISIBLE);
                Log.e("price","====yueYaoManager.getPrice()==="+yueYaoManager.getPrice());

                tv_appointment_money_to_pay_down.setText("￥" +
                        Constant.decimalFormat.format(yueYaoManager.getPrice()));

                Toast.makeText(FtjYueYaoActivity.this, getString(R.string.yueyao_tips_add_cart), Toast.LENGTH_SHORT).show();
            }
        });
        payDialog.show();
    }

    private void refreshYueyaoListView(final String subjectSn) {

        yueYaoManager.getItemsBySubjectSn(subjectSn, new AbstractDefaultHttpHandlerCallback(this) {
            @Override
            protected void onResponseSuccess(Object obj) {
                List<ResourceItem> yueyaoItems = (List<ResourceItem>) obj;
                if (yueyaoItems.size() > 0) {
                    mInfoTv.setVisibility(View.GONE);
                    yueyaoItemsListView.setVisibility(View.VISIBLE);

                    yueYaoListAdapter.setDatas(yueyaoItems, typeIndex);
                    yueYaoListAdapter.notifyDataSetChanged();
                } else {
                    mInfoTv.setVisibility(View.VISIBLE);
                    yueyaoItemsListView.setVisibility(View.GONE);
                    // 查询成功，但没有数据
                    mInfoTv.setText(getString(R.string.my_music_tips_no_data));
                }
            }

            @Override
            protected void onHttpError(int httpErrorCode) {
                mInfoTv.setVisibility(View.VISIBLE);
                yueyaoItemsListView.setVisibility(View.GONE);
                mInfoTv.setText(getString(R.string.yueyao_text_select_failure));
            }

            @Override
            protected void onResponseError(int errorCode, String errorDesc) {
                mInfoTv.setVisibility(View.VISIBLE);
                yueyaoItemsListView.setVisibility(View.GONE);
                mInfoTv.setText(getString(R.string.yueyao_text_select_failure));
            }
        });
    }

    @Override
    protected void onConnectFailed(Error error) {
        Toast.makeText(FtjYueYaoActivity.this, error.getDesc(), Toast.LENGTH_SHORT).show();
        //设备未连接
        imgbtn_bluetooth_state.setImageResource(R.drawable.bluetooth_not_connected);
        //未连接可以点击蓝牙按钮
        imgbtn_bluetooth_state.setEnabled(true);
        //蓝牙未连接状态
        bluetoothState=false;
    }

    @Override
    protected void onConnectFailedEnglist(Error_English error) {
        Toast.makeText(FtjYueYaoActivity.this, error.getDesc(), Toast.LENGTH_SHORT).show();
        //设备未连接
        imgbtn_bluetooth_state.setImageResource(R.drawable.bluetooth_not_connected);
        //未连接可以点击蓝牙按钮
        imgbtn_bluetooth_state.setEnabled(true);
        //蓝牙未连接状态
        bluetoothState=false;
    }

    @Override
    protected void onConnected() {
        //设备已连接
        imgbtn_bluetooth_state.setImageResource(R.drawable.bluetooth_connected);
        //已连接不可点击蓝牙按钮
        imgbtn_bluetooth_state.setEnabled(false);
        //蓝牙已连接状态
        bluetoothState=true;
    }

    @Override
    protected void onDisconnected() {
        //设备已断开
        imgbtn_bluetooth_state.setImageResource(R.drawable.bluetooth_not_connected);
        //可以点击
        imgbtn_bluetooth_state.setEnabled(true);
        // 蓝牙未连接状态
        bluetoothState=false;
    }

    @Override
    protected void onExceptionCaught(Throwable e) {
    }

    /**
     * 接收设备返回的数据
      */
    @Override
    protected void onMessageReceived(AbstractMessage message) {
        switch ((HealthDeviceMessageType) message.getMessageType()) {
            //耳针仪返回数据
            case AURICULAR_COMMON_BLUETOOTH_RESP_MESSAGE:
                AuricularBlueResponseMessage blueResponseMessage= (AuricularBlueResponseMessage) message;
                break;
            default:
                break;
        }
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_pay_away:
                if (yueYaoManager.getPrice() != 0) {
                    Intent intent = new Intent(FtjYueYaoActivity.this, YueyaojiesuanActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(FtjYueYaoActivity.this, getString(R.string.yueyao_tips_to_add), Toast.LENGTH_SHORT).show();
                }
                break;
            //当蓝牙未连接时，点击跳转至系统设置页面
            case R.id.imgbtn_bluetooth_state:
                Intent intent = null;
                // 判断手机系统的版本 即API大于10 就是3.0或以上版本
                if (android.os.Build.VERSION.SDK_INT > 10) {
                    intent = new Intent(
                            android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
                } else {
                    intent = new Intent();
                    //应用程序设置
                    ComponentName component = new ComponentName(
                            "com.android.settings",
                            "com.android.settings.ApplicationSettings");
                    intent.setComponent(component);
                    intent.setAction("android.intent.action.VIEW");
                }
                startActivity(intent);
                break;
            //强度增大
            case R.id.imgbtn_strenght_plus:
                //处于连接状态
                if (ioSession != null&&bluetoothState) {
                    //乐药正在播放
                    if(isPlaying)
                    {
                        if (strength < 60) {
                            strength++;
                            text_strength.setText(String.valueOf(strength));

                            AuricularBlueCommand blueCommand = new AuricularBlueCommand();
                            blueCommand.setCommandType(AuricularBlueCommand.CommandType.BLUE_MESSAGE_ADD);
                            blueCommand.setQiangdu(strength);
                            ioSession.sendMessage(blueCommand);

                        } else {
                            ToastUtil.invokeShortTimeToast(FtjYueYaoActivity.this, getString(R.string.yueyao_tips_max_value));
                        }
                    }else
                    {
                        ToastUtil.invokeShortTimeToast(FtjYueYaoActivity.this, getString(R.string.yueyao_tips_select_play));
                    }
                } else {
                    ToastUtil.invokeShortTimeToast(FtjYueYaoActivity.this, getString(R.string.yueyao_tips_open_bluetooth));
                }

                break;
            //强度减小
            case R.id.imgbtn_strenght_reduce:
                //处于连接状态
                if (ioSession != null&&bluetoothState) {

                    //乐药正在播放
                    if(isPlaying)
                    {
                        if (strength > 0) {
                            strength--;
                            text_strength.setText(String.valueOf(strength));

                            AuricularBlueCommand blueCommand = new AuricularBlueCommand();
                            blueCommand.setCommandType(AuricularBlueCommand.CommandType.BLUE_MESSAGE_REDUCE);
                            blueCommand.setQiangdu(strength);
                            ioSession.sendMessage(blueCommand);

                        } else {
                            ToastUtil.invokeShortTimeToast(FtjYueYaoActivity.this, getString(R.string.yueyao_tips_min_value));
                        }
                    }else {
                        ToastUtil.invokeShortTimeToast(FtjYueYaoActivity.this, getString(R.string.yueyao_tips_select_play));
                    }

                } else {
                    ToastUtil.invokeShortTimeToast(FtjYueYaoActivity.this, getString(R.string.yueyao_tips_open_bluetooth));
                }
                break;

            //电流开
            case R.id.img_bluetooth_open:
                setBlueToothClose();
                break;
            //电流关
            case R.id.img_bluetooth_close:
                setBlueToothOpen();
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            //是否是乐洛仪
            if(getString(R.string.web_leluoyi).equals(title))
            {
                //关闭电刺激
                setBlueToothClose();
            }

            //去掉activity跳转动画
            FtjYueYaoActivity.this.overridePendingTransition(0, 0);
            if(yueYaoManager.getDownLoadingItemSize()==0){
                FtjYueYaoActivity.this.finish();
            }else {
                FtjYueYaoActivity.this.finish();
            }

            return false;
        }
        return super.onKeyDown(keyCode, event);
    }
    @Override
    protected void onDestroy() {
        yueYaoManager.clear();
        yueYaoManager = null;
        // 动态注销广播 乐药支付成功之后，刷新当前页面的乐药状态
        if (yaoStaUpdateBroadcast != null) {
            unregisterReceiver(yaoStaUpdateBroadcast);
            yaoStaUpdateBroadcast = null;
        }
        super.onDestroy();
    }

    /**
     * 控制电流开关
     */
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

    }

    /**
     * 蓝牙打开，发送电流开始命令
     */
    private void setBlueToothOpen()
    {
        if (ioSession != null&&bluetoothState) {

            //乐药正在播放
            img_bluetooth_open.setVisibility(View.GONE);
            img_bluetooth_close.setVisibility(View.GONE);

            //电流开
            AuricularBlueCommand blueCommand = new AuricularBlueCommand();
            blueCommand.setCommandType(AuricularBlueCommand.CommandType.BLUE_MESSAGE_BEGIN);

            ioSession.sendMessage(blueCommand);
        }
        else {
            //ToastUtil.invokeShortTimeToast(YueYaoActivity.this, "请去蓝牙页面打开蓝牙连接接乐络怡");
        }
    }

    /**
     *  蓝牙打开，发送电流关闭命令
     */
    private void setBlueToothClose()
    {
        if (ioSession != null&&bluetoothState) {
            img_bluetooth_open.setVisibility(View.GONE);
            img_bluetooth_close.setVisibility(View.GONE);

            //电流关
            AuricularBlueCommand blueCommand = new AuricularBlueCommand();
            blueCommand.setCommandType(AuricularBlueCommand.CommandType.BLUE_MESSAGE_STOP);

            ioSession.sendMessage(blueCommand);
        }
        else {
            //ToastUtil.invokeShortTimeToast(YueYaoActivity.this, "请去蓝牙页面打开蓝牙连接接乐络怡");

        }
    }
}
