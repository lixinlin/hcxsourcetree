package com.hcy_futejia.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.hcy.ky3h.R;
import com.hcy_futejia.utils.DialogUtils;
import com.hxlm.android.hcy.AbstractBaseActivity;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.asynchttp.HcyHttpResponseHandler;
import com.hxlm.android.hcy.user.ForgetPasswordActivity;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.user.UserManager;
import com.hxlm.android.hcy.utils.MyCountTimer;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.bodydiagnose.IntroductionActivity;
import com.hxlm.hcyandroid.util.IsMobile;
import com.hxlm.hcyandroid.util.StatusBarUtils;
import com.hxlm.hcyandroid.util.ToastUtil;
import com.hxlm.hcyandroid.view.ContainsEmojiEditText;
import com.hxlm.hcyphone.MainActivity;
import com.loopj.android.http.AsyncHttpClient;
import com.umeng.commonsdk.UMConfigure;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;

import net.sourceforge.simcpux.Constants;

import java.util.List;
import java.util.Map;

/**
 * @author Administrator
 */
public class FtjLoginActivity extends AbstractBaseActivity implements View.OnClickListener {

    private TextView tv_pwd_login;
    private TextView tv_sms_login;
    private LinearLayout ll_sms_login;
    private LinearLayout ll_pwd_login;
    private ImageView weixin_login;
    private Button bt_login_yzm;
    private Button bt_login_mm;
    private ContainsEmojiEditText et_sms_phone;
    private ContainsEmojiEditText et_sms_yzm;
    private TextView tv_get_yzm;
    private CheckBox cb_sms;
    private ContainsEmojiEditText et_pwd_phone;
    private ContainsEmojiEditText et_pwd_mm;
    private TextView tv_forget_password;
    private CheckBox cb_pwd;

    private Context context;
    public static Activity activity;
    private boolean isChange = false;
    public static OnCompleteListener onCompleteListener;
    private UMShareAPI mShareAPI;
    private TextView tv_sms_login_line;
    private TextView tv_pwd_login_line;
    private ImageView iv_visible;

    private boolean flag = false;
    private TextView tv_xieyi;
    private TextView tv_pwd_xieyi;

    @Override
    protected void setContentView() {
        StatusBarUtils.setWindowStatusBarColor(this, R.color.white);
        setContentView(R.layout.activity_ftj_login);
        context = FtjLoginActivity.this;
        activity = FtjLoginActivity.this;
//        if(isWeixinAvilible(this)){
//            Toast.makeText(context, "微信可用", Toast.LENGTH_SHORT).show();
//        }else{
//            Toast.makeText(context, "没有微信", Toast.LENGTH_SHORT).show();
//        }
//        startActivity(new Intent(this,FtjVerifyCodeActivity.class));
    }

    @Override
    protected void initViews() {
        ll_sms_login = findViewById(R.id.ll_sms_login);
        ll_pwd_login = findViewById(R.id.ll_pwd_login);
        tv_pwd_login = findViewById(R.id.tv_pwd_login);
        tv_sms_login = findViewById(R.id.tv_sms_login);
        weixin_login = findViewById(R.id.weixin_login);
        bt_login_yzm = findViewById(R.id.bt_login_yzm);
        bt_login_mm = findViewById(R.id.bt_login_mm);
        et_sms_phone = findViewById(R.id.et_sms_phone);
        et_sms_yzm = findViewById(R.id.et_sms_yzm);
        tv_get_yzm = findViewById(R.id.tv_get_yzm);
        cb_sms = findViewById(R.id.cb_sms);
        et_pwd_phone = findViewById(R.id.et_pwd_phone);
        et_pwd_mm = findViewById(R.id.et_pwd_mm);
        tv_forget_password = findViewById(R.id.tv_forget_password);
        cb_pwd = findViewById(R.id.cb_pwd);
        tv_sms_login_line = findViewById(R.id.tv_sms_login_line);
        tv_pwd_login_line = findViewById(R.id.tv_pwd_login_line);
        iv_visible = findViewById(R.id.iv_visible);
        tv_xieyi = findViewById(R.id.tv_xieyi);
        tv_pwd_xieyi = findViewById(R.id.tv_pwd_xieyi);

        tv_pwd_login.setOnClickListener(this);
        tv_sms_login.setOnClickListener(this);
        weixin_login.setOnClickListener(this);
        bt_login_mm.setOnClickListener(this);
        bt_login_yzm.setOnClickListener(this);
        tv_get_yzm.setOnClickListener(this);
        tv_forget_password.setOnClickListener(this);
        iv_visible.setOnClickListener(this);

        //默认显示
        tv_pwd_login_line.setVisibility(View.INVISIBLE);
        tv_pwd_login.setTextColor(Color.parseColor("#9b9b9b"));
        tv_sms_login_line.setVisibility(View.VISIBLE);
        tv_sms_login.setTextColor(Color.parseColor("#1E82D2"));
        ll_pwd_login.setVisibility(View.GONE);
        ll_sms_login.setVisibility(View.VISIBLE);

        SpannableString spannableString = new SpannableString(getString(R.string.ftj_user_xieyi));
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Intent intent = new Intent(FtjLoginActivity.this, IntroductionActivity.class);
                intent.putExtra("title", getString(R.string.ftj_login_yonghuxieyi));
                if (Constant.isEnglish) {

                    intent.putExtra("introduceCategory", Constant.PRIVICY_POLICY_EN);
                } else {
                    intent.putExtra("introduceCategory", Constant.PRIVICY_POLICY);

                }
                startActivity(intent);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.parseColor("#1e82d2"));
                ds.setUnderlineText(false);
            }
        }, 0, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tv_xieyi.setText(getString(R.string.ftj_woyiyuedu));
        tv_xieyi.append(spannableString);
        //开始响应点击事件
        tv_xieyi.setMovementMethod(LinkMovementMethod.getInstance());
        tv_pwd_xieyi.setText(getString(R.string.ftj_woyiyuedu));
        tv_pwd_xieyi.append(spannableString);
        //开始响应点击事件
        tv_pwd_xieyi.setMovementMethod(LinkMovementMethod.getInstance());

        et_sms_phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable == null || editable.length() == 0) {
                    bt_login_yzm.setBackgroundResource(R.drawable.yuanjiaojuxing);
                    return;
                }

                //setText()会回调onTextChanged()，增加标志位防止出现堆栈溢出
                if (isChange) {
                    isChange = false;
                    return;
                }

                isChange = true;

                if (!TextUtils.isEmpty(editable.toString())) {
                    et_pwd_phone.setText(editable.toString());
                }

                if (!TextUtils.isEmpty(et_sms_yzm.getText().toString()) && cb_sms.isChecked()) {
                    bt_login_yzm.setBackgroundResource(R.drawable.yuanjiaojuxing_login_blue);
                }
            }
        });

        et_sms_yzm.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable == null || editable.length() == 0) {
                    bt_login_yzm.setBackgroundResource(R.drawable.yuanjiaojuxing);
                    return;
                } else {
                    if (!TextUtils.isEmpty(et_sms_phone.getText().toString()) && cb_sms.isChecked()) {
                        bt_login_yzm.setBackgroundResource(R.drawable.yuanjiaojuxing_login_blue);
                    }
                }
            }
        });

        et_pwd_phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable == null || editable.length() == 0) {
                    bt_login_mm.setBackgroundResource(R.drawable.yuanjiaojuxing);
                    return;
                }

                //setText()会回调onTextChanged()，增加标志位防止出现堆栈溢出
                if (isChange) {
                    isChange = false;
                    return;
                }

                isChange = true;

                if (!TextUtils.isEmpty(editable.toString())) {
                    et_sms_phone.setText(editable.toString());
                }

                if (!TextUtils.isEmpty(et_pwd_mm.getText().toString()) && cb_pwd.isChecked()) {
                    bt_login_mm.setBackgroundResource(R.drawable.yuanjiaojuxing_login_blue);
                }
            }
        });

        et_pwd_mm.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s == null || s.length() == 0) {
                    bt_login_mm.setBackgroundResource(R.drawable.yuanjiaojuxing);
                    return;
                } else {
                    if (!TextUtils.isEmpty(et_pwd_phone.getText().toString()) && cb_pwd.isChecked()) {
                        bt_login_mm.setBackgroundResource(R.drawable.yuanjiaojuxing_login_blue);
                    }
                }
            }
        });
    }

    @Override
    protected void initDatas() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            //密码登录
            case R.id.tv_pwd_login:
                ll_sms_login.clearAnimation();
                ll_pwd_login.setVisibility(View.VISIBLE);
                ll_sms_login.setVisibility(View.GONE);
                tv_sms_login.setTextColor(Color.parseColor("#9b9b9b"));
                tv_sms_login_line.setVisibility(View.GONE);
                tv_pwd_login.setTextColor(Color.parseColor("#1E82D2"));
                tv_pwd_login_line.setVisibility(View.VISIBLE);
                break;
            //短信登录
            case R.id.tv_sms_login:
                ll_pwd_login.clearAnimation();
                ll_sms_login.setVisibility(View.VISIBLE);
                ll_pwd_login.setVisibility(View.GONE);
                tv_pwd_login.setTextColor(Color.parseColor("#9b9b9b"));
                tv_pwd_login_line.setVisibility(View.GONE);
                tv_sms_login.setTextColor(Color.parseColor("#1E82D2"));
                tv_sms_login_line.setVisibility(View.VISIBLE);
                break;
            //微信登录
            case R.id.weixin_login:
                HcyHttpResponseHandler responseHandler = new HcyHttpResponseHandler(null) {
                    @Override
                    protected Object contentParse(String content) {
                        com.alibaba.fastjson.JSONObject data = JSON.parseObject(content);
                        String secret = data.getString("secret");
                        UMConfigure.init(FtjLoginActivity.this, content, "umeng", UMConfigure.DEVICE_TYPE_PHONE, "");
                        PlatformConfig.setWeixin(Constants.APP_ID, secret);
                        shengQingQuanXian();
                        mShareAPI = UMShareAPI.get(FtjLoginActivity.this);
                        mShareAPI.getPlatformInfo(FtjLoginActivity.this, SHARE_MEDIA.WEIXIN, umAuthListener);
                        return null;
                    }
                };
                HcyHttpClient.getSecret(responseHandler);
                break;
            //密码登录按钮
            case R.id.bt_login_mm:
                String userName = et_pwd_phone.getText().toString();
                String userPwd = et_pwd_mm.getText().toString();
                if (TextUtils.isEmpty(userName) || TextUtils.isEmpty(userPwd)) {

                } else {
                    //验证手机号
                    boolean idMobile = IsMobile.isMobileNO(userName);
//                    if (Constant.isEnglish){
//                        if (idMobile || userName.length() == 10) {
//                            goLoginMM(userName, userPwd);
//                        } else {
//                            ToastUtil.invokeShortTimeToast(context, getString(R.string.login_phone_format_error));
//                        }
//                    }else {
                        if (idMobile) {
                            goLoginMM(userName, userPwd);
                        } else {
                            ToastUtil.invokeShortTimeToast(context, getString(R.string.login_phone_format_error));
                        }
//                    }
                }
                break;
            //短信登录按钮
            case R.id.bt_login_yzm:
                String userNameYZM = et_sms_phone.getText().toString();
                String passwordYZM = et_sms_yzm.getText().toString();
                if (TextUtils.isEmpty(userNameYZM) || TextUtils.isEmpty(passwordYZM)) {
                    ToastUtil.invokeShortTimeToast(context, getString(R.string.login_phone_sms_empty));
                } else {
                    //验证手机号
                    boolean idMobile = IsMobile.isMobileNO(userNameYZM);
//                    if (Constant.isEnglish) {
//                        if (idMobile || userNameYZM.length() == 10 ) {
//                            goLoginYZM(userNameYZM, passwordYZM);
//                        } else {
//                            ToastUtil.invokeShortTimeToast(context, getString(R.string.login_phone_format_error));
//                        }
//                    }else{
                        if (idMobile) {
                            goLoginYZM(userNameYZM, passwordYZM);
                        } else {
                            ToastUtil.invokeShortTimeToast(context, getString(R.string.login_phone_format_error));
                        }
//                    }
                }
                break;
            //获取验证码
            case R.id.tv_get_yzm:
                getYZM();
                break;
            //忘记密码
            case R.id.tv_forget_password:
                Intent intent1 = new Intent(FtjLoginActivity.this, ForgetPasswordActivity.class);
                startActivity(intent1);
                break;
            //密码登录多选框
            case R.id.cb_pwd:
                break;
            //短信登录多选框
            case R.id.cb_sms:
                break;
            //设置密码可见不可见
            case R.id.iv_visible:
                if (flag) {
                    iv_visible.setBackgroundResource(R.drawable.login_pwd_1);
                    et_pwd_mm.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    flag = false;
                } else {
                    iv_visible.setBackgroundResource(R.drawable.login_pwd_2);
                    et_pwd_mm.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    flag = true;
                }
                break;
            default:
                break;
        }
    }

    //登录
    private void goLoginMM(final String userName, final String password) {

            if (cb_pwd.isChecked()) {
                new UserManager().login(userName, password, new AbstractDefaultHttpHandlerCallback(context) {
                    @Override
                    protected void onResponseSuccess(Object obj) {
                        LoginControllor.setAutoLoginByMiMa(true, userName, password);
                        LoginControllor.setIsAutologinBySms(false, "");
                        LoginControllor.setAutoLoginByWeiChart(false, "", "", "", "");
                        if (null != onCompleteListener) {
                            onCompleteListener.onComplete();
                        }
                        Constant.loginType = 1;
                        //跳转主界面
                        toMainActivity();
                    }

                });
            } else {
                ToastUtil.invokeShortTimeToast(FtjLoginActivity.this, getString(R.string.ftj_login_cb_tips));
            }

    }

    private void toMainActivity() {
        Intent intentMain = new Intent(FtjLoginActivity.this, MainActivity.class);
        startActivity(intentMain);
        FtjLoginActivity.this.finish();
    }

    private void shengQingQuanXian() {
        if (Build.VERSION.SDK_INT >= 23) {
            String[] mPermissionList = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CALL_PHONE, Manifest.permission.READ_LOGS, Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.SET_DEBUG_APP, Manifest.permission.SYSTEM_ALERT_WINDOW, Manifest.permission.GET_ACCOUNTS, Manifest.permission.WRITE_APN_SETTINGS};
            ActivityCompat.requestPermissions(this, mPermissionList, 123);
        }
    }

    /**
     * 短信登录
     *
     * @param userNameYZM 手机号
     * @param passwordYZM
     */
    private void goLoginYZM(String userNameYZM, String passwordYZM) {

                if (cb_sms.isChecked()) {
                    new UserManager().loginByDuanXin(userNameYZM, passwordYZM, new AbstractDefaultHttpHandlerCallback(context) {
                        @Override
                        protected void onResponseSuccess(Object obj) {
//                        保存登录标致，下次自动登录
                            LoginControllor.setIsAutologinBySms(true, userNameYZM);
                            LoginControllor.setAutoLoginByWeiChart(false, "", "", "", "");
                            LoginControllor.setAutoLoginByMiMa(false, "", "");
                            if (null != onCompleteListener) {
                                onCompleteListener.onComplete();
                            }
                            Constant.loginType = 1;
                            //跳转主界面
                            toMainActivity();
                        }
                    });
                } else {
                    ToastUtil.invokeShortTimeToast(FtjLoginActivity.this, getString(R.string.ftj_login_cb_tips));
                }

    }

    /**
     * 获取验证码
     */
    public void getYZM() {
        String account = et_sms_phone.getText().toString();
        if (!TextUtils.isEmpty(account)) {
            //验证手机号
            boolean idMobile = IsMobile.isMobileNO(account);
            if (idMobile) {
                LoginControllor.clearLastLoginInfo();
                new UserManager().getCaptchaAgain(account, new AbstractDefaultHttpHandlerCallback(context) {
                    @Override
                    protected void onResponseSuccess(Object obj) {
                        ToastUtil.invokeShortTimeToast(getContext(), getString(R.string.login_has_been_send));

                        MyCountTimer myCountTimer = new MyCountTimer(FtjLoginActivity.this, 60000, 1000, tv_get_yzm, 2);
                        myCountTimer.start();
                        tv_get_yzm.setTextColor(Color.parseColor("#9A9B9B"));
                        tv_get_yzm.setClickable(false);
                        myCountTimer.setCountTimerFinishListener(new MyCountTimer.OnCountTimerFinishListener() {
                            @Override
                            public void onTimerFinish() {
                                tv_get_yzm.setTextColor(Color.parseColor("#1E82D2"));
                                tv_get_yzm.setClickable(true);
                                tv_get_yzm.setText(getString(R.string.bt_get_test_number));
                            }
                        });
                    }
                });
            } else {
                ToastUtil.invokeShortTimeToast(context, getString(R.string.login_phone_format_error));
            }
        } else {
            ToastUtil.invokeShortTimeToast(context, context.getString(R.string.username_is_empty));
        }
    }

//    public boolean isWeixinAvilible(Context context) {
//        PackageManager packageManager = context.getPackageManager();// 获取packagemanager
//        List<PackageInfo> pinfo = packageManager.getInstalledPackages(0);// 获取所有已安装程序的包信息
//        if (pinfo != null) {
//            for (int i = 0; i < pinfo.size(); i++) {
//                String pn = pinfo.get(i).packageName;
//                if (pn.equals("com.tencent.mm")) {
//                    return true;
//                }
//            }
//        }
//        return false;
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }


    UMAuthListener umAuthListener = new UMAuthListener() {
        /**
         * @desc 授权开始的回调
         * @param platform 平台名称
         */
        @Override
        public void onStart(SHARE_MEDIA platform) {
        }

        /**
         * @desc 授权成功的回调
         * @param platform 平台名称
         * @param action 行为序号，开发者用不上
         * @param data 用户资料返回
         */
        @Override
        public void onComplete(SHARE_MEDIA platform, int action, Map<String, String> data) {
//            上传用户信息进行注册。
            new UserManager().loginByWeiXin(
                    data.get("unionid"),
                    data.get("screen_name"),
                    data.get("gender"),
                    data.get("profile_image_url"),
                    new AbstractDefaultHttpHandlerCallback(FtjLoginActivity.this) {
                        @Override
                        protected void onResponseSuccess(Object obj) {
                            boolean success = (boolean) obj;
                            if (success) {
                                LoginControllor.setAutoLoginByWeiChart(
                                        true,
                                        data.get("unionid"),
                                        data.get("screen_name"),
                                        data.get("gender"),
                                        data.get("profile_image_url"));
                                LoginControllor.setIsAutologinBySms(false, "");
                                LoginControllor.setAutoLoginByMiMa(false, "", "");
                                if (null != onCompleteListener) {
                                    onCompleteListener.onComplete();
                                }
                                mShareAPI.deleteOauth(FtjLoginActivity.this, SHARE_MEDIA.WEIXIN, umDeleteOauthListener);
                                //跳转主界面
                                Constant.loginType = 2;
                                toMainActivity();
                            } else {

                            }

                        }
                    }
            );
        }

        /**
         * @desc 授权失败的回调
         * @param platform 平台名称
         * @param action 行为序号，开发者用不上
         * @param t 错误原因
         */
        @Override
        public void onError(SHARE_MEDIA platform, int action, Throwable t) {
            Toast.makeText(context, getString(R.string.login_authorization_failure), Toast.LENGTH_SHORT).show();
        }

        /**
         * @desc 授权取消的回调
         * @param platform 平台名称
         * @param action 行为序号，开发者用不上
         */
        @Override
        public void onCancel(SHARE_MEDIA platform, int action) {
            Toast.makeText(context, getString(R.string.login_authorization_cancel), Toast.LENGTH_SHORT).show();
        }
    };
    UMAuthListener umDeleteOauthListener = new UMAuthListener() {

        @Override
        public void onStart(SHARE_MEDIA share_media) {
        }

        @Override
        public void onComplete(SHARE_MEDIA share_media, int i, Map<String, String> map) {
        }

        @Override
        public void onError(SHARE_MEDIA share_media, int i, Throwable throwable) {
        }

        @Override
        public void onCancel(SHARE_MEDIA share_media, int i) {
        }
    };


}
