package com.hcy_futejia.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.hcy.ky3h.R;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.asynchttp.HcyHttpResponseHandler;
import com.hxlm.android.hcy.report.UploadManager;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.user.Member;
import com.hxlm.android.hcy.user.UserManager;
import com.hxlm.android.hcy.utils.SpUtils;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.android.utils.Logger;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.util.IsMobile;
import com.hxlm.hcyandroid.util.ToastUtil;
import com.hxlm.hcyandroid.view.ChooseSexDialog;
import com.hxlm.hcyandroid.view.CircleImageView;
import com.hxlm.hcyandroid.view.UploadIconDialog;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.umeng.commonsdk.UMConfigure;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;

import net.sourceforge.simcpux.Constants;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

public class FtjUserInfoActivity extends BaseActivity implements View.OnClickListener {

    private LinearLayout ll_alter_head_portrait;
    private CircleImageView iv_user_icon;
    private LinearLayout ll_nickname;
    private TextView tv_nickname;
    private LinearLayout ll_sex;
    private TextView tv_sex;
    private LinearLayout ll_birthday;
    private TextView tv_birthday;
    private LinearLayout ll_phone_number;
    private TextView tv_phone_number;
    public static final int REQUEST_CODE_FROM_ALBUM = 1;
    public static final int REQUEST_CODE_FROM_CAMEIA = 2;
    private UserManager userManager;
    private UploadManager uploadManager;

    private Bitmap userIconBitmap;


    private String strYearNow;// 当前年
    private String strMouthNow;// 当前月
    private String strDayNow;// 当前日

    private String memberImage;
    private String gender;
    private String name;
    private String birthday;
    private String mobile;

    private String path;
    private AbstractDefaultHttpHandlerCallback abstractDefaultHttpHandler = new AbstractDefaultHttpHandlerCallback(this) {
        @Override
        protected void onResponseSuccess(Object obj) {
            ToastUtil.invokeShortTimeToast(FtjUserInfoActivity.this, getString(R.string.user_info_gerenxinxigxcg));
            String mIsMarried = "";
//            if (isMarried != null) {
//                if (isMarried == 1) {
//                    mIsMarried = "true";
//                } else if (isMarried == 0) {
//                    mIsMarried = "false";
//                }
//            }

            //保存信息
            Member member = (Member) obj;
            LoginControllor.setLoginMember(member);

//            FtjUserInfoActivity.this.finish();

        }

    };
    private Dialog dialog;
    private ImageView iv_number;
    private TextView tv_number_empty;
    private LinearLayout ll_bind_weChat;
    private UMShareAPI mShareAPI;

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_ftj_user_info);
    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.ftj_userinfo_title), titleBar, 1);


        ll_alter_head_portrait = findViewById(R.id.ll_alter_head_portrait);
        iv_user_icon = findViewById(R.id.iv_user_icon);
        ll_nickname = findViewById(R.id.ll_nickname);
        tv_nickname = findViewById(R.id.tv_nickname);
        ll_sex = findViewById(R.id.ll_sex);
        tv_sex = findViewById(R.id.tv_sex);
        ll_birthday = findViewById(R.id.ll_birthday);
        tv_birthday = findViewById(R.id.tv_birthday);
        ll_phone_number = findViewById(R.id.ll_phone_number);
        tv_phone_number = findViewById(R.id.tv_phone_number);
        tv_number_empty = findViewById(R.id.tv_number_empty);
        iv_number = findViewById(R.id.iv_number);
        ll_bind_weChat = findViewById(R.id.ll_bind_weChat);

        ll_alter_head_portrait.setOnClickListener(this);
        ll_birthday.setOnClickListener(this);
        ll_nickname.setOnClickListener(this);
        ll_sex.setOnClickListener(this);
        ll_bind_weChat.setOnClickListener(this);

    }

    @Override
    public void initDatas() {
    }

    @Override
    protected void onResume() {
        super.onResume();
        getDefaultData();
    }

    private void getDefaultData() {
        // 获取当前系统时间
        SimpleDateFormat formatNow = new SimpleDateFormat("yyyy年MM月dd日");
        long longNow = System.currentTimeMillis();
        String dateNow = formatNow.format(new Date(longNow));
        // 截取当前年
        int indexyearNow = dateNow.indexOf("年");
        strYearNow = dateNow.substring(0, indexyearNow);
        // 截取当前月
        int indexMouthNow = dateNow.indexOf("月");
        strMouthNow = dateNow.substring(indexyearNow + 1, indexMouthNow);
        // 截取当前日
        int indesDayNow = dateNow.indexOf("日");
        strDayNow = dateNow.substring(indexMouthNow + 1, indesDayNow);

        //-----------------------个人信息
        uploadManager = new UploadManager();
        userManager = new UserManager();
        if (LoginControllor.isLogin()) {
            Member member = LoginControllor.getLoginMember();

            memberImage = member.getMemberImage();
            if (!TextUtils.isEmpty(member.getMemberImage())) {
                DisplayImageOptions options = new DisplayImageOptions.Builder()
                        .cacheOnDisk(true)
                        .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                        .cacheInMemory(false).build();

                ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                        this).denyCacheImageMultipleSizesInMemory()
                        .threadPriority(Thread.NORM_PRIORITY - 2)
                        .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                        .tasksProcessingOrder(QueueProcessingType.LIFO)
                        .memoryCache(new LruMemoryCache(8 * 1024 * 1024))
                        .diskCacheSize(50 * 1024 * 1024).threadPoolSize(3).build();

                ImageLoader loader = ImageLoader.getInstance();
                loader.init(config);
                loader.displayImage(memberImage, iv_user_icon, options);
            }

            gender = member.getGender();
            if (!TextUtils.isEmpty(gender) && gender.equals("male")) {
                tv_sex.setText(getString(R.string.user_info_nan));
            } else if (!TextUtils.isEmpty(gender) && gender.equals("female")) {
                tv_sex.setText(getString(R.string.user_info_nv));
            }
            name = member.getName();
            if (!TextUtils.isEmpty(name)) {
                tv_nickname.setText(name);
            }
            birthday=member.getBirthday();
            if(!TextUtils.isEmpty(birthday))
            {
                tv_birthday.setText(birthday);
            }else{
                tv_birthday.setText(getString(R.string.weisehzhi));
            }
            mobile = member.getUserName();
            if (Constant.isEnglish) {
                tv_phone_number.setText(mobile);
            }
            //loginType=1为手机号登录 loginType=2为微信登录
            if (Constant.loginType == 1) {
                if (!TextUtils.isEmpty(mobile)) {
                    if (mobile.length() > 20) {
                        mobile = "";
                    }
                    iv_number.setVisibility(View.GONE);
                    tv_number_empty.setVisibility(View.VISIBLE);
                    tv_phone_number.setText(mobile);
                }
            } else {
                if (IsMobile.isMobileNO(mobile)){
                    if (!TextUtils.isEmpty(mobile)) {
                        if (mobile.length() > 20) {
                            mobile = "";
                        }
                        iv_number.setVisibility(View.GONE);
                        tv_number_empty.setVisibility(View.VISIBLE);
                        tv_phone_number.setText(mobile);
                    }
                }else if(IsMobile.isEnglishMobileNO(mobile)||IsMobile.isEmail(mobile)){
                    //是英文版登录的
                    iv_number.setVisibility(View.GONE);
                    tv_number_empty.setVisibility(View.VISIBLE);
                    tv_phone_number.setText(mobile);
                }
                else {
                    //未绑定需跳转到绑定页
                    ll_phone_number.setOnClickListener(this);
                    tv_number_empty.setVisibility(View.GONE);
                    iv_number.setVisibility(View.VISIBLE);
                    tv_phone_number.setText(getString(R.string.ftj_user_info_unbind));
                }
            }

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_alter_head_portrait:
                new UploadIconDialog(this, new UploadIconDialog.OnOptionClickedListener() {

                    // 拍照获取头像
                    @Override
                    public void getFromCamera() {

                        //动态开启相机权限
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            int hasWriteContactsPermission = checkSelfPermission(Manifest.permission.CAMERA);
                            if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                                requestPermissions(new String[]{Manifest.permission.CAMERA}, 1);
                                return;
                            }
                        }

                        String state = Environment.getExternalStorageState();
                        if (state.equals(Environment.MEDIA_MOUNTED)) {
                            Intent getImageByCamera = new Intent("android.media.action.IMAGE_CAPTURE");
//                            getImageByCamera.putExtra(MediaStore.EXTRA_OUTPUT, state);
                            try {
                                startActivityForResult(getImageByCamera, REQUEST_CODE_FROM_CAMEIA);
                            } catch (Exception e) {
                                e.printStackTrace();
                                Toast.makeText(FtjUserInfoActivity.this, "没有摄像设备", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), getString(R.string.user_info_sdkabukeyong), Toast.LENGTH_SHORT).show();
                        }
                    }

                    // 从相册获取头像
                    @Override
                    public void chooseFromAlbum() {
                        Intent getImageFromAlbum = new Intent(Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(getImageFromAlbum, REQUEST_CODE_FROM_ALBUM);
                    }
                }).show();
                break;
            case R.id.ll_nickname:
                setNickNameDialog();
                break;
            case R.id.ll_sex:
                new ChooseSexDialog(this, new ChooseSexDialog.OnChoosedListener() {

                    @Override
                    public void onChoosedSex(String sex) {
                        tv_sex.setText(sex);
                        submitData();
                    }
                }).show();
                break;
            case R.id.ll_birthday:
                alertChooseDateDialog();

                break;
            case R.id.ll_phone_number:
                String number = tv_phone_number.getText().toString().trim();
                if (number.equals(getString(R.string.ftj_user_info_unbind))){
                    Intent intent = new Intent(FtjUserInfoActivity.this, BandingPhoneNumberActivity.class);
                    startActivity(intent);
                }
                break;
            //绑定微信
            case R.id.ll_bind_weChat:
                HcyHttpResponseHandler responseHandler = new HcyHttpResponseHandler(null) {
                    @Override
                    protected Object contentParse(String content) {
                        com.alibaba.fastjson.JSONObject data = JSON.parseObject(content);
                        String secret = data.getString("secret");
                        UMConfigure.init(FtjUserInfoActivity.this, content, "umeng", UMConfigure.DEVICE_TYPE_PHONE, "");
                        PlatformConfig.setWeixin(Constants.APP_ID, secret);
                        shengQingQuanXian();
                        mShareAPI = UMShareAPI.get(FtjUserInfoActivity.this);
                        mShareAPI.getPlatformInfo(FtjUserInfoActivity.this, SHARE_MEDIA.WEIXIN, umAuthListener);
                        return null;
                    }
                };
                HcyHttpClient.getSecret(responseHandler);
                break;
            default:
                break;
        }
    }

    private void shengQingQuanXian() {
        if (Build.VERSION.SDK_INT >= 23) {
            String[] mPermissionList = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CALL_PHONE, Manifest.permission.READ_LOGS, Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.SET_DEBUG_APP, Manifest.permission.SYSTEM_ALERT_WINDOW, Manifest.permission.GET_ACCOUNTS, Manifest.permission.WRITE_APN_SETTINGS};
            ActivityCompat.requestPermissions(this, mPermissionList, 123);
        }
    }

    /**
     * 提交
     */
    private void submitData(){
        if (tv_sex.getText().toString().equals(getString(R.string.user_info_nan))) {
            gender = "male";
        } else if (tv_sex.getText().toString().equals(getString(R.string.user_info_nv))) {
            gender = "female";
        }
        name = tv_nickname.getText().toString();
        birthday = tv_birthday.getText().toString();
        mobile = tv_phone_number.getText().toString();
        if (mobile!=null&&TextUtils.isEmpty(mobile.trim())) {
            mobile = null;
        }
        if (name!=null&&TextUtils.isEmpty(name.trim())) {
            name = null;
        }
        if (birthday!=null&&TextUtils.isEmpty(birthday.trim())) {
            birthday = null;
        }
        userManager.updateMemberInfo(memberImage, gender, name, mobile,
                "", "", "", 1, "", "", birthday, abstractDefaultHttpHandler);
    }


    UMAuthListener umAuthListener = new UMAuthListener() {
        /**
         * @desc 授权开始的回调
         * @param platform 平台名称
         */
        @Override
        public void onStart(SHARE_MEDIA platform) {
        }

        /**
         * @desc 授权成功的回调
         * @param platform 平台名称
         * @param action 行为序号，开发者用不上
         * @param data 用户资料返回
         */
        @Override
        public void onComplete(SHARE_MEDIA platform, int action, Map<String, String> data) {
            String unionid = data.get("unionid");
            Toast.makeText(FtjUserInfoActivity.this,"===id===="+unionid,Toast.LENGTH_SHORT).show();

        }

        /**
         * @desc 授权失败的回调
         * @param platform 平台名称
         * @param action 行为序号，开发者用不上
         * @param t 错误原因
         */
        @Override
        public void onError(SHARE_MEDIA platform, int action, Throwable t) {
            Toast.makeText(FtjUserInfoActivity.this, getString(R.string.login_authorization_failure), Toast.LENGTH_SHORT).show();
        }

        /**
         * @desc 授权取消的回调
         * @param platform 平台名称
         * @param action 行为序号，开发者用不上
         */
        @Override
        public void onCancel(SHARE_MEDIA platform, int action) {
            Toast.makeText(FtjUserInfoActivity.this, getString(R.string.login_authorization_cancel), Toast.LENGTH_SHORT).show();
        }
    };

    /**
     * 修改姓名的弹窗
     */
    private void setNickNameDialog() {
        View view = LayoutInflater.from(FtjUserInfoActivity.this).inflate(R.layout.dialog_utils_input, null);
        dialog = new Dialog(FtjUserInfoActivity.this, R.style.dialog);
        dialog.setContentView(view);
        TextView tv_input_title = view.findViewById(R.id.tv_input_title);
        EditText et_input = view.findViewById(R.id.et_input);
        TextView tv_back = view.findViewById(R.id.tv_back);
        TextView tv_sure = view.findViewById(R.id.tv_sure);
        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        tv_sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_nickname.setText(et_input.getText().toString().trim());
                submitData();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void alertChooseDateDialog() {

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);// 当前年份
        int monthOfYear = calendar.get(Calendar.MONTH);// 当前月份
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);// 当前日期


        new DatePickerDialog(this, AlertDialog.THEME_HOLO_LIGHT,new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int yearSelect,
                                  int monthOfYearelect, int dayOfMonthSelect) {

                try {
                    // 出生年份小于当前年份
                    if (yearSelect < Integer.parseInt(strYearNow)) {

                        //判断当前月是否大于10
                        if ((monthOfYearelect + 1) >= 10) {
                            //判断当前日是否大于10
                            if (dayOfMonthSelect >= 10) {
                                tv_birthday.setText(yearSelect + "-" + (monthOfYearelect + 1) + "-"
                                        + dayOfMonthSelect);
                            } else {
                                tv_birthday.setText(yearSelect + "-" + (monthOfYearelect + 1) + "-0"
                                        + dayOfMonthSelect);
                            }
                        }
                        //月份小于10
                        else {
                            if (dayOfMonthSelect >= 10) {
                                tv_birthday.setText(yearSelect + "-0" + (monthOfYearelect + 1) + "-"
                                        + dayOfMonthSelect);
                            } else {
                                tv_birthday.setText(yearSelect + "-0" + (monthOfYearelect + 1) + "-0"
                                        + dayOfMonthSelect);
                            }


                        }


                    }
                    // 出生年份等于当前年份
                    else if (yearSelect == Integer.parseInt(strYearNow)) {
                        // 出生月份小于当前月份
                        if ((monthOfYearelect + 1) < Integer
                                .parseInt(strMouthNow)) {


                            //判断当前月是否大于10
                            if ((monthOfYearelect + 1) >= 10) {
                                //判断当前日是否大于10
                                if (dayOfMonthSelect >= 10) {
                                    tv_birthday.setText(yearSelect + "-" + (monthOfYearelect + 1) + "-"
                                            + dayOfMonthSelect);
                                } else {
                                    tv_birthday.setText(yearSelect + "-" + (monthOfYearelect + 1) + "-0"
                                            + dayOfMonthSelect);
                                }
                            }
                            //月份小于10
                            else {
                                if (dayOfMonthSelect >= 10) {
                                    tv_birthday.setText(yearSelect + "-0" + (monthOfYearelect + 1) + "-"
                                            + dayOfMonthSelect);
                                } else {
                                    tv_birthday.setText(yearSelect + "-0" + (monthOfYearelect + 1) + "-0"
                                            + dayOfMonthSelect);
                                }


                            }


                        }
                        // 出生月份等于当前月份
                        else if ((monthOfYearelect + 1) == Integer
                                .parseInt(strMouthNow)) {
                            // 出生日小于当前日
                            if (dayOfMonthSelect < Integer
                                    .parseInt(strDayNow)) {

                                //判断当前月是否大于10
                                if ((monthOfYearelect + 1) >= 10) {
                                    //判断当前日是否大于10
                                    if (dayOfMonthSelect >= 10) {
                                        tv_birthday.setText(yearSelect + "-" + (monthOfYearelect + 1) + "-"
                                                + dayOfMonthSelect);
                                    } else {
                                        tv_birthday.setText(yearSelect + "-" + (monthOfYearelect + 1) + "-0"
                                                + dayOfMonthSelect);
                                    }
                                }
                                //月份小于10
                                else {
                                    if (dayOfMonthSelect >= 10) {
                                        tv_birthday.setText(yearSelect + "-0" + (monthOfYearelect + 1) + "-"
                                                + dayOfMonthSelect);
                                    } else {
                                        tv_birthday.setText(yearSelect + "-0" + (monthOfYearelect + 1) + "-0"
                                                + dayOfMonthSelect);
                                    }

                                }


                            }
                            // 出生日等于当前日
                            else if (dayOfMonthSelect == Integer
                                    .parseInt(strDayNow)) {

                                //判断当前月是否大于10
                                if ((monthOfYearelect + 1) >= 10) {
                                    //判断当前日是否大于10
                                    if (dayOfMonthSelect >= 10) {
                                        tv_birthday.setText(yearSelect + "-" + (monthOfYearelect + 1) + "-"
                                                + dayOfMonthSelect);
                                    } else {
                                        tv_birthday.setText(yearSelect + "-" + (monthOfYearelect + 1) + "-0"
                                                + dayOfMonthSelect);
                                    }
                                }
                                //月份小于10
                                else {
                                    if (dayOfMonthSelect >= 10) {
                                        tv_birthday.setText(yearSelect + "-0" + (monthOfYearelect + 1) + "-"
                                                + dayOfMonthSelect);
                                    } else {
                                        tv_birthday.setText(yearSelect + "-0" + (monthOfYearelect + 1) + "-0"
                                                + dayOfMonthSelect);
                                    }


                                }

                            } else {
                                setWarning();
                            }

                        }
                        // 出生月份大于当前月份
                        else {
                            setWarning();
                        }
                    }

                    // 出生年份大于当前年份
                    else {
                        setWarning();
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
                submitData();
            }
        }, year, monthOfYear, dayOfMonth).show();

    }

    private void setWarning() {
        ToastUtil.invokeShortTimeToast(FtjUserInfoActivity.this,
                getString(R.string.user_info_ninxuanzederqccdqrq));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_FROM_ALBUM && resultCode == RESULT_OK && data != null) {
            getImageFromData(data);
        }

        if (requestCode == REQUEST_CODE_FROM_CAMEIA && resultCode == RESULT_OK && data != null) {
            getImageFromData(data);
        }

        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    // 从返回的数据中得到图片
    private void getImageFromData(Intent data) {
        Uri selectedImageUri = data.getData();
        if (selectedImageUri == null) {
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                userIconBitmap = (Bitmap) bundle.get("data");

                File dir = new File(Constant.SAVED_IMAGE_DIR_PATH);

                if (!dir.exists()) {
                    dir.mkdirs();
                }

                saveImage(userIconBitmap, Constant.SAVED_IMAGE_DIR_PATH + "/" + System.currentTimeMillis() + ".jpg");
            }
        } else {

            try {
                userIconBitmap = getThumbnail(selectedImageUri, iv_user_icon.getWidth());
            } catch (IOException e) {
                Logger.e(getClass().getSimpleName(), e);
            }

            ContentResolver cr = getContentResolver();
            String[] projection = {MediaStore.Images.Media.DATA};
            Cursor cursor = cr.query(selectedImageUri, projection, null, null, null);
            if (cursor.moveToFirst()) {
                path = cursor.getString(cursor.getColumnIndex(projection[0]));
            }

            cursor.close();
        }
        uploadUserIcon();
    }

    // 拍照后保存图片
    private void saveImage(Bitmap bitmap, String savedImageDirPath) {
        try {
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(savedImageDirPath, false));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            bos.flush();
            bos.close();
            path = savedImageDirPath;
        } catch (Exception e) {
            e.printStackTrace();
        }
        uploadUserIcon();
    }

    // 上传图片
    private void uploadUserIcon() {
        File file = new File(path);

        uploadManager.uploadFile(file, "image", new AbstractDefaultHttpHandlerCallback(this) {
            @Override
            protected void onResponseSuccess(Object obj) {
                if (userIconBitmap != null) {
                    iv_user_icon.setImageBitmap(userIconBitmap);
                }
                memberImage = (String) obj;

//                userManager.updateMemberInfo(memberImage, gender, name,
//                        mobile, phone, nation, address, isMarried,
//                        identityType, idNumber,birthday, abstractDefaultHttpHandler);
                //todo 上传信息待确定
                userManager.updateMemberInfo(memberImage, gender, name,
                        mobile, "", "", "", 1,
                        "", "",birthday, abstractDefaultHttpHandler);
            }
        });
    }

    //根据uri获取指定大小的bitmap
    public Bitmap getThumbnail(Uri uri, int size) throws IOException {
        InputStream input = this.getContentResolver().openInputStream(uri);

        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inJustDecodeBounds = true;
        onlyBoundsOptions.inDither = true;//optional
        onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional

        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
        input.close();

        if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1))
            return null;
        int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ?
                onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

        double ratio = (originalSize > size) ? (originalSize / size) : 1.0;

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
        bitmapOptions.inDither = true;//optional
        bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional

        input = this.getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
        input.close();
        return bitmap;
    }

    private int getPowerOfTwoForSampleRatio(double ratio) {
        int k = Integer.highestOneBit((int) Math.floor(ratio));
        if (k == 0) {
            return 1;
        }
        else {return k;}
    }
}
