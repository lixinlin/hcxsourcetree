package com.hcy_futejia.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.hcy.ky3h.R;
import com.hcy_futejia.utils.DialogUtils;
import com.hxlm.android.hcy.AbstractBaseActivity;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.asynchttp.HcyHttpClient;
import com.hxlm.android.hcy.asynchttp.HcyHttpResponseHandler;
import com.hxlm.android.hcy.user.ForgetPasswordActivity;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.user.UserManager;
import com.hxlm.hcyandroid.BaseApplication;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.bodydiagnose.IntroductionActivity;
import com.hxlm.hcyandroid.util.IsMobile;
import com.hxlm.hcyandroid.util.StatusBarUtils;
import com.hxlm.hcyandroid.util.ToastUtil;
import com.hxlm.hcyandroid.view.ContainsEmojiEditText;
import com.hxlm.hcyphone.MainActivity;
import com.umeng.commonsdk.UMConfigure;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;

import net.sourceforge.simcpux.Constants;

import java.util.List;
import java.util.Map;

public class EnglishLoginActivity extends AbstractBaseActivity implements View.OnClickListener{


    private ContainsEmojiEditText et_phone;
    private ContainsEmojiEditText et_pwd;
    private TextView tv_forget_password;
    private CheckBox cb;
    private TextView tv_xieyi;
    private Button bt_login;
    private Button bt_login_create;
    private ImageView iv_wx_login;

    private UMShareAPI mShareAPI;
    public static OnCompleteListener onCompleteListener;

    private boolean isChange = false;

    @Override
    public void setContentView() {
        StatusBarUtils.setWindowStatusBarColor(this, R.color.white);
//        if(isWeixinAvilible(this)){
            //有微信
            setContentView(R.layout.activity_english_login_wechat);
//        }else{
//            //没有微信
//            setContentView(R.layout.activity_english_login);
//        }

    }

    @Override
    public void initViews() {
        et_phone = findViewById(R.id.et_phone);
        et_pwd = findViewById(R.id.et_pwd);
        tv_forget_password = findViewById(R.id.tv_forget_password);
        cb = findViewById(R.id.cb);
        tv_xieyi = findViewById(R.id.tv_xieyi);
        bt_login = findViewById(R.id.bt_login);
        bt_login_create = findViewById(R.id.bt_login_create);

//        if (isWeixinAvilible(this)){
            iv_wx_login = findViewById(R.id.iv_wx_login);
            iv_wx_login.setOnClickListener(this);
//        }

        tv_forget_password.setOnClickListener(this);
        bt_login.setOnClickListener(this);
        bt_login_create.setOnClickListener(this);

        SpannableString spannableString = new SpannableString(getString(R.string.ftj_user_xieyi));
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Intent intent = new Intent(EnglishLoginActivity.this, IntroductionActivity.class);
                intent.putExtra("title", getString(R.string.ftj_login_yonghuxieyi));
                if (Constant.isEnglish) {

                    intent.putExtra("introduceCategory", Constant.PRIVICY_POLICY_EN);
                } else {
                    intent.putExtra("introduceCategory", Constant.PRIVICY_POLICY);

                }
                startActivity(intent);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.parseColor("#1e82d2"));
                ds.setUnderlineText(false);
            }
        }, 0, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tv_xieyi.setText(getString(R.string.ftj_woyiyuedu));
        tv_xieyi.append(spannableString);
        //开始响应点击事件
        tv_xieyi.setMovementMethod(LinkMovementMethod.getInstance());

        et_phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable == null || editable.length() == 0) {
                    bt_login.setBackgroundResource(R.drawable.yuanjiaojuxing);
                    return;
                }

                //setText()会回调onTextChanged()，增加标志位防止出现堆栈溢出
                if (isChange) {
                    isChange = false;
                    return;
                }

                isChange = true;

                if (!TextUtils.isEmpty(et_pwd.getText().toString()) && cb.isChecked()) {
                    bt_login.setBackgroundResource(R.drawable.yuanjiaojuxing_login_blue);
                }
            }
        });

        et_pwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s == null || s.length() == 0) {
                    bt_login.setBackgroundResource(R.drawable.yuanjiaojuxing);
                    return;
                } else {
                    if (!TextUtils.isEmpty(et_phone.getText().toString()) && cb.isChecked()) {
                        bt_login.setBackgroundResource(R.drawable.yuanjiaojuxing_login_blue);
                    }
                }
            }
        });

    }

    @Override
    public void initDatas() {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            //忘记密码
            case R.id.tv_forget_password:
                Intent intent1 = new Intent(EnglishLoginActivity.this, ForgetPasswordActivity.class);
                startActivity(intent1);
                break;
            //登录
            case R.id.bt_login:
                String phone = et_phone.getText().toString();
                String pwd = et_pwd.getText().toString();
                if (!TextUtils.isEmpty(phone) && !TextUtils.isEmpty(pwd)){
//                    boolean email = IsMobile.isEmail(phone);
//                    boolean englishMobileNO = IsMobile.isEnglishMobileNO(phone);
//                    if (email || englishMobileNO){
                        goLoginMM(phone,pwd);
//                    }else{
//                        ToastUtil.invokeShortTimeToast(EnglishLoginActivity.this, getString(R.string.login_phone_format_error));
//                    }
                }else{

                }
                break;
            //创建新用户
            case R.id.bt_login_create:
                Intent intent = new Intent(EnglishLoginActivity.this, CreateAccountActivity.class);
                startActivity(intent);
                BaseApplication.addDestoryActivity(EnglishLoginActivity.this,"loginActivity");
                break;
            //微信登录
            case R.id.iv_wx_login:
                if (isWeixinAvilible(EnglishLoginActivity.this)) {
                    HcyHttpResponseHandler responseHandler = new HcyHttpResponseHandler(null) {
                        @Override
                        protected Object contentParse(String content) {
                            com.alibaba.fastjson.JSONObject data = JSON.parseObject(content);
                            String secret = data.getString("secret");
                            UMConfigure.init(EnglishLoginActivity.this, content, "umeng", UMConfigure.DEVICE_TYPE_PHONE, "");
                            PlatformConfig.setWeixin(Constants.APP_ID, secret);
                            shengQingQuanXian();
                            mShareAPI = UMShareAPI.get(EnglishLoginActivity.this);
                            mShareAPI.getPlatformInfo(EnglishLoginActivity.this, SHARE_MEDIA.WEIXIN, umAuthListener);
                            return null;
                        }
                    };
                    HcyHttpClient.getSecret(responseHandler);
                }else{
                    ToastUtil.invokeShortTimeToast(EnglishLoginActivity.this,getString(R.string.other_pay_tips_not_installed_wechat));
                }
                break;
            default:
                break;
        }
    }

    //登录
    private void goLoginMM(final String userName, final String password) {

        if (cb.isChecked()) {
            new UserManager().login(userName, password, new AbstractDefaultHttpHandlerCallback(EnglishLoginActivity.this) {
                @Override
                protected void onResponseSuccess(Object obj) {
                    LoginControllor.setAutoLoginByMiMa(true, userName, password);
                    LoginControllor.setIsAutologinBySms(false, "");
                    LoginControllor.setAutoLoginByWeiChart(false, "", "", "", "");
                    if (null != onCompleteListener) {
                        onCompleteListener.onComplete();
                    }
                    Constant.loginType = 1;
                    //跳转主界面
                    toMainActivity();
                }

            });
        } else {
            ToastUtil.invokeShortTimeToast(EnglishLoginActivity.this, getString(R.string.ftj_login_cb_tips));
        }

    }

    private void shengQingQuanXian() {
        if (Build.VERSION.SDK_INT >= 23) {
            String[] mPermissionList = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CALL_PHONE, Manifest.permission.READ_LOGS, Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.SET_DEBUG_APP, Manifest.permission.SYSTEM_ALERT_WINDOW, Manifest.permission.GET_ACCOUNTS, Manifest.permission.WRITE_APN_SETTINGS};
            ActivityCompat.requestPermissions(this, mPermissionList, 123);
        }
    }

    /**
     * 判断是否有微信
     * @param context
     * @return
     */
    public boolean isWeixinAvilible(Context context) {
        // 获取packagemanager
        PackageManager packageManager = context.getPackageManager();
        // 获取所有已安装程序的包信息
        List<PackageInfo> pinfo = packageManager.getInstalledPackages(0);
        if (pinfo != null) {
            for (int i = 0; i < pinfo.size(); i++) {
                String pn = pinfo.get(i).packageName;
                if (pn.equals("com.tencent.mm")) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }


    UMAuthListener umAuthListener = new UMAuthListener() {
        /**
         * @desc 授权开始的回调
         * @param platform 平台名称
         */
        @Override
        public void onStart(SHARE_MEDIA platform) {
        }

        /**
         * @desc 授权成功的回调
         * @param platform 平台名称
         * @param action 行为序号，开发者用不上
         * @param data 用户资料返回
         */
        @Override
        public void onComplete(SHARE_MEDIA platform, int action, Map<String, String> data) {
//            上传用户信息进行注册。
            new UserManager().loginByWeiXin(
                    data.get("unionid"),
                    data.get("screen_name"),
                    data.get("gender"),
                    data.get("profile_image_url"),
                    new AbstractDefaultHttpHandlerCallback(EnglishLoginActivity.this) {
                        @Override
                        protected void onResponseSuccess(Object obj) {
                            boolean success = (boolean) obj;
                            if (success) {
                                LoginControllor.setAutoLoginByWeiChart(
                                        true,
                                        data.get("unionid"),
                                        data.get("screen_name"),
                                        data.get("gender"),
                                        data.get("profile_image_url"));
                                LoginControllor.setIsAutologinBySms(false, "");
                                LoginControllor.setAutoLoginByMiMa(false, "", "");
                                if (null != onCompleteListener) {
                                    onCompleteListener.onComplete();
                                }
                                mShareAPI.deleteOauth(EnglishLoginActivity.this, SHARE_MEDIA.WEIXIN, umDeleteOauthListener);
                                //跳转主界面
                                Constant.loginType = 2;
                                toMainActivity();
                            } else {

                            }

                        }
                    }
            );
        }

        /**
         * @desc 授权失败的回调
         * @param platform 平台名称
         * @param action 行为序号，开发者用不上
         * @param t 错误原因
         */
        @Override
        public void onError(SHARE_MEDIA platform, int action, Throwable t) {
            Toast.makeText(EnglishLoginActivity.this, getString(R.string.login_authorization_failure), Toast.LENGTH_SHORT).show();
        }

        /**
         * @desc 授权取消的回调
         * @param platform 平台名称
         * @param action 行为序号，开发者用不上
         */
        @Override
        public void onCancel(SHARE_MEDIA platform, int action) {
            Toast.makeText(EnglishLoginActivity.this, getString(R.string.login_authorization_cancel), Toast.LENGTH_SHORT).show();
        }
    };
    UMAuthListener umDeleteOauthListener = new UMAuthListener() {

        @Override
        public void onStart(SHARE_MEDIA share_media) {
        }

        @Override
        public void onComplete(SHARE_MEDIA share_media, int i, Map<String, String> map) {
        }

        @Override
        public void onError(SHARE_MEDIA share_media, int i, Throwable throwable) {
        }

        @Override
        public void onCancel(SHARE_MEDIA share_media, int i) {
        }
    };

    private void toMainActivity() {
        Intent intentMain = new Intent(EnglishLoginActivity.this, MainActivity.class);
        startActivity(intentMain);
        EnglishLoginActivity.this.finish();
    }

}
