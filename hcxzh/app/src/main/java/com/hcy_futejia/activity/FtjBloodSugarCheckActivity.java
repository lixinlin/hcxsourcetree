package com.hcy_futejia.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hcy_futejia.utils.DateUtil;
import com.hcy_futejia.widget.ChangeDatePopwindow;
import com.hcy_futejia.widget.ChangeSugarTimeWindow;
import com.hcy_futejia.widget.ChangeTimePopwindow;
import com.hcy_futejia.widget.RulerView;
import com.hxlm.android.hcy.OnCompleteListener;
import com.hxlm.android.hcy.asynchttp.AbstractDefaultHttpHandlerCallback;
import com.hxlm.android.hcy.report.RecordManager;
import com.hxlm.android.hcy.report.UploadManager;
import com.hxlm.android.hcy.user.ChildMember;
import com.hxlm.android.hcy.user.ChooseMemberDialog;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.BaseActivity;
import com.hxlm.hcyandroid.CheckedDataType;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.bean.BloodSugar;
import com.hxlm.hcyandroid.bean.BloodSugarData;
import com.hxlm.hcyandroid.tabbar.MyHealthFileBroadcast;
import com.hxlm.hcyandroid.tabbar.sicknesscheck.BloodSugarCheckActivity;
import com.hxlm.hcyandroid.util.SharedPreferenceUtil;
import com.hxlm.hcyandroid.util.ToastUtil;
import com.hxlm.hcyandroid.view.XRTextView;
import com.hxlm.hcyphone.MainActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class FtjBloodSugarCheckActivity extends BaseActivity implements View.OnClickListener {

    private LinearLayout ll_sugar_date;
    private TextView tv_sugar_date;
    private LinearLayout ll_sugar_time;
    private TextView tv_sugar_time;
    private LinearLayout ll_sugar_time_quantum;
    private TextView tv_sugar_time_quantum;
    private TextView tv_sugar_submit;
    private RulerView rv_sugar;

    private int mYear;
    private int mMonth;
    private int mDay;

    private int mHour;
    private int mMinute;
    private int mApm;
    private LinearLayout ll_sugar_check;

    private String str_blood_sugar_value;

    private int index = 0;// 设置下标
    private int index_fasting = 0;// 空腹
    private int index_after_meal = 0;// 餐后
    private int num_submit = 0;// 记录用户提交的次数
    private int fasting_num = 0;// 空腹提交次数

    private int after_meal_num = 0;// 饭后提交次数
    private int normal_fasting = 0;// 空腹正常次数
    private int not_normal_fasting = 0;// 空腹不正常次数

    private int normal_after_meal = 0;// 饭后正常次数
    private int not_normal_after_meal = 0;// 饭后不正常次数

    private Context context;
    private BloodSugarData bloodSugarData;
    private String isNormal;// 是否正常

    private RecordManager recordManager;
    private UploadManager uploadManager;

    double value_fasting = 0;// 空腹
    int num_fasting = -1;// 空腹血糖是否正常
    double value_after_meal = 0;// 饭后
    int num_after_meal = -1;// 饭后血糖是否正常

    private Map<String, List<BloodSugar>> map;
    private List<BloodSugar> emptyData;// 空腹值
    private List<BloodSugar> fullData;// 饭后值

    private XRTextView tv_bloodsugar_prompt_cishu;//提交次数
    private XRTextView tv_bloodsugar_prompt_xuetang;//血糖值

    String pattern = "yyyy-MM-dd HH:mm";

    @Override
    public void setContentView() {
        setContentView(R.layout.activity_ftj_blood_sugar_check);
        context = FtjBloodSugarCheckActivity.this;
    }

    @Override
    public void initViews() {
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.sugar_title), titleBar, 1);

        ll_sugar_check = findViewById(R.id.ll_sugar_check);
        ll_sugar_date = findViewById(R.id.ll_sugar_date);
        tv_sugar_date = findViewById(R.id.tv_sugar_date);
        ll_sugar_time = findViewById(R.id.ll_sugar_time);
        tv_sugar_time = findViewById(R.id.tv_sugar_time);
        ll_sugar_time_quantum = findViewById(R.id.ll_sugar_time_quantum);
        tv_sugar_time_quantum = findViewById(R.id.tv_sugar_time_quantum);
        tv_sugar_submit = findViewById(R.id.tv_sugar_submit);
        rv_sugar = findViewById(R.id.rv_sugar);

        ll_sugar_date.setOnClickListener(this);
        ll_sugar_time.setOnClickListener(this);
        ll_sugar_time_quantum.setOnClickListener(this);
        tv_sugar_submit.setOnClickListener(this);

        Calendar calendar = Calendar.getInstance();
        mYear = calendar.get(Calendar.YEAR);
        mMonth = calendar.get(Calendar.MONTH)+1;
        mDay = calendar.get(Calendar.DAY_OF_MONTH);
        if (mMonth < 10 && mDay < 10){
            tv_sugar_date.setText(mYear+"/0"+mMonth+"/0"+mDay);
        }else if (mMonth < 10 && mDay >= 10){
            tv_sugar_date.setText(mYear+"/0"+mMonth+"/"+mDay);
        }else if (mDay < 10 && mMonth >= 10){
            tv_sugar_date.setText(mYear+"/"+mMonth+"/0"+mDay);
        }else{
            tv_sugar_date.setText(mYear+"/"+mMonth+"/"+mDay);
        }
        mHour = calendar.get(Calendar.HOUR);
        mMinute = calendar.get(Calendar.MINUTE);
        mApm = calendar.get(Calendar.AM_PM);
        if (mHour < 10 && mMinute < 10){
            tv_sugar_time.setText("0"+mHour + ":" + "0"+mMinute);
        }else if (mHour < 10 && mMinute >=10){
            tv_sugar_time.setText("0"+mHour + ":" + mMinute);
        }else if (mHour >= 10 && mMinute <10){
            tv_sugar_time.setText(mHour + ":" + "0"+mMinute);
        }else {
            tv_sugar_time.setText(mHour + ":" + mMinute);
        }
        tv_sugar_time_quantum.setText(getString(R.string.sugar_wucanqian));

        rv_sugar.setOnChooseResulterListener(new RulerView.OnChooseResulterListener() {
            @Override
            public void onEndResult(String result) {
                Log.e("retrofit","==血糖result=="+result);
                str_blood_sugar_value = result;
            }

            @Override
            public void onScrollResult(String result) {
                str_blood_sugar_value = result;
            }
        });

    }

    @Override
    public void initDatas() {
        recordManager = new RecordManager();
        uploadManager = new UploadManager();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_sugar_date:
                ChangeDatePopwindow changeDatePopwindow = new ChangeDatePopwindow(FtjBloodSugarCheckActivity.this);
                changeDatePopwindow.setDate(""+mYear + "", ""+mMonth + "", ""+ mDay + "");
                changeDatePopwindow.showAtLocation(ll_sugar_check, Gravity.BOTTOM, 0, 0);
                changeDatePopwindow.setBirthdayListener(new ChangeDatePopwindow.OnBirthListener() {
                    @Override
                    public void onClick(String year, String month, String day) {

                        String y = "";
                        String m = "";
                        String d = "";
                        String strDate = "";
                        if (Constant.isEnglish){
                            tv_sugar_date.setText(year+"/"+month+"/"+day);
                        }else {
                            if (year.equals("" + mYear) || month.equals("" + mMonth) || day.equals("" + mDay)) {
                                if (year.equals("" + mYear)) {
                                    y = year + "年";
                                } else {
                                    y = year;
                                }
                                if (month.equals("" + mMonth)) {
                                    m = month + "月";
                                } else {
                                    m = month;
                                }
                                if (day.equals("" + mDay)) {
                                    d = day + "日";
                                } else {
                                    d = day;
                                }
                                strDate = y + m + d;
                            } else {
                                strDate = year + month + day;
                            }

                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日");
                            try {
                                Date parse = simpleDateFormat.parse(strDate);
                                SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy/MM/dd");
                                String format = simpleDateFormat1.format(parse);
                                tv_sugar_date.setText(format);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                });
                break;
            case R.id.ll_sugar_time:
                ChangeTimePopwindow changeTimePopwindow = new ChangeTimePopwindow(FtjBloodSugarCheckActivity.this);
                changeTimePopwindow.setDate("" +mApm, ""+mHour + "", ""+ mMinute + "");
                changeTimePopwindow.showAtLocation(ll_sugar_check, Gravity.BOTTOM, 0, 0);
                changeTimePopwindow.setBirthdayListener(new ChangeTimePopwindow.OnBirthListener() {
                    @Override
                    public void onClick(String year, String month, String day) {
                        tv_sugar_time.setText(month+":"+day);
                    }
                });
                break;
            case R.id.ll_sugar_time_quantum:
                ChangeSugarTimeWindow changeSugarTimeWindow = new ChangeSugarTimeWindow(FtjBloodSugarCheckActivity.this, tv_sugar_time_quantum.getText().toString());
                changeSugarTimeWindow.showAtLocation(ll_sugar_check, Gravity.BOTTOM, 0, 0);
                changeSugarTimeWindow.setOnTimeListener(new ChangeSugarTimeWindow.OnTimeListener() {
                    @Override
                    public void onClick(String time) {
                        tv_sugar_time_quantum.setText(time);
                    }
                });
                break;
            case R.id.tv_sugar_submit:

                if (!TextUtils.isEmpty(str_blood_sugar_value)) {
                    if (Double.parseDouble(str_blood_sugar_value) > 100) {
                        ToastUtil.invokeShortTimeToast(context, getString(R.string.bsc_tips_sugar_more_range));
                    }
                    // 点击提交
                    else {
                        num_submit++;// 次数加1
                        // 将总提交次数保存
                        SharedPreferenceUtil.saveString("num_submit", num_submit + "");

                        bloodSugarData = new BloodSugarData();
                        String s = tv_sugar_time_quantum.getText().toString();
                        Log.e("retrofit","====血糖时间段===="+s);
                        if (s.equals(getString(R.string.sugar_lingchen))){
                            bloodSugarData.setType(BloodSugarData.BLOOD_SUGAR_TYPE_BEFORE_DAWN);
                            index = 1;
                        }else if (s.equals(getString(R.string.sugar_zaocanqian))){
                            bloodSugarData.setType(BloodSugarData.BLOOD_SUGAR_TYPE_BEFORE_BREAKFAST);
                            index = 1;
                        }else if (s.equals(getString(R.string.sugar_zaocanhou))){
                            bloodSugarData.setType(BloodSugarData.BLOOD_SUGAR_TYPE_AFTER_BREAKFAST);
                            index = 2;
                        }else if (s.equals(getString(R.string.sugar_wucanqian))){
                            bloodSugarData.setType(BloodSugarData.BLOOD_SUGAR_TYPE_EMPTY);
                            index = 1;
                        }else if (s.equals(getString(R.string.sugar_wucanhou))){
                            bloodSugarData.setType(BloodSugarData.BLOOD_SUGAR_TYPE_FULL);
                            index = 2;
                        }else if (s.equals(getString(R.string.sugar_wancanqian))){
                            bloodSugarData.setType(BloodSugarData.BLOOD_SUGAR_TYPE_BEFORE_DINNER);
                            index = 1;
                        }else if (s.equals(getString(R.string.sugar_wancanhou))){
                            bloodSugarData.setType(BloodSugarData.BLOOD_SUGAR_TYPE_AFTER_DINNER);
                            index = 2;
                        }else{
                            bloodSugarData.setType(BloodSugarData.BLOOD_SUGAR_TYPE_BEFORE_SLEEP);
                            index = 1;
                        }
                            // 只是选择了空腹提交
                        if (index == 1) {
                            fasting_num++;
                            // 空腹提交总次数
                            SharedPreferenceUtil.saveString("fasting_num", fasting_num + "");

                            // 记录空腹数据
                            final double double_blood_sugar_value1 = Double.parseDouble(str_blood_sugar_value);
                            value_fasting = double_blood_sugar_value1;

//                            bloodSugarData.setType(BloodSugarData.BLOOD_SUGAR_TYPE_EMPTY);
                            bloodSugarData.setLevels(double_blood_sugar_value1);

                            if (double_blood_sugar_value1 > 0 && double_blood_sugar_value1 < 3.9) {

                                isNormal = getString(R.string.bsc_isNormal1);
                                not_normal_fasting++;
                                SharedPreferenceUtil.saveString("not_normal_fasting", not_normal_fasting + "");

                                bloodSugarData.setAbnormity(true);

                            } else if (double_blood_sugar_value1 >= 3.9 && double_blood_sugar_value1 <= 6.1) {
                                isNormal = getString(R.string.bsc_isNormal2);

                                normal_fasting++;
                                SharedPreferenceUtil.saveString("normal_fasting", normal_fasting + "");

                                bloodSugarData.setAbnormity(false);

                            } else if (double_blood_sugar_value1 > 6.1) {

                                isNormal = getString(R.string.bsc_isNormal3);
                                not_normal_fasting++;
                                SharedPreferenceUtil.saveString("not_normal_fasting", not_normal_fasting + "");

                                bloodSugarData.setAbnormity(true);
                            }
                        } else if (index == 2) {
                            after_meal_num++;
                            // 饭后提交总次数
                            SharedPreferenceUtil.saveString("after_meal_num", after_meal_num + "");

                            // 饭后数据
                            final double double_blood_sugar_value2 = Double.parseDouble(str_blood_sugar_value);
                            value_after_meal = double_blood_sugar_value2;

//                            bloodSugarData.setType(BloodSugarData.BLOOD_SUGAR_TYPE_FULL);
                            bloodSugarData.setLevels(double_blood_sugar_value2);

                            if (double_blood_sugar_value2 > 0 && double_blood_sugar_value2 <= 7.8) {
                                isNormal = getString(R.string.bsc_isNormal2);

                                normal_after_meal++;
                                SharedPreferenceUtil.saveString("normal_after_meal", normal_after_meal + "");

                                bloodSugarData.setAbnormity(false);

                            } else if (double_blood_sugar_value2 > 7.8) {
                                isNormal = getString(R.string.bsc_isNormal3);

                                not_normal_after_meal++;
                                SharedPreferenceUtil.saveString("not_normal_after_meal", not_normal_after_meal + "");

                                bloodSugarData.setAbnormity(true);
                            }
                        }

                        LoginControllor.requestLogin(FtjBloodSugarCheckActivity.this, new OnCompleteListener() {
                            @Override
                            public void onComplete() {
                                List<ChildMember> childMembers = LoginControllor.getLoginMember().getMengberchild();
                                int size=childMembers.size();
                                if(true){
                                    String strDate = tv_sugar_date.getText().toString();
                                    String strTime = tv_sugar_time.getText().toString();
                                    long stringToDate = DateUtil.getStringToDate(strDate+" " + strTime, pattern);
                                    uploadManager.uploadCheckedData(CheckedDataType.BLOOD_SUGAR, bloodSugarData, stringToDate,
                                            new AbstractDefaultHttpHandlerCallback(FtjBloodSugarCheckActivity.this) {
                                                @Override
                                                protected void onResponseSuccess(Object obj) {
                                                    // 提交成功之后，获取空腹和餐后列表
                                                    // 测试请求接口
                                                    recordManager.getBloodSugarReport(new AbstractDefaultHttpHandlerCallback(
                                                            FtjBloodSugarCheckActivity.this) {
                                                        @Override
                                                        protected void onResponseSuccess(Object obj) {
                                                            map = (Map<String, List<BloodSugar>>) obj;
                                                            emptyData = map.get("empty");
                                                            fullData = map.get("full");

                                                            // 异常空腹数据
                                                            List<BloodSugar> listemptyDataTrue = new ArrayList<>();
                                                            for (BloodSugar anEmptyData1 : emptyData) {
                                                                if (anEmptyData1.isAbnormity()) {
                                                                    listemptyDataTrue.add(anEmptyData1);
                                                                }
                                                            }
                                                            // 异常饭后数据
                                                            List<BloodSugar> listfullDataTrue = new ArrayList<>();
                                                            for (BloodSugar aFullData : fullData) {
                                                                if (aFullData.isAbnormity()) {
                                                                    listfullDataTrue.add(aFullData);
                                                                }
                                                            }
                                                            //正常空腹数据
                                                            List<BloodSugar> listNormalEmpty = new ArrayList<>();
                                                            for (BloodSugar anEmptyData : emptyData) {
                                                                if (!anEmptyData.isAbnormity()) {
                                                                    listNormalEmpty.add(anEmptyData);
                                                                }
                                                            }
                                                            //正常饭后数据
                                                            List<BloodSugar> listNormalFull = new ArrayList<>();
                                                            for (BloodSugar aFullData : fullData) {
                                                                if (!aFullData.isAbnormity()) {
                                                                    listNormalFull.add(aFullData);
                                                                }
                                                            }

                                                            int allNumber = emptyData.size() + fullData.size();//所有
                                                            int normalNumber = listNormalEmpty.size() + listNormalFull.size();//正常
                                                            int abnaormalkNumber = listemptyDataTrue.size() + listfullDataTrue.size();//异常

                                                            if (index == 1) {
                                                                BloodSugarDialog dialog = new BloodSugarDialog(
                                                                        FtjBloodSugarCheckActivity.this, value_fasting + "", allNumber,
                                                                        normalNumber, abnaormalkNumber, isNormal, listemptyDataTrue,
                                                                        listfullDataTrue);
                                                                dialog.show();
                                                            } else if (index == 2) {
                                                                BloodSugarDialog dialog = new BloodSugarDialog(
                                                                        FtjBloodSugarCheckActivity.this, value_after_meal + "", allNumber,
                                                                        normalNumber, abnaormalkNumber, isNormal, listemptyDataTrue,
                                                                        listfullDataTrue);
                                                                dialog.show();
                                                            }
                                                        }
                                                    });
                                                }
                                            });
                                }

                            }
                        });
                    }
                } else {
                    ToastUtil.invokeShortTimeToast(context, getString(R.string.bsc_tips_not_input));
                }
                break;
            default:
                break;
        }

    }

    public class BloodSugarDialog extends AlertDialog implements
            View.OnClickListener {

        Context context;

        int bloodAllNumber;//所有的血糖次数
        int normalNumber;//正常次数
        int abnormalNumber;//异常次数

        String isnormal;
        String str_tishi = "";
        List<BloodSugar> emptyData;// 空腹值
        List<BloodSugar> fullData;// 饭后值
        private TextView text_back;
        private TextView text_see_dangan;


        public BloodSugarDialog(Context context, String xuetangzhi, int bloodAllNumber, int normalNumber, int abnormalNumber,
                                String isnormal, List<BloodSugar> emptyData,
                                List<BloodSugar> fullData) {
            super(context);
            this.context = context;
            this.bloodAllNumber = bloodAllNumber;
            this.normalNumber = normalNumber;
            this.abnormalNumber = abnormalNumber;
            this.isnormal = isnormal;
            this.emptyData = emptyData;
            this.fullData = fullData;

        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.bloodsugar_submit_prompt);


            final ScrollView scrollview = (ScrollView) findViewById(R.id.scrollview);
            scrollview.post(new Runnable() {
                // 让scrollview跳转到顶部，必须放在runnable()方法中
                @Override
                public void run() {
                    scrollview.scrollTo(0, 0);
                }
            });

            // 次数
            tv_bloodsugar_prompt_cishu = (XRTextView) findViewById(R.id.tv_bloodsugar_prompt_cishu);
            //返回检测
            text_back = findViewById(R.id.text_back);
            text_back.setOnClickListener(this);
            //查看档案
            text_see_dangan = findViewById(R.id.text_see_dangan);
            text_see_dangan.setOnClickListener(this);

            String str = getString(R.string.bsc_dialog_text1) + bloodAllNumber + getString(R.string.bsc_dialog_text2)
                    + normalNumber + getString(R.string.bsc_dialog_text3)
                    + abnormalNumber + getString(R.string.bsc_dialog_text4);
            String source = str;
            SpannableString ss = new SpannableString(source);
            tv_bloodsugar_prompt_cishu.setMText(ss);

            tv_bloodsugar_prompt_cishu.setTextSize(14);
            tv_bloodsugar_prompt_cishu.setTextColor(getResources().getColor(
                    R.color.submit_chengse));
            tv_bloodsugar_prompt_cishu.invalidate();


            // 血糖值
            tv_bloodsugar_prompt_xuetang = (XRTextView) findViewById(R.id.tv_bloodsugar_prompt_xuetang);
            // 血糖
            TextView tv_is_normal = (TextView) findViewById(R.id.tv_is_normal);

            // 血糖是否正常
            LinearLayout linearlayout = (LinearLayout) findViewById(R.id.linearlayout);
//            // 空腹
//            ListView lv_kongfu = (ListView) findViewById(R.id.lv_kongfu);
//            // 饭后
//            ListView lv_canhou = (ListView) findViewById(R.id.lv_canhou);

//            BloodSugarDialog.DialogLvAdapterEmpty adapter1 = new BloodSugarDialog.DialogLvAdapterEmpty(
//                    FtjBloodSugarCheckActivity.this, emptyData);
//            lv_kongfu.setAdapter(adapter1);
//
//            setListHeight(lv_kongfu);
//
//            BloodSugarDialog.DialogLvAdapterFull adapter2 = new BloodSugarDialog.DialogLvAdapterFull(
//                    FtjBloodSugarCheckActivity            lv_canhou.setAdapter(adapter2);
//
//            setListHeight(lv_canhou);


            if (index == 1) {
                String strkonghfu = getString(R.string.bsc_dialog_measure_kongfu_value)
                        + value_fasting + "mmol/L";
                String source2 = strkonghfu;
                SpannableString ss2 = new SpannableString(source2);
                tv_bloodsugar_prompt_xuetang.setMText(ss2);

                tv_bloodsugar_prompt_xuetang.setTextSize(14);
                tv_bloodsugar_prompt_xuetang.setTextColor(getResources().getColor(
                        R.color.submit_chengse));
                tv_bloodsugar_prompt_xuetang.invalidate();

                if (getString(R.string.bsc_isNormal2).equals(isnormal)) {
                    str_tishi = getString(R.string.bsc_dialog_normal2_result);
                    SpannableStringBuilder style = new SpannableStringBuilder(
                            str_tishi);
                    style.setSpan(new ForegroundColorSpan(getResources()
                                    .getColor(R.color.submit_normal)), 4, 6,
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tv_is_normal.setText(style);

                    linearlayout.setVisibility(View.GONE);

                } else if (getString(R.string.bsc_isNormal1).equals(isnormal)) {
                    str_tishi = getString(R.string.bsc_dialog_normal1_result1) + value_fasting
                            + "mmol/L，"+getString(R.string.bsc_dialog_normal1_result2);
                    SpannableStringBuilder style = new SpannableStringBuilder(
                            str_tishi);
                    style.setSpan(new ForegroundColorSpan(getResources()
                                    .getColor(R.color.submit_not_normal)), 5, 7,
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tv_is_normal.setText(style);

                    linearlayout.setVisibility(View.VISIBLE);

                } else if (getString(R.string.bsc_isNormal3).equals(isnormal)) {
                    str_tishi = getString(R.string.bsc_dialog_normal3_result1) + value_fasting
                            + "mmol/L，"+getString(R.string.bsc_dialog_normal3_result2);
                    SpannableStringBuilder style = new SpannableStringBuilder(
                            str_tishi);
                    style.setSpan(new ForegroundColorSpan(getResources()
                                    .getColor(R.color.submit_not_normal)), 5, 7,
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tv_is_normal.setText(style);

                    linearlayout.setVisibility(View.VISIBLE);
                }

            } else if (index == 2) {
                String strfanhou = getString(R.string.bsc_dialog_fanhou_value)
                        + value_after_meal + "mmol/L";
                String source3 = strfanhou;
                SpannableString ss3 = new SpannableString(source3);
                tv_bloodsugar_prompt_xuetang.setMText(ss3);

                tv_bloodsugar_prompt_xuetang.setTextSize(14);
                tv_bloodsugar_prompt_xuetang.setTextColor(getResources().getColor(
                        R.color.submit_chengse));
                tv_bloodsugar_prompt_xuetang.invalidate();

                if (getString(R.string.bsc_isNormal2).equals(isnormal)) {
                    str_tishi = getString(R.string.bsc_dialog_normal2_result);
                    SpannableStringBuilder style = new SpannableStringBuilder(
                            str_tishi);
                    style.setSpan(new ForegroundColorSpan(getResources()
                                    .getColor(R.color.submit_normal)), 4, 6,
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tv_is_normal.setText(style);

                    linearlayout.setVisibility(View.GONE);

                } else if (getString(R.string.bsc_isNormal3).equals(isnormal)) {
                    str_tishi = getString(R.string.bsc_dialog_normal3_fanhou) + value_after_meal
                            + "mmol/L，"+getString(R.string.bsc_dialog_normal3_result2);
                    SpannableStringBuilder style = new SpannableStringBuilder(
                            str_tishi);
                    style.setSpan(new ForegroundColorSpan(getResources()
                                    .getColor(R.color.submit_not_normal)), 8, 10,
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tv_is_normal.setText(style);

                    linearlayout.setVisibility(View.VISIBLE);
                }
            }

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                // 确定
                case R.id.text_back:
//                    strResult.setText("");
                    this.dismiss();
                    break;
                case R.id.text_see_dangan:
                    this.dismiss();
                    // 动态注册广播使用隐士Intent
                    Intent intent=new Intent(MyHealthFileBroadcast.ACTION);
                    intent.putExtra("ArchivesFragment", "3");
                    intent.putExtra("otherReport", "true");
                    intent.putExtra("aaa","123");
                    intent.putExtra("Jump",6);
                    intent.putExtra("tag","blood");
                    FtjBloodSugarCheckActivity.this.sendBroadcast(intent);

                    Intent intent2 = new Intent(FtjBloodSugarCheckActivity.this, MainActivity.class);
//                    intent2.putExtra("mainId",2);
                    intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//清除MainActivity之前所有的activity
                    intent2.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP); //沿用之前的MainActivity
                    startActivity(intent2);
                    default:
                    break;

            }
        }

        // 实现两个ListView在Scrollview中同时滑动
        private void setListHeight(ListView listView) {
            ListAdapter list = listView.getAdapter();
            if (list == null) {
                return;
            }
            int height = 0;
            for (int i = 0; i < list.getCount(); i++) {
                View listItem = list.getView(i, null, listView);
                listItem.measure(0, 0); // 计算子项View 的宽高
                height += listItem.getMeasuredHeight(); // 统计所有子项的总高度
            }
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = height
                    + (listView.getDividerHeight() * (list.getCount() - 1));
            // listView.getDividerHeight()获取子项间分隔符占用的高度
            // params.height最后得到整个ListView完整显示需要的高度
            listView.setLayoutParams(params);
        }

    }
}
