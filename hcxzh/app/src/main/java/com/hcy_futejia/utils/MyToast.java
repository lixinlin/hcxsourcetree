package com.hcy_futejia.utils;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.hcy.ky3h.R;

/**
 * @author Lirong
 * @date 2019/4/1.
 * @description
 */

public class MyToast {
    public static final int LENGTH_SHORT = 0;
    public static final int LENGTH_LONG = 1;
    Toast toast;
    TextView tvToastText;
    Context context;
    public MyToast(Context context) {
        this.context = context; toast = new Toast(context);
        /**
     * 这里：根据自定义的toast xml样式进行加载展示，类似于其他xml对应的view一样
     *      通过InflaterLayout 展示整个rootView 再将内部的toast附着在这个viewRoot上
     */
         LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         View toastRoot = inflater.inflate(R.layout.ftj_toast_item, null);
         tvToastText = (TextView) toastRoot.findViewById(R.id.tvToast);
         toast.setView(toastRoot);
    }

    public void setDuration(int duration) {
        toast.setDuration(duration);
    }

    public void setText(CharSequence text) {
        tvToastText.setText(text);
    }

    /**
     * 这里默认选用屏幕中心为坐标原点，
     * @param xOffset X轴偏离中心的距离（往右为正）
     * @param yOffset Y轴偏离中心的距离（往下为正）
     */
     public void setGravity(int xOffset, int yOffset) {
         toast.setGravity(Gravity.BOTTOM, xOffset, yOffset);
     }
     /**
     *
     * @param context
     * @param text     toast展示文本
     * @param duration toast展示时间
     * @return
     */
     public static MyToast makeText(Context context, CharSequence text, int duration ) {
         MyToast customedToast = new MyToast(context);
         customedToast.setText(text); customedToast.setDuration(duration);
         customedToast.setGravity(0, 0);
         return customedToast;
     }

     public void show() {
         toast.show();
     }

}
