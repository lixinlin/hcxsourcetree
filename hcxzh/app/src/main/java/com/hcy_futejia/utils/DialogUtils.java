package com.hcy_futejia.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hcy.ky3h.R;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Lirong
 * @date 2019/4/1.
 * @description
 */

public class DialogUtils {

    /**
     * 一个按钮
     * @param context
     */
    public static void setDialogOneBtn(Context context){
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_utils_btn1, null);
        Dialog dialog = new Dialog(context,R.style.dialog);
        dialog.setContentView(view);
        TextView tv_btn1_tips = view.findViewById(R.id.tv_btn1_tips);
        TextView tv_dialog_ok = view.findViewById(R.id.tv_dialog_ok);
        dialog.show();
    }

    /**
     * 两个按钮
     * @param context
     */
    public static void setDialogTwoBtn(Context context){
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_utils_btn2, null);
        Dialog dialog = new Dialog(context,R.style.dialog);
        dialog.setContentView(view);
        TextView tv_btn2_tips = view.findViewById(R.id.tv_btn2_tips);
        TextView tvCancel = view.findViewById(R.id.tvCancel);
        TextView tvConfirm = view.findViewById(R.id.tvConfirm);
        dialog.show();
    }

    /**
     * 三个按钮
     * @param context
     */
    public static void setDialogThreeBtn(Context context){
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_utils_btn3, null);
        Dialog dialog = new Dialog(context,R.style.dialog);
        dialog.setContentView(view);
        TextView tv_btn3_tips = view.findViewById(R.id.tv_btn3_tips);
        TextView tv_1 = view.findViewById(R.id.tv_1);
        TextView tv_2 = view.findViewById(R.id.tv_2);
        TextView tv_3 = view.findViewById(R.id.tv_3);
        dialog.show();
    }

    /**
     * 带输入框
     * @param context
     */
    public static void setDialogInput(Context context,String hintText){
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_utils_input, null);
        Dialog dialog = new Dialog(context,R.style.dialog);
        dialog.setContentView(view);
        TextView tv_input_title = view.findViewById(R.id.tv_input_title);
        EditText et_input = view.findViewById(R.id.et_input);
        TextView tv_back = view.findViewById(R.id.tv_back);
        TextView tv_sure = view.findViewById(R.id.tv_sure);

        dialog.show();
    }

    /**
     * 带进度条
     * @param context
     */
    public static Map<String, Object> setDialogProgressBar(Context context, String content, boolean necessary, View.OnClickListener onClickListener){
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_utils_progressbar, null);
        Dialog dialog = new Dialog(context,R.style.dialog);
        dialog.setContentView(view);
        TextView tv_input_title = view.findViewById(R.id.tv_input_title);
        TextView tv_content = view.findViewById(R.id.tv_content);
        TextView tv_dialog_update = view.findViewById(R.id.tv_dialog_update);
        TextView tv_progress = view.findViewById(R.id.tv_progress);
        RelativeLayout rl_progress = view.findViewById(R.id.rl_progress);
        ProgressBar pb = view.findViewById(R.id.pb);
        tv_content.setText(content);
        tv_dialog_update.setOnClickListener(onClickListener);
        if(necessary){
            dialog.setCancelable(false);
        }
        dialog.show();
        Map<String,Object> map = new HashMap<>();
        map.put("pb",pb);
        map.put("tv_dialog_update",tv_dialog_update);
        map.put("dialog",dialog);
        map.put("tv_progress",tv_progress);
        map.put("rl_progress",rl_progress);
        return map;
    }

}
