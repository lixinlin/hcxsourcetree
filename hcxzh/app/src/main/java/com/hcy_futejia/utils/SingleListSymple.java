package com.hcy_futejia.utils;

import com.hxlm.hcyandroid.bean.Symptom;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by KY3H on 2019/5/9.
 */

public class SingleListSymple {
    private static SingleListSymple singleListSymple;

    private List<Symptom> symptomList;
    private SingleListSymple() {
        symptomList = new ArrayList<>();
    }

    public static SingleListSymple getSingleListSymple() {
        if(singleListSymple==null){
            singleListSymple = new SingleListSymple();
        }
        return singleListSymple;
    }

    public List<Symptom> getSymptomList() {
        return symptomList;
    }
}
