package com.hcy_futejia.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hcy_futejia.bean.ScanCardDetailsBean;
import com.hhmedic.activity.CallSelectorAct;
import com.hxlm.android.hcy.bean.CardServiceBean;

import java.util.List;

/**
 * @author Lirong
 * @date 2018/12/10 0010.
 * @description 激活卡详细信息   剩余服务
 */

public class ScanCardDetailsAdapter extends RecyclerView.Adapter<ScanCardDetailsAdapter.CardDetailsViewHolder> {

    private Context context;
    private List<ScanCardDetailsBean.LmMdServiceAttrBean> list;
    //serviceCode
    private String service_code = "10006";

    public ScanCardDetailsAdapter(Context context, List<ScanCardDetailsBean.LmMdServiceAttrBean> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public CardDetailsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_scan_card_details, parent, false);
        return new CardDetailsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CardDetailsViewHolder holder, int position) {
        //获取数据
        ScanCardDetailsBean.LmMdServiceAttrBean cardServiceBean = list.get(position);
        holder.tvDetails1.setText(cardServiceBean.getServiceName());
        holder.tvDetails2.setText(cardServiceBean.getValue()+cardServiceBean.getUnit());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class CardDetailsViewHolder extends RecyclerView.ViewHolder{

        private final TextView tvDetails1;
        private final TextView tvDetails2;
        private final TextView tvDetails3;

        public CardDetailsViewHolder(View itemView) {
            super(itemView);
            tvDetails1 = itemView.findViewById(R.id.item_card_details1);
            tvDetails2 = itemView.findViewById(R.id.item_card_details2);
            tvDetails3 = itemView.findViewById(R.id.item_card_details3);
        }
    }

}
