package com.hcy_futejia.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hxlm.hcyandroid.bean.ICDItem;

import java.util.List;

//右边adapter
public class DiseaseListRightAdapter extends BaseAdapter {

    private Context context;
    public static List<ICDItem> list;
    LayoutInflater lf;

    public String deletename;

    public DiseaseListRightAdapter(Context context, List<ICDItem> list) {

        this.context = context;
        DiseaseListRightAdapter.list = list;
        lf = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        if (list.size() == 0) {
            return 0;
        }
        return list.size();

    }

    @Override
    public Object getItem(int arg0) {
        return list.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {

        return arg0;
    }

    @Override
    public View getView(int position, View view, ViewGroup arg2) {

        HondlerRight hondler = null;

        if (view == null) {
            hondler = new HondlerRight();
            view = lf.inflate(R.layout.item_disease_right_data,
                    null);
            hondler.ll = (LinearLayout) view.findViewById(R.id.ll);
            hondler.iv_right = (ImageView) view.findViewById(R.id.iv_right);
            hondler.tv = (TextView) view.findViewById(R.id.textright);
            hondler.btnItem = (ImageButton) view.findViewById(R.id.btnItemRight);
            hondler.view_line = view.findViewById(R.id.view_line);
            view.setTag(hondler);

        } else {
            hondler = (HondlerRight) view.getTag();
        }


        //hondler.iv_right.setImageResource(R.drawable.disease_icon);
        //删除图片点击事件
        hondler.btnItem.setOnClickListener(new OnDeleteClickListener(list, position));

        hondler.tv.setText(list.get(position).getName());


        return view;
    }

    public class HondlerRight {
        LinearLayout ll;
        ImageView iv_right;//右侧图标
        TextView tv;
        ImageButton btnItem;
        View view_line;
    }


    class OnDeleteClickListener implements OnClickListener {

        private List<ICDItem> list;
        private int position;

        public OnDeleteClickListener(List<ICDItem> list, int position) {
            super();
            this.list = list;
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            list.get(position).setChoosed(false);
            list.remove(position);
            DiseaseListRightAdapter.this.notifyDataSetChanged();

            //如果左侧的列表和右侧的列表都删除了，则不显示提交按钮
//            if (list.size() <= 0 && ListLeftAdapter.list.size() <= 0) {
//                ChooseSelectedDiseasesandSymptomsDialog.img_right_submit.setVisibility(View.GONE);
//            }

        }

    }

}
