package com.hcy_futejia.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hxlm.hcyandroid.bean.Task;

import java.util.List;

/**
 * Created by KY3H on 2019/4/9.
 */

public class TaskAdapter extends BaseAdapter {
    private List<Task> tasks;
    private Context mContext;

    public TaskAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if(tasks!=null){
            return tasks.size();
        }else {
            return 0;
        }

    }

    @Override
    public Object getItem(int i) {
        if(tasks!=null){
            return tasks.get(i);
        }else {
            return null;
        }

    }

    @Override
    public long getItemId(int i) {
        if(tasks!=null){
            return i;
        }else{
            return 0;
        }
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.task_item, null);
            holder = new ViewHolder();
            holder.tv_type = view.findViewById(R.id.tv_task_type);
            holder.tv_content = view.findViewById(R.id.tv_task_content);
            holder.tv_line = view.findViewById(R.id.tv_line);
            holder.iv_dot = view.findViewById(R.id.iv_dot);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        Task task = tasks.get(i);
        String type = task.getType();
        boolean complete = task.isDone();

        if (type != null && !type.isEmpty()) {
            String content = task.getAdvice();
            if (content != null && !content.isEmpty()) {
                if (type.equals("yitui")) {
                    holder.tv_line.setBackgroundColor(Color.parseColor("#66A8E9"));
                    holder.tv_type.setText(mContext.getString(R.string.home_yitui));
                    holder.tv_content.setText(content);
                } else if (type.equals("yizhan")) {
                    holder.tv_line.setBackgroundColor(Color.parseColor("#66A8E9"));
                    holder.tv_type.setText(mContext.getString(R.string.home_yizhan));
                    holder.tv_content.setText(content);
                } else if (type.equals("yiting")) {
                    holder.tv_line.setBackgroundColor(Color.parseColor("#66A8E9"));
                    holder.tv_type.setText(mContext.getString(R.string.home_yiting));
                    holder.tv_content.setText(content);
                } else if (type.equals("yidai")) {
                    holder.tv_line.setBackgroundColor(Color.parseColor("#66A8E9"));
                    holder.tv_type.setText(mContext.getString(R.string.home_yidai));
                    holder.tv_content.setText(content);
                } else if (type.equals("yishuo")) {
                    holder.tv_line.setBackgroundColor(Color.parseColor("#66A8E9"));
                    holder.tv_type.setText(mContext.getString(R.string.home_yishuo));
                    holder.tv_content.setText(content);
                } else if (type.equals("yixie")) {
                    holder.tv_line.setBackgroundColor(Color.parseColor("#F0B764"));
                    holder.tv_type.setText(mContext.getString(R.string.home_yixie));
                    holder.tv_content.setText(content);
                } else if (type.equals("yidian")) {
                    holder.tv_line.setBackgroundColor(Color.parseColor("#8992F0"));
                    holder.tv_type.setText(mContext.getString(R.string.home_yidian));
                    holder.tv_content.setText(content);
                }
            }
        }
        handle_dot(holder.iv_dot,complete);
        return view;
    }

    /**
     * 完成过的任务就不展示红点
     * @param iv_dot
     * @param complete
     */
    private void handle_dot(ImageView iv_dot, boolean complete) {
        if(complete){
            iv_dot.setVisibility(View.GONE);
        }else {
            iv_dot.setVisibility(View.VISIBLE);
        }
    }

    class ViewHolder {
        public TextView tv_type;
        public TextView tv_content;
        public TextView tv_line;
        public ImageView iv_dot;

    }

}
