package com.hcy_futejia.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hxlm.hcyandroid.bean.Symptom;

import java.util.List;

//左侧adapter
public class DiseaseListLeftAdapter extends BaseAdapter {

    private Context context;
    public static List<Symptom> list;
    LayoutInflater lf;
    private int tag ;


    public DiseaseListLeftAdapter(Context context, List<Symptom> list,int tag) {

        this.context = context;
        DiseaseListLeftAdapter.list = list;
        this.tag = tag;
        lf = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        if (list.size() == 0) {
            return 0;
        }
        return list.size();

    }

    @Override
    public Object getItem(int arg0) {
        return list.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {

        return arg0;
    }

    @Override
    public View getView(int position, View view, ViewGroup arg2) {

        Hondler hondler = null;

        if (view == null) {
            hondler = new Hondler();
            view = lf.inflate(R.layout.item_disease_left_data,
                    null);
            hondler.ll = (LinearLayout) view.findViewById(R.id.ll);
            hondler.tv = (TextView) view.findViewById(R.id.text);
            hondler.tvDegree = (TextView) view.findViewById(R.id.tvDegree);
            hondler.btnItem = (ImageButton) view.findViewById(R.id.btnItemLeft);
            hondler.view_line = view.findViewById(R.id.view_line);
            view.setTag(hondler);

        } else {
            hondler = (Hondler) view.getTag();
        }


        //删除图片点击事件
        hondler.btnItem.setOnClickListener(new OnDeleteClickListener(list, position));

        hondler.tv.setText(list.get(position).getDesc());

        if (context.getString(R.string.dialog_choose_degree_light).equals(list.get(position).getDregree())) {
            hondler.tvDegree.setTextColor(context.getResources().getColor(
                    R.color.light));
            hondler.tvDegree.setText(context.getString(R.string.dialog_choose_degree_light));

        } else if (context.getString(R.string.dialog_choose_degree_middle).equals(list.get(position).getDregree())) {
            hondler.tvDegree.setTextColor(context.getResources().getColor(
                    R.color.moderate));
            hondler.tvDegree.setText(context.getString(R.string.dialog_choose_degree_middle));

        } else if (context.getString(R.string.dialog_choose_degree_heavy).equals(list.get(position).getDregree())) {
            hondler.tvDegree.setTextColor(context.getResources().getColor(
                    R.color.severe));
            hondler.tvDegree.setText(context.getString(R.string.dialog_choose_degree_heavy));

        }


        return view;
    }

    public class Hondler {
        LinearLayout ll;
        TextView tv;
        TextView tvDegree;// 程度
        ImageButton btnItem;
        View view_line;
    }

    private TextView tv;
    private TextView txt;
    private TextView tvCircle;

    class OnDeleteClickListener implements OnClickListener {

        private List<Symptom> list;
        private int position;

        public OnDeleteClickListener(List<Symptom> list, int position) {
            super();
            this.list = list;
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            list.get(position).setChoosed(false);
            list.remove(position);
            notifyDataSetChanged();
            if (tag == 1) {
                tv.setText(context.getString(R.string.viscera_selected_symptom) + list.size() + "/5");
            }
        }

    }

    public void setView(TextView tv){
        this.tv = tv;
    }
}