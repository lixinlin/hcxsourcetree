package com.hcy_futejia.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.questionnair.Answer;
import com.hxlm.android.hcy.questionnair.Question;

import java.util.List;

/**
 * Created by KY3H on 2019/4/11.
 */

public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.QuestionHolder> {
    private Context context;
    private List<Question> list;
    private OnItemClickListener onItemClickListener;
    private int chooseAnswer = 0;
    private int currentPosition = 0;
    private String TAG ="question_adapter";
    private final List<Answer> defaultAnswers;

    public QuestionAdapter(Context context, List<Question> list,List<Answer> anAnswers) {
        this.context = context;
        this.list = list;
        this.defaultAnswers = anAnswers;
    }

    public int getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(int currentPosition) {
        this.currentPosition = currentPosition;
    }

    public List<Question> getList() {
        return list;
    }

    public void setList(List<Question> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public QuestionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.ftj_question_item, parent, false);
        QuestionHolder questionHolder = new QuestionHolder(inflate);
        return questionHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull QuestionHolder holder, int position) {
        holder.item.setTag(position);
        holder.question_item.setTag(position);
        Question question = list.get(position);
        holder.tv_num.setText(question.getNumber_ui() + "");
        holder.tv_question.setText(question.getName());
        handle_answer(holder.tv_answer, question.getAnswer_int());
        if (position == currentPosition) {
            holder.tv_question.setTextColor(Color.parseColor("#1E82D2"));//蓝色
        } else {
            holder.tv_question.setTextColor(Color.parseColor("#808080"));
        }
        if(question.isChang_bufen()&&position!=0){
            holder.tv_bufen.setVisibility(View.VISIBLE);
            switch (question.getClassifyId()) {
                case 0:
                    holder.tv_bufen.setText(context.getString(R.string.ftj_question_a_diyibufen));
                    break;
                case 1:
                    holder.tv_bufen.setText(context.getString(R.string.ftj_question_a_dierbufen));
                    break;
                case 2:
                    holder.tv_bufen.setText(context.getString(R.string.ftj_question_a_disanbufen));
                    break;
                case 3:
                    holder.tv_bufen.setText(context.getString(R.string.ftj_question_a_disibufen));
                    break;
            }
        }else {
            holder.tv_bufen.setVisibility(View.GONE);
        }
        holder.question_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(position, holder.question_item);
                }
            }
        });
    }

    private void handle_answer(TextView tv_answer, int answer_int) {

        switch (answer_int) {
            case 1:
                tv_answer.setText(R.string.answer1);
                tv_answer.setBackgroundResource(R.drawable.bg_blue_round_rect);
                break;
            case 2:
                tv_answer.setText(R.string.answer2);
                tv_answer.setBackgroundResource(R.drawable.bg_blue_round_rect);
                break;
            case 3:
                tv_answer.setText(R.string.answer3);
                tv_answer.setBackgroundResource(R.drawable.bg_blue_round_rect);
                break;
            case 4:
                tv_answer.setText(R.string.answer4);
                tv_answer.setBackgroundResource(R.drawable.bg_blue_round_rect);
                break;
            case 5:
                tv_answer.setText(R.string.answer5);
                tv_answer.setBackgroundResource(R.drawable.bg_blue_round_rect);

                break;
            case 0:
//                把未处理的item状态刷新回正常值
                tv_answer.setText(" ");
                tv_answer.setBackgroundColor(Color.parseColor("#00000000"));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class QuestionHolder extends RecyclerView.ViewHolder {


        private final TextView tv_num;
        private final TextView tv_question;
        private final TextView tv_answer;
        private final TextView tv_bufen;
        private final LinearLayout question_item;
        private final LinearLayout item;

        public QuestionHolder(View itemView) {
            super(itemView);
            tv_num = itemView.findViewById(R.id.tv_num);
            tv_question = itemView.findViewById(R.id.tv_content);
            tv_answer = itemView.findViewById(R.id.tv_daan);
            question_item = itemView.findViewById(R.id.question_item);
            tv_bufen = itemView.findViewById(R.id.tv_bufen);
            item = itemView.findViewById(R.id.item);
        }

        public void autoClick(int chosen) {
            handle_chosen(chosen);
//            click是异步，要放在其他改变状态的操作之后
            question_item.performClick();

        }

    }

    private void handle_chosen(int chosen) {
        this.chooseAnswer = chosen;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(int position, View itemLayout);
    }
}
