package com.hcy_futejia.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hxlm.android.hcy.content.ResourceItem;

import java.util.List;

/**
 * 乐药的列表的基本功能
 * <p/>
 * Created by Zhenyu on 2015/8/14.
 */
public class FtjYueYaoListAdapter extends BaseAdapter {
    private final LayoutInflater mInflater;
    private List<ResourceItem> mDatas;
    private int typeIndex;

    public FtjYueYaoListAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
    }

    public void setDatas(List<ResourceItem> datas, int index) {
        this.mDatas = datas;
        this.typeIndex = index;
    }

    @Override
    public int getCount() {
        if (mDatas == null) {
            return 0;
        } else {
            return mDatas.size();
        }
    }

    @Override
    public ResourceItem getItem(int position) {
        if (mDatas == null || mDatas.size() < position) {
            return null;
        }
        return mDatas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mDatas.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_ftj_yueyao_list, parent, false);
            holder = new ViewHolder();
            holder.ivType = (ImageView) convertView.findViewById(R.id.iv_type);
            holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_yueyao_title);
            holder.ivStatus = (ImageView) convertView.findViewById(R.id.iv_status);
            holder.progressbar = (ProgressBar) convertView.findViewById(R.id.progressbar);
            holder.layout = (LinearLayout) convertView.findViewById(R.id.progress_bar_layout);
            holder.tvPrice = convertView.findViewById(R.id.tv_yueyao_price);
            holder.tvAdd = convertView.findViewById(R.id.tv_add_cart);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ResourceItem item = getItem(position);
        if (item == null) {
            return convertView;
        }

        holder.tvTitle.setText(item.getName());
        holder.tvPrice.setText("￥"+item.getPrice());

        switch (typeIndex) {
            case 0:
                holder.ivType.setImageResource(R.drawable.ftj_gong);
                break;
            case 1:
                holder.ivType.setImageResource(R.drawable.ftj_shang);
                break;
            case 2:
                holder.ivType.setImageResource(R.drawable.ftj_jue);
                break;
            case 3:
                holder.ivType.setImageResource(R.drawable.ftj_zhi);
                break;
            case 4:
                holder.ivType.setImageResource(R.drawable.ftj_yu);
                break;
            default:
                break;
        }

        holder.layout.setVisibility(View.GONE);
        holder.ivStatus.setVisibility(View.VISIBLE);
        holder.tvAdd.setVisibility(View.GONE);

        switch (item.getItemStatus()) {
            case ResourceItem.NOT_PAY:
            case ResourceItem.NOT_ORDERED:
                holder.ivStatus.setVisibility(View.GONE);
                holder.tvAdd.setVisibility(View.VISIBLE);
                break;
            case ResourceItem.NOT_DOWNLOAD:
                holder.ivStatus.setImageResource(R.drawable.ftj_yueyao_download);
                break;

            case ResourceItem.DOWNLOADING:
                holder.ivStatus.setVisibility(View.GONE);
                holder.layout.setVisibility(View.VISIBLE);
                holder.progressbar.setMax(item.getTotalSize());
                holder.progressbar.setProgress(item.getDownloadedSize());
                break;

            case ResourceItem.STOP:
                holder.ivStatus.setImageResource(R.drawable.ftj_yueyao_play);
                break;

            case ResourceItem.PLAYING:
                holder.ivStatus.setImageResource(R.drawable.ftj_yueyao_pause);
                break;

            default:
                break;
        }

        return convertView;
    }

    private class ViewHolder {
         ImageView ivType;
         TextView tvTitle;
         ImageView ivStatus;
         TextView tvAdd;
        TextView tvPrice;
        public ProgressBar progressbar;
        public LinearLayout layout;
    }
}