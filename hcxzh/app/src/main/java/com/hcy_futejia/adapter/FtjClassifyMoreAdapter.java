package com.hcy_futejia.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hcy.ky3h.R;
import com.hxlm.hcyandroid.bean.Data3;
import com.hxlm.hcyandroid.bean.Symptom;

import java.util.HashMap;
import java.util.List;

public class FtjClassifyMoreAdapter extends BaseAdapter {

    private Context context;
    public static String[] text_list;
    private int position = 0;
    // 用来控制CheckBox的选中状况
    private static HashMap<Integer, Boolean> isSelected;
    // 用来导入布局
    private LayoutInflater inflater = null;
    private List<Symptom> symptomsToShow;
    public static List<Data3> listMoreData;

    public FtjClassifyMoreAdapter() {


    }

    public FtjClassifyMoreAdapter(Context context, List<Symptom> symptomsToShow) {
        this.context = context;
        this.symptomsToShow = symptomsToShow;
        listMoreData = listMoreData;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return symptomsToShow.size();
    }

    @Override
    public Object getItem(int position) {
        return symptomsToShow.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder hold = null;
        if (view == null) {
            hold = new ViewHolder();

            view = inflater.inflate(R.layout.item_ftj_classify_morelist, null);
            hold.txt = (TextView) view.findViewById(R.id.moreitem_txt);
            hold.tv_circle = (TextView) view.findViewById(R.id.tv_circle);
            view.setTag(hold);
        } else {
            hold = (ViewHolder) view.getTag();
        }

        Symptom symptom = symptomsToShow.get(position);

        hold.txt.setText(symptom.getDesc());

        if (symptom.isChoosed()) {
            hold.tv_circle.setVisibility(View.VISIBLE);
            hold.txt.setTextColor(Color.parseColor("#ffa200"));
        } else {
            hold.tv_circle.setVisibility(View.GONE);
            hold.txt.setTextColor(Color.parseColor("#8e8e93"));
        }


        return view;
    }

    public void setSelectItem(int position) {
        this.position = position;
    }

    public static HashMap<Integer, Boolean> getIsSelected() {
        return isSelected;
    }

    public static void setIsSelected(HashMap<Integer, Boolean> isSelected) {
        FtjClassifyMoreAdapter.isSelected = isSelected;

    }

    public static class ViewHolder {
        TextView txt;
        TextView tv_circle;
    }


}
