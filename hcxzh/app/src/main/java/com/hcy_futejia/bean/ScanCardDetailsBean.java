package com.hcy_futejia.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @author Lirong
 * @date 2019/5/6.
 * @description
 */

public class ScanCardDetailsBean implements Serializable {

    /**
     * code : MQKY00LBTMW702ERSVD
     * name : service01
     * description :
     * lmMdServiceAttr : [{"id":null,"serviceName":"海外医疗","serviceCode":null,"unit":"次数","value":"1","memo":null,"serviceId":null},{"id":null,"serviceName":"特殊检查加急","serviceCode":null,"unit":"次数","value":"3","memo":null,"serviceId":null},{"id":null,"serviceName":"第二诊疗意见","serviceCode":null,"unit":"次数","value":"3","memo":null,"serviceId":null},{"id":null,"serviceName":"面对面咨询","serviceCode":null,"unit":"次数","value":"3","memo":null,"serviceId":null},{"id":null,"serviceName":"视频问诊","serviceCode":null,"unit":"时长","value":"101","memo":null,"serviceId":null},{"id":null,"serviceName":"手术住院安排","serviceCode":null,"unit":"次数","value":"3","memo":null,"serviceId":null}]
     */

    private String code;
    private String name;
    private String description;
    private List<LmMdServiceAttrBean> lmMdServiceAttr;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<LmMdServiceAttrBean> getLmMdServiceAttr() {
        return lmMdServiceAttr;
    }

    public void setLmMdServiceAttr(List<LmMdServiceAttrBean> lmMdServiceAttr) {
        this.lmMdServiceAttr = lmMdServiceAttr;
    }

    public static class LmMdServiceAttrBean implements Serializable{
        /**
         * id : null
         * serviceName : 海外医疗
         * serviceCode : null
         * unit : 次数
         * value : 1
         * memo : null
         * serviceId : null
         */

        private Object id;
        private String serviceName;
        private Object serviceCode;
        private String unit;
        private String value;
        private Object memo;
        private Object serviceId;

        public Object getId() {
            return id;
        }

        public void setId(Object id) {
            this.id = id;
        }

        public String getServiceName() {
            return serviceName;
        }

        public void setServiceName(String serviceName) {
            this.serviceName = serviceName;
        }

        public Object getServiceCode() {
            return serviceCode;
        }

        public void setServiceCode(Object serviceCode) {
            this.serviceCode = serviceCode;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public Object getMemo() {
            return memo;
        }

        public void setMemo(Object memo) {
            this.memo = memo;
        }

        public Object getServiceId() {
            return serviceId;
        }

        public void setServiceId(Object serviceId) {
            this.serviceId = serviceId;
        }

        @Override
        public String toString() {
            return "LmMdServiceAttrBean{" +
                    "id=" + id +
                    ", serviceName='" + serviceName + '\'' +
                    ", serviceCode=" + serviceCode +
                    ", unit='" + unit + '\'' +
                    ", value='" + value + '\'' +
                    ", memo=" + memo +
                    ", serviceId=" + serviceId +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "ScanCardDetailsBean{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", lmMdServiceAttr=" + lmMdServiceAttr +
                '}';
    }
}
