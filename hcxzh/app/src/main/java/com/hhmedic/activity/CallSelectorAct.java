package com.hhmedic.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bigman.wmzx.customcardview.library.CardView;
import com.hcy.ky3h.R;
import com.hcy_futejia.activity.BandingPhoneNumberActivity;
import com.hcy_futejia.activity.FtjTuWenActivity;
import com.hhmedic.android.sdk.HHDoctor;
import com.hhmedic.android.sdk.listener.HHCallListener;
import com.hhmedic.android.sdk.listener.HHLoginListener;
import com.hhmedic.application.HHDemoUtils;
import com.hxlm.android.hcy.user.LoginControllor;
import com.hxlm.android.hcy.user.Member;
import com.hxlm.android.hcy.view.TitleBarView;
import com.hxlm.hcyandroid.Constant;
import com.hxlm.hcyandroid.util.IsMobile;
import com.hxlm.hcyphone.MainActivity;
import com.yanzhenjie.permission.AndPermission;

public class CallSelectorAct extends BaseActivity implements View.OnClickListener{

    private boolean noticeTTS;
    private final String userToken = "ECEEDCCD74B7D54BCF6690B7E26262B73F0D04F68EA2608F6783B874E4F50EEF";
    private Uri imageUri ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected int contentViewId() {
//        return R.layout.act_call_selector;
        return R.layout.activity_online_consulting;
    }

    @Override
    protected void initUI() {
        super.initUI();
        TitleBarView titleBar = new TitleBarView();
        titleBar.init(this, getString(R.string.yh_online_resule_text), titleBar, 1);
        findViewById(R.id.ll_online_chengren).setOnClickListener(this);
        findViewById(R.id.ll_online_child).setOnClickListener(this);
        findViewById(R.id.ll_online_picture).setOnClickListener(this);
        CardView card_chengren = findViewById(R.id.card_chengren);
        CardView card_child = findViewById(R.id.card_child);
        if (Constant.isEnglish){
            card_chengren.setVisibility(View.GONE);
            card_child.setVisibility(View.GONE);
        }else {
            card_chengren.setVisibility(View.VISIBLE);
            card_child.setVisibility(View.VISIBLE);
        }
//        findViewById(R.id.all_btn).setOnClickListener(this);
//        findViewById(R.id.child_btn).setOnClickListener(this);
//        findViewById(R.id.back_btn).setOnClickListener(this);
//        findViewById(R.id.view_list).setOnClickListener(this);
//        findViewById(R.id.view_detail).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_online_chengren:
                Member loginMember = LoginControllor.getLoginMember();
                if(loginMember!=null){
                    String uuid1 = loginMember.getUuid();
                    Log.e("tag","==uuid=="+uuid1);
                    String phone = loginMember.getUsername();
                    boolean mobileNO = IsMobile.isMobileNO(phone);
                    if(mobileNO){
                        //绑定了手机号
                        if (!TextUtils.isEmpty(uuid1)) {
                            getPermission(2);
                        }else {
                            setDialog();
                        }
                    }else {
                        //提示绑定手机号
                        setDialog_binding_phone();
                    }

                }
                //判断Android版本是否是Android7.0以上
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                break;
            case R.id.ll_online_child:
                Member loginMember1 = LoginControllor.getLoginMember();
                if(loginMember1!=null){
                    String uuid1 = loginMember1.getUuid();
                    Log.e("tag","==uuid=="+uuid1);
                    String phone = loginMember1.getUsername();
                    boolean mobileNO = IsMobile.isMobileNO(phone);
                    if(mobileNO){
                        //绑定了手机号
                        if (!TextUtils.isEmpty(uuid1)) {
                            getPermission(1);
                        }else {
                            setDialog();
                        }
                    }else {
                        //提示绑定手机号
                        setDialog_binding_phone();
                    }
                }
                break;
            case R.id.ll_online_picture:
                Intent intent = new Intent(CallSelectorAct.this,
                        FtjTuWenActivity.class);
                SharedPreferences.Editor editor = getSharedPreferences("pub", Context.MODE_PRIVATE).edit();
                editor.putBoolean("pubsort", true);
                editor.commit();
                startActivity(intent);
                break;
//            case R.id.all_btn:
//                callAdult();
//                break;
//            case  R.id.child_btn:
//                callChild();
//                break;
//            case R.id.view_list:
//                viewList();
//                break;
//            case R.id.view_detail:
//                viewDetail();
//                break;
                default:
                    break;
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        HHDemoUtils.notice(this,getString(R.string.hp_notice_depart));
    }

    @Override
    public void onResume() {
        super.onResume();



        if (!noticeTTS) {
            HHDemoUtils.notice(this,getString(R.string.hp_notice_depart));
        }

        noticeTTS = true;

    }

    private void getPermission(int type){
        AndPermission.with(this).permission(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.CALL_PHONE,
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO
        )
                .onGranted(permissions -> doLogin(type))

                .onDenied(permissions -> {


                    if (AndPermission.hasAlwaysDeniedPermission(CallSelectorAct.this, permissions)) {
                        // 这些权限被用户总是拒绝。
//                        alwaysTips(permissionTips());
                    }
                    else
                    {

                    }

                })
                .start();
    }

    private Dialog dialog;
    private Dialog dialog_bind_phone;

    private void doLogin(int type) {

        //这个ID是和和缓对接之后得到的和缓的UUID
        long uuid = 0;
        Member loginMember = LoginControllor.getLoginMember();
        if(loginMember!=null){
            String uuid1 = loginMember.getUuid();
            if(!TextUtils.isEmpty(uuid1)){
                uuid = Long.parseLong(loginMember.getUuid());
            }else {
                uuid = 0;
            }

           if(uuid != 0){
               HHDoctor.login(this,uuid, new HHLoginListener() {
                   @Override
                   public void onSuccess() {
                       if (type == 1){
                           callChild();
                       }else if (type == 2) {
                           callAdult();
                       }
                   }

                   @Override
                   public void onError(String s) {
                       Log.e("retrofit","===一呼Error===="+s.toString());
                       setDialog();
                   }
               });
           }else {
               Toast.makeText(this, "无此权限", Toast.LENGTH_SHORT).show();
           }
        }

    }

    /**
     * 设置提示弹窗
     */
    public void setDialog(){
        dialog = new Dialog(CallSelectorAct.this,R.style.dialog);
        View view = LayoutInflater.from(CallSelectorAct.this).inflate(R.layout.dialog_activate, null);
        dialog.setContentView(view);
        //初始化控件
        Button btn_dialog_activate_no = view.findViewById(R.id.btn_dialog_activate_no);
        Button btn_dialog_activate_yes = view.findViewById(R.id.btn_dialog_activate_yes);
        TextView tv_dialog_text = view.findViewById(R.id.tv_dialog_text);

        tv_dialog_text.setText("您还没有购买视频咨询服务，是否前去购买？");

        btn_dialog_activate_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btn_dialog_activate_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //跳转至商城
                Intent intent = new Intent(CallSelectorAct.this, MainActivity.class);
                intent.putExtra("mainId",3);
                startActivity(intent);
            }
        });

        dialog.show();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
    }

    /**
     * 设置提示弹窗
     */
    public void setDialog_binding_phone(){
        dialog_bind_phone = new Dialog(CallSelectorAct.this,R.style.dialog);
        View view = LayoutInflater.from(CallSelectorAct.this).inflate(R.layout.dialog_activate, null);
        dialog_bind_phone.setContentView(view);
        //初始化控件
        Button btn_dialog_activate_no = view.findViewById(R.id.btn_dialog_activate_no);
        Button btn_dialog_activate_yes = view.findViewById(R.id.btn_dialog_activate_yes);
        TextView tv_dialog_text = view.findViewById(R.id.tv_dialog_text);

        tv_dialog_text.setText(R.string.ftj_callSelectorAct_glsj);

        btn_dialog_activate_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_bind_phone.dismiss();
            }
        });

        btn_dialog_activate_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //跳转至绑手机号页面
                Intent intent = new Intent(CallSelectorAct.this, BandingPhoneNumberActivity.class);
                startActivity(intent);
                dialog_bind_phone.dismiss();
            }
        });

        dialog_bind_phone.show();
        dialog_bind_phone.setCanceledOnTouchOutside(false);
        dialog_bind_phone.setCancelable(true);
    }

    /**
     * 呼叫成人医生
     */
    private void callAdult() {

        HHDoctor.callForAdult(this, new HHCallListener() {

            @Override
            public void onStart() {
                Log.i(TAG,"call onStart");
            }

            @Override
            public void onCalling() {
                Log.i(TAG,"call onCalling");
            }

            @Override
            public void onInTheCall() {
                Log.i(TAG,"call onInTheCall");
            }

            @Override
            public void onFinish() {
                Log.i(TAG,"call onFinish");
            }

            @Override
            public void onCallSuccess() {
                Log.i(TAG,"call onCallSuccess");
            }

            @Override
            public void onFail(int i) {
                Log.i(TAG,"call onFail");
            }

            @Override
            public void onCancel() {
                Log.i(TAG,"call onCancel");
            }

            @Override
            public void onLineUpTimeout() {
                Log.i(TAG,"call onLineUpTimeout");
            }

            @Override
            public void onLineUp() {
                Log.i(TAG,"call onLineUp");
            }
        });
    }

    /**
     * 呼叫儿童医生
     */
    private void callChild() {
        HHDoctor.callForChild(this, new HHCallListener() {

            @Override
            public void onStart() {
                Log.i(TAG,"call onStart");
            }

            @Override
            public void onCalling() {
                Log.i(TAG,"call onCalling");
            }

            @Override
            public void onInTheCall() {
                Log.i(TAG,"call onInTheCall");
            }

            @Override
            public void onFinish() {
                Log.i(TAG,"call onFinish");
            }

            @Override
            public void onCallSuccess() {
                Log.i(TAG,"call onCallSuccess");
            }

            @Override
            public void onFail(int i) {
                Log.i(TAG,"call onFail");
            }

            @Override
            public void onCancel() {
                Log.i(TAG,"call onCancel");
            }

            @Override
            public void onLineUpTimeout() {
                Log.i(TAG,"call onLineUpTimeout");
            }

            @Override
            public void onLineUp() {
                Log.i(TAG,"call onLineUp");
            }
        });
    }

    /**
     * 查看病历存档列表
     */
    private void viewList() {
        Intent intent = new Intent(this, ViewDetailAct.class);
        String url = HHDoctor.getMedicListUrl(this, userToken);
        intent.putExtra("url", url);
        intent.putExtra("title", "病历存档列表");
        startActivity(intent);
    }

    /**
     * 查看病历存档详情
     */
    private void viewDetail() {
        Intent intent = new Intent(this, ViewDetailAct.class);
        String url = HHDoctor.getMedicDetailUrl(this, userToken, "1541041785333");
        intent.putExtra("url", url);
        intent.putExtra("title", "病历存档详情");
        startActivity(intent);
    }
}
