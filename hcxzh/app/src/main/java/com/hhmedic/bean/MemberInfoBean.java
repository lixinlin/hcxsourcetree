package com.hhmedic.bean;

import java.util.List;

/**
 * @author Lirong
 * @date 2018/12/20.
 * @description
 */

public class MemberInfoBean {
    private String uuid;
    private String token;

    public String getUid() {
        return uuid;
    }

    public void setUuid(String uid) {
        this.uuid = uid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
