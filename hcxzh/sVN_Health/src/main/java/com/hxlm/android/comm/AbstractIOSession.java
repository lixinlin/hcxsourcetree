package com.hxlm.android.comm;

import android.os.Handler;

import com.hxlm.android.utils.ByteUtil;
import com.hxlm.android.utils.ConstantHealth;
import com.hxlm.android.utils.Logger;

import java.io.IOException;
import java.util.Collection;

/**
 * IO通讯的抽象类
 * <p/>
 * Created by Zhenyu on 2015/11/15.
 */
public abstract class AbstractIOSession {
    //用于日志输出表示是那个一个实现
    //protected final String tag = this.getClass().getSimpleName();
    protected final String tag = "UsbSerialIOSession";

    protected final AbstractDeviceActivity activity;
    protected final Handler handler;
    public Status status = Status.NONE;
    protected AbstractCodec codec;

    public AbstractIOSession(final AbstractDeviceActivity anActivity,
                             final AbstractCodec abstractCodec) {
        this.activity = anActivity;
        this.codec = abstractCodec;
        this.handler = anActivity.getHandler();
    }

    public synchronized void connect() {
        if (status == Status.NONE) {
            this.status = Status.CONNECTING;

            Logger.i(tag, "开始发起连接，使用的协议类为 ： " + codec.getClass().getSimpleName());

            doConnect();
        }
    }

    public synchronized void sendMessage(AbstractMessage message) {
        if (status == Status.CONNECTED) {
            Logger.d("TemperatureDetectionActivity", "发送新消息：" + message.toString());

            byte[] packet = codec.getPacket(message);

            Logger.d("TemperatureDetectionActivity", "消息协议格式为 = " + ByteUtil.bytesToHexString(packet));


            doSendMessage(packet);

            Logger.d("TemperatureDetectionActivity", "消息发送完成");
        } else {
            Logger.i(tag, "设备未连接，无法发送消息，请先连接设备！");
        }
    }

    protected void onConnected() {
        status = Status.CONNECTED;

        Logger.i(tag, "设备连接成功。");

        handler.sendEmptyMessage(AbstractDeviceActivity.MESSAGE_CONNECTED);
    }

    protected void onConnectFailed(Error error) {
        doClose();

        status = Status.NONE;

        Logger.e(tag, "与设备连接失败！原因为 ：" + error.getDesc());
        if(ConstantHealth.isEnglish){
            Error_English error_english = error2errorEnglish(error);
            handler.obtainMessage(AbstractDeviceActivity.MESSAGE_CONNECT_FAILED, error_english).sendToTarget();
        }else {
            handler.obtainMessage(AbstractDeviceActivity.MESSAGE_CONNECT_FAILED, error).sendToTarget();
        }
    }

    protected  Error_English error2errorEnglish(Error error){
        if( error.getDesc().equalsIgnoreCase("连接驱动未找到，无法支持该种连接方式")){
            return Error_English.DRIVER_NOT_FOUND;
        }else if( error.getDesc().equalsIgnoreCase("系统适配器启动失败")){
            return Error_English.ADAPTER_INIT_FAILED;
        }else if( error.getDesc().equalsIgnoreCase("连接驱动启动失败")){
            return Error_English.DRIVER_INIT_FAILED;
        }else if( error.getDesc().equalsIgnoreCase("请求连接的设备未找到，请确认设备是否开机")){
            return Error_English.DEVICE_NOT_FOUND;
        }else if( error.getDesc().equalsIgnoreCase("连接该设备需要获得系统授权")){
            return Error_English.NEED_PERMISSION;
        }else if( error.getDesc().equalsIgnoreCase("请求的服务类型该设备无法支持")){
            return Error_English.SERVICE_NOT_SUPPORT;
        }else if( error.getDesc().equalsIgnoreCase("设备需要在系统中先完成配对才能进行连接")){
            return Error_English.AUTO_PAIRING_NOT_SUPPORT;
        }else if( error.getDesc().equalsIgnoreCase("启动与设备的配对失败")){
            return Error_English.PAIRING_INIT_FAILED;
        }else if( error.getDesc().equalsIgnoreCase("自动设置设备配对的PIN码失败")){
            return Error_English.PAIRING_SET_PIN_FAILED;
        }else if( error.getDesc().equalsIgnoreCase("连接中出现通信错误")){
            return Error_English.CONNECTING_IO_EXCEPTION;
        }else if( error.getDesc().equalsIgnoreCase("创建数据输入输出通信接口失败")){
            return Error_English.IO_INIT_FAILED;
        }else if( error.getDesc().equalsIgnoreCase("用户拒绝授予系统权限，无法打开设备连接端口")){
            return Error_English.PERMISSION_DENY;
        }else {
            return Error_English.EMPTY;
        }

    }


    public synchronized void close() {
        doClose();

        status = Status.NONE;

        Logger.i(tag, "断开与设备连接。");

        handler.sendEmptyMessage(AbstractDeviceActivity.MESSAGE_DISCONNECTED);
    }

    /**
     * 统一处理连接中出现的异常
     *
     * @param e 抛出的异常对象
     */
    public synchronized void exceptionCaught(Throwable e) {
        Logger.e(tag, e);

        handler.obtainMessage(AbstractDeviceActivity.MESSAGE_EXCEPTION, e).sendToTarget();

        close();
    }

    /**
     * 解析输入的字节数组，如果最后一条消息不完整，则将剩余的字节进行保存。 本方法调用时将上一次剩余的字节数组与新输入的字节数组拼接成一个新的
     * 数组进行处理。 此方法有个基本使用前提，数据包的基本结构需要为： 包头 + 包体 = 【包体长度 + 命令字节 + 传输内容 + 校验和】
     *
     * @param data       输入的字节数组
     * @param dataLength 新数据在缓冲字节数组中的长度
     */
    public void parseData(final byte[] data, final int dataLength) {
        Logger.i(tag, "读取到数据！Data = " + ByteUtil.bytesToHexString(data, 0, dataLength));

        Collection<AbstractMessage> messages = null;
        try {
            messages = codec.getMessages(data, dataLength);
        } catch (InterruptedException e) {
            Logger.e(tag, e);
        }

        if (messages != null && messages.size() > 0) {
            for (AbstractMessage message : messages) {
                handler.obtainMessage(AbstractDeviceActivity.MESSAGE_RECEIVED, message).sendToTarget();

                Logger.i("TemperatureDetectionActivity", "获得新消息：" + message.toString());
            }
        }
    }

    /**
     * 发起连接请求，实现类应该具体实现在不同连接下的发送方法，连接的状态控制统一在抽象类中实现，
     * 保证连接的处理归一化。
     */
    protected abstract void doConnect();

    /**
     * 发送消息，实现类应该具体实现在不同连接下的发送方法
     *
     * @param packet 需要发送的数据包字节数组
     */
    protected abstract void doSendMessage(byte[] packet);

    /**
     * 关闭连接方法，实现类应该在此方法中回收各种资源。
     */
    protected abstract void doClose();

    /**
     * 读取线程从IOSession中获得数据的接口，需要单独启动读取线程的时候使用。使用这个接口，读取线程实现统一处理
     *
     * @param buffer 装载输出的字节数组
     * @return int 实际读出的字节数
     */
    public abstract int read(byte[] buffer) throws IOException;

    public enum Status {
        NONE, CONNECTING, CONNECTED
    }
}
