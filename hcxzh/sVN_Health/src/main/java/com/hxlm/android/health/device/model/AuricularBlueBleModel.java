package com.hxlm.android.health.device.model;

import com.hxlm.android.comm.AbstractDeviceActivity;
import com.hxlm.android.comm.AbstractIOSession;
import com.hxlm.android.comm.AbstractModel;
import com.hxlm.android.comm.iosession.BluetoothBleIOSession;
import com.hxlm.android.health.device.codec.BloodPressureCodec;

import java.io.IOException;
import java.util.UUID;

/**
 * Created by l on 2016/11/2.
 * 耳针仪Model 蓝牙4.0
 */
public class AuricularBlueBleModel extends AbstractModel{

    public AuricularBlueBleModel() {
        super("耳针仪", new AbstractModel.FunctionType[]{AbstractModel.FunctionType.AURICULAR_BULE});
    }

    @Override
    public AbstractIOSession getIOSession(AbstractDeviceActivity activity) {
        BluetoothBleIOSession ioSession;
        try {
            ioSession = new BluetoothBleIOSession(activity, new BloodPressureCodec());
        } catch (IOException e) {
            return null;
        }

        ioSession.setBtName("yueluoyi");
        ioSession.setUuidService(UUID.fromString("000018f0-0000-1000-8000-00805f9b34fb"));//服务id
        ioSession.setUuidClientCharacterConfig(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));//
        ioSession.setUuidCharacterReceive(UUID.fromString("00002af0-0000-1000-8000-00805f9b34fb"));//读
        ioSession.setUuidCharaterSend(UUID.fromString("00002af1-0000-1000-8000-00805f9b34fb"));//写

        return ioSession;
    }
}
