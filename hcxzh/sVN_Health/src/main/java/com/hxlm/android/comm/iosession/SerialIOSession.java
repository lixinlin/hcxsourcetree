package com.hxlm.android.comm.iosession;

import com.hxlm.android.comm.AbstractCodec;
import com.hxlm.android.comm.AbstractDeviceActivity;
import com.hxlm.android.comm.AbstractIOSession;
import com.hxlm.android.comm.Error;
import com.hxlm.android.utils.Logger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android_serialport_api.SerialPort;

/**
 * 串口通信实现类
 * <p/>
 * Created by Zhenyu on 2015/12/20.
 */

public class SerialIOSession extends AbstractIOSession {
    private final File device;
    private ExecutorService readExecutor;

    private int baudrate;
    private int flags;

    private SerialPort serialPort;
    private InputStream inputStream;
    private OutputStream outputStream;

    public SerialIOSession(final AbstractDeviceActivity anActivity,
                           final AbstractCodec abstractCodec, final String filePath) throws IOException {
        super(anActivity, abstractCodec);
        device = new File(filePath);

        if (!device.exists()) {
            throw new IOException("---申请连接的串口不存在！!");
        }
    }

    @Override
    protected void doConnect() {
        Logger.i(tag, "---准备打开串口。");
        try {
            serialPort = new SerialPort(device, baudrate, flags);
        } catch (SecurityException e) {
            onConnectFailed(Error.NEED_PERMISSION);
            return;
        } catch (IOException e) {
            onConnectFailed(Error.DRIVER_INIT_FAILED);
            return;
        }

        Logger.i(tag, "---串口打开成功。");

        inputStream = serialPort.getInputStream();
        outputStream = serialPort.getOutputStream();

        onConnected();

        readExecutor = Executors.newSingleThreadExecutor();
        readExecutor.execute(new ReadRunnable(this));
    }

    @Override
    protected void doClose() {
       try {
            if (inputStream != null) {
                inputStream.close();
            }
            if (outputStream != null) {
                outputStream.close();
            }
        } catch (IOException e) {
            Logger.e(tag, e);
        }

        if (serialPort != null) {
            serialPort.close();
        }

        if (readExecutor != null) {
            readExecutor.shutdownNow();
        }
    }

    @Override
    public int read(byte[] buffer) throws IOException {
        return inputStream.read(buffer);
    }

    @Override
    protected void doSendMessage(byte[] packet) {
        try {
            outputStream.write(packet);
        } catch (IOException e) {
            Logger.e(tag, e);

            close();
        }
    }

    public void setBaudrate(int baudrate) {
        this.baudrate = baudrate;
    }

    public void setFlags(int flags) {
        this.flags = flags;
    }
}
