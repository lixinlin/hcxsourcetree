package com.hxlm.android.health.device.model;

import com.hxlm.android.comm.AbstractDeviceActivity;
import com.hxlm.android.comm.AbstractIOSession;
import com.hxlm.android.comm.AbstractModel;
import com.hxlm.android.comm.iosession.UsbSerialIOSession;
import com.hxlm.android.health.device.codec.BodyTemperGunCodec;
import com.hxlm.android.health.device.codec.RongtaiBoardCodec;

import java.io.IOException;

/**
 * Created by l on 2016/12/26.
 * 体温枪的Model
 */
public class BodyTemperGunModel extends AbstractModel {
    public BodyTemperGunModel() {
        super("体温枪", new FunctionType[]{FunctionType.BODYTEMPERATUREGU});
    }
    @Override
    public AbstractIOSession getIOSession(AbstractDeviceActivity activity) {
        UsbSerialIOSession ioSession;
        try {
            ioSession = new UsbSerialIOSession(activity, new BodyTemperGunCodec(),
                    UsbSerialIOSession.DriverType.COMMON_DRIVER_CP2102, 4292);
        } catch (IOException e) {
            return null;
        }

        ioSession.setBaudrate(9600);
        ioSession.setStopBits((byte) 1);
        ioSession.setDataBits((byte) 8);
        ioSession.setParityBits((byte) 0);
        ioSession.setFlowControl((byte) 0);

        return ioSession;
    }
}
