package com.hxlm.android.health.device.codec;

import android.util.Log;
import com.hxlm.android.comm.AbstractCodec;
import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;
import com.hxlm.android.health.device.message.bodytempergun.BodyTemperGunCommand;
import com.hxlm.android.health.device.message.bodytempergun.BodyTemperGunMessage;
import com.hxlm.android.utils.ByteUtil;
import com.hxlm.android.utils.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by l on 2016/12/26.
 * 体温枪的Code
 */
public class BodyTemperGunCodec extends AbstractCodec {

    private static final boolean IsTrue = true;
    public BodyTemperGunCodec() {
        super(new byte[]{(byte)0x02,(byte)0x3B},new byte[]{(byte) 0x00}, 9, 10);
    }

    @Override
    protected int getBodyLength(int bodyStartIndex) {
        return 9;
    }

    //接收设备返回信息
    @Override
    protected AbstractMessage decodeMessage(int bodyStartIndex) throws InterruptedException {
        BodyTemperGunMessage message=new BodyTemperGunMessage();
        message.setBodyTemperNum1(dataBuffer[bodyStartIndex]);
        message.setBodyTemperNum2(dataBuffer[bodyStartIndex+1]&0x000000FF);
        message.setBodyTemperNum3(dataBuffer[bodyStartIndex+2]&0x000000FF);
        message.setBodyTemperNum4(dataBuffer[bodyStartIndex+3]&0x000000FF);

        Logger.i(tag,"dataBuffer[bodyStartIndex]-->"
                +dataBuffer[bodyStartIndex]+
                "\ndataBuffer[bodyStartIndex+1]-->"
                +dataBuffer[bodyStartIndex+1]+
                "\ndataBuffer[bodyStartIndex+2]-->"
                +dataBuffer[bodyStartIndex+2]+
                "\ndataBuffer[bodyStartIndex+3]-->"
                +dataBuffer[bodyStartIndex+3]);
        return message;
    }

    // 向设备发送数据
    @Override
    protected byte[] encodeMessage(AbstractMessage message) {
        byte[] packBody = new byte[5];
        // 数据包内容
        switch ((HealthDeviceMessageType) message.getMessageType()) {
            case BODYTEMPERGUN_COMMAND:
                BodyTemperGunCommand temperCommand= (BodyTemperGunCommand) message;
                switch (temperCommand.getCommandType())
                {
                    //体温测量
                    case BODY_TEMPER_MEASURE:
                        packBody[0]=(byte)0x05;
                        packBody[1]=(byte)0x00;
                        packBody[2]=(byte)0x11;
                        packBody[3]=(byte)0x00;
                        packBody[4]=(byte)0x20;
                        break;
                    //重新获取读数
                    case BODY_TEMPER_READING:
                        packBody[0]=(byte)0x02;
                        packBody[1]=(byte)0x00;
                        packBody[2]=(byte)0x35;
                        packBody[3]=(byte)0x00;
                        packBody[4]=(byte)0x55;
                        break;
                }
                break;
        }
        return packBody;
    }
}
