package com.hxlm.android.health.device.message.ecg;

import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;

/**
 * 用于控制心电参数数据包的输出状态；
 * <p/>
 * false 禁止数据包输出
 * true 允许数据包输出
 * Created by dells on 2015/11/26.
 */
public class EcgDataOutputCommand extends AbstractMessage {
    private final boolean isOutput;

    public EcgDataOutputCommand(boolean b) {
        super(HealthDeviceMessageType.ECG_DATA_OUTPUT_COMMAND);
        isOutput = b;
    }

    public boolean isOutput() {
        return isOutput;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("心电信号质量：").append(isOutput ? "允许数据包输出" : "禁止数据包输出\n");
        return sb.toString();
    }
}
