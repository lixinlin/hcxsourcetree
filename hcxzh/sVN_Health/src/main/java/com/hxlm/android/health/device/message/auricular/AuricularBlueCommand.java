package com.hxlm.android.health.device.message.auricular;

import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;

/**
 * Created by l on 2016/9/28.
 * //耳针仪蓝牙发送数据
 */
public class AuricularBlueCommand extends AbstractMessage {


    private byte qiangdu;


    public AuricularBlueCommand() {
        super(HealthDeviceMessageType.AURICULAR_COMMON_BLUETOOTH);
    }

    public enum CommandType {
        BLUE_MESSAGE_BEGIN, // 开始
        BLUE_MESSAGE_STOP, // 停止
        BLUE_MESSAGE_ADD, // 强度增加
        BLUE_MESSAGE_REDUCE, // 强度减小
        BLUE_MESSAGE_PAUSE, // 暂停
        BLUE_MESSAGE_RESUME,// 暂停恢复

    }

    private CommandType commandType;// 耳针仪的指定

    public CommandType getCommandType() {
        return commandType;
    }

    public int getQiangdu() {
        return qiangdu;
    }

    public void setQiangdu(byte qiangdu) {
        this.qiangdu = qiangdu;
    }

    public void setCommandType(CommandType commandType) {
        this.commandType = commandType;
    }

    @Override
    public String toString() {
        return "耳针仪发送指令 :   " + commandType;
    }
}
