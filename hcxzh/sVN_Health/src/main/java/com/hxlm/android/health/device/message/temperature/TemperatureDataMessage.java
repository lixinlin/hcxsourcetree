package com.hxlm.android.health.device.message.temperature;

import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;

/**
 * 体温设备参数消息
 * <p/>
 */
public class TemperatureDataMessage extends AbstractMessage {

    private int temperatureState;           //体温状态   0x00 正常 ； 0x01 体温探头滑落
    private int temperatureInteger;         //体温整数 ： 范围0~45
    private int temperatureDecimals;        //体温小数 ： 范围0~9

    public TemperatureDataMessage() {
        super(HealthDeviceMessageType.TEMPERATURE_DATA);
    }


    public int getTemperatureState() {
        return temperatureState;
    }

    public void setTemperatureState(int temperatureState) {
        this.temperatureState = temperatureState;
    }

    public int getTemperatureInteger() {
        return temperatureInteger;
    }

    public void setTemperatureInteger(int temperatureInteger) {
        this.temperatureInteger = temperatureInteger;
    }

    public int getTemperatureDecimals() {
        return temperatureDecimals;
    }

    public void setTemperatureDecimals(int temperatureDecimals) {
        this.temperatureDecimals = temperatureDecimals;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("体温状态：").append(temperatureState == 0 ? "正常" : "探头滑落。  \n");
        sb.append("体温：").append(temperatureInteger+"."+temperatureDecimals).append("\n");
        return sb.toString();
    }
}