package com.hxlm.android.health.device.model;

import com.hxlm.android.comm.AbstractDeviceActivity;
import com.hxlm.android.comm.AbstractModel;
import com.hxlm.android.health.device.codec.RongtaiBoardCodec;
import com.hxlm.android.comm.AbstractIOSession;
import com.hxlm.android.comm.iosession.UsbSerialIOSession;

import java.io.IOException;

/**
 * 充电宝设备
 *
 * @author Administrator
 */
public class RongtaiBoard extends AbstractModel {

    public RongtaiBoard() {
        super("站板设备", new FunctionType[]{FunctionType.BOARD});
    }

    @Override
    public AbstractIOSession getIOSession(AbstractDeviceActivity activity) {
        UsbSerialIOSession ioSession;
        try {
            ioSession = new UsbSerialIOSession(activity, new RongtaiBoardCodec(),
                    UsbSerialIOSession.DriverType.CH34X_DRIVER, 6790);
        } catch (IOException e) {
            return null;
        }

        ioSession.setBaudrate(115200);
        ioSession.setStopBits((byte) 1);
        ioSession.setDataBits((byte) 8);
        ioSession.setParityBits((byte) 0);
        ioSession.setFlowControl((byte) 0);

        return ioSession;
    }
}
