package com.hxlm.android.health.device.message.bloodpressure;

import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;

/**
 * 血压计电量数据格式
 * 
 * @author l
 * 
 */
public class BloodPressureBatteryPercentageMessage extends AbstractMessage {

	private int batteryPercentage;// 血压计电量百分比

	public BloodPressureBatteryPercentageMessage() {
		super(HealthDeviceMessageType.BLOOD_PRESSURE_BATTERY_PERCENTAGE);
	}

	public int getBatteryPercentage() {
		return batteryPercentage;
	}

	public void setBatteryPercentage(int batteryPercentage) {
		this.batteryPercentage = batteryPercentage;
	}

}
