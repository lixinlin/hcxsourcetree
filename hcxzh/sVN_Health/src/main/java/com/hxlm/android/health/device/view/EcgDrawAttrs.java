package com.hxlm.android.health.device.view;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Xml;
import android.view.View;
import com.hxlm.android.utils.Logger;
import org.xmlpull.v1.XmlPullParser;

import java.io.IOException;
import java.io.InputStream;


/**
 * 存放心电图绘制的参数。
 * <p/>
 * Created by Zhenyu on 2015/11/30.
 */
public class EcgDrawAttrs {
    public final static int DEFAULT_BG_BOLD_LINE_COLOR = 0xAAFFFFFF;  //默认粗线条颜色
    public final static int DEFAULT_BG_LINE_COLOR = 0x55FFFFFF;  //默认线条颜色
    public final static int DEFAULT_BG_LINE_WIDTH = 1;  //默认线条宽度，一个像素
    public final static int ITEMS_OF_GROUP = 5;   //每个组含多少个小格，每组边缘需要标注出来
    private static final String TAG = "EcgDrawAttrs";
    private static final String DEVICE_FILE_NAME = "devices.xml";
    private final static double MM_PER_INCH = 25.4;    //每英寸等于25.4毫米
    private static EcgDrawAttrs instance;
    final int screenWidth;
    final int screenHeight;
    double xPixelPerMM;
    double yPixelPerMM;
    int viewWidth; //当前控件宽度
    int viewHeight;  //当前控件高度
    //显示时以5格为单位进行显示，计算时需要加上最后一条线的宽度
    int xCount;  //横向能显示多少小格
    int yCount;  //纵向能显示多少小格
    long xStart; //横向线条的起点
    long yStart;  //纵向线条的起点
    long xEnd;   //横向线条的终点位置
    long yEnd;   //纵向线条的终点位置
    int width; //显示区域宽度
    int height;  //显示区域高度

    public EcgDrawAttrs(Context context) {
        float screenSize = getScreenSize(context);

        DisplayMetrics dm = context.getResources().getDisplayMetrics();

        if (dm.xdpi != dm.densityDpi || screenSize == 0) {
            xPixelPerMM = dm.xdpi / MM_PER_INCH;
            yPixelPerMM = dm.ydpi / MM_PER_INCH;
        } else {
            final double ppi = Math.sqrt(dm.widthPixels * dm.widthPixels +
                    dm.heightPixels * dm.heightPixels) / screenSize;
            xPixelPerMM = ppi / MM_PER_INCH;
            yPixelPerMM = ppi / MM_PER_INCH;
        }

        screenWidth = dm.widthPixels;
        screenHeight = dm.heightPixels;

        Logger.i(TAG, "手机型号: " + android.os.Build.MODEL + ",\nSDK版本:"
                + Build.VERSION.SDK_INT + ",\n系统版本:"
                + android.os.Build.VERSION.RELEASE + ", xdpi = " + dm.xdpi + ", ydpi = " + dm.ydpi + ", densityDpi = " +
                dm.densityDpi + ", density = " + dm.density + ", scaledDensity = " + dm.scaledDensity +
                ", screenWidth = " + dm.widthPixels + ", screenHeight = " + dm.heightPixels +
                "\n横向每毫米像素数为：" + xPixelPerMM + "; 纵向每毫米像素数为：" + yPixelPerMM);
    }

    public static EcgDrawAttrs getInstance(View view) {
        if (instance == null) {
            instance = new EcgDrawAttrs(view.getContext());
        }
        if (view.getWidth() != 0 && view.getHeight() != 0) {
            instance.setView(view);
        }
        return instance;
    }

    private void setView(View view) {
        if (view.getWidth() != viewWidth || view.getHeight() != viewHeight) {
            viewWidth = view.getWidth();
            viewHeight = view.getHeight();

            //显示时以5格为单位进行显示，计算时需要加上最后一条线的宽度
            xCount = ((int) ((viewWidth - DEFAULT_BG_LINE_WIDTH) / (xPixelPerMM * ITEMS_OF_GROUP))) * ITEMS_OF_GROUP;
            yCount = ((int) ((viewHeight - DEFAULT_BG_LINE_WIDTH) / (yPixelPerMM * ITEMS_OF_GROUP))) * ITEMS_OF_GROUP;

            width = (int) Math.round(xCount * xPixelPerMM) + DEFAULT_BG_LINE_WIDTH;
            height = (int) Math.round(yCount * yPixelPerMM) + DEFAULT_BG_LINE_WIDTH;

            xStart = Math.round((viewWidth - xCount * xPixelPerMM) / 2); //横向线条的起点
            yStart = Math.round((viewHeight - yCount * yPixelPerMM) / 2);  //纵向线条的起点

            xEnd = xStart + width;  //横向线条的终点位置
            yEnd = yStart + height;  //纵向线条的终点位置
        }
    }

    private float getScreenSize(Context context) {
        XmlPullParser parser = Xml.newPullParser(); //由android.util.Xml创建一个XmlPullParser实例
        InputStream is = null;

        try {
            is = context.getAssets().open(DEVICE_FILE_NAME);
            parser.setInput(is, "UTF-8");               //设置输入流 并指明编码方式

            int eventType = parser.getEventType();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (parser.getName().equals("device")) {
                            String deviceName = parser.getAttributeValue(null, "name");
                            if (android.os.Build.MODEL.equals(deviceName)) {
                                String screenSize = parser.getAttributeValue(null, "screen-size");
                                if (!TextUtils.isEmpty(screenSize)) {
                                    return Float.parseFloat(screenSize);
                                }
                            }
                        }
                        break;
                    default:
                        break;
                }
                eventType = parser.next();
            }
        } catch (Exception e) {
            Logger.e(TAG, e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    Logger.e(TAG, e);
                }
            }
        }
        return 0;
    }
}
