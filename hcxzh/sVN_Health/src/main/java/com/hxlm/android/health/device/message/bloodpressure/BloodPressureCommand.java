package com.hxlm.android.health.device.message.bloodpressure;

import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;

/**
 * 血压发送数据
 * 
 * @author l
 * 
 */
public class BloodPressureCommand extends AbstractMessage {

	public BloodPressureCommand() {

		super(HealthDeviceMessageType.BLOOD_PRESSURE_COMMAND);
	}

	public enum CommandType {
		Bl_PRESSURE_START_MEASUTING, // 开始测量
		Bl_PRESSURE_STOP_MEASUTING, // 结束测量
	//	Bl_PRESSURE_OPEN_VOICE_PROMPT, // 打开血压计语音提示命令
	//	Bl_PRESSURE_STOP_VOICE_PROMPT, // 关闭血压计语音提示命令
	//	Bl_PRESSURE_SWITCH_VOICE_PROMPT, // 切换血压计语音提示命令（此指令会循环切换血压计支持的语音类型）
	//	Bl_PRESSURE_SET_UP_VOICE_PROMPT_GERMAN,// 设置血压计语音提示类型命令（此指令会指定一类血压计支持的语音类型） 德语
	//	Bl_PRESSURE_SET_UP_VOICE_PROMPT_ENGLISH,//设置为英语
	//	Bl_PRESSURE_SET_UP_VOICE_PROMPT_FRENCH//设置为法语
	}

	private CommandType commandType;// 血压测量的指令

	public CommandType getCommandType() {
		return commandType;
	}

	public void setCommandType(CommandType commandType) {
		this.commandType = commandType;
	}

	@Override
	public String toString() {
		return "测量血压的输入指令 :   " + commandType;
	}
}
