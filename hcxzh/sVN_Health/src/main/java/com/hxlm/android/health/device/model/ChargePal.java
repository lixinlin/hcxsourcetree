package com.hxlm.android.health.device.model;

import com.hxlm.android.comm.AbstractDeviceActivity;
import com.hxlm.android.comm.AbstractModel;
import com.hxlm.android.health.device.codec.BerryMedECGV2Codec;
import com.hxlm.android.comm.AbstractIOSession;
import com.hxlm.android.comm.iosession.BluetoothBleIOSession;

import java.io.IOException;
import java.util.UUID;

/**
 * 充电宝设备
 *
 * @author Administrator
 */
public class ChargePal extends AbstractModel {

    public ChargePal() {
        super("充电宝", new FunctionType[]{FunctionType.ECG, FunctionType.SPO2});
    }

    @Override
    public AbstractIOSession getIOSession(AbstractDeviceActivity activity) {
        BluetoothBleIOSession ioSession;
        try {
            ioSession = new BluetoothBleIOSession(activity, new BerryMedECGV2Codec());
        } catch (IOException e) {
            return null;
        }

        ioSession.setBtName("ChargePal&&BerryMed");
        ioSession.setUuidService(UUID.fromString("49535343-fe7d-4ae5-8fa9-9fafd205e455"));
        ioSession.setUuidClientCharacterConfig(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));
        ioSession.setUuidCharacterReceive(UUID.fromString("49535343-1e4d-4bd9-ba61-23c647249616"));
        ioSession.setUuidCharaterSend(UUID.fromString("49535343-8841-43f4-a8d4-ecbe34729bb3"));

        return ioSession;
    }
}
