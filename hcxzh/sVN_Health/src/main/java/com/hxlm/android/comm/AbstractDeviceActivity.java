package com.hxlm.android.comm;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import com.hxlm.android.health.AbstractBaseActivity;
import com.hxlm.android.utils.ConstantHealth;

/**
 * 设备界面的抽象Activity
 * <p/>
 * Created by Zhenyu on 2015/11/28.
 */
@SuppressLint("NewApi")
public abstract class AbstractDeviceActivity extends AbstractBaseActivity {
    final static int MESSAGE_CONNECT_FAILED = 10001;
    final static int MESSAGE_CONNECTED = 10002;
    final static int MESSAGE_DISCONNECTED = 10003;
    final static int MESSAGE_EXCEPTION = 10004;
    final static int MESSAGE_RECEIVED = 10005;
    private Handler handler;

    protected final Handler.Callback callback = new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_CONNECT_FAILED:
                    if(ConstantHealth.isEnglish){
                        onConnectFailedEnglist((Error_English) msg.obj);
                    }else {
                        onConnectFailed((Error) msg.obj);
                    }

                    break;
                case MESSAGE_CONNECTED:
                    onConnected();
                    break;
                case MESSAGE_DISCONNECTED:
                    onDisconnected();
                    break;
                case MESSAGE_EXCEPTION:
                    onExceptionCaught((Throwable) msg.obj);
                    break;
                case MESSAGE_RECEIVED:
                    onMessageReceived((AbstractMessage) msg.obj);
                    break;
            }
            return false;
        }
    };

    protected AbstractIOSession ioSession;

    public Handler getHandler() {
        if(handler == null){
            handler = new Handler(callback);
        }
        return handler;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (ioSession != null) {
            ioSession.close();
        }
        this.finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (ioSession != null) {
            ioSession.close();
        }
    }

    protected abstract void onConnectFailed(Error error);
    protected abstract void onConnectFailedEnglist(Error_English error);

    protected abstract void onConnected();

    protected abstract void onDisconnected();

    protected abstract void onExceptionCaught(Throwable e);

    protected abstract void onMessageReceived(AbstractMessage message);
}
