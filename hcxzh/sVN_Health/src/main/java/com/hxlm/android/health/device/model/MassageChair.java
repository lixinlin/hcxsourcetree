package com.hxlm.android.health.device.model;

import com.hxlm.android.comm.AbstractDeviceActivity;
import com.hxlm.android.comm.AbstractModel;
import com.hxlm.android.health.device.codec.RongtaiChairCodec;
import com.hxlm.android.comm.AbstractIOSession;
import com.hxlm.android.comm.iosession.UsbSerialIOSession;

import java.io.IOException;

/**
 * 按摩椅的协议处理类
 *
 * @author l
 */
public class MassageChair extends AbstractModel {
    public MassageChair() {

        super("按摩椅", new FunctionType[]{FunctionType.MASSAGE_CHAIR});
    }

    @Override
    public AbstractIOSession getIOSession(AbstractDeviceActivity anActivity) {
        UsbSerialIOSession ioSession;
        try {
            ioSession = new UsbSerialIOSession(anActivity, new RongtaiChairCodec(),
                    UsbSerialIOSession.DriverType.XR_DRIVER, 1250);
        } catch (IOException e) {
            return null;
        }

        ioSession.setSerialportsel(0);
        ioSession.setParityBits((byte)0x00);
        ioSession.setBaudrate(115200);
        ioSession.setFlowControl((byte)0x00);
        ioSession.setDataBits((byte)0x08);
        ioSession.setStopBits((byte)0x01);
        ioSession.setLoopBack(false);

        return ioSession;
    }
}
