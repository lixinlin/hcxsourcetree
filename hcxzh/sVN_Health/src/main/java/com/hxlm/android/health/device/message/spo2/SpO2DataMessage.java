package com.hxlm.android.health.device.message.spo2;

import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;

public class SpO2DataMessage extends AbstractMessage {

	private int status;// 血氧状态 0x00 正常，0x01 血氧探头脱落，0x02 血氧指夹空，0x03 正在搜索脉搏，0x04
						// 脉搏搜索时间过长
	private int saturate;// 饱和度 正常值范围0~100，只有探头状态为“正常”时，数据有效；否则数据无效（无效值为127）
	private int pulseRate;// 脉搏率 正常值范围0~250，只有探头状态为“正常”时，数据有效；否则数据无效（无效值为255）

	public SpO2DataMessage() {

		super(HealthDeviceMessageType.SPO2_DATA);
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getSaturate() {
		return saturate;
	}

	public void setSaturate(int saturate) {
		this.saturate = saturate;
	}

	public int getPulseRate() {
		return pulseRate;
	}

	public void setPulseRate(int pulseRate) {
		this.pulseRate = pulseRate;
	}
}
