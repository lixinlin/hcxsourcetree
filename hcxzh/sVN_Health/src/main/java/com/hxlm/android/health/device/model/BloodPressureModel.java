package com.hxlm.android.health.device.model;

import com.hxlm.android.comm.AbstractDeviceActivity;
import com.hxlm.android.comm.AbstractIOSession;
import com.hxlm.android.comm.AbstractModel;
import com.hxlm.android.comm.iosession.BluetoothBleIOSession;
import com.hxlm.android.health.device.codec.BloodPressureCodec;
import com.hxlm.android.utils.Logger;

import java.io.IOException;
import java.util.UUID;

/**
 * 血压计设备
 * Created by l on 2016/3/31.
 */
public class BloodPressureModel extends AbstractModel {
    public BloodPressureModel() {
        super("血压计", new FunctionType[]{FunctionType.BLOOD_PRESSUER});
    }

    @Override
    public AbstractIOSession getIOSession(AbstractDeviceActivity activity) {
        BluetoothBleIOSession ioSession;
        try {
            ioSession = new BluetoothBleIOSession(activity, new BloodPressureCodec());
        } catch (IOException e) {
            return null;
        }

        ioSession.setBtName("BPM-188");
        ioSession.setUuidService(UUID.fromString("000018f0-0000-1000-8000-00805f9b34fb"));//服务id
        ioSession.setUuidClientCharacterConfig(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));//
        ioSession.setUuidCharacterReceive(UUID.fromString("00002af0-0000-1000-8000-00805f9b34fb"));//读
        ioSession.setUuidCharaterSend(UUID.fromString("00002af1-0000-1000-8000-00805f9b34fb"));//写

        return ioSession;
    }
}
