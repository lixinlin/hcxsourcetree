package com.hxlm.android.utils;

import android.content.Context;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * 用于操作Assets文件
 * Created by Zhenyu on 2016/4/1.
 */
public class AssetsUtil {

    /**
     * 从assets目录中复制整个文件夹内容
     *
     * @param context Context 使用CopyFiles类的Activity
     * @param oldPath String  原文件路径  如：/aa
     * @param newPath String  复制后路径  如：xx:/bb/cc
     */
    public static void copyFilesFromAssets(Context context, String oldPath, String newPath) throws IOException {
        String fileNames[] = context.getAssets().list(oldPath);//获取assets目录下的所有文件及目录名

        if (fileNames.length > 0) {//如果是目录
            File file = new File(newPath);

            if (file.mkdirs()) {
                for (String fileName : fileNames) {
                    copyFilesFromAssets(context, oldPath + "/" + fileName, newPath + "/" + fileName);
                }
            } else {
                throw new IOException("创建目录失败：" + file.getName());
            }
        } else {//如果是文件
            InputStream is = null;
            FileOutputStream fos = null;

            try {
                is = context.getAssets().open(oldPath);
                fos = new FileOutputStream(new File(newPath));

                byte[] buffer = new byte[1024];
                int byteCount;

                while ((byteCount = is.read(buffer)) != -1) {//循环从输入流读取 buffer字节
                    fos.write(buffer, 0, byteCount);//将读取的输入流写入到输出流
                }

                fos.flush();//刷新缓冲区
            } finally {
                if (is != null) {
                    is.close();
                }
                if (fos != null) {
                    fos.close();
                }
            }
        }
    }
}
