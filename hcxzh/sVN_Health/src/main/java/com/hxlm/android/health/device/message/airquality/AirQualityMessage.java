package com.hxlm.android.health.device.message.airquality;

import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;

/**
 * Created by l on 2017/1/22.
 * 空气质量检测接收设备返回信息
 */
public class AirQualityMessage extends AbstractMessage {

    private int PM1_0_CF;//PM1.0浓度（CF=1,标准颗粒物）
    private int PM2_5_CF;//PM2.5浓度（CF=1,标准颗粒物）
    private int PM10_CF;//PM10浓度（CF=1,标准颗粒物）
    private int PM1_0_Atmo;//PM1.0浓度（大气环境下）
    private int PM2_5_Atmo;//PM2.5浓度（大气环境下）
    private int PM10_Atmo;//PM10浓度（大气环境下）
    private int numberOfParticles_01_03;//表示0.1升空气中直径在0.3um以上颗粒物个数
    private int numberOfParticles_01_05;//表示0.1升空气中直径在0.5um以上颗粒物个数
    private int numberOfParticles_01_10;//表示0.1升空气中直径在1.0um以上颗粒物个数
    private int numberOfParticles_01_25;//表示0.1升空气中直径在2.5um以上颗粒物个数
    private int numberOfParticles_01_50;//表示0.1升空气中直径在5.0um以上颗粒物个数
    private int numberOfParticles_01_100;//表示0.1升空气中直径在10um以上颗粒物个数
    private int formaldehyde_Con_Value;//甲醛浓度数值



    public AirQualityMessage() {
        super(HealthDeviceMessageType.AIR_QUALITY_RESPONSE);
    }

    public int getPM1_0_CF() {
        return PM1_0_CF;
    }

    public void setPM1_0_CF(int PM1_0_CF) {
        this.PM1_0_CF = PM1_0_CF;
    }

    public int getPM2_5_CF() {
        return PM2_5_CF;
    }

    public void setPM2_5_CF(int PM2_5_CF) {
        this.PM2_5_CF = PM2_5_CF;
    }

    public int getPM10_CF() {
        return PM10_CF;
    }

    public void setPM10_CF(int PM10_CF) {
        this.PM10_CF = PM10_CF;
    }

    public int getPM1_0_Atmo() {
        return PM1_0_Atmo;
    }

    public void setPM1_0_Atmo(int PM1_0_Atmo) {
        this.PM1_0_Atmo = PM1_0_Atmo;
    }

    public int getPM2_5_Atmo() {
        return PM2_5_Atmo;
    }

    public void setPM2_5_Atmo(int PM2_5_Atmo) {
        this.PM2_5_Atmo = PM2_5_Atmo;
    }

    public int getPM10_Atmo() {
        return PM10_Atmo;
    }

    public void setPM10_Atmo(int PM10_Atmo) {
        this.PM10_Atmo = PM10_Atmo;
    }

    public int getNumberOfParticles_01_03() {
        return numberOfParticles_01_03;
    }

    public void setNumberOfParticles_01_03(int numberOfParticles_01_03) {
        this.numberOfParticles_01_03 = numberOfParticles_01_03;
    }

    public int getNumberOfParticles_01_05() {
        return numberOfParticles_01_05;
    }

    public void setNumberOfParticles_01_05(int numberOfParticles_01_05) {
        this.numberOfParticles_01_05 = numberOfParticles_01_05;
    }

    public int getNumberOfParticles_01_10() {
        return numberOfParticles_01_10;
    }

    public void setNumberOfParticles_01_10(int numberOfParticles_01_10) {
        this.numberOfParticles_01_10 = numberOfParticles_01_10;
    }

    public int getNumberOfParticles_01_25() {
        return numberOfParticles_01_25;
    }

    public void setNumberOfParticles_01_25(int numberOfParticles_01_25) {
        this.numberOfParticles_01_25 = numberOfParticles_01_25;
    }

    public int getNumberOfParticles_01_50() {
        return numberOfParticles_01_50;
    }

    public void setNumberOfParticles_01_50(int numberOfParticles_01_50) {
        this.numberOfParticles_01_50 = numberOfParticles_01_50;
    }

    public int getNumberOfParticles_01_100() {
        return numberOfParticles_01_100;
    }

    public void setNumberOfParticles_01_100(int numberOfParticles_01_100) {
        this.numberOfParticles_01_100 = numberOfParticles_01_100;
    }

    public int getFormaldehyde_Con_Value() {
        return formaldehyde_Con_Value;
    }

    public void setFormaldehyde_Con_Value(int formaldehyde_Con_Value) {
        this.formaldehyde_Con_Value = formaldehyde_Con_Value;
    }

    @Override
    public String toString() {
        return "AirQualityMessage{" +
                "PM1_0_CF=" + PM1_0_CF +
                ", PM2_5_CF=" + PM2_5_CF +
                ", PM10_CF=" + PM10_CF +
                ", PM1_0_Atmo=" + PM1_0_Atmo +
                ", PM2_5_Atmo=" + PM2_5_Atmo +
                ", PM10_Atmo=" + PM10_Atmo +
                ", numberOfParticles_01_03=" + numberOfParticles_01_03 +
                ", numberOfParticles_01_05=" + numberOfParticles_01_05 +
                ", numberOfParticles_01_10=" + numberOfParticles_01_10 +
                ", numberOfParticles_01_25=" + numberOfParticles_01_25 +
                ", numberOfParticles_01_50=" + numberOfParticles_01_50 +
                ", numberOfParticles_01_100=" + numberOfParticles_01_100 +
                ", formaldehyde_Con_Value=" + formaldehyde_Con_Value +
                '}';
    }
}
