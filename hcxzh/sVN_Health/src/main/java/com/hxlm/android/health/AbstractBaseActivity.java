package com.hxlm.android.health;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.DisplayMetrics;

import com.hxlm.R;
import com.hxlm.android.mobiledata.DeviceInfo;
import java.util.Locale;

@SuppressWarnings("unused")
public abstract class AbstractBaseActivity extends Activity {
    public static final String ACTION_EXIT = "exit_system";
    public static DeviceInfo deviceInfo;
    private Dialog loadingDialog;
    private ExitReceiver mBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //初始化
        setContentView();
        IntentFilter exitIntentFilter = new IntentFilter();
        exitIntentFilter.addAction(ACTION_EXIT);
        //注册广播
        mBroadcastReceiver = new ExitReceiver();
        registerReceiver(mBroadcastReceiver, exitIntentFilter);

        if (deviceInfo == null) {
            deviceInfo = new DeviceInfo();

            DisplayMetrics dm = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(dm);

            Locale locale = getResources().getConfiguration().locale;
            deviceInfo.setWidthPixels(dm.widthPixels);
            deviceInfo.setHeightPixels(dm.heightPixels);
            deviceInfo.setLanguage(locale.getLanguage());
            deviceInfo.setCountry(locale.getCountry());
        }
        initViews();
        initDatas();
    }



    // 加载布局
    public abstract void setContentView();

    // 初始化views
    public abstract void initViews();

    // 初始化数据
    public abstract void initDatas();

    protected void showLoadingDialog(Dialog dialog) {
        if (dialog != null && !dialog.isShowing()) {
            loadingDialog = dialog;
            dialog.show();
        }
    }

    protected void dismissLoadingDialog() {
        if (loadingDialog != null) {
            loadingDialog.dismiss();
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mBroadcastReceiver);
    }

    public class ExitReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            finish();
        }
    }

}
