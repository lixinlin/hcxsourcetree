package com.hxlm.android.health.device.message.bodytempergun;

import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;

/**
 * Created by l on 2016/12/26.
 * 体温枪发送命令
 */
public class BodyTemperGunCommand extends AbstractMessage {
    public enum CommandType {
        BODY_TEMPER_MEASURE,//体温测量
        BODY_TEMPER_READING,//重新获取读数
    }

    private CommandType commandType;// 命令

    public BodyTemperGunCommand() {
        super(HealthDeviceMessageType.BODYTEMPERGUN_COMMAND);
    }

    public CommandType getCommandType() {
        return commandType;
    }

    public void setCommandType(CommandType commandType) {
        this.commandType = commandType;
    }

    @Override
    public String toString() {
        return "体温枪发送的数据:   " + commandType;
    }
}
