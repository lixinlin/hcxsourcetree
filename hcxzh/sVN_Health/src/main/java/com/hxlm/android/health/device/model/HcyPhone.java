package com.hxlm.android.health.device.model;

import android.text.TextUtils;
import com.hxlm.android.utils.Logger;
import com.hxlm.android.comm.AbstractDeviceActivity;
import com.hxlm.android.comm.AbstractModel;
import com.hxlm.android.health.device.codec.BerryMedECGV2Codec;
import com.hxlm.android.comm.AbstractIOSession;
import com.hxlm.android.comm.iosession.SerialIOSession;

import java.io.*;

/**
 * 充电宝设备
 *
 * @author Administrator
 */
public class HcyPhone extends AbstractModel {
    private final static String TAG = "HcyPhoneEcgPowerDriver";
    private final static String LDO_PATCH = "/sys/bus/platform/drivers/vgp1_power/ldo";
    private PowerStatus powerStatus;

    public HcyPhone() {
        super("和畅依定制手机", new FunctionType[]{FunctionType.ECG, FunctionType.SPO2});
        readPowerStatus();
    }

    /**
     * 初始化心电芯片供电状态的方法
     */
    private void readPowerStatus() {
        File file = new File(LDO_PATCH);
        if (!file.exists()) {
            this.powerStatus = PowerStatus.UNSUPPORT;
        } else {
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new FileReader(LDO_PATCH));
                String prop = reader.readLine();
                if (!TextUtils.isEmpty(prop)) {
                    if ("1".equals(prop)) {
                        this.powerStatus = PowerStatus.ON;
                    } else {
                        this.powerStatus = PowerStatus.OFF;
                    }
                }
            } catch (IOException e) {
                this.powerStatus = PowerStatus.UNSUPPORT;
                Logger.e(TAG, e);
            } finally {
                if (null != reader) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        Logger.e(TAG, e);
                    }
                }
            }
        }
    }

    @Override
    public AbstractIOSession getIOSession(AbstractDeviceActivity activity) {
        SerialIOSession ioSession;
        try {
            ioSession = new SerialIOSession(activity, new BerryMedECGV2Codec(), "/dev/ttyMT3");
        } catch (IOException e) {
            return null;
        }

        ioSession.setBaudrate(115200);
        ioSession.setFlags(0);

        return ioSession;
    }

    public PowerStatus getPowerStatus() {
        return powerStatus;
    }

    public void setPowerStatus(PowerStatus status) throws IOException {
        if (this.powerStatus != PowerStatus.UNSUPPORT) {
            BufferedWriter bufWriter = null;

            try {
                bufWriter = new BufferedWriter(new FileWriter(LDO_PATCH));
                if (status == PowerStatus.ON) {
                    bufWriter.write("1");
                } else {
                    bufWriter.write("0");
                }
                this.powerStatus = status;

            } finally {
                if (null != bufWriter) {
                    bufWriter.close();
                }
            }
        }
    }

    public enum PowerStatus {
        ON,
        OFF,
        UNSUPPORT
    }
}
