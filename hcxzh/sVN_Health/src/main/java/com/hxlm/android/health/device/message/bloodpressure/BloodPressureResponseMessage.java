package com.hxlm.android.health.device.message.bloodpressure;

import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;

/**
 * 结果数据格式
 * 
 * @author l
 * 
 */
public class BloodPressureResponseMessage extends AbstractMessage {

	private int systol_ic;//收缩压
	private int diastol_ic;//舒张压值
	private int pulse_rate;//心率值
	
	
	
	public BloodPressureResponseMessage() {
		super(HealthDeviceMessageType.BLOOD_PRESSURE_RESPONSE);
	}



	public int getSystol_ic() {
		return systol_ic;
	}



	public void setSystol_ic(int systol_ic) {
		this.systol_ic = systol_ic;
	}



	public int getDiastol_ic() {
		return diastol_ic;
	}



	public void setDiastol_ic(int diastol_ic) {
		this.diastol_ic = diastol_ic;
	}



	public int getPulse_rate() {
		return pulse_rate;
	}



	public void setPulse_rate(int pulse_rate) {
		this.pulse_rate = pulse_rate;
	}

	@Override
	public String toString() {
		return "BloodPressureResponseMessage{" +
				"systol_ic=" + systol_ic +
				", diastol_ic=" + diastol_ic +
				", pulse_rate=" + pulse_rate +
				'}';
	}


	//	public enum ResultType {
//		Bl_PRESSURE_START_MEASUTING_SUCCESS, // 开始测量
//		Bl_PRESSURE_STOP_MEASUTING_SUCCESS, // 结束测量
//		Bl_PRESSURE_OPEN_VOICE_PROMPT_SUCCESS, // 打开血压计语音提示命令
//		Bl_PRESSURE_STOP_VOICE_PROMPT_SUCCESS, // 关闭血压计语音提示命令
//		Bl_PRESSURE_SWITCH_VOICE_PROMPT_SUCCESS, // 切换血压计语音提示命令（此指令会循环切换血压计支持的语音类型）
//		Bl_PRESSURE_SET_UP_VOICE_PROMPT_GERMAN_SUCCESS,// 设置血压计语音提示类型命令（此指令会指定一类血压计支持的语音类型）德语
//		Bl_PRESSURE_SET_UP_VOICE_PROMPT_ENGLISH_SUCCESS,//设置为英语
//		Bl_PRESSURE_SET_UP_VOICE_PROMPT_FRENCH_SUCCESS//设置为法语
//	
//	}
//
//	public ResultType result;
//
//	public ResultType getResult() {
//		return result;
//	}
//
//	public void setResult(ResultType result) {
//		this.result = result;
//	}
//
//	@Override
//	public String toString() {
//		switch (result) {
//		case Bl_PRESSURE_START_MEASUTING_SUCCESS:
//			return "开始测量指令数据成功";
//		case Bl_PRESSURE_STOP_MEASUTING_SUCCESS:
//			return "结束测量指令数据成功";
//		case Bl_PRESSURE_OPEN_VOICE_PROMPT_SUCCESS:
//			return "打开血压计语音提示命令成功";
//		case Bl_PRESSURE_STOP_VOICE_PROMPT_SUCCESS:
//			return "关闭血压计语音提示命令成功";
//		case Bl_PRESSURE_SWITCH_VOICE_PROMPT_SUCCESS:
//			return "切换血压计语音提示命令成功";
//		case Bl_PRESSURE_SET_UP_VOICE_PROMPT_GERMAN_SUCCESS:
//			return "设置为德语命令成功";
//		case Bl_PRESSURE_SET_UP_VOICE_PROMPT_ENGLISH_SUCCESS:
//			return "设置为英语命令成功";
//		case Bl_PRESSURE_SET_UP_VOICE_PROMPT_FRENCH_SUCCESS:
//			return "设置为法语命令成功";
//		
//		}
//		return "未知消息类型";
//	}
}
