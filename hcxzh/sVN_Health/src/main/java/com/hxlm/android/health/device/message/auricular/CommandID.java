package com.hxlm.android.health.device.message.auricular;

/**
 * Created by Zhenyu on 13-10-27.
 * 耳针仪操作命令
 */
public class CommandID {
    /**
     * 返回码
     * 三字节操作指令接收正确并完成操作后，从机返回0xFAEE00.
     * 接收到的三字节操作指令错误，返回0xFAEF00
     */
    //开始命令，数据内容为空，用0x00替代，状态指示灯常亮
    public static final byte REQUEST_BEGIN = (byte) 0xE1;
    //停止命令，数据内容为空，用0x00替代，状态指示灯熄灭
    public static final byte REQUEST_STOP = (byte) 0xE2;
    /**
     * 强度增大命令，数据内容为强度实际数值。
     * XX取值范围暂定00-60（十进制显示，十六进制传输）
     * 状态指示灯闪烁2次，熄灭
     */
    public static final byte REQUEST_ADD = (byte) 0xE3;

    /**
     * 强度减小命令，数据内容为强度实际数值。
     * XX取值范围暂定00-60（十进制显示，十六进制传输）
     * 状态指示灯闪烁3次，熄灭
     */
    public static final byte REQUEST_REDUCE = (byte) 0xE4;

    //暂停命令，数据内容为空，用0x00替代，状态指示灯连续闪烁
    public static final byte REQUEST_PAUSE = (byte) 0xE5;
    //暂停恢复命令，数据内容为空，用0x00替代，状态指示灯停止闪烁，熄灭
    public static final byte REQUEST_RESUME = (byte) 0xE6;
}
