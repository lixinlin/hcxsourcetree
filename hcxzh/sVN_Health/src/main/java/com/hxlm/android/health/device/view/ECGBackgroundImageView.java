package com.hxlm.android.health.device.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.hxlm.R;

/**
 * 该类用于绘制心电图的井字格背景图，线与线之间的距离需要等于1毫米。用户可以定义背景线条的颜色，默认为黄色。
 */
public class ECGBackgroundImageView extends ImageView {
    private int boldLineColor;   //格子组的分隔线条颜色
    private int lineColor;    //普通格子的分隔线条颜色

    private Paint paint;

    public ECGBackgroundImageView(Context context, AttributeSet attrs) {
        super(context);

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ECGBackgroundImageView, 0, 0);
        try {
            lineColor = a.getColor(R.styleable.ECGBackgroundImageView_line_color,
                    EcgDrawAttrs.DEFAULT_BG_LINE_COLOR);
            boldLineColor = a.getColor(R.styleable.ECGBackgroundImageView_bold_line_color,
                    EcgDrawAttrs.DEFAULT_BG_BOLD_LINE_COLOR);
        } finally {
            a.recycle();
        }

        paint = new Paint();
    }

    /**
     * 此方法为画出心电图背景图的方法，背景图必须是每毫米一格，每五个格子为一组，采用不同颜色的线条分
     * 割，其余使用一根细线条进行分隔。画布上显示的格子数必须是五的倍数。由于每毫米对应的像素数是小数
     * ，但是单一像素无法分割，格子必须采用像素进行显示，因此必须在画线条中均匀补齐出现的误差。
     * 首先从系统中获得本设备的ppi信息，即每英寸对应多少像素，该值精确到小数。从实践看，某些设备没有提
     * 供准确的值，此时需要从外部输入该设备的尺寸（即对角线是多少英寸），然后计算出大致的ppi。
     * 使用画布宽、高度除以每毫米像素数计算出可以容纳多少个格子， 按照宽度、高度的剩余像素计算出横向、
     * 纵向画线条的起点和终点位置（将图画在画布中央）；
     * 为实现较好的显示效果，需要再对每毫米对应的像素数进行四舍五入，获得最佳的每格像素整数，同时获得
     * 按照最佳像素进行绘制时会带来的单次误差；
     * 横向和纵向线条描绘各使用一个全局偏移量来对线条的位置进行控制，各设定一个全局变量来记录每次绘制
     * 出的整数像素积累的误差之和；
     * 每次绘制线条时默认按照最佳像素整数进行绘制，如果累积误差绝对值大于一个像素，则将该格的像素数增
     * 加或减少一个像素（视乎误差是正数或负数），同时累积误差减一；
     * 绘制至最后一根线条时，对剩余的累积误差进行四舍五入来决定最后是需要调整、或是按照最佳像素数进行
     * 绘制。
     *
     * @param canvas 画布
     */
    @Override
    protected void onDraw(Canvas canvas) {
        EcgDrawAttrs drawAttrs = EcgDrawAttrs.getInstance(this);

        //每一格显示的最佳像素数，按照四舍五入计算
        final int xIntPixelPerGrid = (int) Math.round(drawAttrs.xPixelPerMM);
        final int yIntPixelPerGrid = (int) Math.round(drawAttrs.yPixelPerMM);

        //按照整数像素的方法画线的话，每次会余下多少像素
        final double xBaseRemain = drawAttrs.xPixelPerMM - xIntPixelPerGrid;
        final double yBaseRemain = drawAttrs.yPixelPerMM - yIntPixelPerGrid;

        //全局偏移量定义
        long xOffset = drawAttrs.xStart;
        long yOffset = drawAttrs.yStart;

        //累积误差变量定义
        double yRemain = 0;
        double xRemain = 0;

        //计算出的间隔需要减掉画图自身所占的像素，首先画横线，接下来画竖线
        for (int i = 0; i <= drawAttrs.yCount; i++) {
            if (i % EcgDrawAttrs.ITEMS_OF_GROUP == 0) {
                paint.setColor(boldLineColor);
            } else {
                paint.setColor(lineColor);
            }

            //坐标值由0开始，终点坐标值应该是长度减线条宽度
            canvas.drawLine(drawAttrs.xStart, yOffset, drawAttrs.xEnd, yOffset, paint);

            //采用yRemain来记录每次的余数之和，如果yRemain已经大于1，则向下一格补充一个像素的宽度，同时将yRemain减1
            //用此种办法不断修正每毫米像素值为小数带来的误差。
            yRemain += yBaseRemain;
            if (Math.abs(yRemain) >= 1) {
                yOffset += yIntPixelPerGrid + (int) yRemain;
                yRemain -= (int) yRemain;
            } else if (i == drawAttrs.yCount - 1) {
                //如果已到最后一格，则按照yRemain的剩余值四舍五入作为最终的补充量。
                yOffset += yIntPixelPerGrid + Math.round(yRemain);
            } else {
                yOffset += yIntPixelPerGrid;
            }
        }

        for (int j = 0; j <= drawAttrs.xCount; j++) {
            if (j % EcgDrawAttrs.ITEMS_OF_GROUP == 0) {
                paint.setColor(boldLineColor);
            } else {
                paint.setColor(lineColor);
            }

            canvas.drawLine(xOffset, drawAttrs.yStart, xOffset, drawAttrs.yEnd, paint);

            xRemain += xBaseRemain;
            if (Math.abs(xRemain) >= 1) {
                xOffset += xIntPixelPerGrid + (int) xRemain;
                xRemain -= (int) xRemain;
            } else if (j == drawAttrs.xCount - 1) {
                xOffset += xIntPixelPerGrid + Math.round(xRemain);
            } else {
                xOffset += xIntPixelPerGrid;
            }
        }
    }
}

