package com.hxlm.android.health.device.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.*;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.hxlm.R;

/**
 * 用于绘制心电图
 * <p/>
 * Created by Zhenyu on 2016/4/1.
 */
public class ECGWaveImageView extends ImageView {
    private final static int MM_PER_SECONDS = 25; // 基准扫描速度为 25毫米/秒

    private final Paint paint;
    private final Path path;

    private final int numberPerMv; // 每毫伏对应的数值，用于换算距离
    private final int pointsPerSecond; // 每秒发送多少个数据
    private final int maxValue; // 传递的数据最大值是多少
    private final int minValue; // 传递的数据最小值是多少，默认为0
    private final PointF lastPoint;
    private float xStep = 1; // 扫描速度的倍速，默认25MM/s，
    private byte[] data;
    private int startIndex;

    // 每个数据包对应多少个像素
    private double pixelPerPacket;
    // 传过来的每个数字的1对应多少像素
    private double pixelPerNumber;
    // 实际需要的高度
    private double maxHeight;
    // 如果要把最大高度时坐标系中值定位在中间的话，需要减去本偏移量
    private float offset;
    //y轴上如果显示不下时，缩放比例
    private double yScale;

    private EcgDrawAttrs ecgDrawAttrs;

    private int pointsPerView;

    public ECGWaveImageView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ECGBackgroundImageView, 0, 0);
        int strokeWidth;
        int lineColor;
        try {
            lineColor = a.getColor(R.styleable.ECGWaveImageView_wave_line_color, Color.WHITE);
            strokeWidth = a.getColor(R.styleable.ECGWaveImageView_stroke_width, 3);
        } finally {
            a.recycle();
        }

        paint = new Paint();
        paint.setColor(lineColor);
        paint.setStrokeWidth(strokeWidth);
        paint.setAntiAlias(true);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStyle(Paint.Style.STROKE);

        path = new Path();

        setBackgroundColor(Color.TRANSPARENT);

        numberPerMv = 63;
        pointsPerSecond = 250;
        maxValue = 250;
        minValue = 0;

        lastPoint = new PointF(0, 0);
    }

    public void setData(byte[] data, int index) {
        this.data = data;
        this.startIndex = index;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (ecgDrawAttrs == null) {
            ecgDrawAttrs = EcgDrawAttrs.getInstance(this);

            // 每个数据包对应多少个像素
            pixelPerPacket = xStep * ecgDrawAttrs.xPixelPerMM * MM_PER_SECONDS / pointsPerSecond;
            // 传过来的每个数字的1对应多少像素
            pixelPerNumber = ecgDrawAttrs.yPixelPerMM * 10 / numberPerMv;
            // 实际需要的高度
            maxHeight = (maxValue - minValue) * pixelPerNumber;
            // 如果要把最大高度时坐标系中值定位在中间的话，需要减去本偏移量
            offset = (float) ((maxHeight - ecgDrawAttrs.height) / 2);
            //y轴上如果显示不下时，缩放比例
            yScale = (double) ecgDrawAttrs.height / ((maxValue - minValue) * pixelPerNumber);
            // yScale = 1;

            pointsPerView = (int) (ecgDrawAttrs.width / pixelPerPacket);
        }

        if (data != null) {
            path.reset();

            int displayPointCount = 0;

            for (int i = startIndex; i < (data.length - startIndex); i++) {
                //采用25mm/s的标准扫描速度进行绘制，X坐标每个点应该表示1/250秒 。
                // (xPixelPerMM * MM_PER_SECONDS / pointsPerSecond)表示每个包对应的毫米值。
                //计算显示到某个数据时，实际x坐标在屏幕上的位置
                float x = Math.round(displayPointCount * pixelPerPacket);

                //如果x坐标值已经超出显示区域，则跳出循环，准备从头显示。
                if (x > ecgDrawAttrs.width) {
                    break;
                }
                //显示时需要加上边缘的偏移量
                x += ecgDrawAttrs.xStart;

                //Y坐标需要重新定位到Android以左上角为原点的坐标系 （如果电极接反，会出现R波向下的情况）
                //y = ecgDrawAttrs.yEnd - (float) (data * pixelPerNumber * yScale) + offset;
                //将电压中值至于网格中央时，波形一直在下方，因此还是采用底部在下方的方式
                float y = ecgDrawAttrs.yEnd - (float) ((data[i] & 0x000000FF) * pixelPerNumber * yScale);

                if (lastPoint.x != 0 || lastPoint.y != 0) {
                    path.moveTo(lastPoint.x, lastPoint.y);
                    path.lineTo(x, y);
                }

                lastPoint.x = x;
                lastPoint.y = y;

                displayPointCount++;
            }
            canvas.drawPath(path, paint);
        }
        lastPoint.x = 0;
        lastPoint.y = 0;
    }

    public int getPointsPerView() {
        return pointsPerView;
    }
}
