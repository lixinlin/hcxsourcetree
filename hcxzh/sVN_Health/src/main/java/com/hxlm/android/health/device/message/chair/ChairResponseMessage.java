package com.hxlm.android.health.device.message.chair;

import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;

/**
 * 按摩椅接收数据
 *
 * @author l
 */
public class ChairResponseMessage extends AbstractMessage {
    public enum ResultType {
        NONE_SUCCESS,//无键值
        POWER_SWITCH_SUCCESS,//电源键
        LEGPLANK_RESET_SUCCESS,//复位-站板0档
        BACKREST_UP_SUCCESS,//靠背起接收成功
        BACKREST_UP_STOP_SUCCESS,//靠背升停接收成功
        BACKREST_DOWN_SUCCESS,//靠背降接收成功
        BACKREST_DOWN_STOP_SUCCESS,//靠背降停接收成功
        BOARD_STRETCH_SUCCESS,//小腿伸接收成功
        BOARD_SHRINK_SUCCESS,//小腿缩接收成功
        MASSAGE_PATTERN_1_SUCCESS,//自动程序1接收成功
        MASSAGE_PATTERN_2_SUCCESS,//自动程序2接收成功
        MASSAGE_PATTERN_3_SUCCESS,//自动程序3接收成功
        MESSAGE_ZERO_GRAVITY_SUCCESS,//零重力命令接收成功
        BOARD_PAUSE_SUCCESS,//站板暂停功能接收成功
        MASSAGE_PAUSE_SUCCESS, //按摩暂停接受成功
        MASSAGE_CONTINUE_SUCCESS, //按摩恢复接收成功
        POWER_SWITCH_CLOSE_SUCCESS,//电源关闭接收成功
        ALL_RESET_SUCCESS,//全部复位接收成功
    }

    private ResultType result;

    public ChairResponseMessage() {
        super(HealthDeviceMessageType.CHAIR_COMMAND_RESPONSE);
    }

    public ResultType getResult() {
        return result;
    }

    public void setResult(ResultType result) {
        this.result = result;
    }

    @Override
    public String toString() {
        if(result==null)
        {
            return "返回的消息值为空";
        }
        switch (result){
            case NONE_SUCCESS:
                return "无按键接收成功";
            case POWER_SWITCH_SUCCESS:
                return "电源设置成功";
            case LEGPLANK_RESET_SUCCESS:
                return "0档设置成功";
            case BACKREST_UP_SUCCESS:
                return "靠背起设置成功";
            case BACKREST_UP_STOP_SUCCESS:
                return "靠背升停接收成功";
            case BACKREST_DOWN_SUCCESS:
                return "靠背降设置成功";
            case BACKREST_DOWN_STOP_SUCCESS:
                return "靠背降停接收成功";
            case BOARD_STRETCH_SUCCESS:
                return "站板伸出设置成功";
            case BOARD_SHRINK_SUCCESS:
                return "站板缩回设置成功";
            case MASSAGE_PATTERN_1_SUCCESS:
                return "按摩模式一设置成功";
            case MASSAGE_PATTERN_2_SUCCESS:
                return "按摩模式二设置成功";
            case MASSAGE_PATTERN_3_SUCCESS:
                return "按摩模式三设置成功";
            case MESSAGE_ZERO_GRAVITY_SUCCESS:
                return "零重力命令接收成功";
            case BOARD_PAUSE_SUCCESS:
                return "站板暂停功能接收成功";
            case MASSAGE_PAUSE_SUCCESS:
                return "按摩暂停功能接收成功";
            case MASSAGE_CONTINUE_SUCCESS:
                return "按摩恢复功能接收成功";

        }
        return "未知消息类型";
    }
}
