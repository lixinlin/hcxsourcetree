package com.hxlm.android.health.device.message.ecg;

import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;

/**
 * 心电设备参数消息
 * <p/>
 * Created by Zhenyu on 2015/11/24.
 */
public class EcgDataMessage extends AbstractMessage {
    private int signalQuality;      //心电信号质量    0 正常；1 心电信号太弱；
    private int connection;         //心电导联状态     0 正常；1 心电导联脱落；
    private int signalGain;         //心电模块增益    00： x0.25 增益；01： x0.5 增益；2： x1 增益；3： x2 增益
    private int ecgFilteringModel;   //心电滤波模式    00： 手术模式；01： 监护模式；2： 诊断模式；

    private int heartRate;           //心率   范围0~250，单位：次/分
    private int respiratoryRate;     //呼吸率： 范围0~250，单位：次/分
    private int stPotential;          //范围-100~+100 表示–1mV~+1mV，例如：-75 表示-0.75mV，+55 表示+0.55mV

    public EcgDataMessage() {
        super(HealthDeviceMessageType.ECG_DATA);
    }

    public int getSignalQuality() {
        return signalQuality;
    }

    public void setSignalQuality(int signalQuality) {
        this.signalQuality = signalQuality;
    }

    public int getConnection() {
        return connection;
    }

    public void setConnection(int connection) {
        this.connection = connection;
    }

    public int getSignalGain() {
        return signalGain;
    }

    public void setSignalGain(int signalGain) {
        this.signalGain = signalGain;
    }

    public int getEcgFilteringModel() {
        return ecgFilteringModel;
    }

    public void setEcgFilteringModel(int ecgFilteringModel) {
        this.ecgFilteringModel = ecgFilteringModel;
    }

    public int getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int heartRate) {
        this.heartRate = heartRate;
    }

    public int getRespiratoryRate() {
        return respiratoryRate;
    }

    public void setRespiratoryRate(int respiratoryRate) {
        this.respiratoryRate = respiratoryRate;
    }

    public int getStPotential() {
        return stPotential;
    }

    public void setStPotential(int stPotential) {
        this.stPotential = stPotential;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("心电信号质量：").append(signalQuality == 0 ? "正常" : "信号太弱。  \n");
        sb.append("心电导联状态：").append(connection == 0 ? "正常" : "导联脱落。  \n");
        sb.append("心电模块增益：");
        switch (signalGain) {
            case 0:
                sb.append("x0.25 增益\n");
                break;
            case 1:
                sb.append("x0.5 增益\n");
                break;
            case 2:
                sb.append("x1 增益\n");
                break;
            case 3:
                sb.append("x2 增益\n");
                break;
        }
        sb.append("心电滤波模式：");
        switch (ecgFilteringModel) {
            case 0:
                sb.append("手术模式\n");
                break;
            case 1:
                sb.append("监护模式\n");
                break;
            case 2:
                sb.append("诊断模式\n");
                break;
        }
        sb.append("心率：").append(heartRate).append("\n");
        sb.append("呼吸率：").append(respiratoryRate).append("\n");
        sb.append("电平：").append(stPotential).append("\n");
        return sb.toString();
    }
}