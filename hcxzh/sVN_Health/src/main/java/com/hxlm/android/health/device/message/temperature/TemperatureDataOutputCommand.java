package com.hxlm.android.health.device.message.temperature;

import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;

/**
 * 用于控制体温数据包的输出状态；
 *
 *     false 禁止数据包输出
 *     true 允许数据包输出
 * Created by dells on 2015/11/26.
 */
public class TemperatureDataOutputCommand extends AbstractMessage {
    private  final boolean isOutput;

    public TemperatureDataOutputCommand(boolean b) {
        super(HealthDeviceMessageType.TEMPERATURE_DATA_OUTPUT_COMMAND);
        isOutput = b;
    }

    public boolean isOutput() {
        return isOutput;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("体温信号质量：").append(isOutput ? "允许数据包输出" : "禁止数据包输出\n");
        return sb.toString();
    }
    
    
    
}
