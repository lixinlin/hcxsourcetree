package com.hxlm.android.health.device.codec;

import com.hxlm.android.comm.AbstractCodec;
import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.ChecksumErrorMessage;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;
import com.hxlm.android.health.device.message.ecg.*;
import com.hxlm.android.health.device.message.spo2.SpO2DataMessage;
import com.hxlm.android.health.device.message.spo2.SpO2DataOutputCommand;
import com.hxlm.android.health.device.message.spo2.SpO2WaveOutputCommand;


/**
 * 充电宝设备
 *
 * @author Administrator
 */
public class BerryMedECGV2Codec extends AbstractCodec {
    private final static int POINTS_PER_SECOND = 250;
    private final static int NUMBER_PER_MV = 63;
    private final static int DATA_MAX_VALUE = 250;
    private final static int DATA_MIN_VALUE = 0;

    private EcgWaveQueueMessage ecgWaveQueueMessage;

    public BerryMedECGV2Codec() {
        super(new byte[]{(byte) 0x55, (byte) 0xAA}, 3, 30);
    }

    @Override
    protected int getBodyLength(int bodyStartIndex) {
        return dataBuffer[bodyStartIndex] & 0x000000FF;
    }

    @Override
    public AbstractMessage decodeMessage(final int bodyStartIndex) throws InterruptedException {
        int bodyLength = dataBuffer[bodyStartIndex] & 0x000000FF; // 内容的总长度，默认是包头后的第一个字节

        AbstractMessage message = null;

        // 只有当校验和正确时才进行处理
        if (getCheckSumByte(dataBuffer, bodyStartIndex, (bodyLength - 1)) ==
                dataBuffer[bodyStartIndex + bodyLength - 1]) {

            // 传递给解码器的位置索引从内容长度字节的后一位开始，也就是命令字
            switch (dataBuffer[bodyStartIndex + 1]) {

                case 0x01:
                    if (ecgWaveQueueMessage == null) {
                        ecgWaveQueueMessage = new EcgWaveQueueMessage(NUMBER_PER_MV,
                                POINTS_PER_SECOND, DATA_MAX_VALUE, DATA_MIN_VALUE);
                        message = ecgWaveQueueMessage;
                    }

                    for (int i = 0; i < 5; i++) {
                        ecgWaveQueueMessage.getWaveQueue().put(dataBuffer[bodyStartIndex + 2 + i] & 0x000000FF);
                    }
                    break;

                case 0x02:
                    EcgDataMessage ecgDataMessage = new EcgDataMessage();

                    ecgDataMessage.setSignalQuality(dataBuffer[bodyStartIndex + 2] >> 7);
                    ecgDataMessage.setConnection((dataBuffer[bodyStartIndex + 2] & 0x40) >> 6);
                    ecgDataMessage.setSignalGain((dataBuffer[bodyStartIndex + 2] & 0x30) >> 4);
                    ecgDataMessage.setEcgFilteringModel((dataBuffer[bodyStartIndex + 2] & 0x0C) >> 2);

                    ecgDataMessage.setHeartRate(dataBuffer[bodyStartIndex + 3] & 0x000000FF);
                    ecgDataMessage.setRespiratoryRate(dataBuffer[bodyStartIndex + 4] & 0x000000FF);
                    ecgDataMessage.setStPotential(dataBuffer[bodyStartIndex + 5] & 0x000000FF);

                    message = ecgDataMessage;
                    break;

                case 0x04:
                    SpO2DataMessage spO2DataMessage = new SpO2DataMessage();
                    spO2DataMessage.setStatus(dataBuffer[bodyStartIndex + 2] & 0x000000FF);
                    spO2DataMessage.setSaturate(dataBuffer[bodyStartIndex + 3] & 0x000000FF);
                    spO2DataMessage.setPulseRate(dataBuffer[bodyStartIndex + 4] & 0x000000FF);

                    message = spO2DataMessage;
                    break;
            }
        } else {
            message = new ChecksumErrorMessage();
        }

        return message;
    }

    @Override
    public byte[] encodeMessage(final AbstractMessage message) {
        byte[] packBody = new byte[4];
        packBody[0] = (byte) 0x04;

        switch ((HealthDeviceMessageType) message.getMessageType()) {
            case ECG_SIGNAL_GAIN_COMMAND:
                packBody[1] = 0x07;
                packBody[2] = (byte) ((EcgSignalGainCommand) message).getSignalGain();
                break;
            case ECG_DATA_OUTPUT_COMMAND:
                packBody[1] = 0x01;
                packBody[2] = (byte) (((EcgDataOutputCommand) message).isOutput() ? 0x01 : 0x00);
                break;
            case ECG_FILTER_TYPE_COMMAND:
                packBody[1] = 0x08;
                packBody[2] = (byte) ((EcgFilterTypeCommand) message).getSignalFiltrate();
                break;
            case ECG_WAVE_OUTPUT_COMMAND:
                packBody[1] = (byte) 0xFB;
                packBody[2] = (byte) (((EcgWaveOutputCommand) message).isOutput() ? 0x01 : 0x00);
                break;
            case SPO2_DATA_OUTPUT_COMMAND:
                packBody[1] = 0x03;
                packBody[2] = (byte) (((SpO2DataOutputCommand) message).isOutput() ? 0x01 : 0x00);
                break;
            case SPO2_WAVE_OUTPUT_COMMAND:
                packBody[1] = (byte) 0xFE;
                packBody[2] = (byte) (((SpO2WaveOutputCommand) message).isOutput() ? 0x01 : 0x00);
                break;
        }
        packBody[3] = getCheckSumByte(packBody, 0, 3);

        return packBody;
    }
}
