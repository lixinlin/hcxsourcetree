package com.hxlm.android.comm.iosession;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import com.exar.android.usbdriver.XRDriver;
import com.hoho.android.usbserial.driver.Cp2102SerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.hoho.android.usbserial.util.SerialInputOutputManager;
import com.hxlm.android.comm.AbstractCodec;
import com.hxlm.android.comm.AbstractDeviceActivity;
import com.hxlm.android.comm.AbstractIOSession;
import com.hxlm.android.comm.Error;
import com.hxlm.android.health.device.drivers.CH341AndroidDriver;
import com.hxlm.android.utils.Logger;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * USB串口通讯的会话管理类
 * <p/>
 * Created by Zhenyu on 2015/11/15.
 */
public class UsbSerialIOSession extends AbstractIOSession {
    private static final String ACTION_USB_PERMISSION = "com.hxlm.USB_PERMISSION";
    private final UsbManager usbManager;

    private final BroadcastReceiver usbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (ACTION_USB_PERMISSION.equals(action)) {
                if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                    Logger.d(getClass().getName(), "用户授权成功。");
                    open();
                } else {
                    Logger.d(getClass().getName(), "用户没有提供授权。");
                    onConnectFailed(Error.PERMISSION_DENY);
                }
            } else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                Logger.d(getClass().getName(), "设备被拔出");
                close();
            }
        }
    };

    private final DriverType driverType;
    private ExecutorService readExecutor;
    private UsbDevice mUsbDevice = null;
    private SerialInputOutputManager mSerialIoManager;
    private UsbSerialDriver mUsbSerialDriver;
    private XRDriver xrDriver;
    private CH341AndroidDriver ch341Driver;

    // 设置设备连接的基本数据
    private byte parityBits = (byte) 0x00;// 校验位
    private byte flowControl = (byte) 0x00;// 流控制
    private int baudrate = 115200;// 波特率
    private byte stopBits = (byte) 0x01;// 停止位
    private int serialportsel = 0;
    private byte dataBits = (byte) 0x08;// 数据位
    private boolean loopBack = false;

    private int vendorId;
    private boolean isRegistered = false;

    public UsbSerialIOSession(final AbstractDeviceActivity anActivity, final AbstractCodec abstractCodec,
                              final DriverType aDriverType, final int aVendorId) throws IOException {
        super(anActivity, abstractCodec);
        usbManager = (UsbManager) activity.getSystemService(Context.USB_SERVICE);
        driverType = aDriverType;
        vendorId = aVendorId;

        Collection<UsbDevice> devices = usbManager.getDeviceList().values();

        if (devices.size() > 0) {
            if (vendorId == 0) {
                mUsbDevice = devices.iterator().next();
                vendorId = mUsbDevice.getVendorId();
            } else {
                for (UsbDevice device : devices) {
                    if (device.getVendorId() == vendorId) {
                        mUsbDevice = device;
                        Logger.i(tag,"name-->"+device.getDeviceName()+"--VendorId-->"+device.getVendorId()+"--DeviceId"+device.getDeviceId());
                        break;
                    }
                }
                if(mUsbDevice == null){
                    throw new IOException("No USB device is found !");
                }
            }
        } else {
            throw new IOException("No USB device is connected !");
        }
    }

    /**
     * 获取已经连接的USB设备并启动连接
     */
    @Override
    public void doConnect() {
        if (mUsbDevice == null) {
            onConnectFailed(Error.DEVICE_NOT_FOUND);

        } else {
            Logger.d(tag, "设备已找到， Vendor ID = " + vendorId);

            if (usbManager.hasPermission(mUsbDevice)) {
                open();
            } else {
                Logger.d(tag, "获取设备访问授权。");
                requestPermission();
            }
        }
    }

    /**
     * 连接设备前需要验证并申请访问设备的系统权限
     */
    private void requestPermission() {
        Logger.d(tag, "开始申请用户授权。");

        final PendingIntent mPermissionIntent = PendingIntent.getBroadcast(
                activity, 0, new Intent(ACTION_USB_PERMISSION), 0);

        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        activity.registerReceiver(usbReceiver, filter);
        isRegistered = true;

        usbManager.requestPermission(mUsbDevice, mPermissionIntent);
    }

    private void open() {
        Logger.d(tag, "开始进行连接。");

        switch (driverType) {
            case COMMON_DRIVER:
                openCommon();
                break;
            case COMMON_DRIVER_CP2102:
                openCommonCp2102();
                break;
            case XR_DRIVER:
                openXR();
                break;
            case CH34X_DRIVER:
                openCH34X();
                break;
        }
    }

    //普通驱动
    private void openCommon() {

        List<UsbSerialDriver> drivers = UsbSerialProber.probeSingleDevice(usbManager, mUsbDevice);

        Logger.i(tag,"drivers的个数-->"+drivers.size());

        if (drivers != null&&drivers.size()>0) {
            mUsbSerialDriver = drivers.get(0);
        }

        if (mUsbSerialDriver != null) {
            try {
                mUsbSerialDriver.open();
            } catch (IOException e) {
                onConnectFailed(Error.DRIVER_INIT_FAILED);
            }

            mSerialIoManager = new SerialInputOutputManager(mUsbSerialDriver, new SerialInputOutputManager.Listener() {
                @Override
                public void onRunError(Exception e) {
                    exceptionCaught(e);
                    Logger.i(tag, "异常1-->" + e.getMessage());
                }

                @Override
                public void onNewData(final byte[] data) {
                    //普通Usb返回数据
                    parseData(data, data.length);
                }
            });

            readExecutor = Executors.newSingleThreadExecutor();
            readExecutor.execute(mSerialIoManager);

            onConnected();
        } else {
            onConnectFailed(Error.DRIVER_NOT_FOUND);
        }
    }

    //针对Cp2102SerialDriver的驱动
    private void openCommonCp2102() {

        UsbDeviceConnection connection = usbManager.openDevice(mUsbDevice);
        mUsbSerialDriver = new Cp2102SerialDriver(mUsbDevice,connection);

        if (mUsbSerialDriver != null) {
            try {
                mUsbSerialDriver.open();
            } catch (IOException e) {
                onConnectFailed(Error.DRIVER_INIT_FAILED);
            }

            mSerialIoManager = new SerialInputOutputManager(mUsbSerialDriver, new SerialInputOutputManager.Listener() {
                @Override
                public void onRunError(Exception e) {
                    exceptionCaught(e);
                    Logger.i(tag, "异常1-->" + e.getMessage());
                }

                @Override
                public void onNewData(final byte[] data) {
                    //普通Usb返回数据
                    parseData(data, data.length);
                }
            });

            readExecutor = Executors.newSingleThreadExecutor();
            readExecutor.execute(mSerialIoManager);

            onConnected();
        } else {
            onConnectFailed(Error.DRIVER_NOT_FOUND);
        }
    }

    private void openXR() {
        xrDriver = new XRDriver(usbManager);

        if (xrDriver.begin()) {
            xrDriver.setParameters(serialportsel, baudrate, dataBits, stopBits,
                    parityBits, flowControl, loopBack);

            // 此方法启动另一个线程连续读取数据从UART。
            readExecutor = Executors.newSingleThreadExecutor();
            readExecutor.execute(new ReadRunnable(this));

            //先设置成连接状态，下面会读取数据
            onConnected();

        } else {
            onConnectFailed(Error.DRIVER_INIT_FAILED);
        }
    }

    private void openCH34X() {
        ch341Driver = new CH341AndroidDriver(usbManager, activity, ACTION_USB_PERMISSION);

        // 判断设备是否已经连接到Android系统 true为连入;设置初始化CH34x芯片 若初始化失败，则返回false,成功返回true
        ch341Driver.OpenUsbDevice(mUsbDevice);
        if (ch341Driver.UartInit()) {
            // 若初始化成功，设置UART接口的波特率，数据位，停止位，奇偶校验位以及流控
            ch341Driver.SetConfig(baudrate, dataBits, stopBits, parityBits, flowControl);

            // 此方法启动另一个线程连续读取数据从UART。
            readExecutor = Executors.newSingleThreadExecutor();
            readExecutor.execute(new ReadRunnable(this));

            onConnected();
        } else {
            onConnectFailed(Error.DRIVER_INIT_FAILED);
        }
    }

    @Override
    protected void doClose() {
        switch (driverType) {
            case COMMON_DRIVER:
                if (mSerialIoManager != null) {
                    mSerialIoManager.stop();
                }
                break;
            case COMMON_DRIVER_CP2102:
                if (mSerialIoManager != null) {
                    mSerialIoManager.stop();
                }
                break;
            case XR_DRIVER:
                if (xrDriver != null) {
                    xrDriver.end();
                }
                break;
            case CH34X_DRIVER:
                if (ch341Driver != null) {
                    ch341Driver.CloseDevice();
                }
                break;
        }

        if (isRegistered) {
            activity.unregisterReceiver(usbReceiver);
            isRegistered = false;
        }

        mUsbSerialDriver = null;

        if (readExecutor != null) {
            readExecutor.shutdownNow();
        }
    }

    @Override
    public void doSendMessage(byte[] packet) {
        switch (driverType) {
            case COMMON_DRIVER:
                mSerialIoManager.writeAsync(packet);
                break;
            case COMMON_DRIVER_CP2102:
                mSerialIoManager.writeAsync(packet);
                break;
            case XR_DRIVER:
                xrDriver.write(packet, packet.length);
                break;
            case CH34X_DRIVER:
                try {
                    ch341Driver.WriteData(packet, packet.length);
                } catch (IOException e) {
                    exceptionCaught(e);
                }
                break;
        }
    }


    @Override
    public int read(byte[] buffer) {
        switch (driverType) {
            case XR_DRIVER:

                if (xrDriver != null) {
                    return xrDriver.read(buffer, serialportsel);
                }
//                return xrDriver.read(buffer, serialportsel);

            case CH34X_DRIVER:
                return ch341Driver.ReadData(buffer, buffer.length);
        }
        return -1;
    }

    public void setParityBits(byte parityBits) {
        this.parityBits = parityBits;
    }

    public void setFlowControl(byte flowControl) {
        this.flowControl = flowControl;
    }

    public void setBaudrate(int baudrate) {
        this.baudrate = baudrate;
    }

    public void setStopBits(byte stopBits) {
        this.stopBits = stopBits;
    }

    public void setSerialportsel(int serialportsel) {
        this.serialportsel = serialportsel;
    }

    public void setDataBits(byte dataBits) {
        this.dataBits = dataBits;
    }

    public void setLoopBack(boolean loopBack) {
        this.loopBack = loopBack;
    }

    public enum DriverType {
        COMMON_DRIVER,
        COMMON_DRIVER_CP2102,
        XR_DRIVER,
        CH34X_DRIVER
    }
}
