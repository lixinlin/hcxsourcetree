package com.hxlm.android.health.device.message.auricular;

import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;

/**
 * Created by l on 2016/11/2.
 * 耳针仪接收命令
 */
public class AuricularBlueResponseMessage extends AbstractMessage {
    private String resposeMessage;

    public AuricularBlueResponseMessage() {
        super(HealthDeviceMessageType.AURICULAR_COMMON_BLUETOOTH_RESP_MESSAGE);
    }

    public String getResposeMessage() {
        return resposeMessage;
    }

    public void setResposeMessage(String resposeMessage) {
        this.resposeMessage = resposeMessage;
    }


}
