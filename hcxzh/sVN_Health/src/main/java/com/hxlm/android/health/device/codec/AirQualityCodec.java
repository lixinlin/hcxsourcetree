package com.hxlm.android.health.device.codec;

import com.hxlm.android.comm.AbstractCodec;
import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.ChecksumErrorMessage;
import com.hxlm.android.health.device.message.airquality.AirQualityMessage;
import com.hxlm.android.utils.ByteUtil;
import com.hxlm.android.utils.Logger;

/**
 * Created by l on 2017/1/22.
 * 空气质量检测的Codec
 */
public class AirQualityCodec extends AbstractCodec {

    public AirQualityCodec() {
        super(new byte[]{(byte) 0x42, (byte) 0x4d}, 30, 31);
    }

    @Override
    protected int getBodyLength(int bodyStartIndex) {
        return 30;
    }

    //接收设备返回信息
    @Override
    protected AbstractMessage decodeMessage(int bodyStartIndex) throws InterruptedException {

        int bodyLength = ByteUtil.bytes2Int(dataBuffer, bodyStartIndex, 2, true) + 2; // 内容的总长度，默认是包头后的第一个字节（数据+校验位）+自己所占的2个字节(30个字节)

        Logger.i(tag, "bodyLength-->"
                + bodyLength + "\ngetCheckSumByteAdd-->"
                + getCheckSumByteAdd(dataBuffer, bodyStartIndex - 2, bodyLength)
                + "\nbytes2Int-->" + ByteUtil.bytes2Int(dataBuffer, bodyStartIndex + bodyLength - 2, 2, true)
        );

        Logger.i(tag, "Code--空气质量-->" + ByteUtil.bytesToHexString(dataBuffer, bodyStartIndex - 2, 32));

        AbstractMessage message;
        // 只有当校验和正确时才进行处理
        if (getCheckSumByteAdd(dataBuffer, bodyStartIndex - 2, bodyLength) == ByteUtil.bytes2Int(dataBuffer, bodyStartIndex + bodyLength - 2, 2, true)) {
            AirQualityMessage airQualityMessage = new AirQualityMessage();
            airQualityMessage.setPM1_0_CF(ByteUtil.bytes2Int(dataBuffer, bodyStartIndex + 2, 2, true));
            airQualityMessage.setPM2_5_CF(ByteUtil.bytes2Int(dataBuffer, bodyStartIndex + 4, 2, true));
            airQualityMessage.setPM10_CF(ByteUtil.bytes2Int(dataBuffer, bodyStartIndex + 6, 2, true));
            airQualityMessage.setPM1_0_Atmo(ByteUtil.bytes2Int(dataBuffer, bodyStartIndex + 8, 2, true));
            airQualityMessage.setPM2_5_Atmo(ByteUtil.bytes2Int(dataBuffer, bodyStartIndex + 10, 2, true));
            airQualityMessage.setPM10_Atmo(ByteUtil.bytes2Int(dataBuffer, bodyStartIndex + 12, 2, true));
            airQualityMessage.setNumberOfParticles_01_03(ByteUtil.bytes2Int(dataBuffer, bodyStartIndex + 14, 2, true));
            airQualityMessage.setNumberOfParticles_01_05(ByteUtil.bytes2Int(dataBuffer, bodyStartIndex + 16, 2, true));
            airQualityMessage.setNumberOfParticles_01_10(ByteUtil.bytes2Int(dataBuffer, bodyStartIndex + 18, 2, true));
            airQualityMessage.setNumberOfParticles_01_25(ByteUtil.bytes2Int(dataBuffer, bodyStartIndex + 20, 2, true));
            airQualityMessage.setNumberOfParticles_01_50(ByteUtil.bytes2Int(dataBuffer, bodyStartIndex + 22, 2, true));
            airQualityMessage.setNumberOfParticles_01_100(ByteUtil.bytes2Int(dataBuffer, bodyStartIndex + 24, 2, true));
            airQualityMessage.setFormaldehyde_Con_Value(ByteUtil.bytes2Int(dataBuffer, bodyStartIndex + 26, 2, true));
            message = airQualityMessage;

            Logger.i(tag, "空气质量各个数值-->" + airQualityMessage.toString());

        } else {
            message = new ChecksumErrorMessage();
        }

        return message;
    }

    // 向设备发送数据
    @Override
    protected byte[] encodeMessage(AbstractMessage message) {
        return null;
    }
}
