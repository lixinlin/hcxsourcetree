package com.hxlm.android.utils;


import android.util.Log;

/**
 * 重新封装Android的日志输出类，便于控制日志在上线后不输出。
 * Created by Zhenyu on 2016/4/6.
 */
@SuppressWarnings("unused")
public class Logger {
    private final static String LOG_PREFIX = "HXLM-";
    //如果设置为ERROR+1，可以把所有的log关闭
    private static int logLevel = Log.VERBOSE;
    //控制是否输出所有错误信息
    private static final boolean IS_PRINT_STACK_TRACE = true;

    public static void i(String tag, String msg) {
        if (logLevel <= Log.INFO && msg != null)
            Log.i(LOG_PREFIX + tag, msg);
    }

    public static void e(String tag, String msg) {
        if (logLevel <= Log.ERROR && msg != null)
            Log.e(LOG_PREFIX + tag, msg);
    }

    public static void e(String tag, Throwable e) {
        if (logLevel <= Log.ERROR && e != null)
            if(IS_PRINT_STACK_TRACE){
                e.printStackTrace();
            }else {
                Log.e(LOG_PREFIX + tag, e.getMessage());
            }
    }

    public static void d(String tag, String msg) {
        if (logLevel <= Log.DEBUG && msg != null)
            Log.d(LOG_PREFIX + tag, msg);
    }

    public static void v(String tag, String msg) {
        if (logLevel <= Log.VERBOSE && msg != null)
            Log.v(LOG_PREFIX + tag, msg);
    }

    public static void w(String tag, String msg) {
        if (logLevel <= Log.WARN && msg != null)
            Log.w(LOG_PREFIX + tag, msg);
    }
}
