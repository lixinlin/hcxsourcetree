package com.hxlm.android.health.device.message.chair;

import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;

/**
 * 按摩椅发送数据
 *
 * @author l
 */
public class ChairCommand extends AbstractMessage {
    public enum CommandType {
        NONE,//无按键
        POWER_SWITCH,//电源键
        POWER_SWITCH_CLOSE,//电源键关
        LEGPLANK_RESET,//复位-站板0档
        BACKREST_UP_START,
        BACKREST_UP_STOP,
        BACKREST_DOWN_START,
        BACKREST_DOWN_STOP,
        BOARD_STRETCH_START,
        BOARD_STRETCH_STOP,
        BOARD_SHRINK_START,
        BOARD_SHRINK_STOP,
        MASSAGE_PATTERN_1,
        MASSAGE_PATTERN_2,
        MASSAGE_PATTERN_3,
        MESSAGE_ZERO_GRAVITY,//零重力
        MASSAGE_PAUSE,//按摩暂停
        MASSAGE_CONTINUE,//按摩恢复
        ALL_RESET,//全部复位
    }

    private CommandType commandType;// 命令

    public ChairCommand() {
        super(HealthDeviceMessageType.CHAIR_COMMAND);
    }

    public CommandType getCommandType() {
        return commandType;
    }

    public void setCommandType(CommandType commandType) {
        this.commandType = commandType;
    }

    @Override
    public String toString() {
        return "按摩椅发送的数据:   " + commandType;
    }

}
