package com.hxlm.android.health.device.model;

import android.widget.Toast;
import com.hxlm.android.comm.AbstractDeviceActivity;
import com.hxlm.android.comm.AbstractIOSession;
import com.hxlm.android.comm.AbstractModel;
import com.hxlm.android.comm.iosession.BluetoothBleIOSession;
import com.hxlm.android.comm.iosession.UsbSerialIOSession;
import com.hxlm.android.health.device.codec.AirQualityCodec;
import com.hxlm.android.health.device.codec.BloodPressureCodec;
import com.hxlm.android.health.device.codec.BodyTemperGunCodec;
import com.hxlm.android.utils.Logger;

import java.io.IOException;
import java.util.UUID;

/**
 * Created by l on 2016/12/26.
 * 空气质量检测的Model
 */
public class AirQualityModel extends AbstractModel {
    public AirQualityModel() {
        super("空气质量", new FunctionType[]{FunctionType.AIR_QUALITY});
    }
    @Override
    public AbstractIOSession getIOSession(AbstractDeviceActivity activity) {
        BluetoothBleIOSession ioSession;
        try {
            ioSession = new BluetoothBleIOSession(activity, new AirQualityCodec());
        } catch (IOException e) {
            return null;
        }

        ioSession.setBtName("MLT-BT05");
        ioSession.setUuidService(UUID.fromString("0000ffe0-0000-1000-8000-00805f9b34fb"));//服务id
        ioSession.setUuidClientCharacterConfig(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));//
        ioSession.setUuidCharacterReceive(UUID.fromString("0000ffe1-0000-1000-8000-00805f9b34fb"));//读
        ioSession.setUuidCharaterSend(UUID.fromString("0000ffe1-0000-1000-8000-00805f9b34fb"));//写

        return ioSession;
    }
}
