package com.hxlm.android.health.device.codec;

import com.hxlm.android.comm.AbstractCodec;
import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;
import com.hxlm.android.health.device.message.bloodpressure.*;
import com.hxlm.android.utils.ByteUtil;

/**
 * 血压计 指定发送命令与接收返回命令
 *
 * @author l
 */
public class BloodPressureCodec extends AbstractCodec {
    //定义两个返回数据读取的包头,读包头，写包头，最小包长度，最大包长度
    public BloodPressureCodec() {
        super(new byte[]{(byte) 0xFF}, new byte[]{(byte) 0x02}, 6, 10);
    }

    // 包体长度 返回的数据：02(定义为包头) 或者ff包头
    @Override
    protected int getBodyLength(int bodyStartIndex) {
        //如果读取数据开始的包头为0x02
        if ((byte) 0x02 == readPacketHeader[0]) {
            return (dataBuffer[bodyStartIndex + 2] & 0x000000FF) + 4;
        }
        //读取数据开始的包头为0xff
        else if ((byte) 0xFF == readPacketHeader[0]) {
            return (dataBuffer[bodyStartIndex + 1] & 0x000000FF) + 3;
        } else {
            return 0;
        }
    }

    // 接收设备返回信息
    @Override
    protected AbstractMessage decodeMessage(int bodyStartIndex)
            throws InterruptedException {
        AbstractMessage message = null;
        //如果读取数据开始的包头为0x02
        if ((byte) 0x02 == readPacketHeader[0]) {
            int bodyLength = (dataBuffer[bodyStartIndex + 2] & 0x000000FF) + 4; // 内容的总长度，除了包头之后的所有数据
            // 只有当校验和正确时才进行处理
            if (getCheckSumByteXOR(dataBuffer, bodyStartIndex - 1, bodyLength) == dataBuffer[bodyStartIndex
                    + bodyLength - 1]) {

                // 判断返回的是什么数值
                // 压力值数据
                if (dataBuffer[bodyStartIndex + 2] == 2) {
                    BloodPressureValueMessage bloodPressureValue = new BloodPressureValueMessage();
                    bloodPressureValue.setPressureValue(ByteUtil.bytes2Int(
                            dataBuffer, dataBuffer[bodyStartIndex + 3], 2, true));
                    message = bloodPressureValue;
                }
                // 血压计电量百分比
                else if (dataBuffer[bodyStartIndex + 2] == 3) {
                    BloodPressureBatteryPercentageMessage batteryPercentageMessage = new BloodPressureBatteryPercentageMessage();
                    batteryPercentageMessage
                            .setBatteryPercentage(dataBuffer[bodyStartIndex + 5]);
                    message = batteryPercentageMessage;
                }


            }
        }//读取数据开始的包头为0xff
        else if ((byte) 0xFF == readPacketHeader[0]) {

            int bodyLength = (dataBuffer[bodyStartIndex + 1] & 0x000000FF) + 3; // 内容的总长度，除了包头之后的所有数据

            // 只有当校验和正确时才进行处理
            if (getCheckSumByteXOR(dataBuffer, bodyStartIndex - 1, bodyLength) == (dataBuffer[bodyStartIndex
                    + bodyLength - 1])) {
                // 结果数据格式 数据长度为3
                if (dataBuffer[bodyStartIndex + 1] == 3) {

                    //判断收缩压是否为0，不为0，则有数据，否则是返回错误信息
                    int systol_Ic = dataBuffer[bodyStartIndex + 2];
                    if (systol_Ic != 0) {
                        BloodPressureResponseMessage bloodPressureResponseMessage = new BloodPressureResponseMessage();
                        bloodPressureResponseMessage.setSystol_ic(dataBuffer[bodyStartIndex + 2] & 0x000000FF);// 收缩压
                        bloodPressureResponseMessage.setDiastol_ic(dataBuffer[bodyStartIndex + 3] & 0x000000FF);// 舒张压
                        bloodPressureResponseMessage.setPulse_rate(dataBuffer[bodyStartIndex + 4] & 0x000000FF);// 心率

                        message = bloodPressureResponseMessage;
                    } else {
                        // 测量失败
                        BloodPressureERR bloodPressureERR = new BloodPressureERR();
                        bloodPressureERR.setErr(dataBuffer[bodyStartIndex + 4]);
                        message = bloodPressureERR;
                    }
                }
            }
        }


        return message;
    }

    // 向设备发送指令
    @Override
    protected byte[] encodeMessage(AbstractMessage message) {
        byte[] packBody = new byte[5];

        //Logger.i(BluetoothBleIOSession.TAG, "走到这里1");
        // 数据包内容
        switch ((HealthDeviceMessageType) message.getMessageType()) {
            case BLOOD_PRESSURE_COMMAND:

                BloodPressureCommand msg = (BloodPressureCommand) message;

                switch (msg.getCommandType()) {
                    // 开始测量
                    case Bl_PRESSURE_START_MEASUTING:
                        //packBody = new byte[5];
                        packBody[0] = (byte) 0x40;
                        packBody[1] = (byte) 0xdc;
                        packBody[2] = (byte) 0x01;
                        packBody[3] = (byte) 0xa1;
                        packBody[4] = (byte) 0x3c;
                        break;

                    // 结束测量
                    case Bl_PRESSURE_STOP_MEASUTING:
                        // packBody = new byte[5];
                        packBody[0] = (byte) 0x40;
                        packBody[1] = (byte) 0xdc;
                        packBody[2] = (byte) 0x01;
                        packBody[3] = (byte) 0xa2;
                        packBody[4] = (byte) 0x3f;
                        break;
                    // 打开血压计语音提示命令
//                    case Bl_PRESSURE_OPEN_VOICE_PROMPT:
//                        packBody = new byte[5];
//                        packBody[0] = (byte) 0x40;
//                        packBody[1] = (byte) 0xdc;
//                        packBody[2] = (byte) 0x01;
//                        packBody[3] = (byte) 0xa4;
//                        packBody[4] = (byte) 0x39;
//                        break;
                    // 关闭血压计语音提示命令
//                    case Bl_PRESSURE_STOP_VOICE_PROMPT:
//                        packBody = new byte[5];
//                        packBody[0] = (byte) 0x40;
//                        packBody[1] = (byte) 0xdc;
//                        packBody[2] = (byte) 0x01;
//                        packBody[3] = (byte) 0xa3;
//                        packBody[4] = (byte) 0x3e;
//                        break;
                    // 切换血压计语音提示命令（此指令会循环切换血压计支持的语音类型）
//                    case Bl_PRESSURE_SWITCH_VOICE_PROMPT:
//                        packBody = new byte[5];
//                        packBody[0] = (byte) 0x40;
//                        packBody[1] = (byte) 0xdc;
//                        packBody[2] = (byte) 0x01;
//                        packBody[3] = (byte) 0xa5;
//                        packBody[4] = (byte) 0x38;
//                        break;
                    // 设置血压计语音提示类型命令（此指令会指定一类血压计支持的语音类型）德语
//                    case Bl_PRESSURE_SET_UP_VOICE_PROMPT_GERMAN:
//                        packBody = new byte[6];
//                        packBody[0] = (byte) 0x40;
//                        packBody[1] = (byte) 0xdc;
//                        packBody[2] = (byte) 0x02;
//                        packBody[3] = (byte) 0xa6;
//                        packBody[4] = (byte) 0x01;
//                        packBody[5] = (byte) 0x39;
//                        break;
                    // 英语
//                    case Bl_PRESSURE_SET_UP_VOICE_PROMPT_ENGLISH:
//                        packBody = new byte[6];
//                        packBody[0] = (byte) 0x40;
//                        packBody[1] = (byte) 0xdc;
//                        packBody[2] = (byte) 0x02;
//                        packBody[3] = (byte) 0xa6;
//                        packBody[4] = (byte) 0x02;
//                        packBody[5] = (byte) 0x3a;
//                        break;
                    // 法语
//                    case Bl_PRESSURE_SET_UP_VOICE_PROMPT_FRENCH:
//                        packBody = new byte[6];
//                        packBody[0] = (byte) 0x40;
//                        packBody[1] = (byte) 0xdc;
//                        packBody[2] = (byte) 0x02;
//                        packBody[3] = (byte) 0xa6;
//                        packBody[4] = (byte) 0x03;
//                        packBody[5] = (byte) 0x3b;
//                        break;
                }

                break;
        }
        return packBody;
    }
}
