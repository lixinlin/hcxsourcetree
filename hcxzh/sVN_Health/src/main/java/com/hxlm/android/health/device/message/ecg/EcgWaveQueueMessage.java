package com.hxlm.android.health.device.message.ecg;

import com.hxlm.android.comm.AbstractMessage;
import com.hxlm.android.health.device.message.HealthDeviceMessageType;
import com.hxlm.android.utils.IntArrayBlockingQueue;

/**
 * 存放心电波形数据
 * <p/>
 * Created by Zhenyu on 2015/11/25.
 */
public class EcgWaveQueueMessage extends AbstractMessage {
    private final static int QUEUE_CAPACITY = 500;  //初始化心电数据传送的阻塞队列容量

    private final IntArrayBlockingQueue waveQueue;

    private final int numberPerMv;  //每个数字代表的电压数，单位为毫伏
    private final int pointsPerSecond;   //每秒钟传送的数据个数
    private final int maxValue;    //数据的最大值
    private final int minValue;    //数据的最小值

    public EcgWaveQueueMessage(int theNumberPerMv, int thePointsPerSecond,
                               int dataMaxValue, int dataMinValue) {
        super(HealthDeviceMessageType.ECG_WAVE);

        waveQueue = new IntArrayBlockingQueue(QUEUE_CAPACITY);
        numberPerMv = theNumberPerMv;
        pointsPerSecond = thePointsPerSecond;
        maxValue = dataMaxValue;
        minValue = dataMinValue;
    }

    public IntArrayBlockingQueue getWaveQueue() {
        return waveQueue;
    }

    public int getNumberPerMv() {
        return numberPerMv;
    }

    public int getPointsPerSecond() {
        return pointsPerSecond;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public int getMinValue() {
        return minValue;
    }

    @Override
    public String toString() {
        return "心电数据队列数据量：" + waveQueue.size() + "；  剩余空间：" +
                waveQueue.remainingCapacity();
    }
}
