package com.hxlm.android.health.device.view;

import android.graphics.*;
import android.text.TextUtils;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android_serialport_api.ECGfilter;
import com.hxlm.android.health.device.message.ecg.EcgWaveQueueMessage;
import com.hxlm.android.utils.AssetsUtil;
import com.hxlm.android.utils.IntArrayBlockingQueue;
import com.hxlm.android.utils.Logger;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * 心电图记录的是电压随时间变化的曲线。心电图记录在坐标纸上，坐标纸为由1mm宽和1mm高的小格组成。
 * 横坐标表示时间，纵坐标表示电压。通常采用25mm/s纸速记录，1小格=1mm=0.04秒。纵坐标电压1小格=1mm=0.1mv。
 * <p/>
 * Created by Zhenyu on 2016/3/30.
 */
@SuppressWarnings("unused")
public class ECGDrawWaveManager {
    static final String ECG_FILTER_ASSETS_PATH = "filters";
    public final static String ECG_FILE_PATH = "/ECG";
    static final String ECG_FILTER_FILE_PATH = "/Filters";
    private final static String TAG = "ECGDrawWaveManager";

    private final static int MM_PER_SECONDS = 25; // 基准扫描速度为 25毫米/秒
    private final static int POINTS_PER_TIME = 5; // 每次处理的点的数量
    private final static int POINTS_PER_WRITE = 512; // 每次向文件写入的点的数量，SD卡每扇区为512字节
    private final static int DATA_FILTER_COUNT = 1000; //滤波程序每次处理的数据量
    private final static int FILTER_QUEUE_CAPACITY = 3000; //滤波程序输出队列长度
    private final static int SEMPAPHORE_COUNT = 10; //滤波时控制画图的速度与取数据速度保持一致的信号量
    private final static int POINT_IGNORE = 1000; //略过最初的多少个点不写入文件

    private final int surfaceViewHeight;
    private final SurfaceHolder surfaceHolder;

    private final IntArrayBlockingQueue inQueue;

    private final int numberPerMv; // 每毫伏对应的数值，用于换算距离
    private final int pointsPerSecond; // 每秒发送多少个数据
    private final int maxValue; // 传递的数据最大值是多少
    private final int minValue; // 传递的数据最小值是多少，默认为0

    private final String basePath;
    private final EcgDrawAttrs attrs;
    private String filterPath;
    private String dataFileName;
    private IntArrayBlockingQueue filterQueue;
    private ExecutorService executorService;
    private int strokeWidth = 2; // 绘制心电图的线条宽度，像素值
    private int cleanerWidth = 20; // 心电图循环绘制时新旧线条的空隔宽度，像素值
    private float xStep = 1; // 扫描速度的倍速，默认25MM/s，
    private int lineColor = Color.WHITE;
    private boolean isRunning = false;
    private boolean isUsingFilter = false;
    private boolean isRecordOn = true;
    private Semaphore sempaphore;
    private BufferedOutputStream bos = null;

    public ECGDrawWaveManager(final SurfaceView sfv, final EcgWaveQueueMessage ecgWaveQueueMessage,
                              final String aBasePath) throws IOException {
        basePath = aBasePath;

        if (!TextUtils.isEmpty(aBasePath)) {
            File ecgDataFileDir = new File(aBasePath + ECG_FILE_PATH);

            if (!ecgDataFileDir.exists() && !ecgDataFileDir.mkdirs()) {
                throw new IOException("Unable to create the dir to save ECG files!");
            }
        }

        File filterFiles = new File(aBasePath + ECG_FILTER_FILE_PATH);
        if (!filterFiles.exists()) {
            try {
                AssetsUtil.copyFilesFromAssets(sfv.getContext(), ECG_FILTER_ASSETS_PATH,
                        aBasePath + ECG_FILTER_FILE_PATH);

                filterPath = aBasePath + ECG_FILTER_FILE_PATH;
            } catch (IOException e) {
                filterPath = null;
                Logger.e(TAG, e);
            }
        } else {
            filterPath = aBasePath + ECG_FILTER_FILE_PATH;
        }

        attrs = EcgDrawAttrs.getInstance(sfv);

        this.surfaceViewHeight = sfv.getHeight();
        surfaceHolder = sfv.getHolder();
        surfaceHolder.setFormat(PixelFormat.TRANSPARENT);

        this.inQueue = ecgWaveQueueMessage.getWaveQueue();

        numberPerMv = ecgWaveQueueMessage.getNumberPerMv();
        pointsPerSecond = ecgWaveQueueMessage.getPointsPerSecond();
        maxValue = ecgWaveQueueMessage.getMaxValue();
        minValue = ecgWaveQueueMessage.getMinValue();
    }

    public void startDraw() {
        isRunning = true;

        if (isUsingFilter) {
            Logger.i(TAG, "开始绘制波形修正后的心电图。");

            filterQueue = new IntArrayBlockingQueue(FILTER_QUEUE_CAPACITY);
            executorService = Executors.newFixedThreadPool(3);
            executorService.execute(new FilterRunnable());
            executorService.execute(new DrawWaveRunnable());
        } else {
            Logger.i(TAG, "开始绘制原始心电图。");

            executorService = Executors.newSingleThreadExecutor();
            executorService.execute(new DrawWaveRunnable());
        }
    }

    public void stopDraw() {

        isRunning = false;

        //必须释放掉同步信号量，否则进程会被锁住无法退出
        if (sempaphore != null) {
            sempaphore.release(SEMPAPHORE_COUNT);
        }

        Logger.i(TAG, "停止画心电图，释放同步信号量。");

        if (executorService != null) {
            executorService.shutdownNow();
        }

        Logger.i(TAG, "停止画图进程。");

        if (bos != null) {
            try {
                bos.close();
                bos = null;
            } catch (IOException e) {
                Logger.e(TAG, e);
            }
        }
    }

    private String generateFileName() {
        return System.currentTimeMillis() + ".ecg";
    }

    public boolean isRecordOn() {
        return isRecordOn;
    }

    public void setRecordOn(boolean isRecord) {
        this.isRecordOn = isRecord;
    }

    public String getFilterPath() {
        return filterPath;
    }

    public String getDataFileName() {
        return basePath + ECG_FILE_PATH + "/" + dataFileName;
    }

    public void setCleanerWidth(int cleanerWidth) {
        this.cleanerWidth = cleanerWidth;
    }

    public void setStrokeWidth(int strokeWidth) {
        this.strokeWidth = strokeWidth;
    }

    public void setxStep(float xStep) {
        this.xStep = xStep;
    }

    public void setLineColor(int lineColor) {
        this.lineColor = lineColor;
    }

    public boolean isUsingFilter() {
        return isUsingFilter;
    }

    public void setUsingFilter(boolean isUsingFilter) {
        this.isUsingFilter = isUsingFilter;

        if (TextUtils.isEmpty(filterPath)) {
            this.isUsingFilter = false;
        }
    }

    private final class FilterRunnable implements Runnable {
        @Override
        public void run() {
            int[] data;

            while (isRunning) {
                data = new int[DATA_FILTER_COUNT];

                for (int i = 0; i < DATA_FILTER_COUNT && isRunning; i++) {
                    try {
                        data[i] = inQueue.take();
                        if (sempaphore != null) {
                            sempaphore.release();
                        }
                    } catch (InterruptedException e) {
                        Logger.e(TAG, e);
                    }
                }

                if (sempaphore == null) {
                    sempaphore = new Semaphore(SEMPAPHORE_COUNT);
                }

                if (isRunning) {
                    executorService.execute(new ECGfilter(data, filterQueue, filterPath));
                }
            }
            Logger.i(TAG, "滤波进程已退出。");
        }
    }

    private final class DrawWaveRunnable implements Runnable {

        private final PointF lastPoint;
        private  final Paint paint;
        private long pointsCount;

        DrawWaveRunnable() {
            paint = new Paint();
            paint.setColor(lineColor);
            paint.setStrokeWidth(strokeWidth);
            paint.setAntiAlias(true);
            paint.setStrokeCap(Paint.Cap.ROUND);
            paint.setStyle(Paint.Style.STROKE);

            lastPoint = new PointF(0, 0);
        }

        /**
         * 采用25mm/s的标准扫描速度。不缩放的情况下，纵向需要能显示40小格，即4毫伏的区间。
         * 由于X坐标非常密集，测试发现使不使用贝塞尔曲线对心电图绘制影响不大。
         */
        @Override
        public void run() {
            int displayPointCount = 0;  //控制超出显示尺寸时折回显示

            if (isRecordOn) {
                dataFileName = generateFileName();
                try {
                    bos = new BufferedOutputStream(
                            new FileOutputStream(basePath + ECG_FILE_PATH + "/" + dataFileName));
                } catch (IOException e) {
                    Logger.e(TAG, e);
                }
            }

            // 每个数据包对应多少个像素
            final double pixelPerPacket = xStep * attrs.xPixelPerMM * MM_PER_SECONDS / pointsPerSecond;
            // 传过来的每个数字的1对应多少像素
            final double pixelPerNumber = attrs.yPixelPerMM * 10 / numberPerMv;
            // 实际需要的高度
            final double maxHeight = (maxValue - minValue) * pixelPerNumber;
            // 如果要把最大高度时坐标系中值定位在中间的话，需要减去本偏移量
            final float offset = (float) ((maxHeight - attrs.height) / 2);
            //y轴上如果显示不下时，缩放比例
             final double yScale = (double) attrs.height / ((maxValue - minValue) * pixelPerNumber);
           // final double yScale = 1;

            Path path = new Path();

            while (isRunning) {
                //开始绘制心电图
                 Canvas  canvas = surfaceHolder.lockCanvas(new Rect((int) (lastPoint.x), 0,
                        (int) (lastPoint.x + POINTS_PER_TIME + cleanerWidth), surfaceViewHeight));

                if (canvas != null) {
                    //清理锁定区域
                    paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                    canvas.drawPaint(paint);
                    //设定绘图模式
                    paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));

                    path.reset();

                    //每次绘制一定数量的点
                    for (int i = 0; i < POINTS_PER_TIME && isRunning; i++) {
                        //采用25mm/s的标准扫描速度进行绘制，X坐标每个点应该表示1/250秒 。
                        // (xPixelPerMM * MM_PER_SECONDS / pointsPerSecond)表示每个包对应的毫米值。
                        //计算显示到某个数据时，实际x坐标在屏幕上的位置
                        float x = Math.round(displayPointCount * pixelPerPacket);

                        //如果x坐标值已经超出显示区域，则跳出循环，准备从头显示。
                        if (x > attrs.width) {
                            displayPointCount = 0;
                            lastPoint.x = attrs.xStart;
                            break;
                        }
                        //显示时需要加上边缘的偏移量
                        x += attrs.xStart;

                        float y = 0;

                        try {
                            //从阻塞队列中获得数据
                            int data;
                            if (isUsingFilter) {
                                //如果使用滤波，则从处理后的队列中读取
                                data = filterQueue.take();
                                //获取到信号量才能进行画图，保证滤波后与原始速度一致
                                sempaphore.acquire();
                            } else {
                                //如果不使用滤波，则从原始队列中读取
                                data = inQueue.take();
                            }
                            //向文件缓冲写入数据
                            if (bos != null && pointsCount > POINT_IGNORE) {
                                bos.write(data);
                            }
                            //Y坐标需要重新定位到Android以左上角为原点的坐标系 （如果电极接反，会出现R波向下的情况）
                            //y = attrs.yEnd - (float) (data * pixelPerNumber * yScale) + offset;
                            //将电压中值至于网格中央时，波形一直在下方，因此还是采用底部在下方的方式
                            y = attrs.yEnd - (float) (data * pixelPerNumber * yScale);
                        } catch (Exception e) {
                            Logger.e(TAG, e);
                        }

                        if (lastPoint.x != 0 || lastPoint.y != 0) {
                            path.moveTo(lastPoint.x, lastPoint.y);
                            path.lineTo(x, y);
                        }

                        lastPoint.x = x;
                        lastPoint.y = y;

                        pointsCount++;
                        displayPointCount++;

                        //只有当收集到定义数量的数据时，才向文件写入，提升性能
                        if (bos != null && pointsCount % POINTS_PER_WRITE == 0
                                && pointsCount > POINT_IGNORE) {
                            try {
                                bos.flush();
                            } catch (IOException e) {
                                Logger.e(TAG, e);
                            }
                        }
                    }
                    canvas.drawPath(path, paint);
                    surfaceHolder.unlockCanvasAndPost(canvas);
                }
            }

            if (bos != null) {
                try {
                    //退出时写入剩余数据，关闭流输出
                    bos.flush();
                    bos.close();
                    bos = null;
                } catch (IOException e) {

                    Logger.e(TAG, e);
                }
            }
            Logger.i(TAG, "绘制心电图进程已退出。");
        }
    }

    //清楚界面信息
    public void clearView()
    {
       Canvas lockCanvas = surfaceHolder.lockCanvas();
        if (lockCanvas != null) {
            //清理锁定区域
            Paint clearPaint=new Paint();
            clearPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
            lockCanvas.drawPaint(clearPaint);
        }
        surfaceHolder.unlockCanvasAndPost(lockCanvas);
    }

    public boolean isRunning() {
        return isRunning;
    }
}
