/*
 * Copyright (C) 2012 Capricorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.capricorn;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/**
 * A custom view that looks like the menu in <a href="https://path.com">Path
 * 2.0</a> (for iOS).
 * 
 * @author Capricorn
 * 
 */
public class ArcMenu extends RelativeLayout {
	private ArcLayout mArcLayout;
	private ImageView mHintView;
	private ImageView circleNormal;
	private ImageView mHintViewJun;

	private int normalIconId;

	public ImageView getHintView() {
		return mHintView;
	}

	public void close() {
		mArcLayout.close(true, circleNormal);
	}

	//状态改变
	public void switchState() {
		mArcLayout.switchState(circleNormal);
	}

	//状态改变
	public void switchState(int index) {
		mArcLayout.switchState(circleNormal,index,mHintView,mHintViewJun);
	}

	public void setNormalIcon(int id) {
		normalIconId = id;
		mHintView.setImageResource(normalIconId);


	}

	public ArcMenu(Context context) {
		super(context);
		init(context);
	}

	public ArcMenu(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
		applyAttrs(attrs);
	}

	private void init(Context context) {
		LayoutInflater li = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		li.inflate(R.layout.arc_menu, this);

		mArcLayout = (ArcLayout) findViewById(R.id.item_layout);

		mHintView = (ImageView) findViewById(R.id.control_hint);
		circleNormal = (ImageView) findViewById(R.id.img_circle_normal);
		mHintViewJun=(ImageView)findViewById(R.id.control_hint_jun);
	}

	private void applyAttrs(AttributeSet attrs) {
		if (attrs != null) {
			TypedArray a = getContext().obtainStyledAttributes(attrs,
					R.styleable.ArcLayout, 0, 0);

			float fromDegrees = a.getFloat(R.styleable.ArcLayout_fromDegrees,
					ArcLayout.DEFAULT_FROM_DEGREES);
			float toDegrees = a.getFloat(R.styleable.ArcLayout_toDegrees,
					ArcLayout.DEFAULT_TO_DEGREES);
			mArcLayout.setArc(fromDegrees, toDegrees);

			int defaultChildSize = mArcLayout.getChildSize();
			int newChildSize = a.getDimensionPixelSize(
					R.styleable.ArcLayout_childSize, defaultChildSize);
			mArcLayout.setChildSize(newChildSize);

			a.recycle();
		}
	}

	public void addItem(View item, OnClickListener listener) {
		mArcLayout.addView(item);
		item.setOnClickListener(getItemClickListener(listener));
	}

	private OnClickListener getItemClickListener(final OnClickListener listener) {
		return new OnClickListener() {

			@Override
			public void onClick(final View viewClicked) {
//				Animation animation = bindItemAnimation(viewClicked, true, 400);
//				animation.setAnimationListener(new AnimationListener() {
//
//					@Override
//					public void onAnimationStart(Animation animation) {
//
//					}
//
//					@Override
//					public void onAnimationRepeat(Animation animation) {
//
//					}
//
//					@Override
//					public void onAnimationEnd(Animation animation) {
//						itemDidDisappear();
//					}
//				});

//				final int itemCount = mArcLayout.getChildCount();
//				for (int i = 0; i < itemCount; i++) {
//					View item = mArcLayout.getChildAt(i);
//					if (viewClicked != item) {
//						bindItemAnimation(item, false, 300);
//					}
//				}
//
//				mArcLayout.invalidate();
//				circleNormal.invalidate();

				if (listener != null) {
					listener.onClick(viewClicked);
				}
			}
		};
	}

	private Animation bindItemAnimation(final View child,
			final boolean isClicked, final long duration) {
		Animation animation = createItemDisapperAnimation(duration, isClicked);
		child.setAnimation(animation);
		child.setVisibility(View.INVISIBLE);
		return animation;
	}

	private void itemDidDisappear() {
		final int itemCount = mArcLayout.getChildCount();
		for (int i = 0; i < itemCount; i++) {
			View item = mArcLayout.getChildAt(i);
			item.clearAnimation();
		}
		mArcLayout.close(true, circleNormal);
		circleNormal.clearAnimation();
	}

	private static Animation createItemDisapperAnimation(final long duration,
			final boolean isClicked) {
		AnimationSet animationSet = new AnimationSet(false);
		animationSet.addAnimation(new ScaleAnimation(1.0f, isClicked ? 2.0f
				: 0.0f, 1.0f, isClicked ? 2.0f : 0.0f,
				Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
				0.5f));
		animationSet.addAnimation(new AlphaAnimation(1.0f, 0.0f));

		animationSet.setDuration(duration);
		animationSet.setFillAfter(true);

		return animationSet;
	}

}
