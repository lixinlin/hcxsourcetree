package com.jiudaifu.moxademo.activity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jiudaifu.moxademo.R;
import com.jiudaifu.moxademo.utils.Size;

/**
 * author: mojinshu
 * date: 2018/8/21 下午4:48
 * description:
 */
public class ActionBarActivity extends AppCompatActivity {

    private View mTitleBar;
    private View mBack;
    private LinearLayout mActionContainer;
    private TextView mTitleView;

    private FrameLayout mContentContainer;

    protected Size mScreenSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.setContentView(R.layout.moxa_activity_base);
        initViews();
        initSize();
        setListeners();

    }

    protected void showToast(String msg) {
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
    }

    private void initSize() {
        WindowManager wm = getWindow().getWindowManager();
        DisplayMetrics dm = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(dm);

        mScreenSize = new Size();
        mScreenSize.with = dm.widthPixels;
        mScreenSize.height = dm.heightPixels;

    }

    protected View getTitleBar() {
        return mTitleBar;
    }

    private final void initViews(){
        mTitleBar = findViewById(R.id.title_bar);
        mTitleView = (TextView) findViewById(R.id.moxa_title);
        mBack = findViewById(R.id.moxa_back);
        mActionContainer = (LinearLayout) findViewById(R.id.action_container);
        mContentContainer = (FrameLayout) findViewById(R.id.moxa_content);

        onCreateActionMenu(mActionContainer);

    }

    protected boolean onCreateActionMenu(LinearLayout actionMenu) {
        return false;
    }

    protected void onBackClick(){
        ActionBarActivity.this.finish();
    }

    private void setListeners(){
        mBack.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                onBackClick();
            }
        });
    }

    @Override
    public void setContentView(int layoutResID) {
        View content = getLayoutInflater().inflate(layoutResID, null);
        mContentContainer.addView(content);
    }

    @Override
    public void setContentView(View view) {
        //do nothing
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        //do nothing
    }

    //==========Override methods for set custom title==========
    @Override
    public void setTitle(CharSequence title) {
        mTitleView.setText(title);
    }

    @Override
    public void setTitle(int titleId) {
        mTitleView.setText(titleId);
    }
}
