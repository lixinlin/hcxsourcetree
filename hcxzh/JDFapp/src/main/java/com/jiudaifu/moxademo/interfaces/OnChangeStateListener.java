package com.jiudaifu.moxademo.interfaces;

/**
 * author: mojinshu
 * date: 2018/8/20 下午2:26
 * description: 改变温度与时间信息监听器
 */
public interface OnChangeStateListener {
    /**
     *
     * @param temp  温度
     * @param time	时间
     * @param tempForAll	是否同时应用到其他通道,true表示同时应用,否则表示只应用到某一通道
     * @param timeForAll	是否同时应用到其他通道,true表示同时应用,否则表示只应用到某一通道
     */
    void onChangeState(int which, int temp, int time, boolean tempForAll, boolean timeForAll);
}
