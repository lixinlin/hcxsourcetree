package com.jiudaifu.moxademo.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.jiudaifu.moxademo.R;
import com.jiudaifu.moxademo.adapter.ChannelAdapter;
import com.jiudaifu.moxademo.interfaces.OnChangeStateListener;
import com.jiudaifu.moxademo.jiuliaoData.ActiviteInfo;
import com.jiudaifu.moxademo.jiuliaoData.ErrorLog;
import com.jiudaifu.moxademo.jiuliaoData.I9DataHttp;
import com.jiudaifu.moxademo.jiuliaoData.I9DataJDF;
import com.jiudaifu.moxademo.utils.Utils;
import com.jiudaifu.moxademo.view.DeviceGuideView;
import com.jiudaifu.moxademo.view.GridDividerItemDecoration;
import com.jiudaifu.moxademo.view.MoxaClueDialog;
import com.jiudaifu.moxademo.view.NormalDialog;
import com.jiudaifu.moxademo.view.PasswdInputDialog;
import com.jiudaifu.moxademo.view.RecyclerItemTouchDelegate;
import com.jiudaifu.moxademo.view.TopStateView;
import com.jiudaifu.moxademo.view.WenDuWarnningDialog;
import com.jiudaifu.moxademo.view.WheelControlDialog;
import com.telink.bluetooth.light.ConnectionStatus;
import com.telink.bluetooth.light.LightPeripheral;
import com.telink.ibluetooth.expose.ChannelsPresenter;
import com.telink.ibluetooth.expose.MeshClient;
import com.telink.ibluetooth.interfaces.ChannelsContract;
import com.telink.ibluetooth.model.Channel;
import com.telink.ibluetooth.model.Channels;
import com.telink.ibluetooth.model.MoxaBleComm;
import com.telink.ibluetooth.sdk.Configuration;
import com.telink.ibluetooth.sdk.MeshAddressRepository;
import com.telink.ibluetooth.sdk.MoxaModule;
import com.telink.ibluetooth.utils.AndroidUtils;
import com.telink.ibluetooth.utils.MoxaUtils;
import com.telink.ibluetooth.utils.VoiceUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;
import pub.devrel.easypermissions.PermissionRequest;

public class MainActivityJDF extends AppCompatActivity implements TopStateView.TopStateListener,EasyPermissions.PermissionCallbacks,
        RecyclerItemTouchDelegate.OnItemClickListener,ChannelsContract.View{

    private static final String TAG = "MainActivityJDF";

    private static final String[] LOCATION_PERMISSIONS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION
    };

    //用于观察蓝牙开始的状态，反正用户拒绝蓝牙无法刷新页面
    final static int MESSAGE_TIME = 0x101F;
    final static int TIMER_PERIOD = 5000;

    private static final int REQ_SETTING = 10000;
    private static final String TAG_FIRSTCOUNT = "TAG_FIRSTCOUNT";
    private static final int REQUEST_SCAN = 102;
    private static final int NUM_COLUMNS = 2;

    private static final int RC_LOCATION_PERMISSIONS = 12302;
    private static final int REQUEST_CODE_LOCATION_SETTINGS = 1101;


    public static int HIGHEST_TIME = 0;//用于记录设置灸疗的最高时间 add by mojinshu
    private Map<Integer,Integer> firstSetTime = new HashMap<Integer, Integer>(); //第一次点击的灸疗时间 add by mojinshu
    private int firstCount = 0;//第一次不执行全部暂停 add by mojinshu
    private boolean isReConnect = false; //是否是蓝牙重连 add by mojinshu
    private boolean hasStart = false;//是否已经开始灸疗 add by mojinshu
    private int reUploadCount = 0; //蓝牙重连上传灸头的数量 add by mojinshu

    private boolean isScanWork = true; //蓝牙是否能正常扫描
    private int notScanCount = 0; //未扫描到设备次数
    private boolean isShowControlView = true;//标志当前灸疗效控制模式还是灸疗选择模式
    private boolean mIsSwitchOn = true;
    private boolean channelIsAllOpen = false; //所有的灸头是否开启
    private int mRecipelId = 0;           // 当前处方ID
    private String mDefaultTempTimePair = null; //格式:温度-时间
    private int mCurrIndex = -1; //-1表示当前没有灸头被选中

    private ObserveBLEStatusHandler observeBLEStatusHandler = null;
    private ViewTreeObserver.OnGlobalLayoutListener mLayoutObserver;
    private ToolbarClickListener mListener;
    private Context mContext;
    private PowerManager.WakeLock mWakeLock = null;
    private ChannelAdapter mChannelAdapter;
    private ChannelsContract.Presenter mPresenter;
    private VoiceUtils mVoiceUtils;
    private Configuration mAppConfig;

    private TopStateView mTopStateView;
    private DeviceGuideView mDeviceGuideView;//没有找到I9时的提示
    private LinearLayout layoutControl;
    private LinearLayout childHighTempHint;//儿童高温提醒
    private TextView mOneKeySwitch;
    private TextView mEmptyView;
    private TextView moxaScheme;
    private TextView moxaRecord;
    private RecyclerView mChannelGrid;
    private PasswdInputDialog mPasswdDialog;
    private View root;
    private Dialog mWarnningDialog;
    private Dialog mBluetoothErrorDialog;
    private int userid;
    private SharedPreferences sp;
    private String deviceName = "";

    class ObserveBLEStatusHandler extends Handler{
        @Override
        public void handleMessage(Message msg) {
            if(msg.what == MESSAGE_TIME){
                if(mPresenter != null && !mPresenter.isBluetoothEnable()){
                    doSomethingOnConnectFail();
                }
            }
        }
    }

    private class ToolbarClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            final int id = v.getId();
            if(id == R.id.one_key_on_off){
                oneKeySwitchClick();
            }
        }
    }

    private class ChangeStateListener implements OnChangeStateListener {

        @Override
        public void onChangeState(int which, int temp, int time, boolean tempForAll, boolean timeForAll) {
            //判断当前设置的时间是否是最长的时间 add by mojinshu
            if(time > HIGHEST_TIME){
                HIGHEST_TIME = time;
            }
            if (!MeshClient.getInstance().isLogin()) {
                MoxaClueDialog.ShowMsgForClue(MainActivityJDF.this, getResources().getString(R.string.moxa_first_link_jiudaifu_hint));
                return ;
            }
            boolean playSound = false;
            final List<Channel> channels = Channels.getInstance().get();
            for (int i = 0;i < channels.size(); i++) {
                Channel channel = channels.get(i);
                if (channel.getConnectionStatus() == ConnectionStatus.OFFLINE) {
                    continue;
                }
                if (i == which /*&& mSettingDialog.canSetTemp()*/) {
                    playSound = true;
                }
                if (channel.isWorking()) {
                    //设置温度
                    if ((tempForAll || which == i) && (temp != channel.getTargetState().getTemp())
                        /*&& mSettingDialog.canSetTemp()*/) {
                        channel.getTargetState().setTemp(temp);
                        mPresenter.setTemp(channel);
                    }
                    //设置时间
                    if ((timeForAll || which == i) && (time != channel.getTargetState().getTimeSeconds())
                        /*&& mSettingDialog.canSetTime()*/) {
                        channel.getTargetState().setTimeMilliseconds(time * 1000);
                        mPresenter.setTime(channel);
                    }
                } else {
                    //开启灸头
                    if (tempForAll || timeForAll || which == i) {
                        if ((tempForAll || which == i)/* && mSettingDialog.canSetTemp()*/) {
                            channel.getTargetState().setTemp(temp);
                        }
                        if ((timeForAll || which == i) /*&& mSettingDialog.canSetTime()*/) {
                            channel.getTargetState().setTimeMilliseconds(time * 1000);
                        }
                        //时间为0,不启动
                        if (channel.getTargetState().getTimeMilliseconds() == 0) {
                            continue;
                        }
                        channel.getTargetState().setState(Channel.State.WORKING);
                        channel.setLastCountDownTime(SystemClock.elapsedRealtime());
                        mPresenter.openChannel(channel);
                    }
                }
                refreshItem(i);
//              提交灸疗原始数据
                sumbitI9Data(channel,userid);

//              在此处判断是否已上传过灸头激活信息
                deviceName = channel.getDeviceName();
                String name = sp.getString("deviceName", "null");
                if(name.equals("null")){
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putString("deviceName", deviceName);
                    editor.apply();submitActivitedInfo(deviceName);
//                    上传激活信息

                }

            }

            if (tempForAll||timeForAll){
                //更新一键开/一键关的文字
                channelIsAllOpen = true;
                updateSwitchText();
            }

            if (temp >= VoiceUtils.WARNNING_WENDU && playSound) {
                mVoiceUtils.startWarnningVoice(temp);
                showWarnningDialog(mCurrIndex,temp);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(null != savedInstanceState){
            firstCount = savedInstanceState.getInt(TAG_FIRSTCOUNT);
        }
        mPresenter = new ChannelsPresenter(this.getApplication(),this,this);
        MeshAddressRepository.getInstance().loadAddress();

        mContext = MainActivityJDF.this;
        setWakeMode(mContext, PowerManager.SCREEN_BRIGHT_WAKE_LOCK);
        setContentView(R.layout.activity_main_jdf);

        Intent intent = this.getIntent();
        userid = intent.getIntExtra("userid",0);

        init();
        sp = this.getSharedPreferences("ijiuinfo", Context.MODE_PRIVATE);
        Log.d(TAG, "onCreate: ");

    }
    private void submitActivitedInfo(String deviceSn){
        ActiviteInfo activiteInfo = new ActiviteInfo();
        activiteInfo.setActiviteStatus("activited");
        activiteInfo.setActiviteTime(System.currentTimeMillis()+"");
        activiteInfo.setDeviceSn(deviceSn);
        activiteInfo.setMember(userid+"");
        EventBus.getDefault().post(activiteInfo);
    }
    private void sumbitI9Data(Channel channel, int userid) {
        I9DataHttp i9Data = new I9DataHttp();
        i9Data.setMember(userid+"");
        i9Data.setDeviceSn(channel.getDeviceName());
        i9Data.setNum(channel.getNumber()+"");
        long start = System.currentTimeMillis();
        i9Data.setStarTime(start);
        int timeSeconds = channel.getTargetState().getTimeSeconds();
        i9Data.setDuration(timeSeconds);
        i9Data.setEndtime(start+timeSeconds*1000);
        i9Data.setTemperature(channel.getTargetState().getTemp());
        EventBus.getDefault().post(i9Data);
    }

//    private void sumbitI9Dataceshi() {
//        I9DataHttp i9Data = new I9DataHttp();
//        i9Data.setMember(userid+"");
//        i9Data.setDeviceSn("E000113");
//        i9Data.setNum(5+"");
//        long start = System.currentTimeMillis();
//        i9Data.setStarTime(start);
//        int timeSeconds = 60;
//        i9Data.setDuration(timeSeconds);
//        i9Data.setEndtime(start+timeSeconds*1000);
//        i9Data.setTemperature(39);
//        EventBus.getDefault().post(i9Data);

//    }

    private void submitDateForJDF(String info,String name,String memberid) {
        I9DataJDF i9Data = new I9DataJDF();
        i9Data.setDevice_name(name);
        i9Data.setMember(memberid);
        i9Data.setMoxa_info(info);
        EventBus.getDefault().post(i9Data);
    }
    private void submitError(String memberid,String name,int tpye,String exceDesc) {
        ErrorLog errorLog = new ErrorLog(memberid,name,tpye+"",System.currentTimeMillis()+"",exceDesc);
        EventBus.getDefault().post(errorLog);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(TAG_FIRSTCOUNT,firstCount);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAppConfig.read(MainActivityJDF.this);
        mChannelAdapter.notifyDataSetChanged();
        presenterStart();
//        测试
//        sumbitI9Dataceshi();
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseWakeMode();

        if (observeBLEStatusHandler != null) {
            observeBLEStatusHandler.removeMessages(MESSAGE_TIME);
            observeBLEStatusHandler = null;
        }

        AndroidUtils.dismissDialog(mWarnningDialog);
        AndroidUtils.dismissDialog(mPasswdDialog);
        AndroidUtils.dismissDialog(mBluetoothErrorDialog);

        mPresenter.end();

        mVoiceUtils.stopAllVoice();

        MeshAddressRepository.getInstance().saveAddress();

        firstCount = 0;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_SETTING && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                String action = data.getAction();
                if (MoxaModule.ACTION_MODIFY_PSWD.equals(action)) {

                }else if (MoxaModule.ACTION_FORGOT_PSWD.equals(action)) {

                    LightPeripheral peripheral = MeshClient.getInstance().getPeripheral();
                    if (peripheral != null) {
                        showPwdDialog(peripheral.getMeshNameStr());
                    }
                }
            }
        }else if (requestCode == REQUEST_SCAN){
            mPresenter.onResult(requestCode,resultCode,data);
        }
    }


    @Override
    public void setPresenter(ChannelsContract.Presenter presenter) {

    }

    @Override
    public void onItemDown(View view, int position) {

    }

    @Override
    public void onItemClick(View view, int position) {
        if(true){
            showWheelControlPop(position);
            return;
        }

        channelChange(position);
    }

    @Override
    public void onItemLongClick(View view, int position) {
        if (mCurrIndex != position) {
            setCurrentIndex(position);
        }
        Channel channel = Channels.getInstance().get(position);

        if (channel.getConnectionStatus() != ConnectionStatus.OFFLINE) {
            for (int i=0;i<3;i++){
                mPresenter.vibrateChannel(channel);
            }
        }
    }

    @Override
    public void onSwitchClick(View view, int position) {
        channelChange(position);
    }



    /**
     * date： 2018/8/20 上午11:54
     * description：蓝牙设备登录失败隐藏控制界面
     * param：[]
     * return：void
     */
    private void doSomethingOnConnectFail(){
        isShowControlView = false;
        mTopStateView.hideProgressBar();
        mTopStateView.setBlueState(getString(R.string.ununited));
    }

    /**
     * 蓝牙设备登录成功则显示控制界面
     */
    private void doSomethingOnConnectSuccess(String msg){
        if (observeBLEStatusHandler != null) {
            observeBLEStatusHandler.removeMessages(MESSAGE_TIME);
            observeBLEStatusHandler = null;
        }
        isShowControlView = true;
        isShowGuide(false);
        presenterStart();
        showLoadingIndicator(false,msg);
    }



    /**
     * date： 2018/8/15 下午5:57
     * description：初始化
     * param：[]
     * return：void
     */
    private void init() {
        root = LayoutInflater.from(mContext).inflate(R.layout.activity_main_jdf,null);
        mVoiceUtils = new VoiceUtils(this);
        mAppConfig = new Configuration();
        mAppConfig.read(this);
        mVoiceUtils.setAppConfig(mAppConfig);

        mDeviceGuideView = findViewById(R.id.layout_guide);
        layoutControl = findViewById(R.id.layout_control);

        mTopStateView = findViewById(R.id.topstate_view);
        mTopStateView.setTopStateListener(this);
        mListener = new ToolbarClickListener();

        mOneKeySwitch = findViewById(R.id.one_key_on_off);
        mOneKeySwitch.setOnClickListener(mListener);

        moxaScheme = findViewById(R.id.btn_moxa_scheme);
        moxaRecord = findViewById(R.id.btn_moxa_record);

        moxaScheme.setOnClickListener(mListener);
        moxaRecord.setOnClickListener(mListener);
        mDeviceGuideView.setOnGuideTextClickListener(buyDeviceListener);

        childHighTempHint = (LinearLayout)findViewById(R.id.layout_child_high_temp_hint);

        //初始化开关按钮状态
        setSwitchOn(mIsSwitchOn);

        mEmptyView = findViewById(R.id.empty_view);
        mChannelGrid = findViewById(R.id.home_channel_grid);
        mChannelGrid.setLayoutManager(new GridLayoutManager(getContext(), NUM_COLUMNS));
        mChannelGrid.addItemDecoration(new GridDividerItemDecoration(getContext(), NUM_COLUMNS));
        mChannelGrid.setItemAnimator(null);
        mChannelAdapter = new ChannelAdapter(MainActivityJDF.this);
        mChannelGrid.setAdapter(mChannelAdapter);

        RecyclerItemTouchDelegate clickListener = new RecyclerItemTouchDelegate(getContext(), mChannelGrid, this);
        mChannelGrid.addOnItemTouchListener(clickListener);

        registserLayoutObserver();
        //显示儿童高温提醒
        childHighTempHint.setVisibility(View.VISIBLE);

        mTopStateView.setLampState(true);
        registserLayoutObserver();
        mPresenter.start();
    }

    private View.OnClickListener buyDeviceListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //TODO
        }
    };

    /**
     * date： 2018/8/15 下午6:23
     * description：开关切换
     * param：[enable]
     * return：void
     */
    public void setSwitchOn(boolean enable) {
        mIsSwitchOn = enable;
    }


    /**
     * date： 2018/8/15 下午5:57
     * description：捕获唤醒锁,防止锁屏
     * param：[context, mode]
     * return：void
     */
    protected void setWakeMode(Context context, int mode) {
        if (mWakeLock != null) return;
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(mode, MainActivityJDF.class.getName());
        mWakeLock.acquire();
    }


    /**
     * date： 2018/8/15 下午5:58
     * description：释放唤醒锁
     * param：[]
     * return：void
     */
    protected void releaseWakeMode() {
        if (mWakeLock != null) {
            mWakeLock.release();
            mWakeLock = null;
        }
    }

    private AlertDialog.Builder builder = null;
    /**
     * date： 2018/8/15 下午5:58
     * description：提示用户去设置位置信息弹框
     * param：[]
     * return：void
     */
    private void showSetLocationServiceDialog(){
        if(builder == null){
            builder = new AlertDialog.Builder(mContext);
            builder.setTitle(R.string.set_location_service_dialog_title_text)
                    .setMessage(R.string.set_location_service_dialog_msg_text)
                    .setPositiveButton(R.string.set_location_service_dialog_pbtn_text, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            setLocationService();
                        }
                    })
                    .setNegativeButton(R.string.set_location_service_dialog_nbtn_text, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
        }
        builder.show();
    }

    /**
     * date： 2018/8/15 下午5:59
     * description：申请打开定位服务
     * param：[]
     * return：void
     */
    private void setLocationService() {
        Intent locationIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        this.startActivityForResult(locationIntent, REQUEST_CODE_LOCATION_SETTINGS);
    }

    /**
     * date： 2018/8/15 下午5:59
     * description：申请定位权限
     * param：[]
     * return：void
     */
    @AfterPermissionGranted(RC_LOCATION_PERMISSIONS)
    protected void requestLocationPermission() {
        try {
            if (Build.VERSION.SDK_INT >= 23) {
                if (hasLocationPermissions()) {
                    //继续执行其他操作
                    if(!Utils.isLocationEnable(mContext) && !isScanWork){
                        showSetLocationServiceDialog();
                        isScanWork = true;
                    }
                } else {
                    // 申请定位权限
                    EasyPermissions.requestPermissions(
                            new PermissionRequest.Builder(this, RC_LOCATION_PERMISSIONS, LOCATION_PERMISSIONS)
                                    .setRationale(R.string.request_permission_rationale_tips_text)
                                    .setPositiveButtonText(R.string.request_permission_rationale_pbtn_text)
                                    .setNegativeButtonText(R.string.request_permission_rationale_nbtn_text)
                                    .build());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean hasLocationPermissions() {
        return EasyPermissions.hasPermissions(mContext,LOCATION_PERMISSIONS);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if(EasyPermissions.somePermissionPermanentlyDenied(this,perms)){ //不在询问并拒绝
            new AppSettingsDialog.Builder(this)
                    .setTitle(R.string.app_setting_dialog_title_text)
                    .setRationale(R.string.app_setting_dialog_rationale_text)
                    .setPositiveButton(R.string.app_setting_dialog_pbtn_text)
                    .setNegativeButton(R.string.app_setting_dialog_nbtn_text)
                    .build()
                    .show();
        }
    }

    @Override
    public void clickBlueStateText() {
        requestLocationPermission();
        mPresenter.onTitleClicked();
    }

    @Override
    public void onHeadClicked(View v) {

    }

    @Override
    public void clickSettingBtn() {
        Intent intent = new Intent();
        intent.setClass(MainActivityJDF.this, SettingActivity.class);
        startActivityForResult(intent, REQ_SETTING);
    }

    @Override
    public void onLampStateChange(boolean turnOn) {

    }

    @Override
    public void onClickLeftView(View view) {
        MainActivityJDF.this.finish();
    }

    @Override
    public Context getContext() {
        return MainActivityJDF.this;
    }

    @Override
    public boolean getDefaultSetting(int[] data) {
        if(!TextUtils.isEmpty(mDefaultTempTimePair)){
            String[] pair = mDefaultTempTimePair.split("-");
            if (!MoxaUtils.I9DeviceIsWorking()){
                data[0] = MoxaBleComm.defaultTemp;
            }else {
                data[0] = toInt(pair[0], MoxaBleComm.defaultTemp);
            }
            data[1] = toInt(pair[1], MoxaBleComm.defaultTime/60)*60;
            return true;
        }
        return false;
    }

    @Override
    public void showLoadingIndicator(boolean showProgress, int msgId) {
        showLoadingIndicator(showProgress,getString(msgId));
    }

    @Override
    public int getCurrentIndex() {
        return mCurrIndex;
    }

    @Override
    public void setCurrIndex(int index) {
        mCurrIndex = index;
    }

    @Override
    public void showConnecting() {
        showLoadingIndicator(true,R.string.moxa_connecting_ble_hint);
    }

    @Override
    public void showConnectSuccess(String msg) {
        showLoadingIndicator(false,msg);
        if (isShowControlView){
            mVoiceUtils.moxaStartVoice();
        }
        presenterStart();
        doSomethingOnConnectSuccess(msg);
    }

    @Override
    public void showConnectFail() {
        doSomethingOnConnectFail();
    }

    @Override
    public void showMeshError() {
        if (mBluetoothErrorDialog != null && mBluetoothErrorDialog.isShowing()) return;
        mBluetoothErrorDialog = new android.app.AlertDialog.Builder(this)
                .setTitle(R.string.warm_prompt)
                .setMessage(R.string.restart_bluetooth)
                .setCancelable(false)
                .setNeutralButton(R.string.btn_confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mBluetoothErrorDialog.dismiss();
                        mPresenter.restartBluetooth();
                    }
                }).create();
        mBluetoothErrorDialog.show();
    }

    @Override
    public void showMeshOffline() {

    }

    @Override
    public void showNoChannels() {

    }

    @Override
    public void showMoxaOverView() {
        isReConnect = false;
        doMoxaOver();
        //清空临时数据 add by mojinshu
        firstSetTime.clear();
        resetTempOnConnect();
        channelIsAllOpen = false;
        updateStatus();
    }

    @Override
    public void showEmptyView(int msgId) {
        mChannelGrid.setVisibility(View.GONE);
        mEmptyView.setVisibility(View.VISIBLE);
        mEmptyView.setText(msgId);
    }

    @Override
    public void showPwdDialog(final String meshName) {
        if (mPasswdDialog != null && mPasswdDialog.isShowing()){
            return;
        }
        mPasswdDialog = PasswdInputDialog.create(getContext())
                .setMessage(meshName)
                .setPositiveListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mPasswdDialog.dismiss();
                        boolean connect = mPresenter.connect(mPasswdDialog.getInputPasswd());
                        if(connect){
//                            请输入AJi9-13169的密码
                            String substring = meshName.substring(3, 13);
                            ActiviteInfo activiteInfo = new ActiviteInfo();
                            activiteInfo.setActiviteStatus("activited");
                            activiteInfo.setActiviteTime(System.currentTimeMillis()+"");
                            activiteInfo.setDeviceSn(substring);
                            activiteInfo.setMember(userid+"");
                            EventBus.getDefault().post(activiteInfo);


                        }
                    }
                })
                .setNeutralListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getPassword();
                    }
                })
                .setNegativeListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showLoadingIndicator(false,R.string.moxa_nolink_ble_hint);
                        postDelayedReStartAutoConnect();
                    }
                });
        if(null != MainActivityJDF.this && !MainActivityJDF.this.isFinishing()){
            mPasswdDialog.show();
        }
    }

    @Override
    public void dimissPwdDialog() {
        if(mPasswdDialog != null && mPasswdDialog.isShowing()){
            mPasswdDialog.dismiss();
        }
    }

    @Override
    public void turnSwitch(boolean isOn) {
        setSwitchOn(isOn);
    }

    @Override
    public void enableActions(boolean enable) {
        mTopStateView.setBlueState(getString(R.string.moxa_bt_manage));
        mTopStateView.hideProgressBar();
        mTopStateView.getBleStateView().setEnabled(enable);
    }

    @Override
    public void refreshAll() {
        mChannelAdapter.notifyDataSetChanged();
    }

    @Override
    public void refreshItem(int position) {
        mChannelAdapter.notifyItemChanged(position);
        if (position == mCurrIndex) {
            updateChannelInfo(position);
        }
        //蓝牙重连后, 开始添加新的灸疗数据
        if(null != Channels.getInstance()
                && Channels.getInstance().size() > 0
                && position < Channels.getInstance().size()){
            Channel channel = Channels.getInstance().get(position);
            if(channel.isWorking()){
                hasStart = true;
            }
            //蓝牙重连
            if(isReConnect && channel != null && hasStart && firstCount > 0){
                //蓝牙断开连接重连后灸头开始默认是全部停止工作的状态，需firstCount > 0判断
                int temp = channel.getTargetState().getTemp();
                int time = channel.getTargetState().getTimeSeconds();
                int linkedCount = Channels.getInstance().size();
                int channelTag = channel.getNumber();
                reUploadCount++;
                if(reUploadCount == Channels.getInstance().size()){
                    isReConnect = false;
                    reUploadCount = 0;
                }
            }
        }
    }

    @Override
    public void pauseAll() {
        resetTempOnConnect();
    }

    @Override
    public void waitingStart() {

    }

    @Override
    public void disconnectAll() {
        /**
         * 蓝牙断开时当做全部暂停灸头情况处理 add by mojinshu
         * 全部灸头暂停工作
         */
        isReConnect = false;
        firstCount = 0;//防止再次连接蓝牙已启动就全部暂停
    }

    @Override
    public void connectAll() {
        //蓝牙重连
        isReConnect = true;
    }

    @Override
    public void startScanUI() {
        Intent intent = new Intent(MainActivityJDF.this,DeviceScanActivity.class);
        startActivityForResult(intent,REQUEST_SCAN);
    }

    @Override
    public void reStart() {
        reStartAutoConnect();
    }

    @Override
    public void reScanRecord() {
        notScanCount ++;
        if(notScanCount > 5){
            isScanWork = false;
            notScanCount = 0;
        }
        requestLocationPermission();
    }

    @Override
    public void showChannelsInfo(String s) {
        submitDateForJDF(s,deviceName,userid+"");
    }

    @Override
    public void showBatteryException(int i) {
        submitError(userid+"",deviceName,i,"电池异常");
    }

    @Override
    public void showTempException(int i) {
        submitError(userid+"",deviceName,i,"温度异常");
    }


    /**
     * 延时一段时间后自动重连
     */
    private void postDelayedReStartAutoConnect(){
        new Handler().postDelayed(new Runnable(){
            public void run() {
                reStartAutoConnect();
            }
        }, 2000);   //2秒

    }

    /**
     * 重新自动连接
     */
    private void reStartAutoConnect(){
        if(null != mPresenter){
            mPresenter.end();
            MeshAddressRepository.getInstance().saveAddress();
            firstCount = 0;
            mPresenter = null;
            mPresenter = new ChannelsPresenter(this.getApplication(),getContext(),this);
            mPresenter.start();
        }
    }

    private void getPassword(){
        Intent intent = new Intent();
        intent.setClass(getContext(),ForgotPasswdActivity.class);
        startActivity(intent);
    }

    private void updateStatus(){
        updateSwitchText();
        mChannelAdapter.notifyDataSetChanged();
    }

    /**
     * 重新设置温度为最低的温度（起因源于儿童使用时被烫伤了）add by xq @2017-12-05
     */
    private void resetTempOnConnect() {
        List<Channel> channels = Channels.getInstance().get();
        if (MoxaUtils.I9DeviceIsWorking()){
            return;
        }
        for (Channel channel:channels){
            Channel.Info targetState = channel.getTargetState();
            int temp = targetState.getTemp();
            if (temp > MoxaBleComm.defaultTemp){
                targetState.setTemp(MoxaBleComm.defaultTemp);
                mPresenter.setTemp(channel);
            }
        }

    }

    /**
     * 灸疗结束做相应处理
     */
    protected void doMoxaOver() {
        mVoiceUtils.moxaStopVoice(getMoxaOverVoiceId());
    }

    protected int getMoxaOverVoiceId() {
        return R.raw.moxaend_i9;
    }





    /**
     * 一键开/一键关 的文本设置
     */
    protected void updateSwitchText() {
        if (channelIsAllOpen){
            mOneKeySwitch.setText(getString(R.string.one_key_close));
        }else {
            mOneKeySwitch.setText(getString(R.string.one_key_open));
        }
    }

    /**
     * 设置并显示当前选中的通道
     *
     * @param index
     */
    private void setCurrentIndex(int index) {
        if (Channels.getInstance().isEmpty()) return;
        if (index < 0 || index >= Channels.getInstance().size()) {
            return ;
        }
        if (mCurrIndex == index) return ;

        int oldIdx = mCurrIndex;
        mCurrIndex = index;

        if (oldIdx != -1) {
            mChannelAdapter.notifyItemChanged(oldIdx);
        }
        mChannelAdapter.notifyItemChanged(mCurrIndex);

        updateChannelInfo(index);

    }

    private void updateChannelInfo(int index) {
        final Channel channel = Channels.getInstance().get(index);
        final Channel.Info targetState = channel.getTargetState();

        String statusText ;
        int colorRes;

        if(channel.getConnectionStatus() == ConnectionStatus.OFFLINE){
            if (channel.getTargetState().getBattery() < 5) {
                statusText = getString(R.string.chl_status_low_battery);
                colorRes = R.color.warnning_text;
            } else {
                statusText = getString(R.string.chl_status_disconnect);
                colorRes = R.color.normal_text;
            }

        } else {
            if (targetState.getState() == Channel.State.WORKING) {
                if (targetState.getTemp() >= VoiceUtils.WARNNING_WENDU) {
                    statusText = getString(R.string.chl_status_high_temp);
                    colorRes = R.color.warnning_text;
                } else {
                    statusText = getString(R.string.chl_status_normal_temp);
                    colorRes = R.color.normal_text;
                }
            }else{
                statusText = getString(R.string.chl_status_stop);
                colorRes = R.color.normal_text;
            }
        }
        setStatusInfo(statusText,colorRes);
    }

    public void setStatusInfo(String msg, int colorRes) {
        if (mRecipelId != 0) {
            return;
        }
    }

    protected void oneKeySwitchClick() {
        //顶部的一键开/一键关按钮
        if (channelIsAllOpen){
            showAllChannelCloseHintDialog();
        }else {
            openAllChannel();
        }
    }

    private void showAllChannelCloseHintDialog(){
        final NormalDialog dialog = new NormalDialog(MainActivityJDF.this);
        dialog.setTitle(getString(R.string.dialog_title));
        dialog.setMsg(getString(R.string.sure_close_all_channel));
        dialog.setNegativeListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setPositiveListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeAllChannel();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void openAllChannel() {
        List<Channel> channels = Channels.getInstance().get();
        int lastTemp=0,lastIndex=0,maxTempIndex=0,maxTemp=0;
        for (int i=0;i<channels.size();i++){
            Channel channel = channels.get(i);
            Channel.Info info = channel.getTargetState();
            Channel.State state = info.getState();
            if (state != Channel.State.WORKING){
                //查找最高温度和对应的index
                int temp = info.getTemp();
                int time = info.getTimeSeconds(); //获取频道的时间，add by mojinshu
                if (temp >= VoiceUtils.WARNNING_WENDU ){
                    if (temp>lastTemp){
                        maxTempIndex = i;
                        maxTemp = temp;
                    }else {
                        maxTempIndex = lastIndex;
                        maxTemp = lastTemp;
                    }
                    lastTemp = temp;
                    lastIndex = i;
                }

                if (info.getTimeSeconds() == 0){
                    //设置时间为30分钟（30*60*1000）
                    info.setTimeMilliseconds(1800000);
                }
//todo   记录开启
//               本次设置的需要记录的时间
                int timeSeconds = info.getTimeSeconds();
                mPresenter.openChannel(channel);
            }
        }

        if ( maxTemp >= VoiceUtils.WARNNING_WENDU ){
            mVoiceUtils.startWarnningVoice(maxTemp);
            showWarnningDialog(maxTempIndex,maxTemp);
        }

        channelIsAllOpen = true;
        updateStatus();
    }

    /**
     * 关闭全部灸头
     */
    private void closeAllChannel(){
        List<Channel> channels = Channels.getInstance().get();
        int i = 0;
        for (Channel channel:channels){
            Channel.State state = channel.getTargetState().getState();
            Channel.Info info = channel.getTargetState();
            //查找最高温度和对应的index
            int temp = info.getTemp();
            int time = info.getTimeSeconds(); //获取频道的时间，add by mojinshu
            if (state == Channel.State.WORKING){
                mPresenter.closeChannel(channel);
            }
        }
        channelIsAllOpen = false;
        updateStatus();
//        todo 记录关闭

    }

    private void checkCurrentChannelStatus(){
        List<Channel> channels = Channels.getInstance().get();
        if (channels!=null && channels.size() == 0){
            return;
        }

        for (Channel channel:channels){
            Channel.State state = channel.getTargetState().getState();
            if (state != Channel.State.WORKING){
                channelIsAllOpen = false;
                updateStatus();
                return;
            }
        }
        channelIsAllOpen = true;
        updateStatus();
    }

    private void isShowGuide(boolean isShow){
        if (isShow){
            mDeviceGuideView.setVisibility(View.VISIBLE);
            layoutControl.setVisibility(View.GONE);
        }else {
            mDeviceGuideView.setVisibility(View.GONE);
            layoutControl.setVisibility(View.VISIBLE);
        }
    }

    private void isShowGuide(String msg){
        boolean flag = TextUtils.equals(msg,getString(R.string.moxa_nolink_ble_hint)) || TextUtils.equals(msg,getString(R.string.moxa_cannot_open_ble_hint));
        isShowGuide(flag);
    }

    private void presenterStart(){
        registserLayoutObserver();
        mPresenter.start();
        if(observeBLEStatusHandler == null){
            observeBLEStatusHandler = new ObserveBLEStatusHandler();
        }
        if(mPresenter != null && !mPresenter.isBluetoothEnable()){
            observeBLEStatusHandler.sendMessageDelayed(observeBLEStatusHandler.obtainMessage(MESSAGE_TIME), TIMER_PERIOD);
        }
    }

    private void registserLayoutObserver() {
        if (mLayoutObserver != null || mChannelGrid == null) return ;
        mLayoutObserver = new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {

                if (AndroidUtils.isJellyBeanOrLater()) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        mChannelGrid.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                }else {
                    mChannelGrid.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
                mLayoutObserver = null;

                int h = mChannelGrid.getHeight();
                int itemHeight = h/2 - (h/(2*10));
                mChannelAdapter.setItemHeight(itemHeight);
                Log.e(TAG, "grid h="+h+" item h=" + itemHeight);

            }
        };
        mChannelGrid.getViewTreeObserver().addOnGlobalLayoutListener(mLayoutObserver);
    }

    private void showWarnningDialog(final int position, int temp) {
        AndroidUtils.dismissDialog(mWarnningDialog);
        mWarnningDialog = new WenDuWarnningDialog(mContext, position, temp, new WenDuWarnningDialog.WenDuWarnningListener() {

            @Override
            public void resetWenDu(int no) {
                mVoiceUtils.stopWarnningVoice();
//                showSettingDialog(no,ChannelSettingDidalog.OP_MODE_TEMP);
                showWheelControlPop(position);
            }

            @Override
            public void confirmWenDu(int no) {
                mVoiceUtils.stopWarnningVoice();

            }
        });
        mWarnningDialog.show();
    }

    private PopupWindow.OnDismissListener popDismissListener = new PopupWindow.OnDismissListener() {
        @Override
        public void onDismiss() {
//            mChannelAdapter.notifyDataSetChanged();
        }
    };

    private void showWheelControlPop(int position) {
        WheelControlDialog dialog = new WheelControlDialog(this,position,new ChangeStateListener());//,R.style.AlertDialogStyle
        dialog.setOnDismissListener(popDismissListener);
        dialog.showPopwindow(root);
        //点击时需要震动
        Channel channel = Channels.getInstance().get(position);
        mPresenter.vibrateChannel(channel);
    }

    private void channelChange(int position) {
        Channel channel = Channels.getInstance().get(position);
        if(channel == null) return ;

        if (mCurrIndex != position) {
            setCurrentIndex(position);
        }

        if (channel.getConnectionStatus() == ConnectionStatus.OFFLINE) {
            return ;
        }

        Channel.Info realState = channel.getRealState();
        Channel.Info targetState = channel.getTargetState();
        int temp = targetState.getTemp();
        if(!channel.isWorking()) {
            //开启灸头
            if (temp ==0 || targetState.getTimeSeconds() == 0) {
//                showSettingDialog(position, ChannelSettingDidalog.OP_MODE_BOTH);
                showWheelControlPop(position);
                return ;
            }
            //保持临时时间，防止频繁点击导致记录的时间数据不准确 add by mojinshu
            if(firstSetTime != null){
                if(null == firstSetTime.get(position)){
                    firstSetTime.put(position,channel.getTargetState().getTimeSeconds()+1);
                }
            }
            mPresenter.openChannel(channel); //开启灸头
            //温度提醒
            if (temp >= VoiceUtils.WARNNING_WENDU /*&& playSound*/) {
                mVoiceUtils.startWarnningVoice(temp);
                showWarnningDialog(mCurrIndex,temp);
            }

        }else{
            //关闭灸头
            mPresenter.closeChannel(channel);
        }

        mChannelAdapter.notifyItemChanged(position);
        checkCurrentChannelStatus();

    }

    protected int toInt(String str, int defaultVal) {
        if (TextUtils.isEmpty(str)) {
            return defaultVal;
        }
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            //do nothing
        }
        return defaultVal;
    }

    public void showLoadingIndicator(boolean showProgress,String msg) {
        if (!isShowControlView){
            if (!TextUtils.isEmpty(msg) && msg.contains("AJi9")){
                msg = msg.replace(getString(R.string.moxa_linked_ble_hint),"");
                setConnectDeviceInfo(msg);
            }
            if(!TextUtils.isEmpty(msg) && msg.contains(getString(R.string.moxa_connecting_ble_hint))){
                mTopStateView.setBlueState(msg);
                mTopStateView.showProgressBar();
            }
            if(!TextUtils.isEmpty(msg) && msg.contains(getString(R.string.moxa_cannot_open_ble_hint))){
                mTopStateView.setBlueState(msg);
            }
            return;
        }
        mTopStateView.setBlueState(msg);
        isShowGuide(msg);
        if (showProgress) {
            mTopStateView.showProgressBar();
        } else {
            mTopStateView.hideProgressBar();
        }
    }

    private void setConnectDeviceInfo(String name) {
        String deviceNameText = mDeviceGuideView.getDeviceNameText();
        if (!isShowControlView && !TextUtils.equals(deviceNameText,name)){
            mDeviceGuideView.setDeviceIconAndName(R.mipmap.ajys_connect, name);
            mDeviceGuideView.setCurrentMode(DeviceGuideView.TYPE_DEVICE_ONLINE);
            mDeviceGuideView.setOnMyDeviceTextClickListener(myDeviceTextClickListener);
            mTopStateView.setBlueState(getString(R.string.select_other_mode));
        }
    }

    private View.OnClickListener myDeviceTextClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            isShowGuide(false);
            presenterStart();
            String str = getResources().getString(R.string.moxa_linked_ble_hint);
            mTopStateView.setBlueState(str + mDeviceGuideView.getDeviceNameText());
        }
    };

}
