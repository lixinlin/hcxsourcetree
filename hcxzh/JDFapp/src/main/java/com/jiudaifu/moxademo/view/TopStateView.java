package com.jiudaifu.moxademo.view;

import android.content.Context;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jiudaifu.moxademo.R;

/**
 * author: mojinshu
 * date: 2018/8/15 上午11:56
 * description: 自定义顶部视图
 */
public class TopStateView extends RelativeLayout implements View.OnClickListener {

    private ImageView mLampImgView = null;
    private TextView mBleStateTextView = null;
    private ProgressBar mProgressBar = null;
    private TopStateListener mListener = null;
    private boolean mLampState = true;

    private ImageButton mReceipControl;
    private ImageButton moxaBack;

    public TopStateView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public TextView getBleStateView() {
        return mBleStateTextView;
    }

    private void initView() {

        LayoutInflater inflater = (LayoutInflater) this.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.top_state_view, this, true);
        findViewById(R.id.top_state_color_iv).setOnClickListener(this);
//        findViewById(R.id.blue_icon_ib).setOnClickListener(this);
        mLampImgView = (ImageView) findViewById(R.id.lamp_iv);
        mLampImgView.setOnClickListener(this);
        mReceipControl = (ImageButton) findViewById(R.id.settings_icon_ib);
        mReceipControl.setOnClickListener(this);
        moxaBack = findViewById(R.id.moxa_back);
        moxaBack.setOnClickListener(this);
        mBleStateTextView = (TextView) findViewById(R.id.blue_state_iv);
        mBleStateTextView.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        mBleStateTextView.getPaint().setAntiAlias(true);
        mBleStateTextView.setOnClickListener(this);
        mProgressBar = (ProgressBar) findViewById(R.id.progress);
    }

    public ImageView getHeadIconView(){
        return mLampImgView;
    }

    public void showReceipControl(boolean show){
        if(show){
            mReceipControl.setVisibility(View.VISIBLE);
        }else{
            mReceipControl.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        int id = v.getId();
//		if(id == R.id.blue_icon_ib)
//		{
//			if(mListener != null){
//				mListener.clickBlueIcon();
//			}
//		}
        if(id == R.id.lamp_iv){
//			mLampImgView.setBackgroundResource(R.drawable.lampoff);
            if(mListener != null){
                mListener.onHeadClicked(v);
            }
        }
        else if(id == R.id.blue_state_iv || id == R.id.top_state_color_iv)
        {
            if(mListener != null){
                mListener.clickBlueStateText();
            }
        }
        else if(id == R.id.settings_icon_ib)
        {
            if(mListener != null){
                mListener.clickSettingBtn();
            }
        }
        else if(id == R.id.moxa_back){
            if (mListener != null){
                mListener.onClickLeftView(v);
            }
        }
    }

    /*
     * 切换灯状态
     */
    public boolean switchLampState(boolean update){
        mLampState = !mLampState;
        if(update){
            setLampState(mLampState);
        }
        return mLampState;
    }

    /*
     * 设置蓝牙灯状态
     */
    public void setLampState(boolean on){
        if(on){
            mLampState = true;
//			mLampImgView.setBackgroundResource(R.drawable.btn_on);
        }else{
            mLampState = false;
//			mLampImgView.setBackgroundResource(R.drawable.btn_off);
        }
        if(mListener != null){
            mListener.onLampStateChange(mLampState);
        }
    }

    /*
     * 获取状态
     */
    public boolean getLampState(){
        return mLampState;
    }


    /*
     * 设置蓝牙状态
     */
    public void setBlueState(String msg)
    {
        mBleStateTextView.setText(msg);
    }

    public String getBlueStateText(){
        String text = mBleStateTextView.getText().toString().trim();
        return text == null ?"":text;
    }

    /*
     * show 进度
     */
    public void showProgressBar()
    {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    /*
     * hide 进度
     */
    public void hideProgressBar()
    {
        mProgressBar.setVisibility(View.INVISIBLE);
    }

    /*
     * 定义外部监听接口
     */
    public interface TopStateListener
    {
        void clickBlueStateText();
        void onHeadClicked(View v);
        //		void clickBlueIcon();
        void clickSettingBtn();

        void onLampStateChange(boolean turnOn);

        void onClickLeftView(View view);
    };

    /*
     * 设置监听的回调
     */
    public void setTopStateListener(TopStateListener listener)
    {
        this.mListener = listener;
    }
}
