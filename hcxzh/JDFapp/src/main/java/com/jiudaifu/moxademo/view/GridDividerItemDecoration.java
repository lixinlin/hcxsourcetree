package com.jiudaifu.moxademo.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.jiudaifu.moxademo.R;

/**
 * author: mojinshu
 * date: 2018/8/20 下午3:40
 * description:
 */
public class GridDividerItemDecoration extends RecyclerView.ItemDecoration{

    private Drawable mDivider;
    private int mSpanCount = 2;

    public GridDividerItemDecoration(Context context, int spanCount)
    {
        mSpanCount = spanCount;
        mDivider = context.getResources().getDrawable(R.drawable.channel_grid_divider);
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state)
    {

        drawHorizontal(c, parent);
        drawVertical(c, parent);

    }

    public void drawHorizontal(Canvas c, RecyclerView parent)
    {
        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++)
        {
            final View child = parent.getChildAt(i);
            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                    .getLayoutParams();
            final int left = child.getLeft() - params.leftMargin;
            final int right = child.getRight() + params.rightMargin + mDivider.getIntrinsicWidth();
            final int top = child.getBottom() + params.bottomMargin;
            final int bottom = top + mDivider.getIntrinsicHeight();
            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }
    }

    public void drawVertical(Canvas c, RecyclerView parent)
    {
        final int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++)
        {
            final View child = parent.getChildAt(i);

            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                    .getLayoutParams();
            final int top = child.getTop() - params.topMargin;
            final int bottom = child.getBottom() + params.bottomMargin;
            final int left = child.getRight() + params.rightMargin;
            final int right = left + mDivider.getIntrinsicWidth();

            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

        int itemPosition = parent.getChildAdapterPosition(view);
        int childCount = parent.getAdapter().getItemCount();
        final boolean isLastRow = isLastRaw(itemPosition, mSpanCount, childCount);
        final boolean isLastCol = isLastColum(itemPosition, mSpanCount);
        if (!isLastRow && !isLastCol) {
            outRect.set(0, 0, mDivider.getIntrinsicWidth(), mDivider.getIntrinsicHeight());
            return ;
        }

        if (isLastRow && isLastCol) {
            outRect.set(0, 0, 0, 0);
            return ;
        }

        // 如果是最后一行，则不需要绘制底部
        if (isLastRow && !isLastCol) {
            outRect.set(0, 0, mDivider.getIntrinsicWidth(), 0);
            return ;
        }
        // 如果是最后一列，则不需要绘制右边
        if (isLastCol && !isLastRow) {
            outRect.set(0, 0, 0, mDivider.getIntrinsicHeight());
        }
    }

    private boolean isLastColum(int pos, int spanCount)
    {
        return ((pos+1) % spanCount == 0);
    }

    private boolean isLastRaw(int pos, int spanCount,
                              int childCount) {

        int rowCount = (childCount+spanCount-1)/spanCount;
        int rowNum = 0;
        if ((pos +1)% spanCount == 0){
            rowNum = (pos+1)/spanCount;
        } else {
            rowNum = (pos+1)/spanCount+1;
        }
        return (rowNum == rowCount);
    }
}
