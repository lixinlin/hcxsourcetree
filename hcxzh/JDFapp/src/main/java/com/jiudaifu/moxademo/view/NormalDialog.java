package com.jiudaifu.moxademo.view;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.jiudaifu.moxademo.R;

/**
 * author: mojinshu
 * date: 2018/8/20 下午4:05
 * description:
 */
public class NormalDialog extends Dialog{

    private TextView title,msg;
    private TextView cancel,besure;

    public NormalDialog(Context context) {
        this(context, R.style.NormalDialogStyle);
    }

    public NormalDialog(Context context, int theme) {
        super(context, theme);
        setContentView(R.layout.layout_normal_dialog);
        initView();
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        int width = getContext().getResources().getDisplayMetrics().widthPixels;
        //取屏幕最短边的80%作为对话框的宽
        lp.width = (int)(width*0.8);
    }

    private void initView() {
        title = (TextView) findViewById(R.id.txt_title_dialog);
        msg = (TextView) findViewById(R.id.txt_msg_dialog);
        cancel = (TextView) findViewById(R.id.text_cancel_dialog);
        besure = (TextView) findViewById(R.id.text_sure_dialog);
    }

    public void setMsg(String msgText){
        msg.setText(msgText);
    }
    public void setMsg(int resId){
        setMsg(getContext().getString(resId));
    }

    public void setTitle(String titles){
        title.setText(titles);
    }

    public TextView getTitleView(){
        return title;
    }

    public TextView getMsgView(){
        return msg;
    }

    public void setPositiveText(String text){
        besure.setText(text);
    }

    public void setNegativeText(String text){
        cancel.setText(text);
    }

    public void setOnlyShowPositiveButton(){
        cancel.setVisibility(View.GONE);
        findViewById(R.id.line_dialog).setVisibility(View.GONE);

    }

    public void setPositiveListener(View.OnClickListener listener){
        if (listener!=null){
            besure.setOnClickListener(listener);
        }
    }

    public void setNegativeListener(View.OnClickListener listener){
        if (listener!=null){
            cancel.setOnClickListener(listener);
        }
    }
}
