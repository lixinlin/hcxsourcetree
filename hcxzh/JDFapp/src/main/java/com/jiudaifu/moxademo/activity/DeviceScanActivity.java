package com.jiudaifu.moxademo.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.jiudaifu.moxademo.R;
import com.jiudaifu.moxademo.view.CustomProgressDialog;
import com.jiudaifu.moxademo.view.PasswdInputDialog;
import com.telink.bluetooth.LeBluetooth;
import com.telink.bluetooth.event.MeshEvent;
import com.telink.bluetooth.light.DeviceInfo;
import com.telink.ibluetooth.expose.ChannelListPresenter;
import com.telink.ibluetooth.expose.DeviceNameUtils;
import com.telink.ibluetooth.interfaces.ChannelListContract;
import com.telink.ibluetooth.model.Channel;
import com.telink.ibluetooth.model.ChannelGroup;
import com.telink.ibluetooth.model.ChannelGroups;
import com.telink.ibluetooth.model.GroupId;
import com.telink.ibluetooth.model.Mesh;


/**
 * author: mojinshu
 * date: 2018/8/22 上午10:19
 * description:
 */
public class DeviceScanActivity extends ActionBarActivity implements View.OnClickListener,ChannelListContract.View{

    private ExpandableListView mGroupListView;

    private TextView mEmptyView;

    private TextView mCurrGroupView;

    private Button mScanButton;

    private PasswdInputDialog mPasswdDialog;

    private GroupAdapter mAdapter;

    private ChannelGroups mGroups;

    /**
     * 当前连接的网络组
     */
    private ChannelGroup mCurrGroup;

    /**
     * 当前正在移动的灸头
     */
    private int mMoveChannelPos;

    /**
     * 当前正在移动的灸头组
     */
    private int mMoveGroupPos;

    private Handler mHandler = new Handler();

    private ChannelGroup group;
    private Context mContext;

    private ChannelListContract.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = DeviceScanActivity.this;
        mPresenter = new ChannelListPresenter(mContext,this);
        setContentView(R.layout.moxa_activity_device_scan);
        setTitle(getString(R.string.title_device_scan));
        initUI();

        mGroups = new ChannelGroups();

        mAdapter = new GroupAdapter();
        mGroupListView.setAdapter(mAdapter);

        showEmpty(false);

        mPresenter.init(mGroups);
        mPresenter.startScan();
    }

    private void initUI() {

        mGroupListView = (ExpandableListView) findViewById(R.id.channel_list);
        mGroupListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return true;
            }
        });
        mEmptyView = (TextView) findViewById(R.id.empty_view);
        mScanButton = (Button) findViewById(R.id.btn_search);
        mScanButton.setOnClickListener(this);

        mCurrGroupView = (TextView) findViewById(R.id.curr_group);
    }

    private void getPassword(){
        Intent intent = new Intent();
        intent.setClass(this,ForgotPasswdActivity.class);
        startActivity(intent);
    }


    private void showMeshPasswdDialog(final int groupPosition, final String message) {
        if(null != mHandler){
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mPasswdDialog = PasswdInputDialog.create(DeviceScanActivity.this)
                            .setMessage(message)
                            .setPositiveListener(new View.OnClickListener(){

                                @Override
                                public void onClick(View v) {
                                    mPasswdDialog.dismiss();
                                    String input = mPasswdDialog.getInputPasswd();
                                    if (mPresenter.isRightPwd(input)) {
                                        mPresenter.changeMesh(groupPosition,input);
                                    } else {
                                        showMeshPasswdDialog(groupPosition,getString(R.string.pswd_error));
                                    }
                                }
                            })
                            .setNeutralListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    getPassword();
                                }
                            });
                    if(null != DeviceScanActivity.this && !DeviceScanActivity.this.isFinishing()){
                        mPasswdDialog.show();
                    }

                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mPasswdDialog != null) mPasswdDialog.dismiss();

        if (mProDialog != null) mProDialog.dismiss();

        mPresenter.stopScan();
        mPresenter.end();
    }

    private void showEmpty(boolean show) {
        if (show) {
            mGroupListView.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);
        } else {
            mEmptyView.setVisibility(View.GONE);
            mGroupListView.setVisibility(View.VISIBLE);
        }
    }

    private void showConnectErrorDialog() {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(R.string.moxa_connect_error_title)
                .setMessage(R.string.moxa_connect_error_msg)
                .setNeutralButton(R.string.btn_confirm,null)
                .create();
        dialog.show();
    }

    private CustomProgressDialog mProDialog;

    private void hideProgressDialog() {
        if (mProDialog != null && mProDialog.isShowing()) {
            mProDialog.dismiss();
        }
        mProDialog = null;
    }

    private void showProgressDialog(String message, DialogInterface.OnCancelListener onCancelListener) {
        hideProgressDialog();
        mProDialog = new CustomProgressDialog(this);
        if (onCancelListener != null) {
            mProDialog.setOnCancelListener(onCancelListener);
        }
        mProDialog.setCanceledOnTouchOutside(false);
        mProDialog.setMessage(message);
        mProDialog.show();
    }

    @Override
    public void onClick(View v) {
        final int id = v.getId();
        if (id == R.id.btn_search) {
            if (!LeBluetooth.getInstance().isScanning()) {
                if(mGroups.size() != 0){
                    mGroups.clear();
                }
                mPresenter.startScan();
            }
        }
    }

    private void showChannelPswdDialog(String message) {
        mPasswdDialog = PasswdInputDialog.create(DeviceScanActivity.this)
                .setMessage(message)
                .setPositiveListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mPasswdDialog.dismiss();
                        String input = mPasswdDialog.getInputPasswd();
                        if (mPresenter.isRightPwd(input)) {
                            Channel channel = mGroups.get(mMoveGroupPos).get(mMoveChannelPos);
                            mPresenter.addChannel(channel,input);
                        } else {
                            showChannelPswdDialog(getString(R.string.pswd_error));
                        }
                    }
                })
                .setNeutralListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getPassword();
                    }
                });
        mPasswdDialog.show();
    }

    @Override
    public void addConnectingGroup(ChannelGroup group) {
        if(mGroups != null && null != group){
            ChannelGroup group1 = mGroups.findById(group.getGroupId());
            if(group1 != null){
                mGroups.remove(0);
            }
            mGroups.add(group,0);
            mAdapter.notifyDataSetChanged();
            mGroupListView.expandGroup(mAdapter.getGroupCount()-1);
        }
    }

    @Override
    public void setConnectingGroup(ChannelGroup group) {
        mCurrGroup = group;
        if (mCurrGroup == null) {
            mCurrGroupView.setText(getString(R.string.fmt_curr_group,getString(R.string.ununited)));
        } else {
            mCurrGroupView.setText(getString(R.string.fmt_curr_group,mCurrGroup.getName()));
        }
        mGroups.active(group == null?null:group.getGroupId());
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void showSearchLoading() {
        showProgressDialog(getString(R.string.searching),null);
    }

    @Override
    public void hideSearchLoading() {
        hideProgressDialog();
    }

    @Override
    public void showAddLoading() {
        showProgressDialog(getString(R.string.adding),null);
    }

    @Override
    public void hideAddLoading() {
        hideProgressDialog();
    }

    @Override
    public void showMeshPasswdDialog(int groupPosition) {
        showMeshPasswdDialog(groupPosition,getString(R.string.msg_input_pswd));
    }

    @Override
    public void showChannelPswdDialog(Channel channel) {
        if (mPasswdDialog != null && mPasswdDialog.isShowing()) mPasswdDialog.dismiss();
        showChannelPswdDialog(getString(R.string.fmt_add_to_group,channel.getDeviceName()));
    }

    @Override
    public void noConncetDevice() {
        showToast(getString(R.string.hint_no_connection));
    }

    @Override
    public void noScanDevice() {
        showToast(getString(R.string.hint_no_info));
    }

    @Override
    public void cannotAdd() {
        showToast(getString(R.string.upper_limit));
    }

    @Override
    public void refreshData(boolean isNew) {
        mAdapter.notifyDataSetChanged();
        if(isNew){
            mGroupListView.expandGroup(mAdapter.getGroupCount()-1);
        }
    }

    @Override
    public void onMeshError(MeshEvent event) {
        new AlertDialog.Builder(DeviceScanActivity.this).setMessage(R.string.restart_bluetooth1).show();
    }

    @Override
    public void onMeshUpdateSuccess(MeshEvent event) {
        DeviceInfo deviceInfo = event.getDeviceInfo();

        deviceInfo.deviceName = DeviceNameUtils.fillDeviceName(deviceInfo.deviceName);

        //从旧的网络组移除
        ChannelGroup moveGroup = mGroups.get(mMoveGroupPos);
        moveGroup.remove(mMoveChannelPos);
        if (moveGroup.isEmpty()) {
            mGroups.remove(mMoveGroupPos);
        }

        //添加到新的网络组
        ChannelGroup group = mGroups.findById(new GroupId(deviceInfo.meshName, Mesh.DEFAULT_FACTORY_PSWD));
        if(group != null){
            Channel channel = group.findChannelByMeshAddress(deviceInfo.meshAddress);
            if (channel == null) {
                channel = Channel.create(deviceInfo);
                group.add(channel);

                mPresenter.collect(deviceInfo.meshName,deviceInfo.meshAddress);

            }
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onMeshUpdateFailure(MeshEvent event) {
        showConnectErrorDialog();
    }

    @Override
    public void connectSuccess() {
        finish();
    }

    @Override
    public void setPresenter(ChannelListContract.Presenter presenter) {

    }

    private class GroupButtonClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            int groupPosition = (int) v.getTag();
            group = mGroups.get(groupPosition);
            mPresenter.changeMesh(groupPosition,null);
        }
    }

    private class ButtonClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            String posStr = (String)v.getTag();
            String[] pos = posStr.split("_");

            final int groupPos = Integer.parseInt(pos[0]);
            final int childPos = Integer.parseInt(pos[1]);

            ChannelGroup group = mAdapter.getGroup(groupPos);

            if (!group.isActive()) {
                Channel channel = mGroups.get(groupPos).get(childPos);
                mMoveGroupPos = groupPos;
                mMoveChannelPos = childPos;
                mPresenter.addChannel(channel,null);
            }

        }
    }

    private class GroupAdapter extends BaseExpandableListAdapter {

        private int mGroupDividerHeight = 0;

        public GroupAdapter() {
            mGroupDividerHeight = getResources().getDimensionPixelSize(R.dimen.group_divider_height);
        }

        @Override
        public int getGroupCount() {
            return mGroups.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return mGroups.get(groupPosition).size();
        }

        @Override
        public ChannelGroup getGroup(int groupPosition) {
            return mGroups.get(groupPosition);
        }

        @Override
        public Channel getChild(int groupPosition, int childPosition) {
            return mGroups.get(groupPosition).get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        public void remove(int groupPos,int childPosition) {
            ChannelGroup group = getGroup(groupPos);
            group.remove(childPosition);
            notifyDataSetChanged();
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            GroupViewHolder holder;
            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.moxa_channel_group_item,null);
                holder = new GroupViewHolder(convertView);
                holder.mChangeButton.setOnClickListener(new GroupButtonClickListener());
                convertView.setTag(holder);
            } else {
                holder = (GroupViewHolder) convertView.getTag();
            }
            bindGroupView(holder,groupPosition);
            return convertView;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

            ChannelViewHolder holder;

            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.moxa_channel_list_item,null);
                holder = new ChannelViewHolder(convertView);
                holder.mControlButtton.setOnClickListener(new ButtonClickListener());
                convertView.setTag(holder);
            } else {
                holder = (ChannelViewHolder) convertView.getTag();
            }
            bindChannelView(holder,groupPosition,childPosition);
            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        private void bindGroupView(GroupViewHolder holder,int position) {
            if(mGroups == null || mGroups.size() == 0 ){
                return;
            }
            ChannelGroup group = mGroups.get(position);
            if (group.isActive()) {
                holder.mChangeButton.setVisibility(View.INVISIBLE);
            } else {
                holder.mChangeButton.setVisibility(View.VISIBLE);
            }
            if (position == 0) {
                holder.mItemView.setPadding(0,0,0,0);
            }else {
                holder.mItemView.setPadding(0,mGroupDividerHeight,0,0);
            }
            holder.mChangeButton.setTag(position);
            holder.mNameView.setText(getString(R.string.fmt_mesh_name,group.getName()));
        }

        private void bindChannelView(ChannelViewHolder holder,int groupPos,int position) {
            ChannelGroup group = getGroup(groupPos);
            Channel channel = getChild(groupPos,position);
            DeviceInfo deviceInfo = channel.getDeviceInfo();
            if (deviceInfo != null) {
                holder.mDeviceName.setText(getString(R.string.fmt_chl_id,deviceInfo.deviceName));
            } else {
                holder.mDeviceName.setText(getString(R.string.fmt_chl_id,"Unkown device"));
            }

            if (group.isActive()) {
                holder.mControlButtton.setVisibility(View.INVISIBLE);
            } else {
                holder.mControlButtton.setVisibility(View.VISIBLE);
            }
            holder.mControlButtton.setTag(groupPos+"_"+position);
        }
    }

    private class GroupViewHolder{

        View mItemView;

        TextView mNameView;

        ImageButton mChangeButton;

        public GroupViewHolder(View itemView) {
            mItemView = itemView;
            mNameView = (TextView) itemView.findViewById(R.id.group_name);
            mChangeButton = (ImageButton) itemView.findViewById(R.id.btn_change_mesh);
        }

    }

    private class ChannelViewHolder {

        TextView mDeviceName;
        ImageButton   mControlButtton;

        public ChannelViewHolder(View itemView) {
            mDeviceName = (TextView) itemView.findViewById(R.id.device_name);
            mControlButtton = (ImageButton) itemView.findViewById(R.id.btn_action);
        }
    }
}
