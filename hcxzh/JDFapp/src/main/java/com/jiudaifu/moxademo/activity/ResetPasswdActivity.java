package com.jiudaifu.moxademo.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jiudaifu.moxademo.R;
import com.jiudaifu.moxademo.view.CustomProgressDialog;
import com.telink.ibluetooth.expose.ResetPasswdPresenter;
import com.telink.ibluetooth.interfaces.ResetPasswdContract;

/**
 * author: mojinshu
 * date: 2018/8/21 下午4:30
 * description:
 */
public class ResetPasswdActivity extends ActionBarActivity implements ResetPasswdContract.View{

    private EditText mNewPasswd;
    private EditText mAgainPasswd;

    private Button mResetButton;

    private CustomProgressDialog mProDialog;

    private ResetPasswdContract.Presenter mPresenter;

    private String newPasswd;
    private String againPasswd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new ResetPasswdPresenter(this,this);
        setContentView(R.layout.activity_reset_passwd);

        setTitle(R.string.title_reset_passwd);

        mPresenter.start();

        mNewPasswd = (EditText) findViewById(R.id.et_new_passwd);
        mAgainPasswd = (EditText) findViewById(R.id.et_again_passwd);
        mResetButton = (Button) findViewById(R.id.btn_confirm);

        mResetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newPasswd = mNewPasswd.getText().toString();
                againPasswd = mAgainPasswd.getText().toString();
                mPresenter.resetPwd(newPasswd,againPasswd);
            }
        });

        findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResetPasswdActivity.this.finish();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.end();
    }

    public void clearNewPasswd(View v) {
        mNewPasswd.setText("");
    }

    public void clearNewPasswdAgain(View v) {
        mAgainPasswd.setText("");
    }

    private void showProgressDialog(String message) {
        hideProgressDialog();
        mProDialog = new CustomProgressDialog(this);
        mProDialog.setCanceledOnTouchOutside(false);
        mProDialog.setMessage(message);
        mProDialog.show();
    }

    private void hideProgressDialog() {
        if (mProDialog != null && mProDialog.isShowing()) {
            mProDialog.dismiss();
        }
        mProDialog = null;
    }

    @Override
    public void inputError(int errorCode) {
        switch (errorCode){
            case ResetPasswdContract.ERROR_PWD:
                Toast.makeText(this,getString(R.string.pswd_error),Toast.LENGTH_SHORT).show();
                break;
            case ResetPasswdContract.NOT_SAME_PWD:
                Toast.makeText(this,getString(R.string.pswd_not_same),Toast.LENGTH_SHORT).show();
                break;
            case ResetPasswdContract.NO_CONTECT:
                Toast.makeText(this,getString(R.string.hint_no_connection),Toast.LENGTH_SHORT).show();
                break;
            case ResetPasswdContract.NO_ONLINE:
                Toast.makeText(this,getString(R.string.no_online),Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void resetSuccess() {
        hideProgressDialog();
        Toast.makeText(this,getString(R.string.msg_pswd_reset_success),Toast.LENGTH_SHORT).show();
        //返回
        finish();
    }

    @Override
    public void resetFailure() {
        hideProgressDialog();
        Toast.makeText(this,getString(R.string.msg_pswd_reset_failure),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void resetting() {
        showProgressDialog(getString(R.string.msg_reseting_passwd));
    }

    @Override
    public void setPresenter(ResetPasswdContract.Presenter presenter) {

    }
}
