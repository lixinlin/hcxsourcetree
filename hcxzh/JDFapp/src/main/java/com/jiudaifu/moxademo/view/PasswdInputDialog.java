package com.jiudaifu.moxademo.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jiudaifu.moxademo.R;
import com.telink.ibluetooth.expose.JDFMeshUtils;

/**
 * author: mojinshu
 * date: 2018/8/20 上午11:31
 * description:
 */
public class PasswdInputDialog extends Dialog implements View.OnClickListener{

    private String mMessage;

    private TextView mMsgView;

    private EditText mPasswdInput;

    private View.OnClickListener mPositiveListener;

    private View.OnClickListener mNegativeListener;

    private View.OnClickListener mNeutralListener;

    public PasswdInputDialog(Context context, String message) {
        this(context,message,null);
    }

    public PasswdInputDialog(Context context, String message, View.OnClickListener listener) {
        super(context, R.style.Theme_Dialog_Custom);
        this.mMessage = message;
        mPositiveListener = listener;
    }

    public static PasswdInputDialog create(Context context){
        return new PasswdInputDialog(context,null);
    }

    public PasswdInputDialog setNegativeListener(View.OnClickListener listener) {
        mNegativeListener = listener;
        return this;
    }

    public PasswdInputDialog setNeutralListener(View.OnClickListener listener) {
        mNeutralListener = listener;
        return this;
    }

    public PasswdInputDialog setPositiveListener(View.OnClickListener listener) {
        mPositiveListener = listener;
        return this;
    }

    public PasswdInputDialog setMessage(String message) {
        this.mMessage = message;
        if (mMsgView != null){
            mMsgView.setText(mMessage);
        }
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_passwd_input);

        WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics out = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(out);

        View v = findViewById(R.id.container);
        // 调整dialog背景大小
        v.setLayoutParams(new FrameLayout.LayoutParams((int) (out.widthPixels * 0.85), LinearLayout.LayoutParams.WRAP_CONTENT));

        mMsgView = (TextView) findViewById(R.id.msg_view);
        mMsgView.setText(mMessage);

        mPasswdInput = (EditText) findViewById(R.id.et_input);

        findViewById(R.id.btn_confirm).setOnClickListener(this);
        findViewById(R.id.btn_cancel).setOnClickListener(this);

        if (mNeutralListener != null) findViewById(R.id.btn_neutral).setOnClickListener(mNeutralListener);

    }

    public String getInputPasswd() {
        return mPasswdInput.getText().toString();
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.btn_confirm) {
            if (!JDFMeshUtils.isValidPwd(getInputPasswd())) {
                setMessage(getContext().getString(R.string.pswd_error));
                return ;
            }
            if (mPositiveListener != null) mPositiveListener.onClick(v);

        } else if (v.getId() == R.id.btn_cancel){
            dismiss();

            if (mNegativeListener != null) mNegativeListener.onClick(v);
        }
    }
}
