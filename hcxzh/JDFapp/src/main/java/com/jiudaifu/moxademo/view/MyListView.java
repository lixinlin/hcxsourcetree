package com.jiudaifu.moxademo.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * author: mojinshu
 * date: 2018/8/15 下午2:49
 * description:
 */
public class MyListView extends ListView {

    public MyListView(Context context) {
        this(context,null);
    }

    public MyListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }
}
