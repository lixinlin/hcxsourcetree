package com.jiudaifu.moxademo.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;

/**
 * author: mojinshu
 * date: 2018/8/20 上午9:31
 * description:
 */
public class ShadowImageView extends ImageView {

    private AlphaAnimation animationFadeIn;
    private AlphaAnimation animationFadeOut;
    private final int BREATH_INTERVAL_TIME = 1000;
    private boolean cleared = true;

    public ShadowImageView(Context context) {
        this(context,null);
    }

    public ShadowImageView(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public ShadowImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAnimation();
    }

    private void initAnimation() {
        animationFadeIn = new AlphaAnimation(0.1f,1.0f);
        animationFadeIn.setDuration(BREATH_INTERVAL_TIME);

        animationFadeOut = new AlphaAnimation(1.0f,0.1f);
        animationFadeOut.setDuration(BREATH_INTERVAL_TIME);

        animationFadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (!cleared){
                    startAnimation(animationFadeOut);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animationFadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (!cleared){
                    startAnimation(animationFadeIn);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }

    public void clearAnim(){
        clearAnimation();
        cleared = true;
    }

    public void startAnim(){
        if (cleared){
        }
        startAnimation(animationFadeOut);
        cleared = false;
    }
}
