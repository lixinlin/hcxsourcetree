package com.jiudaifu.moxademo.jiuliaoData;

/**
 * Created by lixinlin on 2018/11/12.
 */

public class I9DataHttp {
    private String member;
    private String deviceSn;
    private String num;
    private int temperature;
    private Long starTime;
    private Long endtime;
    private int duration;

    public I9DataHttp(String member, String deviceSn, String num, int temperature, Long starTime, Long endtime, int duration) {
        this.member = member;
        this.deviceSn = deviceSn;
        this.num = num;
        this.temperature = temperature;
        this.starTime = starTime;
        this.endtime = endtime;
        this.duration = duration;
    }

    public String getMember() {
        return member;
    }

    public void setMember(String member) {
        this.member = member;
    }

    public String getDeviceSn() {
        return deviceSn;
    }

    public void setDeviceSn(String deviceSn) {
        this.deviceSn = deviceSn;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public Long getStarTime() {
        return starTime;
    }

    public void setStarTime(Long starTime) {
        this.starTime = starTime;
    }

    public Long getEndtime() {
        return endtime;
    }

    public void setEndtime(Long endtime) {
        this.endtime = endtime;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public I9DataHttp() {
    }
}
