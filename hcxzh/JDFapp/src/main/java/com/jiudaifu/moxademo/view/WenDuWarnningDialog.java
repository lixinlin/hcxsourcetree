package com.jiudaifu.moxademo.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.jiudaifu.moxademo.R;
import com.telink.ibluetooth.utils.VoiceUtils;

/**
 * author: mojinshu
 * date: 2018/8/20 下午2:11
 * description:
 */
public class WenDuWarnningDialog extends Dialog{

    private WenDuWarnningListener mListener;
    private int mTemp, mSj;
    private int mNo;
    private Context mContext;

    public interface WenDuWarnningListener
    {
        void resetWenDu(int no);
        void confirmWenDu(int no);
    }

    public WenDuWarnningDialog(Context context, int no, int wd, WenDuWarnningListener listener) {
        super(context, R.style.Theme_Dialog_Custom);
        setCanceledOnTouchOutside(false);
        this.mNo = no;
        this.mTemp = wd;
        this.mContext = context;
        this.mListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.warnning_dialog);
        initWindow();
        findViewById(R.id.confirm_button).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(mListener != null){
                    mListener.confirmWenDu(mNo);
                }
                dismiss();
            }
        });
        findViewById(R.id.reset_button).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(mListener != null){
                    mListener.resetWenDu(mNo);
                }
                dismiss();
            }
        });

        TextView hint1 = (TextView) findViewById(R.id.tv_hint1);
        TextView hint2 = (TextView) findViewById(R.id.tv_hint2);
        if(mTemp == VoiceUtils.WARNNING_WENDU){
            hint1.setText(mContext.getResources().getString(R.string.moxa_enter_gaowenqu_hint10));
            hint2.setText(mContext.getResources().getString(R.string.moxa_enter_gaowenqu_hint11));
        } else if(mTemp >= 49 && mTemp <= 52){
            hint1.setText(mContext.getResources().getString(R.string.moxa_enter_gaowenqu_hint20));
            hint2.setText(mContext.getResources().getString(R.string.moxa_enter_gaowenqu_hint21));
        }
        else if(mTemp >= 53 && mTemp <= 55){
            hint1.setText(mContext.getResources().getString(R.string.moxa_enter_gaowenqu_hint30));
            hint2.setText(mContext.getResources().getString(R.string.moxa_enter_gaowenqu_hint31));
        }
        else if(mTemp == 56){
            hint1.setText(mContext.getResources().getString(R.string.moxa_enter_gaowenqu_hint40));
            hint2.setText(mContext.getResources().getString(R.string.moxa_enter_gaowenqu_hint41));
        }
    }

    private void initWindow(){
        WindowManager wm = getWindow().getWindowManager();
        DisplayMetrics dm = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(dm);

        int w = dm.widthPixels;
        int h = dm.heightPixels;

        int smallestWidth = Math.min(w,h);

        WindowManager.LayoutParams lp = getWindow().getAttributes();
        //取屏幕最短边的85%作为对话框的宽
        lp.width = (int)(smallestWidth*0.85);
        lp.windowAnimations = R.anim.dialog_popup_enter;
        getWindow().setAttributes(lp);
    }
}
