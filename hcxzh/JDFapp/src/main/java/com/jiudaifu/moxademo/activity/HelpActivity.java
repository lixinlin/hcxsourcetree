package com.jiudaifu.moxademo.activity;

import android.os.Bundle;

import com.jiudaifu.moxademo.R;

/**
 * author: mojinshu
 * date: 2018/8/21 下午4:58
 * description:
 */
public class HelpActivity extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.moxa_guide);
    }
}
