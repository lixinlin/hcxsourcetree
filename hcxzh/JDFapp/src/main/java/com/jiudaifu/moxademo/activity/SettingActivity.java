package com.jiudaifu.moxademo.activity;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.jiudaifu.moxademo.R;
import com.telink.ibluetooth.sdk.Configuration;
import com.telink.ibluetooth.sdk.MoxaModule;
import com.telink.ibluetooth.utils.VoiceUtils;

/**
 * author: mojinshu
 * date: 2018/8/21 下午4:07
 * description:
 */
public class SettingActivity extends ActionBarActivity implements View.OnClickListener{

    private static final int REQCODE_GET_PASSWD 	= 1;
    private static final int REQCODE_RESET_PASSWD 	= 2;
    private static final int REQCODE_ADD_RECIPE 	= 3;
    private static final int REQCODE_SWITCH_MODEL 	= 4;

    private Configuration mAppConf = null;

    private ImageButton mVoiceSwitch;

    //设备信息,找回密码时用
    private String mDeviceName;
    private String mDevicePasswd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.moxa_activity_setting);
        setTitle(R.string.title_setting);
        mAppConf = new Configuration();
        mAppConf.read(this);

        mDeviceName = getIntent().getStringExtra(MoxaModule.EXTRA_DEVICE_NAME);
        mDevicePasswd = getIntent().getStringExtra(MoxaModule.EXTRA_DEVICE_PSWD);

        mVoiceSwitch = (ImageButton) findViewById(R.id.voice_switch);
        toggleVoiceSwitch();
        findViewById(R.id.action_setting_pswd).setOnClickListener(this);
        findViewById(R.id.action_show_help).setOnClickListener(this);
        findViewById(R.id.action_forgot_pswd).setOnClickListener(this);

    }

    private void toggleVoiceSwitch(){
        if(mAppConf.isVoiceOn()){
            mVoiceSwitch.setImageResource(R.mipmap.btn_set_s_on);
        }else{
            mVoiceSwitch.setImageResource(R.mipmap.btn_set_s_off);
        }
    }

    private int getCurrRecipeId() {

        SharedPreferences preferences = getSharedPreferences("curr_recipel_name", Context.MODE_PRIVATE);
        return preferences.getInt("id", 0);
    }

    public void showJiuLiaoRecord(View v) {
        int recipeId = getCurrRecipeId();
        if (recipeId == 0) {
            Toast.makeText(this,getString(R.string.hint_no_recipe_selected),Toast.LENGTH_SHORT).show();
            return ;
        }
        Intent intent = new Intent(MoxaModule.ACTION_LIST_RECORD);
        intent.putExtra("recipeId",recipeId);
        setResult(RESULT_OK,intent);
        finish();
    }

    public void onSettingPswd(View v) {
        Intent intent = new Intent();
        intent.setClass(this,ResetPasswdActivity.class);
        startActivityForResult(intent,REQCODE_RESET_PASSWD);
    }

    public void onForgotPswd() {
//		Intent data = new Intent(MoxaModule.ACTION_FORGOT_PSWD);
//		setResult(RESULT_OK, data);
//		finish();

        Intent intent = new Intent();
        intent.setClass(this,ForgotPasswdActivity.class);
        startActivityForResult(intent,REQCODE_GET_PASSWD);
    }

    public void onManagerObject(View v) {
        try{
            // 选择灸疗对象
            Intent intent = new Intent();
//			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            ComponentName comp = new ComponentName("com.jiudaifu.yangsheng",
                    "com.jiudaifu.yangsheng.activity.SelMoxaUserActivity");
            intent.setComponent(comp);
            startActivity(intent);
            overridePendingTransition(R.anim.a2, R.anim.a1);
        }catch(ActivityNotFoundException e){
            e.printStackTrace();
        }
    }

    public void onToggleVoice(View v) {

        mAppConf.setVoiceOn(!mAppConf.isVoiceOn());
        mAppConf.save(this);
        if(!mAppConf.isVoiceOn()){
            VoiceUtils voiceUtils = new VoiceUtils(this);
            voiceUtils.setAppConfig(mAppConf);
            voiceUtils.stopAllVoice();
        }
        toggleVoiceSwitch();
        if (!mAppConf.isVoiceOn()) {
            Toast.makeText(this,
                    getResources().getString(R.string.moxa_sound_off),
                    Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this,
                    getResources().getString(R.string.moxa_sound_on),
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void onShowHelp(View v) {
        Intent helpIntent = new Intent();
        helpIntent.setClass(this, HelpActivity.class);
        startActivity(helpIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) {
            super.onActivityResult(requestCode,resultCode,data);
        }else {
            if (requestCode == REQCODE_GET_PASSWD) {
                Intent newData = new Intent(MoxaModule.ACTION_FORGOT_PSWD);
                setResult(resultCode,newData);
                finish();
            } else if (requestCode == REQCODE_SWITCH_MODEL) {
                finish();
            } else if (requestCode == REQCODE_ADD_RECIPE){
                finish();
            }
        }

    }

    private void login(){
        Intent intent = new Intent("com.jiudaifu.intent.action.LOGIN");
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.action_setting_pswd){
            onSettingPswd(v);
        }else if(id == R.id.action_show_help){
            onShowHelp(v);
        }else if (id == R.id.action_forgot_pswd) {
            onForgotPswd();
        }
    }
}
