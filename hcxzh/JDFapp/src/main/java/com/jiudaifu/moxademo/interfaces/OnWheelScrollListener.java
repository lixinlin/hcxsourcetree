package com.jiudaifu.moxademo.interfaces;

import com.jiudaifu.moxademo.view.WheelView;

/**
 * author: mojinshu
 * date: 2018/8/20 下午2:24
 * description:
 */
public interface OnWheelScrollListener {

    /**
     * Callback method to be invoked when scrolling started.
     * @param wheel the wheel view whose state has changed.
     */
    void onScrollingStarted(WheelView wheel);

    /**
     * Callback method to be invoked when scrolling ended.
     * @param wheel the wheel view whose state has changed.
     */
    void onScrollingFinished(WheelView wheel);
}
