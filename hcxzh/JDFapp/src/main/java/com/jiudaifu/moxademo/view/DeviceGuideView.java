package com.jiudaifu.moxademo.view;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jiudaifu.moxademo.R;

import java.util.Arrays;
import java.util.List;

/**
 * author: mojinshu
 * date: 2018/8/15 下午2:48
 * description:自定义导向
 */
public class DeviceGuideView extends RelativeLayout{

    public static final int TYPE_DEVICE_ONLINE = 1; //设备在线
    public static final int TYPE_DEVICE_OFFLINE = 2;//设备未连接
    public int current_mode = TYPE_DEVICE_ONLINE;
    private TextView guideText;
    private MyListView mListView;
    private ListAdapter mAdapter;
    private LinearLayout mDeviceListLayout;
    private RelativeLayout mGuideLayout;
    private ImageView deviceIcon;
    private TextView deviceTitle;
    private String comefrom = "";

    public DeviceGuideView(Context context) {
        this(context, null);
    }

    public DeviceGuideView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.layout_device_not_found, this);
        initView();
    }

    private void initView() {
        guideText = (TextView) findViewById(R.id.text_guide_device_not_found);
        guideText.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG); //下划线;
        guideText.getPaint().setAntiAlias(true);

        mDeviceListLayout = (LinearLayout) findViewById(R.id.layout_device_guideview);

        mGuideLayout = (RelativeLayout) findViewById(R.id.layout_guide_guideview);
        deviceIcon = (ImageView) findViewById(R.id.image_icon_guide_item);
        deviceTitle = (TextView) findViewById(R.id.text_title_guide_item);

        mListView = (MyListView) findViewById(R.id.list_other_moxi);
        mListView.setAdapter(mAdapter = new ListAdapter());

//        mAdapter.setList(createList());
        setCurrentMode(TYPE_DEVICE_OFFLINE);
        setOnItemClickListener(listener);
    }

    public TextView getGuideText(){
        return this.guideText;
    }

    public String getDeviceNameText(){
        return deviceTitle.getText().toString().trim();
    }

    public void setListData(List<String> list){
        mAdapter.setList(list);
    }

    /**
     * 设置当前的展示模式（引导帮助 or 设备列表）
     * @param mode
     */
    public void setCurrentMode(int mode){
        this.current_mode = mode;
        refresh();
    }

    /**
     * 设置"我的设备"中的图标和网络名称
     * @param iconRes
     * @param name
     */
    public void setDeviceIconAndName(int iconRes,String name){
        deviceIcon.setImageResource(iconRes);
        if (!TextUtils.isEmpty(name)){
            deviceTitle.setText(name);
        }
    }

    public void setComeForm(String comefrom){
        this.comefrom = comefrom;
    }

    public void setDeviceTitle(String name){
        if (!TextUtils.isEmpty(name)){
            deviceTitle.setText(name);
        }
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener listener){
        if (listener !=null){
            mListView.setOnItemClickListener(listener);
        }

    }

    private void refresh() {
        if (current_mode == TYPE_DEVICE_ONLINE){
            mDeviceListLayout.setVisibility(VISIBLE);
            mGuideLayout.setVisibility(GONE);
        }else if(current_mode == TYPE_DEVICE_OFFLINE){
            mDeviceListLayout.setVisibility(GONE);
            mGuideLayout.setVisibility(VISIBLE);
        }
    }

    private List<String> createList(){
        String[] array = getResources().getStringArray(R.array.moxa_types);
        List<String> list = Arrays.asList(array);
        return list;
    }

    public void setOnGuideTextClickListener(OnClickListener listener){
        if (listener!=null){
            guideText.setOnClickListener(listener);
        }
    }

    public void setOnMyDeviceTextClickListener(OnClickListener listener){
        if (listener!=null){
            mDeviceListLayout.setOnClickListener(listener);
        }
    }


    private class ListAdapter extends BaseAdapter {

        private List<String> list;
        private int[] icons = {R.mipmap.ajys_ununtited_one, R.mipmap.ajys_ununtited_two, R.mipmap.ajys_ununtited_three, R.mipmap.ajys_ununtited_four};

        public ListAdapter(){
            list = createList();
        }

        public void setList(List<String> list){
            this.list = list;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return list == null?0:list.size();
        }

        @Override
        public String getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder holder = null;
            if (convertView == null){
                holder = new Holder();
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_guide_view,null);
                holder.title = (TextView) convertView.findViewById(R.id.text_title_guide_item);
                holder.icon = (ImageView) convertView.findViewById(R.id.image_icon_guide_item);
                convertView.setTag(holder);
            }
            holder = (Holder) convertView.getTag();
            String t = getItem(position);
            holder.title.setText(t);
            holder.icon.setImageResource(icons[position]);
            return convertView;
        }

        private class Holder{
            private TextView title;
            private ImageView icon;
        }
    }

    private AdapterView.OnItemClickListener listener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        }
    };

}
