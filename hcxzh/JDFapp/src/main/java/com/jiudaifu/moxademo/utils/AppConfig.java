package com.jiudaifu.moxademo.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * author: mojinshu
 * date: 2018/8/20 下午2:49
 * description:
 */
public class AppConfig {

    protected static final String SHARE_SETTINGS = "settings";
    public static final String TAB_HEIGHT = "tab_height";
    /** 灸疗模式是否是儿童模式 */
    public static final String CHILD_MOXA_MODE = "child_moxa_mode";


    protected static SharedPreferences sp(Context context) {
        return context.getSharedPreferences(SHARE_SETTINGS, 0);
    }

    protected static SharedPreferences.Editor ed(Context context) {
        return sp(context).edit();
    }


    public static int getTabLayoutHeight(Context context) {
        return sp(context).getInt(TAB_HEIGHT, 0);
    }

    public static void setTabLayoutHeight(Context context, int height) {
        ed(context).putInt(TAB_HEIGHT, height).commit();
    }

    /**
     * 获取灸疗模式
     * @param context
     * @return 0为成人模式，1为儿童模式
     */
    public static int getMoxaMode(Context context) {
        return sp(context).getInt(CHILD_MOXA_MODE, 0);
    }

    public static void setMoxaMode(Context context, int height) {
        ed(context).putInt(CHILD_MOXA_MODE, height).commit();
    }
}
