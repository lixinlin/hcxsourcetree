package com.jiudaifu.moxademo.jiuliaoData;

/**
 * Created by lixinlin on 2018/11/12.
 */

public class I9DataJDF {
    private String member;
    private String device_name;
    private String moxa_info;

    public I9DataJDF(String member, String device_name, String moxa_info) {
        this.member = member;
        this.device_name = device_name;
        this.moxa_info = moxa_info;
    }

    public I9DataJDF() {
    }

    public String getMember() {
        return member;
    }

    public void setMember(String member) {
        this.member = member;
    }

    public String getDevice_name() {
        return device_name;
    }

    public void setDevice_name(String device_name) {
        this.device_name = device_name;
    }

    public String getMoxa_info() {
        return moxa_info;
    }

    public void setMoxa_info(String moxa_info) {
        this.moxa_info = moxa_info;
    }

    @Override
    public String toString() {
        return "I9DataJDF{" +
                "member='" + member + '\'' +
                ", device_name='" + device_name + '\'' +
                ", moxa_info='" + moxa_info + '\'' +
                '}';
    }
}
