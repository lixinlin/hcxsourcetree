package com.jiudaifu.moxademo.view;

import android.content.Context;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

/**
 * author: mojinshu
 * date: 2018/8/20 上午11:48
 * description:
 */
public class RecyclerItemTouchDelegate implements RecyclerView.OnItemTouchListener {

    public interface OnItemClickListener {

        void onItemDown(View view, int position);

        void onItemClick(View view, int position);

        void onItemLongClick(View view, int position);

        void onSwitchClick(View view, int position);
    }

    private RecyclerView mRecyclerView;

    private OnItemClickListener mListener;

    private GestureDetectorCompat mGestureDetector;

    public RecyclerItemTouchDelegate(Context context, final RecyclerView recyclerView, OnItemClickListener listener) {

        mRecyclerView = recyclerView;

        mListener = listener;

        mGestureDetector = new GestureDetectorCompat(context, new GestureHandler());
        mGestureDetector.setIsLongpressEnabled(true);
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
        return mGestureDetector.onTouchEvent(e);
    }

    @Override
    public void onTouchEvent(RecyclerView view, MotionEvent motionEvent) {
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean arg0) {
        // TODO Auto-generated method stub

    }

    private class GestureHandler extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onDown(MotionEvent e) {
            View childView = mRecyclerView.findChildViewUnder(e.getX(), e.getY());

            if (childView != null) {
                int position = mRecyclerView.getChildAdapterPosition(childView);
                Log.d("TouchEvent", "item down position=" + position);
                if (mListener != null) mListener.onItemDown(childView, position);
                return true;
            }
            return false;
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            float x = e.getX();
            float y = e.getY();
            float rawX = e.getRawX();
            float rawY = e.getRawY();
            ViewGroup childView = (ViewGroup) mRecyclerView.findChildViewUnder(x, y);
            if (childView != null) {
                int position = mRecyclerView.getChildAdapterPosition(childView);
                Log.d("TouchEvent", "item click position=" + position);
                int childCount = childView.getChildCount();
                if (position >= 0 && mListener != null) {
                    for (int i = 0; i < childCount; i++) {
                        View child = childView.getChildAt(i);
                        if (isTouchPointInView(child, (int) rawX, (int) rawY)) {
                            if (child instanceof ShadowImageView) {
                                mListener.onSwitchClick(child, position);
                                return true;
                            }
                        }
                    }
                    mListener.onItemClick(childView, position);
                }

            }
            return true;
        }

        //(x,y)是否在view的区域内
        private boolean isTouchPointInView(View view, int x, int y) {
            if (view == null) {
                return false;
            }
            int[] location = new int[2];
            view.getLocationOnScreen(location);
            int left = location[0];
            int top = location[1];
            int right = left + view.getMeasuredWidth();
            int bottom = top + view.getMeasuredHeight();
            if (y >= top && y <= bottom && x >= left && x <= right) {
                return true;
            }
            return false;
        }

        @Override
        public void onShowPress(MotionEvent e) {
            super.onShowPress(e);
        }


        @Override
        public void onLongPress(MotionEvent e) {
            View childView = mRecyclerView.findChildViewUnder(e.getX(), e.getY());

            if (childView != null && mListener != null) {
                int position = mRecyclerView.getChildAdapterPosition(childView);
                Log.d("TouchEvent", "item long click position=" + position);
                mListener.onItemLongClick(childView, position);
            }
        }
    }
}
