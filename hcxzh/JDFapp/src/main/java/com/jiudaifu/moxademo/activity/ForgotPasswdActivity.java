package com.jiudaifu.moxademo.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.jiudaifu.moxademo.R;
import com.jiudaifu.moxademo.view.CustomProgressDialog;
import com.telink.ibluetooth.expose.ForgetPasswdPresenter;
import com.telink.ibluetooth.expose.MeshClient;
import com.telink.ibluetooth.interfaces.ForgetPasswdContract;

/**
 * author: mojinshu
 * date: 2018/8/21 下午4:39
 * description:
 */
public class ForgotPasswdActivity extends ActionBarActivity implements View.OnClickListener,ForgetPasswdContract.View{

    private Button mGetPasswdBtn;

    private EditText mMachineCodeEt;

    private TextView mHintView;

    private ForgetPasswdContract.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new ForgetPasswdPresenter(this,this);
        setContentView(R.layout.moxi_activity_forgot_passwd);

        setTitle(R.string.title_forgot_passwd);

        mMachineCodeEt = (EditText) findViewById(R.id.et_machine);
        mHintView = (TextView) findViewById(R.id.passwd_hint_View);

        mGetPasswdBtn = (Button) findViewById(R.id.btn_get_passwd);
        mGetPasswdBtn.setOnClickListener(this);

        findViewById(R.id.btn_call).setOnClickListener(this);

        mPresenter.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.end();
    }

    @Override
    protected void onBackClick() {
        if (!MeshClient.getInstance().isLogin() && mPresenter.isGotPasswd()) {
            goBackWithResult();
        }else {
            super.onBackClick();
        }
    }

    @Override
    public void onBackPressed() {
        if (!MeshClient.getInstance().isLogin() && mPresenter.isGotPasswd()) {
            goBackWithResult();
        }else {
            super.onBackPressed();
        }
    }

    private void goBackWithResult() {
        finish();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_get_passwd) {
            String machineCode = mMachineCodeEt.getText().toString();
            mPresenter.findPwd(machineCode);
        } else if (id == R.id.btn_call) {
            dialToCustomerService();
        }
    }

    private String mMeshName = null;

    private CustomProgressDialog mProDialog;

    private void hideProgressDialog() {
        if (mProDialog != null && mProDialog.isShowing()) {
            mProDialog.dismiss();
        }
        mProDialog = null;
    }

    private void showProgressDialog(String message, DialogInterface.OnCancelListener onCancelListener) {
        hideProgressDialog();
        mProDialog = new CustomProgressDialog(this);
        if (onCancelListener != null) {
            mProDialog.setOnCancelListener(onCancelListener);
        }
        mProDialog.setCanceledOnTouchOutside(false);
        mProDialog.setMessage(message);
        mProDialog.show();
    }

    private void dialToCustomerService() {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:400-966-8187"));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void showError(int errorCode) {
        switch (errorCode){
            case ForgetPasswdContract.NOT_SUPPORT_BLE:
                Toast.makeText(this, R.string.ble_not_supported,Toast.LENGTH_SHORT).show();
                break;
            case ForgetPasswdContract.NO_OPEN_BLE:
                Toast.makeText(this, R.string.open_buletooth_first,Toast.LENGTH_SHORT).show();
                break;
            case ForgetPasswdContract.NOT_INPUT_MACHINE_CODE:
                mHintView.setVisibility(View.VISIBLE);
                mHintView.setText(R.string.input_machine_code);
                break;
            case ForgetPasswdContract.INPUT_MACHINE_CODE_ERROR:
                mHintView.setVisibility(View.VISIBLE);
                mHintView.setText(R.string.wrong_machine_code);
                break;
            case ForgetPasswdContract.NOT_FOUND_DEVICE:
                mHintView.setText(getString(R.string.not_found_device));
                break;
        }
    }

    @Override
    public void findPwdSuccess(String passwd) {
        hideProgressDialog();
        mHintView.setVisibility(View.VISIBLE);
        mHintView.setText(getString(R.string.your_passwd_is,new Object[]{passwd}));

    }

    @Override
    public void findPwdFailure() {
        hideProgressDialog();
        mHintView.setVisibility(View.VISIBLE);
        mHintView.setText(R.string.passwd_not_found);
    }

    @Override
    public void finding() {
        showProgressDialog(getString(R.string.reback_password_now), new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mPresenter.stopfind();
            }
        });
    }

    @Override
    public void setPresenter(ForgetPasswdContract.Presenter presenter) {

    }
}
