package com.jiudaifu.moxademo.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.jiudaifu.moxademo.R;

/**
 * author: mojinshu
 * date: 2018/8/21 下午4:31
 * description:
 */
public class CustomProgressDialog extends Dialog {

    private String mMessage;

    public CustomProgressDialog(Context context) {
        super(context, R.style.Theme_Dialog_Custom);
    }

    public CustomProgressDialog(Context context, int theme) {
        super(context, theme);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_custom_progress);

        WindowManager windowManager = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics out = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(out);

        View root = findViewById(R.id.root);
        root.setLayoutParams(new FrameLayout.LayoutParams((int) (out.widthPixels * 0.85), FrameLayout.LayoutParams.WRAP_CONTENT));

        TextView messageView = (TextView) findViewById(R.id.message);
        messageView.setText(mMessage);

    }

    public void setMessage(String message) {
        this.mMessage = message;
    }
}
